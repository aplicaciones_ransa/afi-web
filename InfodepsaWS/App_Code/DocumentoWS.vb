﻿Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports DEPSA.LibCapaNegocio
'Imports InfodepsaWS.Infodepsa
Imports System.IO
'Imports Infodepsa
'Imports WsFilemaster.Infodepsa
Imports System.Data

Public Class DocumentoWS
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb = New clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objEntidad As Entidad = New Entidad
    Private objReportes As Reportes = New Reportes
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private strEndosoPath As String = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    Private strFin As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadFinanciera")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private objLiberacion As Liberacion = New Liberacion
    Private strSessMensaje As String
    Private strSessResultado As String
    Dim strSessContraseña As String
    Dim strSessFecha As String

    'Dim strNroDoc As String
    'Dim strCod_TipOtro As String
    'Dim strIdSico As String
    'Dim strNroLib As String
    'Dim strPeriodoAnual As String
    'Dim strCodDoc As String
    'Dim PagRedirect As String



    'Private objReportes As Reportes = New Reportes
    'Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    'Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    'Private oHashedData As New CAPICOM.HashedData


#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    'Private Sub pr_VALI_SESI()
    '    '--Validacion de conexion al sistema
    '    If Session("IdUsuario") = "" Then
    '        Response.Redirect("Salir.aspx?caduco=1")
    '    End If
    'End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    'Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
    '    Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
    '    ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
    '    Exit Sub
    'End Sub
#End Region


    'Public Function InicializaFirma(strNroDoc As String, strNroLib As String, strIdTipoDocumento As String, strPeriodoAnual As String, _
    '                       strCod_Doc As String, strCod_TipOtro As String, strIdSico As String, _
    '                       strCertDigital As Boolean, strVar As String, strPagina As String, strArchivoFirmado As String, _
    '                      strContrasenaClave As String, strNuevaContrasenaClave As String, strArchivoPDF As String, _
    '                       strIdUsuario As String, strUsuarioLogin As String, strIdTipoEntidad As String, strNombreEntidad As String, strNombrePDF As String) As String

    '    'sessiones de infodepsa:
    '    'strCertDigital As Boolean =  Session("CertDigital")
    '    'strVar As String =  Request.QueryString("var")
    '    'strPagina As String = Request.QueryString("Pagina")
    '    'strArchivoFirmado As String = Request.Form("txtArchivo_pdf_firmado")

    '    'strContrasenaClave = Request.Form("txtContrasena")
    '    'strNuevaContrasenaClave = Request.Form("txtNuevaContrasena")
    '    'strArchivoPDF = Request.Form("txtArchivo_pdf") -- ver porque se repite strArchivoPDF = strArchivoFirmado ?

    '    'strIdUsuario as string = Session.Item("IdUsuario")
    '    'strIdTipoEntidad as string = Session.Item("IdTipoEntidad")
    '    'strNombreEntidad as string = Session.Item("NombreEntidad")
    '    'strNombrePDF as string = Session.Item("NombrePDF")

    '    'strUsuarioLogin = Session.Item("UsuarioLogin")


    '    'Dim strVar As String
    '    'Dim strPagina As String
    '    'strVar = Request.QueryString("var")
    '    'strPagina = Request.QueryString("Pagina")
    '    Dim strEntidadFinanciera As String
    '    'Dim strArchivoFirmado As String
    '    Dim strContrasena As String
    '    'Dim strContrasenaClave As String
    '    Dim strNuevaContrasena As String
    '    'Dim strNuevaContrasenaClave As String
    '    Dim contenido As Byte()
    '    '--------------------------------------------
    '    ' Dim SignedData As CAPICOM.SignedData
    '    '--------------------------------------------
    '    Dim objConexion As SqlConnection
    '    Dim objTrans As SqlTransaction
    '    objConexion = New SqlConnection(strConn)
    '    objConexion.Open()
    '    objTrans = objConexion.BeginTransaction()
    '    Try
    '        Dim strMSG As String
    '        If strCertDigital = True Then
    '            '-----------COMENTAR TODO ESTE BLOQUE SI SE QUIERE LA VERSION SIN FIRMAS----------------------
    '            'strArchivoFirmado = Request.Form("txtArchivo_pdf_firmado")
    '            If Len(strArchivoFirmado) = 0 Then
    '                'Session.Add("Mensaje", "Fallo al firmar el documento, verifique la configuración de su PC")
    ' strSessResultado = "X" : strSessMensaje = "Fallo al firmar el documento, verifique la configuración de su PC"

    '                If strPagina = "1" Then
    '                    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '                    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '                Else
    '                    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '                    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '                End If

    '                Return strSessMensaje

    '            End If
    '            contenido = Convert.FromBase64String(strArchivoFirmado)
    '            '-----------------------------------------------------------------------------------------------------------
    '        Else
    '            'strContrasenaClave = Request.Form("txtContrasena")
    '            'strNuevaContrasenaClave = Request.Form("txtNuevaContrasena")
    '            'strArchivoFirmado = Request.Form("txtArchivo_pdf")
    '            contenido = Convert.FromBase64String(strArchivoFirmado)
    '            strContrasena = Funciones.EncryptString(strContrasenaClave.Trim, "©¹ã_[]0")
    '            strNuevaContrasena = Funciones.EncryptString(strNuevaContrasenaClave.Trim, "©¹ã_[]0")
    '            Dim strIp As String()
    '            Dim strIp1 As String
    '            Dim strIp2 As String
    '            Dim strIp3 As String
    '            Dim strIp4 As String

    '            strIp = pr_OBTI_IP_PETI().Split(".")

    '            If strIp.Length < 3 Then
    '                strIp1 = "111111111111"
    '            Else
    '                strIp1 = Right("000" + strIp(0), 3)
    '                strIp2 = Right("000" + strIp(1), 3)
    '                strIp3 = Right("000" + strIp(2), 3)
    '                strIp4 = Right("000" + strIp(3), 3)
    '                strIp1 = strIp1 & strIp2 & strIp3 & strIp4
    '            End If

    '            strMSG = objDocumento.gGetValidaContrasena(strIdUsuario, strContrasena, strNuevaContrasena, strIp1, objTrans)
    '            Select Case strMSG
    '                Case "I"
    '                    If strNuevaContrasenaClave <> "" Then
    '                        'Session.Add("strContrasenia", strNuevaContrasenaClave)
    '                        strSessContraseña = strNuevaContrasenaClave
    '                    Else
    '                        'Session.Add("strContrasenia", strContrasenaClave)
    '                        strSessContraseña = strContrasenaClave
    '                    End If
    '                    'Session.Add("strfecha", Now)
    '                    strSessFecha = Now
    '                    Exit Select
    '                Case "C"
    '                    'Session.Add("Mensaje", "La contraseña anterior no es correcta")
    '                   strSessResultado = "C" :  strSessMensaje = "La contraseña anterior no es correcta"
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                    If strPagina = "1" Then
    '                        'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '                    Else
    '                        'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '                    End If
    '                    'Exit Sub
    '                    Return strSessMensaje
    '                Case "X"
    '                    'Session.Add("Mensaje", "La contraseña ingresada no es correcta")
    '                   strSessResultado = "X" :  strSessMensaje = "La contraseña ingresada no es correcta"
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                    If strPagina = "1" Then
    '                        'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '                    Else
    '                        'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '                    End If
    '                    'Exit Sub
    '                    Return strSessMensaje
    '                Case "P"
    '                    'Session.Add("Mensaje", "No puede firmar desde la maquina, no es la IP correcta")
    '                     strSessResultado = "X" : strSessMensaje = "No puede firmar desde la maquina, no es la IP correcta"
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                    If strPagina = "1" Then
    '                        'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '                    Else
    '                        'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '                        PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '                    End If
    '                    'Exit Sub
    '                    Return strSessMensaje
    '            End Select

    '            '----------------------------------------------------------------------------------------------------------

    '        End If
    '        'strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strCod_Doc, strCod_TipOtro, strVar, strIdSico, contenido, objTrans)
    '        strMSG = objFunciones.gFirmarDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strVar, strIdSico, contenido, objTrans)
    '        If strMSG = "X" Then
    '            'Session.Add("Mensaje", "Error al firmar documento")
    '           strSessResultado = "X" :  strSessMensaje = "Error al firmar documento"
    '            objTrans.Rollback()
    '            objTrans.Dispose()
    '            If strPagina = "1" Then
    '                'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '                PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '            Else
    '                'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '                PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '            End If
    '            'Exit Sub
    '            Return strSessMensaje
    '        End If
    '        'Session.Add("Mensaje", strMSG)
    '        strSessMensaje = strMSG
    '        Dim strEstado As String = objDocumento.gContabilizarLiberacion(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strIdUsuario, objTrans)
    '        objTrans.Commit()
    '        objTrans.Dispose()
    '        If strEstado = "04" Then
    '            Dim dtProrroga As DataTable
    '            Dim NombreReporte As String
    '            Dim strIds As String
    '            Dim dtDetalle As DataTable
    '            If strIdTipoDocumento = "02" Then
    '                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then


    '                    Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
    '                    objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
    '                    objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
    '                End If
    '            End If
    '        End If
    '        If strPagina = "1" Then
    '            'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
    '            PagRedirect = "DocumentoPDF.aspx?var=" & strVar
    '        Else
    '            'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
    '            PagRedirect = "DocumentoResumen.aspx?var=" & strVar
    '        End If

    '        'Registrar Docu,ento 07/12/2018
    '        Dim dtSecu As DataTable
    '        dtSecu = objDocumento.gBuscarSecuencia(strIdTipoDocumento, strNroDoc, strIdUsuario)
    '        If dtSecu.Rows.Count <> 0 Then
    '            If strIdTipoEntidad = strFin And dtSecu.Rows(0)("NRO_SECU") = dtSecu.Rows(0)("NRO_SECU_MAX") Then
    '                RegistrarDocumento(strIdUsuario, strIdTipoEntidad, strIdSico, strNombreEntidad, strNombrePDF, strUsuarioLogin)
    '            End If
    '        End If


    '        'Return strSessMensaje

    '    Catch ex As Exception
    '        objTrans.Rollback()
    '        objTrans.Dispose()
    '        'Session.Add("Mensaje", ex.Message)
    '        strSessResultado = "X" : strSessMensaje = ex.Message.ToString
    '        Response.Write(ex.Message)
    '    Finally
    '        objConexion = Nothing
    '    End Try
    'End Function

    Public Function InicializarObtenerDatos(ByVal strNroWarrant As String, ByVal strNroLiberacion As String, ByVal strPeriodoAnual As String,
        ByVal strIdTipoDocumento As String, ByVal strIdSico As String, ByVal strContrasenaClave As String, ByVal strNuevaContrasenaClave As String,
         ByVal strIdUsuario As String, ByVal strCodEmpr As String, ByVal strUsuarioLogin As String, strIdTipoEntidad As String, ByVal strTipoAccion As String) As String

        Dim dt As DataTable
        Dim strNroDoc As String, strNroLib As String, strCod_Doc As String, strCod_TipOtro As String, strCertDigital As Boolean,
            strVar As String, strPagina As String, strArchivoFirmado As String,
            strNombreEntidad As String, strNombrePDF As String, strCodUnidad As String, strArchivoPDF As String

        strNroDoc = "" : strNroLib = "" : strCod_Doc = "" : strCod_TipOtro = "" : strVar = "" : strPagina = ""
        strArchivoFirmado = "" : strNombreEntidad = "" : strNombrePDF = "" : strCodUnidad = "" : strArchivoPDF = ""

        dt = objDocumento.gGetDataDocumentoMobil(strNroWarrant, strNroLiberacion, strPeriodoAnual, strIdTipoDocumento, strCodEmpr)

        For Each dr1 As DataRow In dt.Rows
            strNroDoc = dt.Rows(0).Item("NRO_DOCU")
            strNroLib = dt.Rows(0).Item("NRO_LIBE")
            strIdTipoDocumento = dt.Rows(0).Item("COD_TIPDOC")
            strPeriodoAnual = dt.Rows(0).Item("PRD_DOCU")
            strCod_Doc = dt.Rows(0).Item("COD_DOC")
            strCod_TipOtro = dt.Rows(0).Item("COD_TIPOTRO")
            'strVar = dt.Rows(0).Item("COD_ESTADO")
            'strPagina = dt.Rows(0).Item("DES_PAGINA")
            ''----------------------------
            'oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
            Dim oHashedData As String
            '------------------------------

            If strTipoAccion <> "DES" Then
                If Not IsDBNull(dt.Rows(0).Item("DOC_PDFFIRM")) Then
                    strArchivoFirmado = Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFFIRM"))
                Else
                    If Not IsDBNull(dt.Rows(0).Item("DOC_PDFORIG")) Then
                        'oHashedData.Hash(Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFORIG")))
                        oHashedData = Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFORIG"))
                    Else
                        strSessResultado = "X" : strSessMensaje = "El documento PDF no está creado"
                        Return strSessMensaje
                    End If
                    strArchivoFirmado = oHashedData
                End If
            End If

            strArchivoPDF = strArchivoFirmado
            'strIdTipoEntidad = dt.Rows(0).Item("COD_ENTI")
            strNombreEntidad = dt.Rows(0).Item("NOM_ENTI")
            strNombrePDF = dt.Rows(0).Item("NOM_PDF")
            strCodUnidad = dt.Rows(0).Item("CO_UNID")
        Next

        If strTipoAccion = "APR" Then
            strVar = "1"
            strPagina = "1"
        ElseIf strTipoAccion = "DES" Then

            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, (CInt(strIdTipoEntidad)).ToString, strIdSico, strNombreEntidad, "C", "1", "RECHAZA DOCUMENTO - MOVIL", "", "")

            strSessMensaje = Fn_RechazarDocumento(strIdUsuario, strIdTipoEntidad, strNroDoc, strNroLib, strPeriodoAnual, strUsuarioLogin, strNombreEntidad, strIdSico, strIdTipoDocumento)
            Return strSessMensaje
        End If

        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, (CInt(strIdTipoEntidad)).ToString, strIdSico, strNombreEntidad, "C", "1", "FIRMA DOCUMENTO - MOVIL", "", "")

        Return InicializaFirmaMobil(strNroDoc, strNroLib, strIdTipoDocumento, strPeriodoAnual, strCod_Doc, strCod_TipOtro, strIdSico, strCertDigital, strVar, strPagina, strArchivoFirmado, strContrasenaClave, strNuevaContrasenaClave, strArchivoPDF, strIdUsuario, strUsuarioLogin, strIdTipoEntidad, strNombreEntidad, strNombrePDF, strCodUnidad)

    End Function

    Public Function InicializaFirmaMobil(strNroDoc As String, strNroLib As String, strIdTipoDocumento As String, strPeriodoAnual As String,
                          strCod_Doc As String, strCod_TipOtro As String, strIdSico As String,
                          strCertDigital As Boolean, strVar As String, strPagina As String, strArchivoFirmado As String,
                         strContrasenaClave As String, strNuevaContrasenaClave As String, strArchivoPDF As String,
                          strIdUsuario As String, strUsuarioLogin As String, strIdTipoEntidad As String, strNombreEntidad As String, strNombrePDF As String, strCodUnidad As String) As String

        Dim strEntidadFinanciera As String
        Dim strContrasena As String
        Dim strNuevaContrasena As String
        Dim contenido As Byte()

        Dim dtAcceso As DataTable
        Dim strIdTipoUsuario As String = ""
        dtAcceso = objAccesoWeb.gCNMostrarDatosUsuario(strIdUsuario, strIdSico)
        For Each dr0 As DataRow In dtAcceso.Rows
            strIdTipoUsuario = dtAcceso.Rows(0).Item("COD_TIPUSR")
        Next



        '--------------------------------------------
        ' Dim SignedData As CAPICOM.SignedData
        '--------------------------------------------
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strMSG As String
            If strCertDigital = True Then
                '-----------COMENTAR TODO ESTE BLOQUE SI SE QUIERE LA VERSION SIN FIRMAS----------------------
                'strArchivoFirmado = Request.Form("txtArchivo_pdf_firmado")
                If Len(strArchivoFirmado) = 0 Then
                    'Session.Add("Mensaje", "Fallo al firmar el documento, verifique la configuración de su PC")
                    strSessResultado = "X" : strSessMensaje = "Fallo al firmar el documento, verifique la configuración de su PC"
                    'If strPagina = "1" Then
                    '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                    '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                    'Else
                    '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                    '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                    'End If
                    Return strSessMensaje
                End If
                contenido = Convert.FromBase64String(strArchivoFirmado)
                '-----------------------------------------------------------------------------------------------------------
            Else
                'strContrasenaClave = Request.Form("txtContrasena")
                'strNuevaContrasenaClave = Request.Form("txtNuevaContrasena")
                'strArchivoFirmado = Request.Form("txtArchivo_pdf")
                contenido = Convert.FromBase64String(strArchivoFirmado)
                'strContrasena = Funciones.EncryptString(strContrasenaClave.Trim, "©¹ã_[]0")
                'strNuevaContrasena = Funciones.EncryptString(strNuevaContrasenaClave.Trim, "©¹ã_[]0")

                strContrasena = strContrasenaClave
                strNuevaContrasena = strNuevaContrasenaClave

                Dim strIp As String()
                Dim strIp1 As String
                Dim strIp2 As String
                Dim strIp3 As String
                Dim strIp4 As String

                strIp1 = ""

                'If pr_OBTI_IP_PETI() <> "" Then

                '    strIp = pr_OBTI_IP_PETI().Split(".")

                '    If strIp.Length < 3 Then
                '        strIp1 = "111111111111"
                '    Else
                '        strIp1 = Right("000" + strIp(0), 3)
                '        strIp2 = Right("000" + strIp(1), 3)
                '        strIp3 = Right("000" + strIp(2), 3)
                '        strIp4 = Right("000" + strIp(3), 3)
                '        strIp1 = strIp1 & strIp2 & strIp3 & strIp4
                '    End If
                'End If
                Dim dt As DataTable
                dt = objDocumento.gGetValidaContrasenaMobil(strIdUsuario, "", strContrasena, strNuevaContrasena, strIp1, "F", objTrans) 'ok(sin sessiones internas)
                strMSG = dt.Rows(0).Item("RESULTADO").ToString()
                Select Case strMSG
                    Case "I"
                        If strNuevaContrasenaClave <> "" Then
                            'Session.Add("strContrasenia", strNuevaContrasenaClave)
                            strSessContraseña = strNuevaContrasenaClave
                        Else
                            'Session.Add("strContrasenia", strContrasenaClave)
                            strSessContraseña = strContrasenaClave
                        End If
                        'Session.Add("strfecha", Now)
                        strSessFecha = Now
                        Exit Select
                    Case "C"
                        'Session.Add("Mensaje", "La contraseña anterior no es correcta")
                        strSessResultado = "C" : strSessMensaje = "La contraseña anterior no es correcta"
                        objTrans.Rollback()
                        objTrans.Dispose()

                        'If strPagina = "1" Then
                        '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                        'Else
                        '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                        'End If
                        ''Exit Sub

                        Return strSessMensaje
                    Case "X"
                        'Session.Add("Mensaje", "La contraseña ingresada no es correcta")
                        strSessResultado = "X" : strSessMensaje = "La contraseña ingresada no es correcta"
                        objTrans.Rollback()
                        objTrans.Dispose()
                        'If strPagina = "1" Then
                        '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                        'Else
                        '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                        'End If
                        'Exit Sub
                        Return strSessMensaje
                        'Case "P"
                        '    'Session.Add("Mensaje", "No puede firmar desde la maquina, no es la IP correcta")
                        '   strSessResultado = "X" :  strSessMensaje = "No puede firmar desde la maquina, no es la IP correcta"
                        '    objTrans.Rollback()
                        '    objTrans.Dispose()
                        '    'If strPagina = "1" Then
                        '    '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        '    '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                        '    'Else
                        '    '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        '    '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                        '    'End If
                        '    'Exit Sub
                        '    Return strSessMensaje
                        'Case "P"
                        '    'Session.Add("Mensaje", "No puede firmar desde la maquina, no es la IP correcta")
                        '   strSessResultado = "X" :  strSessMensaje = "No puede firmar desde la maquina, no es la IP correcta"
                        '    objTrans.Rollback()
                        '    objTrans.Dispose()
                        '    'If strPagina = "1" Then
                        '    '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        '    '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                        '    'Else
                        '    '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        '    '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                        '    'End If
                        '    'Exit Sub
                        '    Return strSessMensaje
                End Select

                '----------------------------------------------------------------------------------------------------------

            End If
            ' strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strCod_Doc, strCod_TipOtro, strVar, strIdSico, contenido, objTrans)
            'strMSG = objFunciones.gFirmarDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strVar, strIdSico, contenido, objTrans)
            strMSG = objFunciones.gFirmarDocumento(strIdUsuario, strUsuarioLogin, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strCertDigital, strCod_Doc, strCod_TipOtro, strIdSico, strIdTipoEntidad, strCodUnidad, contenido, strVar, objTrans)
            If strMSG = "X" Then
                'Session.Add("Mensaje", "Error al firmar documento")
                strSessResultado = "X" : strSessMensaje = "Error al firmar documento"
                objTrans.Rollback()
                objTrans.Dispose()
                'If strPagina = "1" Then
                '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
                'Else
                '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
                'End If
                'Exit Sub
                Return strSessMensaje
            Else
                strSessResultado = objFunciones.FnSessResultado : strSessMensaje = objFunciones.FnSessMensaje
            End If
            'Session.Add("Mensaje", strMSG)
            Dim strEstado As String = objDocumento.gContabilizarLiberacion(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strIdUsuario, objTrans) 'ok(sin sessiones internas)
            strSessMensaje = strMSG
            objTrans.Commit()
            objTrans.Dispose()

            '=== Copiar los documentos al Filemaster , verificar en que estado debe copiar !!! ===
            If strEstado = "04" Then
                Dim dtProrroga As DataTable
                Dim NombreReporte As String
                Dim strIds As String
                Dim dtDetalle As DataTable
                If strIdTipoDocumento = "02" Then
                    If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then  'ok(sin sessiones internas)

                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)  'ok(sin sessiones internas)
                    End If
                End If
            End If
            'If strPagina = "1" Then
            '    'Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
            '    PagRedirect = "DocumentoPDF.aspx?var=" & strVar
            'Else
            '    'Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
            '    PagRedirect = "DocumentoResumen.aspx?var=" & strVar
            'End If

            'Registrar Docucumento 07/12/2018
            Dim dtSecu As DataTable
                dtSecu = objDocumento.gBuscarSecuencia(strIdTipoDocumento, strNroDoc, strNroLib, strIdUsuario) 'ok(sin sessiones internas)
                If dtSecu.Rows.Count <> 0 Then
                    If strIdTipoEntidad = strFin And dtSecu.Rows(0)("NRO_SECU") = dtSecu.Rows(0)("NRO_SECU_MAX") Then

                    RegistrarDocumento(strIdTipoUsuario, strIdUsuario, strIdTipoEntidad, strIdSico, strNombreEntidad, strNombrePDF, strUsuarioLogin, strIdTipoDocumento, strNroDoc, strNroLib, strCod_TipOtro, strPeriodoAnual, strCod_Doc) 'ok(sin sessiones internas)
                End If
                End If


            ''Registrar Documento segun configuracion, si es la ultima aprobacion
            'Dim dtSecu As DataTable
            'dtSecu = objDocumento.gBuscarSecuencia(strIdTipoDocumento, strNroDoc, strNroLib, Session.Item("IdUsuario"))
            'If dtSecu.Rows.Count <> 0 Then
            '    'Valida Maxima Aprobación
            '    If Session.Item("IdTipoEntidad") = strFin And dtSecu.Rows(0)("NRO_SECU") = dtSecu.Rows(0)("NRO_SECU_MAX") Then
            '        RegistrarDocumento(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), strNroDoc, strNroLib, strPeriodoAnual, Session.Item("IdUsuario"), strIdTipoDocumento, strIdSico)
            '    End If
            'End If




            Return strSessMensaje

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            'Session.Add("Mensaje", ex.Message)
            strSessMensaje = ex.Message.ToString
            Response.Write(ex.Message)
        Finally
            objConexion = Nothing
        End Try
    End Function

    Private Sub RegistrarDocumento(strIdTipoUsuario As String, strIdUsuario As String, strIdTipoEntidad As String, strIdSico As String, strNombreEntidad As String, strNombrePDF As String, strUsuarioLogin As String, strIdTipoDocumento As String, strNroDoc As String, strNroLib As String, strCod_TipOtro As String, strPeriodoAnual As String, strCod_Doc As String)


        '=== VALIDAMOS CONFIGURACION PARA REGISTRAR === str
        'Dim strValidaRegistrar As Boolean = False
        'Dim dtControles As New DataTable
        'dtControles = objDocumento.gGetAccesoControles(strIdTipoEntidad, strIdTipoUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdUsuario, strIdTipoDocumento, strIdSico, 0)
        'For i As Integer = 0 To dtControles.Rows.Count - 1
        '    Select Case dtControles.Rows(i)("COD_CTRL")
        '        '==== REGISTRAR ===
        '        Case "08"
        '            strValidaRegistrar = True
        '    End Select
        'Next

        'If strValidaRegistrar = True Then

        objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, CStr(strIdTipoEntidad).Substring(1), strIdSico, strNombreEntidad, "C", "1", "REGISTRAR DOCUMENTO - MOVIL", "", "")

            Dim drEndoso As DataRow
            Dim drSICO As DataRow
            Dim NombreReporte As String
            Dim strIds As String
            Dim dtProrroga As DataTable
            Dim objConexion As SqlConnection
            Dim objTrans As SqlTransaction
            objConexion = New SqlConnection(strConn)
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()
            Try

                If strIdTipoDocumento = "05" Then
                    dtProrroga = objDocumento.gGetDataProrroga(strNroDoc, strNroLib, strCod_TipOtro) 'ok(sin sessiones internas)
                    If dtProrroga.Rows.Count = 0 Then
                        objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "No se encontró la Prorroga"
                        Exit Sub
                    Else
                        NombreReporte = strEndosoPath & strNombrePDF
                        drSICO = objEntidad.gUpdProrrogaSICO(strEmpresa, dtProrroga.Rows(0)("COD_TIPDOC"), dtProrroga.Rows(0)("NRO_DOCU"),
                        dtProrroga.Rows(0)("MON_ADEC"), dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("SAL_ACTU"),
                        dtProrroga.Rows(0)("FCH_OBLIGACION"), strUsuarioLogin) 'ok(sin sessiones internas)
                        If drSICO.IsNull("UL_NUME_FOLI") Then
                            objTrans.Rollback()
                            objTrans.Dispose()
                            'Session.Add("Mensaje", "No se pudo actualizar en SICO")
                            strSessResultado = "X" : strSessMensaje = "No se pudo actualizar en SICO"
                            Exit Sub
                        End If

                        objDocumento.gUpdFolioProrroga(drSICO("UL_NUME_FOLI"), drSICO("FE_NUME_FOLI"), drSICO("NU_SECU"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)
                        objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado la Prorroga<br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)
                        objTrans.Commit()
                        objTrans.Dispose()
                    End If
                    '   Warrants de Custodia
                ElseIf strIdTipoDocumento = "20" Then
                    objDocumento.gUpdRegistrar_Custodia_SICO(strNroDoc, strIdTipoDocumento, strNroLib, strPeriodoAnual, objTrans) 'ok(sin sessiones internas)
                    objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)

                    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado Warrants Custodia <br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)

                    objTrans.Commit()
                    objTrans.Dispose()
                    'Session.Add("Mensaje", "Se registró correctamente")
                    strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
                ElseIf strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "10" Then
                    objDocumento.gRegistraEndosoSICO(strCod_Doc, strIdUsuario, objTrans) 'ok(sin sessiones internas)
                    If objDocumento.strResultado = "0" Then
                        objDocumento.gRegistraDobleEndosoSICO(strCod_Doc, objTrans) 'ok(sin sessiones internas)
                        If objDocumento.strResultado <> "0" Then
                            objTrans.Rollback()
                            objTrans.Dispose()
                            Exit Try
                        End If
                    'dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans) 'ok(sin sessiones internas)
                    'For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    '    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                    '        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans) 'ok(sin sessiones internas)
                    '    Else
                    '        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    '    End If
                    '    'strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    'Next
                    'If strIds = "" Then
                    '    objTrans.Rollback()
                    '    objTrans.Dispose()
                    '    'Session.Add("Mensaje", "No se encontró el warrant relacionado a este documento")
                    '    strSessResultado = "X" : strSessMensaje = "No se encontró el warrant relacionado a este documento"
                    '    Exit Sub
                    'End If
                    objReportes.GeneraPDFWarrantCopia(strCod_Doc, strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)

                    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado el Documento<br><br>", strEndosoPath, "S", "N", objTrans) 'ok(sin sessiones internas)
                    objTrans.Commit()
                    objTrans.Dispose()
                    strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
                    ElseIf objDocumento.strResultado = "3" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "El documento ya fue registrado"
                        Exit Sub
                    ElseIf objDocumento.strResultado = "4" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "Debe de realizar el doble endoso en el SICO antes de registrarlo"
                        Exit Sub
                    Else
                        objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "Fallo al registrar"
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                objTrans.Rollback()
                objTrans.Dispose()
                'Session.Add("Mensaje", ex.Message.ToString)
                strSessResultado = "X" : strSessMensaje = ex.Message.ToString
            Finally
            End Try
        'End If
    End Sub


    Public Function Fn_RechazarDocumento(strIdUsuario As String, strIdTipoEntidad As String, strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strUsuarioLogin As String, strNombreEntidad As String, strIdSico As String, strIdTipoDocumento As String) As String



        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, CStr(strIdTipoEntidad).Substring(1), strIdSico, strNombreEntidad, "C", "1", "RECHAZAR DOCUMENTO", "", "")

        Dim strResultado As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            strResultado = objDocumento.gUpdRechazarDocumento(strIdTipoEntidad, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, objTrans)
            If strResultado = "X" Then
                objTrans.Rollback()
                objTrans.Dispose()
                strSessResultado = "X" : strSessMensaje = "No puede rechazar el documento, está relacionado"
                Return strSessMensaje
            End If

            If strIdTipoDocumento = "24" Then
                objFunciones.SendMail("Depsa – Sistema Infodepsa <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                System.Configuration.ConfigurationManager.AppSettings.Item("EmailInspectoria"),
                "Solicitud de Warrant fue rechazado - " & strNombreEntidad,
                "Se rechazó la solicitud de warrants Nro: <STRONG><FONT color='#330099'> " & strNroDoc & "<br> </FONT></STRONG>" &
                "Cliente: <STRONG><FONT color='#330099'> " & strNombreEntidad & "<br> </FONT></STRONG>" &
                "Rechazado por: <STRONG><FONT color='#330099'> " & UCase(strUsuarioLogin) & "</FONT></STRONG><br>" &
                "<br><br>Atte.<br><br>Sírvanse ingresar al Infodepsa haciendo click sobre el siguiente vinculo: <A href='https://www.depsa.com.pe/Infodepsa/Login.aspx'>Depsa - Sistema Infodepsa</A>", "")
            Else
                objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento,
                "El usuario " & strUsuarioLogin & " ha rechazado el documento", "", "S", "S", objTrans)
            End If
            objTrans.Commit()
            objTrans.Dispose()
            'Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
            strSessResultado = "I" : strSessMensaje = "Se Rechazó con éxito"
            Return strSessMensaje

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            strSessResultado = "X" : strSessMensaje = "Error: " & ex.Message.ToString
            Return strSessMensaje
        End Try
    End Function


    Public Function GeneraPDF(strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strIdTipoDocumento As String, strEmpresa As String, strIdUsuario As String) As String
        Try
            '--------------------------------------------------------------------

            Dim dt As DataTable, strCod_TipOtro As String, strPagina As String
            strCod_TipOtro = "" : strPagina = ""
            dt = objDocumento.gGetDataDocumentoMobil(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)

            For Each dr1 As DataRow In dt.Rows
                strCod_TipOtro = dt.Rows(0).Item("COD_TIPOTRO")
                strPagina = dt.Rows(0).Item("DES_PAGINA")
            Next

            '--------------------------------------------------------------------------
            Dim drUsuario As DataRow
            Dim strTipoDocumento As String
            Dim blnEndoso As Boolean
            drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)
            If strIdTipoDocumento = "20" Then
                strTipoDocumento = strCod_TipOtro
                blnEndoso = True
            Else
                strTipoDocumento = strIdTipoDocumento
                blnEndoso = False
            End If
            'If strPagina = "2" Then
            If strPagina = "1" Then
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then

                    If strIdTipoDocumento <> "02" And strIdTipoDocumento <> "05" And strIdTipoDocumento <> "13" And strIdTipoDocumento <> "14" And strIdTipoDocumento <> "21" Then

                        objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, strIdUsuario, "0", strNroDoc)
                    End If

                    If objReportes.strMensaje <> "0" Then
                        'Session.Add("Mensaje", objReportes.strMensaje)
                        strSessMensaje = objReportes.strMensaje
                    Else
                        strSessMensaje = "I"
                    End If
                Else
                    strSessMensaje = "El PDF se creó anteriormente"
                End If
            End If

            If strPagina = "1" Then
                If strIdTipoDocumento = "20" Or strIdTipoDocumento = "12" Then
                    If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                        objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, strIdUsuario, "0", "")
                        If objReportes.strMensaje <> "0" Then
                            'Session.Add("Mensaje", objReportes.strMensaje)
                            strSessMensaje = objReportes.strMensaje
                        Else
                            strSessMensaje = "I"
                        End If

                    End If
                    Return strSessMensaje
                End If
                drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim, strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento)
                    strSessMensaje = "I"
                End If
            End If
            drUsuario = Nothing

            Return strSessMensaje
        Catch ex As Exception
            strSessMensaje = "ERROR " + ex.Message
            Return strSessMensaje
        End Try

        Return ""
    End Function

    Private Sub GrabaPDF(ByVal strNombArch As String, strIdUsuario As String, strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strIdTipoDocumento As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, Contenido, strIdTipoDocumento)
        fs.Close()
    End Sub



    Public Function Fn_ResetearClaveFirma(strIdUsuario As String, strUsuarioLogin As String, strEmail As String, strTipoEnvio As String) As String
        Dim dtResult As DataTable
        Dim strContraseña As String
        Dim strEncriptado As String
        Dim strDecIdUsuario As String
        Dim strDecUsuarioLogin As String
        Dim strDecEmail As String

        If strTipoEnvio = "A" Then
            strDecIdUsuario = ""
            strEmail = ""
        Else
            strDecIdUsuario = Funciones.DecryptString(strIdUsuario, "©¹ã_[]0")
            strDecEmail = Funciones.DecryptString(strEmail, "©¹ã_[]0")
        End If

        strDecUsuarioLogin = Funciones.DecryptString(strUsuarioLogin, "©¹ã_[]0")
        Dim Generator As System.Random = New System.Random()
        strContraseña = (Generator.Next(100000, 999999)).ToString()
        strEncriptado = Funciones.EncryptString(strContraseña, "©¹ã_[]0")
        dtResult = objUsuario.gUpdClaveFirmaMobil(strEncriptado, strDecIdUsuario, strDecUsuarioLogin, strDecIdUsuario, strTipoEnvio)

        'Select CaseCaseCaseCaseCaseCaseCase@Resultado 'RESULTADO' , isnull(@Mensaje,'') 'MENSAJE' , isnull(@Email,'') 'EMAIL'

        If dtResult.Rows(0).Item("RESULTADO").trim = "I" Then
            strDecEmail = dtResult.Rows(0).Item("EMAIL").TRIM
            EnvioMailReseteo(strDecUsuarioLogin, strContraseña, strDecEmail, strTipoEnvio)
        End If

        Return dtResult.Rows(0).Item("MENSAJE").TRIM

    End Function
    Private Sub EnvioMailReseteo(ByVal strLogin As String, ByVal strContraseña As String, strEmail As String, strTipoEnvio As String)

        'objFunciones.SendMail("Depsa – Sistema Infodepsa <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmail, "Clave para firmar", "Estimado(a) sr(a).  " &
        '                       strLogin.ToLower & " su clave de acceso al App es:  " & strContraseña & "<br><br>El Link de acceso al sistema es: <A href='https://www.depsa.com.pe/Infodepsa/Login.aspx'>Sistema Infodepsa</A>" &
        '                     "<br>Cualquier consulta sobre la aplicación escribir a soporte@depsa.com.pe", "")

        If strTipoEnvio = "A" And strEmail <> "" Then
            objFunciones.SendMail("Depsa – Sistema Infodepsa <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmail, "Clave de acceso", "Estimado(a) sr(a).  " &
                               strLogin.ToLower & " su clave de acceso es:  " & strContraseña &
                             "<br><br>Cualquier consulta sobre la aplicación escribir a soporte@depsa.com.pe", "")

        End If
        If strTipoEnvio = "F" And strEmail <> "" Then
            objFunciones.SendMail("Depsa – Sistema Infodepsa <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmail, "Clave para firmar", "Estimado(a) sr(a).  " &
                               strLogin.ToLower & "  su clave para firmar los documentos es:  " & strContraseña &
                             "<br><br>Cualquier consulta sobre la aplicación escribir a soporte@depsa.com.pe", "")
        End If


    End Sub

    Public Function fn_ActualizarClave(ByVal strNroWarrant As String, ByVal strNroLiberacion As String, ByVal strPeriodoAnual As String,
        ByVal strIdTipoDocumento As String, ByVal strIdSico As String, ByVal strContrasenaClave As String, ByVal strNuevaContrasenaClave As String,
         ByVal strIdUsuario As String, ByVal strCodEmpr As String, ByVal strUsuarioLogin As String, strIdTipoEntidad As String, ByVal strTipoAccion As String) As String

        'Dim strNroDoc As String
        'Dim strCod_TipOtro As String
        'Dim strIdSico As String
        'Dim strNroLib As String
        'Dim strPeriodoAnual As String
        'Dim strCodDoc As String
        'Dim PagRedirect As String

        Dim dt As DataTable
        Dim strNroDoc As String, strNroLib As String, strCod_Doc As String, strCod_TipOtro As String, strCertDigital As Boolean,
            strVar As String, strPagina As String, strArchivoFirmado As String,
            strNombreEntidad As String, strNombrePDF As String, strCodUnidad As String, strArchivoPDF As String

        strNroDoc = "" : strNroLib = "" : strCod_Doc = "" : strCod_TipOtro = "" : strVar = "" : strPagina = ""
        strArchivoFirmado = "" : strNombreEntidad = "" : strNombrePDF = "" : strCodUnidad = "" : strArchivoPDF = ""

        dt = objDocumento.gGetDataDocumentoMobil(strNroWarrant, strNroLiberacion, strPeriodoAnual, strIdTipoDocumento, strCodEmpr)

        For Each dr1 As DataRow In dt.Rows
            strNroDoc = dt.Rows(0).Item("NRO_DOCU")
            strNroLib = dt.Rows(0).Item("NRO_LIBE")
            strIdTipoDocumento = dt.Rows(0).Item("COD_TIPDOC")
            strPeriodoAnual = dt.Rows(0).Item("PRD_DOCU")
            strCod_Doc = dt.Rows(0).Item("COD_DOC")
            strCod_TipOtro = dt.Rows(0).Item("COD_TIPOTRO")
            'strVar = dt.Rows(0).Item("COD_ESTADO")
            'strPagina = dt.Rows(0).Item("DES_PAGINA")
            ''----------------------------
            'oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
            Dim oHashedData As String
            '------------------------------

            If strTipoAccion <> "DES" Then
                If Not IsDBNull(dt.Rows(0).Item("DOC_PDFFIRM")) Then
                    strArchivoFirmado = Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFFIRM"))
                Else
                    If Not IsDBNull(dt.Rows(0).Item("DOC_PDFORIG")) Then
                        'oHashedData.Hash(Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFORIG")))
                        oHashedData = Convert.ToBase64String(dt.Rows(0).Item("DOC_PDFORIG"))
                    Else
                        strSessResultado = "X" : strSessMensaje = "El documento PDF no está creado"
                        Return strSessMensaje
                    End If
                    strArchivoFirmado = oHashedData
                End If
            End If

            strArchivoPDF = strArchivoFirmado
            'strIdTipoEntidad = dt.Rows(0).Item("COD_ENTI")
            strNombreEntidad = dt.Rows(0).Item("NOM_ENTI")
            strNombrePDF = dt.Rows(0).Item("NOM_PDF")
            strCodUnidad = dt.Rows(0).Item("CO_UNID")
        Next

        If strTipoAccion = "APR" Then
            strVar = "1"
            strPagina = "1"
        ElseIf strTipoAccion = "DES" Then
            strSessMensaje = Fn_RechazarDocumento(strIdUsuario, strIdTipoEntidad, strNroDoc, strNroLib, strPeriodoAnual, strUsuarioLogin, strNombreEntidad, strIdSico, strIdTipoDocumento)
            Return strSessMensaje
        End If

        Return InicializaFirmaMobil(strNroDoc, strNroLib, strIdTipoDocumento, strPeriodoAnual, strCod_Doc, strCod_TipOtro, strIdSico, strCertDigital, strVar, strPagina, strArchivoFirmado, strContrasenaClave, strNuevaContrasenaClave, strArchivoPDF, strIdUsuario, strUsuarioLogin, strIdTipoEntidad, strNombreEntidad, strNombrePDF, strCodUnidad)

    End Function

    'Public Property FnstrResultado() As String
    '    Get
    '        Return sstrResultado
    '    End Get
    '    Set(ByVal value As String)
    '        sstrResultado = value
    '    End Set
    'End Property

    'Public Property FnstrMensaje() As String
    '    Get
    '        Return sstrMensaje
    '    End Get
    '    Set(ByVal value As String)
    '        sstrMensaje = value
    '    End Set
    'End Property

    Public ReadOnly Property FnSessResultado() As String
        Get
            Return strSessResultado
        End Get
    End Property

    Public ReadOnly Property FnSessMensaje() As String
        Get
            Return strSessMensaje
        End Get
    End Property


End Class
