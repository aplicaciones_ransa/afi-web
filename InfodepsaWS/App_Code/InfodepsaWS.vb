﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
'Imports DEPSA.FileMaster.BL.BE
'Imports DEPSA.FileMaster.BL.BC
Imports DEPSA.FileMaster.DL.DataModel
Imports System.Data
Imports System.IO

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class InfodepsaWS
    Inherits System.Web.Services.WebService
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones

    '<WebMethod()>
    'Public Function HelloWorld() As String
    '    Return "Hola a todos"
    'End Function

    <WebMethod()> Public Function InicializarDocumento(ByVal strNroWarrant As String, ByVal strNroLiberacion As String, ByVal strPeriodoAnual As String,
        ByVal strIdTipoDocumento As String, ByVal strIdSico As String, ByVal strContrasenaClave As String, ByVal strNuevaContrasenaClave As String,
         ByVal strIdUsuario As String, ByVal strCodEmpr As String, ByVal strUsuarioLogin As String, strIdTipoEntidad As String, strTipoAccion As String) As String
        Dim strMensaje As String = ""
        Dim strResultado As String = ""
        Try

            Dim objDocumentoWS As DocumentoWS
            objDocumentoWS = New DocumentoWS
            strMensaje = objDocumentoWS.InicializarObtenerDatos(strNroWarrant, strNroLiberacion, strPeriodoAnual, strIdTipoDocumento, strIdSico, strContrasenaClave,
                                                                     strNuevaContrasenaClave, strIdUsuario, strCodEmpr, strUsuarioLogin, strIdTipoEntidad, strTipoAccion)
            strResultado = objDocumentoWS.FnSessResultado
            strMensaje = objDocumentoWS.FnSessMensaje

            '---ini--- prueba enviar datos ingresados
            'Dim strCadena As String = strNroWarrant & " , " & strNroLiberacion & " , " & strPeriodoAnual & " , " & strIdTipoDocumento & " , " & strIdSico & " , " & strContrasenaClave & " , " & strNuevaContrasenaClave & " , " & strIdUsuario & " , " & strCodEmpr & " , " & strUsuarioLogin & " , " & strIdTipoEntidad & " , " & strTipoAccion
            'objFunciones.SendMail("infodepsaWS_prueba(InicializarDocumento)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(InicializarDocumento)", "  strCadena : " & strCadena & "<br> Resultado : " & strMensaje, "")
            '---fin--- prueba enviar datos ingresados

            Return strMensaje
        Catch ex As Exception
            Return strMensaje
        End Try

    End Function

    <WebMethod()> Public Function Resetear(strIdUsuario As String, strUsuarioLogin As String, strEmail As String, strTipoEnvio As String) As String
        Dim strMensaje As String = ""
        Dim strResultado As String = ""
        Try
            Dim objDocumentoWS As DocumentoWS
            objDocumentoWS = New DocumentoWS
            strMensaje = objDocumentoWS.Fn_ResetearClaveFirma(strIdUsuario, strUsuarioLogin, strEmail, strTipoEnvio)

            ''---ini--- prueba enviar datos ingresados
            'Dim strCadena As String = strIdUsuario & " , " & strUsuarioLogin & " , " & strEmail & " , " & strTipoEnvio
            'objFunciones.SendMail("infodepsaWS_prueba(Resetear)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(Resetear)", "  strCadena : " & strCadena & "<br> Resultado : " & strMensaje, "")
            ''---fin--- prueba enviar datos ingresados

            Return strMensaje

        Catch ex As Exception
            Return strMensaje
        End Try
    End Function



    <WebMethod()> Public Function vizualizarPDF(ByVal strNroWarrant As String, ByVal strNroLiberacion As String, ByVal strPeriodoAnual As String,
        ByVal strIdTipoDocumento As String, ByVal strCodEmpr As String, ByVal strIdUsuario As String) As String
        Dim strMensaje As String = ""
        Dim strResultado As String = ""
        Try
            Dim objDocumentoWS As DocumentoWS
            objDocumentoWS = New DocumentoWS

            strMensaje = objDocumentoWS.GeneraPDF(strNroWarrant, strNroLiberacion, strPeriodoAnual, strIdTipoDocumento, strCodEmpr, strIdUsuario)
            '=== Retornando el resultado ===
            strResultado = objDocumentoWS.FnSessResultado
            strMensaje = objDocumentoWS.FnSessMensaje

            ''---ini--- prueba enviar datos ingresados
            'Dim strCadena As String = strNroWarrant & " , " & strNroLiberacion & " , " & strPeriodoAnual & " , " & strIdTipoDocumento & " , " & strCodEmpr & " , " & strIdUsuario
            'objFunciones.SendMail("infodepsaWS_prueba(vizualizarPDF)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(vizualizarPDF)", "  strCadena : " & strCadena & "<br> Resultado : " & strMensaje, "")
            ''---fin--- prueba enviar datos ingresados

            Return strResultado & " - " & strMensaje

        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    Function crearTabla() As DataTable
        Dim dtResultado As New DataTable
        Dim Column0 As New DataColumn("RESULTADO")
        Column0.DataType = GetType(Integer)
        Dim Column1 As New DataColumn("MENSAJE")
        Column1.DataType = GetType(String)
        dtResultado.Columns.Add(Column0)
        dtResultado.Columns.Add(Column1)
        Session.Add("dtResultado", dtResultado)
        Return dtResultado
    End Function


End Class