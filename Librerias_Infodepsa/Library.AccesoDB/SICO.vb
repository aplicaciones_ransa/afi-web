Imports System.Data.SqlClient
Public Class SICO
    Inherits System.ComponentModel.Component
    Private cnCO_BDAT As New SqlConnection
    Private oSE_CREA_SERV As New clsSE_CREA_SERV
    Private strCCOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strCCOfisegu As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringSeguridad")

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region
    Public Function fn_TTTIPO_INF2_Q01(ByVal sCO_EMPR As String) As DataSet
        Dim sSQL As String
        sSQL = "Select t1.TI_INF2 'codigo', Max(t1.DE_TIPO_INF2) 'descripcion' " &
               "From TTTIPO_INF2 t1 INNER JOIN TRINFO_GRUP t2 " &
               "ON t2.CO_EMPR = t2.CO_EMPR " &
               " And SubString(t2.TI_INF1, 1, 2) = SubString(t1.TI_INF1, 1, 2) " &
               "Where t1.CO_EMPR = '" & sCO_EMPR & "' " &
               "And SubString (t1.TI_INF1, 1, 2) = 'OP' " &
               "Group by t1.TI_INF2 " &
               "Order by t1.TI_INF2 "
        Return SqlHelper.ExecuteDataset(strCCOfisegu, CommandType.Text, sSQL)
    End Function

    Public Function fn_TMPARA_OPER_Q01(ByVal sCO_EMPR As String) As String
        Dim sDE_SELE As String
        sDE_SELE = "Select TI_DOCU_RETI " & _
               "From TMPARA_OPER " & _
               "Where CO_EMPR = '" & sCO_EMPR & "' "
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_VALO(sDE_SELE, CommandType.Text).ToString
    End Function

    Public Function fn_TCALMA_MERC_Q01(ByVal sCO_EMPR As String) As DataSet
        Dim sSQL As String
        sSQL = "Select t1.TI_TITU 'codigo', t1.TI_TITU 'descripcion' " &
               "From TCALMA_MERC t1 INNER JOIN TTDOCU_CNTB t2 " &
               "ON t2.TI_DOCU = t1.TI_TITU " &
               "Where t1.CO_EMPR = '" & sCO_EMPR & "' " &
               "And t1.TI_SITU = 'ACT' " &
               "Group By t1.TI_TITU " &
               "Order By t1.TI_TITU "
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.Text, sSQL)
    End Function

    Public Function fn_TCALMA_MERC_Q02() As DataSet
        Dim sSQL As String
        sSQL = "Select t1.TI_DOCU_RECE 'codigo', t1.TI_DOCU_RECE AS 'descripcion' " &
                   "From TCALMA_MERC t1 INNER JOIN TTDOCU_CNTB t2 " &
                   "ON t2.TI_DOCU = t1.TI_DOCU_RECE " &
                   "Where t1.TI_SITU = 'ACT' " &
                   "Group By t1.TI_DOCU_RECE " &
                   "Order By t1.TI_DOCU_RECE "
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.Text, sSQL)
    End Function

    Public Function fn_TTMONE_Q01() As DataSet
        Dim sSQL As String = "Select CO_MONE as codigo,DE_MONE as descripcion " & _
                     "From TTMONE order by descripcion asc "
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.Text, sSQL)
    End Function

    Public Function fn_CONS_MODA_0002(ByVal sCO_EMPR As String) As DataSet
        Dim sDE_SELE As String
        sDE_SELE = "Select CO_MODA_MOVI 'CODIGO', DE_MODA_MOVI 'DESCRIPCION' " & _
            "From TTMODA_MOVI " & _
            "Where CO_EMPR = '" & sCO_EMPR & "' " & _
            "And TI_SITU = 'ACT' and (st_vali_adua='N' OR CO_MODA_MOVI='016') and CO_MODA_MOVI " & _
            "not in (select ISNULL(CO_DATO,'') From TTTABL_APOY Where nu_iden = '012' And " & _
            "TTTABL_APOY.co_dato = TTMODA_MOVI.CO_MODA_MOVI )"
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.Text, sDE_SELE)
    End Function

    Public Function fn_TCDOCU_CLIE_Q07(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                          ByVal sFE_PROC As String, ByVal sCO_ENTI_TRAB As String) As DataSet
        Dim sCO_MONE_NACI, sCO_MONE_DOLA As String
        sCO_MONE_DOLA = fn_TMPARA_COME_Q01(sCO_EMPR)
        sCO_MONE_NACI = fn_TMEMPR_Q01(sCO_EMPR)
        Dim sDE_SELE As String
        sDE_SELE = "Select IsNull(FA_CMPR_OFIC,0) " & _
                    "From TCFACT_CAMB " & _
                    "Where CO_MONE='" & sCO_MONE_DOLA & "' " & _
                    "And FE_CAMB=Convert(datetime,Convert(varchar(10),GETDATE(),103),103)"

        Dim nFA_CAMB_OPER As Decimal = CType(oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_VALO(sDE_SELE, CommandType.Text), Decimal)
        Dim dsTCDOCU_CLIE As DataSet = oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_DAST("SP_TCDOCU_CLIE_Q20", _
                                                sCO_EMPR, sCO_UNID, sCO_MONE_NACI, sCO_MONE_DOLA, CType(sFE_PROC, DateTime), _
                                                nFA_CAMB_OPER, sCO_MONE_DOLA, sCO_ENTI_TRAB, sCO_ENTI_TRAB, "", "", "", _
                                                "S", "N", "P", "P", "")
        dsTCDOCU_CLIE.Tables(0).TableName = "TCDOCU_CLIE"
        Return dsTCDOCU_CLIE
    End Function

    Public Function fn_TMEMPR_Q01(ByVal sCO_EMPR As String) As String
        Dim sDE_SELE As String = "Select CO_MONE_NACI " & _
                         "From TMEMPR " & _
                         "Where  CO_EMPR = '" & sCO_EMPR & "' "
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_VALO(sDE_SELE, CommandType.Text).ToString
    End Function

    Public Function fn_TMPARA_COME_Q01(ByVal sCO_EMPR As String) As String
        Dim sDE_SELE As String = "Select CO_MONE_DOLA " & _
                    "From TMPARA_COME " & _
                     "Where  CO_EMPR = '" & sCO_EMPR & "' "

        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_VALO(sDE_SELE, CommandType.Text).ToString
    End Function

    Public Function fn_TMALMA_Q01(ByVal sCO_EMPR As String, ByVal sCO_ALMA As String) As String
        Dim sDE_SELE As String
        sDE_SELE = "Select ISNULL(ST_BODE,'') From TMALMA Where CO_EMPR = '" & sCO_EMPR & "' And CO_ALMA = '" & sCO_ALMA & "' "
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_VALO(sDE_SELE, CommandType.Text)
    End Function

    Public Function fn_TCALMA_MERC_Q05(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                        Optional ByVal sTI_DOCU_RECE As String = "", Optional ByVal sNU_DOCU_RECE As String = "", _
                                        Optional ByVal sTI_TITU As String = "", Optional ByVal sNU_TITU As String = "") As SqlDataReader
        'Extrae tipo de documento que identifica a retiros
        Dim sTI_DOCU_RETI As String = fn_TMPARA_OPER_Q01(sCO_EMPR)
        Dim sDE_SELE As String
        sDE_SELE = "Select t3.NU_CANT_LINE, t1.CO_ALMA, t1.CO_MODA_MOVI, t1.CO_TARI, t1.NU_SECU_TARI, t1.CO_MONE, t1.ST_ALMA_SUBI, " &
        "Max(t1.DE_MERC_GENE) DE_MERC_GENE, Max(t2.DE_ALMA) DE_ALMA, Max(t3.DE_MODA_MOVI) DE_MODA_MOVI, Max(t4.DE_MONE) DE_MONE " &
        "From TCALMA_MERC t1 INNER JOIN TMALMA t2 " &
        "ON t2.CO_EMPR = t1.CO_EMPR " &
        "And t2.CO_ALMA = t1.CO_ALMA " &
        "INNER JOIN TTMODA_MOVI t3 " &
        "ON t3.CO_EMPR = t1.CO_EMPR " &
        "And t3.CO_MODA_MOVI = t1.CO_MODA_MOVI " &
        "INNER JOIN TTMONE t4 " &
        "ON t4.CO_MONE = t1.CO_MONE " &
        "Where t1.CO_EMPR = '" & sCO_EMPR & "' " &
        "And t1.CO_UNID = '" & sCO_UNID & "' " &
        "And t1.TI_TITU = '" & sTI_TITU & "' " &
        "And t1.NU_TITU = '" & sNU_TITU & "' " &
        "And t1.TI_DOCU_RECE = '" & sTI_DOCU_RECE & "' " &
        "And t1.NU_DOCU_RECE = '" & sNU_DOCU_RECE & "' " &
        "And t1.TI_SITU = 'ACT' " &
        "Group By t3.NU_CANT_LINE, t1.CO_ALMA, t1.CO_MODA_MOVI, t1.CO_TARI, t1.NU_SECU_TARI, t1.CO_MONE, t1.ST_ALMA_SUBI "

        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DREA(sDE_SELE, CommandType.Text)
    End Function

    Public Function fn_TDRETI_MERC_Q04(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                          ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String) As String
        Dim sDE_SELE As String = "Select Max(CO_TIPO_BULT) as CO_TIPO_BULT, Sum(NU_UNID_RETI) as NU_UNID_RETI "
        sDE_SELE = sDE_SELE & " From TDRETI_MERC "
        sDE_SELE = sDE_SELE & " Where CO_EMPR = '" & sCO_EMPR & "' "
        sDE_SELE = sDE_SELE & " And CO_UNID = '" & sCO_UNID & "' "
        sDE_SELE = sDE_SELE & " And TI_DOCU_RETI = '" & sTI_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And NU_DOCU_RETI = '" & sNU_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And TI_SITU = 'ACT' "

        Dim dsTDRETI_MERC As DataSet = oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DAST(sDE_SELE)

        Dim sCO_TIPO_BULT, sNU_UNID_RETI, sCA_MERC_LIBE As String
        If dsTDRETI_MERC.Tables(0).Rows.Count > 0 Then
            sCO_TIPO_BULT = IIf(IsDBNull(dsTDRETI_MERC.Tables(0).Rows(0)("CO_TIPO_BULT")), "", dsTDRETI_MERC.Tables(0).Rows(0)("CO_TIPO_BULT"))
            sNU_UNID_RETI = IIf(IsDBNull(dsTDRETI_MERC.Tables(0).Rows(0)("NU_UNID_RETI")), "", dsTDRETI_MERC.Tables(0).Rows(0)("NU_UNID_RETI"))
            sCA_MERC_LIBE = sCO_TIPO_BULT + " " + sNU_UNID_RETI
        End If

        Return sCA_MERC_LIBE
    End Function

    Public Function fn_TCRETI_MERC_I01(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                     ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String, ByVal sST_ALMA_SUBI As String, _
                     ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String, ByVal sCO_ALMA As String, _
                     ByVal sCO_MONE As String, ByVal sCO_USUA As String, ByVal sNO_CHOF As String, _
                     ByVal sNU_DNIS As String, ByVal sNU_PLAC As String, ByVal sDE_OBSE As String, ByVal sNU_RECE_GRAB As String, _
                     ByVal sCO_USUA_FIR1 As String, ByVal sCO_USUA_FIR2 As String, ByVal sST_EMBA As String) As String
        Dim drTCRETI_MERC As SqlDataReader = oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DREA("USP_TCRETI_MERC_I09", CommandType.StoredProcedure, sCO_EMPR, sCO_UNID, _
                                sTI_DOCU_RECE, sNU_DOCU_RECE, sST_ALMA_SUBI, sTI_DOCU_RETI, sNU_DOCU_RETI, sCO_ALMA, sCO_MONE, sCO_USUA, sNO_CHOF, sNU_DNIS, sNU_PLAC, sDE_OBSE, sNU_RECE_GRAB, sST_EMBA)

        Dim sNU_DOCU_GRAB, sST_DIFE_RETI As String
        If drTCRETI_MERC.Read Then
            If Not IsDBNull(drTCRETI_MERC(0)) Then sNU_DOCU_GRAB = drTCRETI_MERC(0) Else sNU_DOCU_GRAB = ""
            If Not IsDBNull(drTCRETI_MERC(1)) Then sST_DIFE_RETI = drTCRETI_MERC(1) Else sST_DIFE_RETI = ""
        End If

        drTCRETI_MERC.Close()
        drTCRETI_MERC.Close()

        If sST_DIFE_RETI = "S" Then
            Return sNU_DOCU_GRAB
        Else
            Return sNU_DOCU_RETI
        End If
    End Function

    Public Function fn_TCFACT_CAMB_Q01(ByVal sCO_EMPR As String) As String
        Dim drTCFACT_CAMB As SqlDataReader = oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DREA("SP_TCFACT_CAMB_Q01", CommandType.StoredProcedure, sCO_EMPR)
        Dim sTI_CAMB, sFA_CAMB As String
        If drTCFACT_CAMB.Read Then
            If Not IsDBNull(drTCFACT_CAMB(0)) Then sTI_CAMB = drTCFACT_CAMB(0) Else sTI_CAMB = ""
            If Not IsDBNull(drTCFACT_CAMB(1)) Then sFA_CAMB = drTCFACT_CAMB(1) Else sFA_CAMB = ""
        End If
        drTCFACT_CAMB.Close()

        If sTI_CAMB = "N" Then
            Return "Tipo Cambio No est� Parametrizado"
        Else
            If sFA_CAMB = "N" Then
                Return "No se ha Ingresado Factor de Cambio del D�a"
            Else
                Return ""
            End If
        End If
    End Function

    Public Function fn_TCALMA_MERC_Q06(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                           ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String, ByVal sCO_CLIE As String, _
                           ByVal sCO_ALMA As String, ByVal sST_ALMA_SUBI As String, ByVal sCO_TIPO_MERC As String, _
                           ByVal sDE_VAPO As String, ByVal sFE_VENC_INIC As String, ByVal sFE_VENC_FINA As String, _
                           ByVal sDE_PROD As String, ByVal sDE_REFE As String, ByVal sCO_MODA As String, ByVal sCO_MONE As String, _
                           Optional ByVal sCO_PROD_CLIE As String = "") As DataSet
        Dim sDE_SELE As String
        If sST_ALMA_SUBI = "S" Then
            sDE_SELE = "Select Max(t1.CO_ALMA) as CO_ALMA,t2.NU_SECU, NULL as NU_SECU_UBIC, Max(t2.DE_MERC) as DE_MERC, Max(t2.CO_TIPO_MERC) CO_TIPO_MERC, " & _
                "Max(t3.DE_TIPO_MERC) DE_TIPO_MERC, Max(t2.CO_STIP_MERC) CO_STIP_MERC,  " & _
                "Max(t4.DE_STIP_MERC) DE_STIP_MERC, NULL CO_BODE_ACTU, NULL CO_UBIC_ACTU, Max(t2.CO_TIPO_BULT) CO_TIPO_BULT,  " & _
                "Max(t5.DE_TIPO_BULT) DE_TIPO_BULT, Max(t2.NU_UNID_RECI) - Sum(IsNull(t7.NU_UNID_RETI, 0)) CANTIDAD_UNIDAD,  " & _
                "Max(t2.CO_UNME_PESO) CO_UNME_PESO, Max(t6.DE_UNME) DE_UNME,  " & _
                "Max(t2.NU_PESO_RECI) - Sum(IsNull(t7.NU_PESO_RETI, 0)) CANTIDAD_BULTO, Max(t2.TI_UNID_PESO) TI_UNID_PESO,  " & _
                "Max(t2.NU_PESO_UNID) NU_PESO_UNID, Max(t1.CO_UNME_AREA) CO_UNME_AREA, Max(t1.NU_AREA_USAD) NU_AREA_USAD,  " & _
                "Max(t1.CO_UNME_VOLU) CO_UNME_VOLU, Max(t1.NU_VOLU_USAD) NU_VOLU_USAD, Max(t2.NU_PESO_RECI) NU_PESO_RECI,  " & _
                "Max(t2.NU_UNID_RECI) NU_UNID_RECI, Sum(IsNull(t7.NU_UNID_RETI, 0)) NU_UNID_RETI,  " & _
                "Sum(IsNull(t7.NU_PESO_RETI, 0)) NU_PESO_RETI, Max(t2.CO_PROD_CLIE) CO_PROD_CLIE, Max(t2.IM_UNIT) as IM_UNIT , " & _
                "Max(t1.FE_FACT) AS FECHA_FACTURACION,  " & _
                "Max(t1.TI_DOCU_RECE) AS TIPO_DOCUMENTO_RECEPCION,  " & _
                "Max(t1.NU_DOCU_RECE) AS NUMERO_RECEPCION,  " & _
                "Max(t2.FE_VCTO_MERC) AS FECHA_VCTO_MERCADERIA,  " & _
                "Max(t2.DE_REFE_0002) AS REFERENCIA,  " & _
                "Max(t1.FE_REGI_INGR) AS FECHA_INGRESO, Max(t1.CO_MONE) as CO_MONE, " & _
                "(Max(t2.NU_UNID_RECI) - Sum(IsNull(t7.NU_UNID_RETI, 0)))*Max(t2.IM_UNIT) as IM_TOTA " & _
                "From TCALMA_MERC t1 INNER JOIN TDALMA_MERC t2 " & _
             "ON (t2.CO_EMPR = t1.CO_EMPR And t2.CO_UNID = t1.CO_UNID " & _
             "    And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE " & _
             "    And t2.TI_SITU = 'ACT' ) " & _
             "LEFT OUTER JOIN TTTIPO_MERC t3 ON (t3.CO_TIPO_MERC = t2.CO_TIPO_MERC) " & _
             "LEFT OUTER JOIN TTSTIP_MERC t4 ON (t4.CO_TIPO_MERC = t2.CO_TIPO_MERC " & _
             "    And t4.CO_STIP_MERC = t2.CO_STIP_MERC ) " & _
                "LEFT OUTER JOIN TTTIPO_BULT t5 ON (t5.CO_TIPO_BULT = t2.CO_TIPO_BULT ) " & _
             "LEFT OUTER JOIN TTUNID_MEDI t6 ON (t6.CO_UNME = t2.CO_UNME_PESO ) " & _
             "LEFT OUTER JOIN TDRETI_MERC t7 ON (t7.CO_EMPR = t2.CO_EMPR " & _
             "    And t7.CO_UNID = t2.CO_UNID And t7.TI_DOCU_RECE = t2.TI_DOCU_RECE " & _
             "    And t7.NU_DOCU_RECE = t2.NU_DOCU_RECE And t7.NU_SECU_RECE = t2.NU_SECU " & _
            "    And t7.TI_SITU = 'ACT' " & _
            "And Exists (Select CO_EMPR From TCRETI_MERC " & _
            "Where(TCRETI_MERC.CO_EMPR = t7.CO_EMPR) " & _
            "And TCRETI_MERC.CO_UNID = t7.CO_UNID " & _
            "And TCRETI_MERC.TI_DOCU_RETI = t7.TI_DOCU_RETI " & _
            "And TCRETI_MERC.NU_DOCU_RETI = t7.NU_DOCU_RETI " & _
            "And NullIf(TCRETI_MERC.TI_DOCU_AFRE, '') IS NULL " & _
            "And NullIf(TCRETI_MERC.NU_DOCU_AFRE, '') IS NULL " & _
            "And TCRETI_MERC.TI_SITU = 'ACT' )) "

            If sDE_VAPO.Trim <> "" Then
                sDE_SELE = sDE_SELE & ", INNER JOIN TCBALA t10 ON  " & _
                "(t10.NU_DOCU_RECE = t1.NU_DOCU_RECE " & _
                "And t10.TI_DOCU_RECE = t1.TI_DOCU_RECE " & _
                "And t10.CO_ALMA = t1.CO_ALMA " & _
                "And t10.CO_UNID = t1.CO_UNID " & _
                "And t10.CO_EMPR = t1.CO_EMPR  " & _
                "And t10.TI_SITU = 'ACT') "
            End If

            sDE_SELE = sDE_SELE & " Where t1.CO_EMPR = '" & sCO_EMPR & "' " & _
                "And t1.CO_UNID = '" & sCO_UNID & "' " & _
                "and t1.TI_SITU = 'ACT'" & _
                "and t1.ST_CIER_COMP = 'S'" & _
                "and t1.OR_WEBB = 'S' "

            If sTI_DOCU_RECE.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.TI_DOCU_RECE = '" & sTI_DOCU_RECE & "' "
            End If

            If sNU_DOCU_RECE.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.NU_DOCU_RECE = '" & sNU_DOCU_RECE & "' "
            End If

            sDE_SELE = sDE_SELE & " And t1.CO_CLIE_ACTU = '" & sCO_CLIE & "' " & _
                "And ((DataLength(rtrim(ltrim('" & sCO_ALMA & "')))!= 0 And t1.CO_ALMA = '" & sCO_ALMA & "') Or(DataLength(Rtrim(Ltrim('" & sCO_ALMA & "'))) = 0)) "

            If sCO_MODA.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.CO_MODA_MOVI = '" & sCO_MODA & "' "
            End If

            If sCO_MONE.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.CO_MONE = '" & sCO_MONE & "' "
            End If

            sDE_SELE = sDE_SELE & " And ((DataLength(rtrim(ltrim('" & sCO_TIPO_MERC & "')))!= 0 And t1.CO_TIPO_MERC = '" & sCO_TIPO_MERC & "') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sCO_TIPO_MERC & "'))) = 0))	" & _
                "And t1.TI_SITU = 'ACT' " & _
                "And t1.ST_CIER_COMP = 'S' "

            sDE_SELE = sDE_SELE & " And t2.CO_EMPR = t1.CO_EMPR " & _
                "And t2.CO_UNID = t1.CO_UNID " & _
                "And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE " & _
                "And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE "

            If sCO_PROD_CLIE <> "" Then
                sDE_SELE = sDE_SELE + " And ((DataLength(rtrim(ltrim('" & sCO_PROD_CLIE & "')))!= 0 And t2.CO_PROD_CLIE = '" & sCO_PROD_CLIE & "') " & _
                              " Or(DataLength(Rtrim(Ltrim('" & sCO_PROD_CLIE & "'))) = 0)) "
            End If

            If sDE_VAPO.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t10.DE_VAPO Like '%" & sDE_VAPO & "%' "
            End If

            sDE_SELE = sDE_SELE & " And ((DataLength(IsNull('" & sFE_VENC_INIC & "',''))!= 0 And t2.FE_VCTO_MERC >= CONVERT(DATETIME,'" & sFE_VENC_INIC & "',103) " & _
                "And t2.FE_VCTO_MERC <= CONVERT(DATETIME,'" & sFE_VENC_FINA & "',103)) Or(DataLength(IsNull('" & sFE_VENC_INIC & "','')) = 0)) " & _
                "And ((DataLength(rtrim(ltrim('" & sDE_PROD & "')))!= 0 And t2.DE_MERC Like '%" & sDE_PROD & "%') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sDE_PROD & "'))) = 0)) " & _
                "And ((DataLength(rtrim(ltrim('" & sDE_REFE & "')))!= 0 And t2.DE_REFE_0002 Like '%" & sDE_REFE & "%') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sDE_REFE & "'))) = 0)) " & _
                "Group By t2.TI_DOCU_RECE, t2.NU_DOCU_RECE, t2.NU_SECU " & _
                "Having ( ( Max(t2.NU_UNID_RECI) - Sum(IsNull(t7.NU_UNID_RETI, 0)) ) > 0  " & _
                "Or ( Max(t2.NU_PESO_RECI) - Sum(IsNull(t7.NU_PESO_RETI, 0)) ) > 0 )  " & _
                "Order By t2.TI_DOCU_RECE Asc, t2.NU_DOCU_RECE Desc, t2.NU_SECU Asc  "

        ElseIf sST_ALMA_SUBI = "N" Then

            sDE_SELE = "Select Max(t1.CO_ALMA) as CO_ALMA, t3.NU_SECU, t3.NU_SECU_UBIC, Max(t2.DE_MERC) as DE_MERC, Max(t2.CO_TIPO_MERC) CO_TIPO_MERC,  " & _
                "Max(t4.DE_TIPO_MERC) DE_TIPO_MERC, Max(t2.CO_STIP_MERC) CO_STIP_MERC, Max(t5.DE_STIP_MERC) DE_STIP_MERC,  " & _
                "Max(t3.CO_BODE_ACTU) CO_BODE_ACTU, Max(t3.CO_UBIC_ACTU) CO_UBIC_ACTU, Max(t2.CO_TIPO_BULT) CO_TIPO_BULT,  " & _
                "Max(t6.DE_TIPO_BULT) DE_TIPO_BULT, Max(t3.NU_UNID_RECI) - Sum(IsNull(t8.NU_UNID_RETI, 0))  CANTIDAD_UNIDAD,  " & _
                "Max(t2.CO_UNME_PESO) CO_UNME_PESO, Max(t7.DE_UNME) DE_UNME,  " & _
                "Max(t3.NU_PESO_RECI) - Sum(IsNull(t8.NU_PESO_RETI, 0))  CANTIDAD_BULTO, Max(t2.TI_UNID_PESO) TI_UNID_PESO,  " & _
                "Max(t2.NU_PESO_UNID) NU_PESO_UNID, Max(t1.CO_UNME_AREA) CO_UNME_AREA, Max(t1.NU_AREA_USAD) NU_AREA_USAD,  " & _
                "Max(t1.CO_UNME_VOLU) CO_UNME_VOLU, Max(t1.NU_VOLU_USAD)  NU_VOLU_USAD, Max(t3.NU_PESO_RECI) NU_PESO_RECI,  " & _
                "Max(t3.NU_UNID_RECI) NU_UNID_RECI, Sum(IsNull(t8.NU_UNID_RETI, 0)) NU_UNID_RETI,  " & _
                "Sum(IsNull(t8.NU_PESO_RETI, 0)) NU_PESO_RETI, Max(t2.CO_PROD_CLIE) CO_PROD_CLIE, Max(t2.IM_UNIT) as IM_UNIT , " & _
                "Max(t1.FE_FACT) AS FECHA_FACTURACION, " & _
                "Max(t1.TI_DOCU_RECE) AS TIPO_DOCUMENTO_RECEPCION, " & _
                "Max(t1.NU_DOCU_RECE) AS NUMERO_RECEPCION, " & _
                "Max(t2.FE_VCTO_MERC) AS FECHA_VCTO_MERCADERIA, " & _
                "Max(t2.DE_REFE_0002) AS REFERENCIA, " & _
                "Max(t1.FE_REGI_INGR) AS FECHA_INGRESO, Max(t1.CO_MONE) as CO_MONE,  " & _
                "(Max(t2.NU_UNID_RECI) - Sum(IsNull(t8.NU_UNID_RETI, 0)))*Max(t2.IM_UNIT) as IM_TOTA " & _
                "FROM TCALMA_MERC t1 INNER JOIN TDALMA_MERC t2 " & _
                "ON (t2.CO_EMPR = t1.CO_EMPR And t2.CO_UNID = t1.CO_UNID " & _
                "And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE  " & _
                "And t2.TI_SITU = 'ACT' ) " & _
                "INNER JOIN TDALME_UBIC t3 ON (t3.CO_EMPR = t2.CO_EMPR " & _
                "And t3.CO_UNID = t2.CO_UNID And t3.TI_DOCU_RECE = t2.TI_DOCU_RECE " & _
                "And t3.NU_DOCU_RECE = t2.NU_DOCU_RECE And t3.NU_SECU = t2.NU_SECU " & _
                "And t3.TI_SITU = 'ACT' ) " & _
                "LEFT OUTER JOIN TTTIPO_MERC t4 ON (t4.CO_TIPO_MERC = t2.CO_TIPO_MERC ) " & _
                "LEFT OUTER JOIN TTSTIP_MERC t5 ON (t5.CO_TIPO_MERC = t2.CO_TIPO_MERC  " & _
                "And t5.CO_STIP_MERC = t2.CO_STIP_MERC ) " & _
                "LEFT OUTER JOIN TTTIPO_BULT t6 ON (t6.CO_TIPO_BULT = t2.CO_TIPO_BULT ) " & _
                "LEFT OUTER JOIN TTUNID_MEDI t7 ON (t7.CO_UNME = t2.CO_UNME_PESO ) " & _
                "LEFT OUTER JOIN TDRETI_UBIC t8 ON (t8.CO_EMPR = t3.CO_EMPR  " & _
                "And t8.CO_UNID = t3.CO_UNID And t8.TI_DOCU_RECE = t3.TI_DOCU_RECE " & _
                "And t8.NU_DOCU_RECE = t3.NU_DOCU_RECE And t8.NU_SECU_RECE = t3.NU_SECU " & _
                "And t8.NU_SECU_UBIC = t3.NU_SECU_UBIC And t8.TI_SITU = 'ACT' " & _
                "And Exists " & _
                "(Select tB.CO_EMPR From " & _
                "	TCRETI_MERC tA INNER JOIN TDRETI_MERC tB " & _
                "	ON (tA.TI_SITU = 'ACT' And tB.CO_EMPR = tA.CO_EMPR " & _
                "	And tB.CO_UNID = tA.CO_UNID And tB.TI_DOCU_RETI = tA.TI_DOCU_RETI " & _
                "	And tB.NU_DOCU_RETI = tA.NU_DOCU_RETI And tB.TI_SITU = 'ACT') " & _
                "Where tA.CO_EMPR = '" & sCO_EMPR & "' " & _
                "	And tA.CO_UNID = '" & sCO_UNID & "' And IsNull(tA.TI_DOCU_AFRE, '') = ''  " & _
                "	And IsNull(tA.NU_DOCU_AFRE, '') = '' And t8.CO_EMPR = tB.CO_EMPR  " & _
                "	And t8.CO_UNID = tB.CO_UNID And t8.TI_DOCU_RETI = tB.TI_DOCU_RETI " & _
                "	And t8.NU_DOCU_RETI = tB.NU_DOCU_RETI And t8.NU_SECU = tB.NU_SECU " & _
                "	And t8.TI_SITU = 'ACT') ) "
            If sDE_VAPO.Trim <> "" Then
                sDE_SELE = sDE_SELE & " INNER JOIN TCBALA t10 " & _
                "ON (t10.NU_DOCU_RECE = t1.NU_DOCU_RECE " & _
                "And t10.TI_DOCU_RECE = t1.TI_DOCU_RECE " & _
                "And t10.CO_ALMA = t1.CO_ALMA " & _
                "And t10.CO_UNID = t1.CO_UNID " & _
                "And t10.CO_EMPR = t1.CO_EMPR  " & _
                "And t10.TI_SITU = 'ACT' " & _
                "And t10.DE_VAPO Like '%" & sDE_VAPO & "%') "
            End If
            sDE_SELE = sDE_SELE & " Where t1.CO_EMPR = '" & sCO_EMPR & "' " & _
                "And t1.CO_UNID = '" & sCO_UNID & "' " & _
                "and t1.TI_SITU = 'ACT'" & _
                "and t1.ST_CIER_COMP = 'S'" & _
                "and t1.OR_WEBB = 'S' "

            If sTI_DOCU_RECE.Trim.Length > 0 Then
                sDE_SELE = sDE_SELE & " And t1.TI_DOCU_RECE = '" & sTI_DOCU_RECE & "' "
            End If
            If sNU_DOCU_RECE.Trim.Length > 0 Then
                sDE_SELE = sDE_SELE & " And t1.NU_DOCU_RECE = '" & sNU_DOCU_RECE & "' "
            End If
            sDE_SELE = sDE_SELE & " And t1.CO_CLIE_ACTU = '" & sCO_CLIE & "' " & _
                "And ((DataLength(rtrim(ltrim('" & sCO_ALMA & "')))!= 0 And t1.CO_ALMA = '" & sCO_ALMA & "')Or(DataLength(Rtrim(Ltrim('" & sCO_ALMA & "'))) = 0)) "

            If sCO_MODA.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.CO_MODA_MOVI = '" & sCO_MODA & "' "
            End If

            If sCO_MONE.Trim <> "" Then
                sDE_SELE = sDE_SELE & " And t1.CO_MONE = '" & sCO_MONE & "' "
            End If

            sDE_SELE = sDE_SELE & " And ((DataLength(rtrim(ltrim('" & sCO_TIPO_MERC & "')))!= 0 And t1.CO_TIPO_MERC = '" & sCO_TIPO_MERC & "') " & _
              "Or(DataLength(Rtrim(Ltrim('" & sCO_TIPO_MERC & "'))) = 0))	" & _
              "And t1.TI_SITU = 'ACT' " & _
              "And t1.ST_CIER_COMP = 'S' "

            sDE_SELE = sDE_SELE & " And ((DataLength(rtrim(ltrim('" & sCO_PROD_CLIE & "')))!= 0 And t2.CO_PROD_CLIE = '" & sCO_PROD_CLIE & "') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sCO_PROD_CLIE & "'))) = 0)) " & _
                "And ((DataLength(IsNull('" & sFE_VENC_INIC & "',''))!= 0 And t2.FE_VCTO_MERC >= CONVERT(DATETIME,'" & sFE_VENC_INIC & "',103) " & _
                "And t2.FE_VCTO_MERC <= CONVERT(DATETIME,'" & sFE_VENC_FINA & "',103)) Or(DataLength(IsNull('" & sFE_VENC_INIC & "','')) = 0)) " & _
                "And ((DataLength(rtrim(ltrim('" & sDE_PROD & "')))!= 0 And t2.DE_MERC Like '%" & sDE_PROD & "%') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sDE_PROD & "'))) = 0)) " & _
                "And ((DataLength(rtrim(ltrim('" & sDE_REFE & "')))!= 0 And t2.DE_REFE_0002 Like '%" & sDE_REFE & "%') " & _
                "Or(DataLength(Rtrim(Ltrim('" & sDE_REFE & "'))) = 0)) " & _
                "Group By t3.TI_DOCU_RECE, t3.NU_DOCU_RECE, t3.NU_SECU, t3.NU_SECU_UBIC " & _
                "Having ((Max(t3.NU_UNID_RECI) - Sum(IsNull(t8.NU_UNID_RETI, 0))) > 0 " & _
                "Or (Max(t3.NU_PESO_RECI) - Sum(IsNull(t8.NU_PESO_RETI, 0))) > 0 ) " & _
                "Order By t3.TI_DOCU_RECE Asc, t3.NU_DOCU_RECE Desc, t3.NU_SECU Asc, t3.NU_SECU_UBIC Asc "
        End If
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DAST(sDE_SELE)
    End Function

    Public Function fn_TWMERC_SELE_I01(ByVal sNU_RECE_GRAB As String, ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String, _
         ByVal nNU_SECU_RECE As Integer, ByVal nNU_SECU_UBIC As Integer, ByVal sCO_TIPO_BULT As String, _
         ByVal sCO_UNME_PESO As String, ByVal nNU_UNID_RETI As Decimal, ByVal nNU_PESO_RETI As Decimal, _
         ByVal sCO_MONE As String, ByVal sCO_UNME_AREA As String, ByVal nNU_AREA_USAD As Decimal, _
         ByVal sCO_UNME_VOLU As String, ByVal nNU_VOLU_USAD As Decimal, ByVal nNU_UNID_RECI As Decimal, _
         ByVal nNU_PESO_RECI As Decimal, ByVal sCO_TIPO_MERC As String, ByVal sDE_TIPO_MERC As String, _
         ByVal sCO_STIP_MERC As String, ByVal sDE_STIP_MERC As String, ByVal sDE_MERC As String, _
         ByVal sCO_USUA As String) As Integer
        Dim arParms() As SqlParameter = New SqlParameter(21) {}
        arParms(0) = New SqlParameter("@sNU_RECE_GRAB", SqlDbType.VarChar, 40)
        arParms(0).Value = sNU_RECE_GRAB
        arParms(1) = New SqlParameter("@sTI_DOCU_RECE", SqlDbType.VarChar, 3)
        arParms(1).Value = sTI_DOCU_RECE
        arParms(2) = New SqlParameter("@sNU_DOCU_RECE", SqlDbType.VarChar, 20)
        arParms(2).Value = sNU_DOCU_RECE
        arParms(3) = New SqlParameter("@nNU_SECU_RECE", SqlDbType.Int)
        arParms(3).Value = nNU_SECU_RECE
        arParms(4) = New SqlParameter("@nNU_SECU_UBIC", SqlDbType.Int)
        arParms(4).Value = nNU_SECU_UBIC
        arParms(5) = New SqlParameter("@sCO_TIPO_BULT", SqlDbType.VarChar, 3)
        arParms(5).Value = sCO_TIPO_BULT
        arParms(6) = New SqlParameter("@sCO_UNME_PESO", SqlDbType.VarChar, 3)
        arParms(6).Value = sCO_UNME_PESO
        arParms(7) = New SqlParameter("@nNU_UNID_RETI", SqlDbType.Decimal)
        arParms(7).Value = nNU_UNID_RETI
        arParms(8) = New SqlParameter("@nNU_PESO_RETI", SqlDbType.Decimal)
        arParms(8).Value = nNU_PESO_RETI
        arParms(9) = New SqlParameter("@sCO_MONE", SqlDbType.VarChar, 3)
        arParms(9).Value = sCO_MONE
        arParms(10) = New SqlParameter("@sCO_UNME_AREA", SqlDbType.VarChar, 3)
        arParms(10).Value = sCO_UNME_AREA
        arParms(11) = New SqlParameter("@nNU_AREA_USAD", SqlDbType.Decimal)
        arParms(11).Value = nNU_AREA_USAD
        arParms(12) = New SqlParameter("@sCO_UNME_VOLU", SqlDbType.VarChar, 3)
        arParms(12).Value = sCO_UNME_VOLU
        arParms(13) = New SqlParameter("@nNU_VOLU_USAD", SqlDbType.Decimal)
        arParms(13).Value = nNU_VOLU_USAD
        arParms(14) = New SqlParameter("@nNU_UNID_RECI", SqlDbType.Decimal)
        arParms(14).Value = nNU_UNID_RECI
        arParms(15) = New SqlParameter("@nNU_PESO_RECI", SqlDbType.Decimal)
        arParms(15).Value = nNU_PESO_RECI
        arParms(16) = New SqlParameter("@sCO_TIPO_MERC", SqlDbType.VarChar, 3)
        arParms(16).Value = sCO_TIPO_MERC
        arParms(17) = New SqlParameter("@sDE_TIPO_MERC", SqlDbType.VarChar, 100)
        arParms(17).Value = sDE_TIPO_MERC
        arParms(18) = New SqlParameter("@sCO_STIP_MERC", SqlDbType.VarChar, 3)
        arParms(18).Value = sCO_STIP_MERC
        arParms(19) = New SqlParameter("@sDE_STIP_MERC", SqlDbType.VarChar, 100)
        arParms(19).Value = sDE_STIP_MERC
        arParms(20) = New SqlParameter("@sDE_MERC", SqlDbType.VarChar, 200)
        arParms(20).Value = sDE_MERC
        arParms(21) = New SqlParameter("@sCO_USUA", SqlDbType.VarChar, 8)
        arParms(21).Value = sCO_USUA
        Return SqlHelper.ExecuteNonQuery(strCCOfioper, CommandType.StoredProcedure, "usp_TWMERC_SELE_I01", arParms)
    End Function

    Public Function fn_THESTA_CLIE_I01(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, ByVal sTI_DOCU As String,
                                       ByVal sNU_DOCU As String, ByVal sTI_VALI As String, ByVal sCO_MONE As String,
                                       ByVal nIM_RETI As Decimal, ByVal sCO_USUA As String, ByVal sST_RETI_REGU As String,
                                       ByVal sTIP_LIB As String, ByVal sCO_MONE_DEU As String, ByVal nIM_TOTA_DEU As Decimal,
                                       ByVal nIM_DEU_00 As Decimal, ByVal nIM_DEU_30 As Decimal, ByVal nIM_DEU_60 As Decimal, ByVal nIM_DEU_90 As Decimal) As String
        Dim arParms() As SqlParameter = New SqlParameter(16) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = sCO_UNID
        arParms(2) = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        arParms(2).Value = sTI_DOCU
        arParms(3) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        arParms(3).Value = sNU_DOCU
        arParms(4) = New SqlParameter("@ISTI_VALI", SqlDbType.Char, 1)
        arParms(4).Value = sTI_VALI
        arParms(5) = New SqlParameter("@VSCO_MONE", SqlDbType.VarChar, 3)
        arParms(5).Value = sCO_MONE
        arParms(6) = New SqlParameter("@VNIM_RETI", SqlDbType.Decimal)
        arParms(6).Value = nIM_RETI
        arParms(7) = New SqlParameter("@VSCO_USUA", SqlDbType.VarChar, 20)
        arParms(7).Value = sCO_USUA
        arParms(8) = New SqlParameter("@VSST_RETI_REGU", SqlDbType.Char, 1)
        arParms(8).Value = sST_RETI_REGU
        arParms(9) = New SqlParameter("@TIP_LIB", SqlDbType.Char, 2)
        arParms(9).Value = sTIP_LIB
        arParms(10) = New SqlParameter("@VNCO_MONE_DEUD", SqlDbType.VarChar, 3)
        arParms(10).Value = sCO_MONE_DEU
        arParms(11) = New SqlParameter("@VSIM_TOTA", SqlDbType.Decimal)
        arParms(11).Value = nIM_TOTA_DEU
        arParms(12) = New SqlParameter("@VSIM_DEU_00", SqlDbType.Decimal)
        arParms(12).Value = nIM_DEU_00
        arParms(13) = New SqlParameter("@VSIM_DEU_30", SqlDbType.Decimal)
        arParms(13).Value = nIM_DEU_30
        arParms(14) = New SqlParameter("@VSIM_DEU_60", SqlDbType.Decimal)
        arParms(14).Value = nIM_DEU_60
        arParms(15) = New SqlParameter("@VSIM_DEU_90", SqlDbType.Decimal)
        arParms(15).Value = nIM_DEU_90
        arParms(16) = New SqlParameter("@ISDE_MENS", SqlDbType.VarChar, 8000)
        arParms(16).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCCOfioper, CommandType.StoredProcedure, "SP_ENVI_MAIL_Q05_A2_OLD", arParms)
        Return arParms(16).Value
    End Function

    Public Function fn_TCALMA_MERC_Q07(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
       ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String, ByVal nNU_SECU As Integer, _
       ByVal nNU_SECU_UBIC As Integer, ByVal sST_ALMA_SUBI As String, ByVal nNU_UNID_RETI As Decimal, _
       ByVal nNU_PESO_RETI As Decimal) As Boolean
        Dim drTCALMA_MERC As SqlDataReader = oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DREA("SP_TCALMA_MERC_Q03", CommandType.StoredProcedure, sCO_EMPR, sCO_UNID, _
                                      sTI_DOCU_RECE, sNU_DOCU_RECE, nNU_SECU, nNU_SECU_UBIC, sST_ALMA_SUBI)

        Dim nNU_SALD_UNID, nNU_SALD_PESO As Decimal
        If drTCALMA_MERC.Read Then
            If Not IsDBNull(drTCALMA_MERC(0)) Then nNU_SALD_UNID = drTCALMA_MERC(0) Else nNU_SALD_UNID = 0
            If Not IsDBNull(drTCALMA_MERC(1)) Then nNU_SALD_PESO = drTCALMA_MERC(1) Else nNU_SALD_PESO = 0
        End If

        drTCALMA_MERC.Close()
        drTCALMA_MERC.Close()

        If (nNU_UNID_RETI > Decimal.Round(nNU_SALD_UNID, 6)) Or (nNU_PESO_RETI > Decimal.Round(nNU_SALD_PESO, 6)) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function fn_TCDOCU_CLIE_Q08(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                          ByVal sFE_PROC As String, ByVal sCO_ENTI_TRAB As String) As DataSet
        Dim sCO_MONE_NACI, sCO_MONE_DOLA As String
        sCO_MONE_DOLA = fn_TMPARA_COME_Q01(sCO_EMPR)
        sCO_MONE_NACI = fn_TMEMPR_Q01(sCO_EMPR)

        Dim sDE_SELE As String
        sDE_SELE = "Select IsNull(FA_CMPR_OFIC,0) " & _
                    "From TCFACT_CAMB " & _
                    "Where CO_MONE='" & sCO_MONE_DOLA & "' " & _
                    "And FE_CAMB=Convert(datetime,Convert(varchar(10),GETDATE(),103),103)"

        oSE_CREA_SERV = Nothing
        oSE_CREA_SERV = New clsSE_CREA_SERV
        Dim nFA_CAMB_OPER As Decimal = CType(oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_VALO(sDE_SELE, CommandType.Text), Decimal)

        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOficome").fn_OBTI_DAST("USP_TCDOCU_CLIE_Q21", sCO_EMPR, sCO_UNID, sCO_MONE_NACI, sCO_MONE_DOLA, CType(sFE_PROC, DateTime), nFA_CAMB_OPER, sCO_ENTI_TRAB)
    End Function

    Public Function fn_TMCLIE_MERC_Q01(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
     ByVal sCO_CLIE As String, ByVal sCO_MONE As String, ByVal nIM_TOTA As Decimal, ByVal sCO_MONE_DEU As String, ByVal nIM_TOTA_DEU As Decimal) As String
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_VALO("SP_TMCLIE_MERC_Q01", CommandType.StoredProcedure, sCO_EMPR, sCO_UNID,
                     sCO_CLIE, sCO_MONE, nIM_TOTA, sCO_MONE_DEU, nIM_TOTA_DEU).ToString
    End Function

    'Public Function fn_TMCLIE_MERC_Q04(ByVal sCO_CLIE As String, ByVal nIM_TOTA As Decimal) As String
    '    Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_VALO("SP_TMCLIE_MERC_Q04", CommandType.StoredProcedure, sCO_CLIE, nIM_TOTA).ToString
    'End Function

    Public Function fn_TMCLIE_MERC_Q04(ByVal sCO_CLIE As String, ByVal nIM_TOTA As Decimal) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = sCO_CLIE
        arParms(1) = New SqlParameter("@INIM_VALO_NUEV", SqlDbType.Decimal)
        arParms(1).Value = nIM_TOTA
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.StoredProcedure, "SP_TMCLIE_MERC_Q04_OLD", arParms).Tables(0)
    End Function

    'nuevofn_TCRETI_MERC_SEL_APR
    Public Function fn_TCRETI_MERC_SEL_APR(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, ByVal sTI_DOCU As String, _
                                        ByVal sNU_DOCU As String) As String
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_VALO("SP_TCRETI_MERC_SEL_APR", CommandType.StoredProcedure, sCO_EMPR, sCO_UNID, _
                     sTI_DOCU, sNU_DOCU).ToString

        '---------------------------------------
    End Function


    Public Function fn_TMMENS_VALI_Q01(ByVal sCO_EMPR As String) As DataSet
        Dim sDE_SELE As String = "SELECT DE_MEN1, DE_MEN2 FROM TMMENS_VALI WHERE CO_EMPR='" & sCO_EMPR & "' "
        Return oSE_CREA_SERV.fn_SERV_DATO("ConnectionStringOfioper").fn_OBTI_DAST(sDE_SELE)
    End Function

    Public Class clsSE_CREA_SERV
#Region "Declaracion de variables"
        Private oSR_DATO As Capas.Datos.ClsDatos
#End Region
#Region "Privadas"
        '--------------------------------------------------------'
        '--Funcion           : fn_SERV_DATO                    --'
        '--Descripcion       : Creacion de Servidor de Datos   --'
        '--Parametro Salida  : objeto Clase Datos              --'
        '--Desarrollado por  : Diana Mena Cordova              --' 
        '--Fecha Creacion    : 15/03/2003                      --' 
        '--------------------------------------------------------'
        Public Function fn_SERV_DATO(ByVal sNO_MODU As String) As Capas.Datos.ClsDatos
            'If oSR_DATO Is Nothing Then
            oSR_DATO = New Capas.Datos.ClsDatosSqlServer(sNO_MODU)
            'End If
            Return oSR_DATO
        End Function
#End Region
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class


    'Valida UITS
    Public Function fn_VALUIT(ByVal sCO_MONE As String, ByVal nIM_TOTA As Decimal, ByVal NU_DCR As String, ByVal TI_VAL As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CO_MONE", SqlDbType.VarChar, 3)
        arParms(0).Value = sCO_MONE
        arParms(1) = New SqlParameter("@VAL_SOLI_TOT", SqlDbType.Decimal)
        arParms(1).Value = nIM_TOTA
        arParms(2) = New SqlParameter("@NU_DCR", SqlDbType.VarChar, 20)
        arParms(2).Value = NU_DCR
        arParms(3) = New SqlParameter("@TI_VAL", SqlDbType.VarChar, 1)
        arParms(3).Value = TI_VAL
        Return SqlHelper.ExecuteDataset(strCCOfioper, CommandType.StoredProcedure, "SP_VALIDA_UIT_AFI", arParms).Tables(0)
    End Function


End Class
