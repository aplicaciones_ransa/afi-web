Imports System.Data
Imports System.Data.SqlClient
Public Class Usuario
    Inherits System.ComponentModel.Component
    Private strCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region
    Public Function gGetTipoUsuarios() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFTIPOUSR_Sel_TraerTipoUsuario").Tables(0)
    End Function

    Public Function gInsUsuarioEntidad(ByVal strIdEntidad As String, ByVal strFax As String, _
                            ByVal strTelefono As String, ByVal bytFirma As Byte(), _
                            ByVal strTitulo As String, ByVal strNombreEntidad As String, _
                            ByVal strTipoEntidad As String, ByVal strIdUsuarioCrea As String, _
                            ByVal blnLiberador As Boolean, ByVal blnNotificar As Boolean, _
                            ByVal strRUC As String, ByVal strDireccion As String, _
                            ByVal strNombreLargo As String, ByVal strIdUsuario As String, _
                            ByVal strTipoUsuario As String, ByVal bnlUsuarioActivo As Boolean, _
                            ByVal blnAlerta As Boolean, ByVal blnAdjunto As Boolean, _
                            ByVal strCoGrup As String, ByVal strCoSist As String, _
                            ByVal blnSucursales As Boolean, ByVal blnVisto As Boolean) As String
        Dim arParms() As SqlParameter = New SqlParameter(22) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        arParms(1).Value = strFax.Trim
        arParms(2) = New SqlParameter("@Telefono", SqlDbType.VarChar, 15)
        arParms(2).Value = strTelefono.Trim
        arParms(3) = New SqlParameter("@Firma", SqlDbType.Image)
        arParms(3).Value = bytFirma
        arParms(4) = New SqlParameter("@Titulo", SqlDbType.VarChar, 50)
        arParms(4).Value = strTitulo.Trim
        arParms(5) = New SqlParameter("@NombreEntidad", SqlDbType.VarChar, 100)
        arParms(5).Value = strNombreEntidad.Trim
        arParms(6) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(6).Value = strTipoEntidad.Trim
        arParms(7) = New SqlParameter("@IdUsuarioCrea", SqlDbType.VarChar, 20)
        arParms(7).Value = strIdUsuarioCrea.Trim
        arParms(8) = New SqlParameter("@Liberador", SqlDbType.Bit)
        arParms(8).Value = blnLiberador
        arParms(9) = New SqlParameter("@Notificar", SqlDbType.Bit)
        arParms(9).Value = blnNotificar
        arParms(10) = New SqlParameter("@RUC", SqlDbType.VarChar, 11)
        arParms(10).Value = strRUC
        arParms(11) = New SqlParameter("@Direccion", SqlDbType.VarChar, 100)
        arParms(11).Value = strDireccion
        arParms(12) = New SqlParameter("@NombreLargo", SqlDbType.VarChar, 180)
        arParms(12).Value = strNombreLargo
        arParms(13) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(13).Value = strIdUsuario
        arParms(14) = New SqlParameter("@TipoUsuario", SqlDbType.Char, 2)
        arParms(14).Value = strTipoUsuario
        arParms(15) = New SqlParameter("@UsuarioActivo", SqlDbType.Bit)
        arParms(15).Value = bnlUsuarioActivo
        arParms(16) = New SqlParameter("@Alerta", SqlDbType.Bit)
        arParms(16).Value = blnAlerta
        arParms(17) = New SqlParameter("@Adjunto", SqlDbType.Bit)
        arParms(17).Value = blnAdjunto
        arParms(18) = New SqlParameter("@co_grup", SqlDbType.VarChar, 8)
        arParms(18).Value = strCoGrup
        arParms(19) = New SqlParameter("@co_sist", SqlDbType.VarChar, 8)
        arParms(19).Value = strCoSist
        arParms(20) = New SqlParameter("@Sucursales", SqlDbType.Bit)
        arParms(20).Value = blnSucursales
        arParms(21) = New SqlParameter("@Visto", SqlDbType.Bit)
        arParms(21).Value = blnVisto
        arParms(22) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 10)
        arParms(22).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Ins_NuevaEntidad_Info", arParms)
        Return arParms(22).Value.ToString
    End Function

    Public Function gInsUsuario(ByVal strDNI As String, ByVal strNombre As String, _
                                ByVal strApellido As String, ByVal strFechaCaducidad As String, _
                                ByVal strLogin As String, ByVal strContraseņa As String, _
                                ByVal strIdUsuarioCrea As String, ByVal strCorreo As String, _
                                ByVal blnActivo As Boolean) As String
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@DNI", SqlDbType.Char, 8)
        arParms(0).Value = strDNI.Trim
        arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar, 80)
        arParms(1).Value = strNombre.Trim
        arParms(2) = New SqlParameter("@Apellido", SqlDbType.VarChar, 80)
        arParms(2).Value = strApellido.Trim
        arParms(3) = New SqlParameter("@FechaCaducidad", SqlDbType.VarChar, 10)
        If strFechaCaducidad = "" Then
            arParms(3).Value = DBNull.Value
        Else
            arParms(3).Value = strFechaCaducidad.Trim
        End If
        arParms(4) = New SqlParameter("@Login", SqlDbType.VarChar, 20)
        arParms(4).Value = strLogin.Trim
        arParms(5) = New SqlParameter("@Contrasena", SqlDbType.VarChar, 100)
        arParms(5).Value = strContraseņa
        arParms(6) = New SqlParameter("@IdUsuarioCrea", SqlDbType.Char, 4)
        arParms(6).Value = strIdUsuarioCrea.Trim
        arParms(7) = New SqlParameter("@Correo", SqlDbType.VarChar, 50)
        arParms(7).Value = strCorreo.Trim
        arParms(8) = New SqlParameter("@Activo", SqlDbType.Bit)
        arParms(8).Value = blnActivo
        arParms(9) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 10)
        arParms(9).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Ins_NuevoUsuario", arParms)
        Return arParms(9).Value.ToString
    End Function

    Public Function gUpdUsuarioEntidad(ByVal strIdEntidad As String, ByVal strFax As String, _
                            ByVal strTelefono As String, ByVal bytFirma As Byte(), _
                            ByVal strTitulo As String, ByVal strNombreEntidad As String, _
                            ByVal strTipoEntidad As String, ByVal strIdUsuario As String, _
                            ByVal strIdUsuarioModi As String, ByVal blnLiberador As Boolean, _
                            ByVal blnNotificar As Boolean, ByVal strRUC As String, _
                            ByVal strDireccion As String, ByVal strNomLargo As String, _
                            ByVal strCodEntidad As String, ByVal strTipoUsuario As String, _
                            ByVal bnlUsuarioActivo As Boolean, ByVal blnAlerta As Boolean, _
                            ByVal blnAdjunto As Boolean, ByVal strCoGrup As String, _
                            ByVal strCoSist As String, ByVal blnSucursales As Boolean, ByVal blnVisto As Boolean) As String
        Dim arParms() As SqlParameter = New SqlParameter(23) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        arParms(1).Value = strFax.Trim
        arParms(2) = New SqlParameter("@Telefono", SqlDbType.VarChar, 15)
        arParms(2).Value = strTelefono.Trim
        arParms(3) = New SqlParameter("@Firma", SqlDbType.Image)
        arParms(3).Value = bytFirma
        arParms(4) = New SqlParameter("@Titulo", SqlDbType.VarChar, 50)
        arParms(4).Value = strTitulo.Trim
        arParms(5) = New SqlParameter("@NombreEntidad", SqlDbType.VarChar, 100)
        arParms(5).Value = strNombreEntidad.Trim
        arParms(6) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(6).Value = strTipoEntidad.Trim
        arParms(7) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(7).Value = strIdUsuario.Trim
        arParms(8) = New SqlParameter("@IdUsuarioModi", SqlDbType.VarChar, 20)
        arParms(8).Value = strIdUsuarioModi.Trim
        arParms(9) = New SqlParameter("@Liberador", SqlDbType.Bit)
        arParms(9).Value = blnLiberador
        arParms(10) = New SqlParameter("@Notificar", SqlDbType.Bit)
        arParms(10).Value = blnNotificar
        arParms(11) = New SqlParameter("@RUC", SqlDbType.VarChar, 11)
        arParms(11).Value = strRUC
        arParms(12) = New SqlParameter("@Direccion", SqlDbType.VarChar, 100)
        arParms(12).Value = strDireccion
        arParms(13) = New SqlParameter("@NomLargo", SqlDbType.VarChar, 180)
        arParms(13).Value = strNomLargo
        arParms(14) = New SqlParameter("@CodEntidad", SqlDbType.VarChar, 20)
        arParms(14).Value = strCodEntidad
        arParms(15) = New SqlParameter("@TipoUsuario", SqlDbType.Char, 2)
        arParms(15).Value = strTipoUsuario
        arParms(16) = New SqlParameter("@UsuarioActivo", SqlDbType.Bit)
        arParms(16).Value = bnlUsuarioActivo
        arParms(17) = New SqlParameter("@Alerta", SqlDbType.Bit)
        arParms(17).Value = blnAlerta
        arParms(18) = New SqlParameter("@Adjunto", SqlDbType.Bit)
        arParms(18).Value = blnAdjunto
        arParms(19) = New SqlParameter("@Co_Grup", SqlDbType.VarChar, 8)
        arParms(19).Value = strCoGrup
        arParms(20) = New SqlParameter("@Co_Sist", SqlDbType.VarChar, 8)
        arParms(20).Value = strCoSist
        arParms(21) = New SqlParameter("@Sucursales", SqlDbType.Bit)
        arParms(21).Value = blnSucursales
        arParms(22) = New SqlParameter("@Visto", SqlDbType.Bit)
        arParms(22).Value = blnVisto
        arParms(23) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 40)
        arParms(23).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Upd_ActualizarEntidadUsuario_Info", arParms)
        Return arParms(23).Value.ToString
    End Function

    Public Function gUpdUsuario(ByVal strDNI As String, ByVal strNombre As String, _
                                ByVal strApellido As String, ByVal strFechaCaducidad As String, _
                                ByVal strLogin As String, ByVal strIdUsuario As String, _
                                ByVal strIdUsuarioModi As String, ByVal strCorreo As String, _
                                ByVal blnActivo As Boolean) As String
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@DNI", SqlDbType.Char, 10)
        arParms(0).Value = strDNI.Trim
        arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar, 80)
        arParms(1).Value = strNombre.Trim
        arParms(2) = New SqlParameter("@Apellido", SqlDbType.VarChar, 80)
        arParms(2).Value = strApellido.Trim
        arParms(3) = New SqlParameter("@FechaCaducidad", SqlDbType.VarChar, 10)
        If strFechaCaducidad = "" Then
            arParms(3).Value = DBNull.Value
        Else
            arParms(3).Value = strFechaCaducidad.Trim
        End If
        arParms(4) = New SqlParameter("@Login", SqlDbType.VarChar, 20)
        arParms(4).Value = strLogin.Trim
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario.Trim
        arParms(6) = New SqlParameter("@IdUsuarioModi", SqlDbType.Char, 4)
        arParms(6).Value = strIdUsuarioModi.Trim
        arParms(7) = New SqlParameter("@Correo", SqlDbType.VarChar, 50)
        arParms(7).Value = strCorreo.Trim
        arParms(8) = New SqlParameter("@Activo", SqlDbType.Bit)
        arParms(8).Value = blnActivo
        arParms(9) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 50)
        arParms(9).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Upd_ActualizarUsuario", arParms)
        Return arParms(9).Value.ToString
    End Function

    Public Function gUpdClaveFirma(ByVal strcontrasena As String, ByVal strusuario As String, ByVal strIdUsuarioModi As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Contrasena", SqlDbType.VarChar, 100)
        arParms(0).Value = strcontrasena.Trim
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(1).Value = strusuario.Trim
        arParms(2) = New SqlParameter("@IdUsuarioModi", SqlDbType.Char, 4)
        arParms(2).Value = strIdUsuarioModi
        arParms(3) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 50)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Upd_ActualizarClaveFirma", arParms)
        Return arParms(3).Value.ToString
    End Function
    Public Function gUpdClaveFirmaMobil(ByVal strcontrasena As String, ByVal strusuario As String, ByVal strDecUsuarioLogin As String, ByVal strIdUsuarioModi As String, ByVal strTipoEnvio As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@Contrasena", SqlDbType.VarChar, 100)
        arParms(0).Value = strcontrasena.Trim
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(1).Value = strusuario.Trim
        arParms(2) = New SqlParameter("@Login", SqlDbType.Char, 50)
        arParms(2).Value = strDecUsuarioLogin.Trim
        arParms(3) = New SqlParameter("@IdUsuarioModi", SqlDbType.Char, 4)
        arParms(3).Value = strIdUsuarioModi
        arParms(4) = New SqlParameter("@TipoEnvio", SqlDbType.Char, 1)
        arParms(4).Value = strTipoEnvio.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Mobil_Upd_ActualizarClave", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaUsuario(ByVal strTipoEntidad As String, ByVal strEntidad As String, _
                                        ByVal strNombres As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strTipoEntidad.Trim
        arParms(1) = New SqlParameter("@Entidad", SqlDbType.VarChar, 20)
        arParms(1).Value = strEntidad.Trim
        arParms(2) = New SqlParameter("@Nombres", SqlDbType.VarChar, 80)
        arParms(2).Value = strNombres.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_BuscarUsuarios", arParms).Tables(0)
    End Function

    Public Function gGetUsuarioAGD(ByVal strTipoEntidad As String, ByVal strEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strTipoEntidad.Trim
        arParms(1) = New SqlParameter("@Entidad", SqlDbType.VarChar, 20)
        arParms(1).Value = strEntidad.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_UsuariosAGD", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaUsuarioxTipoDocumento(ByVal strTipoEntidad As String, ByVal strEntidad As String, _
                                                  ByVal strNombres As String, ByVal strCliente As String, _
                                                  ByVal strCodDepartamento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strTipoEntidad.Trim
        arParms(1) = New SqlParameter("@Entidad", SqlDbType.VarChar, 20)
        arParms(1).Value = strEntidad.Trim
        arParms(2) = New SqlParameter("@Nombres", SqlDbType.VarChar, 80)
        arParms(2).Value = strNombres.Trim
        arParms(3) = New SqlParameter("@Cliente", SqlDbType.VarChar, 20)
        arParms(3).Value = strCliente.Trim
        arParms(4) = New SqlParameter("@CodDepartamento", SqlDbType.VarChar, 6)
        arParms(4).Value = strCodDepartamento.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_BuscarUsuariosxTipoDocumento", arParms).Tables(0)
    End Function

    Public Function gGetUsuario(ByVal strCodUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodUsuario.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_DataUsuario", arParms).Tables(0)
    End Function

    Public Function gGetEmailAprobadores(Optional ByVal strIdSicoAGD As String = "", Optional ByVal strIdSicoDepositante As String = "", Optional ByVal strTipDoc As String = "") As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdSicoAGD", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdSicoAGD.Trim
        arParms(1) = New SqlParameter("@IdSicoDepositante", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdSicoDepositante.Trim
        arParms(2) = New SqlParameter("@TipDoc", SqlDbType.VarChar, 20)
        arParms(2).Value = strTipDoc.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_Email", arParms).Tables(0)
    End Function

    'Public Function gGetEmailAprobadores(ByVal strIdSicoAGD As String, ByVal strIdSicoDepositante As String, ByVal strTipDoc As String) As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(2) {}
    '    arParms(0) = New SqlParameter("@IdSicoAGD", SqlDbType.VarChar, 20)
    '    arParms(0).Value = strIdSicoAGD.Trim
    '    arParms(1) = New SqlParameter("@IdSicoDepositante", SqlDbType.VarChar, 20)
    '    arParms(1).Value = strIdSicoDepositante.Trim
    '    arParms(2) = New SqlParameter("@TipDoc", SqlDbType.VarChar, 20)
    '    arParms(2).Value = strTipDoc.Trim
    '    Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_Email", arParms).Tables(0)
    'End Function

    Public Function gGetEmailTraslado(Optional ByVal strIdAGD As String = "", Optional ByVal strIdDepositante As String = "", Optional ByVal strIdFinanciador As String = "") As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdSicoAGD", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdAGD.Trim
        arParms(1) = New SqlParameter("@IdSicoDepositante", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdDepositante.Trim
        arParms(2) = New SqlParameter("@IdSicoFinanciador", SqlDbType.VarChar, 20)
        arParms(2).Value = strIdFinanciador.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_EnvioCorreoTraslado", arParms).Tables(0)
    End Function

    Public Function gGetDataUsuarioxEntidad(ByVal strCodUsuario As String, ByVal strCodSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodUsuario.Trim
        arParms(1) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodSico.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Sel_DataUsuarioEntidad", arParms).Tables(0)
    End Function

    Public Function gDelEntidadDeUsuario(ByVal strCodUsuario As String, ByVal strCodSICO As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodUsuario.Trim
        arParms(1) = New SqlParameter("@CodSICO", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodSICO
        arParms(2) = New SqlParameter("@Error", SqlDbType.Char, 1)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Del_Entidad", arParms)
        Return arParms(2).Value
    End Function

    Public Function gCambiarEstadoUsuario(ByVal strIdUsuario As String, ByVal strIdSico As String, ByVal blnEstado As Boolean)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdSico
        arParms(2) = New SqlParameter("@Estado", SqlDbType.Bit)
        arParms(2).Value = blnEstado
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Upd_ActualizarEstadoUsuario", arParms)
    End Function
End Class
