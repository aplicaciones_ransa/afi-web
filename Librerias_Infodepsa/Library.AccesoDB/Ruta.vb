Imports System.Data
Imports System.Data.SqlClient
Public Class Ruta
    Inherits System.ComponentModel.Component
    Private strCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region
    Public Function gGetRutaxTipoDocumento(ByVal strTipoDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(0).Value = strTipoDocumento.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFSECUENCIARUTA_Sel_TraerSecuenciaxTipoDocumento", arParms).Tables(0)
    End Function

    Public Function gGetEnvioMailxTipoEntidad(ByVal strSecuencia As String, ByVal strRuta As String, _
                                                ByVal strTipDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(0).Value = CInt(strSecuencia)
        arParms(1) = New SqlParameter("@Ruta", SqlDbType.Char, 8)
        arParms(1).Value = strRuta.Trim
        arParms(2) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(2).Value = strTipDoc.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENVIARMAILXENTIDAD_Sel_TraerEntidadesaEnviarMail", arParms).Tables(0)
    End Function

    Public Function gGetControlesxTipoUsuario(ByVal strSecuencia As String, ByVal strRuta As String, _
                                                ByVal strTipDoc As String, ByVal strTipEnt As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(0).Value = CInt(strSecuencia)
        arParms(1) = New SqlParameter("@Ruta", SqlDbType.Char, 8)
        arParms(1).Value = strRuta.Trim
        arParms(2) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(2).Value = strTipDoc.Trim
        arParms(3) = New SqlParameter("@TipEnt", SqlDbType.Char, 2)
        arParms(3).Value = strTipEnt.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFACCESOCONTROLES_Sel_TraerControlesxUsuario", arParms).Tables(0)
    End Function

    Public Function gInsEnvioMail(ByVal strCodTipoEntidad As String, ByVal strSecuencia As String, _
                                    ByVal strRuta As String, ByVal strTipDoc As String, _
                                    ByVal strIdUsuario As String, ByVal blnMail As Boolean, _
                                    ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@CodTipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strCodTipoEntidad
        arParms(1) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(1).Value = CInt(strSecuencia)
        arParms(2) = New SqlParameter("@Ruta", SqlDbType.Char, 8)
        arParms(2).Value = strRuta.Trim
        arParms(3) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(3).Value = strTipDoc.Trim
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(4).Value = strIdUsuario
        arParms(5) = New SqlParameter("@Mail", SqlDbType.Bit)
        arParms(5).Value = blnMail
        SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFENVIARMAILXENTIDAD_Ins_InsertarEntidadesaEnviarMail", arParms)
    End Function

    Public Function gInsAccesoControles(ByVal strIdControl As String, ByVal strSecuencia As String, _
                                    ByVal strRuta As String, ByVal strTipDoc As String, _
                                    ByVal strTipEnt As String, ByVal strIdUsuario As String, _
                                    ByVal blnUsrComun As Boolean, ByVal blnUsrConsultor As Boolean, _
                                    ByVal blnUsrAprobador As Boolean, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@IdControl", SqlDbType.Char, 2)
        arParms(0).Value = strIdControl
        arParms(1) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(1).Value = CInt(strSecuencia)
        arParms(2) = New SqlParameter("@Ruta", SqlDbType.Char, 8)
        arParms(2).Value = strRuta.Trim
        arParms(3) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(3).Value = strTipDoc.Trim
        arParms(4) = New SqlParameter("@TipEnt", SqlDbType.Char, 2)
        arParms(4).Value = strTipEnt.Trim
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@UsrComun", SqlDbType.Bit)
        arParms(6).Value = blnUsrComun
        arParms(7) = New SqlParameter("@UsrConsultor", SqlDbType.Bit)
        arParms(7).Value = blnUsrConsultor
        arParms(8) = New SqlParameter("@UsrAprobador", SqlDbType.Bit)
        arParms(8).Value = blnUsrAprobador
        SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFACCESOCONTROLES_Ins_InsertarControlesxTipoUsuario", arParms)
    End Function

    Public Function gInsSecuencia(ByVal strNroSecuencia As String, ByVal strTipoDocumento As String, _
                                    ByVal strDescripcion As String, ByVal strCodTipoEntidad As String, _
                                    ByVal strCodEstado As String, ByVal strIdUsuario As String, _
                                    ByVal ObjTrans As SqlTransaction) As String

        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroSecuencia", SqlDbType.Int)
        arParms(0).Value = strNroSecuencia
        arParms(1) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(1).Value = strTipoDocumento
        arParms(2) = New SqlParameter("@Descripcion", SqlDbType.VarChar, 100)
        arParms(2).Value = strDescripcion
        arParms(3) = New SqlParameter("@CodTipoEntidad", SqlDbType.Char, 2)
        arParms(3).Value = strCodTipoEntidad
        arParms(4) = New SqlParameter("@CodEstado", SqlDbType.Char, 2)
        arParms(4).Value = strCodEstado
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFSECUENCIARUTA_Ins_InsertarSecuencia", arParms)
    End Function

    Public Function gUpdFirma(ByVal strSecuencia As String, ByVal strTipoDocumento As String, _
                                   ByVal strIdUsuario As String, ByVal blnFirma As Boolean) As String

        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(0).Value = strSecuencia
        arParms(1) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(1).Value = strTipoDocumento
        arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(2).Value = strIdUsuario
        arParms(3) = New SqlParameter("@Firma", SqlDbType.Bit)
        arParms(3).Value = blnFirma
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFSECUENCIARUTA_Upd_Firma", arParms)
    End Function

    Public Function gUpdDblEndo(ByVal strSecuencia As String, ByVal strTipoDocumento As String, _
                                   ByVal strIdUsuario As String, ByVal blnDblEndo As Boolean) As String

        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Secuencia", SqlDbType.Int)
        arParms(0).Value = strSecuencia
        arParms(1) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(1).Value = strTipoDocumento
        arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(2).Value = strIdUsuario
        arParms(3) = New SqlParameter("@DblEndo", SqlDbType.Bit)
        arParms(3).Value = blnDblEndo
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFSECUENCIARUTA_Upd_DblEndo", arParms)
    End Function
End Class
