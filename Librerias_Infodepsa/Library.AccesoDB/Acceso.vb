Imports System.Data
Imports System.Data.SqlClient

Public Class Acceso
    Public strIdUsuario As String
    Public strIdTipoUsuario As String
    Public strIdTipoEntidad As String
    Public strIdSico As String
    Public intCaduco As Int16
    Public blnEstado As Boolean
    Public strNroCertificado As String
    Public strResult As String
    Public strNomEntidad As String
    Public strPendiente As String
    Public strCoGrup As String
    Public strCoSist As String
    Public strNombre As String
    Public strTipoUsuario As String
    Public strDirEmai As String
    Public blnCert As Boolean
    Public blnLibMult As Boolean
    Public strCodSAP As String
    Private strCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCadenaOfisegu As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringSeguridad")

    Public Function gInicioSesion(ByVal strLogin As String, ByVal strPassword As String, ByVal sIP_PETI As String) As Boolean
        Dim dt As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Login", SqlDbType.VarChar, 20)
        arParms(0).Value = strLogin
        arParms(1) = New SqlParameter("@Password", SqlDbType.VarChar, 50)
        arParms(1).Value = strPassword
        arParms(2) = New SqlParameter("@ISIP_PETI", SqlDbType.VarChar, 50)
        arParms(2).Value = sIP_PETI
        arParms(3) = New SqlParameter("@Result", SqlDbType.Char, 1)
        arParms(3).Direction = ParameterDirection.Output
        dt = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_ValidaUsuario_Info", arParms).Tables(0)
        strResult = arParms(3).Value
        If dt.Rows.Count = 1 Then
            With dt
                strIdUsuario = .Rows(0)("COD_USER").ToString
                intCaduco = CInt(.Rows(0)("FIR_CADU"))
                strNroCertificado = .Rows(0)("CER_NUMSERI")
                strNombre = CStr(.Rows(0)("NOM_USER"))
                strTipoUsuario = CStr(.Rows(0)("TI_USUA"))
                strDirEmai = CStr(.Rows(0)("DIR_EMAI"))
            End With
        End If
        If strResult = "I" Then
            Return True
        Else
            Return False
        End If
        dt.Dispose()
        dt = Nothing
    End Function

    Public Function gInicioSesionMobil(ByVal strLogin As String, sIMEI As String) As Boolean
        Dim dt As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@Login", SqlDbType.VarChar, 20)
        arParms(0).Value = strLogin
        'arParms(1) = New SqlParameter("@Password", SqlDbType.VarChar, 50)
        'arParms(1).Value = strPassword
        arParms(2) = New SqlParameter("@IMEI", SqlDbType.VarChar, 50)
        arParms(2).Value = sIMEI
        arParms(1) = New SqlParameter("@Result", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        dt = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_ValidaUsuario_InfoMobil", arParms).Tables(0)
        strResult = arParms(1).Value
        If dt.Rows.Count = 1 Then
            With dt
                strIdUsuario = .Rows(0)("COD_USER").ToString
                intCaduco = CInt(.Rows(0)("FIR_CADU"))
                strNroCertificado = .Rows(0)("CER_NUMSERI")
                strNombre = CStr(.Rows(0)("NOM_USER"))
                strTipoUsuario = CStr(.Rows(0)("TI_USUA"))
                strDirEmai = CStr(.Rows(0)("DIR_EMAI"))
            End With
        End If
        If strResult = "I" Then
            Return True
        Else
            Return False
        End If
        dt.Dispose()
        dt = Nothing
    End Function

    Public Function gInicioSesionGeneral(ByVal strLogin As String, ByVal sIP_PETI As String) As Boolean
        Dim dt As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@Login", SqlDbType.VarChar, 20)
        arParms(0).Value = strLogin
        arParms(1) = New SqlParameter("@ISIP_PETI", SqlDbType.VarChar, 50)
        arParms(1).Value = sIP_PETI
        arParms(2) = New SqlParameter("@Result", SqlDbType.Char, 1)
        arParms(2).Direction = ParameterDirection.Output
        dt = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_ValidaUsuarioGeneral", arParms).Tables(0)
        strResult = arParms(2).Value
        If dt.Rows.Count = 1 Then
            With dt
                strIdUsuario = .Rows(0)("COD_USER").ToString
                intCaduco = CInt(.Rows(0)("FIR_CADU"))
                strNroCertificado = .Rows(0)("CER_NUMSERI")
                strNombre = CStr(.Rows(0)("NOM_USER"))
                strTipoUsuario = CStr(.Rows(0)("TI_USUA"))
                strDirEmai = CStr(.Rows(0)("DIR_EMAI"))
            End With
        End If
        If strResult = "I" Then
            Return True
        Else
            Return False
        End If
        dt.Dispose()
        dt = Nothing
    End Function

    Public Function gInicioSesionEmpresa(ByVal strCodUser As String, ByVal strCodSico As String)
        Dim dt As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodUser", SqlDbType.Char, 4)
        arParms(0).Value = strCodUser
        arParms(1) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodSico
        dt = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTDAD_Sel_ValidaEntidadUsuario", arParms).Tables(0)
        If dt.Rows.Count = 1 Then
            strIdTipoUsuario = dt.Rows(0)("COD_TIPUSR").ToString
            strIdSico = dt.Rows(0)("COD_SICO").ToString
            strIdTipoEntidad = dt.Rows(0)("COD_TIPENTI").ToString
            blnEstado = dt.Rows(0)("EST_USER")
            strNomEntidad = dt.Rows(0)("NOM_ENTI")
            strPendiente = dt.Rows(0)("FLG_LIBPEND").ToString
            strCoGrup = dt.Rows(0)("COD_GRUP").ToString
            strCoSist = dt.Rows(0)("COD_SIST").ToString
            blnCert = dt.Rows(0)("FLG_CERT")
            blnLibMult = dt.Rows(0)("FLG_LIBMULT")
            strCodSAP = dt.Rows(0)("NU_DOCU_0004")
        End If
        dt.Dispose()
        dt = Nothing
    End Function

    Public Function gRecuperaCodSAP(ByVal strCodSico As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodSico
        arParms(1) = New SqlParameter("@CodSAP", SqlDbType.VarChar, 20)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTDAD_Sel_RecuperaCodSAP", arParms)
        Return arParms(1).Value
    End Function

    Public Function gGetMenuxTipoUsuario(ByVal strTipoUsuario As String, ByVal strTipoEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@TipoUsuario", SqlDbType.Char, 2)
        arParms(0).Value = strTipoUsuario
        arParms(1) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(1).Value = strTipoEntidad
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFMENU_Sel_ListarxTipoUsuario", arParms).Tables(0)
    End Function

    Public Function gInsLogAcceso(ByVal strCodUsuario As String, ByVal strTipEntidad As String, _
                                  ByVal strCodEntidad As String, ByVal strNomEntidad As String, _
                                  ByVal strTipMenu As String, ByVal strCodMenu As String, _
                                  ByVal strNomMenu As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@sCO_USUA", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodUsuario
        arParms(1) = New SqlParameter("@sTI_ENTI_TRAB", SqlDbType.Char, 1)
        arParms(1).Value = strTipEntidad
        arParms(2) = New SqlParameter("@sCO_ENTI_TRAB", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodEntidad
        arParms(3) = New SqlParameter("@sNO_ENTI_TRAB", SqlDbType.VarChar, 200)
        arParms(3).Value = strNomEntidad
        arParms(4) = New SqlParameter("@sTI_MENU", SqlDbType.Char, 1)
        arParms(4).Value = strTipMenu
        arParms(5) = New SqlParameter("@sCO_MENU", SqlDbType.VarChar, 20)
        arParms(5).Value = strCodMenu
        arParms(6) = New SqlParameter("@sNO_MENU", SqlDbType.VarChar, 50)
        arParms(6).Value = strNomMenu
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_TMREGI_ACCE_Ins_Acceso", arParms).Tables(0)
    End Function

    Public Function gGetMenuPrincipal(ByVal strCO_GRUP As String, ByVal strCO_SIST As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CO_GRUP", SqlDbType.VarChar, 8)
        arParms(0).Value = strCO_GRUP
        arParms(1) = New SqlParameter("@CO_SIST", SqlDbType.VarChar, 8)
        arParms(1).Value = strCO_SIST
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFMENU_Sel_MenuPrincipal", arParms).Tables(0)
    End Function

    Public Function gGetOpciones(ByVal strCO_SIST As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strCO_SIST
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFMENU_Sel_Opciones", arParms).Tables(0)
    End Function

    Public Function gGetMenuxGrupo(ByVal strCO_GRUP As String, ByVal strCO_SIST As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CO_GRUP", SqlDbType.VarChar, 8)
        arParms(0).Value = strCO_GRUP
        arParms(1) = New SqlParameter("@CO_SIST", SqlDbType.VarChar, 8)
        arParms(1).Value = strCO_SIST
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFMENU_Sel_ListarxGrupo", arParms).Tables(0)
    End Function

    Public Function gCertificadoRevocado(ByVal strIdUsuario As String) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@Mensaje", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_VerificarRevocacion", arParms)
        Return arParms(1).Value
    End Function

    Public Function gCambiarContrase�a(ByVal strIdUsuario As String, ByVal strPassAnterior As String,
                                           ByVal strPassNuevo As String, ByVal intNroDiasCaduca As Int16,
                                           ByVal strPregSegu As String, ByVal strRespSecre As String) As Boolean
        Dim Result As Integer
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@PassAnterior", SqlDbType.VarChar, 50)
        arParms(1).Value = strPassAnterior
        arParms(2) = New SqlParameter("@PassNuevo", SqlDbType.VarChar, 50)
        arParms(2).Value = strPassNuevo
        arParms(3) = New SqlParameter("@NroDiasCaduca", SqlDbType.Int)
        arParms(3).Value = intNroDiasCaduca
        arParms(4) = New SqlParameter("@COD_PGTSECR", SqlDbType.Int)
        arParms(4).Value = strPregSegu
        arParms(5) = New SqlParameter("@RPT_SECR ", SqlDbType.VarChar, 200)
        arParms(5).Value = strRespSecre
        arParms(6) = New SqlParameter("@Mensaje", SqlDbType.Bit)
        arParms(6).Direction = ParameterDirection.Output
        Result = SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Upd_CambiarPassword", arParms)
        Return arParms(6).Value
    End Function
    Public Function gCambiarContrase�aMobil(ByVal strIdUsuario As String, ByVal strPassAnterior As String,
                                           ByVal strPassNuevo As String, ByVal intNroDiasCaduca As Int16) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@PassAnterior", SqlDbType.VarChar, 50)
        arParms(1).Value = strPassAnterior
        arParms(2) = New SqlParameter("@PassNuevo", SqlDbType.VarChar, 50)
        arParms(2).Value = strPassNuevo
        arParms(3) = New SqlParameter("@NroDiasCaduca", SqlDbType.Int)
        arParms(3).Value = intNroDiasCaduca
        'arParms(4) = New SqlParameter("@COD_PGTSECR", SqlDbType.Int)
        'arParms(4).Value = strPregSegu
        'arParms(5) = New SqlParameter("@RPT_SECR ", SqlDbType.VarChar, 200)
        'arParms(5).Value = strRespSecre
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Mobil_Upd_CambiarPassword", arParms).Tables(0)

    End Function

    Public Function gDesencryptarSQLMobil(ByVal strVar1 As String) As DataTable
        'Dim Result As Integer
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@LoginEncode", SqlDbType.VarChar, 500)
        arParms(0).Value = strVar1
        'arParms(1) = New SqlParameter("@PasswordEncode", SqlDbType.VarChar, 500)
        'arParms(1).Value = strVar2
        arParms(1) = New SqlParameter("@Result", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        'Result = SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_ValidaUsuario_InfoMobil_Encryp", arParms)
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Mobil_Sel_ValidaUsuario_Info_Encryp", arParms).Tables(0)
        Return arParms(1).Value
    End Function







    Public Function gDesencryptarSQL(ByVal strIdUsuario As String, ByVal strPassAnterior As String,
                                           ByVal strPassNuevo As String, ByVal intNroDiasCaduca As Int16) As Boolean
        Dim Result As Integer
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@PassAnterior", SqlDbType.VarChar, 50)
        arParms(1).Value = strPassAnterior
        arParms(2) = New SqlParameter("@PassNuevo", SqlDbType.VarChar, 50)
        arParms(2).Value = strPassNuevo
        arParms(3) = New SqlParameter("@NroDiasCaduca", SqlDbType.Int)
        arParms(3).Value = intNroDiasCaduca
        'arParms(4) = New SqlParameter("@COD_PGTSECR", SqlDbType.Int)
        'arParms(4).Value = strPregSegu
        'arParms(5) = New SqlParameter("@RPT_SECR ", SqlDbType.VarChar, 200)
        'arParms(5).Value = strRespSecre
        arParms(4) = New SqlParameter("@Mensaje", SqlDbType.Bit)
        arParms(4).Direction = ParameterDirection.Output
        Result = SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Upd_CambiarPasswordMobil", arParms)
        Return arParms(4).Value
    End Function

    Public Function gGetCaducaContrasena(ByVal strIdUsuario As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@Result", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_EstadoContrasena", arParms)
        Return arParms(1).Value
    End Function

    Public Function gCADListarPreguntaSeguridad() As DataTable
        Dim query As String
        query = "SELECT COD_PGTSECR, DSC_PGTSECR FROM WFPREGUNTASECRETA ORDER BY DSC_PGTSECR ASC"
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.Text, query).Tables(0)
    End Function

    Public Function gCADListarConfiguracionSeguridad(ByVal Usuario As String) As DataTable
        Dim query As String
        query = "SELECT ISNULL(COD_PGTSECR,0) AS COD_PGTSECR, ISNULL(RPT_SECR,'')AS RPT_SECR FROM WFUSUARIO WHERE USR_ACCE = '" & Usuario & "'"
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.Text, query).Tables(0)
    End Function

    Public Function gCADValidarUsuario(ByVal usuario As String) As DataTable
        Dim query As String
        query = "select USR_ACCE from WFUSUARIO where USR_ACCE = '" & usuario & "'"
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.Text, query).Tables(0)
    End Function

    Public Function gCADListarRespuestaSecreta(ByVal usuario As String) As DataTable
        Dim query As String
        query = "SELECT WP.DSC_PGTSECR, WU.RPT_SECR, WU.DIR_EMAI, WU.COD_USER, CASE ISNULL(WU.USR_PASFIRM,'') WHEN '' THEN 'FALSE' ELSE 'TRUE' END EXISTE_USR_PASFIRM FROM WFUSUARIO WU INNER JOIN WFPREGUNTASECRETA WP ON WP.COD_PGTSECR=WU.COD_PGTSECR WHERE  USR_ACCE = '" & usuario & "'"
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.Text, query).Tables(0)
    End Function

    '======= Req Encuesta   ===========
    Public Function sCADGrabarEncuesta(ByVal CodEncu As String, ByVal codUser As String, ByVal CodClie As String, ByVal CodTipEnti As String, ByVal strRes1 As String, ByVal strRes2 As String, ByVal strRes3 As String, ByVal strRes4 As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@COD_ENCU", SqlDbType.Int)
        arParms(0).Value = CodEncu
        arParms(1) = New SqlParameter("@COD_USER", SqlDbType.Char, 4)
        arParms(1).Value = codUser
        arParms(2) = New SqlParameter("@COD_CLIE", SqlDbType.VarChar, 20)
        arParms(2).Value = CodClie
        arParms(3) = New SqlParameter("@COD_TIPENTI", SqlDbType.Char, 2)
        arParms(3).Value = CodTipEnti
        arParms(4) = New SqlParameter("@RES_PREG01", SqlDbType.VarChar, 2)
        arParms(4).Value = strRes1
        arParms(5) = New SqlParameter("@RES_PREG02", SqlDbType.VarChar, 2)
        arParms(5).Value = strRes2
        arParms(6) = New SqlParameter("@RES_PREG03", SqlDbType.VarChar, 2)
        arParms(6).Value = strRes3
        arParms(7) = New SqlParameter("@RES_PREG04", SqlDbType.VarChar, 500)
        arParms(7).Value = strRes4
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_WFENCUESTA_INS_ENCUESTA", arParms).Tables(0)
    End Function

    Public Function gCADValidaEncuesta(ByVal codUser As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@COD_USER", SqlDbType.Char, 4)
        arParms(0).Value = codUser
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_WFENCUESTA_SEL_ValidaEncuesta", arParms).Tables(0)
    End Function

    '======= Fin Req Encuesta   ===========
    Public Function gCADUpdFueraOficina(ByVal strCodUsua As String, ByVal strFechIni As String, _
                                        ByVal strFechFin As String, ByVal strHoraIni As String, _
                                        ByVal strHoraFin As String, ByVal strCome As String, _
                                        ByVal strFlgEstado As Boolean) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@COD_USER", SqlDbType.VarChar, 5)
        arParms(0).Value = strCodUsua
        arParms(1) = New SqlParameter("@FCH_FOFI_INI", SqlDbType.VarChar, 10)
        arParms(1).Value = strFechIni
        arParms(2) = New SqlParameter("@FCH_FOFI_FIN", SqlDbType.VarChar, 10)
        arParms(2).Value = strFechFin
        arParms(3) = New SqlParameter("@HOR_FOFI_INI", SqlDbType.VarChar, 5)
        arParms(3).Value = strHoraIni
        arParms(4) = New SqlParameter("@HOR_FOFI_FIN", SqlDbType.VarChar, 5)
        arParms(4).Value = strHoraFin
        arParms(5) = New SqlParameter("@DES_FOFI", SqlDbType.VarChar, 500)
        arParms(5).Value = strCome
        arParms(6) = New SqlParameter("@FLG_FOFI_EST", SqlDbType.Bit)
        arParms(6).Value = strFlgEstado
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_WFUSUARIO_UPD_FueraOficina", arParms).Tables(0)
    End Function

    Public Function gCADSelFueraOficina(ByVal strCodUsua As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@COD_USER", SqlDbType.VarChar, 5)
        arParms(0).Value = strCodUsua
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_WFUSUARIO_SEL_FueraOficina", arParms).Tables(0)
    End Function
End Class
