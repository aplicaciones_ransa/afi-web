Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Public Class Documento
    Inherits System.ComponentModel.Component
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private lstrResultado As String
    Private lstrTipoDocumento As String
    Private LstrNombrePDF As String
    Private lstrCodDocWar As String
#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Public Function gGetTipoDocumento() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerTipoDocumento").Tables(0)
    End Function

    Public Function gCADGetListarAlmacen(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
        arParms(1).Direction = ParameterDirection.Output
        arParms(2) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
        arParms(2).Direction = ParameterDirection.Output
        Return (SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarAlmacenxClientePropios", arParms).Tables(0))
    End Function

    Public Function gCADGetRetiroImporte(ByVal strCodCliente As String, ByVal strCodModalidad As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@CodModalidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodModalidad
        arParms(2) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
        arParms(2).Direction = ParameterDirection.Output
        arParms(3) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TTRETI_CLIE_ESPE_Sel_ClienteRetiroImporte", arParms)
        Return arParms(2).Value().ToString()
    End Function

    Public Function gGetBusquedaDocumentos(ByVal strNroWarrant As String, ByVal strFechaInicial As String,
                                        ByVal strFechaFinal As String, ByVal strIdEstado As String,
                                        ByVal strTipoDocumento As String, ByVal strIdTipoEntidad As String,
                                        ByVal strIdSico As String, ByVal strDepositante As String,
                                        ByVal strFinanciador As String, ByVal strSituacion As String, ByVal strIdUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 8)
        arParms(1).Value = strFechaInicial.Trim
        arParms(2) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 8)
        arParms(2).Value = strFechaFinal
        arParms(3) = New SqlParameter("@IdEstado", SqlDbType.VarChar, 2)
        arParms(3).Value = strIdEstado.Trim
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(4).Value = strTipoDocumento.Trim
        arParms(5) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(5).Value = strIdTipoEntidad
        arParms(6) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(6).Value = strIdSico
        arParms(7) = New SqlParameter("@Depositante", SqlDbType.VarChar, 20)
        arParms(7).Value = strDepositante
        arParms(8) = New SqlParameter("@Financiador", SqlDbType.VarChar, 20)
        arParms(8).Value = strFinanciador
        arParms(9) = New SqlParameter("@Situacion", SqlDbType.Char, 1)
        arParms(9).Value = strSituacion
        arParms(10) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(10).Value = strIdUsuario

        Dim SVAR As String
        SVAR = "('" &
        arParms(0).Value & "','" &
        arParms(1).Value & "','" &
        arParms(2).Value & "','" &
        arParms(3).Value & "','" &
        arParms(4).Value & "','" &
        arParms(5).Value & "','" &
        arParms(6).Value & "','" &
        arParms(7).Value & "','" &
        arParms(8).Value & "','" &
        arParms(9).Value & "','" &
        arParms(10).Value & "')"

        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentos_V3", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentosV2(ByVal strNroWarrant As String, ByVal strFechaInicial As String,
                                        ByVal strFechaFinal As String, ByVal strIdEstado As String,
                                        ByVal strTipoDocumento As String, ByVal strIdTipoEntidad As String,
                                        ByVal strIdSico As String, ByVal strDepositante As String,
                                        ByVal strFinanciador As String, ByVal strSituacion As String, ByVal strIdUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 8)
        arParms(1).Value = strFechaInicial.Trim
        arParms(2) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 8)
        arParms(2).Value = strFechaFinal
        arParms(3) = New SqlParameter("@IdEstado", SqlDbType.VarChar, 2)
        arParms(3).Value = strIdEstado.Trim
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(4).Value = strTipoDocumento.Trim
        arParms(5) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(5).Value = strIdTipoEntidad
        arParms(6) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(6).Value = strIdSico
        arParms(7) = New SqlParameter("@Depositante", SqlDbType.VarChar, 20)
        arParms(7).Value = strDepositante
        arParms(8) = New SqlParameter("@Financiador", SqlDbType.VarChar, 20)
        arParms(8).Value = strFinanciador
        arParms(9) = New SqlParameter("@Situacion", SqlDbType.Char, 1)
        arParms(9).Value = strSituacion
        arParms(10) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(10).Value = strIdUsuario

        Dim SVAR As String
        SVAR = "('" &
        arParms(0).Value & "','" &
        arParms(1).Value & "','" &
        arParms(2).Value & "','" &
        arParms(3).Value & "','" &
        arParms(4).Value & "','" &
        arParms(5).Value & "','" &
        arParms(6).Value & "','" &
        arParms(7).Value & "','" &
        arParms(8).Value & "','" &
        arParms(9).Value & "','" &
        arParms(10).Value & "')"

        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentos_V4", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentosMobil(ByVal strTipoConsu As String, ByVal strNroWarrant As String, ByVal strTipoDocumento As String, ByVal strIdTipoEntidad As String,
                                        ByVal strIdSico As String, ByVal strIdUsuario As String, ByVal strFlgPendiente As String, ByVal strCodEmpr As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@TipoConsulta", SqlDbType.Char, 1)
        arParms(0).Value = strTipoConsu.Trim
        arParms(1) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(1).Value = strNroWarrant.Trim
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(2).Value = strTipoDocumento.Trim
        arParms(3) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoEntidad
        arParms(4) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(4).Value = strIdSico
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@FlgPendiente", SqlDbType.Char, 1)
        arParms(6).Value = strFlgPendiente
        arParms(7) = New SqlParameter("@CO_EMPR", SqlDbType.Char, 2)
        arParms(7).Value = strCodEmpr
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Mobil_Sel_BuscarDocumentos", arParms).Tables(0)
    End Function


    Public Function gGetBusquedaDocumentosSolicitudWarrants(ByVal strNroWarrant As String, ByVal strFechaInicial As String,
                                    ByVal strFechaFinal As String, ByVal strDepositante As String, ByVal strFinanciador As String,
                                    ByVal strIdUsuario As String, ByVal bolPendientes As Boolean) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 8)
        arParms(1).Value = strFechaInicial.Trim
        arParms(2) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 8)
        arParms(2).Value = strFechaFinal
        arParms(3) = New SqlParameter("@Depositante", SqlDbType.VarChar, 20)
        arParms(3).Value = strDepositante
        arParms(4) = New SqlParameter("@Financiador", SqlDbType.VarChar, 20)
        arParms(4).Value = strFinanciador
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@Pendientes", SqlDbType.Bit)
        arParms(6).Value = bolPendientes
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentos_SolicitudWarrants", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentosSolicitador(ByVal strNSico As String, ByVal strNDepositante As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@str_nclie", SqlDbType.VarChar, 200)
        arParms(0).Value = strNSico
        arParms(1) = New SqlParameter("@str_nloca", SqlDbType.VarChar, 200)
        arParms(1).Value = strNDepositante
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFSTOCK_LISTAR_SOLICITUD_WARRANTS", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentosInformesTecnicos(ByVal strFechaInicial As String, ByVal strFechaFinal As String,
                                                       ByVal strIdSico As String, ByVal strNroVapor As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 10)
        arParms(0).Value = strFechaInicial.Trim
        arParms(1) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 10)
        arParms(1).Value = strFechaFinal
        arParms(2) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(2).Value = strIdSico
        arParms(3) = New SqlParameter("@NroVapor", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroVapor
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentosInformesTecnicos", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentosInformes(ByVal strFechaInicial As String, ByVal strFechaFinal As String,
                                                       ByVal strIdSico As String, ByVal strNroVapor As String, ByVal tipo As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 10)
        arParms(0).Value = strFechaInicial.Trim
        arParms(1) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 10)
        arParms(1).Value = strFechaFinal
        arParms(2) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(2).Value = strIdSico
        arParms(3) = New SqlParameter("@NroVapor", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroVapor
        arParms(4) = New SqlParameter("@Tipo", SqlDbType.Char, 1)
        arParms(4).Value = tipo
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentosInformes", arParms).Tables(0)
    End Function

    Public Function GetIdsDocumentos(ByVal strNroDoc As String, ByVal objTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@COD_DOCU", SqlDbType.Int)
        arParms(0).Value = CInt(strNroDoc)
        Return SqlHelper.ExecuteDataset(objTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerIdsDocumentos", arParms).Tables(0)
    End Function

    Public Function cerrarLiberacionContable(ByVal nro_sec_lib_contable As Integer, ByVal nro_libe_contable As String) As String
        'FUNCION AGREGADA POR RICHARD PEREZ CENTENO
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdLibContable", SqlDbType.Int)
        arParms(0).Value = nro_sec_lib_contable
        arParms(1) = New SqlParameter("@NroLibContable", SqlDbType.VarChar, 20)
        arParms(1).Value = nro_libe_contable
        arParms(2) = New SqlParameter("@Resp", SqlDbType.VarChar, 50)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_CerrarLiberacionContable", arParms)
        Return arParms(2).Value().ToString()
    End Function

    Public Function gGetBusquedaDocumentosCreados(ByVal strNroWarrant As String, ByVal strTipoDocumento As String,
                                        ByVal strIdTipoEntidad As String, ByVal strIdSico As String, ByVal strIdUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(1).Value = strTipoDocumento.Trim
        arParms(2) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoEntidad
        arParms(3) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(3).Value = strIdSico
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(4).Value = strIdUsuario
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarDocumentosCreados", arParms).Tables(0)
    End Function

    Public Function gGetDocumentosProrroga(ByVal strNroWarrant As String, ByVal strIdEndosatario As String,
                                            ByVal strIdSico As String, ByVal strTipoEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@co_clie", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdSico
        arParms(1) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(1).Value = strNroWarrant.Trim
        arParms(2) = New SqlParameter("@IdEndosatario", SqlDbType.VarChar, 20)
        arParms(2).Value = strIdEndosatario
        arParms(3) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(3).Value = strTipoEntidad
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "SP_LISTA_WARRANT_PARA_PROR", arParms).Tables(0)
    End Function

    Public Function gGetWarrantxVencer(ByVal strNroWarrant As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CO_ENTI_FINA", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdSico
        arParms(1) = New SqlParameter("@NU_TITU", SqlDbType.VarChar, 8)
        arParms(1).Value = strNroWarrant.Trim
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_REPORTE_VENC_WARR", arParms).Tables(0)
    End Function

    Public Function gGetWarrantLiberaciones(ByVal strNroWarrant As String, ByVal strIdEndosatario As String,
                                            ByVal strTipoMercaderia As String, ByVal strDescripcion As String,
                                            ByVal strIdCliente As String, ByVal strEmbarque As String,
                                            ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@sNU_TITU", SqlDbType.VarChar, 20)
        arParms(0).Value = strNroWarrant
        arParms(1) = New SqlParameter("@IdEndosatario", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdEndosatario
        arParms(2) = New SqlParameter("@TipoMercaderia", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipoMercaderia
        arParms(3) = New SqlParameter("@DscMercaderia", SqlDbType.VarChar, 200)
        arParms(3).Value = strDescripcion
        arParms(4) = New SqlParameter("@sCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(4).Value = strIdCliente
        arParms(5) = New SqlParameter("@sTIP_LIBE", SqlDbType.Char, 1)
        arParms(5).Value = strEmbarque
        arParms(6) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 5)
        arParms(6).Value = strCodAlmacen
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "usp_fn_CONS_WARR_V3", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaProrroga(ByVal strNroWarrant As String, ByVal strIdEstado As String,
                                        ByVal strIdTipoEntidad As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@IdEstado", SqlDbType.Char, 2)
        arParms(1).Value = strIdEstado
        arParms(2) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoEntidad
        arParms(3) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(3).Value = strIdSico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFPRORROGA_Sel_BuscarProrroga", arParms).Tables(0)
    End Function

    Public Function gEliminarProrroga(ByVal strNroWarrant As String, ByVal strSecuProrr As String,
                                       ByVal strCodTipoDoc As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@SecuProrr", SqlDbType.Char, 9)
        arParms(1).Value = strSecuProrr
        arParms(2) = New SqlParameter("@CodTipoDoc", SqlDbType.VarChar, 2)
        arParms(2).Value = strCodTipoDoc
        arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 50)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFPRORROGA_Del_EliminaProrroga", arParms)
        Return arParms(3).Value().ToString()
    End Function

    Public Function gGetBusquedaLiberacion(ByVal strNroWarrant As String, ByVal strIdEstado As String,
                                        ByVal strLiberacion As String,
                                        ByVal strIdTipoEntidad As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@IdEstado", SqlDbType.Char, 2)
        arParms(1).Value = strIdEstado
        arParms(2) = New SqlParameter("@Liberacion", SqlDbType.VarChar, 11)
        arParms(2).Value = strLiberacion
        arParms(3) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoEntidad
        arParms(4) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(4).Value = strIdSico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_BuscarLiberacion", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaLiberacionPendiente(ByVal strNroWarrant As String, ByVal strLiberacion As String,
                                                    ByVal strIdTipoEntidad As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@Liberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strLiberacion
        arParms(2) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoEntidad
        arParms(3) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(3).Value = strIdSico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_BuscarLiberacionPendiente", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaLiberacionPendienteWarCustodia(ByVal strNroWarrant As String, ByVal strLiberacion As String,
                                                ByVal strIdTipoEntidad As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@Liberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strLiberacion
        arParms(2) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoEntidad
        arParms(3) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(3).Value = strIdSico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_BuscarLiberacionPendienteWarCustodia", arParms).Tables(0)
    End Function

    Public Function gGetLiberacionWarCustodia(ByVal strNroWarrant As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_LiberacionWarCustodia", arParms).Tables(0)
    End Function

    Public Function gGetRetiros(ByVal strNroDoc As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroDoc.Trim
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_LiberacionWarCustodia", arParms).Tables(0)
    End Function

    Public Function gUpdProrroga(ByVal strNroWarrant As String, ByVal strSecuencia As String,
                                ByVal strIdTipoDocumento As String, ByVal strFchRegistro As String,
                                ByVal strFchProrroga As String, ByVal strSaldoAdecuado As String,
                                ByVal strSaldoActual As String, ByVal strMonedaAdecuado As String,
                                ByVal strMonedaActual As String, ByVal strIdUsuario As String,
                                ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@Secuencia", SqlDbType.VarChar, 9)
        arParms(1).Value = strSecuencia
        arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocumento
        arParms(3) = New SqlParameter("@FchRegistro", SqlDbType.VarChar, 8)
        arParms(3).Value = strFchRegistro
        arParms(4) = New SqlParameter("@FchProrroga", SqlDbType.VarChar, 8)
        arParms(4).Value = strFchProrroga
        arParms(5) = New SqlParameter("@SaldoAdecuado", SqlDbType.VarChar, 20)
        arParms(5).Value = strSaldoAdecuado
        arParms(6) = New SqlParameter("@SaldoActual", SqlDbType.VarChar, 20)
        arParms(6).Value = strSaldoActual
        arParms(7) = New SqlParameter("@MonedaAdecuado", SqlDbType.Int)
        arParms(7).Value = strMonedaAdecuado
        arParms(8) = New SqlParameter("@MonedaActual", SqlDbType.Int)
        arParms(8).Value = strMonedaActual
        arParms(9) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(9).Value = strIdUsuario
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFPRORROGA_Upd_ActualizarProrroga", arParms).Tables(0)
    End Function

    Public Function gGetDataDocumento(ByVal strNroWarrant As String, ByVal strNroLiberacion As String,
                                       ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String, ByVal strEmpresa As String) As DataRow
        Dim Result As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLiberacion.Trim
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@CO_EMPR", SqlDbType.Char, 2)
        arParms(4).Value = strEmpresa

        Dim SVAR As String
        SVAR = "('" &
        arParms(0).Value & "','" &
        arParms(1).Value & "','" &
        arParms(2).Value & "','" &
        arParms(3).Value & "','" &
        arParms(4).Value & "')"

        Result = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerDataDocumentos", arParms).Tables(0)
        If Result.Rows.Count > 0 Then
            Return Result.Rows(0)
        End If
        Result.Dispose()
        Result = Nothing
    End Function

    Public Function gGetDataDocumentoMobil(ByVal strNroWarrant As String, ByVal strNroLiberacion As String,
                                       ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String, ByVal strEmpresa As String) As DataTable
        Dim Result As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLiberacion.Trim
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@CO_EMPR", SqlDbType.Char, 2)
        arParms(4).Value = strEmpresa 'codigo de empresa  logueada 

        Result = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Mobil_Sel_CabeDocumento", arParms).Tables(0)
        If Result.Rows.Count > 0 Then
            Return Result
        End If
        Result.Dispose()
        Result = Nothing

    End Function

    Public Function gGetDataDocumentoRetiro(ByVal strNroWarrant As String, ByVal strNroLiberacion As String,
                                   ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                   ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLiberacion.Trim
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerDataDocumentos", arParms).Tables(0)
    End Function

    Public Function gGetDataProrroga(ByVal strNroWarrant As String, ByVal strNroSecuencia As String,
                                     ByVal strIdTipoDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroSecuencia", SqlDbType.Char, 9)
        arParms(1).Value = strNroSecuencia
        arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFPRORROGA_Sel_TraerDataProrroga", arParms).Tables(0)
    End Function

    Public Function gGetDetalleLiberacion(ByVal strEmpresa As String, ByVal strUnidad As String,
                                 ByVal strIdLiberacion As String, ByVal strTipoDocumento As String,
                                 ByVal strNroDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@Empresa", SqlDbType.Char, 2)
        arParms(0).Value = strEmpresa.Trim
        arParms(1) = New SqlParameter("@Unidad", SqlDbType.Char, 3)
        arParms(1).Value = strUnidad
        arParms(2) = New SqlParameter("@IdLiberacion", SqlDbType.VarChar, 11)
        arParms(2).Value = strIdLiberacion
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 3)
        arParms(3).Value = strTipoDocumento
        arParms(4) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(4).Value = strNroDoc
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_DetalleLiberacion", arParms).Tables(0)
    End Function

    Public Function gGetDetalleLiberacionWIP(ByVal strEmpresa As String, ByVal strUnidad As String,
                                      ByVal strTipoDocumento As String, ByVal strNroDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strEmpresa.Trim
        arParms(1) = New SqlParameter("@CO_UNID", SqlDbType.Char, 3)
        arParms(1).Value = strUnidad
        arParms(2) = New SqlParameter("@TI_DOCU_RETI", SqlDbType.Char, 3)
        arParms(2).Value = strTipoDocumento
        arParms(3) = New SqlParameter("@NU_DOCU_RETI ", SqlDbType.VarChar, 11)
        arParms(3).Value = strNroDoc
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_LiberacionWIP", arParms).Tables(0)
    End Function

    Public Function gGetDetalleLiberacionRegistra(ByVal strEmpresa As String, ByVal strUnidad As String,
                                     ByVal strIdLiberacion As String, ByVal strTipoDocumento As String,
                                     ByVal strNroDoc As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@Empresa", SqlDbType.Char, 2)
        arParms(0).Value = strEmpresa.Trim
        arParms(1) = New SqlParameter("@Unidad", SqlDbType.Char, 3)
        arParms(1).Value = strUnidad
        arParms(2) = New SqlParameter("@IdLiberacion", SqlDbType.VarChar, 11)
        arParms(2).Value = strIdLiberacion
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(3).Value = strTipoDocumento
        arParms(4) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(4).Value = strNroDoc
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFLIBERACION_Sel_DetalleLiberacion", arParms).Tables(0)
    End Function

    Public Function gGetLogDocumento(ByVal strNroWarrant As String, ByVal strNroLiberacion As String,
                                      ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                      ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLiberacion.Trim
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@IdTipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerLogDocumentos", arParms).Tables(0)
    End Function

    Public Function gGetWarrantEnlazados(ByVal strNroWarrant As String, ByVal strNroLiberacion As String,
                                      ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroWarrant.Trim
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLiberacion.Trim
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_WarrantEnlazados", arParms).Tables(0)
    End Function

    Public Function gGetAccesoControles(ByVal strTipoEntidad As String, ByVal strTipoUsuario As String,
                                    ByVal strNroDoc As String, ByVal strNroLib As String,
                                    ByVal strPeriodoAnual As String, ByVal strIdUsuario As String,
                                    ByVal strIdTipoDocumento As String, ByVal strIdSico As String,
                                    ByVal strVisualiza As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strTipoEntidad.Trim
        arParms(1) = New SqlParameter("@TipoUsuario", SqlDbType.Char, 2)
        arParms(1).Value = strTipoUsuario.Trim
        arParms(2) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(2).Value = strNroDoc
        arParms(3) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(3).Value = strNroLib
        arParms(4) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(4).Value = strPeriodoAnual
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(6).Value = strIdTipoDocumento
        arParms(7) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(7).Value = strIdSico
        arParms(8) = New SqlParameter("@Visualiza", SqlDbType.Char, 1)
        arParms(8).Value = strVisualiza

        'Dim strCadena As String = CapturaVariables(arParms)

        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFACCESOCONTROLES_Sel_AccesarControles", arParms).Tables(0)
    End Function

    Public Function gVerificarDocumentos(ByVal blnGeneraPDF As Boolean, ByVal strCodTipoEntidad As String,
                                    ByVal strIds As String, ByVal strCodUser As String) As DataTable
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@K_GENERA_PDF", SqlDbType.Bit)
        arParms(0).Value = blnGeneraPDF
        arParms(1) = New SqlParameter("@K_COD_TIPENTI", SqlDbType.Char, 2)
        arParms(1).Value = strCodTipoEntidad
        arParms(2) = New SqlParameter("@K_IdS", SqlDbType.VarChar, 1000)
        arParms(2).Value = strIds
        arParms(3) = New SqlParameter("@K_COD_USER", SqlDbType.Char, 4)
        arParms(3).Value = strCodUser
        arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(4).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ValidarVerificacion", arParms)
        lstrResultado = arParms(4).Value().ToString()
        Return ds.Tables(0)
    End Function

    Public Function gCerrarComprobante(ByVal strCodTipoDocumento As String, ByVal strNroDoc As String, ByVal strCodUser As String) As Boolean
        Try
            Dim arParms() As SqlParameter = New SqlParameter(3) {}
            arParms(0) = New SqlParameter("@ISTI_DOCU", SqlDbType.Char, 2)
            arParms(0).Value = strCodTipoDocumento
            arParms(1) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 8)
            arParms(1).Value = strNroDoc
            arParms(2) = New SqlParameter("@ISCO_USUA", SqlDbType.VarChar, 8)
            arParms(2).Value = IIf(strCodUser.Length > 0, Left(strCodUser, 8), strCodUser)
            arParms(3) = New SqlParameter("@VSDE_MENS", SqlDbType.VarChar, 400)
            arParms(3).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(strCadenaOfioper, CommandType.StoredProcedure, "SP_CIERRA_DCR", arParms)
            lstrResultado = arParms(3).Value().ToString()
            If lstrResultado = "" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Dim strert As String = ex.Message
        End Try
    End Function
    Public Function gBuscarSecuencia(ByVal strCodTipoDocumento As String, ByVal strNroDoc As String, strNroLib As String, ByVal strIdUsuario As String) As DataTable
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(1).Value = strCodTipoDocumento
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(3).Value = strIdUsuario
        ds = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarSecuencia", arParms)
        Return ds.Tables(0)
    End Function



    Public Function gUpdEntidadFinanciera(ByVal strEntidad As String, ByVal strNroDoc As String,
                                            ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                            ByVal strIdUsuario As String, ByVal strIdTipoDocumento As String,
                                            ByVal strAsociado As String, ByVal blnDobleEndoso As Boolean) As String
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.Char, 8)
        arParms(0).Value = strEntidad
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(4).Value = strIdUsuario
        arParms(5) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(5).Value = strIdTipoDocumento
        arParms(6) = New SqlParameter("@Asociado", SqlDbType.VarChar, 20)
        arParms(6).Value = strAsociado
        arParms(7) = New SqlParameter("@DobleEndoso", SqlDbType.Bit)
        arParms(7).Value = blnDobleEndoso
        arParms(8) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 100)
        arParms(8).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ActualizarEntidadFinanciera", arParms)
        Return arParms(8).Value
    End Function

    Public Function gUpdRechazarDocumento(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                            ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                            ByVal strIdTipoDocumento As String, ByVal ObjTrans As SqlTransaction) As String
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        arParms(5) = New SqlParameter("@Resultado", SqlDbType.Char, 1)
        arParms(5).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_RechazarDocumento", arParms)
        Return arParms(5).Value.ToString
    End Function

    Public Function gUpdAnularDocumento(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                          ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                          ByVal strIdTipoDocumento As String, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_AnularDocumento", arParms)
    End Function

    Public Function gEnviarMail(ByVal strNroDoc As String, ByVal strNroLib As String,
                                ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                 ByVal strFlgTodos As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.VarChar, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@FlgTodos", SqlDbType.Char, 1)
        arParms(4).Value = strFlgTodos
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_EnvioCorreo", arParms).Tables(0)
    End Function

    Public Function gEnviarMail(ByVal strNroDoc As String, ByVal strNroLib As String,
                               ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                ByVal strFlgTodos As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@FlgTodos", SqlDbType.Char, 1)
        arParms(4).Value = strFlgTodos
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_EnvioCorreo", arParms).Tables(0)
    End Function

    Public Function gInsDetalleLiberacion(ByVal strCodEmpresa As String, ByVal strCodUnidad As String,
                                ByVal strCodTipDoc As String, ByVal strNroLiberacion As String,
                                ByVal strNroItem As String, ByVal strCodAlmacen As String,
                                ByVal strUnidadBulto As String, ByVal strCntBulto As Decimal,
                                ByVal strDescItem As String, ByVal strUnidadItem As String,
                                ByVal strCntDisponible As Decimal, ByVal strCntRetirado As Decimal,
                                ByVal strCntRetiradoBulto As Decimal, ByVal strFchFactura As String,
                                ByVal strFchIngreso As String, ByVal strBodega As String,
                                ByVal strPrecioUnit As Decimal, ByVal strIdUsuario As String,
                                ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(18) {}
        arParms(0) = New SqlParameter("@CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@CodUnidad", SqlDbType.Char, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@CodTipDoc", SqlDbType.Char, 3)
        arParms(2).Value = strCodTipDoc
        arParms(3) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(3).Value = strNroLiberacion
        arParms(4) = New SqlParameter("@NroItem", SqlDbType.Int)
        arParms(4).Value = strNroItem
        arParms(5) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 5)
        arParms(5).Value = strCodAlmacen
        arParms(6) = New SqlParameter("@UnidadBulto", SqlDbType.VarChar, 3)
        arParms(6).Value = strUnidadBulto
        arParms(7) = New SqlParameter("@CntBulto", strCntBulto)
        arParms(8) = New SqlParameter("@DescItem", SqlDbType.VarChar, 200)
        arParms(8).Value = strDescItem
        arParms(9) = New SqlParameter("@UnidadItem", SqlDbType.VarChar, 3)
        arParms(9).Value = strUnidadItem
        arParms(10) = New SqlParameter("@CntDisponible", strCntDisponible)
        arParms(11) = New SqlParameter("@CntRetirado", strCntRetirado)
        arParms(12) = New SqlParameter("@CntRetiradoBulto", strCntRetiradoBulto)
        arParms(13) = New SqlParameter("@FchFactura", SqlDbType.VarChar, 10)
        arParms(13).Value = strFchFactura
        arParms(14) = New SqlParameter("@FchIngreso", SqlDbType.VarChar, 10)
        arParms(14).Value = strFchIngreso
        arParms(15) = New SqlParameter("@Bodega", SqlDbType.VarChar, 10)
        arParms(15).Value = strBodega
        arParms(16) = New SqlParameter("@PrecioUnit", strPrecioUnit)
        arParms(17) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(17).Value = strIdUsuario
        arParms(18) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(18).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFITEMLIBERACION_Ins_DetalleLiberacion", arParms)
        Return arParms(18).Value.ToString
    End Function

    Public Function gUpdDetalleLiberacionCustodia(ByVal strNumeroDoc As String, ByVal strNroLibe As String,
                                                                        ByVal strTipDoc As String, ByVal strIdUsuario As String,
                                                                        ByVal strCod_Unidad As String, ByVal strNroDocuDest As String,
                                                                        ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDocu", SqlDbType.VarChar, 8)
        arParms(0).Value = strNumeroDoc
        arParms(1) = New SqlParameter("@NroLibe", SqlDbType.Char, 11)
        arParms(1).Value = strNroLibe
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipDoc
        arParms(3) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(3).Value = strIdUsuario
        arParms(4) = New SqlParameter("@Cod_Unidad", SqlDbType.Char, 3)
        arParms(4).Value = strCod_Unidad
        arParms(5) = New SqlParameter("@NroDocuDest", SqlDbType.VarChar, 8)
        arParms(5).Value = strNroDocuDest
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFITEMLIBERACION_Upd_DetalleLiberacionCustodia", arParms)
    End Function

    Public Function gInsDetalleLiberacion_WarrantsCustodia(ByVal strCodEmpresa As String, ByVal strCodUnidad As String,
                               ByVal strCodTipDoc As String, ByVal strNroLiberacion As String,
                               ByVal strNroItem As String, ByVal strCodAlmacen As String,
                               ByVal strUnidadBulto As String, ByVal strCntBulto As Decimal,
                               ByVal strDescItem As String, ByVal strUnidadItem As String,
                               ByVal strCntDisponible As Decimal, ByVal strCntRetirado As Decimal,
                               ByVal strCntRetiradoBulto As Decimal, ByVal strFchFactura As String,
                               ByVal strFchIngreso As String, ByVal strBodega As String,
                               ByVal strPrecioUnit As Decimal, ByVal strIdUsuario As String,
                               ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(18) {}
        arParms(0) = New SqlParameter("@CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@CodUnidad", SqlDbType.Char, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@CodTipDoc", SqlDbType.Char, 3)
        arParms(2).Value = strCodTipDoc
        arParms(3) = New SqlParameter("@NroLiberacion", SqlDbType.VarChar, 11)
        arParms(3).Value = strNroLiberacion
        arParms(4) = New SqlParameter("@NroItem", SqlDbType.Int)
        arParms(4).Value = strNroItem
        arParms(5) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 5)
        arParms(5).Value = strCodAlmacen
        arParms(6) = New SqlParameter("@UnidadBulto", SqlDbType.VarChar, 3)
        arParms(6).Value = strUnidadBulto
        arParms(7) = New SqlParameter("@CntBulto", strCntBulto)
        arParms(8) = New SqlParameter("@DescItem", SqlDbType.VarChar, 200)
        arParms(8).Value = strDescItem
        arParms(9) = New SqlParameter("@UnidadItem", SqlDbType.VarChar, 3)
        arParms(9).Value = strUnidadItem
        arParms(10) = New SqlParameter("@CntDisponible", strCntDisponible)
        arParms(11) = New SqlParameter("@CntRetirado", strCntRetirado)
        arParms(12) = New SqlParameter("@CntRetiradoBulto", strCntRetiradoBulto)
        arParms(13) = New SqlParameter("@FchFactura", SqlDbType.VarChar, 10)
        arParms(13).Value = strFchFactura
        arParms(14) = New SqlParameter("@FchIngreso", SqlDbType.VarChar, 10)
        arParms(14).Value = strFchIngreso
        arParms(15) = New SqlParameter("@Bodega", SqlDbType.VarChar, 10)
        arParms(15).Value = strBodega
        arParms(16) = New SqlParameter("@PrecioUnit", strPrecioUnit)
        arParms(17) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(17).Value = strIdUsuario
        arParms(18) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(18).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFITEMLIBERACION_Ins_DetalleLiberacion_WarrantsCustodia", arParms)
        Return arParms(18).Value.ToString
    End Function

    Public Function gUpdDocumento(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                 ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                 ByVal strIdTipoDocumento As String, ByVal strPag As String,
                                 ByVal strIdSico As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        arParms(5) = New SqlParameter("@NumPag", SqlDbType.Int)
        arParms(5).Value = Convert.ToInt16(strPag)
        arParms(6) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(6).Value = strIdSico
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ActualizarDocumento", arParms).Tables(0)
    End Function

    Public Sub gUpdRechazarDocumentoRetiro(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                             ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                             ByVal strIdTipoDocumento As String)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        arParms(5) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(5).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_RechazarRetiro_Documento", arParms)
        lstrResultado = arParms(5).Value
    End Sub

    Public Function gGetValidarFirmantes(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                   ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                   ByVal strIdTipoDocumento As String, ByVal strIdSico As String,
                                   ByVal ObjTrans As SqlTransaction) As Integer
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(3).Value = strPeriodoAnual
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        arParms(5) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(5).Value = strIdSico
        arParms(6) = New SqlParameter("@Mensaje", SqlDbType.Int)
        arParms(6).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFUSRXFIRMAR_Sel_ValidarFirmantes", arParms)
        Return arParms(6).Value
    End Function

    Public Function gInsLogDocumento(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                    ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                    ByVal strEvento As String, ByVal strIdTipoDocumento As String,
                                    ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@NroLib", SqlDbType.VarChar, 11)
        arParms(2).Value = strNroLib
        arParms(3) = New SqlParameter("@Evento", SqlDbType.Char, 1)
        arParms(3).Value = strEvento
        arParms(4) = New SqlParameter("@PeriodoAnual", SqlDbType.VarChar, 4)
        arParms(4).Value = strPeriodoAnual
        arParms(5) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(5).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFLOGDOCUMENTO_Ins_LogDocumento", arParms).Tables(0)
    End Function

    Public Function BLGetDatosFirmantes(ByVal strNroDoc As String, ByVal strNroLib As String, ByVal strPerAnual As String,
                                    ByVal strTipDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@STRNUM_DOC", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@STRNROLIB", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@STRPERANUAL", SqlDbType.Char, 4)
        arParms(2).Value = strPerAnual
        arParms(3) = New SqlParameter("@STRTIPDOC", SqlDbType.Char, 2)
        arParms(3).Value = strTipDoc
        arParms(4) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(4).Direction = ParameterDirection.Output
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ListarFirmantes", arParms).Tables(0)
        Return arParms(1).Value
    End Function

    Public Function gUpdEndoso(ByVal strOperacion As String, ByVal strMoneda As String,
                                   ByVal strVencimiento As String, ByVal strValor As String,
                                   ByVal strTasa As String, ByVal strIdUsuario As String,
                                   ByVal strNroDoc As String, ByVal strNroLib As String,
                                   ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                   ByVal strBanco As String, ByVal strOficina As String,
                                   ByVal strNroCuenta As String, ByVal strDC As String,
                                   ByVal strLugar As String, ByVal blnEmbarque As Boolean,
                                   ByVal strNroGarantia As String, ByVal blnProtesto As Boolean, ByVal strflgEnviar As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(19) {}
        arParms(0) = New SqlParameter("@Operacion", SqlDbType.VarChar, 200)
        arParms(0).Value = strOperacion
        arParms(1) = New SqlParameter("@Moneda", SqlDbType.Int)
        arParms(1).Value = strMoneda
        arParms(2) = New SqlParameter("@Vencimiento", SqlDbType.VarChar, 10)
        arParms(2).Value = strVencimiento
        arParms(3) = New SqlParameter("@Valor", SqlDbType.VarChar, 30)
        arParms(3).Value = strValor
        arParms(4) = New SqlParameter("@Tasa", SqlDbType.VarChar, 150)
        arParms(4).Value = strTasa
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(6).Value = strNroDoc
        arParms(7) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(7).Value = strNroLib
        arParms(8) = New SqlParameter("@PeriodoAnual", SqlDbType.Char, 4)
        arParms(8).Value = strPeriodoAnual
        arParms(9) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(9).Value = strIdTipoDocumento
        arParms(10) = New SqlParameter("@Banco", SqlDbType.VarChar, 100)
        arParms(10).Value = strBanco
        arParms(11) = New SqlParameter("@Oficina", SqlDbType.VarChar, 150)
        arParms(11).Value = strOficina
        arParms(12) = New SqlParameter("@NroCuenta", SqlDbType.VarChar, 30)
        arParms(12).Value = strNroCuenta
        arParms(13) = New SqlParameter("@DC", SqlDbType.VarChar, 30)
        arParms(13).Value = strDC
        arParms(14) = New SqlParameter("@Lugar", SqlDbType.VarChar, 100)
        arParms(14).Value = strLugar
        arParms(15) = New SqlParameter("@Embarque", SqlDbType.Bit)
        arParms(15).Value = blnEmbarque
        arParms(16) = New SqlParameter("@NroGarantia", SqlDbType.VarChar, 20)
        arParms(16).Value = strNroGarantia
        arParms(17) = New SqlParameter("@Protesto", SqlDbType.Bit)
        arParms(17).Value = blnProtesto
        arParms(18) = New SqlParameter("@flg_enviar", SqlDbType.VarChar, 3)
        arParms(18).Value = strflgEnviar
        arParms(19) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(19).Direction = ParameterDirection.Output
        SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ActualizarEndoso", arParms)
        Return arParms(19).Value.ToString
    End Function

    Public Function gRegistraPDF(ByVal strIdUsuario As String, ByVal strNroDoc As String,
                                 ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                 ByVal imgArch1 As Byte(), ByVal strIdTipoDocumento As String)
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@Doc1", SqlDbType.Image)
        arParms(3).Value = imgArch1
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ArchivoPDF", arParms)
    End Function

    Public Function gSelArchivoPDF(ByVal strNroDoc As String, ByVal strNroLib As String,
                               ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                               ByVal intPagina As Integer) As DataRow
        Dim dt As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@Pagina", SqlDbType.Int, 1)
        arParms(4).Value = intPagina
        dt = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ArchivoPDF", arParms).Tables(0)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)
        Else
            Return Nothing
        End If
        dt.Dispose()
        dt = Nothing
    End Function

    Public Function gCADGelArchivoPDF(ByVal strNroDocu As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 8)
        arParms(0).Value = strNroDocu
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Inventario", arParms).Tables(0)
    End Function

    Public Function gRegistraArchivoFirmado(ByVal strNroDoc As String, ByVal strNroLib As String,
                                           ByVal strPeriodoAnual As String, ByVal imgArch3 As Byte(),
                                           ByVal strIdTipoDocumento As String, ByVal strPag As String,
                                           ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@Doc3", SqlDbType.Image)
        arParms(3).Value = imgArch3
        arParms(4) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(4).Value = strIdTipoDocumento
        arParms(5) = New SqlParameter("@Pagina", SqlDbType.Char, 1)
        arParms(5).Value = strPag
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ArchivoFirmado", arParms)
    End Function

    Public Function gGenerarDataProrroga(ByVal strNroDoc As String, ByVal strNroLib As String,
                                ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerDataProrrogaFolio", arParms).Tables(0)
    End Function

    Public Function gGenerarDataLiberacion(ByVal strNroDoc As String, ByVal strNroLib As String,
                                ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerFirmasLiberacion", arParms).Tables(0)
    End Function

    Public Function GetFirmantesLiberacion(ByVal strCodSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@COD_SICO", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodSico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_TraerFirmasCliente", arParms).Tables(0)
    End Function

    Public Function gGrabaContenidoWarrantMultiple(ByVal strNroDoc As String, ByVal imgArch1 As Byte(), ByVal blnFirmas As Boolean, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strNroDoc)
        arParms(1) = New SqlParameter("@Doc1", SqlDbType.Image)
        arParms(1).Value = imgArch1
        arParms(2) = New SqlParameter("@Firmas", SqlDbType.Bit)
        arParms(2).Value = blnFirmas
        arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_WarrantMultiple", arParms)
        lstrResultado = arParms(3).Value().ToString()
    End Function


    Public Function InsWarrantMultiple(ByVal strCod_Doc As String, ByVal strUsuario As String, ByVal strNroWarrants As String, ByVal strDblEndoso As String, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.VarChar, 200)
        arParms(0).Value = strCod_Doc
        arParms(1) = New SqlParameter("@CodUsuario", SqlDbType.Char, 4)
        arParms(1).Value = strUsuario
        arParms(2) = New SqlParameter("@Nro_Wars", SqlDbType.VarChar, 180)
        arParms(2).Value = strNroWarrants
        arParms(3) = New SqlParameter("@DblEndoso", SqlDbType.Char, 1)
        arParms(3).Value = strDblEndoso
        arParms(4) = New SqlParameter("@CodWarMultiple", SqlDbType.Int)
        arParms(4).Direction = ParameterDirection.Output
        arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(5).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_WarrantMultiple", arParms)
        lstrCodDocWar = arParms(4).Value().ToString()
        lstrResultado = arParms(5).Value().ToString()
    End Function

    Public Function InsCartaCompromiso(ByVal strNroDoc As String, ByVal strCodDepositante As String,
                                        ByVal strCodFinanciador As String, ByVal strCodAlmacen As String,
                                        ByVal byContenido As Byte(), ByVal strIdUsuarioCrea As String,
                                        ByVal strMoneda As String, ByVal strValor As String,
                                        ByVal strDestinatario As String) As String
        'Dim dt As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.VarChar, 200)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodDepositante
        arParms(2) = New SqlParameter("@CodFinanciador", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodFinanciador
        arParms(3) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 6)
        arParms(3).Value = strCodAlmacen
        arParms(4) = New SqlParameter("@Contenido", SqlDbType.Image)
        arParms(4).Value = byContenido
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 8)
        arParms(5).Value = strIdUsuarioCrea
        arParms(6) = New SqlParameter("@Moneda", SqlDbType.Int)
        arParms(6).Value = strMoneda
        arParms(7) = New SqlParameter("@Valor", SqlDbType.VarChar, 30)
        arParms(7).Value = strValor
        arParms(8) = New SqlParameter("@Destinatario", SqlDbType.VarChar, 200)
        arParms(8).Value = strDestinatario
        arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 2000)
        arParms(9).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_CartaCompromiso", arParms)
        Return arParms(9).Value().ToString()
        'Return strResultado
    End Function

    Public Function InsSolicitudWarrants(ByVal strCodDepositante As String, ByVal strCodAlmacen As String,
                                    ByVal strCodFinanciador As String, ByVal strIdUsuarioCrea As String,
                                    ByVal byContenido As Byte(), ByVal strImporteTotal As Decimal,
                                    ByVal strMoneda As String, ByVal strFechaSolicitud As String,
                                    ByVal strHoraSolicitud As String, ByVal strCodUnidad As String,
                                    ByVal strNroComprobante As String, ByVal strTipoTitulo As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        Dim dsResult As DataSet
        arParms(0) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodDepositante
        arParms(1) = New SqlParameter("@strAlmacen", SqlDbType.VarChar, 500)
        arParms(1).Value = strCodAlmacen
        arParms(2) = New SqlParameter("@CodFinanciador", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodFinanciador
        arParms(3) = New SqlParameter("@Contenido", SqlDbType.Image)
        arParms(3).Value = byContenido
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 8)
        arParms(4).Value = strIdUsuarioCrea
        arParms(5) = New SqlParameter("@strImpTot", SqlDbType.Decimal, 15, 6)
        arParms(5).Value = strImporteTotal
        arParms(6) = New SqlParameter("@strMoneda", SqlDbType.VarChar, 3)
        arParms(6).Value = strMoneda
        arParms(7) = New SqlParameter("@FechaSolicitud", SqlDbType.VarChar, 10)
        arParms(7).Value = strFechaSolicitud
        arParms(8) = New SqlParameter("@HoraSolicitud", SqlDbType.Char, 5)
        arParms(8).Value = strHoraSolicitud
        arParms(9) = New SqlParameter("@CodUnidad", SqlDbType.Char, 3)
        arParms(9).Value = strCodUnidad
        arParms(10) = New SqlParameter("@NroComprobante", SqlDbType.VarChar, 20)
        arParms(10).Value = strNroComprobante
        arParms(11) = New SqlParameter("@TipoTitulo", SqlDbType.VarChar, 3)
        arParms(11).Value = strTipoTitulo
        arParms(12) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 9)
        arParms(12).Direction = ParameterDirection.Output
        dsResult = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SolicitudWarrants_old", arParms)
        lstrResultado = arParms(12).Value().ToString()
        Return dsResult.Tables(0)
    End Function

    Public Function InsSolicitudWarrants(ByVal strCodDepositante As String, ByVal strCodAlmacen As String,
                                   ByVal strCodFinanciador As String, ByVal strIdUsuarioCrea As String,
                                   ByVal byContenido As Byte(), ByVal strImporteTotal As Decimal,
                                   ByVal strMoneda As String, ByVal strFechaSolicitud As String,
                                   ByVal strHoraSolicitud As String, ByVal strCodUnidad As String,
                                   ByVal strNroComprobante As String, ByVal strTipoTitulo As String,
                                   ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        Dim dsResult As DataSet
        arParms(0) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodDepositante
        arParms(1) = New SqlParameter("@strAlmacen", SqlDbType.VarChar, 500)
        arParms(1).Value = strCodAlmacen
        arParms(2) = New SqlParameter("@CodFinanciador", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodFinanciador
        arParms(3) = New SqlParameter("@Contenido", SqlDbType.Image)
        arParms(3).Value = byContenido
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 8)
        arParms(4).Value = strIdUsuarioCrea
        arParms(5) = New SqlParameter("@strImpTot", SqlDbType.Decimal, 15, 6)
        arParms(5).Value = strImporteTotal
        arParms(6) = New SqlParameter("@strMoneda", SqlDbType.VarChar, 3)
        arParms(6).Value = strMoneda
        arParms(7) = New SqlParameter("@FechaSolicitud", SqlDbType.VarChar, 10)
        arParms(7).Value = strFechaSolicitud
        arParms(8) = New SqlParameter("@HoraSolicitud", SqlDbType.Char, 5)
        arParms(8).Value = strHoraSolicitud
        arParms(9) = New SqlParameter("@CodUnidad", SqlDbType.Char, 3)
        arParms(9).Value = strCodUnidad
        arParms(10) = New SqlParameter("@NroComprobante", SqlDbType.VarChar, 20)
        arParms(10).Value = strNroComprobante
        arParms(11) = New SqlParameter("@TipoTitulo", SqlDbType.VarChar, 3)
        arParms(11).Value = strTipoTitulo
        arParms(12) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 9)
        arParms(12).Direction = ParameterDirection.Output
        dsResult = SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SolicitudWarrants_old", arParms)
        lstrResultado = arParms(12).Value().ToString()
        Return dsResult.Tables(0)
    End Function

    Public Function gGetSaldo(ByVal strCodDepositante As String, ByVal strCodAlmacen As String,
                                ByVal strCodUnidad As String, ByVal strNroComprobante As String,
                                ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        Dim dsResult As DataSet
        arParms(0) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodDepositante
        arParms(1) = New SqlParameter("@ISCO_UNID ", SqlDbType.Char, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(2).Value = strCodAlmacen
        arParms(3) = New SqlParameter("@ISNU_DOCU_RECE", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroComprobante
        dsResult = SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "USP_TCALMA_MERC_Saldo", arParms)
        Return dsResult.Tables(0)
    End Function

    Public Function InsInventario(ByVal strCodUnid As String, ByVal strNomArchivo As String,
                                    ByVal strRutaArchivo As String, ByVal strNroComprobante As String,
                                    ByVal strCodAlmacen As String, ByVal strCodCliente As String,
                                    ByVal strNroSolicitud As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(0).Value = strCodUnid
        arParms(1) = New SqlParameter("@ISNO_ARCH", SqlDbType.VarChar, 50)
        arParms(1).Value = strNomArchivo
        arParms(2) = New SqlParameter("@ISRU_ARCH", SqlDbType.VarChar, 100)
        arParms(2).Value = strRutaArchivo
        arParms(3) = New SqlParameter("@ISNU_DOCU_RECE", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroComprobante
        arParms(4) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(4).Value = strCodAlmacen
        arParms(5) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(5).Value = strCodCliente
        arParms(6) = New SqlParameter("@ISNU_SOLI", SqlDbType.VarChar, 20)
        arParms(6).Value = strNroSolicitud
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "USP_TCALMA_INSP_INVENTARIO", arParms).Tables(0)
    End Function

    Public Function InsSolicitudWarrants_Limite(ByVal strCodEmp As String, ByVal strNrodocu As String, ByVal strCodDepositante As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmp
        arParms(1) = New SqlParameter("@ISNRO_DOCU", SqlDbType.VarChar, 8)
        arParms(1).Value = strNrodocu
        arParms(2) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodDepositante
        arParms(3) = New SqlParameter("@DE_MENS", SqlDbType.VarChar, 2000)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ValidaLimite", arParms)
        Return arParms(3).Value().ToString()
    End Function

    Public Function InsSolicitudWarrants_LimiteNvoWarrant(ByVal strCodEmp As String, ByVal strNrodocu As Integer, ByVal strCodDepositante As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmp
        arParms(1) = New SqlParameter("@ISNRO_DOCU", SqlDbType.Int)
        arParms(1).Value = strNrodocu
        arParms(2) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodDepositante
        arParms(3) = New SqlParameter("@DE_MENS", SqlDbType.VarChar, 2000)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ValidaLimiteNvoWarrant", arParms)
        Return arParms(3).Value().ToString()
    End Function

    Public Function InsSolicitudWarrantsDetalle(ByVal strNroDocu As String, ByVal strDisp As String,
                                            ByVal strDes As String, ByVal strCan As String,
                                            ByVal strMon As String, ByVal strPre As String,
                                            ByVal strCanT As String, ByVal strUnidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@STR_NRO_DOCU", SqlDbType.Char, 8)
        arParms(0).Value = strNroDocu
        arParms(1) = New SqlParameter("@STR_DIS", SqlDbType.VarChar, 50)
        arParms(1).Value = strDisp
        arParms(2) = New SqlParameter("@STR_DSC_MERC", SqlDbType.VarChar, 300)
        arParms(2).Value = strDes
        arParms(3) = New SqlParameter("@STR_CANT_SOLI", SqlDbType.VarChar, 50)
        arParms(3).Value = strCan
        arParms(4) = New SqlParameter("@STR_TIP_MONE", SqlDbType.Char, 3)
        arParms(4).Value = strMon
        arParms(5) = New SqlParameter("@STR_PREC_MERC", SqlDbType.VarChar, 50)
        arParms(5).Value = strPre
        arParms(6) = New SqlParameter("@STR_CANT_TOTAL", SqlDbType.VarChar, 50)
        arParms(6).Value = strCanT
        arParms(7) = New SqlParameter("@STR_UNI_MEDI", SqlDbType.VarChar, 10)
        arParms(7).Value = strUnidad
        arParms(8) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(8).Direction = ParameterDirection.Output
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SolicitudWarrantsDetalle", arParms).Tables(0)
    End Function

    Public Function gGetValidaCertificado(ByVal strIdUsuario As String, ByVal strNroCert As String,
                                            ByVal strNroCA As String, ByVal strFchExp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@in_chr_coduser", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario.Trim
        arParms(1) = New SqlParameter("@in_var_seriecert", SqlDbType.VarChar, 50)
        arParms(1).Value = strNroCert
        arParms(2) = New SqlParameter("@in_var_serieca ", SqlDbType.VarChar, 100)
        arParms(2).Value = strNroCA
        arParms(3) = New SqlParameter("@in_chr_fchexpir", SqlDbType.Char, 8)
        arParms(3).Value = strFchExp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_ValidaCertificado", arParms).Tables(0)
    End Function

    Public Function gGetValidarCancelacion(ByVal strNroDoc As String) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Mensaje", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ValidarCancelacion", arParms)
        Return arParms(1).Value
    End Function

    Public Function gGetTipoMoneda() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFMONEDA_Sel_TraerTipoMoneda").Tables(0)
    End Function

    Public Function gRegistraEndosoSICO(ByVal strNroDoc As String, ByVal strIdUsuario As String,
                                        ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdUsuario
        arParms(2) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_GrabaEndosoEnSICO", arParms)
        lstrResultado = arParms(2).Value().ToString()
    End Function

    Public Function gRegistraDobleEndosoSICO(ByVal strNroDoc As String, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_GrabaDobleEndosoEnSICO", arParms)
        lstrResultado = arParms(1).Value().ToString()
    End Function

    Public Function gRegistraFilemaster(ByVal strNroDoc As String)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISCOD_DOC", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@OSERROR", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaOfioper, CommandType.StoredProcedure, "USP_FILEMASTER_Ins_Documentos", arParms)
        lstrResultado = arParms(1).Value().ToString()
    End Function

    Public Function gImagenesFilemaster(ByVal strNroDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@ISCOD_DOC", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_FILEMASTER_Sel_Documentos", arParms).Tables(0)
    End Function

    Public Function gUpdRegistrar_Custodia_SICO(ByVal strNroDoc As String, ByVal strTipDoc As String,
                        ByVal strNroLibe As String, ByVal strPrdDocu As String, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(1).Value = strTipDoc
        arParms(2) = New SqlParameter("@NroLibe", SqlDbType.VarChar, 11)
        arParms(2).Value = strNroLibe
        arParms(3) = New SqlParameter("@PrdDocu", SqlDbType.Char, 4)
        arParms(3).Value = strPrdDocu
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_Actualizar_Documento_Registrar", arParms)
    End Function

    Public Function gUpdFolioProrroga(ByVal strNumeroFolio As String, ByVal strFechaFolio As String,
                           ByVal strSecuencial As String, ByVal strNroDoc As String, ByVal strNroLibe As String,
                           ByVal strPrdDocu As String, ByVal strTipDoc As String,
                           ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@NumeroFolio", SqlDbType.Int)
        arParms(0).Value = CInt(strNumeroFolio)
        arParms(1) = New SqlParameter("@FechaFolio", SqlDbType.DateTime)
        arParms(1).Value = Convert.ToDateTime(strFechaFolio)
        arParms(2) = New SqlParameter("@Secuencial", SqlDbType.Int)
        arParms(2).Value = strSecuencial
        arParms(3) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(3).Value = strNroDoc
        arParms(4) = New SqlParameter("@NroLibe", SqlDbType.VarChar, 11)
        arParms(4).Value = strNroLibe
        arParms(5) = New SqlParameter("@PrdDocu", SqlDbType.Char, 4)
        arParms(5).Value = strPrdDocu
        arParms(6) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(6).Value = strTipDoc
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ActualizarFolioProrrga", arParms)
    End Function

    Public Function gInsLiberacionFlujo(ByVal strNumeroDoc As String, ByVal strNroLibe As String,
                            ByVal strTipDoc As String, ByVal strIdUsuario As String, ByVal strCod_Unidad As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@NumeroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNumeroDoc
        arParms(1) = New SqlParameter("@NroLibe", SqlDbType.Char, 11)
        arParms(1).Value = strNroLibe
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipDoc
        arParms(3) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(3).Value = strIdUsuario
        arParms(4) = New SqlParameter("@Cod_Unidad", SqlDbType.Char, 3)
        arParms(4).Value = strCod_Unidad
        arParms(5) = New SqlParameter("@Asunto", SqlDbType.VarChar, 100)
        arParms(5).Direction = ParameterDirection.Output
        arParms(6) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 2)
        arParms(6).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_InsertarLiberacion", arParms)
        Return arParms(6).Value & arParms(5).Value
    End Function

    Public Function gInsLiberacionFlujoCustodia(ByVal strIdUsuario As String,
                                                ByVal strCod_Unidad As String, ByVal strCodClie As String,
                                                ByVal strCodLib As String, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strIdUsuario
        arParms(1) = New SqlParameter("@Cod_Unidad", SqlDbType.Char, 3)
        arParms(1).Value = strCod_Unidad
        arParms(2) = New SqlParameter("@COD_CLIE", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodClie
        arParms(3) = New SqlParameter("@COD_LIB", SqlDbType.VarChar, 100)
        arParms(3).Value = strCodLib
        arParms(4) = New SqlParameter("@Mensaje", SqlDbType.VarChar, 8)
        arParms(4).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_InsertarLiberacionCustodia", arParms)
        Return arParms(4).Value
    End Function

    Public Function gRegEndosoFolio(ByVal strNroDoc As String, ByVal strNroLib As String,
                                    ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                    ByVal Nombre As String, ByVal imgArch As Byte(),
                                    ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@Nomb", SqlDbType.VarChar, 29)
        arParms(4).Value = Nombre
        arParms(5) = New SqlParameter("@Doc", SqlDbType.Image)
        arParms(5).Value = imgArch
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFENDOSOFOLIO_Ins_EndosoFolio", arParms)
    End Function

    Public Function gInsProrroga(ByVal strNroDoc As String, ByVal strIdTipoDocumento As String,
                            ByVal strCodClie As String, ByVal strCodEntiFina As String,
                            ByVal strFechaVencLimite As String, ByVal strFechaVenc As String,
                            ByVal strDscMercaderia As String, ByVal intSecuencia As Int16,
                            ByVal dblSaldo As Double, ByVal strEmiteCertificado As String,
                            ByVal strModalidad As String, ByVal strMoneda As String, ByVal strIdUsuario As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@IdTipoDocumento", SqlDbType.VarChar, 5)
        arParms(1).Value = strIdTipoDocumento
        arParms(2) = New SqlParameter("@CodClie", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodClie
        arParms(3) = New SqlParameter("@CodEntiFina", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodEntiFina
        arParms(4) = New SqlParameter("@FechaVencLimite", SqlDbType.VarChar, 10)
        arParms(4).Value = strFechaVencLimite
        arParms(5) = New SqlParameter("@FechaVenc", SqlDbType.VarChar, 10)
        arParms(5).Value = strFechaVenc
        arParms(6) = New SqlParameter("@DscMercaderia", SqlDbType.VarChar, 180)
        arParms(6).Value = strDscMercaderia
        arParms(7) = New SqlParameter("@Secu", SqlDbType.Int)
        arParms(7).Value = intSecuencia
        arParms(8) = New SqlParameter("@Saldo", SqlDbType.Float)
        arParms(8).Value = dblSaldo
        arParms(9) = New SqlParameter("@EmiteCertificado", SqlDbType.Char, 1)
        arParms(9).Value = strEmiteCertificado
        arParms(10) = New SqlParameter("@Modalidad", SqlDbType.Char, 3)
        arParms(10).Value = strModalidad
        arParms(11) = New SqlParameter("@Moneda", SqlDbType.Char, 3)
        arParms(11).Value = strMoneda
        arParms(12) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 8)
        arParms(12).Value = strIdUsuario
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFPRORROGA_Ins_GenerarProrroga", arParms).Tables(0)
    End Function

    Public Function gInsLiberacion(ByVal strUnidad As String, ByVal strNroDoc As String,
                                ByVal strIdTipoDocumento As String, ByVal strCodClie As String,
                                ByVal strCodEntiFina As String, ByVal strTipoRetiro As String,
                                ByVal strNroLiberacion As String, ByVal strFechaDoc As String,
                                ByVal strIdUsuario As String, ByVal strModalidad As String,
                                ByVal strMoneda As String, ByVal strTipoComprobante As String,
                                ByVal strNroComprobante As String, ByVal strAlmacen As String,
                                ByVal strObservacion As String, ByVal strDeudaSoles As String,
                                ByVal strDeudaDolares As String, ByVal strMercaderia As String,
                                ByVal strVirtual As String, ByVal blnEmbarque As Boolean,
                                ByVal blnContable As Boolean, ByVal blnTraslado As Boolean,
                                ByVal blnCustodia As Boolean, ByVal strDestino As String, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(24) {}
        arParms(0) = New SqlParameter("@Unidad", SqlDbType.Char, 3)
        arParms(0).Value = strUnidad
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.VarChar, 3)
        arParms(2).Value = strIdTipoDocumento
        arParms(3) = New SqlParameter("@CodClie", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodClie
        arParms(4) = New SqlParameter("@CodEntiFina", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodEntiFina
        arParms(5) = New SqlParameter("@TipoRetiro", SqlDbType.VarChar, 3)
        arParms(5).Value = strTipoRetiro
        arParms(6) = New SqlParameter("@NroLiberacion", SqlDbType.Char, 11)
        arParms(6).Value = strNroLiberacion
        arParms(7) = New SqlParameter("@FechaDoc", SqlDbType.VarChar, 10)
        arParms(7).Value = strFechaDoc
        arParms(8) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(8).Value = strIdUsuario
        arParms(9) = New SqlParameter("@Modalidad", SqlDbType.Char, 3)
        arParms(9).Value = strModalidad
        arParms(10) = New SqlParameter("@Moneda", SqlDbType.VarChar, 3)
        arParms(10).Value = strMoneda
        arParms(11) = New SqlParameter("@TipoComprobante", SqlDbType.VarChar, 3)
        arParms(11).Value = strTipoComprobante
        arParms(12) = New SqlParameter("@NroComprobante", SqlDbType.VarChar, 20)
        arParms(12).Value = strNroComprobante
        arParms(13) = New SqlParameter("@Almacen", SqlDbType.VarChar, 200)
        arParms(13).Value = strAlmacen
        arParms(14) = New SqlParameter("@Observacion", SqlDbType.VarChar, 200)
        arParms(14).Value = strObservacion
        arParms(15) = New SqlParameter("@DeudaSoles", SqlDbType.VarChar, 20)
        arParms(15).Value = strDeudaSoles
        arParms(16) = New SqlParameter("@DeudaDolares", SqlDbType.VarChar, 20)
        arParms(16).Value = strDeudaDolares
        arParms(17) = New SqlParameter("@Mercaderia", SqlDbType.VarChar, 200)
        arParms(17).Value = strMercaderia
        arParms(18) = New SqlParameter("@Virtual", SqlDbType.VarChar, 1)
        arParms(18).Value = strVirtual
        arParms(19) = New SqlParameter("@Embarque", SqlDbType.Bit)
        arParms(19).Value = blnEmbarque
        arParms(20) = New SqlParameter("@Contable", SqlDbType.Bit)
        arParms(20).Value = blnContable
        arParms(21) = New SqlParameter("@Traslado", SqlDbType.Bit)
        arParms(21).Value = blnTraslado
        arParms(22) = New SqlParameter("@Custodia", SqlDbType.Bit)
        arParms(22).Value = blnCustodia
        arParms(23) = New SqlParameter("@Destino", SqlDbType.VarChar, 200)
        arParms(23).Value = strDestino
        arParms(24) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(24).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFLIBERACION_Ins_GenerarLiberacion", arParms)
        Return arParms(24).Value
    End Function

    Public Function gInsLiberacionContable(ByVal strUnidad As String, ByVal strNroDoc As String,
                                ByVal strIdTipoDocumento As String, ByVal strCodClie As String,
                                ByVal strCodEntiFina As String, ByVal strTipoRetiro As String,
                                ByVal strNroLiberacion As String, ByVal strFechaDoc As String,
                                ByVal strIdUsuario As String, ByVal strModalidad As String,
                                ByVal strMoneda As String, ByVal strTipoComprobante As String,
                                ByVal strNroComprobante As String, ByVal strCodigoAlmacen As String,
                                ByVal strObservacion As String, ByVal strDeudaSoles As String,
                                ByVal strDeudaDolares As String, ByVal strMercaderia As String,
                                ByVal strVirtual As String, ByVal blnEmbarque As Boolean,
                                ByVal strCodAlmaDest As String, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(21) {}
        arParms(0) = New SqlParameter("@Unidad", SqlDbType.Char, 3)
        arParms(0).Value = strUnidad
        arParms(1) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(1).Value = strNroDoc
        arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.VarChar, 3)
        arParms(2).Value = strIdTipoDocumento
        arParms(3) = New SqlParameter("@CodClie", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodClie
        arParms(4) = New SqlParameter("@CodEntiFina", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodEntiFina
        arParms(5) = New SqlParameter("@TipoRetiro", SqlDbType.VarChar, 3)
        arParms(5).Value = strTipoRetiro
        arParms(6) = New SqlParameter("@NroLiberacion", SqlDbType.Char, 11)
        arParms(6).Value = strNroLiberacion
        arParms(7) = New SqlParameter("@FechaDoc", SqlDbType.VarChar, 10)
        arParms(7).Value = strFechaDoc
        arParms(8) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(8).Value = strIdUsuario
        arParms(9) = New SqlParameter("@Modalidad", SqlDbType.Char, 3)
        arParms(9).Value = strModalidad
        arParms(10) = New SqlParameter("@Moneda", SqlDbType.VarChar, 3)
        arParms(10).Value = strMoneda
        arParms(11) = New SqlParameter("@TipoComprobante", SqlDbType.VarChar, 3)
        arParms(11).Value = strTipoComprobante
        arParms(12) = New SqlParameter("@NroComprobante", SqlDbType.VarChar, 20)
        arParms(12).Value = strNroComprobante
        arParms(13) = New SqlParameter("@CodigoAlmacen", SqlDbType.VarChar, 5)
        arParms(13).Value = strCodigoAlmacen
        arParms(14) = New SqlParameter("@Observacion", SqlDbType.VarChar, 200)
        arParms(14).Value = strObservacion
        arParms(15) = New SqlParameter("@DeudaSoles", SqlDbType.VarChar, 20)
        arParms(15).Value = strDeudaSoles
        arParms(16) = New SqlParameter("@DeudaDolares", SqlDbType.VarChar, 20)
        arParms(16).Value = strDeudaDolares
        arParms(17) = New SqlParameter("@Mercaderia", SqlDbType.VarChar, 200)
        arParms(17).Value = strMercaderia
        arParms(18) = New SqlParameter("@Virtual", SqlDbType.VarChar, 1)
        arParms(18).Value = strVirtual
        arParms(19) = New SqlParameter("@Embarque", SqlDbType.Bit)
        arParms(19).Value = blnEmbarque
        arParms(20) = New SqlParameter("@CodAlmaDest", SqlDbType.VarChar, 5)
        arParms(20).Value = strCodAlmaDest
        arParms(21) = New SqlParameter("@Mensaje", SqlDbType.Char, 1)
        arParms(21).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFLIBERACION_Ins_GenerarLiberacionContable_V3", arParms)
        Return arParms(21).Value
    End Function

    Public Function gUpdAccesoFirmar(ByVal strNroDoc As String, ByVal strNroLibe As String,
                                    ByVal strPrdDocu As String, ByVal strTipDoc As String,
                                    ByVal strUsuario As String)
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLibe", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLibe
        arParms(2) = New SqlParameter("@PrdDocu", SqlDbType.Char, 4)
        arParms(2).Value = strPrdDocu
        arParms(3) = New SqlParameter("@TipDoc", SqlDbType.Char, 2)
        arParms(3).Value = strTipDoc
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(4).Value = strUsuario
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFACCESOFIRMAR_Upd_Acceso", arParms)
    End Function

    Public Property strResultado() As String
        Get
            Return lstrResultado
        End Get
        Set(ByVal Value As String)
            lstrResultado = Value
        End Set
    End Property

    Public Property strTipoDocumentoGenerado() As String
        Get
            Return lstrTipoDocumento
        End Get
        Set(ByVal Value As String)
            lstrTipoDocumento = Value
        End Set
    End Property

    Public Property strNombrePDF() As String
        Get
            Return LstrNombrePDF
        End Get
        Set(ByVal Value As String)
            LstrNombrePDF = Value
        End Set
    End Property

    Public Property strCodDocWar() As String
        Get
            Return lstrCodDocWar
        End Get
        Set(ByVal Value As String)
            lstrCodDocWar = Value
        End Set
    End Property

    Public Function SelPdfpintarendo(ByVal Cod_Doc As Integer, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim CadenaCnn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@Cod_Doc", SqlDbType.Int, 2)
        arParms(0).Value = Cod_Doc
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Endoso", arParms).Tables(0)
    End Function

    Public Function SelPdfpintaSolWar_Det(ByVal Cod_Doc As Integer, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim CadenaCnn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@Cod_Doc", SqlDbType.Int)
        arParms(0).Value = Cod_Doc
        'arParms(1) = New SqlParameter("@NombrePDF", SqlDbType.VarChar, 29)
        'arParms(1).Direction = ParameterDirection.Output
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Solicitud_Warrant", arParms).Tables(0)
        'LstrNombrePDF = arParms(1).Value().ToString()
        'Return ds.Tables(0)
    End Function

    Public Function GetFirmantes(ByVal Cod_Doc As Integer, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim CadenaCnn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@Cod_Doc", SqlDbType.Int, 2)
        arParms(0).Value = Cod_Doc
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Firmantes", arParms).Tables(0)
    End Function

    Public Function GetDCR(ByVal Cod_Doc As Integer, ByVal CodUsuario As String, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@Cod_Doc", SqlDbType.Int)
        arParms(0).Value = Cod_Doc
        arParms(1) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
        arParms(1).Value = CodUsuario
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_DCR", arParms).Tables(0)
    End Function

    Public Function GetSello(ByVal Cod_Doc As Integer, ByVal ObjTrans As SqlTransaction) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@Cod_Doc", SqlDbType.Int)
        arParms(0).Value = Cod_Doc
        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Sello", arParms).Tables(0)
    End Function

    Public Function GetInformacionWarrantSICO(ByVal strCodEmpre As String, ByVal strCodUser As String, ByVal strCodDoc As String, _
                                                ByVal ObjTrans As SqlTransaction) As DataTable
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@CodEmpre", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpre
        arParms(1) = New SqlParameter("@CodUser", SqlDbType.VarChar, 8)
        arParms(1).Value = strCodUser
        arParms(2) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(2).Value = strCodDoc
        arParms(3) = New SqlParameter("@CodTipoDoc", SqlDbType.Char, 2)
        arParms(3).Direction = ParameterDirection.Output
        arParms(4) = New SqlParameter("@NombrePDF", SqlDbType.VarChar, 29)
        arParms(4).Direction = ParameterDirection.Output
        arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(5).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_InformacionPDF_PruebaJG", arParms)
        lstrTipoDocumento = arParms(3).Value().ToString()
        LstrNombrePDF = arParms(4).Value().ToString()
        lstrResultado = arParms(5).Value().ToString()
        Return ds.Tables(0)
    End Function

    Public Function GetInformacionSolictudWarrant(ByVal strCodEmpre As String, ByVal strCodDoc As String, _
                                            ByVal ObjTrans As SqlTransaction) As DataTable
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CodEmpre", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpre
        arParms(1) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(1).Value = strCodDoc
        arParms(2) = New SqlParameter("@NombrePDF", SqlDbType.VarChar, 29)
        arParms(2).Direction = ParameterDirection.Output
        arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(3).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_InformacionPDF_SolWar", arParms)
        '  LstrNombrePDF = arParms(2).Value().ToString()
        Return ds.Tables(0)
    End Function

    Public Function gEliminarLiberacion(ByVal strSEC_CONTA As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@SEC_CONT", SqlDbType.Int)
        arParms(0).Value = strSEC_CONTA
        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 50)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Del_EliminarLiberacion_V3", arParms)
        Return arParms(1).Value().ToString()
    End Function

    Public Function gEliminarLiberacionPendiente(ByVal strCOD_EMPR As String, ByVal strCOD_UNID As String, _
                                                  ByVal strNRO_DOC_WARR As String, ByVal strID_TIPO_DOC As String, _
                                                  ByVal strNRO_LIBE As String, ByVal strCOD_USER As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@COD_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCOD_EMPR
        arParms(1) = New SqlParameter("@COD_UNID", SqlDbType.Char, 3)
        arParms(1).Value = strCOD_UNID
        arParms(2) = New SqlParameter("@NRO_DOC_WARR", SqlDbType.VarChar, 8)
        arParms(2).Value = strNRO_DOC_WARR
        arParms(3) = New SqlParameter("@ID_TIPO_DOC", SqlDbType.Char, 2)
        arParms(3).Value = strID_TIPO_DOC
        arParms(4) = New SqlParameter("@NRO_LIBE", SqlDbType.VarChar, 11)
        arParms(4).Value = strNRO_LIBE
        arParms(5) = New SqlParameter("@COD_USER", SqlDbType.Char, 4)
        arParms(5).Value = strCOD_USER
        arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 50)
        arParms(6).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_Upd_AnularLiberacion", arParms)
        Return arParms(6).Value().ToString()
    End Function

    Public Function gGetReporteOperaciones(ByVal strFechaInicial As String, ByVal strFechaFinal As String, _
                                            ByVal strDepositante As String, ByVal strFinanciador As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@FEC_INICIAL", SqlDbType.VarChar, 8)
        arParms(0).Value = strFechaInicial.Trim
        arParms(1) = New SqlParameter("@FEC_FINAL", SqlDbType.VarChar, 8)
        arParms(1).Value = strFechaFinal
        arParms(2) = New SqlParameter("@COD_BANCO", SqlDbType.VarChar, 20)
        arParms(2).Value = strFinanciador
        arParms(3) = New SqlParameter("@COD_CLIENTE", SqlDbType.VarChar, 20)
        arParms(3).Value = strDepositante
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_Reporte_MovimientoWarrant", arParms).Tables(0)
    End Function

    Public Function DBGetKardex(ByVal strDepositante As String, ByVal strEstado As String, _
                                         ByVal strCodEntidad As String, ByVal strTipoMercaderia As String, _
                                         ByVal strNroWarrant As String, ByVal strTipoEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@COD_DEPO", SqlDbType.VarChar, 20)
        arParms(0).Value = strDepositante.Trim
        arParms(1) = New SqlParameter("@COD_ESTA", SqlDbType.VarChar, 2)
        arParms(1).Value = strEstado
        arParms(2) = New SqlParameter("@COD_ENTI_FINA", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodEntidad
        arParms(3) = New SqlParameter("@TipoMercaderia", SqlDbType.VarChar, 3)
        arParms(3).Value = strTipoMercaderia
        arParms(4) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 20)
        arParms(4).Value = strNroWarrant
        arParms(5) = New SqlParameter("@strTipoEntidad", SqlDbType.Char, 2)
        arParms(5).Value = strTipoEntidad
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_Reporte_KardexWarrant", arParms).Tables(0)
    End Function

    Public Function gGetReporteOperacionesTotal(ByVal strFechaInicial As String, ByVal strFechaFinal As String, _
                                            ByVal strDepositante As String, ByVal strFinanciador As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@FEC_INICIAL", SqlDbType.VarChar, 8)
        arParms(0).Value = strFechaInicial.Trim
        arParms(1) = New SqlParameter("@FEC_FINAL", SqlDbType.VarChar, 8)
        arParms(1).Value = strFechaFinal
        arParms(2) = New SqlParameter("@COD_BANCO", SqlDbType.VarChar, 20)
        arParms(2).Value = strFinanciador
        arParms(3) = New SqlParameter("@COD_CLIENTE", SqlDbType.VarChar, 20)
        arParms(3).Value = strDepositante
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_Reporte_MovimientoWarrantTotal", arParms).Tables(0)
    End Function

    Public Function gGetEstadoImpresion(ByVal strCodDoc As String) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresion", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetEstadoImpresionTran(ByVal strCodDoc As String, ByVal ObjTrans As SqlTransaction) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresion", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetEstadoImpresionTrans(ByVal strCodDoc As String, ByVal ObjTrans As SqlTransaction) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresion", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetEstadoImpresionCreados(ByVal strCodDoc As String) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresionCreado", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetEstadoImpresionEmitido(ByVal strCodDoc As String) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresionEmitido", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetEstadoImpresionVB(ByVal strCodDoc As String, ByVal strUsuario As String, _
                                            ByVal ObjTrans As SqlTransaction) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
        arParms(1).Value = strUsuario
        arParms(2) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresionVB", arParms)
        Return arParms(2).Value()
    End Function

    Public Function gGetEstadoImpresion(ByVal strCodDoc As String, ByVal ObjTrans As SqlTransaction) As Boolean

        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.Bit)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_EstadoImpresion", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gCADGetListarFirmas(ByVal strCodEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodEnti", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodEntidad
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Sel_ListarFirmas", arParms).Tables(0)
    End Function

    Public Function gCADGetDetalleWarrant(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                         ByVal strTipTitulo As String, ByVal strNumTitulo As String) As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@CO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@TI_TITU", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipTitulo
        arParms(3) = New SqlParameter("@NU_TITU", SqlDbType.VarChar, 8)
        arParms(3).Value = strNumTitulo
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_Reporte_DetalleWarrant", arParms)
    End Function

    Public Function gCADGetTraerDestinatarioCarta(ByVal strCodDoc As String, ByVal objTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@COD_DOC", SqlDbType.Int)
        arParms(0).Value = CInt(strCodDoc)
        arParms(1) = New SqlParameter("@Return", SqlDbType.VarChar, 500)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(objTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_Remitente", arParms)
        Return arParms(1).Value()
    End Function

    Public Function gGetValidaContrasena(ByVal strCodUser As String, ByVal strContrasena As String,
                                        ByVal strContrasenaNueva As String, ByVal strIP As String, ByVal ObjTrans As SqlTransaction) As String

        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strCodUser
        arParms(1) = New SqlParameter("@Contrasena", SqlDbType.VarChar, 100)
        arParms(1).Value = strContrasena
        arParms(2) = New SqlParameter("@ContrasenaNueva", SqlDbType.VarChar, 100)
        arParms(2).Value = strContrasenaNueva
        arParms(3) = New SqlParameter("@IP", SqlDbType.VarChar, 20)
        arParms(3).Value = strIP
        arParms(4) = New SqlParameter("@Return", SqlDbType.Char, 1)
        arParms(4).Direction = ParameterDirection.Output
        SqlHelper.ExecuteScalar(strCadenaWE, CommandType.StoredProcedure, "Usp_WFUSUARIO_Sel_ValidaClaveFirma", arParms)
        Return arParms(4).Value()
    End Function

    Public Function gGetValidaContrasenaMobil(ByVal strCodUser As String, strLogin As String, ByVal strContrasena As String,
                                        ByVal strContrasenaNueva As String, ByVal strIP As String, strTipoAct As String, ByVal ObjTrans As SqlTransaction) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        Dim dsResult As DataSet
        arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strCodUser
        arParms(1) = New SqlParameter("@Login", SqlDbType.VarChar, 50)
        arParms(1).Value = strCodUser
        arParms(2) = New SqlParameter("@Contrasena", SqlDbType.VarChar, 100)
        arParms(2).Value = strContrasena
        arParms(3) = New SqlParameter("@ContrasenaNueva", SqlDbType.VarChar, 100)
        arParms(3).Value = strContrasenaNueva
        arParms(4) = New SqlParameter("@IP", SqlDbType.VarChar, 20)
        arParms(4).Value = strIP
        arParms(5) = New SqlParameter("@TipoAct", SqlDbType.Char, 1)
        arParms(5).Value = strTipoAct
        dsResult = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFUSUARIO_Mobil_Sel_ValidaClaveFirma", arParms)
        Return dsResult.Tables(0)
    End Function


    Public Function gContabilizarLiberacion(ByVal strNroDoc As String, ByVal strNroLib As String, _
                                    ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String, _
                                    ByVal strIdUsuario As String, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(4).Value = strIdUsuario
        arParms(5) = New SqlParameter("@Estado", SqlDbType.Char, 2)
        arParms(5).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ContabilizarLiberacion_PRU", arParms)
        Return arParms(5).Value()
    End Function

    Public Function gEnvioCorreo(ByVal strCorreoDe As String, ByVal strCorreoPara As String, _
                                ByVal strAsunto As String, ByVal strMensaje As String, ByVal strFile As String)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = "01"
        arParms(1) = New SqlParameter("@To", SqlDbType.VarChar, 1000)
        arParms(1).Value = strCorreoPara
        arParms(2) = New SqlParameter("@Cc", SqlDbType.VarChar, 100)
        arParms(2).Value = ""
        arParms(3) = New SqlParameter("@Subject", SqlDbType.VarChar, 200)
        arParms(3).Value = strAsunto
        arParms(4) = New SqlParameter("@Body", SqlDbType.VarChar)
        arParms(4).Value = strMensaje
        arParms(5) = New SqlParameter("@attachments", SqlDbType.VarChar, 4000)
        arParms(5).Value = strFile
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_EnvioCorreo_old", arParms)
    End Function

    Public Function sSetVistoBueno(ByVal strSEC_CONTA As String, ByVal IdUsuario As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@SEC_CONT", SqlDbType.VarChar, 8)
        arParms(0).Value = strSEC_CONTA
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(1).Value = IdUsuario
        arParms(2) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 50)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFLIBERACION_CONTABLE_Ins_VistoBueno", arParms)
        Return arParms(2).Value().ToString()
    End Function

    Public Function EnviarFueraLimite(ByVal strCodDepositante As String, ByVal strCodAlmacen As String, _
                                  ByVal strNroComprobante As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        Dim dsResult As DataSet
        arParms(0) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodDepositante
        arParms(1) = New SqlParameter("@strAlmacen", SqlDbType.VarChar, 500)
        arParms(1).Value = strCodAlmacen
        arParms(2) = New SqlParameter("@NroComprobante", SqlDbType.VarChar, 20)
        arParms(2).Value = strNroComprobante
        dsResult = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SoliWarrEnviFueraLimite", arParms)
        Return dsResult.Tables(0)
    End Function

    Public Function gGetEntAprubCli(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_ValidaAproDepo", arParms).Tables(0)
    End Function

    Public Function gUpdDocLib(ByVal strNroDoc As String, ByVal strNroLib As String,
                                      ByVal strPeriodoAnual As String, ByVal strIdTipoDocumento As String,
                                      ByVal Nombre As String, ByVal imgArch As Byte(),
                                      ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@NroLib", SqlDbType.Char, 11)
        arParms(1).Value = strNroLib
        arParms(2) = New SqlParameter("@Periodo", SqlDbType.Char, 4)
        arParms(2).Value = strPeriodoAnual
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strIdTipoDocumento
        arParms(4) = New SqlParameter("@Nomb", SqlDbType.VarChar, 29)
        arParms(4).Value = Nombre
        arParms(5) = New SqlParameter("@Doc", SqlDbType.Image)
        arParms(5).Value = imgArch
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_DocLib", arParms)
    End Function

    'nuevo cambio solicitud warrant similar a afi: 09-01-2019 
    '--cabecera

    Public Function gGetBusquedaDocumentosSolicitadorWarrant(strNroDoc As String, strCod_Doc As String,
                                                             strIdTipoDocu As String, ByVal strCodEsta As String,
                                                             strCodClie As String, strCodEndo As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Cod_Doc", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_Doc
        arParms(2) = New SqlParameter("@TipoDoc", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocu
        arParms(3) = New SqlParameter("@CodEsta", SqlDbType.Int)
        arParms(3).Value = CInt(strCodEsta)
        arParms(4) = New SqlParameter("@CodClie", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodClie
        arParms(5) = New SqlParameter("@CodEndo", SqlDbType.VarChar, 20)
        arParms(5).Value = strCodEndo


        'Dim strCad1, strCad2 As String
        'Dim p As SqlParameter
        'For Each p In arParms
        '    strCad1 = strCad1 & "" & p.ParameterName & "='" & (p.Value).ToString & "',"
        '    strCad2 = strCad2 & "" & "'" & (p.Value).ToString & "',"
        'Next p

        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_WFDOCUMENTO_SEL_SolicitadorWarrant", arParms).Tables(0)
    End Function
    Public Function gGetBusquedaDocumentoTipoWarrant(strCodEmp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@codEmpr", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerTipoWarrant", arParms).Tables(0)
    End Function
    Public Function gGetBusquedaDocumentoTraeMoneda(strCodEmp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@codEmpr", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerMoneda", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentoTraerRegimen(strCodEmp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@codEmpr", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerRegimen", arParms).Tables(0)
    End Function

    Public Function gGetAlmacenCampoPropioXCliente(ByVal strCodCliente As String, strTiAlma As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@TiAlma ", SqlDbType.VarChar, 3)
        arParms(1).Value = strTiAlma
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarAlmacenPropioCampoXCliente", arParms).Tables(0)
    End Function



    Public Function SetGrabarSolicitudWarrantCabecera(strNroDoc As String, strCod_Doc As String, strIdTipoDocu As String, strTipoWarrant As String, strEndoso As String, strTipoAlmacen As String, strAnioDua As String, strRegimen As String, strTipoDocuSol As String, strAsociado As String,
                                                      strAlmacen As String, strCodAduana As String, strMoneda As String, strCodDepositante As String, strFinanciador As String, strNroDua As String, strIdUsuario As String, strNroTitulo As String, ByVal ObjTrans As SqlTransaction) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(17) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Cod_Doc", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_Doc
        arParms(2) = New SqlParameter("@TipoDoc", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocu
        arParms(3) = New SqlParameter("@CoModaMovi", SqlDbType.VarChar, 3)
        arParms(3).Value = strTipoWarrant
        arParms(4) = New SqlParameter("@Endoso", SqlDbType.Int)
        arParms(4).Value = strEndoso
        arParms(5) = New SqlParameter("@TipoAlmacen", SqlDbType.VarChar, 3)
        arParms(5).Value = strTipoAlmacen
        arParms(6) = New SqlParameter("@AnioDua", SqlDbType.VarChar, 20)
        arParms(6).Value = strAnioDua
        arParms(7) = New SqlParameter("@CodRegimen", SqlDbType.VarChar, 100)
        arParms(7).Value = strRegimen
        arParms(8) = New SqlParameter("@TipoDocuSol", SqlDbType.Int)
        arParms(8).Value = CInt(strTipoDocuSol)
        arParms(9) = New SqlParameter("@Asociado", SqlDbType.VarChar, 20)
        arParms(9).Value = strAsociado
        arParms(10) = New SqlParameter("@CodAlma", SqlDbType.VarChar, 8)
        arParms(10).Value = strAlmacen
        arParms(11) = New SqlParameter("@CodAduana", SqlDbType.VarChar, 20)
        arParms(11).Value = strCodAduana
        arParms(12) = New SqlParameter("@Moneda", SqlDbType.VarChar, 3)
        arParms(12).Value = strMoneda
        arParms(13) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(13).Value = strCodDepositante
        arParms(14) = New SqlParameter("@CodFinanciador", SqlDbType.VarChar, 20)
        arParms(14).Value = strFinanciador
        arParms(15) = New SqlParameter("@NroDua", SqlDbType.VarChar, 20)
        arParms(15).Value = strNroDua
        arParms(16) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(16).Value = strIdUsuario
        arParms(17) = New SqlParameter("@NroTitulo", SqlDbType.VarChar, 8)
        arParms(17).Value = strNroTitulo
        'Dim strCadena As String = CapturaVariables(arParms)

        Return SqlHelper.ExecuteDataset(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SolicitudWarrantsCabe", arParms).Tables(0)

    End Function

    Public Function gGetBusquedaDocumentoProducto(strCodEmp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@codEmpr", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerSolicitudProducto", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaDocumentoUnidadMedida(strCodEmp As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@codEmpr", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmp
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFTIPODOCUMENTO_Sel_TraerSolicitudUnidMedida", arParms).Tables(0)
    End Function

    Public Function SetGrabarSolicitudWarrantDetalle(strNroDoc As String, strCod_Doc As String, strIdTipoDocu As String, strNroItem As String,
                                                     strCodpro As String, strCodUnidMed As String, strCantPeso As String, strCantInic As String,
                                                     strDesc As String, strValorUnit As String, strCodDepositante As String, strIdUsuario As String,
                                                     strSerie As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Cod_Doc", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_Doc
        arParms(2) = New SqlParameter("@TipoDoc", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocu
        arParms(3) = New SqlParameter("@NroItem", SqlDbType.VarChar, 4)
        arParms(3).Value = strNroItem
        arParms(4) = New SqlParameter("@Codpro", SqlDbType.VarChar, 6)
        arParms(4).Value = strCodpro
        arParms(5) = New SqlParameter("@CodUnidMed", SqlDbType.VarChar, 5)
        arParms(5).Value = strCodUnidMed
        arParms(6) = New SqlParameter("@CantPeso", SqlDbType.Decimal, 15, 3)
        arParms(6).Value = strCantPeso
        arParms(7) = New SqlParameter("@CantInic", SqlDbType.VarChar, 5)
        arParms(7).Value = strCantInic
        arParms(8) = New SqlParameter("@Desc", SqlDbType.VarChar, 300)
        arParms(8).Value = strDesc
        arParms(9) = New SqlParameter("@ValorUnit", SqlDbType.Decimal, 15, 3)
        arParms(9).Value = strValorUnit
        arParms(10) = New SqlParameter("@CodDepositante", SqlDbType.VarChar, 20)
        arParms(10).Value = strCodDepositante
        arParms(11) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 5)
        arParms(11).Value = strIdUsuario
        arParms(12) = New SqlParameter("@Serie", SqlDbType.Int)
        arParms(12).Value = strSerie

        'Dim strCadena As String = CapturaVariables(arParms)

        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_SolicitudWarrantsDeta", arParms).Tables(0)

    End Function


    Public Function gGetBusquedaDocumentosSolicitadorWarrantDetalle(strNroDoc As String, strCod_Doc As String,
                                                         strIdTipoDocu As String, ByVal strCodItem As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Cod_Doc", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_Doc
        arParms(2) = New SqlParameter("@TipoDoc", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocu
        arParms(3) = New SqlParameter("@CodItem", SqlDbType.Char, 4)
        arParms(3).Value = strCodItem
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_WFDOCUMENTO_SEL_SolicitadorWarrantDeta", arParms).Tables(0)
    End Function


    Public Function SetUpdateDocumentosSolicitadorWarrant(strCod_Doc As String, strIdUsuario As String, strflgApro As String, strdscCome As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@COD_DOCU", SqlDbType.Char, 8)
        arParms(0).Value = strCod_Doc
        arParms(1) = New SqlParameter("@COD_USER", SqlDbType.VarChar, 8)
        arParms(1).Value = strIdUsuario
        arParms(2) = New SqlParameter("@FLG_APRO", SqlDbType.Char, 3)
        arParms(2).Value = strflgApro
        arParms(3) = New SqlParameter("@DSC_COME", SqlDbType.VarChar, 200)
        arParms(3).Value = strdscCome

        'Dim strCadena As String = CapturaVariables(arParms)
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Upd_ActualizarDocumentoSolicitadorWarrant", arParms).Tables(0)

    End Function

    Public Function SetDeleteDocumentosSolicitadorWarrantDeta(strNroDoc As String, strCod_Doc As String, strIdTipoDocu As String, ByVal strCodItem As String, strIdUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@Cod_Doc", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_Doc
        arParms(2) = New SqlParameter("@TipoDoc", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoDocu
        arParms(3) = New SqlParameter("@CodItem", SqlDbType.Char, 4)
        arParms(3).Value = strCodItem
        arParms(4) = New SqlParameter("@COD_USER", SqlDbType.Char, 4)
        arParms(4).Value = strIdUsuario
        'Dim strCadena As String = CapturaVariables(arParms)
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_WFDOCUMENTO_DEL_SolicitadorWarrantDeta", arParms).Tables(0)

    End Function

    Private Function CapturaVariables(arParms() As SqlParameter) As String
        '=== Captura Variables === felix malaver :      Dim strCadena As String = CapturaVariables(arParms)
        Dim strCad1, strCad2, strCad3 As String
        Dim p As SqlParameter
        For Each p In arParms
            strCad1 = strCad1 & "" & p.ParameterName & "='" & (p.Value).ToString & "',"
            strCad2 = strCad2 & "" & "'" & (p.Value).ToString & "',"
        Next p
        strCad3 = "Cadena1:( " & strCad1 & ")   Cadena2:(" & strCad2 & ")"
        Return Replace(strCad3, ",)", ")")
    End Function
    '========================= prueba regularizar endoso =======================================

    Public Function gRegistraEndosoSICO_PruRegu(ByVal strNroDoc As String, ByVal strIdUsuario As String, strFechApro As DateTime, strNroFolio As String,
                                        ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdUsuario
        arParms(2) = New SqlParameter("@VDFE_ACTU", SqlDbType.DateTime)
        arParms(2).Value = strFechApro
        arParms(3) = New SqlParameter("@V_NRO_FOLIO", SqlDbType.Char, 4)
        arParms(3).Value = strNroFolio
        arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(4).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_GrabaEndosoEnSICO_PruReg", arParms)
        lstrResultado = arParms(4).Value().ToString()
    End Function

    Public Function gRegistraDobleEndosoSICO_PruRegu(ByVal strNroDoc As String, ByVal ObjTrans As SqlTransaction)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodDoc", SqlDbType.Int)
        arParms(0).Value = strNroDoc
        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.Char, 1)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Ins_GrabaDobleEndosoEnSICO_PruRegu", arParms)
        lstrResultado = arParms(1).Value().ToString()
    End Function

    Public Function gGetDocumento_PruRegu(strTipoConsu As String) As DataTable
        Dim Result As New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@TipoConsu", SqlDbType.VarChar, 1)
        arParms(0).Value = strTipoConsu.Trim

        Result = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Mobil_Sel_PruReg", arParms).Tables(0)
        If Result.Rows.Count > 0 Then
            Return Result
        End If
        Result.Dispose()
        Result = Nothing

    End Function

    Public Function gUpdDocumento_IMPR(ByVal strNumeroDoc As String, ByVal strNroLibe As String,
                                                                    ByVal strTipDoc As String, ByVal strFLG_VAL_IMPR As Boolean
                                                                    ) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@NroDocu", SqlDbType.VarChar, 8)
        arParms(0).Value = strNumeroDoc
        arParms(1) = New SqlParameter("@NroLibe", SqlDbType.Char, 11)
        arParms(1).Value = strNroLibe
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipDoc
        arParms(3) = New SqlParameter("@FLG_VAL_IMPR", SqlDbType.Bit)
        arParms(3).Value = strFLG_VAL_IMPR
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_upd_impr", arParms)

    End Function

    Public Function SetWFDOCUMENTO_FLGIMPR(ByVal strNumeroDoc As String, ByVal strNroLibe As String, ByVal strTipDoc As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@NroDoc", SqlDbType.Char, 8)
        arParms(0).Value = strNumeroDoc
        arParms(1) = New SqlParameter("@NroLibe", SqlDbType.VarChar, 11)
        arParms(1).Value = strNroLibe
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 3)
        arParms(2).Value = strTipDoc

        'Dim strCadena As String = CapturaVariables(arParms)
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_WFDOCUMENTO_DEL_SolicitadorWarrantDeta", arParms).Tables(0)

    End Function

End Class
