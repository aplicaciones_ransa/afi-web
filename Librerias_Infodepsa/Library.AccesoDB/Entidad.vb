Imports System.Data
Imports System.Data.SqlClient
Public Class Entidad
    Inherits System.ComponentModel.Component
    Private strCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Public Function gGetTipoEntidades() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFTIPOENTIDAD_Sel_TraerTipoEntidades").Tables(0)
    End Function

    Public Function gGetDepartamento() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFDEPARTAMENTO_Sel_ListarDepartamento").Tables(0)
    End Function

    Public Function gGetEstado(ByVal strEnProceso As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@EnProceso", SqlDbType.Char, 1)
        arParms(0).Value = strEnProceso.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFESTADO_Sel_TraerEstados", arParms).Tables(0)
    End Function

    Public Function gGetDataEntidad(ByVal strIdEntidad As String, ByVal strTipoEntidad As String) As DataRow
        Dim Result As DataTable = New DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(1).Value = strTipoEntidad.Trim
        Result = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_TraerInformacionEntidad", arParms).Tables(0)
        If Result.Rows.Count > 0 Then
            Return Result.Rows(0)
        End If
        Result.Dispose()
        Result = Nothing
    End Function

    Public Function gGetFirmaxEntidad(ByVal strIdEntidad As String, ByVal strTipoEntidad As String,
                                        ByVal strTipoDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@TipoEntidad", SqlDbType.VarChar, 2)
        arParms(1).Value = strTipoEntidad.Trim
        arParms(2) = New SqlParameter("@TipoDocumento", SqlDbType.VarChar, 2)
        arParms(2).Value = strTipoDocumento.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFEVENTOSXENTIDAD_Sel_TraerFirmaxEntidad", arParms).Tables(0)
    End Function

    Public Function gGetEntidadesUsuario(ByVal strCodUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodUsuario", SqlDbType.Char, 4)
        arParms(0).Value = strCodUsuario.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFUSRXENTIDAD_Sel_TraerEntidades", arParms).Tables(0)
    End Function

    Public Function gGetEntidadesSICO(ByVal strIdTipoEntidad As String) As DataTable
        Dim strCnn As String
        Dim query As String
        Select Case strIdTipoEntidad
            Case "01"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
                query = "Select COD_SICO as Codigo, NOM_ENTI as Nombre From WFENTIDAD Where COD_TIPENTI= " & strIdTipoEntidad & " Order By NOM_ENTI asc"
            Case "02"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                query = "Select CO_ENTI_FINA as Codigo, DE_LARG_ENFI as Nombre From TMENTI_FINA Where TI_SITU='ACT' and DE_CORT_ENFI not in ('','null') and st_visu_webb='S' Order By DE_CORT_ENFI asc"
            Case "03"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
                query = "Select CO_CLIE as Codigo, LTRIM(NO_CLIE_REPO) as Nombre From TMCLIE Where TI_SITU='ACT' AND NO_CLIE_REPO not in ('','null') Order By LTRIM(NO_CLIE_REPO) asc"
            Case "04"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
                query = "SELECT CO_ALMA  as Codigo, DE_DIRE_ALMA  as Nombre FROM TMALMA WHERE TI_ALMA in ('002','005') ORDER BY LTRIM(DE_DIRE_ALMA) ASC"
        End Select
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0)
    End Function

    Public Function gGetEntidadesSICO2(ByVal strIdTipoEntidad As String) As DataTable
        Dim strCnn As String
        Dim query As String
        strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        query = "select distinct  '1' as Codigo, dsc_clie as Nombre from dbo.WFSTOCK where  dsc_clie not in ('','null') and  dsc_clie = '" & strIdTipoEntidad & "'"
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0)
    End Function

    Public Function gGetNombreAlmacen(ByVal strIdSico As String) As DataTable
        Dim strCnn As String
        Dim query As String
        strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        query = "select distinct COD_ALMA as Codigo, DSC_ALMA as Nombre from WFSTOCK  where cod_clie = '" & strIdSico & "' and convert(varchar, fch_carg, 103) = convert(varchar, getdate(), 103) union all select 'OTROS'  as Codigo, 'OTROS' as Nombre "
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0)
    End Function

    Public Function gGetTipoMerc() As DataTable
        Dim strCnn As String
        Dim query As String
        strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
        query = "SELECT CO_TIPO_MERC, DE_TIPO_MERC FROM TTTIPO_MERC WHERE LEFT(CO_TIPO_MERC,1)='0' Order By DE_TIPO_MERC asc"
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0)
    End Function

    Public Function gMostrarGrupos(ByVal strISCO_SIST As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@ISCO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strISCO_SIST.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_SEL_TRSIST_GRUP", arParms).Tables(0)
    End Function

    Public Function gUpdProrrogaSICO(ByVal strCO_EMPR As String, ByVal strTI_TITU As String,
                                 ByVal strNU_TITU As String, ByVal strCO_MONE As String,
                                 ByVal strIM_WARR As Decimal, ByVal strIM_SALW As Decimal,
                                 ByVal strFE_VENC As String, ByVal strUSR_ACCE As String) As DataRow
        Dim strCnn As String
        Dim store As String
        strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
        store = "SP_REGISTRA_PRORROGAS"
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCO_EMPR
        arParms(1) = New SqlParameter("@sTI_TITU", SqlDbType.VarChar, 2)
        arParms(1).Value = strTI_TITU
        arParms(2) = New SqlParameter("@NU_TITU", SqlDbType.VarChar, 20)
        arParms(2).Value = strNU_TITU
        arParms(3) = New SqlParameter("@sCO_MONE", SqlDbType.Int)
        arParms(3).Value = CInt(strCO_MONE)
        arParms(4) = New SqlParameter("@IM_WARR", strIM_WARR)
        arParms(5) = New SqlParameter("@IM_SALW", strIM_SALW)
        arParms(6) = New SqlParameter("@FE_VENC", SqlDbType.VarChar, 10)
        arParms(6).Value = strFE_VENC
        If strUSR_ACCE.Length > 8 Then
            strUSR_ACCE = strUSR_ACCE.Substring(0, 8)
        End If
        arParms(7) = New SqlParameter("@sCO_USUA", SqlDbType.VarChar, 8)
        arParms(7).Value = strUSR_ACCE
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, store, arParms).Tables(0).Rows(0)
    End Function

    Public Function GetInformacionFinanciera(ByVal strCodSico As String, ByVal strTipoEntidad As String) As DataRow
        Dim strCnn As String
        Dim query As String
        Select Case strTipoEntidad
            Case "01"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
                query = "Select NRO_RUC as RUC, DIR_ENTI as DIRECCION, '' as NombreLargo From WFENTIDAD Where COD_SICO='00000001'"
            Case "02"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                query = "Select NU_RUCS as RUC, DE_DIRE as DIRECCION, DE_LARG_ENFI as NombreLargo From TMENTI_FINA Where CO_ENTI_FINA=" & strCodSico
            Case "03"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
                query = "Select NU_RUCS_0001 as RUC, '' as DIRECCION, '' as NombreLargo From TMCLIE Where CO_CLIE='" & strCodSico & "'"
            Case "04"
                strCnn = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
                query = "SELECT CO_ALMA AS RUC, DE_DIRE_ALMA AS DIRECCION, DE_DIRE_ALMA AS NombreLargo FROM TMALMA WHERE TI_ALMA in ('002','005')  AND CO_ALMA ='" & strCodSico & "'"
        End Select
        Return SqlHelper.ExecuteDataset(strCnn, CommandType.Text, query).Tables(0).Rows(0)
    End Function

    Public Function gGetEntidades(ByVal strIdTipoEntidad As String, ByVal strParaWarrant As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strIdTipoEntidad.Trim
        arParms(1) = New SqlParameter("@ParaWarrant", SqlDbType.Char, 1)
        arParms(1).Value = strParaWarrant.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_TraerEntidadesxTipoEntidad", arParms).Tables(0)
    End Function

    Public Function gGetFinanciador(ByVal strIdTipoEntidad As String, ByVal strParaWarrant As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@IdTipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strIdTipoEntidad.Trim
        arParms(1) = New SqlParameter("@ParaWarrant", SqlDbType.Char, 1)
        arParms(1).Value = strParaWarrant.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_TraerEntidadesxTipoEntidad2", arParms).Tables(0)
    End Function

    Public Function gGetAlmacen(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarAlmacenxDepartamento", arParms).Tables(0)
    End Function

    Public Function gGetAlmacenes(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarAlmacenxDepartamento2", arParms).Tables(0)
    End Function

    Public Function gGetMoneda() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "USP_TTMONE_SOL_DOL").Tables(0)
    End Function

    Public Function gCADGetListarFinanciadores(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                ByVal strTipoEntidad As String) As DataTable
        Dim dsResult As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strTipoEntidad
        dsResult = SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_TCTITU_Sel_Financiadores", arParms)
        If dsResult.Tables.Count = 1 Then
            Return dsResult.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function gGetClientesxFinanciador(ByVal strCodFinanciador As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CO_ENTI_FINA", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodFinanciador.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_TraerClientesxFinanciador", arParms).Tables(0)
    End Function

    Public Function gGetListarEntidades() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_ListarEntidades").Tables(0)
    End Function

    Public Function gGetAsociados(ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdSico.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_TraerAsociadosxEntidad", arParms).Tables(0)
    End Function

    Public Function gGetBusquedaEntidad(ByVal strTipoEntidad As String, ByVal strNombreEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(0).Value = strTipoEntidad.Trim
        arParms(1) = New SqlParameter("@NombreEntidad", SqlDbType.VarChar, 100)
        arParms(1).Value = strNombreEntidad.Trim
        Return SqlHelper.ExecuteDataset(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Sel_BuscarEntidades", arParms).Tables(0)
    End Function

    Public Function gInsAsociados(ByVal strIdEntidad As String, ByVal strIdAsociado As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@IdAsociado", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdAsociado.Trim
        arParms(2) = New SqlParameter("@Error", SqlDbType.Char, 1)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Ins_Asociado", arParms)
        Return arParms(2).Value
    End Function

    Public Function gDelAsociados(ByVal strIdEntidad As String, ByVal strIdAsociado As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IdEntidad", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad.Trim
        arParms(1) = New SqlParameter("@IdAsociado", SqlDbType.VarChar, 20)
        arParms(1).Value = strIdAsociado.Trim
        arParms(2) = New SqlParameter("@Error", SqlDbType.Char, 1)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaInicial, CommandType.StoredProcedure, "Usp_WFENTIDAD_Del_Asociado", arParms)
        Return arParms(2).Value
    End Function

    Public Function gUpdEventosxEntidad(ByVal strIdEntidad As String, ByVal strNroSecu As String, _
                                        ByVal strNroRuta As String, ByVal strTipoDocumento As String, _
                                        ByVal strCntAprobadores As String, ByVal strIdUsuario As String, _
                                        ByVal strIdTipoEntidad As String, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad
        arParms(1) = New SqlParameter("@NroSecu", SqlDbType.Int)
        arParms(1).Value = strNroSecu
        arParms(2) = New SqlParameter("@NroRuta", SqlDbType.Char, 8)
        arParms(2).Value = strNroRuta
        arParms(3) = New SqlParameter("@TipoDocumento", SqlDbType.Char, 2)
        arParms(3).Value = strTipoDocumento
        arParms(4) = New SqlParameter("@CntAprobadores", SqlDbType.Int)
        arParms(4).Value = CInt(strCntAprobadores)
        arParms(5) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(5).Value = strIdUsuario
        arParms(6) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(6).Value = strIdTipoEntidad
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFEVENTOSXENTIDAD_Upd_ActualizarEventosxEntidad", arParms)
    End Function

    Public Function gUpdEntidad(ByVal strIdEntidad As String, ByVal strIdUsuario As String, _
                                        ByVal strIdTipoEntidad As String, ByVal strTasa As String, _
                                        ByVal strOperacion As String, ByVal strNroDias As String, _
                                        ByVal blnFirma As Boolean, ByVal blnProtesto As Boolean, _
                                        ByVal blnFirmaCert As Boolean, ByVal blnLibMult As Boolean, _
                                        ByVal strIpIni As String, ByVal strIpFin As String, _
                                        ByVal strFirmDepo As Boolean, ByVal ObjTrans As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        arParms(0) = New SqlParameter("@IdSico", SqlDbType.VarChar, 20)
        arParms(0).Value = strIdEntidad
        arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Char, 4)
        arParms(1).Value = strIdUsuario
        arParms(2) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strIdTipoEntidad
        arParms(3) = New SqlParameter("@Tasa", SqlDbType.VarChar, 150)
        arParms(3).Value = strTasa
        arParms(4) = New SqlParameter("@Operacion", SqlDbType.VarChar, 200)
        arParms(4).Value = strOperacion
        arParms(5) = New SqlParameter("@NroDias", SqlDbType.VarChar, 10)
        arParms(5).Value = strNroDias
        arParms(6) = New SqlParameter("@Firma", SqlDbType.Bit)
        arParms(6).Value = blnFirma
        arParms(7) = New SqlParameter("@Protesto", SqlDbType.Bit)
        arParms(7).Value = blnProtesto
        arParms(8) = New SqlParameter("@FirmaCert", SqlDbType.Bit)
        arParms(8).Value = blnFirmaCert
        arParms(9) = New SqlParameter("@LibMult", SqlDbType.Bit)
        arParms(9).Value = blnLibMult
        arParms(10) = New SqlParameter("@IpIni", SqlDbType.VarChar, 12)
        arParms(10).Value = strIpIni
        arParms(11) = New SqlParameter("@IpFin", SqlDbType.VarChar, 12)
        arParms(11).Value = strIpFin
        arParms(12) = New SqlParameter("@FLGFIRMDEP", SqlDbType.Bit)
        arParms(12).Value = strFirmDepo
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFENTIDAD_Upd_Informacion", arParms)
    End Function

    Public Function gInsUsrxTipDoc(ByVal strCorrelativo As String, ByVal strCodUser As String, ByVal strCodSico As String, _
                                    ByVal strCodTipEnti As String, ByVal strAlmacen As String, _
                                    ByVal strCliente As String, ByVal strIdUsuario As String, _
                                    ByVal blnDocTitu As Boolean, ByVal blnDocLibe As Boolean, _
                                    ByVal blnDocSimp As Boolean, ByVal blnDocAdua As Boolean, _
                                    ByVal ObjTrans As SqlTransaction) As String

        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        arParms(0) = New SqlParameter("@CodCorr", SqlDbType.Int)
        arParms(0).Value = strCorrelativo
        arParms(1) = New SqlParameter("@CodUser", SqlDbType.VarChar, 4)
        arParms(1).Value = strCodUser
        arParms(2) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodSico
        arParms(3) = New SqlParameter("@CodTipEnti", SqlDbType.Char, 2)
        arParms(3).Value = strCodTipEnti
        arParms(4) = New SqlParameter("@Almacen", SqlDbType.VarChar, 5)
        arParms(4).Value = strAlmacen
        arParms(5) = New SqlParameter("@Cliente", SqlDbType.VarChar, 20)
        arParms(5).Value = strCliente
        arParms(6) = New SqlParameter("@IdUsuario", SqlDbType.VarChar, 4)
        arParms(6).Value = strIdUsuario
        arParms(7) = New SqlParameter("@DocTitu", SqlDbType.Bit)
        arParms(7).Value = blnDocTitu
        arParms(8) = New SqlParameter("@DocLibe", SqlDbType.Bit)
        arParms(8).Value = blnDocLibe
        arParms(9) = New SqlParameter("@DocSimp", SqlDbType.Bit)
        arParms(9).Value = blnDocSimp
        arParms(10) = New SqlParameter("@DocAdua", SqlDbType.Bit)
        arParms(10).Value = blnDocAdua
        SqlHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "Usp_WFUSRXTIPODOCUMENTO_Ins_InsertarTipoDocumento", arParms)
    End Function
End Class
