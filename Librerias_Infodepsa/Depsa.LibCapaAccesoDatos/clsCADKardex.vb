Imports System.Data.SqlClient
Public Class clsCADKardex
    Private cnCO_BDAT As New SqlConnection
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private pstrco_alma As String
    Private pstrti_docu_movi As String
    Private pstrnu_docu_movi As String
    Private pstrti_docu_rece As String
    Private pstrnu_docu_rece As String
    Private pintnu_secu_merc As Integer
    Private pintnu_secu_ubic As Integer
    Private pdtfe_movi As Date
    Private pdtfe_actu As Date
    Private pstrco_tipo_bult As String
    Private pdblnu_unid_ingr As Double
    Private pdblnu_unid_reto As Double
    Private pdblnu_unid_comp As Double
    Private pdblnu_unid_reco As Double
    Private pdblnu_unid_real As Double
    Private pdblnu_unid_sald As Double
    Private pstrco_unme_peso As String
    Private pdblnu_peso_ingr As Double
    Private pdblnu_peso_reto As Double
    Private pdblnu_peso_comp As Double
    Private pdblnu_peso_reco As Double
    Private pdblnu_peso_real As Double
    Private pdblnu_peso_sald As Double
    Private pstrco_mone As String
    Private pdblim_remo_ingr As Double
    Private pdblim_remo_reto As Double
    Private pdblim_remo_comp As Double
    Private pdblim_remo_reco As Double
    Private pdblim_remo_real As Double
    Private pdblim_remo_sald As Double
    Private pdblim_resi_ingr As Double
    Private pdblim_resi_reto As Double
    Private pdblim_resi_comp As Double
    Private pdblim_resi_reco As Double
    Private pdblim_resi_real As Double
    Private pdblim_resi_sald As Double
    Private pstrco_unme_area As String
    Private pdblnu_area_ingr As Double
    Private pdblnu_area_reto As Double
    Private pdblnu_area_comp As Double
    Private pdblnu_area_reco As Double
    Private pdblnu_area_real As Double
    Private pdblnu_area_sald As Double
    Private pdblco_unme_volu As String
    Private pdblnu_volu_ingr As Double
    Private pdblnu_volu_reto As Double
    Private pdblnu_volu_comp As Double
    Private pdblnu_volu_reco As Double
    Private pdblnu_volu_real As Double
    Private pdblnu_volu_sald As Double
    Private pstrco_clie_actu As String
    Private pstrco_clie_fact As String
    Private pstrco_tipo_merc As String
    Private pstrco_stip_merc As String
    Private pstrti_situ As String
    Private pstrnu_titu As String
    Private pbst_warr As Boolean
    Private pstrnu_mani As String
    Private pstrco_usua_crea As String
    Private pdtfe_usua_crea As Date
    Private pstrco_usua_modi As String
    Private pdtfe_usua_modi As Date
    Private pintnu_corr_warr As Integer
    Private pstrst_tipo_item As String
    Private dttemporal As DataTable
    Private dsTemporal As DataSet
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pintResultado As Integer

    Public Function devolverco_alma() As String
        Return pstrco_alma
    End Function

    Public Function devolverpstrti_docu_movi() As String
        Return pstrti_docu_movi
    End Function

    Public Function devolverpstrnu_docu_movi() As String
        Return pstrnu_docu_movi
    End Function

    Public Function devolverpstrti_docu_rece() As String
        Return pstrti_docu_rece
    End Function

    Public Function devolverpstrnu_docu_rece() As String
        Return pstrnu_docu_rece
    End Function

    Public Function devolverpintnu_secu_merc() As String
        Return pintnu_secu_merc
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dttemporal
    End Function

    Public Function fn_DevolverDataSet() As DataSet
        Return dsTemporal
    End Function

    Public Sub gCADListarKardex(ByVal strco_empr As String, ByVal strco_clie As String, _
                                ByVal strti_moda As String, ByVal strflg_sald As String, _
                                ByVal strfe_inic As String, ByVal strfe_fina As String, _
                                ByVal strti_refe As String, ByVal strnu_refe As String, _
                                ByVal blndor_pend As Boolean, ByVal strco_alma As String)
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(1).Value = strco_clie
        arParms(2) = New SqlParameter("@ISTI_MODA", SqlDbType.VarChar, 3)
        arParms(2).Value = strti_moda
        arParms(3) = New SqlParameter("@ISFLG_SALD", SqlDbType.Char, 1)
        arParms(3).Value = strflg_sald
        arParms(4) = New SqlParameter("@ISFE_INIC", SqlDbType.VarChar, 10)
        arParms(4).Value = strfe_inic
        arParms(5) = New SqlParameter("@ISFE_FINA", SqlDbType.VarChar, 10)
        arParms(5).Value = strfe_fina
        arParms(6) = New SqlParameter("@ISTI_REFE", SqlDbType.Int)
        arParms(6).Value = CInt(strti_refe)
        arParms(7) = New SqlParameter("@ISNU_REFE", SqlDbType.VarChar, 20)
        arParms(7).Value = strnu_refe
        arParms(8) = New SqlParameter("@ISDOR_PEND", SqlDbType.Bit)
        arParms(8).Value = blndor_pend
        arParms(9) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(9).Value = strco_alma
        dsTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Q04_D01_INF", arParms)
    End Sub

    Public Sub gCADMostrarTipoMercaderiaxCliente(ByVal strco_empr As String, ByVal strco_clie As String)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(1).Value = strco_clie
        dttemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TCALMA_MERC_Busca_Modalidad", arParms).Tables(0)
    End Sub

    Public Function gCADListarKardexWMS(ByVal strCodigo As String, ByVal strCodprod As String, ByVal strDescMerc As String, _
        ByVal strLote As String, ByVal strFechIni As String, ByVal strFechFin As String, ByVal strAlmacen As String, ByVal strIdSico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@Orden", SqlDbType.VarChar, 200)
        arParms(0).Value = strCodigo
        arParms(1) = New SqlParameter("@Producto", SqlDbType.VarChar, 200)
        arParms(1).Value = strCodprod
        arParms(2) = New SqlParameter("@Descrip", SqlDbType.VarChar, 200)
        arParms(2).Value = strDescMerc
        arParms(3) = New SqlParameter("@Lote", SqlDbType.VarChar, 200)
        arParms(3).Value = strLote
        arParms(4) = New SqlParameter("@FecInicial", SqlDbType.VarChar, 10)
        arParms(4).Value = strFechIni
        arParms(5) = New SqlParameter("@FecFinal", SqlDbType.VarChar, 10)
        arParms(5).Value = strFechFin
        arParms(6) = New SqlParameter("@Almacen", SqlDbType.VarChar, 20)
        arParms(6).Value = strAlmacen
        arParms(7) = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        arParms(7).Value = strIdSico

        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_HISTORY_MASTER_KARDEX", arParms).Tables(0)
    End Function
End Class
