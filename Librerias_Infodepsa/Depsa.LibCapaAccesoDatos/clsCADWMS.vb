Imports System.Data.SqlClient

Public Class clsCADWMS
    Private strCadenaConexionWMS As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWMS")
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

    Public Function gCADGetInventario(ByVal strCodCliente As String, ByVal strCodProducto As String, _
                                    ByVal strDscProducto As String, ByVal strLote As String, _
                                    ByVal strFecVenc As String, ByVal strTipReporte As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@ISCO_PROD", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodProducto
        arParms(2) = New SqlParameter("@ISDE_PROD", SqlDbType.VarChar, 200)
        arParms(2).Value = strDscProducto
        arParms(3) = New SqlParameter("@ISNU_LOTE", SqlDbType.VarChar, 20)
        arParms(3).Value = strLote
        arParms(4) = New SqlParameter("@IDFE_VENC", SqlDbType.VarChar, 10)
        arParms(4).Value = strFecVenc
        arParms(5) = New SqlParameter("@IDTI_REPO", SqlDbType.Char, 1)
        arParms(5).Value = strTipReporte
        Return SqlHelper.ExecuteDataset(strCadenaConexionWMS, CommandType.StoredProcedure, "USP_WMS_Sel_Inventario_old", arParms).Tables(0)
    End Function

    Public Function gCADGetPedidosPreparados(ByVal strNroInterno As String, ByVal strFechaInicio As String, _
                                        ByVal strFechaFin As String, ByVal strEstado As String, _
                                        ByVal strCodCliente As String, ByVal strPedido As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@IS_NRO_INTE", SqlDbType.VarChar, 20)
        arParms(0).Value = strNroInterno
        arParms(1) = New SqlParameter("@IS_FCH_INIC", SqlDbType.VarChar, 10)
        arParms(1).Value = strFechaInicio
        arParms(2) = New SqlParameter("@IS_FCH_FINA", SqlDbType.VarChar, 10)
        arParms(2).Value = strFechaFin
        arParms(3) = New SqlParameter("@IS_FLG_ESTA", SqlDbType.VarChar, 2)
        arParms(3).Value = strEstado
        arParms(4) = New SqlParameter("@IS_COD_ALMA", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodCliente
        arParms(5) = New SqlParameter("@IS_NUM_PEDI", SqlDbType.VarChar, 20)
        arParms(5).Value = strPedido
        Return SqlHelper.ExecuteDataset(strCadenaConexionWMS, CommandType.StoredProcedure, "Usp_WMS_VENCEDOR_Sel_PedidosPreparados", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarPedidos(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strTipoEntidad As String, _
                                    ByVal strNroPedido As String, ByVal strFecInicial As String, ByVal strFecFinal As String, _
                                    ByVal strEstSitu As String, ByVal strIdEstado As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@sTipEntidad", SqlDbType.Char, 2)
        arParms(1).Value = strTipoEntidad
        arParms(2) = New SqlParameter("@sNroPedido", SqlDbType.VarChar, 11)
        arParms(2).Value = strNroPedido
        arParms(3) = New SqlParameter("@sFecInicial", SqlDbType.VarChar, 8)
        arParms(3).Value = strFecInicial
        arParms(4) = New SqlParameter("@sFecFinal", SqlDbType.VarChar, 8)
        arParms(4).Value = strFecFinal
        arParms(5) = New SqlParameter("@sIdEstado", SqlDbType.VarChar, 2)
        arParms(5).Value = strIdEstado.Trim
        arParms(6) = New SqlParameter("@sIdSituacion", SqlDbType.Int)
        arParms(6).Value = strEstSitu
        arParms(7) = New SqlParameter("@sAlmacen", SqlDbType.VarChar, 5)
        arParms(7).Value = strCodAlmacen
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_BuscarPedidosXAlm", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarMercaderia_WMS(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                            ByVal strCodAlmacen As String, ByVal strCodProducto As String, _
                                                            ByVal strLote As String, ByVal strReferencia As String, ByVal strNroReferencia As String, ByVal strFecVenc As String, _
                                                            ByVal strIdUsuario As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@sCodAlmacen", SqlDbType.VarChar, 5)
        arParms(1).Value = strCodAlmacen
        arParms(2) = New SqlParameter("@sCodProducto", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodProducto
        arParms(3) = New SqlParameter("@sLote", SqlDbType.VarChar, 20)
        arParms(3).Value = strLote
        arParms(4) = New SqlParameter("@sReferencia", SqlDbType.Int)
        arParms(4).Value = CInt(strReferencia)
        arParms(5) = New SqlParameter("@sNroReferencia", SqlDbType.VarChar, 20)
        arParms(5).Value = strNroReferencia
        arParms(6) = New SqlParameter("@sFecVenc", SqlDbType.VarChar, 10)
        arParms(6).Value = strFecVenc
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_iv_f_sel_saldowms", arParms).Tables(0)
    End Function

    Public Function gCADGetListarDetallePedido(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strCodAlma As String, _
                                                  ByVal strNroPedido As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.Char, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@sCodAlma", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodAlma
        arParms(2) = New SqlParameter("@sNroPedido", SqlDbType.VarChar, 8)
        arParms(2).Value = strNroPedido
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_DetallePedidos", arParms).Tables(0)
    End Function

    Public Function gCADAddCABRetiroWMS(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                    ByVal pstrCodUsuario As String, ByVal pNom_Cliente As String, _
                                    ByVal pFechaRecojo As String, ByVal pNPedido As String, _
                                    ByVal pRetiradoX As String, ByVal Co_AlmaW As String, _
                                    ByVal pstrTipUsua As String, ByVal objTransaction As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = pstrCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = pstrCodCliente
        arParms(2) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
        arParms(2).Value = pstrCodUsuario
        arParms(3) = New SqlParameter("@sNom_Cliente", SqlDbType.VarChar, 200)
        arParms(3).Value = pNom_Cliente
        arParms(4) = New SqlParameter("@sFechaRecojo", SqlDbType.VarChar, 10)
        arParms(4).Value = pFechaRecojo
        arParms(5) = New SqlParameter("@sNPedido", SqlDbType.VarChar, 40)
        arParms(5).Value = pNPedido
        arParms(6) = New SqlParameter("@sRetiradoX", SqlDbType.VarChar, 20)
        arParms(6).Value = pRetiradoX
        arParms(7) = New SqlParameter("@sCo_AlmaW", SqlDbType.VarChar, 20)
        arParms(7).Value = Co_AlmaW
        arParms(8) = New SqlParameter("@sTIP_USUA", SqlDbType.VarChar, 2)
        arParms(8).Value = pstrTipUsua
        arParms(9) = New SqlParameter("@Return", SqlDbType.VarChar, 8)
        arParms(9).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFRETIRO_Ins_AgregarItemCabWMS", arParms)
        Return arParms(9).Value()
    End Function

    Public Function gCADAddDETRetiroWMS(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                        ByVal pstrCodUsuario As String, ByVal pNRO_DOCUMENTO As String, ByVal pCO_PROD_CLIE As String, _
                                        ByVal pDE_MERC As String, ByVal pLOTE As String, _
                                        ByVal pFE_VCTO_MERC As String, ByVal pCO_REFERENCIA As String, _
                                        ByVal pCO_TIPO_BULT As String, ByVal pNU_UNID_SALD As String, _
                                        ByVal pNU_UNID_RETI As String, ByVal pCo_AlmaW As String, ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(13) {}
            arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
            arParms(0).Value = pstrCodEmpresa
            arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
            arParms(1).Value = pstrCodCliente
            arParms(2) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
            arParms(2).Value = pstrCodUsuario
            arParms(3) = New SqlParameter("@sNRO_DOCUMENTO", SqlDbType.VarChar, 20)
            arParms(3).Value = pNRO_DOCUMENTO
            arParms(4) = New SqlParameter("@sCO_PROD_CLIE", SqlDbType.VarChar, 20)
            arParms(4).Value = pCO_PROD_CLIE
            arParms(5) = New SqlParameter("@sDE_MERC", SqlDbType.VarChar, 200)
            arParms(5).Value = pDE_MERC
            arParms(6) = New SqlParameter("@sLOTE", SqlDbType.VarChar, 20)
            arParms(6).Value = pLOTE
            arParms(7) = New SqlParameter("@sFE_VCTO_MERC", SqlDbType.VarChar, 10)
            arParms(7).Value = pFE_VCTO_MERC
            arParms(8) = New SqlParameter("@sCO_REFERENCIA", SqlDbType.VarChar, 20)
            arParms(8).Value = IIf(pCO_REFERENCIA = "&nbsp;", "", pCO_REFERENCIA)
            arParms(9) = New SqlParameter("@sCO_TIPO_BULT", SqlDbType.VarChar, 20)
            arParms(9).Value = pCO_TIPO_BULT
            arParms(10) = New SqlParameter("@pNU_UNID_SALD", SqlDbType.Decimal)
            arParms(10).Value = IIf(pNU_UNID_SALD = "", 0, pNU_UNID_SALD)
            arParms(11) = New SqlParameter("@sNU_UNID_RETI", SqlDbType.Decimal)
            arParms(11).Value = IIf(pNU_UNID_RETI = "", 0, pNU_UNID_RETI)
            arParms(12) = New SqlParameter("@sCodAlmacen", SqlDbType.VarChar, 5)
            arParms(12).Value = pCo_AlmaW
            arParms(13) = New SqlParameter("@sMsg", SqlDbType.VarChar, 50)
            arParms(13).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFRETIRO_Ins_AgregarItemDetWMS", arParms)
            Return arParms(13).Value()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCADAddIngresoWMS(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                        ByVal strCodUsuario As String, ByVal strNroGuia As String, _
                                        ByVal strCO_PROD_CLIE As String, ByVal strDE_PROD_CLIE As String, _
                                        ByVal strNU_CANT As String, ByVal strNU_LOTE As String, _
                                        ByVal strCod_Almacen As String, ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(9) {}
            arParms(0) = New SqlParameter("@CodEmpresa", SqlDbType.Char, 2)
            arParms(0).Value = strCodEmpresa
            arParms(1) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
            arParms(1).Value = strCodCliente
            arParms(2) = New SqlParameter("@CodUsuario", SqlDbType.VarChar, 20)
            arParms(2).Value = strCodUsuario
            arParms(3) = New SqlParameter("@NRO_GUIA", SqlDbType.VarChar, 20)
            arParms(3).Value = strNroGuia
            arParms(4) = New SqlParameter("@CO_PROD_CLIE", SqlDbType.VarChar, 20)
            arParms(4).Value = strCO_PROD_CLIE
            arParms(5) = New SqlParameter("@DE_PROD_CLIE", SqlDbType.VarChar, 200)
            arParms(5).Value = strDE_PROD_CLIE
            arParms(6) = New SqlParameter("@NU_CANT", SqlDbType.Decimal)
            arParms(6).Value = IIf(strNU_CANT = "", 0, strNU_CANT)
            arParms(7) = New SqlParameter("@NU_LOTE", SqlDbType.VarChar, 20)
            arParms(7).Value = strNU_LOTE
            arParms(8) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 5)
            arParms(8).Value = strCod_Almacen
            arParms(9) = New SqlParameter("@ERROR", SqlDbType.VarChar, 50)
            arParms(9).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_TWTEMP_INGR_Ins_AgregarItem", arParms)
            Return arParms(9).Value()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCADGENE_SALI_INFO_WMS(ByVal strNroDocu As String, ByVal objTransaction As SqlTransaction) As DataTable
        Try
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@ISNU_DOCU_PEDI", SqlDbType.VarChar, 20)
            arParms(0).Value = strNroDocu
            arParms(1) = New SqlParameter("@MENSAJE", SqlDbType.VarChar, 200)
            arParms(1).Direction = ParameterDirection.Output
            Return SqlHelper.ExecuteDataset(objTransaction, CommandType.StoredProcedure, "USP_GENE_SALI_INFO_WMS", arParms).Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function gCADValidarDeuda(ByVal strNroDocu As String, ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@ISNU_DOCU_PEDI", SqlDbType.VarChar, 20)
            arParms(0).Value = strNroDocu
            arParms(1) = New SqlParameter("@MENSAJE", SqlDbType.VarChar, 200)
            arParms(1).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFDOCUMENTO_Sel_ValidarDeuda", arParms)
            Return arParms(1).Value()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return "Error gCADValidarDeuda"
        End Try
    End Function

    Public Function gCADCrearPedido(ByVal strNu_Docu_Pedi As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISNU_DOCU_PEDI", SqlDbType.VarChar, 20)
        arParms(0).Value = strNu_Docu_Pedi
        arParms(1) = New SqlParameter("@ISMENSAJE", SqlDbType.VarChar, 200)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_GENE_SALI_INFO_WMS", arParms)
        Return arParms(1).Value
    End Function

    Public Function gGetExisteWMS(ByVal strCoClie As String, ByVal strCoAlm As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CoClie", SqlDbType.VarChar, 20)
        arParms(0).Value = strCoClie
        arParms(1) = New SqlParameter("@CoAlm", SqlDbType.VarChar, 20)
        arParms(1).Value = strCoAlm
        arParms(2) = New SqlParameter("@Return", SqlDbType.Int)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_TTCLIE_WMS_Sel_ExisteWMS", arParms)
        Return arParms(2).Value()
    End Function

    Public Function gCADEliminarPedidoWMS(ByVal nropedido As String, ByVal sCodUsuario As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@nropedido", SqlDbType.VarChar, 8)
        arParms(0).Value = nropedido
        arParms(1) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 8)
        arParms(1).Value = sCodUsuario
        arParms(2) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 2)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFRETIRO_Del_EliminarItemDetWMS", arParms)
        Return arParms(2).Value()
    End Function

    Public Function gCADActCABRetiroWMS(ByVal pstrCodUsuario As String, ByVal pNumSecu As Integer, ByVal NroPedido As String, ByVal objTransaction As SqlTransaction) As String
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@pstrCodUsuario", SqlDbType.Char, 20)
        arParms(0).Value = pstrCodUsuario
        arParms(1) = New SqlParameter("@pNumSecu", SqlDbType.VarChar, 20)
        arParms(1).Value = pNumSecu
        arParms(2) = New SqlParameter("@NroPedido", SqlDbType.VarChar, 20)
        arParms(2).Value = NroPedido
        arParms(3) = New SqlParameter("@Return", SqlDbType.VarChar, 8)
        arParms(3).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFRETIRO_Act_ActualizarItemCabWMS", arParms)
        Return arParms(3).Value()
    End Function

    Public Function gCADValidaProducto(ByVal CodCliente As String, ByVal CodProducto As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = CodCliente
        arParms(1) = New SqlParameter("@CodProducto", SqlDbType.VarChar, 20)
        arParms(1).Value = CodProducto
        arParms(2) = New SqlParameter("@Return", SqlDbType.VarChar, 50)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaConexionWMS, CommandType.StoredProcedure, "Usp_WMS_Sel_VerificarProducto", arParms)
        Return arParms(2).Value()
    End Function
End Class
