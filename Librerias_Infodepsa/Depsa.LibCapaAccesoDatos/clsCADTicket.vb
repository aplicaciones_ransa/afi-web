Imports System.Data.SqlClient
Public Class clsCADTicket
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private dtTemporal As DataTable

    Public Function fn_TCBALA_Sel_ReportePesadas(ByVal sCO_EMPR As String, ByVal sCO_ENTI_TRAB As String, _
                                                ByVal sFE_EMIS_INIC As String, ByVal sFE_EMIS_FINA As String, _
                                                Optional ByVal sTipoTicket As String = "", Optional ByVal sReferencia As String = "", _
                                                Optional ByVal sNroReferencia As String = "", Optional ByVal sCodAlmacen As String = "", _
                                                Optional ByVal sModalidad As String = "") As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(1).Value = sCO_ENTI_TRAB
        arParms(2) = New SqlParameter("@IDFE_REGI_INIC", SqlDbType.VarChar, 10)
        arParms(2).Value = sFE_EMIS_INIC
        arParms(3) = New SqlParameter("@IDFE_REGI_FINA", SqlDbType.VarChar, 10)
        arParms(3).Value = sFE_EMIS_FINA
        arParms(4) = New SqlParameter("@TipoTicket", SqlDbType.Char, 1)
        arParms(4).Value = sTipoTicket
        arParms(5) = New SqlParameter("@Referencia", SqlDbType.Char, 1)
        arParms(5).Value = sReferencia
        arParms(6) = New SqlParameter("@NroReferencia", SqlDbType.VarChar, 20)
        arParms(6).Value = sNroReferencia
        arParms(7) = New SqlParameter("@CO_ALMA", SqlDbType.VarChar, 5)
        arParms(7).Value = sCodAlmacen
        arParms(8) = New SqlParameter("@CO_MODA_MOVI", SqlDbType.VarChar, 3)
        arParms(8).Value = sModalidad
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TCBALA_Sel_ReportePesadas_Info", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function gCADListar_Vencimientos(ByVal sCO_EMPR As String, ByVal sCO_ENTI_TRAB As String) As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(1).Value = sCO_ENTI_TRAB
        'arParms(2) = New SqlParameter("@TipoVencimiento", SqlDbType.Char, 1)
        'arParms(2).Value = sModalidad
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TCALMA_MERC_Sel_ListarVencimientos", arParms)
    End Function

    Public Function fn_TCBALA_Sel_ReportePesadasPDF(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, ByVal sCO_ALMA As String, ByVal sTI_DOCU As String, ByVal sNU_DOCU As String, ByVal sTipoTicket As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = sCO_UNID
        arParms(2) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(2).Value = sCO_ALMA
        arParms(3) = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        arParms(3).Value = sTI_DOCU
        arParms(4) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        arParms(4).Value = sNU_DOCU
        arParms(5) = New SqlParameter("@TIP_TICK", SqlDbType.Char, 1)
        arParms(5).Value = sTipoTicket
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TCBALA_Sel_ReportePesadas_PDF", arParms).Tables(0)
        Return dtTemporal
    End Function
End Class
