Imports System.Data.SqlClient
Public Class clsCADPerfil
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCodGrupo As String
    Private strNombGrupo As String
    Private dtTemporal As DataTable
    Private strErrorSql As String
    Private intFilasAfectadas As Integer
    Private strResultado As String

    Public Function fn_DevolverCodGrupo() As String
        Return strCodGrupo
    End Function

    Public Function fn_DevolverNombGrupo() As String
        Return strNombGrupo
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Function fn_DevolverResultado() As String
        Return strResultado
    End Function

    Public Sub gCADLlenarComboGrupos(ByVal strco_sist As String)
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@ISCO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strco_sist.Trim
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_TRSIST_GRUP", arParms).Tables(0)
    End Sub
End Class
