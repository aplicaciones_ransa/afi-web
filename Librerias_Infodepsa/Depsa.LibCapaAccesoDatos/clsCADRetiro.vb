Imports System.Data.SqlClient
Public Class clsCADRetiro
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private lstrMensajeError As String

    Public Function gCADGetBuscarRetiros(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strTipoEntidad As String, _
                                        ByVal strNroPedido As String, ByVal strFecInicial As String, ByVal strFecFinal As String, _
                                        ByVal strTipoRetiro As String, ByVal strIdEstado As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sTipEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strTipoEntidad
        arParms(3) = New SqlParameter("@sNroPedido", SqlDbType.VarChar, 11)
        arParms(3).Value = strNroPedido
        arParms(4) = New SqlParameter("@sFecInicial", SqlDbType.VarChar, 8)
        arParms(4).Value = strFecInicial
        arParms(5) = New SqlParameter("@sFecFinal", SqlDbType.VarChar, 8)
        arParms(5).Value = strFecFinal
        arParms(6) = New SqlParameter("@stipRetiro", SqlDbType.VarChar, 3)
        arParms(6).Value = strTipoRetiro
        arParms(7) = New SqlParameter("@IdEstado", SqlDbType.VarChar, 2)
        arParms(7).Value = strIdEstado.Trim
        arParms(8) = New SqlParameter("@CodAlmacen", SqlDbType.VarChar, 5)
        arParms(8).Value = strCodAlmacen
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TCRETI_MERC_Sel_BuscarRetiros", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarMercaderia(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                        ByVal strCodAlmacen As String, ByVal strReferencia As String, _
                                        ByVal strNroReferencia As String, ByVal strFecVenc As String, _
                                        ByVal strIdUsuario As String, ByVal strTipoRetiro As String, ByVal strEstado As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sCodAlmacen", SqlDbType.VarChar, 5)
        arParms(2).Value = strCodAlmacen
        arParms(3) = New SqlParameter("@sReferencia", SqlDbType.Int)
        arParms(3).Value = CInt(strReferencia)
        arParms(4) = New SqlParameter("@sNroReferencia", SqlDbType.VarChar, 20)
        arParms(4).Value = strNroReferencia
        arParms(5) = New SqlParameter("@sFecVenc", SqlDbType.VarChar, 10)
        arParms(5).Value = strFecVenc
        arParms(6) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
        arParms(6).Value = strIdUsuario
        arParms(7) = New SqlParameter("@stipRetiro", SqlDbType.VarChar, 3)
        arParms(7).Value = strTipoRetiro
        arParms(8) = New SqlParameter("@strEstado", SqlDbType.VarChar, 5)
        arParms(8).Value = strEstado
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TMSALD_ALMA_Sel_BuscarMercaderia", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarTransp(ByVal strNroBrevete As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@sBrevete", SqlDbType.Char, 20)
        arParms(0).Value = strNroBrevete
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TMCHOF_EMTR_Sel_BuscarTransportista", arParms).Tables(0)
    End Function

    Public Function gCADGetListarAlmacenes(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                            ByVal strTipoMercaderia As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sTipoMercaderia", SqlDbType.Char, 1)
        arParms(2).Value = strTipoMercaderia
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TMSALD_ALMA_Sel_ListarAlmacenes", arParms).Tables(0)
    End Function

    Public Function gCADGetPredespacho(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                    ByVal strCodAlmacen As String, ByVal strCodCliente As String) As Integer
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@sCodAlmacen", SqlDbType.VarChar, 5)
        arParms(2).Value = strCodAlmacen
        arParms(3) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodCliente
        arParms(4) = New SqlParameter("@Return", SqlDbType.Int)
        arParms(4).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "Usp_WFRETIRO_Sel_ConPredespacho", arParms)
        Return arParms(4).Value()
    End Function

    Public Function gCADAddItemRetiro(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                        ByVal pstrCodUsuario As String, ByVal pNU_DOCU_RECE As String, _
                                        ByVal pNU_SECU As String, ByVal pDE_MERC As String, _
                                        ByVal pCO_PROD_CLIE As String, ByVal pLOTE As String, _
                                        ByVal pFE_VCTO_MERC As String, ByVal pCO_TIPO_BULT As String, _
                                        ByVal pNU_UNID_SALD As String, ByVal pNU_UNID_RETI As String, _
                                        ByVal pADUANERO As String, ByVal pCO_ALMA_ACTU As String, _
                                        ByVal pCO_UNID As String, ByVal pFLG_SEL As String, _
                                        ByVal pCO_BODE_ACTU As String, ByVal pCO_UBIC_ACTU As String, _
                                        ByVal pDE_TIPO_MERC As String, ByVal pDE_STIP_MERC As String, _
                                        ByVal pTI_DOCU_RECE As String, ByVal pFE_REGI_INGR As String, _
                                        ByVal pNU_SECU_UBIC As String, ByVal pCO_TIPO_MERC As String, _
                                        ByVal pCO_STIP_MERC As String, ByVal pDE_TIPO_BULT As String, _
                                        ByVal pCO_UNME_PESO As String, ByVal pDE_UNME As String, _
                                        ByVal pNU_PESO_RETI As String, ByVal pNU_UNID_PESO As String, _
                                        ByVal pCO_MONE As String, ByVal pTI_TITU As String, _
                                        ByVal pNU_TITU As String, ByVal pCO_UNME_AREA As String, _
                                        ByVal pNU_AREA_USAD As String, ByVal pCO_UNME_VOLU As String, _
                                        ByVal pNU_VOLU_USAD As String, ByVal pST_VALI_RETI As String, _
                                        ByVal pNU_UNID_RECI As String, ByVal pNU_PESO_RECI As String, _
                                        ByVal pNU_PESO_UNID As String, ByVal pTI_UNID_PESO As String, _
                                        ByVal pNU_MANI As String, ByVal pST_TEAL As String, _
                                        ByVal objTransaction As SqlTransaction) As Boolean
        Try
            Dim arParms() As SqlParameter = New SqlParameter(43) {}
            arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
            arParms(0).Value = pstrCodEmpresa
            arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
            arParms(1).Value = pstrCodCliente
            arParms(2) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
            arParms(2).Value = pstrCodUsuario
            arParms(3) = New SqlParameter("@sNU_DOCU_RECE", SqlDbType.VarChar, 20)
            arParms(3).Value = pNU_DOCU_RECE
            arParms(4) = New SqlParameter("@sNU_SECU", SqlDbType.Int)
            arParms(4).Value = pNU_SECU
            arParms(5) = New SqlParameter("@sDE_MERC", SqlDbType.VarChar, 200)
            arParms(5).Value = pDE_MERC
            arParms(6) = New SqlParameter("@sCO_PROD_CLIE", SqlDbType.VarChar, 20)
            arParms(6).Value = IIf(pCO_PROD_CLIE = "&nbsp;", "", pCO_PROD_CLIE)
            arParms(7) = New SqlParameter("@sLOTE", SqlDbType.VarChar, 20)
            arParms(7).Value = IIf(pLOTE = "&nbsp;", "", pLOTE)
            arParms(8) = New SqlParameter("@sFE_VCTO_MERC", SqlDbType.VarChar, 10)
            arParms(8).Value = pFE_VCTO_MERC
            arParms(9) = New SqlParameter("@sCO_TIPO_BULT", SqlDbType.VarChar, 3)
            arParms(9).Value = pCO_TIPO_BULT
            arParms(10) = New SqlParameter("@sNU_UNID_SALD", SqlDbType.Decimal)
            arParms(10).Value = pNU_UNID_SALD
            arParms(11) = New SqlParameter("@sNU_UNID_RETI", SqlDbType.Decimal)
            arParms(11).Value = pNU_UNID_RETI
            arParms(12) = New SqlParameter("@sADUANERO", SqlDbType.VarChar, 10)
            arParms(12).Value = pADUANERO
            arParms(13) = New SqlParameter("@sCO_ALMA_ACTU", SqlDbType.VarChar, 5)
            arParms(13).Value = pCO_ALMA_ACTU
            arParms(14) = New SqlParameter("@sCO_UNID", SqlDbType.VarChar, 3)
            arParms(14).Value = pCO_UNID
            arParms(15) = New SqlParameter("@sFLG_SEL", SqlDbType.Bit)
            arParms(15).Value = CInt(pFLG_SEL)
            arParms(16) = New SqlParameter("@sCO_BODE_ACTU", SqlDbType.VarChar, 20)
            arParms(16).Value = pCO_BODE_ACTU
            arParms(17) = New SqlParameter("@sCO_UBIC_ACTU", SqlDbType.VarChar, 20)
            arParms(17).Value = pCO_UBIC_ACTU
            arParms(18) = New SqlParameter("@sDE_TIPO_MERC", SqlDbType.VarChar, 100)
            arParms(18).Value = pDE_TIPO_MERC
            arParms(19) = New SqlParameter("@sDE_STIP_MERC", SqlDbType.VarChar, 100)
            arParms(19).Value = pDE_STIP_MERC
            arParms(20) = New SqlParameter("@sTI_DOCU_RECE", SqlDbType.VarChar, 3)
            arParms(20).Value = pTI_DOCU_RECE
            arParms(21) = New SqlParameter("@sFE_REGI_INGR", SqlDbType.VarChar, 10)
            arParms(21).Value = pFE_REGI_INGR
            arParms(22) = New SqlParameter("@sNU_SECU_UBIC", SqlDbType.Int)
            arParms(22).Value = pNU_SECU_UBIC
            arParms(23) = New SqlParameter("@sCO_TIPO_MERC", SqlDbType.VarChar, 3)
            arParms(23).Value = pCO_TIPO_MERC
            arParms(24) = New SqlParameter("@sCO_STIP_MERC", SqlDbType.VarChar, 3)
            arParms(24).Value = pCO_STIP_MERC
            arParms(25) = New SqlParameter("@sDE_TIPO_BULT", SqlDbType.VarChar, 100)
            arParms(25).Value = pDE_TIPO_BULT
            arParms(26) = New SqlParameter("@sCO_UNME_PESO", SqlDbType.VarChar, 3)
            arParms(26).Value = pCO_UNME_PESO
            arParms(27) = New SqlParameter("@sDE_UNME", SqlDbType.VarChar, 20)
            arParms(27).Value = IIf(pDE_UNME = "&nbsp;", "", pDE_UNME)
            arParms(28) = New SqlParameter("@sNU_PESO_RETI", SqlDbType.Decimal)
            arParms(28).Value = pNU_PESO_RETI
            arParms(29) = New SqlParameter("@sNU_UNID_PESO", SqlDbType.Decimal)
            arParms(29).Value = pNU_UNID_PESO
            arParms(30) = New SqlParameter("@sCO_MONE", SqlDbType.VarChar, 3)
            arParms(30).Value = pCO_MONE
            arParms(31) = New SqlParameter("@sTI_TITU", SqlDbType.VarChar, 3)
            arParms(31).Value = IIf(pTI_TITU = "&nbsp;", "", pTI_TITU)
            arParms(32) = New SqlParameter("@sNU_TITU", SqlDbType.VarChar, 20)
            arParms(32).Value = IIf(pNU_TITU = "&nbsp;", "", pNU_TITU)
            arParms(33) = New SqlParameter("@sCO_UNME_AREA", SqlDbType.VarChar, 3)
            arParms(33).Value = IIf(pCO_UNME_AREA = "&nbsp;", "", pCO_UNME_AREA)
            arParms(34) = New SqlParameter("@sNU_AREA_USAD", SqlDbType.Decimal)
            arParms(34).Value = pNU_AREA_USAD
            arParms(35) = New SqlParameter("@sCO_UNME_VOLU", SqlDbType.VarChar, 3)
            arParms(35).Value = IIf(pCO_UNME_VOLU = "&nbsp;", "", pCO_UNME_VOLU)
            arParms(36) = New SqlParameter("@sNU_VOLU_USAD", SqlDbType.Decimal)
            arParms(36).Value = pNU_VOLU_USAD
            arParms(37) = New SqlParameter("@sST_VALI_RETI", SqlDbType.Char, 1)
            arParms(37).Value = pST_VALI_RETI
            arParms(38) = New SqlParameter("@sNU_UNID_RECI", SqlDbType.Decimal)
            arParms(38).Value = pNU_UNID_RECI
            arParms(39) = New SqlParameter("@sNU_PESO_RECI", SqlDbType.Decimal)
            arParms(39).Value = pNU_PESO_RECI
            arParms(40) = New SqlParameter("@sNU_PESO_UNID", SqlDbType.Decimal)
            arParms(40).Value = pNU_PESO_UNID
            arParms(41) = New SqlParameter("@sTI_UNID_PESO", SqlDbType.Char, 1)
            arParms(41).Value = IIf(pTI_UNID_PESO = "&nbsp;", "", pTI_UNID_PESO)
            arParms(42) = New SqlParameter("@sNU_MANI", SqlDbType.VarChar, 20)
            arParms(42).Value = IIf(pNU_MANI = "&nbsp;", "", pNU_MANI)
            arParms(43) = New SqlParameter("@sST_TEAL", SqlDbType.Char, 1)
            arParms(43).Value = pST_TEAL
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFRETIRO_Ins_AgregarItem", arParms)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Function gCADDelItemRetiro(ByVal pstrCodCliente As String, ByVal objTransaction As SqlTransaction) As Boolean
        Try
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
            arParms(0).Value = pstrCodCliente
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_WFRETIRO_Del_EliminarItem", arParms)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    'MODIFICADO
    Public Function gCADCrearRetiro(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                    ByVal strCodUsuario As String, ByVal strBrevete As String, _
                                    ByVal strChofer As String, ByVal strCiaTransporte As String, _
                                    ByVal strDNI As String, ByVal strPlaca As String, _
                                    ByVal blnPreDespacho As Boolean, ByVal blnPagoElectronico As Boolean, _
                                    ByVal strDestino As String, ByVal objTransaction As SqlTransaction, _
                                    ByVal sST_VALI As String) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(12) {}
            arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
            arParms(0).Value = strCodEmpresa
            arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
            arParms(1).Value = strCodCliente
            arParms(2) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
            arParms(2).Value = strCodUsuario
            arParms(3) = New SqlParameter("@sBrevete", SqlDbType.VarChar, 20)
            arParms(3).Value = strBrevete
            arParms(4) = New SqlParameter("@sChofer", SqlDbType.VarChar, 100)
            arParms(4).Value = strChofer
            arParms(5) = New SqlParameter("@sCiaTransporte", SqlDbType.VarChar, 100)
            arParms(5).Value = strCiaTransporte
            arParms(6) = New SqlParameter("@sDNI", SqlDbType.VarChar, 20)
            arParms(6).Value = strDNI
            arParms(7) = New SqlParameter("@sPlaca", SqlDbType.VarChar, 20)
            arParms(7).Value = strPlaca
            arParms(8) = New SqlParameter("@sPreDespacho", SqlDbType.Bit)
            arParms(8).Value = blnPreDespacho
            arParms(9) = New SqlParameter("@sPago_Elec", SqlDbType.Bit)
            arParms(9).Value = blnPagoElectronico
            arParms(10) = New SqlParameter("@sDeRetiDest", SqlDbType.VarChar, 200)
            arParms(10).Value = strDestino
            arParms(11) = New SqlParameter("@sST_VALI", SqlDbType.Char, 1)
            arParms(11).Value = sST_VALI
            arParms(12) = New SqlParameter("@Return", SqlDbType.VarChar, 100)
            arParms(12).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_TCALMA_MERC_Ins_GenerarOrdenRetiro_old", arParms)
            'SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_TCALMA_MERC_Ins_GenerarOrdenRetiro_old_PRUFM", arParms)
            Return arParms(12).Value().ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCNCrearRetiroAprobado(ByVal strCodEmpresa As String, ByVal strCodUnid As String, _
                                    ByVal strTiDocuReti As String, ByVal strNu_Docu_Reti As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@CO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@CO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnid
        arParms(2) = New SqlParameter("@TI_DOCU_RETI", SqlDbType.VarChar, 3)
        arParms(2).Value = strTiDocuReti
        arParms(3) = New SqlParameter("@NU_DOCU_RETI", SqlDbType.VarChar, 20)
        arParms(3).Value = strNu_Docu_Reti
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_TCALMA_MERC_Reporte_Retiros_Aprobacion", arParms).Tables(0)
    End Function

    Public Function gCNUpdRetiro(ByVal strCodEmpresa As String, ByVal pstrCodUnidad As String, ByVal strCodUsuario As String, _
                                  ByVal strTipoDocRece As String, ByVal strRetiro As String, _
                                  ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(5) {}
            arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
            arParms(0).Value = strCodEmpresa
            arParms(1) = New SqlParameter("@sCodUnidad", SqlDbType.Char, 3)
            arParms(1).Value = pstrCodUnidad
            arParms(2) = New SqlParameter("@sCodUsuario", SqlDbType.VarChar, 20)
            arParms(2).Value = strCodUsuario
            arParms(3) = New SqlParameter("@sTiDocReti", SqlDbType.VarChar, 3)
            arParms(3).Value = strTipoDocRece
            arParms(4) = New SqlParameter("@sNroDocReti", SqlDbType.VarChar, 20)
            arParms(4).Value = strRetiro
            arParms(5) = New SqlParameter("@Return", SqlDbType.VarChar, 100)
            arParms(5).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_TCRETI_MERC_AprobarOrdenRetiro", arParms)
            Return arParms(5).Value().ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCADCrearPedido(ByVal strNu_Docu_Pedi As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISNU_DOCU_PEDI", SqlDbType.VarChar, 20)
        arParms(0).Value = strNu_Docu_Pedi
        arParms(1) = New SqlParameter("@ISMENSAJE", SqlDbType.VarChar, 200)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaOfioper, CommandType.StoredProcedure, "USP_GENE_SALI_INFO_WMS", arParms)
        Return arParms(1).Value
    End Function

    Public Function gCADGetListarAlmacenesWMS(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TTCLIE_WMS_SEL_ALMA_WMS", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarRetirosIngresadosWMS(ByVal strCodCliente As String, ByVal strNroGuia As String, _
                                                        ByVal strFecInicial As String, ByVal strFecFinal As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@sOid", SqlDbType.VarChar, 20)
        arParms(1).Value = strNroGuia
        arParms(2) = New SqlParameter("@sFecInicial", SqlDbType.VarChar, 10)
        arParms(2).Value = strFecInicial
        arParms(3) = New SqlParameter("@sFecFinal", SqlDbType.VarChar, 10)
        arParms(3).Value = strFecFinal
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_HISTORY_MASTER_SEL_INGRESOS", arParms).Tables(0)
    End Function

    Public Function gCADGetBuscarDetaRetirosIngresadosWMS(ByVal strCodCliente As String, ByVal strNroGuia As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@oid", SqlDbType.VarChar, 20)
        arParms(1).Value = strNroGuia
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_HISTORY_MASTER_SEL_INGRESOS_DETA", arParms).Tables(0)
    End Function

    Public Function gCADSetEnviaIngresoWMS(ByVal strCodEmpresa As String, ByVal strNoArchSali As String, _
                                            ByVal strNuRefe As String, ByVal strCoClie As String, _
                                            ByVal strCoAlma As String) As DataSet
        Dim ds As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@ISNO_ARCH", SqlDbType.VarChar, 200)
        arParms(1).Value = strNoArchSali
        arParms(2) = New SqlParameter("@ISNU_REFE", SqlDbType.VarChar, 15)
        arParms(2).Value = strNuRefe
        arParms(3) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(3).Value = strCoClie
        arParms(4) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 20)
        arParms(4).Value = strCoAlma
        arParms(5) = New SqlParameter("@MENSAJE", SqlDbType.VarChar, 200)
        arParms(5).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_INGRESO_WMS_INFO", arParms)
        lstrMensajeError = arParms(5).Value()
        Return ds
    End Function

    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
End Class
