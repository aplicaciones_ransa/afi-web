Imports System.Data.SqlClient
Public Class clsCADEntidad
    Private strCadenaOficome As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
    Private dt As DataTable

    Public Function gCADGetDataEmpresa(ByVal sCO_ENTI_TRAB As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@sCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = sCO_ENTI_TRAB
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_tmclie_Sel_InformacionCliente", arParms).Tables(0)
    End Function

    Public Function gCADGetDataDirecciones(ByVal sCO_ENTI_TRAB As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@sCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = sCO_ENTI_TRAB
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_trdire_clie_Sel_ListarDirecciones", arParms).Tables(0)
    End Function

    Public Function gCADGetDataContactos(ByVal sCO_ENTI_TRAB As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@sCO_CLIE", SqlDbType.VarChar, 20)
        arParms(0).Value = sCO_ENTI_TRAB
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_trcont_clie_Sel_ListarContactos", arParms).Tables(0)
    End Function

    Public Function gCADGetTipoDireccion() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_TTTABL_AYUD_Sel_TipoDireccion").Tables(0)
    End Function

    Public Function gCADGetPais() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_TTPAIS_Sel_ListarPaises").Tables(0)
    End Function

    Public Function gCADGetTipoContacto() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_TTTIPO_CONT_Sel_ListarTipoContacto").Tables(0)
    End Function

    Public Function gCADGetTipoDoc() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_TTDOCU_IDEN_Sel_ListarDocIdentidad").Tables(0)
    End Function
End Class
