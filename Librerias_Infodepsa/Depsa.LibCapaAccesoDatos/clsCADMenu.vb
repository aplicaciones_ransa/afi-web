Imports System.Data.SqlClient
Public Class clsCADMenu
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private pstrco_sist As String
    Private pstrco_modu As String
    Private pstrco_menu As String
    Private pstrco_grup As String
    Private pstrst_adic As String
    Private pstrst_modi As String
    Private pstrst_anul As String
    Private pstrst_repo As String
    Private pstrco_usua_crea As String
    Private dtTemporal As DataTable
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvesistema() As String
        Return pstrco_sist
    End Function

    Public Function fn_devuelvemodulo() As String
        Return pstrco_modu
    End Function

    Public Function fn_devuelvemenu() As String
        Return pstrco_menu
    End Function

    Public Function fn_devuelvegrupo() As String
        Return pstrco_grup
    End Function

    Public Function fn_devuelveadicionar() As String
        Return pstrst_adic
    End Function

    Public Function fn_devuelvemodificar() As String
        Return pstrst_modi
    End Function

    Public Function fn_devuelveanular() As String
        Return pstrst_anul
    End Function

    Public Function fn_devuelvereporte() As String
        Return pstrst_repo
    End Function

    Public Function fn_devuelveerrorsql() As String
        Return pstrErrorSql
    End Function

    Public Function fn_devuelveFilasAfect() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveresultado() As String
        Return pstrResultado
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Sub gCADInsertarMenuxGrupo(ByVal strco_sist As String, ByVal strco_modu As String, ByVal strco_menu As String, _
                                        ByVal strco_grup As String, ByVal strco_usua_crea As String, ByVal strti_sele As String)
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@ISCO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strco_sist
        arParms(1) = New SqlParameter("@ISCO_MODU", SqlDbType.VarChar, 100)
        arParms(1).Value = strco_modu
        arParms(2) = New SqlParameter("@ISCO_MENU", SqlDbType.VarChar, 100)
        arParms(2).Value = strco_menu
        arParms(3) = New SqlParameter("@ISCO_GRUP", SqlDbType.Char, 8)
        arParms(3).Value = strco_grup
        arParms(4) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 10)
        arParms(4).Direction = ParameterDirection.Output
        arParms(5) = New SqlParameter("@OSERROR_SQL", SqlDbType.VarChar, 10)
        arParms(5).Direction = ParameterDirection.Output
        arParms(6) = New SqlParameter("@ISCO_USUA_CREA", SqlDbType.VarChar, 20)
        arParms(6).Value = strco_usua_crea
        arParms(7) = New SqlParameter("@ISTI_SELE", SqlDbType.Char, 1)
        arParms(7).Value = strti_sele
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_INS_TRMENU_GRUP", arParms)
        If Not IsDBNull(arParms(5).Value) Then
            pstrResultado = arParms(5).Value
        End If
    End Sub

    Public Sub gCADEliminarMenuxGrupo(ByVal strco_sist As String, ByVal strco_modu As String, ByVal strco_menu As String, _
                                        ByVal strco_grup As String, ByVal strti_sele As String)
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@ISCO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strco_sist
        arParms(1) = New SqlParameter("@ISCO_MODU", SqlDbType.VarChar, 100)
        arParms(1).Value = strco_modu
        arParms(2) = New SqlParameter("@ISCO_MENU", SqlDbType.VarChar, 100)
        arParms(2).Value = strco_menu
        arParms(3) = New SqlParameter("@ISCO_GRUP", SqlDbType.Char, 8)
        arParms(3).Value = strco_grup
        arParms(4) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 10)
        arParms(4).Direction = ParameterDirection.Output
        arParms(5) = New SqlParameter("@OSERROR_SQL", SqlDbType.VarChar, 10)
        arParms(5).Direction = ParameterDirection.Output
        arParms(6) = New SqlParameter("@ISTI_SELE", SqlDbType.Char, 1)
        arParms(6).Value = strti_sele
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_DEL_TRMENU_GRUP", arParms)
        If Not IsDBNull(arParms(5).Value) Then
            pstrResultado = arParms(5).Value
        End If
    End Sub

    Public Sub gCADTraerMenusxSistema(ByVal strco_sist As String, ByVal strco_grup As String, ByVal strti_sele As String)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@ISCO_SIST", SqlDbType.VarChar, 8)
        arParms(0).Value = strco_sist.Trim
        arParms(1) = New SqlParameter("@ISCO_GRUP", SqlDbType.VarChar, 8)
        arParms(1).Value = strco_grup.Trim
        arParms(2) = New SqlParameter("@ISTI_SELE", SqlDbType.Char, 1)
        arParms(2).Value = strti_sele.Trim
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_TMMENU_TraeMenusxSistema", arParms).Tables(0)
    End Sub
End Class
