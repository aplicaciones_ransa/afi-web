Imports System.Data.SqlClient
Public Class clsCADWarrant
    Private dsResult As DataSet
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")

    Public Function gCADGetListarFinanciadores(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                ByVal strTipoEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strTipoEntidad
        dsResult = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TCTITU_Sel_Financiadores", arParms)
        If dsResult.Tables.Count = 1 Then
            Return dsResult.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function gCADGetListarTipoWarrant(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                ByVal strTipoEntidad As String, ByVal strFlgFinan As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sTipoEntidad", SqlDbType.Char, 2)
        arParms(2).Value = strTipoEntidad
        arParms(3) = New SqlParameter("@sFlgFinan", SqlDbType.Char, 1)
        arParms(3).Value = strFlgFinan
        dsResult = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TCALMA_MERC_Sel_Modalidad", arParms)
        If dsResult.Tables.Count = 1 Then
            Return dsResult.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function gCADGetBuscarWarrant(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                        ByVal strEstado As String, ByVal strCodFina As String, _
                                        ByVal strTipWar As String, ByVal strNroWar As String, _
                                        ByVal strTipoEntidad As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        arParms(0) = New SqlParameter("@sCodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@sCodCliente", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@sEstado", SqlDbType.Char, 1)
        arParms(2).Value = strEstado
        arParms(3) = New SqlParameter("@sCodFina", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodFina
        arParms(4) = New SqlParameter("@sTipWar", SqlDbType.Char, 3)
        arParms(4).Value = strTipWar
        arParms(5) = New SqlParameter("@sNroWar", SqlDbType.VarChar, 8)
        arParms(5).Value = strNroWar
        arParms(6) = New SqlParameter("@sTipoEntidad", SqlDbType.Char, 2)
        arParms(6).Value = strTipoEntidad
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_TCTITU_Sel_BuscarWarrant", arParms).Tables(0)
    End Function

    Public Function gCADGetListarRetiros(ByVal strCodEmpresa As String, ByVal strTipWar As String, _
                                        ByVal strNroWar As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@ISTI_TITU", SqlDbType.VarChar, 3)
        arParms(1).Value = strTipWar
        arParms(2) = New SqlParameter("@ISNU_TITU", SqlDbType.VarChar, 20)
        arParms(2).Value = strNroWar
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "SP_TMSALD_ALMA_Q06_SAD", arParms).Tables(0)
    End Function
End Class
