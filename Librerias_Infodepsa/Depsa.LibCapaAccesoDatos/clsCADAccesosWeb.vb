Imports System.Configuration
Imports System.Data.SqlClient
Public Class clsCADAccesosWeb
    Private dtTemporal As DataTable
    Private strCadenaWE As String = ConfigurationManager.AppSettings("ConnectionStringWE")

    Public Function fn_devuelveDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Sub gCADMostrarAccesosWeb(ByVal strfchini As String, ByVal strfchfin As String, ByVal strco_usua As String)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@ISFECHA_INIC", SqlDbType.VarChar, 10)
        arParms(0).Value = strfchini
        arParms(1) = New SqlParameter("@ISFECHA_FINA", SqlDbType.VarChar, 10)
        arParms(1).Value = strfchfin
        arParms(2) = New SqlParameter("@ISNO_USUA", SqlDbType.VarChar, 100)
        arParms(2).Value = strco_usua
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_WFREGI_ACCE", arParms).Tables(0)
    End Sub

    Public Sub gCADMostrarLogueos(ByVal strfchini As String, ByVal strfchfin As String, ByVal strco_usua As String)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@ISFECHA_INIC", SqlDbType.VarChar, 10)
        arParms(0).Value = strfchini
        arParms(1) = New SqlParameter("@ISFECHA_FINA", SqlDbType.VarChar, 10)
        arParms(1).Value = strfchfin
        arParms(2) = New SqlParameter("@ISNO_USUA", SqlDbType.VarChar, 100)
        arParms(2).Value = strco_usua
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_WFLOG_ACCESO_SEL_LISTARLOGUEOS", arParms).Tables(0)
    End Sub

    Public Function gCADInsAccesosWeb(ByVal sCO_USUA As String,
                                    ByVal sTI_ENTI_TRAB As String,
                                    ByVal sCO_ENTI_TRAB As String,
                                    ByVal sNO_ENTI_TRAB As String,
                                    ByVal sTI_MENU As String,
                                    ByVal sCO_MENU As String,
                                    ByVal sNO_MENU As String,
                                    ByVal sIP_PETI As String,
                                    ByVal sIP_REAL As String) As Integer
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@ISCO_USUA", SqlDbType.VarChar, 8)
        arParms(0).Value = sCO_USUA
        arParms(1) = New SqlParameter("@ISTI_ENTI_TRAB", SqlDbType.Char, 1)
        arParms(1).Value = sTI_ENTI_TRAB
        arParms(2) = New SqlParameter("@ISCO_ENTI_TRAB", SqlDbType.VarChar, 20)
        arParms(2).Value = sCO_ENTI_TRAB
        arParms(3) = New SqlParameter("@ISNO_ENTI_TRAB", SqlDbType.VarChar, 200)
        arParms(3).Value = sNO_ENTI_TRAB
        arParms(4) = New SqlParameter("@ISTI_MENU", SqlDbType.Char, 1)
        arParms(4).Value = sTI_MENU
        arParms(5) = New SqlParameter("@ISCO_MENU", SqlDbType.VarChar, 20)
        arParms(5).Value = sCO_MENU
        arParms(6) = New SqlParameter("@ISNO_MENU", SqlDbType.VarChar, 50)
        arParms(6).Value = sNO_MENU
        arParms(7) = New SqlParameter("@ISIP_PETI", SqlDbType.VarChar, 50)
        arParms(7).Value = sIP_PETI
        arParms(8) = New SqlParameter("@ISIP_REAL", SqlDbType.VarChar, 50)
        arParms(8).Value = sIP_REAL
        Dim nNU_REGI_AFEC As Integer = SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_INS_WFREGI_ACCE", arParms)
        Return nNU_REGI_AFEC
    End Function

    Public Function gCADMostrarDatosUsuario(ByVal strCod_user As String, strCod_sico As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CodUser", SqlDbType.VarChar, 4)
        arParms(0).Value = strCod_user
        arParms(1) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(1).Value = strCod_sico
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_WFUSRXENTDAD_Mobil_Sel_ValidaEntidadUsuario", arParms).Tables(0)
    End Function

End Class
