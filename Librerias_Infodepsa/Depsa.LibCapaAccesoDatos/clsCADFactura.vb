Imports System.Data.SqlClient
Public Class clsCADFactura
    Private strCadenaOficome As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")

    Public Function GetFacturas(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String, _
                                ByVal sCO_UNID As String, ByVal sNU_DOCU As String, _
                                ByVal sFE_EMIS_INIC As String, ByVal sFE_EMIS_FINA As String, _
                                ByVal strNroDCR As String) As DataSet
        Dim dsTemporal As DataSet
        Try
            Dim arParms() As SqlParameter = New SqlParameter(8) {}
            arParms(0) = New SqlParameter("@sCO_EMPR", SqlDbType.Char, 2)
            arParms(0).Value = sCO_EMPR
            arParms(1) = New SqlParameter("@sCO_CLIE", SqlDbType.VarChar, 20)
            arParms(1).Value = sCO_CLIE
            arParms(2) = New SqlParameter("@sCO_UNID_GENE", SqlDbType.VarChar, 3)
            arParms(2).Value = sCO_UNID
            arParms(3) = New SqlParameter("@sNU_DOCU", SqlDbType.VarChar, 20)
            arParms(3).Value = sNU_DOCU
            arParms(4) = New SqlParameter("@sFE_EMIS_INIC", SqlDbType.VarChar, 10)
            arParms(4).Value = sFE_EMIS_INIC
            arParms(5) = New SqlParameter("@sFE_EMIS_FINA", SqlDbType.VarChar, 10)
            arParms(5).Value = sFE_EMIS_FINA
            arParms(6) = New SqlParameter("@sNU_DOCU_RECE", SqlDbType.VarChar, 20)
            arParms(6).Value = strNroDCR
            arParms(7) = New SqlParameter("@II_Error_Sql", SqlDbType.Int)
            arParms(7).Direction = ParameterDirection.Output
            arParms(8) = New SqlParameter("@II_Filas_Afectadas", SqlDbType.Int)
            arParms(8).Direction = ParameterDirection.Output

            dsTemporal = SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "Usp_TMSALD_ALMA_Sel_Facturas2XX_LU", arParms)
            Return dsTemporal
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
            Return dsTemporal
        End Try
    End Function

    Public Function GetDetalleDCR(ByVal sCO_EMPR As String, ByVal sNU_DOCU_RECE As String) As DataSet
        Dim dsTemporal As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISNU_DOCU_RECE", SqlDbType.VarChar, 20)
        arParms(1).Value = sNU_DOCU_RECE
        dsTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Q04", arParms)
        Return dsTemporal
    End Function

    Public Function fn_listarUnidades(ByVal sCO_EMPR As String) As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = sCO_EMPR
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "USP_TTTIPO_INF2", arParms)
    End Function

    Public Function fn_GetFactura(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                            ByVal sCoGrup As String, ByVal sTI_DOC As String, _
                                            ByVal sNU_FACT As String) As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.Char, 3)
        arParms(1).Value = sCO_UNID
        arParms(2) = New SqlParameter("@ISCO_GRUP", SqlDbType.VarChar, 8)
        arParms(2).Value = sCoGrup
        arParms(3) = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        arParms(3).Value = sTI_DOC
        arParms(4) = New SqlParameter("@ISNU_DOCU_REP", SqlDbType.VarChar, 20)
        arParms(4).Value = sNU_FACT
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "USP_TCDOCU_CLIE_ImprimirFactura", arParms)
    End Function

    Public Function fn_GetOrdenTrabajo(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                        ByVal sTI_DOC As String, ByVal sNU_DOC As String, _
                                        ByVal sCO_CLIE As String) As DataSet
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = sCO_EMPR
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.Char, 3)
        arParms(1).Value = sCO_UNID
        arParms(2) = New SqlParameter("@ISTI_ORDE_TRAB", SqlDbType.VarChar, 3)
        arParms(2).Value = sTI_DOC
        arParms(3) = New SqlParameter("@ISNU_ORDE_TRAB", SqlDbType.VarChar, 20)
        arParms(3).Value = sNU_DOC
        arParms(4) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(4).Value = sCO_CLIE
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TCDOCU_CLIE_ImprimirOrden", arParms)
    End Function

    Public Function gCADGetDocumentoDetalleXML(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                    ByVal strti_docu As String, ByVal strnu_docu As String, ByVal strco_clie As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.Char, 3)
        arParms(1).Value = strco_unid
        arParms(2) = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        arParms(2).Value = strti_docu
        arParms(3) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        arParms(3).Value = strnu_docu
        arParms(4) = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        arParms(4).Value = strco_clie
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "USP_TDDOCU_CLIE_XML_SEL_XML", arParms).Tables(0)
    End Function

    Public Function fn_GetBoleta(ByVal sTipoCon As String, ByVal strTipoDoc As String, ByVal strCorrelativo As String, ByVal strSerie As String, ByVal strFechaInicial As String, ByVal strMonto As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@TIPO_CON", SqlDbType.Char, 2)
        arParms(0).Value = sTipoCon
        arParms(1) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(1).Value = "01"
        arParms(2) = New SqlParameter("@ISCO_UNID", SqlDbType.Char, 3)
        arParms(2).Value = "001"
        arParms(3) = New SqlParameter("@ISTI_DOCU", SqlDbType.Char, 3)
        arParms(3).Value = strTipoDoc
        arParms(4) = New SqlParameter("@ISNU_CORR", SqlDbType.Char, 4)
        arParms(4).Value = strCorrelativo
        arParms(5) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        arParms(5).Value = strSerie
        arParms(6) = New SqlParameter("@FE_EMIS", SqlDbType.VarChar, 10)
        arParms(6).Value = strFechaInicial
        arParms(7) = New SqlParameter("@IM_TOTA", SqlDbType.VarChar, 25)
        arParms(7).Value = strMonto
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "USP_TCDOCU_CLIE_ImprimirBoleta", arParms).Tables(0)
    End Function
End Class
