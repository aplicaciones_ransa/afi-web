Imports System.Data.SqlClient


Public Class clsCADCCorrientes
    Private cnCO_BDAT As New SqlConnection
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strCadenaOficome As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
    Private pstrti_docu As String
    Private pstrno_docu As String
    Private dttemporal As DataTable
    Private dstemporal As DataSet
    Private pstrerrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvetidocu() As String
        Return pstrti_docu
    End Function

    Public Function fn_devuelvenodocu() As String
        Return pstrno_docu
    End Function

    Public Function fn_devuelvedatatable() As DataTable
        Return dttemporal
    End Function

    Public Function fn_devuelveErrorSql() As String
        Return pstrerrorSql
    End Function

    Public Function fn_devuelveFilas() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveResultado() As String
        Return pstrResultado
    End Function

    Public Function fn_devuelveDataset() As DataSet
        Return dstemporal
    End Function

    Public Sub gCADGetTipoDocumentos()
        dttemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_TipoDocumento").Tables(0)
    End Sub

    Public Sub gCADGetMonedas()
        dttemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_TTMONE_TraeMonedas").Tables(0)
    End Sub

    Public Sub gCADGetCancelaciones(ByVal strco_empr As String, ByVal strco_clie As String, ByVal strti_docu As String, _
                                    ByVal strfe_inic As String, ByVal strfe_fina As String)
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(1).Value = strco_clie
        arParms(2) = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 20)
        arParms(2).Value = strti_docu
        arParms(3) = New SqlParameter("@ISFE_INIC", SqlDbType.VarChar, 10)
        arParms(3).Value = strfe_inic
        arParms(4) = New SqlParameter("@ISFE_FINA", SqlDbType.VarChar, 10)
        arParms(4).Value = strfe_fina
        dttemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_Cancelaciones", arParms).Tables(0)
    End Sub

    Public Sub gCADGetEstadodeCuenta(ByVal strco_empr As String, ByVal strfe_proc As String, ByVal strco_clie As String)
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaWE
            cnCO_BDAT.Open()
        End If

        Dim cmTCDOCU_CLIE As New SqlCommand
        cmTCDOCU_CLIE.Connection = cnCO_BDAT
        cmTCDOCU_CLIE.CommandType = CommandType.StoredProcedure
        cmTCDOCU_CLIE.CommandText = "SP_TCDOCU_CLIE_Q04"

        Dim pmTCDOCU_CLIE As New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_empr
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "001"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_MONE_NACI", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "SOL"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_MONE_DOLA", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "DOL"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@IDFE_PROC", SqlDbType.DateTime)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = CType(strfe_proc, DateTime)
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@INFA_CAMB_OPER", SqlDbType.Decimal)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = 1
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_MONE_CONV", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "DOL"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_CLIE_INIC", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_clie
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_CLIE_FINA", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_clie
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_COBR_INIC", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = ""
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_COBR_FINA", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = ""
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISTI_PRES", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = ""
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISST_ESTA_DOCU", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "S"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISST_ESTA_HIST", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "N"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISST_SELE_UNID", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "T"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISST_SELE_CLIE", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = "P"
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISST_SELE_COBR", SqlDbType.VarChar, 1)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = ""
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)
        cmTCDOCU_CLIE.CommandTimeout = 0

        Dim daTCDOCU_CLIE As New SqlDataAdapter
        daTCDOCU_CLIE.SelectCommand = cmTCDOCU_CLIE

        Dim dsTCDOCU_CLIE As New DataSet
        daTCDOCU_CLIE.Fill(dsTCDOCU_CLIE, "TCDOCU_CLIE")
        cnCO_BDAT.Close()
        dstemporal = dsTCDOCU_CLIE
    End Sub

    Public Function gCADGetDocumentoDetalle(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                        ByVal strti_docu As String, ByVal strnu_docu As String) As DataTable
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaWE
            cnCO_BDAT.Open()
        End If

        Dim cmTCDOCU_CLIE As New SqlCommand
        cmTCDOCU_CLIE.Connection = cnCO_BDAT
        cmTCDOCU_CLIE.CommandType = CommandType.StoredProcedure
        cmTCDOCU_CLIE.CommandText = "USP_SEL_DetalleCancelaciones"

        Dim pmTCDOCU_CLIE As New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_empr
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_unid
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strti_docu
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strnu_docu
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        Dim daTCDOCU_CLIE As New SqlDataAdapter
        daTCDOCU_CLIE.SelectCommand = cmTCDOCU_CLIE

        Dim dsTCDOCU_CLIE As New DataSet
        daTCDOCU_CLIE.Fill(dsTCDOCU_CLIE, "TCDOCU_CLIE")
        cnCO_BDAT.Close()
        Return dsTCDOCU_CLIE.Tables(0)
    End Function

    Public Function gCADGetDocumentoDetalleDOT(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                ByVal strnu_docu As String, ByVal strco_clie As String, _
                                                ByVal strco_alma As String) As DataTable
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaOfioper
            cnCO_BDAT.Open()
        End If

        Dim cmTTFORMA_DOT As New SqlCommand
        cmTTFORMA_DOT.Connection = cnCO_BDAT
        cmTTFORMA_DOT.CommandType = CommandType.StoredProcedure
        cmTTFORMA_DOT.CommandText = "USP_TTFORMA_DOT_SEL_PDF_DOT"

        Dim pmTTFORMA_DOT As New SqlParameter("@CO_EMPR", SqlDbType.VarChar, 2)
        pmTTFORMA_DOT.Direction = ParameterDirection.Input
        pmTTFORMA_DOT.Value = strco_empr
        cmTTFORMA_DOT.Parameters.Add(pmTTFORMA_DOT)

        pmTTFORMA_DOT = New SqlParameter("@CO_UNID", SqlDbType.VarChar, 3)
        pmTTFORMA_DOT.Direction = ParameterDirection.Input
        pmTTFORMA_DOT.Value = strco_unid
        cmTTFORMA_DOT.Parameters.Add(pmTTFORMA_DOT)

        pmTTFORMA_DOT = New SqlParameter("@NU_ORDE_TRAB", SqlDbType.VarChar, 20)
        pmTTFORMA_DOT.Direction = ParameterDirection.Input
        pmTTFORMA_DOT.Value = strnu_docu
        cmTTFORMA_DOT.Parameters.Add(pmTTFORMA_DOT)

        pmTTFORMA_DOT = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        pmTTFORMA_DOT.Direction = ParameterDirection.Input
        pmTTFORMA_DOT.Value = strco_clie
        cmTTFORMA_DOT.Parameters.Add(pmTTFORMA_DOT)

        pmTTFORMA_DOT = New SqlParameter("@CO_ALMA", SqlDbType.VarChar, 5)
        pmTTFORMA_DOT.Direction = ParameterDirection.Input
        pmTTFORMA_DOT.Value = strco_alma
        cmTTFORMA_DOT.Parameters.Add(pmTTFORMA_DOT)

        Dim daTTFORMA_DOT As New SqlDataAdapter
        daTTFORMA_DOT.SelectCommand = cmTTFORMA_DOT

        Dim dsTTFORMA_DOT As New DataSet
        daTTFORMA_DOT.Fill(dsTTFORMA_DOT, "TTFORMA_DOT")
        cnCO_BDAT.Close()
        Return dsTTFORMA_DOT.Tables(0)
    End Function

    Public Function gCADGetSustentoDCR(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                        ByVal strti_docu As String, ByVal strnu_docu As String) As DataTable
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaOficome
            cnCO_BDAT.Open()
        End If

        Dim cmTWFCOERCI As New SqlCommand
        cmTWFCOERCI.Connection = cnCO_BDAT
        cmTWFCOERCI.CommandType = CommandType.StoredProcedure
        cmTWFCOERCI.CommandText = "USP_TWFCOERCI_SEL_SUST_SEG"

        Dim pmTWFCOERCI As New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        pmTWFCOERCI.Direction = ParameterDirection.Input
        pmTWFCOERCI.Value = strco_empr
        cmTWFCOERCI.Parameters.Add(pmTWFCOERCI)

        pmTWFCOERCI = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        pmTWFCOERCI.Direction = ParameterDirection.Input
        pmTWFCOERCI.Value = strco_unid
        cmTWFCOERCI.Parameters.Add(pmTWFCOERCI)

        pmTWFCOERCI = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        pmTWFCOERCI.Direction = ParameterDirection.Input
        pmTWFCOERCI.Value = strti_docu
        cmTWFCOERCI.Parameters.Add(pmTWFCOERCI)

        pmTWFCOERCI = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        pmTWFCOERCI.Direction = ParameterDirection.Input
        pmTWFCOERCI.Value = strnu_docu
        cmTWFCOERCI.Parameters.Add(pmTWFCOERCI)

        Dim daTWFCOERCI As New SqlDataAdapter
        daTWFCOERCI.SelectCommand = cmTWFCOERCI

        Dim dsTWFCOERCI As New DataSet
        daTWFCOERCI.Fill(dsTWFCOERCI, "TWFCOERCI")
        cnCO_BDAT.Close()
        Return dsTWFCOERCI.Tables(0)
    End Function


    Public Function gCADGetMostrarSustento(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                        ByVal strti_docu As String, ByVal strnu_docu As String) As DataTable

        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaOficome
            cnCO_BDAT.Open()
        End If

        Dim cmTCDOCU_CLIE As New SqlCommand
        cmTCDOCU_CLIE.Connection = cnCO_BDAT
        cmTCDOCU_CLIE.CommandType = CommandType.StoredProcedure
        cmTCDOCU_CLIE.CommandText = "USP_TCDOCU_CLIE_SEL_FACT_SUST_SEG"

        Dim pmTCDOCU_CLIE As New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_empr
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strco_unid
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strti_docu
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        pmTCDOCU_CLIE = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        pmTCDOCU_CLIE.Direction = ParameterDirection.Input
        pmTCDOCU_CLIE.Value = strnu_docu
        cmTCDOCU_CLIE.Parameters.Add(pmTCDOCU_CLIE)

        Dim daTCDOCU_CLIE As New SqlDataAdapter
        daTCDOCU_CLIE.SelectCommand = cmTCDOCU_CLIE

        Dim dsTCDOCU_CLIE As New DataSet
        daTCDOCU_CLIE.Fill(dsTCDOCU_CLIE, "TCDOCU_CLIE")
        cnCO_BDAT.Close()
        Return dsTCDOCU_CLIE.Tables(0)
    End Function

    Public Function gCADGetAbono(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                   ByVal strti_docu As String, ByVal strnu_docu As String) As DataTable
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaOfioper
            cnCO_BDAT.Open()
        End If

        Dim cmTCCOBR As New SqlCommand
        cmTCCOBR.Connection = cnCO_BDAT
        cmTCCOBR.CommandType = CommandType.StoredProcedure
        cmTCCOBR.CommandText = "USP_TCCOBR_Sel_Abonos"

        Dim pmTCCOBR As New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        pmTCCOBR.Direction = ParameterDirection.Input
        pmTCCOBR.Value = strco_empr
        cmTCCOBR.Parameters.Add(pmTCCOBR)

        pmTCCOBR = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        pmTCCOBR.Direction = ParameterDirection.Input
        pmTCCOBR.Value = strco_unid
        cmTCCOBR.Parameters.Add(pmTCCOBR)

        pmTCCOBR = New SqlParameter("@ISTI_DOCU", SqlDbType.VarChar, 3)
        pmTCCOBR.Direction = ParameterDirection.Input
        pmTCCOBR.Value = strti_docu
        cmTCCOBR.Parameters.Add(pmTCCOBR)

        pmTCCOBR = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        pmTCCOBR.Direction = ParameterDirection.Input
        pmTCCOBR.Value = strnu_docu
        cmTCCOBR.Parameters.Add(pmTCCOBR)

        Dim daTCCOBR As New SqlDataAdapter
        daTCCOBR.SelectCommand = cmTCCOBR

        Dim dsTCCOBR As New DataSet
        daTCCOBR.Fill(dsTCCOBR, "TCCOBR")
        cnCO_BDAT.Close()
        Return dsTCCOBR.Tables(0)
    End Function

    Public Function gCADGetDocumentoDetalleWAR(ByVal strco_empr As String, ByVal strco_unid As String, _
                                            ByVal strnu_docu As String, ByVal strti_docu As String) As DataTable
        If cnCO_BDAT.State = ConnectionState.Closed Then
            cnCO_BDAT.ConnectionString = strCadenaOfioper
            cnCO_BDAT.Open()
        End If

        Dim cmWFDOCUMENTO As New SqlCommand
        cmWFDOCUMENTO.Connection = cnCO_BDAT
        cmWFDOCUMENTO.CommandType = CommandType.StoredProcedure
        cmWFDOCUMENTO.CommandText = "USP_WFDOCUMENTO_SEL_InfoPDF_CER_WAR_WIP"

        Dim pmWFDOCUMENTO As New SqlParameter("@CO_EMPR", SqlDbType.VarChar, 2)
        pmWFDOCUMENTO.Direction = ParameterDirection.Input
        pmWFDOCUMENTO.Value = strco_empr
        cmWFDOCUMENTO.Parameters.Add(pmWFDOCUMENTO)

        pmWFDOCUMENTO = New SqlParameter("@CO_UNID", SqlDbType.VarChar, 3)
        pmWFDOCUMENTO.Direction = ParameterDirection.Input
        pmWFDOCUMENTO.Value = strco_unid
        cmWFDOCUMENTO.Parameters.Add(pmWFDOCUMENTO)

        pmWFDOCUMENTO = New SqlParameter("@TI_DOCU", SqlDbType.VarChar, 3)
        pmWFDOCUMENTO.Direction = ParameterDirection.Input
        pmWFDOCUMENTO.Value = strti_docu
        cmWFDOCUMENTO.Parameters.Add(pmWFDOCUMENTO)

        pmWFDOCUMENTO = New SqlParameter("@NU_DOCU", SqlDbType.VarChar, 20)
        pmWFDOCUMENTO.Direction = ParameterDirection.Input
        pmWFDOCUMENTO.Value = strnu_docu
        cmWFDOCUMENTO.Parameters.Add(pmWFDOCUMENTO)

        Dim daWFDOCUMENTO As New SqlDataAdapter
        daWFDOCUMENTO.SelectCommand = cmWFDOCUMENTO

        Dim dsWFDOCUMENTO As New DataSet
        daWFDOCUMENTO.Fill(dsWFDOCUMENTO, "WFDOCUMENTO")
        cnCO_BDAT.Close()
        Return dsWFDOCUMENTO.Tables(0)
    End Function
End Class
