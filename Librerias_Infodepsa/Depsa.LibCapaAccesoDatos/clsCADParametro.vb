Imports System.Data.SqlClient
Public Class clsCADParametro
    Private strCadenaWE As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private pstrco_domin As String
    Private pstrno_domin As String
    Private pstrde_larg_domin As String
    Private pstrvalor1domin As String
    Private pstrvalor2domin As String
    Private pstrvalor3domin As String
    Private pstrvalor4domin As String
    Private dtTemporal As DataTable
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvecoddomin() As String
        Return pstrco_domin
    End Function

    Public Function fn_devuelvenombdomin() As String
        Return pstrno_domin
    End Function

    Public Function fn_devuelvedescdomin() As String
        Return pstrde_larg_domin
    End Function

    Public Function fn_devuelvevalor1domin() As String
        Return pstrvalor1domin
    End Function

    Public Function fn_devuelvevalor2domin() As String
        Return pstrvalor2domin
    End Function

    Public Function fn_devuelvevalor3domin() As String
        Return pstrvalor3domin
    End Function

    Public Function fn_devuelvevalor4domin() As String
        Return pstrvalor4domin
    End Function

    Public Function fn_devuelveDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Function fn_devuelveErrorSql() As String
        Return pstrErrorSql
    End Function

    Public Function fn_devuelveFilasAfect() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveResultado() As String
        Return pstrResultado
    End Function

    Public Sub gCADMostrarDominios(ByVal strti_sele As String, ByVal intco_domin As Integer)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISTI_SELE", SqlDbType.VarChar, 1)
        arParms(0).Value = strti_sele
        arParms(1) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(1).Value = intco_domin
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_DOMINIO", arParms).Tables(0)
    End Sub

    Public Sub gCADValidarDominio(ByVal strno_domin As String)
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@ISNO_DOMIN", SqlDbType.VarChar, 50)
        arParms(0).Value = strno_domin
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_DOMINIO_VALIDA", arParms).Tables(0)
    End Sub

    Public Sub gCADValidarParametro(ByVal strno_parm As String, ByVal strno_domin As String)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@ISNO_PAR", SqlDbType.VarChar, 50)
        arParms(0).Value = strno_parm
        arParms(1) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(1).Value = strno_domin
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_PARAMETRO_VALIDA", arParms).Tables(0)
    End Sub

    Public Sub gCADMostrarParametros(ByVal intco_par As Integer, ByVal intco_dom As Integer, ByVal strti_sele As String)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@INCO_PAR", SqlDbType.Int)
        arParms(0).Value = intco_par
        arParms(1) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(1).Value = intco_dom
        arParms(2) = New SqlParameter("@ISTI_SELE", SqlDbType.VarChar, 1)
        arParms(2).Value = strti_sele
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_PARAMETRO", arParms).Tables(0)
    End Sub

    Public Sub gCADEliminarParametros(ByVal intco_par As Integer, ByVal intco_dom As Integer)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@INCO_PARAM", SqlDbType.Int)
        arParms(0).Value = intco_par
        arParms(1) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(1).Value = intco_dom
        arParms(2) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 2)
        arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_DEL_PARAMETRO", arParms)
        pstrResultado = arParms(2).Value
    End Sub

    Public Sub gCADInsertarParametros(ByVal intco_dom As Integer, ByVal strno_par As String, ByVal strde_larg As String, _
                                      ByVal strvalor1 As String, ByVal strvalor2 As String, ByVal strvalor3 As String, _
                                      ByVal strvalor4 As String, ByVal strvalor5 As String, ByVal strco_usua_crea As String)
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@ISCO_DOMIN", SqlDbType.Int)
        arParms(0).Value = intco_dom
        arParms(1) = New SqlParameter("@ISNO_PAR", SqlDbType.VarChar, 20)
        arParms(1).Value = strno_par
        arParms(2) = New SqlParameter("@ISDE_LARG_PAR", SqlDbType.VarChar, 100)
        arParms(2).Value = strde_larg
        arParms(3) = New SqlParameter("@ISVALOR1", SqlDbType.VarChar, 150)
        arParms(3).Value = strvalor1
        arParms(4) = New SqlParameter("@ISVALOR2", SqlDbType.VarChar, 150)
        arParms(4).Value = strvalor2
        arParms(5) = New SqlParameter("@ISVALOR3", SqlDbType.VarChar, 150)
        arParms(5).Value = strvalor3
        arParms(6) = New SqlParameter("@ISVALOR4", SqlDbType.VarChar, 150)
        arParms(6).Value = strvalor4
        arParms(7) = New SqlParameter("@ISVALOR5", SqlDbType.VarChar, 150)
        arParms(7).Value = strvalor5
        arParms(8) = New SqlParameter("@ISCO_USUA_CREA", SqlDbType.VarChar, 20)
        arParms(8).Value = strco_usua_crea
        arParms(9) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 20)
        arParms(9).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_INS_PARAMETRO", arParms)
        pstrResultado = arParms(9).Value
    End Sub

    Public Sub gCADActualizarParametro(ByVal intco_dom As Integer, ByVal intco_par As Integer, ByVal strno_par As String, ByVal strde_larg As String, _
                                      ByVal strvalor1 As String, ByVal strvalor2 As String, ByVal strvalor3 As String, _
                                      ByVal strvalor4 As String, ByVal strvalor5 As String, ByVal strco_usua_crea As String)
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        arParms(0) = New SqlParameter("@ISCO_DOMIN", SqlDbType.Int)
        arParms(0).Value = intco_dom
        arParms(1) = New SqlParameter("@ISCO_PAR", SqlDbType.Int)
        arParms(1).Value = intco_par
        arParms(2) = New SqlParameter("@ISNO_PAR", SqlDbType.VarChar, 20)
        arParms(2).Value = strno_par
        arParms(3) = New SqlParameter("@ISDE_LARG_PAR", SqlDbType.VarChar, 100)
        arParms(3).Value = strde_larg
        arParms(4) = New SqlParameter("@ISVALOR1", SqlDbType.VarChar, 150)
        arParms(4).Value = strvalor1
        arParms(5) = New SqlParameter("@ISVALOR2", SqlDbType.VarChar, 150)
        arParms(5).Value = strvalor2
        arParms(6) = New SqlParameter("@ISVALOR3", SqlDbType.VarChar, 150)
        arParms(6).Value = strvalor3
        arParms(7) = New SqlParameter("@ISVALOR4", SqlDbType.VarChar, 150)
        arParms(7).Value = strvalor4
        arParms(8) = New SqlParameter("@ISVALOR5", SqlDbType.VarChar, 150)
        arParms(8).Value = strvalor5
        arParms(9) = New SqlParameter("@ISCO_USUA_MODI", SqlDbType.VarChar, 20)
        arParms(9).Value = strco_usua_crea
        arParms(10) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 20)
        arParms(10).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_UPD_PARAMETRO", arParms)
        pstrResultado = arParms(10).Value
    End Sub

    Public Sub gCADEliminarDominio(ByVal intco_domin As Integer)
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(0).Value = intco_domin
        arParms(1) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 2)
        arParms(1).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_DEL_DOMINIO", arParms)
        pstrResultado = arParms(1).Value
    End Sub

    Public Sub gCADInsertarDominio(ByVal strno_domin As String, ByVal strde_larg As String, _
                                      ByVal strvalor1 As String, ByVal strvalor2 As String, ByVal strvalor3 As String, ByVal strvalor4 As String, _
                                      ByVal strco_usua_crea As String)
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@ISNO_DOMIN", SqlDbType.VarChar, 20)
        arParms(0).Value = strno_domin
        arParms(1) = New SqlParameter("@ISDE_LARG_DOMIN", SqlDbType.VarChar, 100)
        arParms(1).Value = strde_larg
        arParms(2) = New SqlParameter("@ISVALOR1", SqlDbType.VarChar, 150)
        arParms(2).Value = strvalor1
        arParms(3) = New SqlParameter("@ISVALOR2", SqlDbType.VarChar, 150)
        arParms(3).Value = strvalor2
        arParms(4) = New SqlParameter("@ISVALOR3", SqlDbType.VarChar, 150)
        arParms(4).Value = strvalor3
        arParms(5) = New SqlParameter("@ISVALOR4", SqlDbType.VarChar, 150)
        arParms(5).Value = strvalor4
        arParms(6) = New SqlParameter("@ISCO_USUA_CREA", SqlDbType.VarChar, 20)
        arParms(6).Value = strco_usua_crea
        arParms(7) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 20)
        arParms(7).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_INS_DOMINIO", arParms)
        pstrResultado = arParms(7).Value
    End Sub

    Public Sub gCADActualizarDominio(ByVal intco_domin As Integer, ByVal strno_domin As String, ByVal strde_larg As String, _
                                      ByVal strvalor1 As String, ByVal strvalor2 As String, ByVal strvalor3 As String, ByVal strvalor4 As String, _
                                      ByVal strco_usua_modi As String)
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@INCO_DOMIN", SqlDbType.Int)
        arParms(0).Value = intco_domin
        arParms(1) = New SqlParameter("@ISNO_DOMIN", SqlDbType.VarChar, 20)
        arParms(1).Value = strno_domin
        arParms(2) = New SqlParameter("@ISDE_LARG_DOMIN", SqlDbType.VarChar, 100)
        arParms(2).Value = strde_larg
        arParms(3) = New SqlParameter("@ISVALOR1", SqlDbType.VarChar, 150)
        arParms(3).Value = strvalor1
        arParms(4) = New SqlParameter("@ISVALOR2", SqlDbType.VarChar, 150)
        arParms(4).Value = strvalor2
        arParms(5) = New SqlParameter("@ISVALOR3", SqlDbType.VarChar, 150)
        arParms(5).Value = strvalor3
        arParms(6) = New SqlParameter("@ISVALOR4", SqlDbType.VarChar, 150)
        arParms(6).Value = strvalor4
        arParms(7) = New SqlParameter("@ISCO_USUA_MODI", SqlDbType.VarChar, 20)
        arParms(7).Value = strco_usua_modi
        arParms(8) = New SqlParameter("@OSMENSAJE", SqlDbType.VarChar, 20)
        arParms(8).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(strCadenaWE, CommandType.StoredProcedure, "USP_UPD_DOMINIO", arParms)
        pstrResultado = arParms(8).Value
    End Sub

    Public Function gCADListarReferencia(ByVal strTipoFiltro As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@TipoFiltro", SqlDbType.Char, 1)
        arParms(0).Value = strTipoFiltro.Trim
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "Usp_PARAMETRO_Sel_TraerReferencia", arParms).Tables(0)
    End Function

    Public Function gCADLlenarSistemas() As DataTable
        Return SqlHelper.ExecuteDataset(strCadenaWE, CommandType.StoredProcedure, "USP_SEL_DOMINIOXPARAMETRO_MuestraSistemas").Tables(0)
    End Function
End Class
