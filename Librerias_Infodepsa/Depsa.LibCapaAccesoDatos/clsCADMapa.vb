Imports System.Data.SqlClient
Public Class clsCADMapa
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private dtTemporal As DataTable

    Public Function GetAlmacenes(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaAlmacenParaVehiculos", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetModalidades(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaModalidades", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetModelos(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaModelos", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetColores(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                        ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaColores", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetBodegas(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaBodegasxAlmacen", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetUbicaciones(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                                ByVal strCodBodega As String, ByVal strModalidad As String, _
                                                ByVal strModelo As String, ByVal strColor As String, _
                                                ByVal strNroChasis As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        arParms(4) = New SqlParameter("@IS_CodBodega", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodBodega
        arParms(5) = New SqlParameter("@IS_Modalidad", SqlDbType.VarChar, 3)
        arParms(5).Value = strModalidad
        arParms(6) = New SqlParameter("@IS_Modelo", SqlDbType.VarChar, 100)
        arParms(6).Value = strModelo
        arParms(7) = New SqlParameter("@IS_Color", SqlDbType.VarChar, 100)
        arParms(7).Value = strColor
        arParms(8) = New SqlParameter("@IS_NroChasis", SqlDbType.VarChar, 6)
        arParms(8).Value = strNroChasis
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ConstruyePlanoxBodega", arParms).Tables(0)
        Return dtTemporal
    End Function

    Public Function GetTotal(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                   ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                   ByVal strCodBodega As String, ByVal strModalidad As String, _
                                   ByVal strModelo As String, ByVal strColor As String, _
                                   ByVal strNroChasis As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        arParms(4) = New SqlParameter("@IS_CodBodega", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodBodega
        arParms(5) = New SqlParameter("@IS_Modalidad", SqlDbType.VarChar, 3)
        arParms(5).Value = strModalidad
        arParms(6) = New SqlParameter("@IS_Modelo", SqlDbType.VarChar, 100)
        arParms(6).Value = strModelo
        arParms(7) = New SqlParameter("@IS_Color", SqlDbType.VarChar, 100)
        arParms(7).Value = strColor
        arParms(8) = New SqlParameter("@IS_NroChasis", SqlDbType.VarChar, 6)
        arParms(8).Value = strNroChasis
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_TotalVehiculosxBodega", arParms).Tables(0)
        Return dtTemporal.Rows(0)("Total").ToString
    End Function

    Public Function gGetVehiculosxUbicacion(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                            ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                                            ByVal strCodBodega As String, ByVal strCodUbicacion As String, _
                                                            ByVal strModalidad As String, ByVal strModelo As String, _
                                                            ByVal strColor As String, ByVal strNroChasis As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_CodCliente", SqlDbType.VarChar, 20)
        arParms(2).Value = strCodCliente
        arParms(3) = New SqlParameter("@IS_CodAlmacen", SqlDbType.VarChar, 5)
        arParms(3).Value = strCodAlmacen
        arParms(4) = New SqlParameter("@IS_CodBodega", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodBodega
        arParms(5) = New SqlParameter("@IS_CodUbicacion", SqlDbType.VarChar, 20)
        arParms(5).Value = strCodUbicacion
        arParms(6) = New SqlParameter("@IS_Modalidad", SqlDbType.VarChar, 3)
        arParms(6).Value = strModalidad
        arParms(7) = New SqlParameter("@IS_Modelo", SqlDbType.VarChar, 100)
        arParms(7).Value = strModelo
        arParms(8) = New SqlParameter("@IS_Color", SqlDbType.VarChar, 100)
        arParms(8).Value = strColor
        arParms(9) = New SqlParameter("@IS_NroChasis", SqlDbType.VarChar, 6)
        arParms(9).Value = strNroChasis
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaVehiculosxUbicacion", arParms).Tables(0)
        Return dtTemporal
    End Function
    Public Function gGetUbicacionesVehiculo(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                            ByVal strTipDocu As String, ByVal strNroDocu As String, _
                                                            ByVal strNroSecu As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@IS_CodEmpresa", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@IS_CodUnidad", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@IS_TipDocu", SqlDbType.VarChar, 3)
        arParms(2).Value = strTipDocu
        arParms(3) = New SqlParameter("@IS_NroDocu", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroDocu
        arParms(4) = New SqlParameter("@IS_NroSecu", SqlDbType.Int)
        arParms(4).Value = CInt(strNroSecu)
        dtTemporal = SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "Usp_ListaUbicacionesVehiculo", arParms).Tables(0)
        Return dtTemporal
    End Function
End Class
