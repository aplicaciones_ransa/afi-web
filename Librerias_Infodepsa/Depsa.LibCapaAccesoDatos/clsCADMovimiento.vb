Imports System.Data.SqlClient
Public Class clsCADMovimiento
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strCadenaOficome As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")

    Function gCADListarMovimiento(ByVal strco_empr As String, ByVal strco_clie As String, _
                                    ByVal strti_movi As String, ByVal strti_refe As String, _
                                    ByVal strnu_refe As String, ByVal strti_repo As String, _
                                    ByVal strti_moda As String, ByVal strfe_inic As String, _
                                    ByVal strfe_fina As String, ByVal strno_cerr As String, _
                                     ByVal strfe_venc As String, ByVal strco_alma As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(11) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(1).Value = strco_clie
        arParms(2) = New SqlParameter("@ISTI_MOVI", SqlDbType.Char, 1)
        arParms(2).Value = strti_movi
        arParms(3) = New SqlParameter("@ISCO_REFE", SqlDbType.Int)
        arParms(3).Value = CInt(strti_refe)
        arParms(4) = New SqlParameter("@ISNU_REFE", SqlDbType.VarChar, 20)
        arParms(4).Value = strnu_refe
        arParms(5) = New SqlParameter("@ISTI_REPO", SqlDbType.Char, 1)
        arParms(5).Value = strti_repo
        arParms(6) = New SqlParameter("@ISCO_MODA_MOVI", SqlDbType.VarChar, 3)
        arParms(6).Value = strti_moda
        arParms(7) = New SqlParameter("@IDFE_REGI_INIC", SqlDbType.VarChar, 10)
        arParms(7).Value = strfe_inic
        arParms(8) = New SqlParameter("@IDFE_REGI_FINA", SqlDbType.VarChar, 10)
        arParms(8).Value = strfe_fina
        arParms(9) = New SqlParameter("@ISST_CIER", SqlDbType.Char, 1)
        arParms(9).Value = strno_cerr
        arParms(10) = New SqlParameter("@IDFE_VENC_LIMI", SqlDbType.VarChar, 10)
        arParms(10).Value = strfe_venc
        arParms(11) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(11).Value = strco_alma
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Sel_Movimientos", arParms).Tables(0)
    End Function

    Function gCADReporteDCR(ByVal strCoEmpr As String, ByVal strCoUnid As String, _
                            ByVal strTiDocuRece As String, ByVal strNuDocuRece As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCoEmpr
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = strCoUnid
        arParms(2) = New SqlParameter("@ISTI_DOCU_RECE", SqlDbType.VarChar, 3)
        arParms(2).Value = strTiDocuRece
        arParms(3) = New SqlParameter("@ISNU_DOCU_RECE", SqlDbType.VarChar, 20)
        arParms(3).Value = strNuDocuRece
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TCALMA_MERC_Q02", arParms).Tables(0)
    End Function

    Function gCADListarMoviMercWMS(ByVal strTipMovi As String, ByVal strTipRepo As String, _
                                    ByVal strCodigo As String, ByVal strDescMerc As String, _
                                    ByVal strLote As String, ByVal strFechaIni As String, _
                                    ByVal strFechaFina As String, ByVal strAlmacen As String, _
                                    ByVal strcodProd As String, ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        arParms(0) = New SqlParameter("@TipMov", SqlDbType.Int)
        arParms(0).Value = strTipMovi
        arParms(1) = New SqlParameter("@TipRepo", SqlDbType.Int)
        arParms(1).Value = strTipRepo
        arParms(2) = New SqlParameter("@Orden", SqlDbType.VarChar, 200)
        arParms(2).Value = strCodigo
        arParms(3) = New SqlParameter("@Descrip", SqlDbType.VarChar, 200)
        arParms(3).Value = strDescMerc
        arParms(4) = New SqlParameter("@Lote", SqlDbType.VarChar, 200)
        arParms(4).Value = strLote
        arParms(5) = New SqlParameter("@FecInicial", SqlDbType.VarChar, 10)
        arParms(5).Value = strFechaIni
        arParms(6) = New SqlParameter("@FecFinal", SqlDbType.VarChar, 10)
        arParms(6).Value = strFechaFina
        arParms(7) = New SqlParameter("@Almacen", SqlDbType.VarChar, 20)
        arParms(7).Value = strAlmacen
        arParms(8) = New SqlParameter("@codProd", SqlDbType.VarChar, 20)
        arParms(8).Value = strcodProd
        arParms(9) = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        arParms(9).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_HISTORY_MASTER_SEL_MOVIMIENTOS", arParms).Tables(0)
    End Function

    Public Function gCADGGetModalidad(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                       ByVal strnu_docu As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strco_empr
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.Char, 3)
        arParms(1).Value = strco_unid
        arParms(2) = New SqlParameter("@ISTI_DOCU", SqlDbType.Char, 3)
        arParms(2).Value = strti_docu
        arParms(3) = New SqlParameter("@ISNU_DOCU", SqlDbType.VarChar, 20)
        arParms(3).Value = strnu_docu
        Return SqlHelper.ExecuteDataset(strCadenaOficome, CommandType.StoredProcedure, "USP_TCALMA_MERC_Sel_Modalidad", arParms).Tables(0)
    End Function
End Class
