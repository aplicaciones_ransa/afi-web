﻿Imports System.Data.SqlClient
Public Class clsCADInventario
    Private ds As DataSet
    Private dt As DataTable
    Private mstrErrorSql As String
    Private mintFilasAfectadas As Integer
    Private mstrResultado As String
    Private mstrCodDocu As String
    Private cnnOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strCnn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
    ' Private strCadena As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionString")
    Private lstrResultado As String
    Private LstrNombrePDF As String
    Public Function gCADGetListarClienteInventario(ByVal co_empre As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CO_EMPR", SqlDbType.VarChar, 20)
        arParms(0).Value = co_empre
        ds = SqlHelper.ExecuteDataset(strCnn, CommandType.StoredProcedure, "SP_Inventario_ListarCliente", arParms)
        Return ds.Tables(0)
    End Function
    Public Function gCADGetListarAlmacen(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
        arParms(1).Direction = ParameterDirection.Output
        arParms(2) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
        arParms(2).Direction = ParameterDirection.Output
        ds = SqlHelper.ExecuteDataset(cnnOfioper, CommandType.StoredProcedure, "Usp_TMALMA_INVE_Sel_ListarAlmacenxCliente", arParms)
        mstrErrorSql = arParms(1).Value
        mintFilasAfectadas = arParms(2).Value
        Return ds.Tables(0)
    End Function

    Public Function gCADListarInventarioXDocumento(ByVal CodDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@NU_DOCU", SqlDbType.Int)
        arParms(0).Value = CodDocumento
        Return SqlHelper.ExecuteDataset(cnnOfioper, CommandType.StoredProcedure, "USP_TCMOVI_INVE_ListarXDocumento", arParms).Tables(0)
    End Function

    Public Function gCADListarInventarioDetalleCardex(ByVal CodDocumento As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@NU_DOCU", SqlDbType.Int)
        arParms(0).Value = CodDocumento
        Return SqlHelper.ExecuteDataset(cnnOfioper, CommandType.StoredProcedure, "USP_TDMOVI_INVE_ListarCardex", arParms).Tables(0)
    End Function

    Public Function gCADListarUnidadMedida() As DataTable
        Dim query As String
        query = "SELECT CO_UNME, CO_UNME + ' - ' + DE_UNME AS DE_UNME FROM TTUNID_MEDI"
        Return SqlHelper.ExecuteDataset(cnnOfioper, CommandType.Text, query).Tables(0)
    End Function

    Public Function gCADGetListarConductor(ByVal strCodAlmacen As String) As String
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@CO_ALMA", SqlDbType.VarChar, 5)
        arParms(0).Value = strCodAlmacen
        arParms(1) = New SqlParameter("@DE_COND", SqlDbType.VarChar, 100)
        arParms(1).Direction = ParameterDirection.Output
        'arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
        'arParms(1).Direction = ParameterDirection.Output
        'arParms(2) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
        'arParms(2).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(cnnOfioper, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarConductorxAlmacen", arParms)
        'mstrErrorSql = arParms(1).Value
        'mintFilasAfectadas = arParms(2).Value
        Return arParms(1).Value
    End Function

    Public Function gCADGetbuscarDocInventario(ByVal FechaDesde As String, ByVal FechaHasta As String,
                                            ByVal vapor As String, ByVal Cliente As String,
                                            ByVal Almacen As String, ByVal Estado As String) As DataTable

        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        arParms(1) = New SqlParameter("@CO_ALMA", SqlDbType.VarChar, 5)
        arParms(2) = New SqlParameter("@DE_VAPO", SqlDbType.VarChar, 50)
        arParms(3) = New SqlParameter("@FE_CREA_DSDE", SqlDbType.VarChar, 10)
        arParms(4) = New SqlParameter("@FE_CREA_HSTA", SqlDbType.VarChar, 10)
        arParms(5) = New SqlParameter("@CO_ESTA", SqlDbType.VarChar, 1)

        arParms(0).Value = Cliente
        arParms(1).Value = Almacen
        arParms(2).Value = vapor
        arParms(3).Value = FechaDesde
        arParms(4).Value = FechaHasta
        arParms(5).Value = Estado

        Return SqlHelper.ExecuteDataset(cnnOfioper, CommandType.StoredProcedure, "USP_TCMOVI_INVE_BUSCAR_DOCU", arParms).Tables(0)
    End Function




    Public ReadOnly Property strErrorSql() As String
        Get
            Return mstrErrorSql
        End Get
    End Property

    Public ReadOnly Property intFilasAfectadas() As Integer
        Get
            Return mintFilasAfectadas
        End Get
    End Property

    Public Property strResultado() As String
        Get
            Return mstrResultado
        End Get
        Set(ByVal value As String)
            mstrResultado = value
        End Set
    End Property


End Class
