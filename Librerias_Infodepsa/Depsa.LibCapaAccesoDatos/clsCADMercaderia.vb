Imports System.Data.SqlClient
Public Class clsCADMercaderia
    Private strCadenaOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

    Public Function gCADGetListarInventario(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                            ByVal strReferencia As String, ByVal strNroReferencia As String, _
                                                            ByVal strModalidad As String, ByVal strFecVenc As String, _
                                                            ByVal strFecCierre As String, ByVal blnNoCerrado As Boolean, _
                                                            ByVal strTipoModulo As String, ByVal strCodAlmacen As String, ByVal strEstado As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@ISCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@ISCO_REFE", SqlDbType.Int)
        arParms(2).Value = CInt(strReferencia)
        arParms(3) = New SqlParameter("@ISNU_REFE", SqlDbType.VarChar, 20)
        arParms(3).Value = strNroReferencia
        arParms(4) = New SqlParameter("@ISCO_MODA_MOVI", SqlDbType.VarChar, 3)
        arParms(4).Value = strModalidad
        arParms(5) = New SqlParameter("@IDFE_VENC", SqlDbType.VarChar, 10)
        arParms(5).Value = strFecVenc
        arParms(6) = New SqlParameter("@IDFE_CIER", SqlDbType.VarChar, 10)
        arParms(6).Value = strFecCierre
        arParms(7) = New SqlParameter("@ISTI_CERR", SqlDbType.Bit)
        arParms(7).Value = blnNoCerrado
        arParms(8) = New SqlParameter("@ISTI_MODU", SqlDbType.Char, 1)
        arParms(8).Value = strTipoModulo
        arParms(9) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(9).Value = strCodAlmacen
        arParms(10) = New SqlParameter("@strEstado", SqlDbType.VarChar, 5)
        arParms(10).Value = strEstado
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Sel_Inventario", arParms).Tables(0)
    End Function

    Public Function gCADGetListarInventarioGlobal(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                        ByVal strTipoEntidad As String, ByVal strCodEntidad As String, _
                                                        ByVal strReferencia As String, ByVal strNroReferencia As String, _
                                                        ByVal strModalidad As String, ByVal strEstado As String, _
                                                        ByVal strCodAlmacen As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@ISCO_CLIE_ACTU", SqlDbType.VarChar, 20)
        arParms(1).Value = strCodCliente
        arParms(2) = New SqlParameter("@ISCO_TIPO_ENTI", SqlDbType.Char, 2)
        arParms(2).Value = strTipoEntidad
        arParms(3) = New SqlParameter("@ISCO_ENTI", SqlDbType.VarChar, 20)
        arParms(3).Value = strCodEntidad
        arParms(4) = New SqlParameter("@ISCO_REFE", SqlDbType.Int)
        arParms(4).Value = CInt(strReferencia)
        arParms(5) = New SqlParameter("@ISNU_REFE", SqlDbType.VarChar, 20)
        arParms(5).Value = strNroReferencia
        arParms(6) = New SqlParameter("@ISCO_MODA_MOVI", SqlDbType.VarChar, 3)
        arParms(6).Value = strModalidad
        arParms(7) = New SqlParameter("@ISCO_ESTA", SqlDbType.VarChar, 2)
        arParms(7).Value = strEstado
        arParms(8) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(8).Value = strCodAlmacen
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Sel_Inventario_Global_Motors", arParms).Tables(0)
    End Function

    Public Function gCADGetListarSolicitud(ByVal strNroWarrant As String, ByVal strFechaInicial As String, _
                                            ByVal strFechaFinal As String, ByVal strTipoEntidad As String, _
                                            ByVal strCodSico As String, ByVal strEstado As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        arParms(0) = New SqlParameter("@NroWarrant", SqlDbType.VarChar, 10)
        arParms(0).Value = strNroWarrant
        arParms(1) = New SqlParameter("@FechaInicial", SqlDbType.VarChar, 10)
        arParms(1).Value = strFechaInicial
        arParms(2) = New SqlParameter("@FechaFinal", SqlDbType.VarChar, 10)
        arParms(2).Value = strFechaFinal
        arParms(3) = New SqlParameter("@TipoEntidad", SqlDbType.Char, 2)
        arParms(3).Value = strTipoEntidad
        arParms(4) = New SqlParameter("@CodSico", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodSico
        arParms(5) = New SqlParameter("@Estado", SqlDbType.Char, 2)
        arParms(5).Value = strEstado
        Return SqlHelper.ExecuteDataset(strCadenaConexion, CommandType.StoredProcedure, "USP_WFTRASLADO_ALMACEN_CAB_Sel_ListarSolicitud", arParms).Tables(0)
    End Function

    Public Function gCNGetListarAlmacen(ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@CodCliente", SqlDbType.VarChar, 20)
        arParms(0).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaConexion, CommandType.StoredProcedure, "Usp_TMALMA_Sel_ListarAlmacenxTraslado", arParms).Tables(0)
    End Function

    Public Function gCADGetListarRetiros(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                    ByVal strNroDCR As String, ByVal strNroItem As String, ByVal strEstado As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.Char, 2)
        arParms(0).Value = strCodEmpresa
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnidad
        arParms(2) = New SqlParameter("@ISNroDCR", SqlDbType.VarChar, 20)
        arParms(2).Value = strNroDCR
        arParms(3) = New SqlParameter("@ISNroItem", SqlDbType.Int)
        arParms(3).Value = CInt(strNroItem)
        arParms(4) = New SqlParameter("@ISEstado", SqlDbType.Char, 1)
        arParms(4).Value = strEstado
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_TMSALD_ALMA_ListarRetiros", arParms).Tables(0)
    End Function

    Public Function gCNCrearTranladoGlobal(ByVal strCodEmpresa As String, ByVal strCodUnid As String, _
                                     ByVal strNroComp As String, ByVal strNroItem As Integer, _
                                      ByVal strNroDocu As String, ByVal strUbiOri As String, _
                                      ByVal strUbiDes As String, ByVal strEst As String, _
                                      ByVal strUsuCrea As String, ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(8) {}
            arParms(0) = New SqlParameter("@COD_EMPR", SqlDbType.Char, 2)
            arParms(0).Value = strCodEmpresa
            arParms(1) = New SqlParameter("@COD_UNID", SqlDbType.Char, 3)
            arParms(1).Value = strCodUnid
            arParms(2) = New SqlParameter("@NRO_COMP", SqlDbType.VarChar, 20)
            arParms(2).Value = strNroComp
            arParms(3) = New SqlParameter("@NRO_ITEM", SqlDbType.Int)
            arParms(3).Value = strNroItem
            arParms(4) = New SqlParameter("@NRO_DOCU", SqlDbType.VarChar, 8)
            arParms(4).Value = strNroDocu
            arParms(5) = New SqlParameter("@UBI_ORIG", SqlDbType.VarChar, 500)
            arParms(5).Value = strUbiOri
            arParms(6) = New SqlParameter("@UBI_FINA", SqlDbType.VarChar, 500)
            arParms(6).Value = strUbiDes
            arParms(7) = New SqlParameter("@EST_TRAS", SqlDbType.Char, 2)
            arParms(7).Value = strEst
            arParms(8) = New SqlParameter("@USR_CREA", SqlDbType.Char, 4)
            arParms(8).Value = strUsuCrea
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "USP_TMSALD_ALMA_Ins_Inventario_Global_Motors", arParms)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCNInsAlmacenNuevo(ByVal strAlmacen As String, ByVal strCodCliente As String, ByVal strCodUsuario As String, ByVal objTransaction As SqlTransaction) As String
        Try
            Dim arParms() As SqlParameter = New SqlParameter(2) {}
            arParms(0) = New SqlParameter("@DE_ALMA", SqlDbType.VarChar, 200)
            arParms(0).Value = strAlmacen
            arParms(1) = New SqlParameter("@COD_CLIE", SqlDbType.VarChar, 20)
            arParms(1).Value = strCodCliente
            arParms(2) = New SqlParameter("@COD_USER", SqlDbType.VarChar, 4)
            arParms(2).Value = strCodUsuario
            SqlHelper.ExecuteNonQuery(objTransaction, CommandType.StoredProcedure, "Usp_TMALMA_Ins_AlmacenTrasladoNuevo", arParms)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Sub gCNUpdEstadoSolicitud(ByVal strNroSolicitud As String, ByVal strCodUsuario As String, ByVal strAccion As String)
        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        arParms(0) = New SqlParameter("@NroSolicitud", SqlDbType.VarChar, 10)
        arParms(0).Value = strNroSolicitud
        arParms(1) = New SqlParameter("@CodUsuario", SqlDbType.Char, 4)
        arParms(1).Value = strCodUsuario
        arParms(2) = New SqlParameter("@Accion", SqlDbType.Char, 1)
        arParms(2).Value = strAccion
        SqlHelper.ExecuteNonQuery(strCadenaConexion, CommandType.StoredProcedure, "Usp_WFTRASLADO_ALMACEN_CAB_Upd_EstadoSolicitud", arParms)
    End Sub

    Public Function gCADGetListarInventarioWMS(ByVal strCodigo As String, ByVal strDescMerc As String, _
                                                ByVal strLote As String, ByVal strFechaInicial As String, _
                                                ByVal strFechaFinal As String, ByVal strFechaVenc As String, _
                                                ByVal strAlmacen As String, ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        arParms(0) = New SqlParameter("@Orden", SqlDbType.VarChar, 200)
        arParms(0).Value = strCodigo
        arParms(1) = New SqlParameter("@Descrip", SqlDbType.VarChar, 200)
        arParms(1).Value = strDescMerc
        arParms(2) = New SqlParameter("@Lote", SqlDbType.VarChar, 200)
        arParms(2).Value = strLote
        arParms(3) = New SqlParameter("@FecInicial", SqlDbType.VarChar, 10)
        arParms(3).Value = strFechaInicial
        arParms(4) = New SqlParameter("@FecFinal", SqlDbType.VarChar, 10)
        arParms(4).Value = strFechaFinal
        arParms(5) = New SqlParameter("@FecVenci", SqlDbType.VarChar, 10)
        arParms(5).Value = strFechaVenc
        arParms(6) = New SqlParameter("@Almacen", SqlDbType.VarChar, 20)
        arParms(6).Value = strAlmacen
        arParms(7) = New SqlParameter("@CO_CLIE", SqlDbType.VarChar, 20)
        arParms(7).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "USP_IV_F_SALDO_WMS", arParms).Tables(0)
    End Function

    Public Function gCADGetGeneraRepoBulxPal(ByVal strCodEmpr As String, ByVal strCodUnid As String, ByVal strFechaInicio As String, ByVal strCodAlma As String, ByVal strCodCliente As String) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        arParms(0) = New SqlParameter("@ISCO_EMPR", SqlDbType.VarChar, 2)
        arParms(0).Value = strCodEmpr
        arParms(1) = New SqlParameter("@ISCO_UNID", SqlDbType.VarChar, 3)
        arParms(1).Value = strCodUnid
        arParms(2) = New SqlParameter("@ISCO_ALMA", SqlDbType.VarChar, 5)
        arParms(2).Value = strCodAlma
        arParms(3) = New SqlParameter("@IDFE_INIC", SqlDbType.DateTime)
        arParms(3).Value = strFechaInicio
        arParms(4) = New SqlParameter("@ISCO_CLIE", SqlDbType.VarChar, 20)
        arParms(4).Value = strCodCliente
        Return SqlHelper.ExecuteDataset(strCadenaOfioper, CommandType.StoredProcedure, "SP_REPO_PALE_BULT", arParms).Tables(0)
    End Function
End Class
