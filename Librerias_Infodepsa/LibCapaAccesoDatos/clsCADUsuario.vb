Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADUsuario
    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringSeguridad")
    Private lstrCodUsuario As String
    Private lstrClave As String

    Private lintRetorno As Integer
    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private dtTemporal As DataTable

    Public Function gdtValidaUsuario() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(1) = New SqlParameter("@K_NO_CLAV", SqlDbType.VarChar, 8)
            arParms(2) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(4) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)

            arParms(2).Direction = ParameterDirection.Output
            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.ReturnValue

            arParms(0).Value = lstrCodUsuario
            arParms(1).Value = lstrClave

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_VALI_USUA", arParms).Tables(0)
            lstrErrorSql = arParms(2).Value.ToString
            lintFilasAfectadas = CInt(arParms(3).Value)
            lintRetorno = CInt(arParms(4).Value)
            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function gdtConTipoCliente() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(2) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(1).Direction = ParameterDirection.Output
            arParms(2).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodUsuario

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_TIPO_CLIE", arParms).Tables(0)
            lstrErrorSql = arParms(1).Value.ToString
            lintFilasAfectadas = CInt(arParms(2).Value)

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strClave() As String
        Get
            Return lstrClave
        End Get
        Set(ByVal Value As String)
            lstrClave = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public ReadOnly Property intRetorno() As Integer
        Get
            Return lintRetorno
        End Get
    End Property

    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub
End Class
