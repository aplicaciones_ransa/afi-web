Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADCliente
    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")
    Private lstrCodCliente As String
    Private lstrCodUsuario As String

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private dtTemporal As DataTable

    Public Function gdtConseguirAreas() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(3) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(2) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(2).Direction = ParameterDirection.Output
            arParms(3).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodUsuario

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_AREA", arParms).Tables(0)
            lstrErrorSql = arParms(2).Value.ToString
            lintFilasAfectadas = CInt(arParms(3).Value)

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property


End Class
