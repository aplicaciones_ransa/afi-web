Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADMenu
    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringSeguridad")
    Private lintCodModulo As Integer
    Private lstrCodGrupo As String
    Private lstrCodModalidad As String

    Private lintCodMenu As Integer
    Private lstrTipoSituacion As String
    Private lstrEstPaginaInicio As String
    Private lstrCodUsuario As String
    Private lstrFlagSeleccion As String

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private dtTemporal As DataTable

    Public Function gdtConseguirMenu() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_MODU", SqlDbType.Int)
            arParms(1) = New SqlParameter("@K_CO_GRUP", SqlDbType.VarChar, 8)
            arParms(2) = New SqlParameter("@K_CO_MODA", SqlDbType.VarChar, 1)
            arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(4) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.Output

            arParms(0).Value = lintCodModulo
            arParms(1).Value = lstrCodGrupo
            arParms(2).Value = lstrCodModalidad

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_MENU", arParms).Tables(0)
            lstrErrorSql = arParms(3).Value.ToString
            lintFilasAfectadas = CInt(arParms(4).Value)

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gbolInsertarMenu(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(8) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_GRUP", SqlDbType.VarChar, 8)
            arParms(1) = New SqlParameter("@K_CO_MODU", SqlDbType.Int)
            arParms(2) = New SqlParameter("@K_CO_MENU", SqlDbType.Int)
            arParms(3) = New SqlParameter("@K_TI_SITU", SqlDbType.VarChar, 3)
            arParms(4) = New SqlParameter("@K_ST_PAGI_INIC", SqlDbType.VarChar, 1)
            arParms(5) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(6) = New SqlParameter("@K_FL_SELE", SqlDbType.VarChar, 1)
            arParms(7) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(8) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(7).Direction = ParameterDirection.Output
            arParms(8).Direction = ParameterDirection.Output



            arParms(0).Value = lstrCodGrupo
            arParms(1).Value = lintCodModulo
            arParms(2).Value = lintCodMenu
            arParms(3).Value = lstrTipoSituacion
            arParms(4).Value = lstrEstPaginaInicio
            arParms(5).Value = lstrCodUsuario
            arParms(6).Value = lstrFlagSeleccion

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAI_GRUP_MENU", arParms)
            lstrErrorSql = arParms(7).Value().ToString()
            lintFilasAfectadas = CInt(arParms(8).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function



    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        dtTemporal = Nothing
    End Sub


    Public Property intCodModulo() As Integer
        Get
            Return lintCodModulo
        End Get
        Set(ByVal Value As Integer)
            lintCodModulo = Value
        End Set
    End Property
    Public Property strCodGrupo() As String
        Get
            Return lstrCodGrupo
        End Get
        Set(ByVal Value As String)
            lstrCodGrupo = Value
        End Set
    End Property
    Public Property strCodModalidad() As String
        Get
            Return lstrCodModalidad
        End Get
        Set(ByVal Value As String)
            lstrCodModalidad = Value
        End Set
    End Property

    Public Property strFlagSeleccion() As String
        Get
            Return lstrFlagSeleccion
        End Get
        Set(ByVal Value As String)
            lstrFlagSeleccion = Value
        End Set
    End Property
    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strEstPaginaInicio() As String
        Get
            Return lstrEstPaginaInicio
        End Get
        Set(ByVal Value As String)
            lstrEstPaginaInicio = Value
        End Set
    End Property
    Public Property strTipoSituacion() As String
        Get
            Return lstrTipoSituacion
        End Get
        Set(ByVal Value As String)
            lstrTipoSituacion = Value
        End Set
    End Property
    Public Property intCodMenu() As Integer
        Get
            Return lintCodMenu
        End Get
        Set(ByVal Value As Integer)
            lintCodMenu = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strCadenaInicial() As String
        Get
            Return lstrCadenaInicial
        End Get
        Set(ByVal Value As String)
            lstrCadenaInicial = Value
        End Set
    End Property


    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub
End Class
