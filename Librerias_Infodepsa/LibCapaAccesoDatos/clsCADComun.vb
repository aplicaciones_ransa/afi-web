'************************************************************************************
'Clase  	    :	clsComun.vb
'Modulo 	    :	Libreria de Acceso a Datos
'Descripción	:   La clase contiene funciones genericas de acceso a datos.
'Entradas	    :	Lista y descripción de todos los valores de entrada.
'Creación	    :   15/01/2007 - 15:51
'Encargado	    :   Noel Callapiña Angles - NCA
'Empresa	    :   CosapiSoft S.A.
'Modificación	:   15/01/2007 - 15:51
'Encargado	    :    
'Empresa	    :   CosapiSoft S.A.
'Notas	        :    
'************************************************************************************

Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADComun
    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")
    Private lintFilasAfectadas As Integer
    Private lstrErrorSql As String
    Private dtTemporal As DataTable

    Public Function gdtComboMaestro(ByVal pstrIndicador As String, ByVal pstrCriterio1 As String,
    ByVal pstrCriterio2 As String, Optional ByVal pintTipoCadenaConexion As Integer = 0) As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}

        Try
            arParms(0) = New SqlParameter("@K_INDICADOR", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CRITERIO1", SqlDbType.VarChar, 25)
            arParms(2) = New SqlParameter("@K_CRITERIO2", SqlDbType.VarChar, 25)
            arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(4) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.Output

            arParms(0).Value = pstrIndicador
            arParms(1).Value = pstrCriterio1
            arParms(2).Value = pstrCriterio2

            If pintTipoCadenaConexion = 1 Then
                lstrCadenaInicial = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringSeguridad")
            End If
            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_MAES_COMB", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(4).Value)
            lstrErrorSql = CStr(arParms(3).Value)

            Return dtTemporal

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public ReadOnly Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
    End Property
    Public ReadOnly Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
    End Property

End Class
