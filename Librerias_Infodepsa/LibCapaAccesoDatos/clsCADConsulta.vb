Imports System.Data.SqlClient

Public Class clsCADConsulta
    Private lstrCodCliente As String
    Private lstrCodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrCodArea As String
    Private lintNumRequTemp As Integer
    Private lstrCodTipoConsulta As String
    Private lstrCodTipoItem As String
    Private lstrCodTipoEnvio As String
    Private lstrDesObservacion As String
    Private lstrCodUsuario As String
    Private lintNumConsulta As Integer
    Private lstrDesEnvio As String
    Private lstrIdUnidad As String
    Private lstrCodAlmacen As String
    Private lstrCodLocalidad As String
    Private lstrTipoDocumento As String
    Private lstrNumDocumento As String
    Private lstrCodCriterio As String
    Private lstrCodDivision As String
    Private lstrCont As String
    Private lstrSecuDivi As Integer
    Private lstrCodPersonaAutorizada As String
    Private lstrAprobador As String
    Private lstrTipoCons As String

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer

    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")

    Public Function gbolInsConsultaTemporal(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(20) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_DE_ENVI", SqlDbType.VarChar, 200)
            arParms(7) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
            arParms(8) = New SqlParameter("@K_CO_LOCA", SqlDbType.VarChar, 3)
            arParms(9) = New SqlParameter("@K_TI_DOCU", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_NU_DOCU", SqlDbType.VarChar, 20)
            arParms(11) = New SqlParameter("@K_DE_OBSE_0001", SqlDbType.VarChar, 200)
            arParms(12) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(13) = New SqlParameter("@K_CO_CRIT", SqlDbType.VarChar, 5)
            arParms(14) = New SqlParameter("@K_CO_DIVI", SqlDbType.VarChar, 20)
            arParms(15) = New SqlParameter("@K_NU_CONT", SqlDbType.VarChar, 4)
            arParms(16) = New SqlParameter("@K_NU_SECU_DIVI", SqlDbType.Int)
            arParms(17) = New SqlParameter("@K_CO_USUA_CREA", SqlDbType.VarChar, 8)
            arParms(18) = New SqlParameter("@K_CO_TIPO_CONS", SqlDbType.VarChar, 3)
            arParms(19) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(20) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(19).Direction = ParameterDirection.Output
            arParms(20).Direction = ParameterDirection.Output

            gSelConseguirNumConsultaTemporal()

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lintNumConsulta
            arParms(5).Value = lstrCodTipoItem
            arParms(6).Value = lstrDesEnvio
            arParms(7).Value = lstrIdUnidad
            arParms(8).Value = lstrCodLocalidad
            arParms(9).Value = lstrTipoDocumento
            arParms(10).Value = lstrNumDocumento
            arParms(11).Value = lstrDesObservacion
            arParms(12).Value = lstrCodArea
            arParms(13).Value = lstrCodCriterio
            arParms(14).Value = lstrCodDivision
            arParms(15).Value = lstrCont
            arParms(16).Value = lstrSecuDivi
            arParms(17).Value = lstrCodUsuario
            arParms(18).Value = lstrTipoCons

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAI_CONS_TEMP", arParms)
            lstrErrorSql = arParms(19).Value().ToString()
            lintFilasAfectadas = CInt(arParms(20).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    'Private Sub gSelConseguirNumConsultaTemporal()
    '    Dim arParms() As SqlParameter = New SqlParameter(6) {}

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
    '        arParms(4) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
    '        arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(4).Direction = ParameterDirection.Output
    '        arParms(5).Direction = ParameterDirection.Output
    '        arParms(6).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodEmpresa
    '        arParms(1).Value = lstrCodUnidad
    '        arParms(2).Value = lstrCodCliente
    '        arParms(3).Value = lintNumRequTemp

    '        clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_ID_CONS_TEMP", arParms)
    '        lintNumConsulta = CInt(arParms(4).Value())
    '        lstrErrorSql = arParms(5).Value().ToString()
    '        lintFilasAfectadas = CInt(arParms(6).Value().ToString())

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Sub

    Private Sub gSelConseguirNumConsultaTemporal()
        Dim arParms() As SqlParameter = New SqlParameter(7) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)

            arParms(5) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
            arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lstrCodArea

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_ID_CONS_TEMP", arParms)
            lintNumConsulta = CInt(arParms(5).Value())
            lstrErrorSql = arParms(6).Value().ToString()
            lintFilasAfectadas = CInt(arParms(7).Value().ToString())

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub


    'Public Function gbolEliminarConsulta(ByVal ObjTrans As SqlTransaction) As Boolean
    '    Dim arParms() As SqlParameter = New SqlParameter(6) {}

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
    '        arParms(4) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
    '        arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(5).Direction = ParameterDirection.Output
    '        arParms(6).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodEmpresa
    '        arParms(1).Value = lstrCodUnidad
    '        arParms(2).Value = lstrCodCliente
    '        arParms(3).Value = lintNumRequTemp
    '        arParms(4).Value = lintNumConsulta

    '        clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAD_CONS_TEMP", arParms)
    '        lstrErrorSql = arParms(5).Value().ToString()
    '        lintFilasAfectadas = CInt(arParms(6).Value().ToString())
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        Return False
    '    End Try

    'End Function

    Public Function gbolEliminarConsulta(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(7) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
            arParms(5) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)

            arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lintNumConsulta
            arParms(5).Value = lstrCodArea

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAD_CONS_TEMP", arParms)
            lstrErrorSql = arParms(6).Value().ToString()
            lintFilasAfectadas = CInt(arParms(7).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function gbolActualizarConsulta(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(7) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_NU_CONS", SqlDbType.Int)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ENVI", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lintNumConsulta
            arParms(5).Value = lstrCodTipoEnvio

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAU_CONS_TEMP", arParms)
            lstrErrorSql = arParms(6).Value().ToString()
            lintFilasAfectadas = CInt(arParms(7).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function
    Public Sub gUsuarioAprobador()
        Dim arParms() As SqlParameter = New SqlParameter(6) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_PERS", SqlDbType.VarChar, 6)
            arParms(2) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(4) = New SqlParameter("@K_ST_APRO", SqlDbType.VarChar, 1)
            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodPersonaAutorizada
            arParms(2).Value = lstrCodArea
            arParms(3).Value = lstrCodUsuario

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_USUA_APRO", arParms)
            lstrAprobador = arParms(4).Value()
            lstrErrorSql = arParms(5).Value().ToString()
            lintFilasAfectadas = CInt(arParms(6).Value().ToString())

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Function gSelConseguirItemApropiado(ByVal ObjTrans As SqlTransaction)

        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        Try
            arParms(0) = New SqlParameter("@K_COD_EMPRESA", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_COD_UNIDAD", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_COD_CLIENTE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_COD_ID_UNID ", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_COD_NU_CONT", SqlDbType.VarChar, 4)
            arParms(5) = New SqlParameter("@K_COD_DIVI ", SqlDbType.VarChar, 6)
            arParms(6) = New SqlParameter("@K_COD_ESTADO", SqlDbType.Int)
            arParms(7) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(8) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(9) = New SqlParameter("@K_COD_USUARIO_TEMP", SqlDbType.VarChar, 200)


            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output
            arParms(8).Direction = ParameterDirection.Output
            arParms(9).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrIdUnidad
            arParms(4).Value = lstrCont
            arParms(5).Value = lstrCodDivision

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_DOCU_ESTADO", arParms)

            lintNumConsulta = CInt(arParms(6).Value())
            lstrErrorSql = arParms(7).Value().ToString()
            lintFilasAfectadas = CInt(arParms(8).Value().ToString())
            lstrCodPersonaAutorizada = arParms(9).Value().ToString()
            If lintNumConsulta = 1 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gbolEliminarTemporal(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(5) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_USUARIO", SqlDbType.VarChar, 30)

            arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)


            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodUsuario

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAD_CONS_TEMP_USUARIO", arParms)

            lstrErrorSql = arParms(4).Value().ToString()
            lintFilasAfectadas = CInt(arParms(5).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function


    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property
    Public Property strCodEmpresa() As String
        Get
            Return lstrCodEmpresa
        End Get
        Set(ByVal Value As String)
            lstrCodEmpresa = Value
        End Set
    End Property
    Public Property strCodUnidad() As String
        Get
            Return lstrCodUnidad
        End Get
        Set(ByVal Value As String)
            lstrCodUnidad = Value
        End Set
    End Property
    Public Property strCodArea() As String
        Get
            Return lstrCodArea
        End Get
        Set(ByVal Value As String)
            lstrCodArea = Value
        End Set
    End Property
    Public Property intNumRequTemp() As Integer
        Get
            Return lintNumRequTemp
        End Get
        Set(ByVal Value As Integer)
            lintNumRequTemp = Value
        End Set
    End Property
    Public Property strCodTipoConsulta() As String
        Get
            Return lstrCodTipoConsulta
        End Get
        Set(ByVal Value As String)
            lstrCodTipoConsulta = Value
        End Set
    End Property
    Public Property strCodTipoItem() As String
        Get
            Return lstrCodTipoItem
        End Get
        Set(ByVal Value As String)
            lstrCodTipoItem = Value
        End Set
    End Property
    Public Property strCodTipoEnvio() As String
        Get
            Return lstrCodTipoEnvio
        End Get
        Set(ByVal Value As String)
            lstrCodTipoEnvio = Value
        End Set
    End Property
    Public Property strDesObservacion() As String
        Get
            Return lstrDesObservacion
        End Get
        Set(ByVal Value As String)
            lstrDesObservacion = Value
        End Set
    End Property
    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property intNumConsulta() As Integer
        Get
            Return lintNumConsulta
        End Get
        Set(ByVal Value As Integer)
            lintNumConsulta = Value
        End Set
    End Property
    Public Property strDesEnvio() As String
        Get
            Return lstrDesEnvio
        End Get
        Set(ByVal Value As String)
            lstrDesEnvio = Value
        End Set
    End Property
    Public Property strIdUnidad() As String
        Get
            Return lstrIdUnidad
        End Get
        Set(ByVal Value As String)
            lstrIdUnidad = Value
        End Set
    End Property
    Public Property strCodAlmacen() As String
        Get
            Return lstrCodAlmacen
        End Get
        Set(ByVal Value As String)
            lstrCodAlmacen = Value
        End Set
    End Property
    Public Property strCodLocalidad() As String
        Get
            Return lstrCodLocalidad
        End Get
        Set(ByVal Value As String)
            lstrCodLocalidad = Value
        End Set
    End Property
    Public Property strTipoDocumento() As String
        Get
            Return lstrTipoDocumento
        End Get
        Set(ByVal Value As String)
            lstrTipoDocumento = Value
        End Set
    End Property
    Public Property strNumDocumento() As String
        Get
            Return lstrNumDocumento
        End Get
        Set(ByVal Value As String)
            lstrNumDocumento = Value
        End Set
    End Property
    Public Property strCodCriterio() As String
        Get
            Return lstrCodCriterio
        End Get
        Set(ByVal Value As String)
            lstrCodCriterio = Value
        End Set
    End Property
    Public Property strCodDivision() As String
        Get
            Return lstrCodDivision
        End Get
        Set(ByVal Value As String)
            lstrCodDivision = Value
        End Set
    End Property
    Public Property strCont() As String
        Get
            Return lstrCont
        End Get
        Set(ByVal Value As String)
            lstrCont = Value
        End Set
    End Property
    Public Property strSecuDivi() As String
        Get
            Return lstrSecuDivi
        End Get
        Set(ByVal Value As String)
            lstrSecuDivi = Value
        End Set
    End Property
    Public Property strCodPersonaAutorizada() As String
        Get
            Return lstrCodPersonaAutorizada
        End Get
        Set(ByVal Value As String)
            lstrCodPersonaAutorizada = Value
        End Set
    End Property
    Public Property strAprobador() As String
        Get
            Return lstrAprobador
        End Get
        Set(ByVal Value As String)
            lstrAprobador = Value
        End Set
    End Property
    Public Property strTipoCons() As String
        Get
            Return lstrTipoCons
        End Get
        Set(ByVal Value As String)
            lstrTipoCons = Value
        End Set
    End Property

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property

    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub
    Public Property strCadenaConexion() As String
        Get
            Return lstrCadenaInicial
        End Get
        Set(ByVal Value As String)
            lstrCadenaInicial = Value
        End Set
    End Property

End Class
