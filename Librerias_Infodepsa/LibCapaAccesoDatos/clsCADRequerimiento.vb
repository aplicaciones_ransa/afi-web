Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADRequerimiento
    Private lstrCodCliente As String
    Private lstrCodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrCodClieAdic As String
    Private lstrCodArea As String
    Private lstrCodRequerimiento As String
    Private lstrCodEstado As String
    Private lstrCodSituacion As String
    Private lstrCodPersAutorizada As String
    Private lstrCodFechaDesde As String
    Private lstrCodFechaHasta As String
    Private lintNumRequTemp As Integer
    Private lstrCodTipoMedioRecepcion As String
    Private lstrCodTipoConsulta As String
    Private lstrCodTipoItem As String
    Private lstrCodTipoEnvio As String
    Private lstrDesPersSolicitante As String
    Private lstrDesDireccionEnvio As String
    Private lstrEstConsultaUrgente As String
    Private lstrDesObservacion As String
    Private lintNumTotalConsulta As Integer
    Private lstrCodUsuario As String
    Private lstrEstadoUrgente As String
    Private lstrEstadoRequimiento As String
    Private lstrCodTipoUnidad As String
    Private lstrTotConReq As String 'Usuario que ingresa al sistema
    Private lstrUsuarioLogin As String
    Private lstrTiempoAtencion As String

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private lintValorRetorno As Integer
    Private lstrMensajeError As String
    Private lstrNombre As String

    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")


    'Public Function gdtConEstadoRequerimiento() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(12) {}
    '    Dim dtTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_COD_CLIENTE", SqlDbType.VarChar, 20)
    '        arParms(1) = New SqlParameter("@K_COD_EMPRESA", SqlDbType.VarChar, 2)
    '        arParms(2) = New SqlParameter("@K_COD_UNIDAD", SqlDbType.VarChar, 3)
    '        arParms(3) = New SqlParameter("@K_COD_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_COD_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_COD_REQUERIMIENTO", SqlDbType.VarChar, 20)
    '        arParms(6) = New SqlParameter("@K_COD_ESTADO", SqlDbType.VarChar, 3)
    '        arParms(7) = New SqlParameter("@K_COD_SITUACION", SqlDbType.VarChar, 3)
    '        arParms(8) = New SqlParameter("@K_COD_PERS_AUTORIZADA", SqlDbType.VarChar, 6)
    '        arParms(9) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
    '        arParms(10) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
    '        arParms(11) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(12) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(11).Direction = ParameterDirection.Output
    '        arParms(12).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrCodRequerimiento
    '        arParms(6).Value = lstrCodEstado
    '        arParms(7).Value = lstrCodSituacion
    '        arParms(8).Value = lstrCodPersAutorizada
    '        arParms(9).Value = lstrCodFechaDesde
    '        arParms(10).Value = lstrCodFechaHasta

    '        dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_SITU_REQU", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(12).Value)
    '        lstrErrorSql = arParms(11).Value
    '        Return dtTemporal

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtConEstadoRequerimiento() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(13) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_COD_CLIENTE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_COD_EMPRESA", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_COD_UNIDAD", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_COD_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_COD_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_COD_REQUERIMIENTO", SqlDbType.VarChar, 20)
            arParms(6) = New SqlParameter("@K_COD_ESTADO", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_COD_SITUACION", SqlDbType.VarChar, 3)
            arParms(8) = New SqlParameter("@K_COD_PERS_AUTORIZADA", SqlDbType.VarChar, 6)
            arParms(9) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(10) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(11) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(12) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(13) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)

            arParms(11).Direction = ParameterDirection.Output
            arParms(12).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodRequerimiento
            arParms(6).Value = lstrCodEstado
            arParms(7).Value = lstrCodSituacion
            arParms(8).Value = lstrCodPersAutorizada
            arParms(9).Value = lstrCodFechaDesde
            arParms(10).Value = lstrCodFechaHasta
            arParms(13).Value = lstrUsuarioLogin

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_SITU_REQU", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(12).Value)
            lstrErrorSql = arParms(11).Value
            Return dtTemporal

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConCabeceraRequerimiento() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        Dim dtTemp As DataTable

        Try
            arParms(0) = New SqlParameter("@K_COD_CLIENTE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_COD_EMPRESA", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_COD_UNIDAD", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_COD_REQUERIMIENTO", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_COD_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodRequerimiento
            arParms(4).Value = lstrCodClieAdic

            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CABE_REQU", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(6).Value)
            lstrErrorSql = arParms(5).Value

            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    'Public Function gdtCuotasXCobrar_Cabecera() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    '    Dim dtTemp As DataTable

    '    Try
    '        'arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 10)
    '        arParms(0) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(1) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(0).Direction = ParameterDirection.Output
    '        arParms(1).Direction = ParameterDirection.Output

    '        'arParms(0).Value = lstrCodCliente
    '        'arParms(1).Value = lstrCodEmpresa
    '        'arParms(2).Value = lstrCodUnidad
    '        'arParms(3).Value = lstrCodRequerimiento
    '        'arParms(4).Value = lstrCodClieAdic

    '        dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "sp_RepInterbank_CuotasXCobrar", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(1).Value)
    '        lstrErrorSql = arParms(0).Value

    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    'Public Function gdtCuotasXCobrar_Detalle() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    '    Dim dtTemp As DataTable

    '    Try
    '        'arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 10)
    '        arParms(0) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(1) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(0).Direction = ParameterDirection.Output
    '        arParms(1).Direction = ParameterDirection.Output

    '        'arParms(0).Value = lstrCodCliente
    '        'arParms(1).Value = lstrCodEmpresa
    '        'arParms(2).Value = lstrCodUnidad
    '        'arParms(3).Value = lstrCodRequerimiento
    '        'arParms(4).Value = lstrCodClieAdic

    '        dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "sp_RepInterbank_CuotasXCobrar", arParms).Tables(1)
    '        lintFilasAfectadas = CInt(arParms(1).Value)
    '        lstrErrorSql = arParms(0).Value

    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function


    'Public Function gdtCuotasXCobrar() As DataSet
    '    Dim arParms() As SqlParameter = New SqlParameter(2) {}
    '    Dim dtTemp As DataSet

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 10)
    '        arParms(1) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)


    '        'arParms(1).Direction = ParameterDirection.Output
    '        'arParms(2).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        'arParms(3).Value = lstrCodRequerimiento
    '        'arParms(4).Value = lstrCodClieAdic

    '        dtTemp = SQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "sp_RepInterbank_CuotasXCobrar", arParms)

    '        'lintFilasAfectadas = CInt(arParms(2).Value)
    '        'lstrErrorSql = arParms(1).Value

    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function


    Public Function gdtConMensajeRegistro() As DataTable 'modificado auris
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        Dim dtTemp As DataTable

        'Dim strcadena As String
        'Dim strcadenaFin As String
        'Dim strcadenaIni As String
        'Dim intcontador() As String
        'Dim i As Integer

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU", SqlDbType.VarChar, 200)
            arParms(4) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 1)
            arParms(5) = New SqlParameter("@K_ST_REQU", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            'intcontador = Split(lstrCodRequerimiento, ",")
            'For i = 0 To UBound(intcontador, 1)
            '    strcadena = intcontador(i)
            '    strcadenaFin = strcadenaFin + ",'" + strcadena + "'"
            'Next
            'lstrCodRequerimiento = Mid(strcadenaFin, 2)
            'lstrCodRequerimiento = Mid(lstrCodRequerimiento, 1, Len(lstrCodRequerimiento) - 2)


            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodRequerimiento
            arParms(4).Value = lstrCodTipoItem
            arParms(5).Value = lstrEstadoRequimiento

            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_MENS_REGI", arParms).Tables(0)
            lstrErrorSql = arParms(6).Value().ToString()
            lintFilasAfectadas = CInt(arParms(7).Value().ToString())
            ' lstrCodRequerimiento = strcadenaIni
            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    'Public Function gdtConMensajeRegistro() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(7) {}
    '    Dim dtTemp As DataTable
    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(3) = New SqlParameter("@K_NU_REQU", SqlDbType.VarChar, 20)
    '        arParms(4) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 1)
    '        arParms(5) = New SqlParameter("@K_ST_REQU", SqlDbType.VarChar, 3)
    '        arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(6).Direction = ParameterDirection.Output
    '        arParms(7).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodEmpresa
    '        dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_MENS_REGI", arParms).Tables(0)
    '        lstrErrorSql = arParms(6).Value().ToString()
    '        lintFilasAfectadas = CInt(arParms(7).Value().ToString())
    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtConRequerimientoxAprobar() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        Dim dtTemp As DataTable
        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_CO_TIPO_ENVI", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(6) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(7) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(8) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(9) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(8).Direction = ParameterDirection.Output
            arParms(9).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodArea
            arParms(4).Value = lstrCodTipoEnvio
            arParms(5).Value = lstrCodFechaDesde
            arParms(6).Value = lstrCodFechaHasta
            arParms(7).Value = lstrCodUsuario

            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_REQU_X_APRO", arParms).Tables(0)
            lstrErrorSql = arParms(8).Value().ToString()
            lintFilasAfectadas = CInt(arParms(9).Value().ToString())
            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConDetalleRequerimiento() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}
        Dim dtTemp As DataTable
        Try
            arParms(0) = New SqlParameter("@K_COD_CLIENTE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_COD_EMPRESA", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_COD_UNIDAD", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_COD_REQUERIMIENTO", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodRequerimiento
            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_DETA_REQU", arParms).Tables(0)
            lstrErrorSql = arParms(4).Value().ToString()
            lintFilasAfectadas = CInt(arParms(5).Value().ToString())

            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function gdtConDetalleRequerimientoTemporal() As DataTable
        Dim dtTemp As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_TI_UNID", SqlDbType.VarChar, 1)
            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lstrCodTipoUnidad

            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_DETA_REQU_TEMP", arParms).Tables(0)
            lstrErrorSql = arParms(5).Value().ToString()
            lintFilasAfectadas = CInt(arParms(6).Value().ToString())
            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function gdtConDetalleRequerimientoTemp() As DataTable
        Dim dtTemp As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(5) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lintNumRequTemp

            dtTemp = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_DETA_REQU_TEMP2", arParms).Tables(0)
            lstrErrorSql = arParms(4).Value().ToString()
            lintFilasAfectadas = CInt(arParms(5).Value().ToString())
            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function gbolInsRequerimientoTemporal(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(17) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_NU_REQU_ASIG", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(6) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_CO_PERS_AUTO", SqlDbType.VarChar, 6)
            arParms(8) = New SqlParameter("@K_DE_PERS_SOLI", SqlDbType.VarChar, 100)
            arParms(9) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(10) = New SqlParameter("@K_DE_DIRE_ENVI", SqlDbType.VarChar, 150)
            arParms(11) = New SqlParameter("@K_ST_CONS_URGE", SqlDbType.VarChar, 1)
            arParms(12) = New SqlParameter("@K_DE_OBSE_0001", SqlDbType.VarChar, 200)
            arParms(13) = New SqlParameter("@K_NU_TOTA_CONS", SqlDbType.Int)
            arParms(14) = New SqlParameter("@K_CO_USUA_CREA", SqlDbType.VarChar, 8)
            arParms(15) = New SqlParameter("@K_CO_TIPO_CONS", SqlDbType.VarChar, 3)
            arParms(16) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(17) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(16).Direction = ParameterDirection.Output
            arParms(17).Direction = ParameterDirection.Output

            gSelConseguirNumRequerimientoTemporal()

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lstrCodRequerimiento
            arParms(5).Value = lstrCodClieAdic
            arParms(6).Value = lstrCodTipoItem
            arParms(7).Value = lstrCodPersAutorizada
            arParms(8).Value = lstrDesPersSolicitante
            arParms(9).Value = lstrCodArea
            arParms(10).Value = lstrDesDireccionEnvio
            arParms(11).Value = lstrEstConsultaUrgente
            arParms(12).Value = lstrDesObservacion
            arParms(13).Value = lintNumTotalConsulta
            arParms(14).Value = lstrCodUsuario
            arParms(15).Value = lstrCodTipoConsulta

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAI_REQU_TEMP", arParms)
            lstrErrorSql = arParms(16).Value().ToString()
            lintFilasAfectadas = CInt(arParms(17).Value().ToString())
            If lintFilasAfectadas = 0 Then
                If lstrErrorSql = "001" Then
                    lstrMensajeError = "El �rea seleccionada no tiene un codigo de persona para su registro satisfactorio. Asignar un c�digo de persona para el �rea elegida."
                End If
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Private Sub gSelConseguirNumRequerimientoTemporal()
        Dim arParms() As SqlParameter = New SqlParameter(5) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_ID_REQU_TEMP", arParms)
            lintNumRequTemp = CInt(arParms(3).Value())
            lstrErrorSql = arParms(4).Value().ToString()
            lintFilasAfectadas = CInt(arParms(5).Value().ToString())

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Function gbolActualizarRequerimiento(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(10) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_NU_REQU_ASIG", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ENVI", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_ST_CONS_URGE", SqlDbType.VarChar, 1)
            arParms(7) = New SqlParameter("@K_ST_REQU", SqlDbType.VarChar, 1)
            arParms(8) = New SqlParameter("@K_NU_TOTA_CONS", SqlDbType.Int)
            arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(9).Direction = ParameterDirection.Output
            arParms(10).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp
            arParms(4).Value = lstrCodRequerimiento
            arParms(5).Value = lstrCodTipoEnvio
            arParms(6).Value = lstrEstadoUrgente
            arParms(7).Value = lstrEstadoRequimiento
            arParms(8).Value = lintNumTotalConsulta

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAU_REQU_TEMP", arParms)
            lstrErrorSql = arParms(9).Value().ToString()
            lintFilasAfectadas = CInt(arParms(10).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Function gbolRegistrarRequerimiento() As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        Dim bolRetorno As Boolean = False
        Try
            arParms(0) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(4) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(5) = New SqlParameter("@K_CO_PERS_AUTO", SqlDbType.VarChar, 6)
            arParms(6) = New SqlParameter("@K_CO_USUA", SqlDbType.VarChar, 8)
            arParms(7) = New SqlParameter("@K_NU_REQU", SqlDbType.VarChar, 200)
            arParms(8) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(9) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(10) = New SqlParameter("@K_NU_TIEMPO_ATENCION", SqlDbType.VarChar, 1000)
            arParms(11) = New SqlParameter("@K_NU_TOT_CONSULTA", SqlDbType.VarChar, 1000)
            arParms(12) = New SqlParameter("@K_COD_USUARIO_NOMBRES", SqlDbType.VarChar, 1000)


            arParms(0).Direction = ParameterDirection.ReturnValue
            arParms(7).Direction = ParameterDirection.Output
            arParms(8).Direction = ParameterDirection.Output
            arParms(9).Direction = ParameterDirection.Output
            arParms(10).Direction = ParameterDirection.Output
            arParms(11).Direction = ParameterDirection.Output
            arParms(12).Direction = ParameterDirection.Output

            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodCliente
            arParms(4).Value = lintNumRequTemp
            arParms(5).Value = lstrCodPersAutorizada
            arParms(6).Value = lstrCodUsuario

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAI_REGI_REQU_VER2", arParms)

            lintValorRetorno = CInt(arParms(0).Value().ToString())
            lstrCodRequerimiento = arParms(7).Value().ToString()
            lstrErrorSql = arParms(8).Value().ToString()
            lintFilasAfectadas = CInt(arParms(9).Value().ToString())
            lstrTiempoAtencion = arParms(10).Value().ToString()
            lstrTotConReq = arParms(11).Value().ToString()
            lstrNombre = arParms(12).Value().ToString()

            Select Case lintValorRetorno
                Case 0
                    lstrMensajeError = "Procedimiento Exitoso"
                    bolRetorno = True
                Case 1
                    lstrMensajeError = "ERROR AL RECUPERAR LOS DATOS"
                    bolRetorno = False
                Case 2
                    lstrMensajeError = "ERROR AL INSERTAR EN TCREQU_DEFI"
                    bolRetorno = False
                Case 3
                    lstrMensajeError = "ERROR AL INSERTAR EN TDCONS_CLIE"
                    bolRetorno = False
                Case 4
                    lstrMensajeError = "ERROR AL INSERTAR EN TRUNID_CLIE"
                    bolRetorno = False
                Case 5
                    lstrMensajeError = "ERROR AL INSERTAR EN TRDIVI_CONS"
                    bolRetorno = False
            End Select
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function gbolEliminarRequerimientoTemp(ByVal ObjTrans As SqlTransaction) As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(5) {}

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_NU_REQU_TEMP", SqlDbType.Int)
            arParms(4) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(5) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(4).Direction = ParameterDirection.Output
            arParms(5).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lintNumRequTemp

            clsSQLHelper.ExecuteNonQuery(ObjTrans, CommandType.StoredProcedure, "SGAD_REQU_TEMP", arParms)
            lstrErrorSql = arParms(4).Value().ToString()
            lintFilasAfectadas = CInt(arParms(5).Value().ToString())
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function gdtPersonaAuto() As Boolean
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        Try
            arParms(0) = New SqlParameter("@K_NU_REQ", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(2) = New SqlParameter("@K_NOM_PERSONA", SqlDbType.VarChar, 50)
            arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(4) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(2).Direction = ParameterDirection.Output
            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.Output
            arParms(0).Value = strCodRequerimiento
            arParms(1).Value = strCodCliente

            clsSQLHelper.ExecuteNonQuery(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_DETALLE_DOCU", arParms)

            lstrErrorSql = arParms(3).Value().ToString()
            lintFilasAfectadas = CInt(arParms(4).Value().ToString())
            lstrNombre = arParms(2).Value().ToString()
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property
    Public Property strCodEmpresa() As String
        Get
            Return lstrCodEmpresa
        End Get
        Set(ByVal Value As String)
            lstrCodEmpresa = Value
        End Set
    End Property
    Public Property strCodUnidad() As String
        Get
            Return lstrCodUnidad
        End Get
        Set(ByVal Value As String)
            lstrCodUnidad = Value
        End Set
    End Property
    Public Property strCodClieAdic() As String
        Get
            Return lstrCodClieAdic
        End Get
        Set(ByVal Value As String)
            lstrCodClieAdic = Value
        End Set
    End Property
    Public Property strCodArea() As String
        Get
            Return lstrCodArea
        End Get
        Set(ByVal Value As String)
            lstrCodArea = Value
        End Set
    End Property
    Public Property strCodRequerimiento() As String
        Get
            Return lstrCodRequerimiento
        End Get
        Set(ByVal Value As String)
            lstrCodRequerimiento = Value
        End Set
    End Property
    Public Property strCodEstado() As String
        Get
            Return lstrCodEstado
        End Get
        Set(ByVal Value As String)
            lstrCodEstado = Value
        End Set
    End Property
    Public Property strCodSituacion() As String
        Get
            Return lstrCodSituacion
        End Get
        Set(ByVal Value As String)
            lstrCodSituacion = Value
        End Set
    End Property
    Public Property strCodPersAutorizada() As String
        Get
            Return lstrCodPersAutorizada
        End Get
        Set(ByVal Value As String)
            lstrCodPersAutorizada = Value
        End Set
    End Property
    Public Property strCodFechaDesde() As String
        Get
            Return lstrCodFechaDesde
        End Get
        Set(ByVal Value As String)
            lstrCodFechaDesde = Value
        End Set
    End Property
    Public Property strCodFechaHasta() As String
        Get
            Return lstrCodFechaHasta
        End Get
        Set(ByVal Value As String)
            lstrCodFechaHasta = Value
        End Set
    End Property
    Public Property intNumRequTemp() As Integer
        Get
            Return lintNumRequTemp
        End Get
        Set(ByVal Value As Integer)
            lintNumRequTemp = Value
        End Set
    End Property
    Public Property strTotConsulReq() As String
        Get
            Return lstrTotConReq
        End Get
        Set(ByVal Value As String)
            lstrTotConReq = Value
        End Set
    End Property
    Public Property strCodTipoMedioRecepcion() As String
        Get
            Return lstrCodTipoMedioRecepcion
        End Get
        Set(ByVal Value As String)
            lstrCodTipoMedioRecepcion = Value
        End Set
    End Property
    Public Property strCodTipoConsulta() As String
        Get
            Return lstrCodTipoConsulta
        End Get
        Set(ByVal Value As String)
            lstrCodTipoConsulta = Value
        End Set
    End Property
    Public Property strCodTipoItem() As String
        Get
            Return lstrCodTipoItem
        End Get
        Set(ByVal Value As String)
            lstrCodTipoItem = Value
        End Set
    End Property
    Public Property strCodTipoEnvio() As String
        Get
            Return lstrCodTipoEnvio
        End Get
        Set(ByVal Value As String)
            lstrCodTipoEnvio = Value
        End Set
    End Property
    Public Property strDesPersSolicitante() As String
        Get
            Return lstrDesPersSolicitante
        End Get
        Set(ByVal Value As String)
            lstrDesPersSolicitante = Value
        End Set
    End Property
    Public Property strDesDireccionEnvio() As String
        Get
            Return lstrDesDireccionEnvio
        End Get
        Set(ByVal Value As String)
            lstrDesDireccionEnvio = Value
        End Set
    End Property
    Public Property strEstConsultaUrgente() As String
        Get
            Return lstrEstConsultaUrgente
        End Get
        Set(ByVal Value As String)
            lstrEstConsultaUrgente = Value
        End Set
    End Property
    Public Property strDesObservacion() As String
        Get
            Return lstrDesObservacion
        End Get
        Set(ByVal Value As String)
            lstrDesObservacion = Value
        End Set
    End Property
    Public Property intNumTotalConsulta() As Integer
        Get
            Return lintNumTotalConsulta
        End Get
        Set(ByVal Value As Integer)
            lintNumTotalConsulta = Value
        End Set
    End Property
    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strUsuarioNombres() As String
        Get
            Return lstrNombre
        End Get
        Set(ByVal Value As String)
            lstrNombre = Value
        End Set
    End Property

    Public Property strEstadoUrgente() As String
        Get
            Return lstrEstadoUrgente
        End Get
        Set(ByVal Value As String)
            lstrEstadoUrgente = Value
        End Set
    End Property
    Public Property strEstadoRequimiento() As String
        Get
            Return lstrEstadoRequimiento
        End Get
        Set(ByVal Value As String)
            lstrEstadoRequimiento = Value
        End Set
    End Property
    Public Property strCodTipoUnidad() As String
        Get
            Return lstrCodTipoUnidad
        End Get
        Set(ByVal Value As String)
            lstrCodTipoUnidad = Value
        End Set
    End Property

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strCadenaConexion() As String
        Get
            Return lstrCadenaInicial
        End Get
        Set(ByVal Value As String)
            lstrCadenaInicial = Value
        End Set
    End Property
    Public Property intValorRetorno() As Integer
        Get
            Return lintValorRetorno
        End Get
        Set(ByVal Value As Integer)
            lintValorRetorno = Value
        End Set
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
    Public Property strUsuarioLogin() As String
        Get
            Return lstrUsuarioLogin
        End Get
        Set(ByVal Value As String)
            lstrUsuarioLogin = Value
        End Set
    End Property
    Public Property strFilasTiempo() As String
        Get
            Return lstrTiempoAtencion
        End Get
        Set(ByVal Value As String)
            lstrTiempoAtencion = Value
        End Set
    End Property

    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
        lintValorRetorno = -1
    End Sub
End Class
