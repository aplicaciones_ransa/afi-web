Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADReporte
    Private lstrCodCliente As String
    Private lstrCodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrCodClieAdic As String
    Private lstrCodArea As String
    Private lstrCodTipoItem As String
    Private lstrCodTipo As String
    Private lstrCodFechaDesde As String
    Private lstrCodFechaHasta As String
    Private lstrNivelAutoridad As String
    Private lstrUsuarioLogin As String 'Usuario que ingresa al sistema

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer

    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")

    'Public Function gdtRepUnidadesExistenesxArea() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(7) {}
    '    Dim dtTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_FECHA", SqlDbType.VarChar, 10)
    '        arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(6).Direction = ParameterDirection.Output
    '        arParms(7).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrCodFechaDesde

    '        dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_EXIS_AREA", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(7).Value)
    '        lstrErrorSql = arParms(6).Value

    '        Return dtTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtRepUnidadesExistenesxArea() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_FECHA", SqlDbType.VarChar, 10)
            arParms(6) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(7) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(8) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)

            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodFechaDesde
            arParms(8).Value = lstrUsuarioLogin

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_EXIS_AREA", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(7).Value)
            lstrErrorSql = arParms(6).Value

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function gdtRepVencimientoCaja() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(10) {}
    '    Dim dtTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
    '        arParms(6) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 1)
    '        arParms(7) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
    '        arParms(8) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
    '        arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(10) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(9).Direction = ParameterDirection.Output
    '        arParms(10).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrCodTipoItem
    '        arParms(6).Value = lstrCodTipo
    '        arParms(7).Value = lstrCodFechaDesde
    '        arParms(8).Value = lstrCodFechaHasta

    '        dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_VENC_CAJA", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(10).Value)
    '        lstrErrorSql = arParms(9).Value

    '        Return dtTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function
    Public Function gdtRepVencimientoCaja() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(11) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 1)
            arParms(7) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(8) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(11) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)

            arParms(9).Direction = ParameterDirection.Output
            arParms(10).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodTipoItem
            arParms(6).Value = lstrCodTipo
            arParms(7).Value = lstrCodFechaDesde
            arParms(8).Value = lstrCodFechaHasta
            arParms(11).Value = lstrUsuarioLogin

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_VENC_CAJA", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(10).Value)
            lstrErrorSql = arParms(9).Value

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtRepPersonasAutorizadas() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(4) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(2) = New SqlParameter("@K_NI_AUTO", SqlDbType.VarChar, 1)
            arParms(3) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(4) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(3).Direction = ParameterDirection.Output
            arParms(4).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodArea
            arParms(2).Value = lstrNivelAutoridad

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_PERS_AUTO", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(4).Value)
            lstrErrorSql = arParms(3).Value

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    'Public Function gdtRepUnidadesVaciasPoderCliente() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(8) {}
    '    Dim dtTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
    '        arParms(6) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
    '        arParms(7) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(8) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(7).Direction = ParameterDirection.Output
    '        arParms(8).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrCodFechaDesde
    '        arParms(6).Value = lstrCodFechaHasta

    '        dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_VACI_PODE_CLIE", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(8).Value)
    '        lstrErrorSql = arParms(7).Value

    '        Return dtTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtRepUnidadesVaciasPoderCliente() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(9) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(6) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(7) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(8) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(9) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)

            arParms(7).Direction = ParameterDirection.Output
            arParms(8).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodFechaDesde
            arParms(6).Value = lstrCodFechaHasta
            arParms(9).Value = lstrUsuarioLogin

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_VACI_PODE_CLIE", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(8).Value)
            lstrErrorSql = arParms(7).Value

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function gdtRepStockxArea() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        Dim dtTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(7) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(8) = New SqlParameter("@K_TI_CONS", SqlDbType.VarChar, 1)
            arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(9).Direction = ParameterDirection.Output
            arParms(10).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodTipoItem
            arParms(6).Value = lstrCodFechaDesde
            arParms(7).Value = lstrCodFechaHasta
            arParms(8).Value = lstrCodTipo

            dtTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_STCK_AREA", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(10).Value)
            lstrErrorSql = arParms(9).Value

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function



    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property
    Public Property strCodEmpresa() As String
        Get
            Return lstrCodEmpresa
        End Get
        Set(ByVal Value As String)
            lstrCodEmpresa = Value
        End Set
    End Property
    Public Property strCodUnidad() As String
        Get
            Return lstrCodUnidad
        End Get
        Set(ByVal Value As String)
            lstrCodUnidad = Value
        End Set
    End Property
    Public Property strCodClieAdic() As String
        Get
            Return lstrCodClieAdic
        End Get
        Set(ByVal Value As String)
            lstrCodClieAdic = Value
        End Set
    End Property
    Public Property strCodArea() As String
        Get
            Return lstrCodArea
        End Get
        Set(ByVal Value As String)
            lstrCodArea = Value
        End Set
    End Property
    Public Property strCodTipoItem() As String
        Get
            Return lstrCodTipoItem
        End Get
        Set(ByVal Value As String)
            lstrCodTipoItem = Value
        End Set
    End Property
    Public Property strCodTipo() As String
        Get
            Return lstrCodTipo
        End Get
        Set(ByVal Value As String)
            lstrCodTipo = Value
        End Set
    End Property
    Public Property strCodFechaDesde() As String
        Get
            Return lstrCodFechaDesde
        End Get
        Set(ByVal Value As String)
            lstrCodFechaDesde = Value
        End Set
    End Property
    Public Property strCodFechaHasta() As String
        Get
            Return lstrCodFechaHasta
        End Get
        Set(ByVal Value As String)
            lstrCodFechaHasta = Value
        End Set
    End Property
    Public Property strNivelAutoridad() As String
        Get
            Return lstrNivelAutoridad
        End Get
        Set(ByVal Value As String)
            lstrNivelAutoridad = Value
        End Set
    End Property

    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strUsuarioLogin() As String
        Get
            Return lstrUsuarioLogin
        End Get
        Set(ByVal Value As String)
            lstrUsuarioLogin = Value
        End Set
    End Property
    Public Property strCadenaConexion() As String
        Get
            Return lstrCadenaInicial
        End Get
        Set(ByVal Value As String)
            lstrCadenaInicial = Value
        End Set
    End Property


End Class
