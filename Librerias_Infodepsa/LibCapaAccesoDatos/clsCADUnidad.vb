Imports System.Data
Imports System.Data.SqlClient

Public Class clsCADUnidad
    Private lstrCodCliente As String
    Private lstrCodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrCodClieAdic As String
    Private lstrIDUnidad As String
    Private lstrCodArea As String
    Private lstrCodCriterio As String
    Private lstrNroDocumento As String
    Private lstrCodFechaDesde As String
    Private lstrCodFechaHasta As String
    Private lstrDesDocumento As String
    Private lstrCodTipo As String
    Private lstrCodTipoItem As String
    Private lintIDConsulta As Double
    Private lintIDConsultaInicio As Double
    Private lintIDConsultaFinal As Double
    Private lstrDireccionPagina As String
    Private lstrEstTotalRegistros As String
    Private lstrUsuarioLogin As String 'Usuario que ingresa al sistema

    Private lstrUnidades As String
    Private lstrQuery As String
    Private lstrPagina As String
    Private lIntInicio As Integer
    Private lintFilasAvance As Integer
    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private lstrCadenaInicial As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfiopdf")

    Public Function gdtConDocumentos() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(12) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_CRIT", SqlDbType.VarChar, 5)
            arParms(6) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
            arParms(7) = New SqlParameter("@K_NRO_DOCU", SqlDbType.VarChar, 20)
            arParms(8) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(9) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(10) = New SqlParameter("@K_DESC_DOCU", SqlDbType.VarChar, 50)
            arParms(11) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(12) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(11).Direction = ParameterDirection.Output
            arParms(12).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodCriterio
            arParms(6).Value = lstrIDUnidad
            arParms(7).Value = lstrNroDocumento
            arParms(8).Value = lstrCodFechaDesde
            arParms(9).Value = lstrCodFechaHasta
            arParms(10).Value = lstrDesDocumento

            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_X_DOCU", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(12).Value)
            lstrErrorSql = arParms(11).Value
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    'Public Function gdtConDocumentosHP() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(17) {}
    '    Dim dsTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_CO_CRIT", SqlDbType.VarChar, 5)
    '        arParms(6) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
    '        arParms(7) = New SqlParameter("@K_NRO_DOCU", SqlDbType.VarChar, 20)
    '        arParms(8) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
    '        arParms(9) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
    '        arParms(10) = New SqlParameter("@K_DESC_DOCU", SqlDbType.VarChar, 50)
    '        arParms(11) = New SqlParameter("@K_ID_CONS", SqlDbType.VarChar, 20)
    '        arParms(12) = New SqlParameter("@K_DI_PAGI", SqlDbType.VarChar, 1)
    '        arParms(13) = New SqlParameter("@K_ST_TOTA_REGI", SqlDbType.VarChar, 1)
    '        arParms(14) = New SqlParameter("@K_ID_CONS_INIC", SqlDbType.Decimal)
    '        arParms(15) = New SqlParameter("@K_ID_CONS_FINA", SqlDbType.Decimal)
    '        arParms(16) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(17) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(14).Direction = ParameterDirection.Output
    '        arParms(15).Direction = ParameterDirection.Output
    '        arParms(16).Direction = ParameterDirection.Output
    '        arParms(17).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodEmpresa
    '        arParms(1).Value = lstrCodUnidad
    '        arParms(2).Value = lstrCodCliente
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrCodCriterio
    '        arParms(6).Value = lstrIDUnidad
    '        arParms(7).Value = lstrNroDocumento
    '        arParms(8).Value = lstrCodFechaDesde
    '        arParms(9).Value = lstrCodFechaHasta
    '        arParms(10).Value = lstrDesDocumento
    '        arParms(11).Value = lintIDConsulta.ToString
    '        arParms(12).Value = lstrDireccionPagina
    '        arParms(13).Value = lstrEstTotalRegistros

    '        dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_X_DOCU_HP", arParms).Tables(0)
    '        'dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_X_DOCU", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(17).Value)
    '        lstrErrorSql = arParms(16).Value
    '        lintIDConsultaInicio = CDbl(arParms(14).Value)
    '        lintIDConsultaFinal = CDbl(arParms(15).Value)
    '        Return dsTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function
    Public Function gdtConDocumentosHP() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(18) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_CRIT", SqlDbType.VarChar, 5)
            arParms(6) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
            arParms(7) = New SqlParameter("@K_NRO_DOCU", SqlDbType.VarChar, 20)
            arParms(8) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(9) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(10) = New SqlParameter("@K_DESC_DOCU", SqlDbType.VarChar, 50)
            arParms(11) = New SqlParameter("@K_ID_CONS", SqlDbType.VarChar, 20)
            arParms(12) = New SqlParameter("@K_DI_PAGI", SqlDbType.VarChar, 1)
            arParms(13) = New SqlParameter("@K_ST_TOTA_REGI", SqlDbType.VarChar, 1)
            arParms(14) = New SqlParameter("@K_ID_CONS_INIC", SqlDbType.Decimal)
            arParms(15) = New SqlParameter("@K_ID_CONS_FINA", SqlDbType.Decimal)
            arParms(16) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(17) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(18) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)

            arParms(14).Direction = ParameterDirection.Output
            arParms(15).Direction = ParameterDirection.Output
            arParms(16).Direction = ParameterDirection.Output
            arParms(17).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodCriterio
            arParms(6).Value = lstrIDUnidad
            arParms(7).Value = lstrNroDocumento
            arParms(8).Value = lstrCodFechaDesde
            arParms(9).Value = lstrCodFechaHasta
            arParms(10).Value = lstrDesDocumento
            arParms(11).Value = lintIDConsulta.ToString
            arParms(12).Value = lstrDireccionPagina
            arParms(13).Value = lstrEstTotalRegistros
            arParms(18).Value = lstrUsuarioLogin

         
            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_X_DOCU_HP", arParms).Tables(0)
            'dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_X_DOCU", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(17).Value)
            lstrErrorSql = arParms(16).Value
            lintIDConsultaInicio = CDbl(arParms(14).Value)
            lintIDConsultaFinal = CDbl(arParms(15).Value)
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function gdtConDocumentoxUnidad() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(6) {}
    '    Dim dsTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(5).Direction = ParameterDirection.Output
    '        arParms(6).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodCliente
    '        arParms(1).Value = lstrCodEmpresa
    '        arParms(2).Value = lstrCodUnidad
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrIDUnidad

    '        dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_DOCU", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(6).Value)
    '        lstrErrorSql = arParms(5).Value
    '        Return dsTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function
    Public Function gdtConDocumentoxUnidad() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(8) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(7) = New SqlParameter("@K_UNIDADES", SqlDbType.VarChar, 100)
            arParms(8) = New SqlParameter("@QUERY", SqlDbType.VarChar, 1000)

            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output
            arParms(8).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrIDUnidad
            arParms(7).Value = lstrUnidades

            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_DOCU", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(6).Value)
            lstrErrorSql = arParms(5).Value
            lstrQuery = arParms(8).Value
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConCabeceraUnidad() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(6) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrIDUnidad

            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CABE_UNID", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(6).Value)
            lstrErrorSql = arParms(5).Value
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConUnidadxArea() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(10) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FECHA", SqlDbType.VarChar, 10)
            arParms(7) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 1)
            arParms(8) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 100)
            arParms(9) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(9).Direction = ParameterDirection.Output
            arParms(10).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrCodTipoItem
            arParms(6).Value = lstrCodFechaDesde
            arParms(7).Value = lstrCodTipo
            arParms(8).Value = lstrIDUnidad

            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_UNID", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(10).Value)
            lstrErrorSql = arParms(9).Value
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    Public Function gdtConDocumentoxArea() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(7) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(1) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(2) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)

            arParms(5) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(6) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)
            arParms(7) = New SqlParameter("@QUERY", SqlDbType.VarChar, 1000)


            arParms(5).Direction = ParameterDirection.Output
            arParms(6).Direction = ParameterDirection.Output
            arParms(7).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodCliente
            arParms(1).Value = lstrCodEmpresa
            arParms(2).Value = lstrCodUnidad
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea


            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_CONS_DOCUXAREA", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(6).Value)
            lstrErrorSql = arParms(5).Value
            lstrQuery = arParms(7).Value
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    'borra jauris
    'Public Function gdtConUnidades() As DataTable
    '    Dim arParms() As SqlParameter = New SqlParameter(11) {}
    '    Dim dsTemporal As DataTable

    '    Try
    '        arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
    '        arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
    '        arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
    '        arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
    '        arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
    '        arParms(5) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 80)
    '        arParms(6) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
    '        arParms(7) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
    '        arParms(8) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 40)
    '        arParms(9) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
    '        arParms(10) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
    '        arParms(11) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

    '        arParms(10).Direction = ParameterDirection.Output
    '        arParms(11).Direction = ParameterDirection.Output

    '        arParms(0).Value = lstrCodEmpresa
    '        arParms(1).Value = lstrCodUnidad
    '        arParms(2).Value = lstrCodCliente
    '        arParms(3).Value = lstrCodClieAdic
    '        arParms(4).Value = lstrCodArea
    '        arParms(5).Value = lstrIDUnidad
    '        arParms(6).Value = lstrCodFechaDesde
    '        arParms(7).Value = lstrCodFechaHasta
    '        arParms(8).Value = lstrCodTipo
    '        arParms(9).Value = lstrCodTipoItem

    '        dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID", arParms).Tables(0)
    '        lintFilasAfectadas = CInt(arParms(11).Value)
    '        lstrErrorSql = arParms(10).Value.ToString
    '        Return dsTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function


    Public Function gdtConUnidades() As DataTable
        Dim arParms() As SqlParameter = New SqlParameter(16) {}
        Dim dsTemporal As DataTable

        Try
            arParms(0) = New SqlParameter("@K_CO_EMPR", SqlDbType.VarChar, 2)
            arParms(1) = New SqlParameter("@K_CO_UNID", SqlDbType.VarChar, 3)
            arParms(2) = New SqlParameter("@K_CO_CLIE", SqlDbType.VarChar, 20)
            arParms(3) = New SqlParameter("@K_CO_CLIE_ADIC", SqlDbType.VarChar, 6)
            arParms(4) = New SqlParameter("@K_CO_AREA", SqlDbType.VarChar, 20)
            arParms(5) = New SqlParameter("@K_ID_UNID", SqlDbType.VarChar, 100)
            arParms(6) = New SqlParameter("@K_FECHA_DE", SqlDbType.VarChar, 10)
            arParms(7) = New SqlParameter("@K_FECHA_HA", SqlDbType.VarChar, 10)
            arParms(8) = New SqlParameter("@K_CO_TIPO", SqlDbType.VarChar, 40)
            arParms(9) = New SqlParameter("@K_CO_TIPO_ITEM", SqlDbType.VarChar, 3)
            arParms(10) = New SqlParameter("@K_CO_USUA_LOGI", SqlDbType.VarChar, 20)
            arParms(11) = New SqlParameter("@K_CO_NUM", SqlDbType.Int)
            arParms(12) = New SqlParameter("@K_CO_FLAG_INICIO", SqlDbType.Int)
            arParms(13) = New SqlParameter("@K_CO_PAG_INI", SqlDbType.Int)
            arParms(14) = New SqlParameter("@K_CO_PAG_FIN", SqlDbType.Int)
            arParms(15) = New SqlParameter("@K_ERROR_SQL", SqlDbType.VarChar, 3)
            arParms(16) = New SqlParameter("@K_FILAS_AFECTADAS", SqlDbType.Int)

            arParms(13).Direction = ParameterDirection.Output
            arParms(14).Direction = ParameterDirection.Output
            arParms(15).Direction = ParameterDirection.Output
            arParms(16).Direction = ParameterDirection.Output

            arParms(0).Value = lstrCodEmpresa
            arParms(1).Value = lstrCodUnidad
            arParms(2).Value = lstrCodCliente
            arParms(3).Value = lstrCodClieAdic
            arParms(4).Value = lstrCodArea
            arParms(5).Value = lstrIDUnidad
            arParms(6).Value = lstrCodFechaDesde
            arParms(7).Value = lstrCodFechaHasta
            arParms(8).Value = lstrCodTipo
            arParms(9).Value = lstrCodTipoItem
            arParms(10).Value = lstrUsuarioLogin
            arParms(11).Value = lstrPagina
            arParms(12).Value = lIntInicio

            'dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID", arParms).Tables(0)
            dsTemporal = clsSQLHelper.ExecuteDataset(lstrCadenaInicial, CommandType.StoredProcedure, "SGAS_UNID_x_CAJA_HP", arParms).Tables(0)
            lintFilasAfectadas = CInt(arParms(16).Value)
            lstrErrorSql = arParms(15).Value.ToString
            lintFilasAvance = CInt(arParms(14).Value)
            Return dsTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property
    Public Property strCodEmpresa() As String
        Get
            Return lstrCodEmpresa
        End Get
        Set(ByVal Value As String)
            lstrCodEmpresa = Value
        End Set
    End Property
    Public Property strCodUnidad() As String
        Get
            Return lstrCodUnidad
        End Get
        Set(ByVal Value As String)
            lstrCodUnidad = Value
        End Set
    End Property
    Public Property strIDUnidad() As String
        Get
            Return lstrIDUnidad
        End Get
        Set(ByVal Value As String)
            lstrIDUnidad = Value
        End Set
    End Property
    Public Property strCodClieAdic() As String
        Get
            Return lstrCodClieAdic
        End Get
        Set(ByVal Value As String)
            lstrCodClieAdic = Value
        End Set
    End Property
    Public Property strCodArea() As String
        Get
            Return lstrCodArea
        End Get
        Set(ByVal Value As String)
            lstrCodArea = Value
        End Set
    End Property
    Public Property strCodCriterio() As String
        Get
            Return lstrCodCriterio
        End Get
        Set(ByVal Value As String)
            lstrCodCriterio = Value
        End Set
    End Property
    Public Property strNroDocumento() As String
        Get
            Return lstrNroDocumento
        End Get
        Set(ByVal Value As String)
            lstrNroDocumento = Value
        End Set
    End Property
    Public Property strCodFechaDesde() As String
        Get
            Return lstrCodFechaDesde
        End Get
        Set(ByVal Value As String)
            lstrCodFechaDesde = Value
        End Set
    End Property
    Public Property strCodFechaHasta() As String
        Get
            Return lstrCodFechaHasta
        End Get
        Set(ByVal Value As String)
            lstrCodFechaHasta = Value
        End Set
    End Property
    Public Property strDesDocumento() As String
        Get
            Return lstrDesDocumento
        End Get
        Set(ByVal Value As String)
            lstrDesDocumento = Value
        End Set
    End Property
    Public Property strCodTipo() As String
        Get
            Return lstrCodTipo
        End Get
        Set(ByVal Value As String)
            lstrCodTipo = Value
        End Set
    End Property
    Public Property strUnidades() As String
        Get
            Return lstrUnidades
        End Get
        Set(ByVal Value As String)
            lstrUnidades = Value
        End Set
    End Property

    Public Property strCodTipoItem() As String
        Get
            Return lstrCodTipoItem
        End Get
        Set(ByVal Value As String)
            lstrCodTipoItem = Value
        End Set
    End Property
    Public Property intIDConsulta() As Double
        Get
            Return lintIDConsulta
        End Get
        Set(ByVal Value As Double)
            lintIDConsulta = Value
        End Set
    End Property
    Public Property intIDConsultaInicio() As Double
        Get
            Return lintIDConsultaInicio
        End Get
        Set(ByVal Value As Double)
            lintIDConsultaInicio = Value
        End Set
    End Property
    Public Property intIDConsultaFinal() As Double
        Get
            Return lintIDConsultaFinal
        End Get
        Set(ByVal Value As Double)
            lintIDConsultaFinal = Value
        End Set
    End Property
    Public Property strDireccionPagina() As String
        Get
            Return lstrDireccionPagina
        End Get
        Set(ByVal Value As String)
            lstrDireccionPagina = Value
        End Set
    End Property

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strEstTotalRegistros() As String
        Get
            Return lstrEstTotalRegistros
        End Get
        Set(ByVal Value As String)
            lstrEstTotalRegistros = Value
        End Set
    End Property
    Public Property strUsuarioLogin() As String
        Get
            Return lstrUsuarioLogin
        End Get
        Set(ByVal Value As String)
            lstrUsuarioLogin = Value
        End Set
    End Property
    Public Property strPagina() As String
        Get
            Return lstrPagina
        End Get
        Set(ByVal Value As String)
            lstrPagina = Value
        End Set
    End Property

    Public Property intInicio() As String
        Get
            Return lIntInicio
        End Get
        Set(ByVal Value As String)
            lIntInicio = Value
        End Set
    End Property
    Public Property intFilasAvance() As Integer
        Get
            Return lintFilasAvance
        End Get
        Set(ByVal Value As Integer)
            lintFilasAvance = Value
        End Set
    End Property
    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub
End Class
