Imports System.Data
Imports System.Data.SqlClient


Public Class clsCNRequerimiento
    Private lstrCodRequerimiento As String
    Private lintNumRequTemp As Integer
    Private lintFilasAfectadas As Integer
    Private lintNumConsultas As Integer
    Private lstrcodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrTiempoMax As String
    Private lstrTotConXReq As String
    Private lstrNomUsuario As String
    Private lstrTotConReq As String
    Private lstrNombre As String
    'Private lstrUsuarioLogin As String 'Usuario que ingresa al sistema 


    Private lstrErrorSql As String
    Private lstrMensajeError As String
    Private objRequ As LibCapaAccesoDatos.clsCADRequerimiento
    Private objTransaccion As SqlTransaction
    Private objConexion As SqlConnection
    Private objFuncion As LibCapaNegocio.clsFunciones

    'Public Function gdtMostrarEstado(ByVal pstrCodCliente As String, _
    ' ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
    ' ByVal pstrCodRequerimiento As String, ByVal pstrCodEstado As String, _
    ' ByVal pstrCodPersAuto As String, _
    ' ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String) As DataTable
    '    Dim dtTemp As DataTable
    '    objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '    Try
    '        With objRequ
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodClieAdic = pstrCodClieAdic
    '            .strCodArea = pstrCodArea
    '            .strCodRequerimiento = pstrCodRequerimiento
    '            .strCodEstado = "APR"
    '            .strCodSituacion = pstrCodEstado
    '            .strCodPersAutorizada = pstrCodPersAuto
    '            .strCodFechaDesde = pstrFechaDesde
    '            .strCodFechaHasta = pstrFechaHasta
    '            dtTemp = .gdtConEstadoRequerimiento
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '        End With
    '        Return dtTemp

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)

    '    End Try
    'End Function

    Public Function gdtMostrarEstado(ByVal pstrCodCliente As String, _
         ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
         ByVal pstrCodRequerimiento As String, ByVal pstrCodEstado As String, _
         ByVal pstrCodPersAuto As String, ByVal pstrFechaDesde As String, _
         ByVal pstrFechaHasta As String, ByVal pstrUsuarioLogin As String) As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodRequerimiento = pstrCodRequerimiento
                .strCodEstado = "APR"
                .strCodSituacion = pstrCodEstado
                .strCodPersAutorizada = pstrCodPersAuto
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strUsuarioLogin = pstrUsuarioLogin
                dtTemp = .gdtConEstadoRequerimiento
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With
            Return dtTemp

        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
    End Function

    Public Function gdtConseguirCabecera(ByVal pstrCodCliente As String, _
    ByVal pstrCodRequerimiento As String, Optional ByVal pstrCodClieAdic As String = "") As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodRequerimiento = pstrCodRequerimiento
                dtTemp = .gdtConCabeceraRequerimiento
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With

            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function gdtConseguirDetalle(ByVal pstrCodCliente As String, _
    'ByVal pstrCodRequerimiento As String) As DataTable
    '    Dim dtTemp As DataTable
    '    objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '    Try
    '        With objRequ
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodRequerimiento = pstrCodRequerimiento
    '            dtTemp = .gdtConDetalleRequerimiento
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '        End With
    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtConseguirDetalle(ByVal pstrCodCliente As String, _
    ByVal pstrCodRequerimiento As String) As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodRequerimiento = pstrCodRequerimiento
                dtTemp = .gdtConDetalleRequerimiento
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With
            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function
    'Public Function gdtConseguirCuotasXCobrar_Interbank_Cabecera() As DataTable
    '    Dim dtTemp As DataTable
    '    objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '    Try
    '        With objRequ
    '            '.strCodCliente = pstrCodCliente
    '            '.strCodEmpresa = lstrcodEmpresa
    '            '.strCodUnidad = lstrCodUnidad
    '            '.strCodClieAdic = pstrCodClieAdic
    '            '.strCodRequerimiento = pstrCodRequerimiento
    '            'dtTemp = .gdtConCabeceraRequerimiento
    '            dtTemp = .gdtCuotasXCobrar_Cabecera


    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '        End With

    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    'Public Function gdtConseguirCuotasXCobrar_Interbank_Detalle() As DataTable
    '    Dim dtTemp As DataTable
    '    objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '    Try
    '        With objRequ
    '            '.strCodCliente = pstrCodCliente
    '            '.strCodEmpresa = lstrcodEmpresa
    '            '.strCodUnidad = lstrCodUnidad
    '            '.strCodClieAdic = pstrCodClieAdic
    '            '.strCodRequerimiento = pstrCodRequerimiento
    '            'dtTemp = .gdtConCabeceraRequerimiento
    '            dtTemp = .gdtCuotasXCobrar_Detalle


    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '        End With

    '        Return dtTemp
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function



    'BORRA JAURIS
    'Public Function gdrConseguirMensajeRegistro(ByVal pstrCodCliente As String, _
    'ByVal pstrCodRequerimiento As String, ByVal pstrCodTipo As String, _
    'ByVal pstrEstRequ As String) As DataRow
    '    Dim dtTemp As DataTable
    '    Try
    '        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '        With objRequ
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodRequerimiento = pstrCodRequerimiento
    '            .strCodTipoItem = pstrCodTipo
    '            .strEstadoRequimiento = pstrEstRequ
    '            dtTemp = .gdtConMensajeRegistro
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '            If .intFilasAfectadas = 1 Then
    '                Return dtTemp.Rows(0)
    '            End If
    '        End With
    '    Catch ex As Exception
    '        Me.strMensajeError = ex.Message
    '    End Try
    'End Function
    Public Function gdrConseguirMensajeRegistro(ByVal pstrCodCliente As String, _
    ByVal pstrCodRequerimiento As String, ByVal pstrCodTipo As String, _
    ByVal pstrEstRequ As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodRequerimiento = pstrCodRequerimiento
                .strCodTipoItem = pstrCodTipo
                .strEstadoRequimiento = pstrEstRequ
                dtTemp = .gdtConMensajeRegistro
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                If .intFilasAfectadas = 1 Then
                    Return dtTemp
                End If
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    Public Function gdtConseguirDetalleTemporal(ByVal pstrCodCliente As String, _
    ByVal pintNumRequTemp As Integer, ByVal pstrCodTipoUnidad As String) As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .intNumRequTemp = pintNumRequTemp
                .strCodTipoUnidad = pstrCodTipoUnidad
                dtTemp = .gdtConDetalleRequerimientoTemporal()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With

            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConseguirDetalleTemp(ByVal pstrCodCliente As String, _
       ByVal pintNumRequTemp As Integer) As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .intNumRequTemp = pintNumRequTemp
                dtTemp = .gdtConDetalleRequerimientoTemp()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With

            Return dtTemp
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gbolInsRequerimientoTemp(ByVal pstrCodCliente As String, _
     ByVal pstrCodRequerimiento As String, _
     ByVal pstrCodClieAdic As String, ByVal pstrCodTipoItem As String, _
     ByVal pstrCodPersAutorizada As String, ByVal pstrDesPersSolicitante As String, _
     ByVal pstrCodArea As String, ByVal pstrDesDireccionEnvio As String, _
     ByVal pstrEstConsultaUrgente As String, ByVal pstrDesObservacion As String, _
     ByVal pintNumTotalConsulta As Integer, ByVal pstrCodUsuario As String, _
     Optional ByVal pstrCodTipoConsulta As String = "CJA") As Boolean
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        With objRequ
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try

                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .strCodRequerimiento = pstrCodRequerimiento
                .strCodClieAdic = pstrCodClieAdic
                .strCodTipoItem = pstrCodTipoItem
                .strCodPersAutorizada = pstrCodPersAutorizada
                .strDesPersSolicitante = pstrDesPersSolicitante
                .strCodArea = pstrCodArea
                .strDesDireccionEnvio = pstrDesDireccionEnvio
                .strEstConsultaUrgente = pstrEstConsultaUrgente
                .strDesObservacion = pstrDesObservacion
                .intNumTotalConsulta = pintNumTotalConsulta
                .strCodUsuario = pstrCodUsuario
                .strCodTipoConsulta = pstrCodTipoConsulta

                If .gbolInsRequerimientoTemporal(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumRequTemp = .intNumRequTemp
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = .strMensajeError
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Me.lstrMensajeError = ex.Message
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function

    'Public Function gbolRegistrarRequerimiento(ByVal pstrCodCliente As String, _
    '    ByVal pintNumRequTemp As Integer, ByVal pstrCodPersAutorizada As String, _
    '    ByVal pstrCodUsuario As String) As Boolean
    '    objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
    '    With objRequ
    '        Try
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodCliente = pstrCodCliente
    '            .intNumRequTemp = pintNumRequTemp
    '            .strCodPersAutorizada = pstrCodPersAutorizada
    '            .strCodUsuario = pstrCodUsuario

    '            If .gbolRegistrarRequerimiento() = True Then
    '                Me.lstrCodRequerimiento = .strCodRequerimiento
    '                Me.lintFilasAfectadas = .intFilasAfectadas
    '                Me.lstrErrorSql = .strErrorSql
    '                Return True
    '            Else
    '                Me.lintFilasAfectadas = .intFilasAfectadas
    '                Me.lstrErrorSql = .strErrorSql
    '                Me.lstrMensajeError = .strMensajeError
    '                Return False
    '            End If

    '        Catch ex As Exception
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '            Throw New Exception(ex.Message)
    '            Return False
    '        End Try
    '    End With
    'End Function

    Public Function gbolRegistrarRequerimiento(ByVal pstrCodCliente As String, _
        ByVal pintNumRequTemp As Integer, ByVal pstrCodPersAutorizada As String, _
        ByVal pstrCodUsuario As String) As Boolean
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        With objRequ
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = pintNumRequTemp
                .strCodPersAutorizada = pstrCodPersAutorizada
                .strCodUsuario = pstrCodUsuario

                If .gbolRegistrarRequerimiento() = True Then
                    Me.lstrCodRequerimiento = .strCodRequerimiento
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrTiempoMax = .strFilasTiempo
                    Me.lstrTotConXReq = .strTotConsulReq
                    Me.lstrNomUsuario = .strUsuarioNombres
                    Return True
                Else
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = .strMensajeError
                    Return False
                End If

            Catch ex As Exception
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            End Try
        End With
    End Function


    Public Function gbolActualizarRequerimiento(ByVal pstrCodCliente As String, _
            ByVal pintNumRequTemp As Integer, ByVal pstrNumRequAsig As String, _
            ByVal pstrCodTipoEnvio As String, ByVal pstrEstConsUrge As String, _
            ByVal pstrEstRequ As String, ByVal pintNumTotaCons As Integer) As Boolean
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        With objRequ
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = pintNumRequTemp
                .strCodRequerimiento = pstrNumRequAsig
                .strCodTipoEnvio = pstrCodTipoEnvio
                .strEstadoUrgente = pstrEstConsUrge
                .strEstadoRequimiento = pstrEstRequ
                .intNumTotalConsulta = pintNumTotaCons

                If .gbolActualizarRequerimiento(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumRequTemp = .intNumRequTemp
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try
        End With
    End Function
    Public Property strCodRequerimiento() As String
        Get
            Return lstrCodRequerimiento
        End Get
        Set(ByVal Value As String)
            lstrCodRequerimiento = Value
        End Set
    End Property
    Public Property intNumRequTemp() As Integer
        Get
            Return lintNumRequTemp
        End Get
        Set(ByVal Value As Integer)
            lintNumRequTemp = Value
        End Set
    End Property

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property intNumConsultas() As Integer
        Get
            Return lintNumConsultas
        End Get
        Set(ByVal Value As Integer)
            lintNumConsultas = Value
        End Set
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
    Public Property strTotConsulReq() As String
        Get
            Return lstrTotConReq
        End Get
        Set(ByVal Value As String)
            lstrTotConReq = Value
        End Set
    End Property

    Public Property strNomUsuario() As String
        Get
            Return lstrNomUsuario
        End Get
        Set(ByVal Value As String)
            lstrNomUsuario = Value
        End Set
    End Property

    'Public Property strUsuarioNombres() As String
    '    Get
    '        Return lstrNombre
    '    End Get
    '    Set(ByVal Value As String)
    '        lstrNombre = Value
    '    End Set
    'End Property

    'Public Sub EnviarCorreoRegistro(ByVal pstrNumRequ As String, ByVal pstrNombreCliente As String)
    '    objFuncion = New LibCapaNegocio.clsFunciones
    '    Try
    '        objFuncion.SendMail("Depsa Files � Sistema de Gesti�n de Archivos <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", _
    '                                    System.Configuration.ConfigurationManager.AppSettings.Item("DestRegiRequ"), _
    '                                    pstrNombreCliente & " - Nuevo Requerimiento", _
    '                                    "El Cliente " & pstrNombreCliente & " ha generado un nuevo requerimiento. <br>" & _
    '                                    "Requermiento : " & pstrNumRequ & "<br>" & _
    '                                    "Fecha Generaci�n :   " & Now() & "<BR><BR> Dep�sitos S.A. - Sistema de Gesti�n de Archivos Depsa Files")
    '    Catch ex As Exception
    '        Me.strMensajeError = "ERROR " + ex.Message
    '    End Try
    'End Sub

    Public Sub EnviarCorreoRegistro(ByVal pstrNumRequ As String, ByVal pstrNombreCliente As String, ByVal strEstado As String, ByVal strTiempoMax As String, ByVal strNombres As String)
        objFuncion = New LibCapaNegocio.clsFunciones
        Try

            objFuncion.SendMail("Depsa Files � Sistema de Gesti�n de Archivos <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                                        System.Configuration.ConfigurationManager.AppSettings.Item("DestRegiRequ"),
                                        pstrNombreCliente & " - Nuevo Requerimiento",
                                        "El Cliente " & pstrNombreCliente & " ha generado  los siguientes requerimientos. <br><br>" & "ESTADO: " & strEstado & "<br>" &
                                         "USUARIO: " & strNombres & "<br><br>" & pstrNumRequ & "<br>" &
                                        "Fecha Generaci�n :   " & Now() & "<br>" & "<br><br>" & " <center>  <A href='http://www.depsa.com.pe/depsafiles'> Sistema de Gesti�n de Archivos Depsa Files </A> </center>")
        Catch ex As Exception
            Me.strMensajeError = "ERROR " + ex.Message
        End Try
    End Sub


    'Public Function gbolAprobarRequerimiento(ByVal pstrCodCliente As String, _
    '    ByVal pintNumRequTemp As Integer, ByVal pstrCodPersAuto As String, _
    '    ByVal pstrNomCliente As String, ByVal pstrCodUsuario As String) As Boolean
    '    Try
    '        If Me.gbolRegistrarRequerimiento(pstrCodCliente, pintNumRequTemp, pstrCodPersAuto, _
    '            pstrCodUsuario) = True Then
    '            Me.strMensajeError = Me.strMensajeError & " Requ: " & Me.lstrCodRequerimiento & " registrado!"
    '            Me.EnviarCorreoRegistro(Me.lstrCodRequerimiento, pstrNomCliente)
    '            Return True
    '        Else
    '            Return False
    '            Me.strMensajeError = "ERROR EN EL PROCEDIMIENTO DE REGISTRO!"
    '        End If
    '    Catch ex As Exception
    '        Me.strMensajeError = "ERROR:" & ex.Message
    '        Return False
    '    End Try
    'End Function

    'Public Function gbolAprobarRequerimiento(ByVal pstrCodCliente As String, _
    '    ByVal pintNumRequTemp As Integer, ByVal pstrCodPersAuto As String, _
    '    ByVal pstrNomCliente As String, ByVal pstrCodUsuario As String, ByVal strAreas As String, ByVal strEstado As String) As Boolean
    '    Dim strMensaje As String
    '    Dim intcontadorReq() As String
    '    Dim intcontadorArea() As String
    '    Dim intcontadorTiempo() As String
    '    Dim intcontadorTotReq() As String

    '    Dim i As Integer
    '    Dim j As Integer
    '    Try
    '        If Me.gbolRegistrarRequerimiento(pstrCodCliente, pintNumRequTemp, pstrCodPersAuto, _
    '            pstrCodUsuario) = True Then
    '            Me.strMensajeError = Me.strMensajeError & " Requ: " & Me.lstrCodRequerimiento & " registrado!"
    '            intcontadorReq = Split(Me.lstrCodRequerimiento, ",")
    '            intcontadorArea = Split(strAreas, ",--")
    '            intcontadorTiempo = Split(lstrTiempoMax, ",")
    '            intcontadorTotReq = Split(lstrTotConXReq, ",")

    '            strMensaje = "<table  BORDER =1" & "> <tr> <td WIDTH=300>" & "<b>" & "N�mero Requerimiento </b> </td>" & "<td WIDTH=600> <b> �rea </b> </td> <td WIDTH=200> <b> N� Consultas </b> </td> </tr></table>"

    '            For i = 0 To UBound(intcontadorReq, 1)
    '                strMensaje = strMensaje + "<table  BORDER =1" & "> <tr> <td WIDTH=300>" & intcontadorReq(i) & "</td>" & "<td WIDTH=600>" & intcontadorArea(i) & "</td> <td WIDTH=200>" & intcontadorTotReq(UBound(intcontadorReq, 1) - i) & "</td> </tr> </table>"
    '            Next



    '            'For i = 0 To UBound(intcontadorReq, 1)
    '            '    strMensaje = strMensaje + "<table  BORDER =1" & "> <tr> <td WIDTH=300>" & "Num.Requerimiento: " & intcontadorReq(i) & "</td>" & "<td WIDTH=600> �rea " & intcontadorArea(i) & "</td> </tr> </table>"
    '            'Next

    '            'Me.EnviarCorreoRegistro(Me.lstrCodRequerimiento, pstrNomCliente)
    '            Me.EnviarCorreoRegistro(strMensaje, pstrNomCliente, strEstado, intcontadorTiempo(i - 1), lstrNomUsuario)
    '            Return True
    '        Else
    '            Return False
    '            Me.strMensajeError = "ERROR EN EL PROCEDIMIENTO DE REGISTRO!"
    '        End If
    '    Catch ex As Exception
    '        Me.strMensajeError = "ERROR:" & ex.Message
    '        Return False
    '    End Try
    'End Function

    Public Function gbolAprobarRequerimiento(ByVal pstrCodCliente As String, _
         ByVal pintNumRequTemp As Integer, ByVal pstrCodPersAuto As String, _
         ByVal pstrNomCliente As String, ByVal pstrCodUsuario As String, ByVal strAreas As String, ByVal strEstado As String) As Boolean
        Dim strMensaje As String
        Dim intcontadorReq() As String
        Dim intcontadorArea() As String
        Dim intcontadorTiempo() As String
        Dim intcontadorTotReq() As String
        Dim intcontadorReqCorreo() As String

        Dim i As Integer
        Dim j As Integer
        Try
            If Me.gbolRegistrarRequerimiento(pstrCodCliente, pintNumRequTemp, pstrCodPersAuto, _
                pstrCodUsuario) = True Then
                Me.strMensajeError = Me.strMensajeError & " Requ: " & Me.lstrCodRequerimiento & " registrado!"
                intcontadorReq = Split(Me.lstrCodRequerimiento, ",")
                intcontadorArea = Split(strAreas, ",--")
                intcontadorTiempo = Split(lstrTiempoMax, ",")
                intcontadorTotReq = Split(lstrTotConXReq, ",")
                intcontadorReqCorreo = Split(Me.lstrCodRequerimiento, "','")

                strMensaje = "<table  BORDER =1" & "> <tr> <td WIDTH=270>" & "<b>" & "N�mero Requerimiento </b> </td>" & "<td WIDTH=610> <b> �rea </b> </td> <td WIDTH=150> <b> N� Consultas </b> </td> </tr></table>"

                For i = 0 To UBound(intcontadorReq, 1)
                    'For i = 0 To UBound(intcontadorReqCorreo, 1)
                    'strMensaje = strMensaje + "<table  BORDER =1" & "> <tr> <td WIDTH=300>" & intcontadorReq(i)
                    strMensaje = strMensaje + "<table  BORDER =1" & "> <tr> <td WIDTH=275>" & intcontadorReqCorreo(i) & "</td>" & "<td WIDTH=630>" & intcontadorArea(i) & "</td> <td WIDTH=160>" & intcontadorTotReq(UBound(intcontadorReq, 1) - i) & "</td> </tr> </table>"
                Next

                'For i = 0 To UBound(intcontadorReq, 1)
                '    strMensaje = strMensaje + "<table  BORDER =1" & "> <tr> <td WIDTH=300>" & "Num.Requerimiento: " & intcontadorReq(i) & "</td>" & "<td WIDTH=600> �rea " & intcontadorArea(i) & "</td> </tr> </table>"
                'Next

                'Me.EnviarCorreoRegistro(Me.lstrCodRequerimiento, pstrNomCliente)
                Me.EnviarCorreoRegistro(strMensaje, pstrNomCliente, strEstado, intcontadorTiempo(i - 1), lstrNomUsuario)
                Return True
            Else
                Return False
                Me.strMensajeError = "ERROR EN EL PROCEDIMIENTO DE REGISTRO!"
            End If
        Catch ex As Exception
            Me.strMensajeError = "ERROR:" & ex.Message
            Return False
        End Try
    End Function


    Public Function gdtConRequerimientosxAprobar(ByVal pstrCodCliente As String, _
       ByVal pstrCodArea As String, ByVal pstrCodTipoEnvio As String, _
       ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String, ByVal pstrCodUsuario As String) As DataTable
        Dim dtTemp As DataTable
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        Try
            With objRequ
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .strCodArea = pstrCodArea
                .strCodTipoEnvio = pstrCodTipoEnvio
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strCodUsuario = pstrCodUsuario
                dtTemp = .gdtConRequerimientoxAprobar
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
            End With
            Return dtTemp

        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
    End Function

    Public Function gbolEliminaRequerimientoTemp(ByVal pstrCodCliente As String, _
         ByVal pintNumRequTemp As Integer) As Boolean
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        With objRequ
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = pintNumRequTemp

                If .gbolEliminarRequerimientoTemp(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function
    'Public Sub pr_REPO_Interbank1()

    '    Dim sb As New System.text.StringBuilder
    '    Dim strCadena1 As String
    '    Dim strCadena2 As String
    '    Dim dt_Temporal_cabecera As New DataTable
    '    Dim dt_Temporal_detalle As New DataTable

    '    'Cabeceara
    '    Dim strTipoRegistro As String
    '    Dim strCodGrupo As String
    '    Dim strCodUnico As String
    '    Dim strCodRubro As String
    '    Dim strCodEmpresa As String
    '    Dim strCodServicio As String
    '    Dim strCodRequerimiento As String
    '    Dim strCodSolicitud As String
    '    Dim strDesSolicitud As String
    '    Dim strCanEnvio As String
    '    Dim strTipInformacion As String
    '    Dim strNumRegistro As String
    '    Dim strImpSoles As String
    '    Dim strImpDolares As String
    '    Dim stFecProceso As String
    '    Dim strLibre As String
    '    Dim strCodFijo As String

    '    'Detalle
    '    Dim strTipRegD As String
    '    Dim strCodDeu As String
    '    Dim strCodCuo As String
    '    Dim strNomDeu As String
    '    Dim strRefCur1 As String
    '    Dim strRefCur2 As String
    '    Dim strFecEmi As String
    '    Dim strFecVen As String
    '    Dim strNumDoc As String
    '    Dim strMonDeu As String
    '    Dim strImpCon1 As String
    '    Dim strImpCon2 As String
    '    Dim strImpCon3 As String
    '    Dim strImpCon4 As String
    '    Dim strImpCon5 As String
    '    Dim strImpCon6 As String
    '    Dim strImpCon7 As String
    '    Dim strLibre1D As String 'libre 1 detalle
    '    Dim strTipOpe As String
    '    Dim strLibre2D As String
    '    Dim strCodFijD As String

    '    Try
    '        'objRequ = New LibCapaNegocio.clsCNRequerimiento
    '        'dt_Temporal_cabecera = objRequ.gdtConseguirCuotasXCobrar_Interbank_Cabecera("XX", "ZZ")
    '        'dt_Temporal_detalle = objRequ.gdtConseguirCuotasXCobrar_Interbank_Detalle("XX", "ZZ")

    '        dt_Temporal_cabecera = gdtConseguirCuotasXCobrar_Interbank_Cabecera("XX", "ZZ")
    '        dt_Temporal_detalle = gdtConseguirCuotasXCobrar_Interbank_Detalle("XX", "ZZ")

    '        '*************Recupera las configuracion de las tablas Cabecera*************************************
    '        Dim Ds_Cabecera As New DataSet 'Archivo Xml
    '        Dim dt_Cabecera As New DataTable

    '        Ds_Cabecera.ReadXml(Server.MapPath("Registro_Cabecera.xml"))
    '        dt_Cabecera = Ds_Cabecera.Tables(0)

    '        For y As Integer = 0 To dt_Temporal_cabecera.Rows.Count - 1
    '            'strTipoRegistro = dt_Temporal_cabecera.Rows(y)("IntTipReg")
    '            strTipoRegistro = Mid((dt_Temporal_cabecera.Rows(y)("IntTipReg")), 1, dt_Cabecera.Rows(0)("Logitud"))
    '            strCodGrupo = Mid((dt_Temporal_cabecera.Rows(y)("IntCodGrupo")), 1, dt_Cabecera.Rows(1)("Logitud"))
    '            strCodUnico = Mid((dt_Temporal_cabecera.Rows(y)("IntCodUni")), 1, dt_Cabecera.Rows(2)("Logitud"))
    '            strCodRubro = Mid((dt_Temporal_cabecera.Rows(y)("IntCodRub")), 1, dt_Cabecera.Rows(3)("Logitud"))
    '            strCodEmpresa = Mid((dt_Temporal_cabecera.Rows(y)("IntCodEmp")), 1, dt_Cabecera.Rows(4)("Logitud"))
    '            strCodServicio = Mid((dt_Temporal_cabecera.Rows(y)("IntCodServicio")), 1, dt_Cabecera.Rows(5)("Logitud"))
    '            strCodRequerimiento = Mid((dt_Temporal_cabecera.Rows(y)("IntCodReq")), 1, dt_Cabecera.Rows(6)("Logitud"))
    '            strCodSolicitud = Mid((dt_Temporal_cabecera.Rows(y)("IntCodSol")), 1, dt_Cabecera.Rows(7)("Logitud"))
    '            strDesSolicitud = Mid((dt_Temporal_cabecera.Rows(y)("IntDesSol")), 1, dt_Cabecera.Rows(8)("Logitud"))
    '            strCanEnvio = Mid((dt_Temporal_cabecera.Rows(y)("intCanalEnv")), 1, dt_Cabecera.Rows(9)("Logitud"))
    '            strTipInformacion = Mid((dt_Temporal_cabecera.Rows(y)("StrTipoInf")), 1, dt_Cabecera.Rows(10)("Logitud"))
    '            strNumRegistro = Mid((dt_Temporal_cabecera.Rows(y)("IntNumReg")), 1, dt_Cabecera.Rows(11)("Logitud"))
    '            strImpSoles = Mid((dt_Temporal_cabecera.Rows(y)("IntImpSol")), 1, dt_Cabecera.Rows(12)("Logitud"))
    '            strImpDolares = Mid((dt_Temporal_cabecera.Rows(y)("IntImpdol")), 1, dt_Cabecera.Rows(13)("Logitud"))
    '            stFecProceso = Mid((dt_Temporal_cabecera.Rows(y)("IntFecPro")), 1, dt_Cabecera.Rows(14)("Logitud"))
    '            strLibre = Mid((dt_Temporal_cabecera.Rows(y)("strLib")), 1, dt_Cabecera.Rows(15)("Logitud"))
    '            strCodFijo = Mid((dt_Temporal_cabecera.Rows(y)("IntCodFij")), 1, dt_Cabecera.Rows(16)("Logitud"))

    '            'sb.Append(dr.ItemArray(y).ToString)
    '            strTipoRegistro = strTipoRegistro.PadLeft(dt_Cabecera.Rows(0)("Logitud"), "0")
    '            sb.Append(strTipoRegistro, 2)
    '            strCodGrupo = strCodGrupo.PadLeft(dt_Cabecera.Rows(1)("Logitud"), "0")
    '            sb.Append(strCodGrupo)
    '            strCodUnico = strCodUnico.PadLeft(dt_Cabecera.Rows(2)("Logitud"), "9")
    '            sb.Append(strCodUnico)
    '            strCodRubro = strCodRubro.PadLeft(dt_Cabecera.Rows(3)("Logitud"), "0")
    '            sb.Append(strCodRubro)
    '            strCodEmpresa = strCodEmpresa.PadLeft(dt_Cabecera.Rows(4)("Logitud"), "0")
    '            sb.Append(strCodEmpresa)
    '            strCodServicio = strCodServicio.PadLeft(dt_Cabecera.Rows(5)("Logitud"), "0")
    '            sb.Append(strCodServicio)
    '            strCodRequerimiento = strCodRequerimiento.PadLeft(dt_Cabecera.Rows(6)("Logitud"), "0")
    '            sb.Append(strCodRequerimiento)
    '            strCodSolicitud = strCodSolicitud.PadLeft(dt_Cabecera.Rows(7)("Logitud"), "0")
    '            sb.Append(strCodSolicitud)
    '            strDesSolicitud = strDesSolicitud.PadLeft(dt_Cabecera.Rows(8)("Logitud"), "0")
    '            sb.Append(strDesSolicitud)
    '            strCanEnvio = strCanEnvio.PadLeft(dt_Cabecera.Rows(9)("Logitud"), "0")
    '            sb.Append(strCanEnvio)
    '            strTipInformacion = strTipInformacion.PadLeft(dt_Cabecera.Rows(10)("Logitud"), "")
    '            sb.Append(strTipInformacion)
    '            strNumRegistro = strNumRegistro.PadLeft(dt_Cabecera.Rows(11)("Logitud"), "0")
    '            sb.Append(strNumRegistro)
    '            strImpSoles = strImpSoles.PadLeft(dt_Cabecera.Rows(12)("Logitud"), "0")
    '            sb.Append(strImpSoles)
    '            strImpDolares = strImpDolares.PadLeft(dt_Cabecera.Rows(13)("Logitud"), "0")
    '            sb.Append(strImpDolares)
    '            stFecProceso = stFecProceso.PadLeft(dt_Cabecera.Rows(14)("Logitud"), "0")
    '            sb.Append(stFecProceso)
    '            strLibre = strLibre.PadLeft(dt_Cabecera.Rows(15)("Logitud"), " ")
    '            sb.Append(strLibre)
    '            strCodFijo = strCodFijo.PadLeft(dt_Cabecera.Rows(16)("Logitud"), "0")
    '            sb.Append(strCodFijo)

    '            'sb.Append("preuba de impresion", 21, 20)
    '            'If y <> dtTCALMA_MERC.Columns.Count - 1 Then
    '            sb.Append("|")
    '            'End If
    '            sb.Append(ControlChars.CrLf)
    '        Next

    '        '*************Seccion de cuotas*************************************
    '        Dim strBlancos As String
    '        sb.Append("12") 'tipo de regsitro
    '        sb.Append("00000000") 'Codigo de cuota
    '        sb.Append("1") 'Numero de Concepto
    '        strBlancos = ""
    '        strBlancos = strBlancos.PadRight(181, "")
    '        sb.Append(strBlancos) 'campo llena espacion
    '        sb.Append("00000000") 'tipo de regsitro
    '        sb.Append("|")
    '        sb.Append(ControlChars.CrLf)

    '        '*************Recupera las configuracion de las tablas Detalle*************************************
    '        Dim Ds_Detalle As New DataSet
    '        Dim dt_Detalle As New DataTable

    '        Ds_Detalle.ReadXml(Server.MapPath("Registro_detalle.xml"))
    '        dt_Detalle = Ds_Detalle.Tables(0)
    '        For Y As Integer = 0 To dt_Temporal_detalle.Rows.Count - 1
    '            strTipRegD = Mid((dt_Temporal_detalle.Rows(y)("IntTipRegD")), 1, dt_Detalle.Rows(0)("Logitud"))
    '            strCodDeu = Mid((dt_Temporal_detalle.Rows(y)("strCodDeu")), 1, dt_Detalle.Rows(1)("Logitud"))
    '            strCodCuo = Mid((dt_Temporal_detalle.Rows(y)("strCodCuo")), 1, dt_Detalle.Rows(2)("Logitud"))
    '            strNomDeu = Mid((dt_Temporal_detalle.Rows(y)("strNomDeu")), 1, dt_Detalle.Rows(3)("Logitud"))
    '            strRefCur1 = Mid((dt_Temporal_detalle.Rows(y)("strRefCur1")), 1, dt_Detalle.Rows(4)("Logitud"))
    '            strRefCur2 = Mid((dt_Temporal_detalle.Rows(y)("strRefCur2")), 1, dt_Detalle.Rows(5)("Logitud"))
    '            strFecEmi = Mid((dt_Temporal_detalle.Rows(y)("IntFecEmi")), 1, dt_Detalle.Rows(6)("Logitud"))
    '            strFecVen = Mid((dt_Temporal_detalle.Rows(y)("IntFecVen")), 1, dt_Detalle.Rows(7)("Logitud"))
    '            strNumDoc = Mid((dt_Temporal_detalle.Rows(y)("strNumDoc")), 1, dt_Detalle.Rows(8)("Logitud"))
    '            strMonDeu = Mid((dt_Temporal_detalle.Rows(y)("IntMonDeu")), 1, dt_Detalle.Rows(9)("Logitud"))
    '            strImpCon1 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon1")), 1, dt_Detalle.Rows(10)("Logitud"))
    '            strImpCon2 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon2")), 1, dt_Detalle.Rows(11)("Logitud"))
    '            strImpCon3 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon3")), 1, dt_Detalle.Rows(12)("Logitud"))
    '            strImpCon4 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon4")), 1, dt_Detalle.Rows(13)("Logitud"))
    '            strImpCon5 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon5")), 1, dt_Detalle.Rows(14)("Logitud"))
    '            strImpCon6 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon6")), 1, dt_Detalle.Rows(15)("Logitud"))
    '            strImpCon7 = Mid((dt_Temporal_detalle.Rows(y)("IntImpCon7")), 1, dt_Detalle.Rows(16)("Logitud"))
    '            strLibre1D = Mid((dt_Temporal_detalle.Rows(y)("strLibre1D")), 1, dt_Detalle.Rows(17)("Logitud"))
    '            strTipOpe = Mid((dt_Temporal_detalle.Rows(y)("strTipOpe")), 1, dt_Detalle.Rows(18)("Logitud"))
    '            strLibre2D = Mid((dt_Temporal_detalle.Rows(y)("strLibre2D")), 1, dt_Detalle.Rows(19)("Logitud"))
    '            strCodFijD = Mid((dt_Temporal_detalle.Rows(y)("IntCodFijD")), 1, dt_Detalle.Rows(20)("Logitud"))

    '            strTipRegD = strTipRegD.PadLeft(dt_Detalle.Rows(0)("Logitud"), "0")
    '            sb.Append(strTipRegD)
    '            strCodDeu = strCodDeu.PadLeft(dt_Detalle.Rows(1)("Logitud"), "")
    '            sb.Append(strCodDeu)
    '            strCodCuo = strCodCuo.PadLeft(dt_Detalle.Rows(2)("Logitud"), "")
    '            sb.Append(strCodCuo)
    '            strNomDeu = strNomDeu.PadRight(dt_Detalle.Rows(3)("Logitud"), "")
    '            sb.Append(strNomDeu)
    '            strRefCur1 = strRefCur1.PadLeft(dt_Detalle.Rows(4)("Logitud"), "")
    '            sb.Append(strRefCur1)
    '            strRefCur2 = strRefCur2.PadLeft(dt_Detalle.Rows(5)("Logitud"), "")
    '            sb.Append(strRefCur2)
    '            strFecEmi = strFecEmi.PadLeft(dt_Detalle.Rows(6)("Logitud"), "0")
    '            sb.Append(strFecEmi)
    '            strFecVen = strFecVen.PadLeft(dt_Detalle.Rows(7)("Logitud"), "0")
    '            sb.Append(strFecVen)
    '            strNumDoc = strNumDoc.PadLeft(dt_Detalle.Rows(8)("Logitud"), "")
    '            sb.Append(strNumDoc)
    '            strMonDeu = strMonDeu.PadLeft(dt_Detalle.Rows(9)("Logitud"), "0")
    '            sb.Append(strMonDeu)
    '            strImpCon1 = strImpCon1.PadLeft(dt_Detalle.Rows(10)("Logitud"), "")
    '            sb.Append(strImpCon1)
    '            strImpCon2 = strImpCon2.PadLeft(dt_Detalle.Rows(11)("Logitud"), "0")
    '            sb.Append(strImpCon2)
    '            strImpCon3 = strImpCon3.PadLeft(dt_Detalle.Rows(12)("Logitud"), "0")
    '            sb.Append(strImpCon3)
    '            strImpCon4 = strImpCon4.PadLeft(dt_Detalle.Rows(13)("Logitud"), "0")
    '            sb.Append(strImpCon4)
    '            strImpCon5 = strImpCon5.PadLeft(dt_Detalle.Rows(14)("Logitud"), "0")
    '            sb.Append(strImpCon5)
    '            strImpCon6 = strImpCon6.PadLeft(dt_Detalle.Rows(15)("Logitud"), "")
    '            sb.Append(strImpCon6)
    '            strImpCon7 = strImpCon7.PadLeft(dt_Detalle.Rows(16)("Logitud"), "0")
    '            sb.Append(strImpCon7)
    '            strLibre1D = strLibre1D.PadLeft(dt_Detalle.Rows(17)("Logitud"), " ")
    '            sb.Append(strLibre1D)
    '            strTipOpe = strTipOpe.PadLeft(dt_Detalle.Rows(18)("Logitud"), "")
    '            sb.Append(strTipOpe)
    '            strLibre2D = strLibre2D.PadLeft(dt_Detalle.Rows(19)("Logitud"), " ")
    '            sb.Append(strLibre2D)
    '            strCodFijD = strCodFijD.PadLeft(dt_Detalle.Rows(20)("Logitud"), "0")
    '            sb.Append(strCodFijD)
    '            sb.Append("|")
    '            sb.Append(ControlChars.CrLf)

    '        Next

    '        Response.Clear()
    '        Response.AddHeader("content-disposition", "attachment;filename=ldetpt.txt")
    '        Response.Charset = ""
    '        Response.Cache.SetCacheability(HttpCacheability.Private)
    '        Response.ContentType = "application/vnd.text"
    '        Dim stringWrite As System.IO.StreamWriter
    '        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
    '        Response.Write(sb)
    '        Response.End()
    '    Catch e1 As Exception
    '        Dim sCA_ERRO As String = e1.Message
    '    End Try
    'End Sub
    Public Function gbolDetalle_Doc(ByVal pstrCodCliente As String, _
          ByVal pNumRequTemp As String, ByVal pstrCodArea As String) As Boolean
        objRequ = New LibCapaAccesoDatos.clsCADRequerimiento
        With objRequ
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .strCodRequerimiento = pNumRequTemp
                .strCodArea = pstrCodArea

                If .gdtPersonaAuto() = True Then
                    Me.lstrNomUsuario = .strUsuarioNombres
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = .strMensajeError
                    Return False
                End If

            Catch ex As Exception
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            End Try
        End With
    End Function

    Public Sub New()
        lintNumConsultas = 0
        lstrcodEmpresa = "01"
        lstrCodUnidad = "001"
    End Sub
End Class
