Public Class clsCNUsuario
    Private lstrCodUsuario As String
    Private lstrClave As String
    Private lstrEstActividad As String    'S: ACTIVO N: NO ACTIVO
    Private lstrEstExpiracionClave As String    'S o N
    Private lstrFecExpiracionClave As String
    Private lstrEstExpiracionUsuario As String
    Private lstrFecExpiracionUsuario As String
    Private lstrEstHora As String
    Private lstrHoraInicio As String
    Private lstrHoraFinal As String
    Private lstrCodGrupo As String
    Private lstrFecModificacionClave As String
    Private lstrFecActual As String
    Private lstrHoraActual As String
    Private lintNroDiasRenovacion As Integer
    Private lstrTipoUsuario As String
    Private lstrCodEmpresa As String
    Private lstrNomEmpresa As String
    Private lstrCodCliente As String
    Private lstrCodCliente2 As String
    Private lstrNomCliente As String
    Private lstrTipoCliente As String

    Private lintRetorno As Integer
    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private lstrMensajeError As String
    Private lblnDepsaFile As Boolean
    Private dtTemporal As DataTable

    Private objUsuario As LibCapaAccesoDatos.clsCADUsuario
    Private objFuncion As LibCapaNegocio.clsFunciones

    Private mstrNombre As String

    Public Function gbolValidaUsuario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            objUsuario = New LibCapaAccesoDatos.clsCADUsuario
            objFuncion = New LibCapaNegocio.clsFunciones

            With objUsuario
                .strCodUsuario = lstrCodUsuario
                .strClave = objFuncion.gstrEncriptar(lstrClave)
                dtTemporal = .gdtValidaUsuario
                lintFilasAfectadas = .intFilasAfectadas
                lintRetorno = .intRetorno
            End With
            Select Case lintRetorno
                Case 0
                    With dtTemporal
                        lstrEstActividad = CStr(.Rows(0)("ST_ACTI"))
                        lstrEstExpiracionClave = CStr(.Rows(0)("ST_EXPI_CLAV"))
                        lstrFecExpiracionClave = CStr(.Rows(0)("FE_EXPI_CLAV"))
                        lstrEstExpiracionUsuario = CStr(.Rows(0)("ST_EXPI_USUA"))
                        lstrFecExpiracionUsuario = CStr(.Rows(0)("FE_EXPI_USUA"))
                        lstrEstHora = CStr(.Rows(0)("ST_HORA"))
                        lstrHoraInicio = CStr(.Rows(0)("HO_INIC"))
                        lstrHoraFinal = CStr(.Rows(0)("HO_FINA"))
                        lstrCodGrupo = CStr(.Rows(0)("CO_GRUP"))
                        lstrFecModificacionClave = CStr(.Rows(0)("FE_MODI_CLAV"))
                        lstrFecActual = CStr(.Rows(0)("FE_ACTU"))
                        lstrHoraActual = CStr(.Rows(0)("HO_ACTU"))
                        lintNroDiasRenovacion = CInt(.Rows(0)("NU_DIAS_RENO"))
                        lstrTipoUsuario = CStr(.Rows(0)("TI_USUA"))
                        mstrNombre = CStr(.Rows(0)("NOM_USER"))
                    End With
                    'bolRetorno = True
                    'lblnDepsaFile = ValidaPerfilUsuario()
                    bolRetorno = ValidaPerfilUsuario()
                Case 1
                    bolRetorno = False
                    lstrMensajeError = "El usuario no existe!"
                Case 2
                    bolRetorno = False
                    lstrMensajeError = "La constraseña ingresada no es correcta!"
            End Select

            Return bolRetorno
        Catch ex As Exception
            lstrMensajeError = ex.Message
            Return False
            Throw New Exception(ex.Message)
        End Try
    End Function

    Private Function ValidaPerfilUsuario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            If lstrEstActividad = "N" Then
                bolRetorno = False
                lstrMensajeError = "El usuario ingresado se encuentra inactivo, verificar con su departamento de Informática!"
            ElseIf lstrEstExpiracionClave = "S" And lintNroDiasRenovacion < 0 Then
                bolRetorno = False
                lstrMensajeError = "La contraseña de usuario ha vencido!, verificar con su departamento de Informática!"
            ElseIf lstrEstExpiracionUsuario = "S" And (lstrFecExpiracionUsuario < lstrFecActual) Then
                bolRetorno = False
                lstrMensajeError = "La cuenta de usuario ha vencido!, verificar con su departamento de Informática!"
            ElseIf (lstrEstHora = "S") And ((lstrHoraActual < lstrHoraInicio) Or (lstrHoraActual > lstrHoraFinal)) Then
                bolRetorno = False
                lstrMensajeError = "Horario no permitido para el usuario!, consultar con su departamento de Informática!"
            Else
                If lstrEstExpiracionClave = "S" Then
                    lstrMensajeError = "Faltan " & CStr(lintNroDiasRenovacion) & " dias para el vencimiento de su contraseña"
                    If lintNroDiasRenovacion > 0 Then
                        bolRetorno = True
                    Else
                        bolRetorno = False
                    End If
                Else
                    bolRetorno = True
                End If
                'If lstrTipoUsuario = "EXTERNO" Then
                '    bolRetorno = gConsigueTipoCliente()
                'Else
                '    bolRetorno = False
                '    lstrMensajeError = "Acceso denegado! Usuario interno no tiene acceso"
                'End If
            End If

            Return bolRetorno
        Catch ex As Exception
            Return False
            lstrMensajeError = ex.Message
        End Try
    End Function

    Public Function gConsigueTipoCliente(ByVal strCodUsuario As String) As Boolean
        Dim bolRetorno As Boolean = False
        objUsuario = New LibCapaAccesoDatos.clsCADUsuario
        Try
            With objUsuario
                '.strCodUsuario = lstrCodUsuario
                .strCodUsuario = strCodUsuario
                dtTemporal = .gdtConTipoCliente
                lintFilasAfectadas = .intFilasAfectadas
            End With
            If lintFilasAfectadas > 0 Then
                With dtTemporal
                    lstrCodEmpresa = .Rows(0)("CO_EMPR")
                    lstrNomEmpresa = .Rows(0)("DE_NOMB_CORT")
                    lstrNomCliente = .Rows(0)("NO_AUXI")
                    If lintFilasAfectadas = 1 Then
                        If .Rows(0)("TIPO") = 1 Then
                            lstrCodCliente = .Rows(0)("CO_AUXI")
                            lstrTipoCliente = "CLIENTE"
                        ElseIf .Rows(0)("TIPO") = 2 Then
                            lstrCodCliente2 = .Rows(0)("CO_AUXI")
                            lstrTipoCliente = "FINANCIADOR"
                        End If
                    ElseIf lintFilasAfectadas = 2 Then
                        If .Rows(0)("TIPO") = 1 Then
                            lstrCodCliente = .Rows(0)("CO_AUXI")
                            lstrCodCliente2 = .Rows(1)("CO_AUXI")
                            lstrNomCliente = .Rows(0)("NO_AUXI")
                        ElseIf .Rows(1)("TIPO") = 1 Then
                            lstrCodCliente = .Rows(1)("CO_AUXI")
                            lstrCodCliente2 = .Rows(0)("CO_AUXI")
                            lstrNomCliente = .Rows(1)("NO_AUXI")
                        End If
                        lstrTipoCliente = "CLIENTE/FINAN"
                    End If
                End With
                bolRetorno = True
            Else
                bolRetorno = False
                lstrMensajeError = "Usuario no asignado a un cliente o a un financiador."
            End If

            Return bolRetorno
        Catch ex As Exception
            Return False
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strClave() As String
        Get
            Return lstrClave
        End Get
        Set(ByVal Value As String)
            lstrClave = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public ReadOnly Property intRetorno() As Integer
        Get
            Return lintRetorno
        End Get
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
    Public Property strTipoUsuario() As String
        Get
            Return lstrTipoUsuario
        End Get
        Set(ByVal Value As String)
            lstrTipoUsuario = Value
        End Set
    End Property
    Public Property strCodEmpresa() As String
        Get
            Return lstrCodEmpresa
        End Get
        Set(ByVal Value As String)
            lstrCodEmpresa = Value
        End Set
    End Property
    Public Property strTipoCliente() As String
        Get
            Return lstrTipoCliente
        End Get
        Set(ByVal Value As String)
            lstrTipoCliente = Value
        End Set
    End Property
    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property
    Public Property strNomCliente() As String
        Get
            Return lstrNomCliente
        End Get
        Set(ByVal Value As String)
            lstrNomCliente = Value
        End Set
    End Property
    Public Property strCodGrupo() As String
        Get
            Return lstrCodGrupo
        End Get
        Set(ByVal Value As String)
            lstrCodGrupo = Value
        End Set
    End Property

    Public Sub New()
        lstrErrorSql = "0"
        lintFilasAfectadas = 0
    End Sub

    Public Property strNombre() As String
        Get
            Return mstrNombre
        End Get
        Set(ByVal Value As String)
            mstrNombre = Value
        End Set
    End Property

    Public Property blnDepsaFile() As Boolean
        Get
            Return lblnDepsaFile
        End Get
        Set(ByVal Value As Boolean)
            lblnDepsaFile = Value
        End Set
    End Property
End Class
