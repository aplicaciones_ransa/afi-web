Imports System.Data
Imports System.Data.SqlClient

Public Class clsCNConsulta
    Private lstrCodRequerimiento As String
    Private lintNumRequTemp As Integer
    Private lintNumConsulta As Integer
    Private lstrcodEmpresa As String
    Private lstrCodUnidad As String

    Private lstrNombreUsuario As String
    Private lintFilasAfectadas As Integer
    Private lstrErrorSql As String
    Private lstrMensajeError As String
    Private objCons As LibCapaAccesoDatos.clsCADConsulta
    Private objTransaccion As SqlTransaction
    Private objConexion As SqlConnection
    Public Function gstrUsuarioAprobador(ByVal pstrCodCliente As String, _
    ByVal pstrCodPersona As String, ByVal pstrCodArea As String, _
    ByVal pstrCodUsuario As String) As String
        Dim strRetorno As String = "N"
        Try
            objCons = New LibCapaAccesoDatos.clsCADConsulta
            With objCons
                .strCodCliente = pstrCodCliente
                .strCodPersonaAutorizada = pstrCodPersona
                .strCodArea = pstrCodArea
                .strCodUsuario = pstrCodUsuario
                .gUsuarioAprobador()
                strRetorno = .strAprobador
            End With
            Return strRetorno
        Catch ex As Exception
            Return strRetorno
        End Try
    End Function
    '*verifica si existe otro item todmado por otro usuario*  AURIS
    Public Function gbolConsultaTempApropiados(ByVal pstrCodCliente As String, _
             ByVal pstrCodTipoItem As String, ByVal pstrIdUnidad As String, _
             ByVal pstrTipoDocumento As String, ByVal pstrCont As String, _
             ByVal pstrCodDivision As String) As Boolean
        objCons = New LibCapaAccesoDatos.clsCADConsulta
        With objCons
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                '.intNumRequTemp = lintNumRequTemp
                .strIdUnidad = pstrIdUnidad
                .strCodTipoItem = pstrCodTipoItem
                '.strCodArea = pstrCodArea
                .strCodDivision = pstrCodDivision
                .strCont = pstrCont
                '.strSecuDivi = pstrSecuDivi
                '.strHoraIngreso = pstrHoraIngreso

                If .gSelConseguirItemApropiado(objTransaccion) = True Then
                    'If gbolInsConsultaTemporal(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumConsulta = .intNumConsulta
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrNombreUsuario = .strCodPersonaAutorizada
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql

                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function

    Public Function gbolInsConsultaTemp(ByVal pstrCodCliente As String, _
         ByVal pstrCodTipoItem As String, ByVal pstrDesEnvio As String, ByVal pstrIdUnidad As String, _
         ByVal pstrCodLocalidad As String, _
         ByVal pstrTipoDocumento As String, ByVal pstrNumDocumento As String, _
         ByVal pstrDesObservacion As String, ByVal pstrCodArea As String, _
         ByVal pstrCodCriterio As String, ByVal pstrCodDivision As String, _
         ByVal pstrCont As String, ByVal pstrSecuDivi As Integer, _
         ByVal pstrCodUsuario As String, ByVal pstrTipoCons As String) As Boolean
        objCons = New LibCapaAccesoDatos.clsCADConsulta
        With objCons

            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = lintNumRequTemp
                .strCodTipoItem = pstrCodTipoItem
                .strDesEnvio = pstrDesEnvio
                .strIdUnidad = pstrIdUnidad
                .strCodLocalidad = pstrCodLocalidad
                .strTipoDocumento = pstrTipoDocumento
                .strNumDocumento = pstrNumDocumento
                .strDesObservacion = pstrDesObservacion
                .strCodArea = pstrCodArea
                .strCodCriterio = pstrCodCriterio
                .strCodDivision = pstrCodDivision
                .strCont = pstrCont
                .strSecuDivi = pstrSecuDivi
                .strCodUsuario = pstrCodUsuario
                .strTipoCons = pstrTipoCons

                If .gbolInsConsultaTemporal(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumConsulta = .intNumConsulta
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function

    'Public Function gbolEliminaConsulta(ByVal pstrCodCliente As String, _
    '         ByVal pintNumRequTemp As Integer, ByVal pintNumConsulta As Integer) As Boolean
    '    objCons = New LibCapaAccesoDatos.clsCADConsulta
    '    With objCons
    '        objConexion = New SqlConnection(.strCadenaConexion)
    '        objConexion.Open()
    '        objTransaccion = objConexion.BeginTransaction
    '        Try
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodCliente = pstrCodCliente
    '            .intNumRequTemp = pintNumRequTemp
    '            .intNumConsulta = pintNumConsulta

    '            If .gbolEliminarConsulta(objTransaccion) = True Then
    '                objTransaccion.Commit()
    '                objTransaccion.Dispose()
    '                Me.lintNumConsulta = .intNumConsulta
    '                Me.lintFilasAfectadas = .intFilasAfectadas
    '                Me.lstrErrorSql = .strErrorSql
    '                Return True
    '            Else
    '                objTransaccion.Rollback()
    '                objTransaccion.Dispose()
    '                Me.lintFilasAfectadas = .intFilasAfectadas
    '                Me.lstrErrorSql = .strErrorSql
    '                Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
    '                Return False
    '            End If

    '        Catch ex As Exception
    '            objTransaccion.Rollback()
    '            objTransaccion.Dispose()
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '            Throw New Exception(ex.Message)
    '            Return False
    '        Finally
    '            objConexion.Close()
    '            objConexion.Dispose()
    '            objConexion = Nothing
    '        End Try

    '    End With

    'End Function

    Public Function gbolEliminaConsulta(ByVal pstrCodCliente As String, _
             ByVal pintNumRequTemp As Integer, ByVal pintNumConsulta As Integer, ByVal pintArea As String) As Boolean
        objCons = New LibCapaAccesoDatos.clsCADConsulta
        With objCons
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = pintNumRequTemp
                .intNumConsulta = pintNumConsulta
                .strCodArea = pintArea



                If .gbolEliminarConsulta(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumConsulta = .intNumConsulta
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function

    Public Function gbolActualizarConsulta(ByVal pstrCodCliente As String, _
             ByVal pintNumRequTemp As Integer, ByVal pintNumConsulta As Integer, _
             ByVal pstrCodTipoEnvio As String) As Boolean
        objCons = New LibCapaAccesoDatos.clsCADConsulta
        With objCons
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                .intNumRequTemp = pintNumRequTemp
                .intNumConsulta = pintNumConsulta
                .strCodTipoEnvio = pstrCodTipoEnvio

                If .gbolActualizarConsulta(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumConsulta = .intNumConsulta
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try
        End With
    End Function

    Public Function gbolEliminaConsultaTemporal(ByVal pstrCodCliente As String, _
             ByVal pstrUsuario As String) As Boolean
        objCons = New LibCapaAccesoDatos.clsCADConsulta
        With objCons
            objConexion = New SqlConnection(.strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction
            Try
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodCliente = pstrCodCliente
                '.intNumRequTemp = pstrUsuario
                .strCodUsuario = pstrUsuario

                If .gbolEliminarTemporal(objTransaccion) = True Then
                    objTransaccion.Commit()
                    objTransaccion.Dispose()
                    Me.lintNumConsulta = .intNumConsulta
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Return True
                Else
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Me.lintFilasAfectadas = .intFilasAfectadas
                    Me.lstrErrorSql = .strErrorSql
                    Me.lstrMensajeError = "No se puedo completar la transaccion, error en el procedimiento."
                    Return False
                End If

            Catch ex As Exception
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Throw New Exception(ex.Message)
                Return False
            Finally
                objConexion.Close()
                objConexion.Dispose()
                objConexion = Nothing
            End Try

        End With

    End Function


    Public Property strCodRequerimiento() As String
        Get
            Return lstrCodRequerimiento
        End Get
        Set(ByVal Value As String)
            lstrCodRequerimiento = Value
        End Set
    End Property
    Public Property intNumRequTemp() As Integer
        Get
            Return lintNumRequTemp
        End Get
        Set(ByVal Value As Integer)
            lintNumRequTemp = Value
        End Set
    End Property
    Public Property intNumConsulta() As Integer
        Get
            Return lintNumConsulta
        End Get
        Set(ByVal Value As Integer)
            lintNumConsulta = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
    Public Property strUsuarioTemp() As String
        Get
            'Return lstrMensajeError
            Return lstrNombreUsuario
        End Get
        Set(ByVal Value As String)
            lstrNombreUsuario = Value
        End Set
    End Property

    Public Sub New()
        lstrcodEmpresa = "01"
        lstrCodUnidad = "001"
        lintFilasAfectadas = 0
        lstrErrorSql = "0"
        lstrMensajeError = ""
    End Sub

End Class
