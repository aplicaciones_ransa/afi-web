Imports System.Web.Mail
Imports System.IO
Imports System.Web.UI

Public Class clsFunciones
    Public Function SendMail(ByVal strCorreoDe As String, ByVal strCorreoPara As String, _
                                 ByVal strAsunto As String, ByVal strMensaje As String) As Boolean
        Try
            Dim mmCorreo As New MailMessage
            With mmCorreo
                Const ConfigNamespace As String = "http://schemas.microsoft.com/cdo/configuration/"
                Dim Flds As System.Collections.IDictionary
                Flds = mmCorreo.Fields
                With Flds
                    .Add(ConfigNamespace & "smtpserver", System.Configuration.ConfigurationManager.AppSettings("smtpserver"))
                    .Add(ConfigNamespace & "smtpserverport", System.Configuration.ConfigurationManager.AppSettings("smtpserverport"))
                    .Add(ConfigNamespace & "sendusing", System.Configuration.ConfigurationManager.AppSettings("sendusing"))
                    .Add(ConfigNamespace & "sendusername", System.Configuration.ConfigurationManager.AppSettings("sendusername"))
                    .Add(ConfigNamespace & "sendpassword", System.Configuration.ConfigurationManager.AppSettings("sendpassword"))
                    .Add(ConfigNamespace & "smtpauthenticate", System.Configuration.ConfigurationManager.AppSettings("smtpauthenticate"))
                End With
                'Se Indica la Dirección de correo que envia
                .From = strCorreoDe
                'Se Indica la Dirección de correo que recibira
                .To = strCorreoPara
                'Se Indica el Asunto del correo a enviar
                .Subject = strAsunto
                'El Mensaje del Correo
                .Body = strMensaje
                'establece el tipo de contenido del texto del mensaje de correo electrónico.
                'MailFormat.Html o MailFormat.Text
                .BodyFormat = MailFormat.Html
                'Establece la prioridad del mensaje de correo electrónico
                'MailPriority.High, MailPriority.Normal o MailPriority.Low
                .Priority = MailPriority.High
                'Establece el nombre del servidor de transmisión de correo SMTP 
                'que se va a utilizar para enviar los mensajes de correo electrónico.mail.servidor.com 
                'SmtpMail.SmtpServer = "161.132.96.92" '"System.Configuration.ConfigurationManager.AppSettings("SMTPServerName")
                'Envía un mensaje de correo electrónico utilizando argumentos 
                'suministrados en las propiedades de la clase MailMessage.
                SmtpMail.Send(mmCorreo)
            End With
            Return True
        Catch ex As Exception
            Dim errorMessage As String = ""
            While Not IsDBNull(ex)
                errorMessage += ex.Message + "\n"
                ex = ex.InnerException
            End While
            Return False
        End Try
    End Function

    Public Sub gCargaCombo(ByRef objCombo As System.Web.UI.WebControls.DropDownList, ByVal pdtData As DataTable)
        objCombo.DataSource = pdtData
        objCombo.DataBind()
        pdtData.Dispose()
        pdtData = Nothing
    End Sub

    Public Sub gCargaGrid(ByRef objGrid As System.Web.UI.WebControls.DataGrid, ByVal pdtData As DataTable)
        objGrid.DataSource = pdtData
        objGrid.DataBind()
        pdtData.Dispose()
        pdtData = Nothing
    End Sub

    Public Function gstrFecha(Optional ByVal pintNroDias As Integer = 0) As String
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime

        If pintNroDias = 0 Then
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
        Else
            dttFecha = Date.Today.Subtract(TimeSpan.FromDays(pintNroDias))
            Dia = "00" + CStr(dttFecha.Day)
            Mes = "00" + CStr(dttFecha.Month)
            Anio = "0000" + CStr(dttFecha.Year)
        End If

        Return Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)

    End Function
    Public Function gstrFechaMes(ByVal pstrPosicion As String) As String
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime

        If pstrPosicion = 0 Then
            dttFecha = CDate("01" & "/" & CStr(Now.Month) & "/" & CStr(Now.Year))

            Dia = "00" + CStr(dttFecha.Day)
            Mes = "00" + CStr(dttFecha.Month)
            Anio = "0000" + CStr(dttFecha.Year)
        ElseIf pstrPosicion = 1 Then
            dttFecha = CDate("01" & "/" & CStr(Now.Month + 1) & "/" & CStr(Now.Year))
            dttFecha = dttFecha.Subtract(TimeSpan.FromDays(1))
            Dia = "00" + CStr(dttFecha.Day)
            Mes = "00" + CStr(dttFecha.Month)
            Anio = "0000" + CStr(dttFecha.Year)
        End If

        Return Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)

    End Function
    Public Function ExportarAExcel(ByVal pstrNombreArchivo As String, ByVal pobjResponse As System.Web.HttpResponse, _
    ByVal pobjGrid As System.Web.UI.WebControls.DataGrid)
        With pobjResponse
            'se limpia el buffer del response
            .Clear()
            .Buffer = True

            'se establece el tipo de accion a ejecutar
            .ContentType = "application/download"
            .Charset = ""

            .AddHeader("Content-Disposition", "attachment;filename=" + pstrNombreArchivo)

            'se rederiza el control datagrid
            Dim sWriter As StringWriter = New StringWriter
            Dim htmlWriter As HtmlTextWriter = New HtmlTextWriter(sWriter)

            pobjGrid.RenderControl(htmlWriter)

            'se escribe el contenido en el listado
            .Write(sWriter.ToString())
            .End()

        End With

    End Function
    Public Function gstrEncriptar(ByVal pstrCadena As String) As String
        Dim nCO_CADE As String = 1
        Dim nCA_ASCI As String = 0
        Dim sCA_PARC As String = ""
        Dim sCA_RESU As String = ""

        Do While (nCO_CADE <= Len(pstrCadena))
            sCA_PARC = Mid(pstrCadena, nCO_CADE, 1)
            nCA_ASCI = Asc(sCA_PARC) + 10 + (nCO_CADE - 1)
            sCA_PARC = Chr(nCA_ASCI)
            sCA_RESU = sCA_RESU & sCA_PARC
            nCO_CADE = nCO_CADE + 1
        Loop

        Return sCA_RESU
    End Function

End Class
