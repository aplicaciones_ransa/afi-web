Imports System.Data
Imports System.Data.SqlClient

Public Class clsCNCliente
    Private lstrCodCliente As String
    Private lstrCodUsuario As String

    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private lstrMensajeError As String
    Private dtTemporal As DataTable

    Private objCliente As LibCapaAccesoDatos.clsCADCliente

    Public Function gstrConseguirCadenaAreas() As String
        Dim strCadena As String = ""
        Dim intI As Integer = 0
        Try
            objCliente = New LibCapaAccesoDatos.clsCADCliente
            With objCliente
                .strCodCliente = lstrCodCliente
                .strCodUsuario = lstrCodUsuario
                dtTemporal = .gdtConseguirAreas
                lintFilasAfectadas = .intFilasAfectadas
                lstrErrorSql = .strErrorSql
            End With
            If lintFilasAfectadas = 0 Then
                strCadena = "-"
            ElseIf lintFilasAfectadas > 0 Then
                While intI < lintFilasAfectadas
                    strCadena += "'" & dtTemporal.Rows(intI)("CO_AREA").ToString & "',"
                    intI += 1
                End While
                strCadena = strCadena.Remove(strCadena.Length - 1, 1)
            End If
            Return strCadena
        Catch ex As Exception
            lstrMensajeError = ex.Message
            Throw New Exception(ex.Message)
        Finally
            dtTemporal.Dispose()
            dtTemporal = Nothing
        End Try
    End Function

    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strCodCliente() As String
        Get
            Return lstrCodCliente
        End Get
        Set(ByVal Value As String)
            lstrCodCliente = Value
        End Set
    End Property

    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
End Class
