Imports System.Web
Imports System.Data

Public Class clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objComun As LibCapaAccesoDatos.clsCADComun
    Private lintFilasAfectadas As Integer
    Private lstrErrorSql As String

    'Public Sub gCargarComboArea(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
    '    ByVal pstrCodCliente As String, Optional ByVal pstrPermisos As String = "*", _
    '    Optional ByVal pstrCodUsuario As String = "")
    '    Dim dtTemporal As DataTable
    '    objComun = New LibCapaAccesoDatos.clsCADComun
    '    objFuncion = New LibCapaNegocio.clsFunciones
    '    dtTemporal = objComun.gdtComboMaestro("AREA", pstrCodCliente, IIf(pstrPermisos = "*" Or pstrPermisos = "-", pstrPermisos, pstrCodUsuario))
    '    If pstrPermisos = "*" Then
    '        Dim drFila As DataRow = dtTemporal.NewRow
    '        drFila("COD") = ""
    '        drFila("DES") = "TODOS"
    '        dtTemporal.Rows.InsertAt(drFila, 0)
    '    End If
    '    objFuncion.gCargaCombo(objCombo, dtTemporal)
    '    lintFilasAfectadas = objComun.intFilasAfectadas
    '    lstrErrorSql = objComun.strErrorSql
    '    objFuncion = Nothing
    '    objComun = Nothing
    'End Sub
    Public Sub gCargarComboArea(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
            ByVal pstrCodCliente As String, Optional ByVal pstrPermisos As String = "*", _
            Optional ByVal pstrCodUsuario As String = "", Optional ByVal pstrTodos As String = "")
        Dim dtTemporal As DataTable
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        dtTemporal = objComun.gdtComboMaestro("AREA", pstrCodCliente, IIf(pstrPermisos = "*" Or pstrPermisos = "-", pstrPermisos, pstrCodUsuario))
        If pstrPermisos = "*" Then
            Dim drFila As DataRow = dtTemporal.NewRow
            drFila("COD") = ""
            drFila("DES") = "TODOS"
            dtTemporal.Rows.InsertAt(drFila, 0)
        Else
            If pstrTodos = "*" Then
                Dim drFila As DataRow = dtTemporal.NewRow
                drFila("COD") = "0"
                drFila("DES") = "TODOS"
                dtTemporal.Rows.InsertAt(drFila, 0)
            End If
        End If
        objFuncion.gCargaCombo(objCombo, dtTemporal)
        'objFuncion.gCargaComboArea(objCombo, dtTemporal, pstrPermisos)
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub


    Public Sub gCargarComboClienteAdicional(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
        ByVal pstrCodCliente As String)
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("CLIENTE_ADICIONAL", pstrCodCliente, ""))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboUsuario(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
        ByVal pstrCodCliente As String)
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("USUARIO", pstrCodCliente, ""))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboTipoItem(ByRef objCombo As System.Web.UI.WebControls.DropDownList)
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("TIPO_ITEM", "", ""))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboPersonaAutorizada(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
        ByVal pstrCodCliente As String, Optional ByVal pstrCodArea As String = "")
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("PERSONA_AUTORIZADA", pstrCodCliente, pstrCodArea))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboTipoDocumento(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
        ByVal pstrCodCliente As String, Optional ByVal pstrCodArea As String = "")
        Dim dtTemporal As DataTable
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        dtTemporal = objComun.gdtComboMaestro("CRITERIO", pstrCodCliente, pstrCodArea)
        Dim drFila As DataRow = dtTemporal.NewRow
        drFila("COD") = ""
        drFila("DES") = "TODOS"
        dtTemporal.Rows.InsertAt(drFila, 0)
        objFuncion.gCargaCombo(objCombo, dtTemporal)
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboTipoEnvio(ByRef objCombo As System.Web.UI.WebControls.DropDownList, _
    Optional ByVal pstrTodo As String = "")
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("TIPO_ENVIO", pstrTodo, ""))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Sub gCargarComboGrupo(ByRef objCombo As System.Web.UI.WebControls.DropDownList)
        objComun = New LibCapaAccesoDatos.clsCADComun
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, objComun.gdtComboMaestro("GRUPO", "", "", 1))
        lintFilasAfectadas = objComun.intFilasAfectadas
        lstrErrorSql = objComun.strErrorSql
        objFuncion = Nothing
        objComun = Nothing
    End Sub
    Public Function gdtGetTipoEnvio() As DataTable
        objComun = New LibCapaAccesoDatos.clsCADComun
        Return objComun.gdtComboMaestro("TIPO_ENVIO", "", "")
        objComun = Nothing
    End Function
    Public Sub gCargarComboSituacion(ByRef objCombo As System.Web.UI.WebControls.DropDownList)
        Dim dt As New System.Data.DataTable

        Dim dcCodigo As New DataColumn("COD", GetType(String))
        Dim dcDescripcion As New DataColumn("DES", GetType(String))

        dt.Columns.Add(dcCodigo)
        dt.Columns.Add(dcDescripcion)

        Dim drFila As System.Data.DataRow
        drFila = dt.NewRow
        drFila("COD") = ""                  ' TODOS
        drFila("DES") = "TODOS"
        dt.Rows.Add(drFila)

        drFila = dt.NewRow
        drFila("COD") = "PRO"               ' EN PROCESO
        drFila("DES") = "EN PROCESO"
        dt.Rows.Add(drFila)


        drFila = dt.NewRow
        drFila("COD") = "CER"               '   TERMINADO
        drFila("DES") = "TERMINADO"
        dt.Rows.Add(drFila)

        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, dt)

    End Sub
    Public Sub gCargarComboEstado(ByRef objCombo As System.Web.UI.WebControls.DropDownList)
        Dim dt As New System.Data.DataTable
        Dim dcCodigo As New DataColumn("COD", GetType(String))
        Dim dcDescripcion As New DataColumn("DES", GetType(String))

        dt.Columns.Add(dcCodigo)
        dt.Columns.Add(dcDescripcion)

        Dim drFila As System.Data.DataRow
        drFila = dt.NewRow
        drFila("COD") = ""                  ' TODOS
        drFila("DES") = "TODOS"
        dt.Rows.Add(drFila)

        drFila = dt.NewRow
        drFila("COD") = "APR"               ' APROBADO
        drFila("DES") = "APROBADO"
        dt.Rows.Add(drFila)


        drFila = dt.NewRow
        drFila("COD") = "NAP"               '   NO APROBADO
        drFila("DES") = "NO APROBADO"
        dt.Rows.Add(drFila)

        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaCombo(objCombo, dt)

    End Sub


    Public Sub New()
        lintFilasAfectadas = 0
        lstrErrorSql = "0"
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        objComun = Nothing
        objFuncion = Nothing
    End Sub
    Public ReadOnly Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
    End Property
    Public ReadOnly Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
    End Property

End Class
