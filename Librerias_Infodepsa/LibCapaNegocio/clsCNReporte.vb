Imports System.Data
Imports System.Data.SqlClient

Public Class clsCNReporte

    Private lintFilasAfectadas As Integer
    Private lstrcodEmpresa As String
    Private lstrCodUnidad As String

    Private lstrErrorSql As String
    Private lstrMensajeError As String

    Private objRep As LibCapaAccesoDatos.clsCADReporte

    Public Function gdrRepVencimientoCaja(ByVal pstrCodCliente As String, _
        ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
        ByVal pstrCodTipoItem As String, ByVal pstrCodTipo As String, _
        ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String, ByVal pstrUsuarioLogin As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRep = New LibCapaAccesoDatos.clsCADReporte
            With objRep
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodTipoItem = pstrCodTipoItem
                .strCodTipo = pstrCodTipo
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strUsuarioLogin = pstrUsuarioLogin
                dtTemp = .gdtRepVencimientoCaja
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Return dtTemp
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    'Public Function gdrRepUnidadesExistentesxArea(ByVal pstrCodCliente As String, _
    '    ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
    '    ByVal pstrFechaDesde As String) As DataTable
    '    Dim dtTemp As DataTable
    '    Try
    '        objRep = New LibCapaAccesoDatos.clsCADReporte
    '        With objRep
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodClieAdic = pstrCodClieAdic
    '            .strCodArea = pstrCodArea
    '            .strCodFechaDesde = pstrFechaDesde
    '            dtTemp = .gdtRepUnidadesExistenesxArea
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '            Return dtTemp
    '        End With
    '    Catch ex As Exception
    '        Me.strMensajeError = ex.Message
    '    End Try
    'End Function


    Public Function gdrRepUnidadesExistentesxArea(ByVal pstrCodCliente As String, _
        ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
        ByVal pstrFechaDesde As String, ByVal pstrUsuarioLogin As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRep = New LibCapaAccesoDatos.clsCADReporte
            With objRep
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodFechaDesde = pstrFechaDesde
                .strUsuarioLogin = pstrUsuarioLogin
                dtTemp = .gdtRepUnidadesExistenesxArea
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Return dtTemp
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    Public Function gdrRepPersonasAutorizadas(ByVal pstrCodCliente As String, _
           ByVal pstrNivelAutoridad As String, ByVal pstrCodArea As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRep = New LibCapaAccesoDatos.clsCADReporte
            With objRep
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strNivelAutoridad = pstrNivelAutoridad
                .strCodArea = pstrCodArea
                dtTemp = .gdtRepPersonasAutorizadas
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Return dtTemp
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    'Public Function gdrRepUnidadesVaciasPoderCliente(ByVal pstrCodCliente As String, _
    '        ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
    '        ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String) As DataTable
    '    Dim dtTemp As DataTable
    '    Try
    '        objRep = New LibCapaAccesoDatos.clsCADReporte
    '        With objRep
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodClieAdic = pstrCodClieAdic
    '            .strCodArea = pstrCodArea
    '            .strCodFechaDesde = pstrFechaDesde
    '            .strCodFechaHasta = pstrFechaHasta
    '            dtTemp = .gdtRepUnidadesVaciasPoderCliente
    '            Me.lintFilasAfectadas = .intFilasAfectadas
    '            Me.lstrErrorSql = .strErrorSql
    '            Return dtTemp
    '        End With
    '    Catch ex As Exception
    '        Me.strMensajeError = ex.Message
    '    End Try
    'End Function
    Public Function gdrRepUnidadesVaciasPoderCliente(ByVal pstrCodCliente As String, _
                ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
                ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String, ByVal pstrUsuarioLogin As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRep = New LibCapaAccesoDatos.clsCADReporte
            With objRep
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strUsuarioLogin = pstrUsuarioLogin
                dtTemp = .gdtRepUnidadesVaciasPoderCliente
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Return dtTemp
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    Public Function gdrRepStockxArea(ByVal pstrCodCliente As String, _
           ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
           ByVal pstrCodTipoItem As String, ByVal pstrCodTipo As String, _
           ByVal pstrFechaDesde As String, ByVal pstrFechaHasta As String) As DataTable
        Dim dtTemp As DataTable
        Try
            objRep = New LibCapaAccesoDatos.clsCADReporte
            With objRep
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodTipoItem = pstrCodTipoItem
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strCodTipo = pstrCodTipo
                dtTemp = .gdtRepStockxArea
                Me.lintFilasAfectadas = .intFilasAfectadas
                Me.lstrErrorSql = .strErrorSql
                Return dtTemp
            End With
        Catch ex As Exception
            Me.strMensajeError = ex.Message
        End Try
    End Function

    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property

    Public Sub New()
        lstrcodEmpresa = "01"
        lstrCodUnidad = "001"
    End Sub
End Class
