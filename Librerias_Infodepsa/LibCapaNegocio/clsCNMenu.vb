Imports System.Web
Imports System.Data
Imports System.Data.SqlClient

Public Class clsCNMenu
    Private lintCodModulo As Integer
    Private lstrCodGrupo As String
    Private lstrCodUsuario As String
    Private lstrErrorSql As String
    Private lintFilasAfectadas As Integer
    Private lstrMensajeError As String
    Private dtTemporal As DataTable

    Private objMenu As LibCapaAccesoDatos.clsCADMenu

    Public Function gdtConseguirPlantillaMarcadaMenu() As DataTable
        Try
            objMenu = New LibCapaAccesoDatos.clsCADMenu
            With objMenu
                .intCodModulo = lintCodModulo
                .strCodGrupo = lstrCodGrupo
                .strCodModalidad = 0
                dtTemporal = .gdtConseguirMenu
                lintFilasAfectadas = .intFilasAfectadas
            End With
            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function gdtConseguirMenuxGrupo() As DataTable
        Try
            objMenu = New LibCapaAccesoDatos.clsCADMenu
            With objMenu
                .intCodModulo = lintCodModulo
                .strCodGrupo = lstrCodGrupo
                .strCodModalidad = 1
                dtTemporal = .gdtConseguirMenu
                lintFilasAfectadas = .intFilasAfectadas
            End With
            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function gstrCadenaMenuAcceso() As String
        Dim strCadena As String = ""
        Dim intI As Integer = 0
        Try
            objMenu = New LibCapaAccesoDatos.clsCADMenu
            With objMenu
                .intCodModulo = lintCodModulo
                .strCodGrupo = lstrCodGrupo
                .strCodModalidad = 1
                dtTemporal = .gdtConseguirMenu
                lintFilasAfectadas = .intFilasAfectadas
            End With
            If lintFilasAfectadas > 0 Then
                While (intI < lintFilasAfectadas)
                    strCadena += "PAG" & dtTemporal.Rows(intI)("CO_MENU").ToString & ","
                    intI += 1
                End While
                strCadena = strCadena.Remove(strCadena.Length - 1, 1)
            End If
            Return strCadena
        Catch ex As Exception
            lstrMensajeError = ex.Message
            Return strCadena
        End Try
    End Function
    Public Function gstrConseguirPaginaInicio() As String
        Dim strRetorno As String = "SolicitarDocumento.aspx"
        Try
            objMenu = New LibCapaAccesoDatos.clsCADMenu
            With objMenu
                .intCodModulo = lintCodModulo
                .strCodGrupo = lstrCodGrupo
                .strCodModalidad = 2
                dtTemporal = .gdtConseguirMenu
                lintFilasAfectadas = .intFilasAfectadas
            End With
            If lintFilasAfectadas = 1 Then
                strRetorno = dtTemporal.Rows(0)("RU_MENU").ToString
            End If
            Return strRetorno
        Catch ex As Exception
            lstrMensajeError = ex.Message
            Return strRetorno
        End Try
    End Function

    Public Function gbolGrabarPlantillaMenu(ByVal objGrid As UI.WebControls.DataGrid) As Boolean
        Dim dgItem As UI.WebControls.DataGridItem
        Dim blnSele As Boolean
        Dim blnPagi As Boolean

        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objMenu = New LibCapaAccesoDatos.clsCADMenu
        objConexion = New SqlConnection(objMenu.strCadenaInicial)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            For Each dgItem In objGrid.Items
                blnSele = CType(dgItem.FindControl("chkSel"), UI.WebControls.CheckBox).Checked
                blnPagi = CType(dgItem.FindControl("chkPagi"), UI.WebControls.CheckBox).Checked
                With objMenu
                    .intCodModulo = lintCodModulo
                    .strCodGrupo = lstrCodGrupo
                    .intCodMenu = dgItem.Cells(5).Text
                    .strTipoSituacion = "ACT"
                    .strEstPaginaInicio = IIf(blnPagi, "S", "N")
                    .strCodUsuario = lstrCodUsuario
                    .strFlagSeleccion = IIf(blnSele, "S", "N")
                    If .gbolInsertarMenu(objTrans) = False Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Return False
                        Exit Function
                    End If
                End With
            Next
            objTrans.Commit()
            objTrans.Dispose()
        Catch ex As Exception
            Me.lstrMensajeError = ex.Message
            objTrans.Rollback()
            objTrans.Dispose()
            Return False
        End Try

    End Function

    Public Property intCodModulo() As Integer
        Get
            Return lintCodModulo
        End Get
        Set(ByVal Value As Integer)
            lintCodModulo = Value
        End Set
    End Property
    Public Property strCodGrupo() As String
        Get
            Return lstrCodGrupo
        End Get
        Set(ByVal Value As String)
            lstrCodGrupo = Value
        End Set
    End Property
    Public Property strErrorSql() As String
        Get
            Return lstrErrorSql
        End Get
        Set(ByVal Value As String)
            lstrErrorSql = Value
        End Set
    End Property
    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property strCodUsuario() As String
        Get
            Return lstrCodUsuario
        End Get
        Set(ByVal Value As String)
            lstrCodUsuario = Value
        End Set
    End Property
    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        dtTemporal = Nothing
    End Sub
End Class
