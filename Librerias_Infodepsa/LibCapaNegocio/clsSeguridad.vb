Imports System.Web

Public Class clsSeguridad
    Private lstrPaginaID As String
    Private lstrPagina As String
    Private lstrIDPaginasPermitidas As String
    Private lbolAutenticado As Boolean

    Public Function gbolValidaPagina() As Boolean
        Dim bolRetorno As Boolean = False
        If InStr(lstrIDPaginasPermitidas, lstrPaginaID) = False Then
            lstrPagina = "CerrarSesion.aspx?caduco=1"
            bolRetorno = False
        Else
            bolRetorno = True
        End If
        Return bolRetorno
    End Function
    Public Function gbolValidaSesion() As Boolean
        Dim bolRetorno As Boolean = False
        If lbolAutenticado = False Then
            lstrPagina = "CerrarSesion.aspx?caduco=1"
            bolRetorno = False
        Else
            bolRetorno = True
        End If
        Return bolRetorno
    End Function
    Public WriteOnly Property strIDPaginasPermitidas() As String
        Set(ByVal Value As String)
            lstrIDPaginasPermitidas = Value
        End Set
    End Property
    Public Property strPaginaID() As String
        Get
            Return lstrPaginaID
        End Get
        Set(ByVal Value As String)
            lstrPaginaID = Value
        End Set
    End Property
    Public ReadOnly Property strPagina() As String
        Get
            Return lstrPagina
        End Get
    End Property
    Public WriteOnly Property bolAutenticado() As Boolean
        Set(ByVal Value As Boolean)
            lbolAutenticado = Value
        End Set
    End Property

End Class
