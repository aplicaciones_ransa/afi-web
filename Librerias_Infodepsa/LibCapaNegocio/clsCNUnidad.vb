Imports System.Data

Public Class clsCNUnidad
    Private lstrcodEmpresa As String
    Private lstrCodUnidad As String
    Private lstrIdUnidad As String
    Private lstrCodTipoItem As String
    Private lstrDesTipoItem As String
    Private lstrCodArea As String
    Private lstrDesArea As String
    Private lstrNumPrecinto As String
    Private lstrUbicacion As String
    Private lintIDConsultaInicio As Double
    Private lintIDConsultaFinal As Double
    Private dtExcel As DataTable

    Private lintFilasAvance As Integer
    Private lintFilasAfectadas As Integer
    Private lstrErrorSql As String
    Private objUnidad As New LibCapaAccesoDatos.clsCADUnidad

    Public Function gdtMostrarDocumentos(ByVal pstrCodCliente As String, _
    ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
    ByVal pstrCodCriterio As String, ByVal pstrIDUnidad As String, _
    ByVal pstrNroDocumento As String, ByVal pstrFechaDesde As String, _
    ByVal pstrFechaHasta As String, ByVal pstrDescripcion As String) As DataTable
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodCriterio = pstrCodCriterio
                .strIDUnidad = pstrIDUnidad
                .strNroDocumento = pstrNroDocumento
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strDesDocumento = pstrDescripcion
                dtTemporal = .gdtConDocumentos()
                lintFilasAfectadas = .intFilasAfectadas
            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        
    End Function
    Public Function gdtMostrarDocumentosHP(ByVal pstrCodCliente As String, _
       ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
       ByVal pstrCodCriterio As String, ByVal pstrIDUnidad As String, _
       ByVal pstrNroDocumento As String, ByVal pstrFechaDesde As String, _
       ByVal pstrFechaHasta As String, ByVal pstrDescripcion As String, _
       ByVal pintIDConsulta As Double, ByVal pstrDireccionPagina As String, _
       Optional ByVal pstrEstTotalRegistros As String = "S", _
       Optional ByVal pstrUsuarioLogin As String = "") As DataTable
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodCriterio = pstrCodCriterio
                .strIDUnidad = pstrIDUnidad
                .strNroDocumento = pstrNroDocumento
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strDesDocumento = pstrDescripcion
                .intIDConsulta = pintIDConsulta
                .strDireccionPagina = pstrDireccionPagina
                .strEstTotalRegistros = pstrEstTotalRegistros
                .strUsuarioLogin = pstrUsuarioLogin
                dtTemporal = .gdtConDocumentosHP()
                lintFilasAfectadas = .intFilasAfectadas
                lintIDConsultaInicio = .intIDConsultaInicio
                lintIDConsultaFinal = .intIDConsultaFinal

            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function gdtConsultarDocumentosxUnidad(ByVal pstrCodCliente As String, _
    '    ByVal pstrCodClieAdic As String, ByVal pstrIDUnidad As String) As DataTable
    '    Dim dtTemporal As DataTable
    '    objUnidad = New LibCapaAccesoDatos.clsCADUnidad
    '    Try
    '        With objUnidad
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodClieAdic = pstrCodClieAdic
    '            .strIDUnidad = pstrIDUnidad
    '            dtTemporal = .gdtConDocumentoxUnidad
    '            lintFilasAfectadas = .intFilasAfectadas
    '        End With

    '        Return dtTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtConsultarDocumentosxUnidad(ByVal pstrCodCliente As String, _
        ByVal pstrCodClieAdic As String, ByVal pstrIDUnidad As String, Optional ByVal pstrUnidades As String = "") As DataTable
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strIDUnidad = pstrIDUnidad
                .strUnidades = pstrUnidades
                dtTemporal = .gdtConDocumentoxUnidad
                lintFilasAfectadas = .intFilasAfectadas
            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Sub gConseguirCabeceraUnidad(ByVal pstrCodCliente As String, _
        ByVal pstrCodClieAdic As String, ByVal pstrIDUnidad As String)
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strIDUnidad = pstrIDUnidad
                dtTemporal = .gdtConCabeceraUnidad
                lintFilasAfectadas = .intFilasAfectadas

                If .intFilasAfectadas = 1 Then
                    lstrIdUnidad = dtTemporal.Rows(0).Item(0)
                    lstrCodTipoItem = dtTemporal.Rows(0).Item(1)
                    lstrDesTipoItem = dtTemporal.Rows(0).Item(2)
                    lstrCodArea = dtTemporal.Rows(0).Item(3)
                    lstrDesArea = dtTemporal.Rows(0).Item(4)
                    lstrNumPrecinto = dtTemporal.Rows(0).Item(5)
                    lstrUbicacion = dtTemporal.Rows(0).Item(6)
                End If
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Function gdtConsultarDocumentosxArea(ByVal pstrCodCliente As String, _
            ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String) As DataTable
        Dim dtTemporal As DataTable

        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                '.strCodTipoItem = pstrCodTipoItem
                '.strCodFechaDesde = pstrFecha
                '.strCodTipo = pstrCodTipo
                '.strIDUnidad = pstrIdUnidad
                dtTemporal = .gdtConDocumentoxArea
                dtExcel = dtTemporal
                lintFilasAfectadas = .intFilasAfectadas
            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    'Public Function gdtMostrarUnidades(ByVal pstrCodCliente As String, _
    '   ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
    '   ByVal pstrIDUnidad As String, _
    '   ByVal pstrFechaDesde As String, _
    '   ByVal pstrFechaHasta As String, ByVal pstrCodTipo As String, ByVal pstrCodTipoItem As String) As DataTable
    '    Dim dtTemporal As DataTable
    '    objUnidad = New LibCapaAccesoDatos.clsCADUnidad
    '    Try
    '        With objUnidad
    '            .strCodCliente = pstrCodCliente
    '            .strCodEmpresa = lstrcodEmpresa
    '            .strCodUnidad = lstrCodUnidad
    '            .strCodClieAdic = pstrCodClieAdic
    '            .strCodArea = pstrCodArea
    '            .strIDUnidad = pstrIDUnidad
    '            .strCodFechaDesde = pstrFechaDesde
    '            .strCodFechaHasta = pstrFechaHasta
    '            .strCodTipo = pstrCodTipo
    '            .strCodTipoItem = pstrCodTipoItem
    '            dtTemporal = .gdtConUnidades()
    '            lintFilasAfectadas = .intFilasAfectadas
    '        End With

    '        Return dtTemporal
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    'End Function

    Public Function gdtMostrarUnidades(ByVal pstrCodCliente As String, _
       ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
       ByVal pstrIDUnidad As String, ByVal pstrFechaDesde As String, _
       ByVal pstrFechaHasta As String, ByVal pstrCodTipo As String, _
       ByVal pstrCodTipoItem As String, ByVal pstrUsuarioLogin As String, ByVal pstrPagina As Integer, ByVal pstrInicio As Integer) As DataTable
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strIDUnidad = pstrIDUnidad
                .strCodFechaDesde = pstrFechaDesde
                .strCodFechaHasta = pstrFechaHasta
                .strCodTipo = pstrCodTipo
                .strCodTipoItem = pstrCodTipoItem
                .strUsuarioLogin = pstrUsuarioLogin
                .strPagina = pstrPagina
                .intInicio = pstrInicio
                dtTemporal = .gdtConUnidades()
                lintFilasAfectadas = .intFilasAfectadas
                lintFilasAvance = .intFilasAvance
            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function

    Public Function gdtConsultarUnidadxArea(ByVal pstrCodCliente As String, _
        ByVal pstrCodClieAdic As String, ByVal pstrCodArea As String, _
        ByVal pstrCodTipoItem As String, Optional ByVal pstrFecha As String = "", _
        Optional ByVal pstrCodTipo As String = "N", Optional ByVal pstrIdUnidad As String = "") As DataTable
        Dim dtTemporal As DataTable
        objUnidad = New LibCapaAccesoDatos.clsCADUnidad
        Try
            With objUnidad
                .strCodCliente = pstrCodCliente
                .strCodEmpresa = lstrcodEmpresa
                .strCodUnidad = lstrCodUnidad
                .strCodClieAdic = pstrCodClieAdic
                .strCodArea = pstrCodArea
                .strCodTipoItem = pstrCodTipoItem
                .strCodFechaDesde = pstrFecha
                .strCodTipo = pstrCodTipo
                .strIDUnidad = pstrIdUnidad
                dtTemporal = .gdtConUnidadxArea
                lintFilasAfectadas = .intFilasAfectadas
            End With

            Return dtTemporal
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    Public Sub New()
        lintFilasAfectadas = 0
        lstrErrorSql = "0"
        lstrcodEmpresa = "01"
        lstrCodUnidad = "001"
    End Sub

    Public Property strUbicacion() As String
        Get
            Return lstrUbicacion
        End Get
        Set(ByVal Value As String)
            lstrUbicacion = Value
        End Set
    End Property
    Public Property strNumPrecinto() As String
        Get
            Return lstrNumPrecinto
        End Get
        Set(ByVal Value As String)
            lstrNumPrecinto = Value
        End Set
    End Property
    Public Property strDesArea() As String
        Get
            Return lstrDesArea
        End Get
        Set(ByVal Value As String)
            lstrDesArea = Value
        End Set
    End Property
    Public Property strCodArea() As String
        Get
            Return lstrCodArea
        End Get
        Set(ByVal Value As String)
            lstrCodArea = Value
        End Set
    End Property
    Public Property strDesTipoItem() As String
        Get
            Return lstrDesTipoItem
        End Get
        Set(ByVal Value As String)
            lstrDesTipoItem = Value
        End Set
    End Property
    Public Property strCodTipoItem() As String
        Get
            Return lstrCodTipoItem
        End Get
        Set(ByVal Value As String)
            lstrCodTipoItem = Value
        End Set
    End Property
    Public Property strIdUnidad() As String
        Get
            Return lstrIdUnidad
        End Get
        Set(ByVal Value As String)
            lstrIdUnidad = Value
        End Set
    End Property
    Public Property intIDConsultaInicio() As Double
        Get
            Return lintIDConsultaInicio
        End Get
        Set(ByVal Value As Double)
            lintIDConsultaInicio = Value
        End Set
    End Property
    Public Property intIDConsultaFinal() As Double
        Get
            Return lintIDConsultaFinal
        End Get
        Set(ByVal Value As Double)
            lintIDConsultaFinal = Value
        End Set
    End Property


    Public Property intFilasAfectadas() As Integer
        Get
            Return lintFilasAfectadas
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public Property intFilasAvance() As Integer
        Get
            Return lintFilasAvance
        End Get
        Set(ByVal Value As Integer)
            lintFilasAfectadas = Value
        End Set
    End Property
    Public ReadOnly Property dtExcelTemp() As DataTable 'Se agrego la propiedad
        Get
            Return dtExcel
        End Get
    End Property
End Class
