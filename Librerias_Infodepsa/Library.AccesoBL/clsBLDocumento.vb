Imports Library.AccesoDB
Public Class clsBLDocumento
    Private objDocumento As Documento = New Documento

    Public Function BLGetKardex(ByVal strDepositante As String, ByVal strEstado As String, _
                                         ByVal strCodEntidad As String, ByVal strTipoMercaderia As String, _
                                         ByVal strNroWarrant As String, ByVal strTipoEntidad As String) As DataTable
        Return objDocumento.DBGetKardex(strDepositante, strEstado, strCodEntidad, strTipoMercaderia, strNroWarrant, strTipoEntidad)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
