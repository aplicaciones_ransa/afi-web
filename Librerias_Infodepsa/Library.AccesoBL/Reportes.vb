Imports System.Data.SqlClient
Imports Root.Reports
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports Library.AccesoDB
Imports System.Web.HttpContext

Public Class Reportes
    Public LstrMensaje As String
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strRutaExe As String = System.Configuration.ConfigurationManager.AppSettings("RutaExe")
    Private strPDFPath As String = System.Configuration.ConfigurationManager.AppSettings.Item("RutaPDFs")


    Private objDocumento As Documento = New Documento
    Private strCodDoc As String
    Public LstrNombrePDF As String
    Private strDes() As String
    Public LstrNombreExternoPDF As String
    Public GrabarPDFxDocumento As Boolean

    Public Sub GeneraPDFWarrant(ByVal strIds As String, ByVal strPathPDF As String, _
                                ByVal strPathFirmas As String, ByVal blnEndoso As Boolean, _
                                ByVal blnFirmas As Boolean, ByVal strTipoDocumento As String, _
                                ByVal strUsuario As String, ByVal strDblEndoso As String, _
                                Optional ByVal strNroWarrants As String = "")
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objDocumento = New Library.AccesoDB.Documento
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            If strTipoDocumento = "03" Then
                objDocumento.InsWarrantMultiple(strIds, strUsuario, strNroWarrants, strDblEndoso, objTrans)
                strCodDoc = objDocumento.strCodDocWar
                'objDocumento.strResultado = "0"
                If objDocumento.strResultado = "0" Then
                    If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso, False, strTipoDocumento) <> "0" Then
                        LstrMensaje = "No se pudo Crear el PDF"
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Exit Sub
                    End If
                ElseIf objDocumento.strResultado = "1" Then
                    LstrMensaje = "Alguno de los parametros no se hallaron"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                ElseIf objDocumento.strResultado = "2" Then
                    LstrMensaje = "No se pudo crear el registro en la BD"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                ElseIf objDocumento.strResultado = "3" Then
                    LstrMensaje = "No se pudo actualizar los Warrants referenciados"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                ElseIf objDocumento.strResultado = "4" Then
                    LstrMensaje = "El o Los documentos seleccionados ya fueron Emitidos"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                End If
            End If
            If strTipoDocumento = "01" Or strTipoDocumento = "10" Then
                If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso, False, strTipoDocumento, strIds) <> "0" Then
                    LstrMensaje = "No se pudo Crear el PDF"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                End If
                strCodDoc = strIds
            End If
            If strTipoDocumento = "04" Then
                If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso, False, strTipoDocumento, strIds) <> "0" Then
                    LstrMensaje = "No se pudo Crear el PDF"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                End If
                strCodDoc = strIds
            End If
            If strTipoDocumento = "12" Then
                If GetReporteWar(strIds, strPathPDF, strPathFirmas, True, blnFirmas, objTrans, strDblEndoso, False, strTipoDocumento, strIds) <> "0" Then
                    LstrMensaje = "No se pudo Crear el PDF"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                End If
                strCodDoc = strIds
            End If
            If strTipoDocumento = "24" Then
                If GetReporte_SolicitudWarrant(strIds, strPathPDF, strPathFirmas, objTrans, strTipoDocumento, strNroWarrants) <> "0" Then
                    LstrMensaje = "No se pudo Crear el PDF"
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                Else
                    If LstrNombreExternoPDF <> "" Then
                        Dim Parametro As String
                        Parametro = strPathPDF & "_" & LstrNombrePDF & " " & LstrNombreExternoPDF & " " & strPathPDF & LstrNombrePDF
                        Dim myProcess As Process = System.Diagnostics.Process.Start(strRutaExe, Parametro)
                        If myProcess.WaitForExit(60000) = False Then
                            objTrans.Rollback()
                            objTrans.Dispose()
                            LstrMensaje = "No se pudo generar la solicitud, adjunte otro pdf"
                            Exit Sub
                        End If
                        myProcess.Close()
                    End If
                End If
                strCodDoc = strIds
            End If
            GrabaPDF(strCodDoc, strPathPDF, blnFirmas, objTrans)
            If objDocumento.strResultado <> "0" Then
                LstrMensaje = "No se pudo guardar el PDF"
                objTrans.Rollback()
                objTrans.Dispose()
                Exit Sub
            End If
            objTrans.Commit()
            objTrans.Dispose()
            LstrMensaje = "0"
        Catch ex As Exception
            Dim str As String = ex.Message
            objTrans.Rollback()
            objTrans.Dispose()
            LstrMensaje = "No se pudo generar el Warrant"
        End Try
    End Sub

    Public Sub GeneraPDFSolicitud(ByVal strIds As String, ByVal strPathPDF As String, _
                               ByVal strPathFirmas As String, ByVal blnEndoso As Boolean, _
                               ByVal blnFirmas As Boolean, ByVal strTipoDocumento As String, _
                               ByVal strUsuario As String, ByVal strDblEndoso As String, ByVal ObjTrans As SqlTransaction, _
                               Optional ByVal strNroWarrants As String = "", Optional ByVal strDireccion As String = "")
        Try
            If GetReporte_SolicitudWarrant(strIds, strPathPDF, strPathFirmas, ObjTrans, strTipoDocumento, strNroWarrants, strDireccion) <> "0" Then
                LstrMensaje = "No se pudo Crear el PDF"
                Exit Sub
            End If
            strCodDoc = strIds
            GrabaPDF(strCodDoc, strPathPDF, blnFirmas, ObjTrans)
            If objDocumento.strResultado <> "0" Then
                LstrMensaje = "No se pudo guardar el PDF"
                Exit Sub
            End If
            LstrMensaje = "0"
        Catch ex As Exception
            LstrMensaje = "No se pudo generar el Warrant " & ex.Message
        End Try
    End Sub

    Public Sub GeneraPDFWarrantFirmasXDocumento(ByVal strCodDoc As String, ByVal strIds As String, _
                                                ByVal strPathPDF As String, ByVal strPathFirmas As String, _
                                                ByVal blnEndoso As Boolean, ByVal blnFirmas As Boolean, _
                                                ByVal strUsuario As String, ByVal strTipoDocumento As String, _
                                                ByVal strNombrePDF As String, ByVal objTrans As SqlTransaction)
        If objDocumento.gGetEstadoImpresion(strCodDoc, objTrans) = True Then
            Exit Sub
        End If
        Dim strDblEndoso As String
        If strTipoDocumento = "03" Or strTipoDocumento = "20" Then
            strDblEndoso = "0"
        End If
        If strTipoDocumento = "10" Then
            strDblEndoso = "1"
        End If
        '------------Indicamos que vamos a guardar el PDF x numero de documento  -------
        GrabarPDFxDocumento = True
        '-----------------------------------------
        If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso,
                        False, strTipoDocumento, strCodDoc, strNombrePDF) <> "0" Then
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        LstrNombrePDF = strNombrePDF
        GrabaPDF(strIds, strPathPDF, blnFirmas, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraPDFWarrantFirmas(ByVal strCodDoc As String, ByVal strIds As String, ByVal strPathPDF As String, _
                                ByVal strPathFirmas As String, ByVal blnEndoso As Boolean, _
                                ByVal blnFirmas As Boolean, ByVal strUsuario As String, _
                                ByVal strTipoDocumento As String, ByVal objTrans As SqlTransaction)
        If objDocumento.gGetEstadoImpresion(strCodDoc, objTrans) = True Then
            Exit Sub
        End If
        Dim strDblEndoso As String
        If strTipoDocumento = "03" Or strTipoDocumento = "20" Then
            strDblEndoso = "0"
        End If
        If strTipoDocumento = "10" Then
            strDblEndoso = "1"
        End If
        '------------Indicamos que vamos a guardar el PDF x numero de documento  -------
        GrabarPDFxDocumento = False
        '-----------------------------------------
        If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso, False, strTipoDocumento, strCodDoc) <> "0" Then
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strCodDoc, strPathPDF, blnFirmas, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraDCR(ByVal strCodDoc As String, ByVal strPathPDF As String,
                        ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String, ByVal strIdUsuario As String,
                        ByVal objTrans As SqlTransaction)
        If objDocumento.gGetEstadoImpresion(strCodDoc, objTrans) = True Then 'ok(sin sessiones internas)
            Exit Sub
        End If
        If GetReportePDF(strCodDoc, strPathPDF, strPathFirmas, blnFirmas, strTipDoc, strIdUsuario, objTrans) <> "0" Then 'ok(sin sessiones internas)
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strCodDoc, strPathPDF, blnFirmas, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraVB(ByVal strCodDoc As String, ByVal strPathPDF As String, _
                       ByVal strPathFirmas As String, ByVal strCodUsuario As String, _
                       ByVal strPaso As String, ByVal objTrans As SqlTransaction)
        If objDocumento.gGetEstadoImpresionVB(strCodDoc, strCodUsuario, objTrans) = True And strPaso = "1" Then
            LstrMensaje = "Revisar la configuraci�n del usuario"
            Exit Sub
        End If
        Dim strDCR As String
        strDCR = GetVBPDF(strCodDoc, strPathPDF, strPathFirmas, strCodUsuario, strPaso, objTrans)
        If strDCR = "X" Then
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strDCR, strPathPDF, 0, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraSello(ByVal strCodDoc As String, ByVal strPathPDF As String, _
                       ByVal strPathFirmas As String, ByVal objTrans As SqlTransaction)
        Dim strLiberacion As String
        strLiberacion = GetSelloPDF(strCodDoc, strPathPDF, strPathFirmas, objTrans)
        If strLiberacion = "X" Then
            LstrMensaje = ""
            'objTrans.Rollback()
            'objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strLiberacion, strPathPDF, 1, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Function GetSelloPDF(ByVal strCod As String, ByVal strPathPDF As String, _
                          ByVal strPathFirmas As String, ByVal ObjTrans As SqlTransaction) As String
        Dim dtFirmantes As DataTable
        Dim Imagen1 As String = String.Empty

        dtFirmantes = objDocumento.GetSello(strCod, ObjTrans)
        If dtFirmantes.Rows.Count = 0 Then
            Return "X"
        End If

        LstrNombrePDF = dtFirmantes.Rows(0)("NOM_PDF")

        Dim pdfNuevo As String = strPathPDF & LstrNombrePDF

        Dim pdfOriginal As Byte() = CType(dtFirmantes.Rows(0)("DOC_PDFFINA"), Byte())
        Dim reader As PdfReader = New PdfReader(pdfOriginal)
        Dim document As document = New document(reader.GetPageSizeWithRotation(1))
        Dim fs As FileStream = New FileStream(pdfNuevo, FileMode.Create, FileAccess.Write)

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, fs)
        document.Open()

        Dim cb As PdfContentByte = writer.DirectContent
        document.NewPage()
        Dim Page As PdfImportedPage = writer.GetImportedPage(reader, 1)
        cb.AddTemplate(Page, 1, 0, 0, 1, 0, 0)
        Dim strVB As String = dtFirmantes.Rows(0)("FE_SALI_ALMA")
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)

        cb.SetRGBColorStroke(255, 0, 0)
        cb.Circle(490, 662, 45)
        cb.Stroke()
        cb.BeginText()
        cb.SetColorFill(iTextSharp.text.Color.RED)
        cb.SetFontAndSize(bf, 8)
        cb.ShowTextAligned(1, "CONTABILIZADO", 490, 680, 0)
        cb.ShowTextAligned(1, strVB, 490, 667, 0)
        cb.ShowTextAligned(1, "ADMINISTRACI�N", 490, 654, 0)
        cb.ShowTextAligned(1, "DOCUMENTARIA", 491, 641, 0)
        cb.EndText()

        For i As Integer = 2 To reader.NumberOfPages
            document.SetPageSize(reader.GetPageSizeWithRotation(i))
            document.NewPage()
            Page = writer.GetImportedPage(reader, i)
            cb.AddTemplate(Page, 1, 0, 0, 1, 0, 0)
        Next
        document.Close()
        fs.Close()
        writer.Close()
        reader.Close()
        Return dtFirmantes.Rows(0)("COD_DOC")
    End Function

    Public Function GetVBPDF(ByVal strCod As String, ByVal strPathPDF As String, _
                           ByVal strPathFirmas As String, ByVal strCodUsuario As String, _
                           ByVal strPaso As String, ByVal ObjTrans As SqlTransaction) As String
        Dim dtFirmantes As DataTable
        Dim Imagen1 As String = String.Empty

        dtFirmantes = objDocumento.GetDCR(strCod, strCodUsuario, ObjTrans)
        If dtFirmantes.Rows.Count = 0 Then
            Return "X"
        End If
        If dtFirmantes.Rows(0)("COD_TIPDOC") = "19" Then
            LstrNombrePDF = dtFirmantes.Rows(0)("NOM_PDF")
        Else
            LstrNombrePDF = "DCR" & dtFirmantes.Rows(0)("NOM_PDF")
        End If

        Dim pdfNuevo As String = strPathPDF & LstrNombrePDF
        Dim pdfOriginal As Byte() = CType(dtFirmantes.Rows(0)("DOC_PDFORIG"), Byte())
        Dim reader As PdfReader = New PdfReader(pdfOriginal)
        Dim size As Rectangle = reader.GetPageSizeWithRotation(1)
        Dim document As document = New document(size)

        Dim fs As FileStream = New FileStream(pdfNuevo, FileMode.Create, FileAccess.Write)
        Dim writer As PdfWriter = PdfWriter.GetInstance(document, fs)
        document.Open()

        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorFill(iTextSharp.text.Color.BLACK)
        cb.SetFontAndSize(bf, 8)

        cb.BeginText()
        Dim strVB As String = dtFirmantes.Rows(0)("VB_DEPSA2")
        If strPaso = "1" Then
            cb.ShowTextAligned(1, strVB, 60, 240, 0)
        End If
        If strPaso = "2" Then
            cb.ShowTextAligned(1, strVB, 60, 230, 0)
        End If
        cb.EndText()

        Dim Page As PdfImportedPage = writer.GetImportedPage(reader, 1)
        cb.AddTemplate(Page, 0, 0)

        document.Close()
        fs.Close()
        writer.Close()
        reader.Close()

        Return dtFirmantes.Rows(0)("COD_DOC")
    End Function

    Public Sub GeneraRetiro(ByVal strCodDoc As String, ByVal strPathPDF As String, _
                    ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String, _
                    ByVal objTrans As SqlTransaction)

        If strTipDoc <> "23" Then
            If objDocumento.gGetEstadoImpresion(strCodDoc, objTrans) = True Then  'ok(sin sessiones internas)
                Exit Sub
            End If
        End If
        'GrabaPDF(strCodDoc, strPathPDF, False, objTrans)
        'If objDocumento.strResultado <> "0" Then
        '    LstrMensaje = "No se pudo guardar el PDF"
        '    objTrans.Rollback()
        '    objTrans.Dispose()
        '    Exit Sub
        'End If
        If GetReporteRetiroPDF(strCodDoc, strPathPDF, strPathFirmas, blnFirmas, strTipDoc, objTrans) <> "0" Then 'ok(sin sessiones internas)
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strCodDoc, strPathPDF, blnFirmas, objTrans) 'ok(sin sessiones internas)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraWarrantCustodia(ByVal strCodDoc As String, ByVal strPathPDF As String, _
                    ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String, _
                    ByVal objTrans As SqlTransaction)
        If objDocumento.gGetEstadoImpresion(strCodDoc, objTrans) = True Then
            Exit Sub
        End If
        If GetReporteWarrantCustodiaPDF(strCodDoc, strPathPDF, strPathFirmas, blnFirmas, strTipDoc, objTrans) <> "0" Then
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
        GrabaPDF(strCodDoc, strPathPDF, blnFirmas, objTrans)
        If objDocumento.strResultado <> "0" Then
            LstrMensaje = "No se pudo guardar el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Public Sub GeneraPDFWarrantCopia(ByVal strCodDoc As String, ByVal strIds As String, ByVal strPathPDF As String, _
                                ByVal strPathFirmas As String, ByVal blnEndoso As Boolean, _
                                ByVal blnFirmas As Boolean, ByVal strUsuario As String, _
                                ByVal strTipoDocumento As String, ByVal objTrans As SqlTransaction)
        Dim strDblEndoso As String
        If strTipoDocumento = "01" Or strTipoDocumento = "04" Then
            strDblEndoso = "0"
        End If
        If strTipoDocumento = "10" Then
            strDblEndoso = "1"
        End If
        If GetReporteWar(strIds, strPathPDF, strPathFirmas, blnEndoso, blnFirmas, objTrans, strDblEndoso, True, strTipoDocumento, strCodDoc) <> "0" Then
            LstrMensaje = "No se pudo Crear el PDF"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        End If
    End Sub

    Private Sub GrabaPDF(ByVal strCodDoc As String, ByVal strPathPDF As String, ByVal blnFirmas As Boolean, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strPathPDF & LstrNombrePDF)
        FilePath = strPathPDF & strNombre
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gGrabaContenidoWarrantMultiple(strCodDoc, Contenido, blnFirmas, ObjTrans) 'ok(sin sessiones internas)
        fs.Close()
    End Sub

    Public Property strMensaje() As String
        Get
            Return LstrMensaje
        End Get
        Set(ByVal Value As String)
            LstrMensaje = Value
        End Set
    End Property

    Public Function GetReporte_SolicitudWarrant(ByVal strIds As String, ByVal strPathPDF As String, _
                                           ByVal strPathFirmas As String, ByVal ObjTrans As SqlTransaction, _
                                           ByVal strTipoDocumento As String, Optional ByVal strNroWarrants As String = "", _
                                           Optional ByVal strDireccion As String = "") As String
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_arial As FontDef = New FontDef(report, "Arial")
        Dim fp_hc As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_pi As FontProp = New FontPropMM(fd_arial, 1.9)
        Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.0)
        Dim fp As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.4)
        Dim fp_TALMA As FontProp = New FontPropMM(fd, 4.5)
        Dim fp_header As FontProp = New FontPropMM(fd, 3.5)
        Dim fp_e As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_h As FontProp = New FontPropMM(fd, 1.9)
        Dim fp_p As FontProp = New FontPropMM(fd, 2.4)
        Dim pp As PenProp = New PenProp(report, 0.8)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.6, System.Drawing.Color.LightGray)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 5.5, System.Drawing.Color.LightGray)
        fp_Certificacion.bBold = True
        fp_p.bBold = True
        fp_e.bBold = True
        Dim dtEndoso As Data.DataTable
        Dim dtEndoso2 As Data.DataTable
        Dim stream_Firma As Stream
        Dim page As page
        Dim intPaginas As Decimal
        Dim intTotal As Decimal
        Dim intTotal2 As Decimal
        Dim intXLeft As Integer
        Dim intYTop As Integer
        Dim intYTopLinea As Integer = 0
        Dim dtResumen As Data.DataTable
        Dim mes As String

        dtEndoso2 = objDocumento.GetInformacionSolictudWarrant("01", strIds, ObjTrans)
        LstrNombrePDF = Trim(dtEndoso2.Rows(0)(4))
        dtEndoso = objDocumento.SelPdfpintaSolWar_Det(strIds, ObjTrans)

        'Pinto la primera hoja
        If dtEndoso.Rows.Count > 13 Then
            If dtEndoso.Rows.Count / 13 - CInt(dtEndoso.Rows.Count / 13) > 0 Then
                intPaginas = CInt(dtEndoso.Rows.Count / 13) + 1
            Else
                intPaginas = CInt(dtEndoso.Rows.Count / 13)
            End If
        Else
            intPaginas = 1
        End If

        fp_header.bBold = True
        fp_TALMA.bBold = True
        Dim intItem As Integer = 0

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New page(report)
            page.rWidthMM = 210
            page.rHeightMM = 297
            intYTop = 80
            intXLeft = 20

            'ENCABEZADO
            'page.Add(intXLeft + 55, intYTop - 25, New RepString(fp_Certificacion, "DIRECCION   :   " & dtEndoso2.Rows(0)(3)))
            'page.Add(intXLeft + 55, intYTop - 12, New RepString(fp_Certificacion, "FINANCIERO :   " & dtEndoso2.Rows(0)(2)))
            If dtEndoso2.Rows(0)("NRO_GARA") <> "" Or strNroWarrants <> "" Then
                page.Add(intXLeft + 370, intYTop, New RepString(fp_p, "Solicitud para Reemplazo: " & dtEndoso2.Rows(0)("NRO_DOCU")))
            Else
                page.Add(intXLeft + 370, intYTop, New RepString(fp_p, "Nro Solicitud : " & dtEndoso2.Rows(0)("NRO_DOCU")))
            End If

            Select Case Now.Month
                Case 1
                    mes = "enero"
                Case 2
                    mes = "febrero"
                Case 3
                    mes = "marzo"
                Case 4
                    mes = "abril"
                Case 5
                    mes = "mayo"
                Case 6
                    mes = "junio"
                Case 7
                    mes = "julio"
                Case 8
                    mes = "agosto"
                Case 9
                    mes = "setiembre"
                Case 10
                    mes = "octubre"
                Case 11
                    mes = "noviembre"
                Case 12
                    mes = "diciembre"
            End Select
            page.Add(intXLeft + 55, intYTop + 40, New RepString(fp, "Lima, " & Now.Day.ToString & " de " & mes & " de " & Now.Year.ToString))
            page.Add(intXLeft + 55, intYTop + 70, New RepString(fp, "Se�ores:"))
            page.Add(intXLeft + 55, intYTop + 85, New RepString(fp, "ALMACENERA DEL PER� S.A."))
            page.Add(intXLeft + 55, intYTop + 100, New RepString(fp, "Lima.-"))
            page.Add(intXLeft + 55, intYTop + 130, New RepString(fp, "Atenci�n: Ing. Gualberto Mariategui - Gerente de Operaciones"))
            page.Add(intXLeft + 55, intYTop + 145, New RepString(fp, "Asunto: Solicitamos Warrant y Certificado de Dep�sito"))
            page.Add(intXLeft + 55, intYTop + 180, New RepString(fp, "De nuestra consideraci�n"))
            page.Add(intXLeft + 55, intYTop + 210, New RepString(fp, "De conformidad con la Legislaci�n Peruana sobre Warrant y Certificado vigentes y el Reglamento Interno de"))
            page.Add(intXLeft + 55, intYTop + 225, New RepString(fp, "''ALMACENERA DEL PER� S.A.'', solicitamos a Uds., se sirvan recibir en dep�sito la mercader�a que se detalla a continuaci�n"))
            If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                page.Add(intXLeft + 55, intYTop + 240, New RepString(fp, "para la emisi�n de un WARRANT en reemplazo del Warrant " & dtEndoso2.Rows(0)("NRO_GARA") & " a la orden de " & dtEndoso2.Rows(0)(1)))
            Else
                page.Add(intXLeft + 55, intYTop + 240, New RepString(fp, "para la emisi�n de un WARRANT a la orden de " & dtEndoso2.Rows(0)(1)))
            End If

            If strDireccion = "" Then
                page.Add(intXLeft + 55, intYTop + 255, New RepString(fp, "Almac�n : " & dtEndoso2.Rows(0)("DE_ALMA")))
            Else
                page.Add(intXLeft + 55, intYTop + 255, New RepString(fp, "Almac�n : " & strDireccion))
            End If

            page.Add(intXLeft + 55, intYTop + 270, New RepString(fp, "Financiador : " & dtEndoso2.Rows(0)("NOM_FINA")))

            page.AddMM(intXLeft + 5, 130, New RepLineMM(ppBold, 160, 0)) '-------------------------
            page.Add(intXLeft + 130, intYTop + 300, New RepString(fp_pi, "PRODUCTO"))
            page.Add(intXLeft + 273, intYTop + 300, New RepString(fp_pi, "UNI.MEDI"))
            page.Add(intXLeft + 328, intYTop + 300, New RepString(fp_pi, "CNT."))
            page.Add(intXLeft + 367, intYTop + 300, New RepString(fp_pi, "MON."))
            page.Add(intXLeft + 408, intYTop + 300, New RepString(fp_pi, "C.U."))
            page.Add(intXLeft + 460, intYTop + 300, New RepString(fp_pi, "TOTAL"))
            page.AddMM(intXLeft + 5, 136, New RepLineMM(ppBold, 160, 0)) '-------------------------

            page.AddMM(intXLeft + 5, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 82, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 97, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 115, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 125, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 143, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 165, 210, New RepLineMM(ppBold, 0, 80))
            page.AddMM(intXLeft + 5, 210, New RepLineMM(ppBold, 160, 0))

            If (p - 1) * (13) + 13 < dtEndoso.Rows.Count Then
                intPaginas = (p - 1) * (13) + 13
            Else
                intPaginas = dtEndoso.Rows.Count
            End If

            For j As Int16 = (p - 1) * (13) To intPaginas - 1
                If intItem <> dtEndoso.Rows(j)("COD_ITEM") Then
                    If Not IsDBNull(dtEndoso.Rows(j)("DSC_MERC")) Then
                        page.Add(intXLeft + 55, intYTop + 320, New RepString(fp_pi, dtEndoso.Rows(j)("DSC_MERC")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("UNI_MEDI")) Then
                        page.Add(intXLeft + 272, intYTop + 320, New RepString(fp_pi, dtEndoso.Rows(j)("UNI_MEDI")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("CANT_SOLI")) Then
                        page.AddRight(intXLeft + 355, intYTop + 320, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("CANT_SOLI"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("SB_MONE")) Then
                        page.AddRight(intXLeft + 383, intYTop + 320, New RepString(fp_pi, dtEndoso.Rows(j)("SB_MONE")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("PREC_MERC")) Then
                        page.AddRight(intXLeft + 435, intYTop + 320, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("PREC_MERC"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("CANT_TOTAL")) Then
                        page.AddRight(intXLeft + 497, intYTop + 320, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("CANT_TOTAL"))))
                        intTotal += dtEndoso.Rows(j)("CANT_TOTAL")
                    End If
                Else
                    If Not IsDBNull(dtEndoso.Rows(j)("DSC_MERC")) Then
                        page.Add(intXLeft + 55, intYTop + 320, New RepString(fp_pi, dtEndoso.Rows(j)("DSC_MERC")))
                    End If
                End If
                intItem = dtEndoso.Rows(j)("COD_ITEM")
                intYTop += 15
            Next
            intYTop = 90
            page.Add(intXLeft + 340, intYTop + 515, New RepString(fp_pi, "TOTAL : "))
            page.AddRight(intXLeft + 497, intYTop + 515, New RepString(fp_pi, dtEndoso.Rows(0)("SB_MONE") & " " & String.Format("{0:##,##0.00}", intTotal)))

            page.Add(intXLeft + 55, intYTop + 540, New RepString(fp, "Declaro bajo juramento que esta mercader�a es de mi legitima propiedad, que se encuentra libre de todo gravamen,"))
            page.Add(intXLeft + 55, intYTop + 555, New RepString(fp, "embargo o medida restrictiva para su libre disposici�n; y que los precios o valores declarados son los reales, hacien-"))
            page.Add(intXLeft + 55, intYTop + 570, New RepString(fp, "dome en todo caso responsable civil y penalmente por la falta de veracidad de esta declaraci�n. As� mismo me"))
            page.Add(intXLeft + 55, intYTop + 585, New RepString(fp, "comprometo a cumplir con todas las formalidades y disposiciones establecidas por los dispositivos legales vigentes"))
            page.Add(intXLeft + 55, intYTop + 600, New RepString(fp, "sin cargo alguno para ustedes."))

            page.Add(intXLeft + 55, intYTop + 630, New RepString(fp, "Sin otro particular, nos reiteramos de ustedes."))
            page.Add(intXLeft + 55, intYTop + 655, New RepString(fp, "Atentamente."))
            page.Add(intXLeft + 100, intYTop + 690, New RepString(fp, dtEndoso2.Rows(0)(1)))
        Next

        If LstrNombreExternoPDF = "" Then
            RT.ViewPDF(report, strPathPDF & LstrNombrePDF)
        Else
            RT.ViewPDF(report, strPathPDF & "_" & LstrNombrePDF)
        End If

        If dtEndoso.Rows.Count = 0 Then
            Return "1"
        End If
        dtEndoso = Nothing
        dtResumen = Nothing
        Return "0"
    End Function

    Public Function GetReporte_Inventario(ByVal strCodUsuario As String, ByVal strCodCliente As String, _
                                            ByVal strCodAlmacen As String, ByVal strCodUnidad As String, _
                                            ByVal strNroComprobante As String, ByVal strPathPDF As String, _
                                            ByVal strFecha As String, ByVal strNroSolicitud As String, _
                                            ByVal strNroWarrant As String, ByVal ObjTrans As SqlTransaction) As String
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_arial As FontDef = New FontDef(report, "Arial")
        Dim fp_hc As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_pi As FontProp = New FontPropMM(fd_arial, 1.9)
        Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.0)
        Dim fp As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 3.5)
        Dim fp_header As FontProp = New FontPropMM(fd, 3.5)
        Dim fp_TALMA As FontProp = New FontPropMM(fd, 3.5)
        Dim fp_e As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_h As FontProp = New FontPropMM(fd, 1.9)
        Dim fp_p As FontProp = New FontPropMM(fd, 2.4)
        Dim pp As PenProp = New PenProp(report, 0.8)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.6, System.Drawing.Color.LightGray)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 5.5, System.Drawing.Color.LightGray)
        fp_Certificacion.bBold = True
        fp_p.bBold = True
        fp_e.bBold = True
        fp_Titulo.bBold = True
        Dim stream_Firma As Stream
        Dim page As page
        Dim intPaginas As Decimal
        Dim intTotal As Decimal
        Dim intTotal2 As Decimal
        Dim intXLeft As Integer
        Dim intYTop As Integer
        Dim intYTopLinea As Integer = 0
        Dim mes As String
        Dim dtResumen As New DataTable

        dtResumen = objDocumento.gGetSaldo(strCodCliente, strCodAlmacen, strCodUnidad, strNroComprobante, ObjTrans)
        Dim drDE_MERC As DataRow
        Dim sDE_MERC As String
        Dim dtSaldo As DataTable
        dtSaldo = dtResumen.Clone()
        'rows = dtNew.Select("", "NU_DOCU_RECE ASC, NU_SECU ASC")
        For Each dr As DataRow In dtResumen.Rows
            sDE_MERC = dr("DE_MERC")
            If dr("DE_MERC").ToString.Length <= 50 Then
                dtSaldo.ImportRow(dr)
            ElseIf dr("DE_MERC").ToString.Length <= 100 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(17) = sDE_MERC.ToString.Substring(0, 50)
                dr.EndEdit()
                dtSaldo.ImportRow(dr)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(50)
                dtSaldo.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 150 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(17) = sDE_MERC.ToString.Substring(0, 50)
                dr.EndEdit()
                dtSaldo.ImportRow(dr)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(50, 50)
                dtSaldo.Rows.Add(drDE_MERC)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(100)
                dtSaldo.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length > 150 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(17) = sDE_MERC.ToString.Substring(0, 50)
                dr.EndEdit()
                dtSaldo.ImportRow(dr)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(50, 50)
                dtSaldo.Rows.Add(drDE_MERC)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(100, 50)
                dtSaldo.Rows.Add(drDE_MERC)
                drDE_MERC = dtSaldo.NewRow
                drDE_MERC(17) = sDE_MERC.ToString.Substring(150)
                dtSaldo.Rows.Add(drDE_MERC)
            End If
            'dtResumen.ImportRow(dr)
        Next
        LstrNombrePDF = "DCR" & dtSaldo.Rows(0)("NUMERO_RECEPCION")

        'Pinto la primera hoja
        If dtSaldo.Rows.Count > 24 Then
            If dtSaldo.Rows.Count / 24 - CInt(dtSaldo.Rows.Count / 24) > 0 Then
                intPaginas = CInt(dtSaldo.Rows.Count / 24) + 1
            Else
                intPaginas = CInt(dtSaldo.Rows.Count / 24)
            End If
        Else
            intPaginas = 1
        End If

        fp_header.bBold = True
        fp_TALMA.bBold = True
        Dim intItem As Integer = 0

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New page(report)
            page.rWidthMM = 210
            page.rHeightMM = 297
            page.SetLandscape()
            intYTop = 60
            intXLeft = 20

            page.AddCB(intYTop, New RepString(fp_Titulo, "INVENTARIO FISICO"))
            page.Add(intXLeft + 250, intYTop + 20, New RepString(fp, "ALMACEN                    : " & dtSaldo.Rows(0)("DE_ALMA")))
            page.Add(intXLeft + 250, intYTop + 30, New RepString(fp, "CLIENTE                      : " & dtSaldo.Rows(0)("NO_CLIE_REPO")))
            page.Add(intXLeft + 250, intYTop + 40, New RepString(fp, "MODALIDAD                : " & dtSaldo.Rows(0)("DE_MODA_MOVI") & " - " & strNroWarrant))
            page.Add(intXLeft + 250, intYTop + 50, New RepString(fp, "FECHA DE INGRESO  : " & dtSaldo.Rows(0)("FECHA_INGRESO")))
            page.Add(intXLeft + 720, intYTop + 10, New RepString(fp, "P�g: " & p))
            page.Add(intXLeft + 720, intYTop + 20, New RepString(fp, strFecha))
            page.Add(intXLeft + 20, intYTop + 10, New RepString(fp, strCodUsuario))

            page.AddMM(10, 43, New RepLineMM(ppBold, 280, 0)) '-------------------------
            page.Add(intXLeft + 10, intYTop + 70, New RepString(fp_pi, "BODEGA"))
            page.Add(intXLeft + 45, intYTop + 70, New RepString(fp_pi, "UBICACION"))
            page.Add(intXLeft + 90, intYTop + 70, New RepString(fp_pi, "ITEM"))
            page.Add(intXLeft + 115, intYTop + 70, New RepString(fp_pi, "CANTIDAD"))
            page.Add(intXLeft + 160, intYTop + 70, New RepString(fp_pi, "UNIDAD"))
            page.Add(intXLeft + 195, intYTop + 70, New RepString(fp_pi, "CANTIDAD"))
            page.Add(intXLeft + 240, intYTop + 70, New RepString(fp_pi, "BULTO"))
            page.Add(intXLeft + 270, intYTop + 70, New RepString(fp_pi, "CODIGO"))
            page.Add(intXLeft + 320, intYTop + 70, New RepString(fp_pi, "DESCRIPCION DE MERCADERIA"))
            page.Add(intXLeft + 555, intYTop + 70, New RepString(fp_pi, "REFERENCIA"))
            page.Add(intXLeft + 625, intYTop + 70, New RepString(fp_pi, "FECHA PROD."))
            page.Add(intXLeft + 680, intYTop + 70, New RepString(fp_pi, "FECHA VENC."))
            page.Add(intXLeft + 735, intYTop + 70, New RepString(fp_pi, "ESTADO"))
            page.AddMM(10, 47, New RepLineMM(ppBold, 280, 0)) '-------------------------

            If (p - 1) * (24) + 24 < dtSaldo.Rows.Count Then
                intPaginas = (p - 1) * (24) + 24
            Else
                intPaginas = dtSaldo.Rows.Count
            End If

            For j As Int16 = (p - 1) * (24) To intPaginas - 1
                If IsDBNull(dtSaldo.Rows(j)("NUMERO_RECEPCION")) Then
                    page.Add(intXLeft + 320, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("DE_MERC")))
                Else
                    If Not IsDBNull(dtSaldo.Rows(j)("CO_BODE_ACTU")) Then
                        page.Add(intXLeft + 15, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("CO_BODE_ACTU")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CO_UBIC_ACTU")) Then
                        page.Add(intXLeft + 50, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("CO_UBIC_ACTU")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("NU_SECU")) Then
                        page.AddRight(intXLeft + 100, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("NU_SECU")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CANTIDAD_UNIDAD")) Then
                        page.AddRight(intXLeft + 155, intYTop + 85, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtSaldo.Rows(j)("CANTIDAD_UNIDAD"))))
                        intTotal += dtSaldo.Rows(j)("CANTIDAD_UNIDAD")
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CO_TIPO_BULT")) Then
                        page.Add(intXLeft + 165, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("CO_TIPO_BULT")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CANTIDAD_BULTO")) Then
                        page.AddRight(intXLeft + 235, intYTop + 85, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtSaldo.Rows(j)("CANTIDAD_BULTO"))))
                        intTotal2 += dtSaldo.Rows(j)("CANTIDAD_BULTO")
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CO_UNME_PESO")) Then
                        page.Add(intXLeft + 245, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("CO_UNME_PESO")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("CO_PROD_CLIE")) Then
                        page.Add(intXLeft + 270, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("CO_PROD_CLIE")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("DE_MERC")) Then
                        page.Add(intXLeft + 320, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("DE_MERC")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("REFERENCIA")) Then
                        page.Add(intXLeft + 555, intYTop + 85, New RepString(fp_pi, Left(dtSaldo.Rows(j)("REFERENCIA"), 20)))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("FCH_PROD")) Then
                        page.Add(intXLeft + 633, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("FCH_PROD")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("FECHA_VCTO_MERCADERIA")) Then
                        page.Add(intXLeft + 685, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("FECHA_VCTO_MERCADERIA")))
                    End If
                    If Not IsDBNull(dtSaldo.Rows(j)("DE_ESTA_MERC")) Then
                        page.Add(intXLeft + 735, intYTop + 85, New RepString(fp_pi, dtSaldo.Rows(j)("DE_ESTA_MERC")))
                    End If
                End If
                'intItem = dtSaldo.Rows(j)("NU_SECU")
                intYTop += 10
            Next
            'page.AddMM(10, 134, New RepLineMM(ppBold, 280, 0))
            page.Add(intXLeft + 10, intYTop + 290, New RepLineMM(ppBold, 280, 0))
            'intYTop = 90
            page.Add(intXLeft + 40, intYTop + 300, New RepString(fp_pi, "TOTALES : "))
            page.AddRight(intXLeft + 155, intYTop + 300, New RepString(fp_pi, String.Format("{0:##,##0.00}", intTotal)))
            page.AddRight(intXLeft + 235, intYTop + 300, New RepString(fp_pi, String.Format("{0:##,##0.00}", intTotal2)))

            page.Add(intXLeft + 55, intYTop + 320, New RepString(fp, "AREA                      : "))
            page.AddRight(intXLeft + 200, intYTop + 320, New RepString(fp, String.Format("{0:##,##0.00}", dtSaldo.Rows(0)("NU_AREA_USAD"))))

            page.Add(intXLeft + 55, intYTop + 330, New RepString(fp, "VOLUMEN              : "))
            page.AddRight(intXLeft + 200, intYTop + 330, New RepString(fp, String.Format("{0:##,##0.00}", dtSaldo.Rows(0)("NU_VOLU_USAD"))))

            page.Add(intXLeft + 55, intYTop + 340, New RepString(fp, "OBSERVACIONES : PARA EMISI�N DE NUEVO WARRANT A SOLICITUD DEL CLIENTE"))
            page.Add(intXLeft + 485, 550, New RepString(fp, "SUPERVISOR"))
            page.Add(intXLeft + 635, 550, New RepString(fp, "ALMACENERO"))
            page.AddMM(intXLeft + 150, 190, New RepLineMM(ppBold, 35, 0))
            page.AddMM(intXLeft + 205, 190, New RepLineMM(ppBold, 35, 0))
            'page.Add(intXLeft + 55, 550, New RepString(fp, "DE-R-0012-GO"))
        Next

        RT.ViewPDF(report, strPathPDF & LstrNombrePDF & ".PDF")
        Dim dtResultado As DataTable
        Dim strResultado As String
        'dtResultado = objDocumento.InsInventario(dtSaldo.Rows(0)("CO_UNID"), LstrNombrePDF, "\\161.132.96.91\SOPORTE\", dtSaldo.Rows(0)("NUMERO_RECEPCION"), dtSaldo.Rows(0)("CO_ALMA"), dtSaldo.Rows(0)("CO_CLIE_ACTU"), strNroSolicitud, ObjTrans)
        dtResultado = objDocumento.InsInventario(dtSaldo.Rows(0)("CO_UNID"), LstrNombrePDF, strPathPDF, dtSaldo.Rows(0)("NUMERO_RECEPCION"), dtSaldo.Rows(0)("CO_ALMA"), dtSaldo.Rows(0)("CO_CLIE_ACTU"), strNroSolicitud, ObjTrans)
        strResultado = dtResultado.Rows(0)(2)
        dtSaldo = Nothing
        Return dtResultado.Rows(0)(1)
    End Function

    Public Function GetReporteWar(ByVal strIds As String, ByVal strPathPDF As String, _
                            ByVal strPathFirmas As String, ByVal blnEndoso As Boolean, _
                            ByVal blnFirmas As Boolean, ByVal ObjTrans As SqlTransaction, _
                            ByVal strDblEndoso As String, ByVal blnCopia As Boolean, _
                            ByVal strTipoDocumento As String, Optional ByVal strCodPadre As String = "", _
                            Optional ByVal strNomPdf As String = "") As String
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fp_hc As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_pi As FontProp = New FontPropMM(fd, 1.9)
        Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.0)
        Dim fp As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_header As FontProp = New FontPropMM(fd, 3.5)
        Dim fp_TALMA As FontProp = New FontPropMM(fd, 4.5)
        Dim fp_e As FontProp = New FontPropMM(fd, 1.8)
        Dim fp_h As FontProp = New FontPropMM(fd, 1.9)
        Dim fp_p As FontProp = New FontPropMM(fd, 2.8)
        Dim pp As PenProp = New PenProp(report, 0.8)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.6, System.Drawing.Color.LightGray)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 5.5, System.Drawing.Color.LightGray)
        Dim fp_SelloProtesto As FontProp = New FontProp(fd, 7, System.Drawing.Color.Red)
        fp_Certificacion.bBold = True
        fp_p.bBold = True
        fp_e.bBold = True
        fp_pi.bBold = True
        Dim dtEndoso As DataTable
        Dim dtEndoso2 As DataTable
        Dim dtFirmantes As DataTable
        Dim intY As Integer
        Dim intX As Integer = 14
        Dim strBanco As String
        Dim strOfi As String
        Dim strCta As String
        Dim strDC As String
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim strProtesto As String
        Dim strFirmaClie1 As String
        Dim strFirmaClie2 As String
        Dim strFirmaClie3 As String
        Dim strFirmaClieAsociado1 As String
        Dim strFirmaClieAsociado2 As String
        Dim strFirmaClieAsociado3 As String
        Dim strFirmaFina1 As String
        Dim strFirmaFina2 As String
        Dim strFirmaFina3 As String
        Dim strDireccionAlmacen As String
        Dim strDepsa1 As String
        Dim strDepsa2 As String
        Dim page As page
        Dim strDirCliente As String
        Dim strNroFolio As String
        Dim strFechaFolio As String
        Dim strNumber2 As String
        Dim stream_Firma As Stream
        Dim rPosLeft As Double = 49
        Dim strError As String
        Dim pivot As Integer
        Dim intPaginas As Decimal
        Dim idsunit As String() = Split(strIds, "-")
        Dim K As Integer
        Dim intXLeft As Integer = 15
        Dim intXRigth As Integer
        Dim intYTop As Integer
        Dim intYBotton As Integer
        Dim var As Integer
        Dim xnum As String
        Dim number As String
        If strCodPadre <> "" And blnEndoso = True Then
            dtFirmantes = objDocumento.GetFirmantes(strCodPadre, ObjTrans)
            If dtFirmantes.Rows(0)("FIR_CLIE1") = "" Then
                strFirmaClie1 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClie1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE1")
            End If
            If dtFirmantes.Rows(0)("FIR_CLIE2") = "" Then
                strFirmaClie2 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClie2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE2")
            End If
            If dtFirmantes.Rows(0)("FIR_CLIE3") = "" Then
                strFirmaClie3 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClie3 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE3")
            End If
            If dtFirmantes.Rows(0)("FIR_CLIE_ASOC1") = "" Then
                strFirmaClieAsociado1 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClieAsociado1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE_ASOC1")
            End If
            If dtFirmantes.Rows(0)("FIR_CLIE_ASOC2") = "" Then
                strFirmaClieAsociado2 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClieAsociado2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE_ASOC2")
            End If
            If dtFirmantes.Rows(0)("FIR_CLIE_ASOC3") = "" Then
                strFirmaClieAsociado3 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaClieAsociado3 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE_ASOC3")
            End If
            If dtFirmantes.Rows(0)("FIR_FINA1") = "" Then
                strFirmaFina1 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaFina1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_FINA1")
            End If
            If dtFirmantes.Rows(0)("FIR_FINA2") = "" Then
                strFirmaFina2 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaFina2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_FINA2")
            End If
            If dtFirmantes.Rows(0)("FIR_FINA3") = "" Then
                strFirmaFina3 = strPathFirmas & "aspl.JPG"
            Else
                strFirmaFina3 = strPathFirmas & dtFirmantes.Rows(0)("FIR_FINA3")
            End If
            If dtFirmantes.Rows(0)("FIR_DEPSA1") = "" Then
                strDepsa1 = strPathFirmas & "aspl.JPG"
            Else
                strDepsa1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA1")
            End If
            If dtFirmantes.Rows(0)("FIR_DEPSA2") = "" Then
                strDepsa2 = strPathFirmas & "aspl.JPG"
            Else
                strDepsa2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA2")
            End If
        End If
        'For i As Integer = 0 To idsunit.Length - 1
        If blnEndoso = True Then
            'Leo la informaci�n del endoso
            dtEndoso2 = objDocumento.SelPdfpintarendo(strCodPadre, ObjTrans)
            If IsDBNull(dtEndoso2.Rows(0)("FCH_FOLIO")) Then
                    strFechaFolio = ""
                Else
                    strFechaFolio = dtEndoso2.Rows(0)("FCH_FOLIO")
                End If
                If dtEndoso2.Rows(0)("NRO_FOLIO") = "" Or dtEndoso2.Rows(0)("NRO_FOLIO") = "0" Then
                    strNroFolio = ""
                Else
                    strNroFolio = "Nro Folio: " & dtEndoso2.Rows(0)("NRO_FOLIO")
                End If
                If dtEndoso2.Rows(0)("FLG_PROT") Then
                    strProtesto = "WARRANT SIN PROTESTO"
                Else
                    strProtesto = ""
                End If
            End If
            page = New page(report)
            page.rWidthMM = 210
            page.rHeightMM = 314
            intYTop = 30
        'Leo la informaci�n de SICO
        dtEndoso = objDocumento.GetInformacionWarrantSICO("01", "DEPSARED", strCodPadre, ObjTrans)
        LstrNombrePDF = objDocumento.strNombrePDF
            If objDocumento.strResultado <> "0" Then
                Return objDocumento.strResultado
            End If
            '----------------------------------------------------------------------------------------------------------------
            If objDocumento.strTipoDocumentoGenerado = "12" And objDocumento.strResultado = "0" Then
                'Pinto la primera hoja del warant
                intPaginas = (((dtEndoso.Rows.Count - 17) / 50) + 2)
                intPaginas = Fix(intPaginas)
            fp_header.bBold = True
            fp_TALMA.bBold = True

            page.Add(intXLeft + 220, intYTop - 10, New RepString(fp_header, "CERTIFICADO DE DEPOSITO"))
            page.Add(intXLeft + 220, intYTop, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))

            page.Add(intXLeft + 220, intYTop + 10, New RepString(fp_hc, "Certificado de Dep�sito con Warrant emitido"))
            page.Add(intXLeft + 220, intYTop + 20, New RepString(fp_hc, "Certificado de Dep�sito sin Warrant emitido"))
            page.Add(intXLeft + 380, intYTop + 11, New RepRectMM(ppBold, 4, 4))
            page.Add(intXLeft + 380, intYTop + 25, New RepRectMM(ppBold, 4, 4))

            If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                    page.Add(intXLeft + 383, intYTop + 9, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                    page.Add(intXLeft + 383, intYTop + 23, New RepString(fp_Certificacion, "X"))
                End If
                If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                    strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                Else
                    strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                End If

                If blnCopia = True Then
                    page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                    page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                End If

                'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                page.Add(intXLeft + 420, intYTop + 10, New RepString(fp_pi, "   N�   " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                intYTop += 30
                page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                page.Add(intXLeft + 91, 164, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 174, New RepString(fp, "con  D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                              " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                page.Add(intXLeft + 70, 175, New RepString(fp_pi, "_______        __________________________________"))
                page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                page.Add(intXLeft + 91, 197, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                  " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                page.Add(intXLeft + 150, 231, New RepString(fp_pi, "_____________________________________________________________________________________________"))
                intYTop -= 10
                page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                If intPaginas = 1 Then
                    page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                    page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                    page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                    page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                End If


            If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) ' 12 certificado de Deposito - ok
            End If
            page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
            page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                If dtEndoso.Rows.Count <= 16 Then
                    K = dtEndoso.Rows.Count - 1
                Else
                    K = 15
                End If
                'Pinto el detalle de los items
                For j As Integer = 0 To K
                    If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                        page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                        page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                        page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                        page.Add(rPosLeft + 347, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                        page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                        page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                    End If
                    intYTop += 10
                Next

                xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                number = NumPalabra(Val(xnum))
                intYTop = 312
                If intPaginas > 2 Then
                    strNumber2 = "02"
                Else
                    strNumber2 = ""
                End If
                page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))
                '-------------------------------------------
                If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                Else
                    page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                End If
                '-------------------------------------------
                page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))
                '--------------------------------------------
                If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                Else
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                End If
                '--------------------------------------------
                page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                intYTop += 25
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                    page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
            page.Add(intXLeft + 40, (intYTop + 350) / 2.84 + 455, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
            If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                    page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
            'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-056-GW"))

            If blnFirmas = True Then
                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                End If

                'Pinto el Anexo al Warrat si es que tiene
                If intPaginas > 1 Then
                    For j As Integer = 2 To intPaginas
                        page = New page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intYTop = 110

                    page.Add(210, 20, New RepString(fp_header, "ANEXO AL WARRANT"))
                    page.Add(217, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                    page.Add(217, 50, New RepString(fp_header, "Warrant con certificado de Dep�sito emitido"))
                        page.Add(217, 60, New RepString(fp_header, "Warrant sin certificado de Dep�sito emitido"))
                        page.Add(352, 51, New RepRectMM(ppBold, 4, 4))
                        page.Add(352, 65, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(354, 49, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(354, 63, New RepString(fp_Certificacion, "X"))
                        End If
                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                        End If
                        'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                        page.Add(365, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(383, 41, New RepString(fp_pi, "___________________________________"))
                        page.Add(125, 140, New RepString(fp, "Pagina N�       " & j & ""))
                        page.Add(165, 141, New RepString(fp_pi, "_________________"))
                        page.Add(40, 200, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(350, 201, New RepString(fp_pi, "_______________________________"))
                        page.Add(320, 217, New RepString(fp, "     " & dtEndoso.Rows(0)("DIA TITULO") & "             de      " & dtEndoso.Rows(0)("MES TITULO") & "         de             " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                        page.Add(325, 218, New RepString(fp_pi, "__________"))
                        page.Add(390, 218, New RepString(fp_pi, "____________"))
                        page.Add(462, 218, New RepString(fp_pi, "_____________"))
                    page.Add(380, 228, New RepString(fp, " ALMACENERA DEL PER�"))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 150, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                        page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 160, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                    'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 175, New RepString(fp, "DE-R-061-GW"))
                    If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(22, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(60, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(110, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        End If

                        page.Add(42, intYTop + 236, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la pagina N� "))
                        page.Add(400, intYTop + 237, New RepString(fp_pi, "_________________"))
                        page.Add(intXLeft + 30, intYTop + 250, New RepString(fp_hc, "Cantidad"))
                        page.Add(intXLeft + 84, intYTop + 250, New RepString(fp_hc, "Unidad y/o peso"))
                        page.Add(intXLeft + 145, intYTop + 250, New RepString(fp_hc, "Descripcion de los bienes (clases,especie)y marcas de los bultos"))
                        page.Add(intXLeft + 370, intYTop + 246, New RepString(fp_hc, "Calidad y estado"))
                        page.Add(intXLeft + 370, intYTop + 256, New RepString(fp_hc, "de conservacion"))
                        page.Add(intXLeft + 430, intYTop + 246, New RepString(fp_hc, "Valor patrimonial con indicacion"))
                        page.Add(intXLeft + 430, intYTop + 256, New RepString(fp_hc, "del criterio de valorizacion"))
                        page.Add(intXLeft + 115, intYTop + 690, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))

                        page.AddMM(intXLeft, intYTop + 13, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intXLeft, intYTop + 20, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.AddMM(intXLeft + 19, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.AddMM(intXLeft + 39, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.AddMM(intXLeft + 120, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.AddMM(intXLeft + 140, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.AddMM(intXLeft + 180, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                        page.Add(intXLeft + 115, intYTop + 680, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                        page.Add(intXLeft + 115, intYTop + 700, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                        If j = intPaginas Then
                            page.AddMM(intX + 140, intYTop + 170, New RepLineMM(ppBold, 40, 0)) '----
                            page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                            page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                            page.AddRight(rPosLeft + 420 + 30, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                        End If

                        If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                            var = (j - 1) * (50) + 15
                        Else
                            var = (dtEndoso.Rows.Count - 1)
                        End If
                        For m As Integer = (j - 2) * (50) + 16 To var
                            If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                            Else
                                If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                    page.Add(intXLeft + 30, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                    page.Add(intXLeft + 84, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                    page.Add(intXLeft + 145, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                    page.Add(intXLeft + 370, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                    page.Add(intXLeft + 430, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                    page.AddRight(intXLeft + 510, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                End If
                                intYTop += 8
                            End If
                        Next
                    Next
                End If
                If blnEndoso = True Then
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 10

                    page.Add(240, intYTop + 10, New RepString(fp_p, "PRIMER ENDOSO"))
                    'page.Add(400, intYTop + 10, New RepString(fp_p, strProtesto))
                    page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                    page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                    'page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREFINANCIERA")))
                    page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                    page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                    page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                    'page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("Direc_FINA")))
                    page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                    page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                    'If dtEndoso2.Rows(0)("RUC") <> "" Then
                    '    page.Add(400, intYTop + 47, New RepString(fp_Certificacion, "R.U.C."))
                    'End If
                    page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                    page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                    'page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("RUC")))
                    page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                    page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                    'If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 40 Then
                    '    page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length)))
                    'End If
                    'If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 40 And CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 120 Then
                    '    page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 40)))
                    '    page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(40, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 40)))
                    'End If
                    'If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 120 Then
                    '    page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 40)))
                    '    page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(40, 80)))
                    '    page.Add(40, intYTop + 80, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(120, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 120)))
                    'End If
                    page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                    page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                    'page.Add(135, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("SIM_MONE") & " " & String.Format("{0:##,##0.00}", dtEndoso2.Rows(0)("VAL_ARCH2"))))
                    page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                    page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                    'page.Add(482, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("VEN_DOCU")))
                    page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                    page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                    page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                    'If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 26 Then
                    '    page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length)))
                    'End If
                    'If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 26 And CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 130 Then
                    '    page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                    '    page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length - 26)))
                    'End If
                    'If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 130 Then
                    '    page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                    '    page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, 102)))
                    'End If
                    page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                    page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                    page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                    intYTop += 20
                    page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    intYTop -= 20
                    page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))

                    'If dtEndoso2.Rows(0)("BAN_ENDO").ToString.Length > 25 Then
                    '    strBanco = dtEndoso2.Rows(0)("BAN_ENDO").ToString.Substring(0, 25)
                    'Else
                    '    strBanco = dtEndoso2.Rows(0)("BAN_ENDO")
                    'End If
                    'If dtEndoso2.Rows(0)("OFI_ENDO").ToString.Length > 20 Then
                    '    strOfi = dtEndoso2.Rows(0)("OFI_ENDO").ToString.Substring(0, 20)
                    'Else
                    '    strOfi = dtEndoso2.Rows(0)("OFI_ENDO")
                    'End If
                    'If dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Length > 15 Then
                    '    strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Substring(0, 15)
                    'Else
                    '    strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO")
                    'End If
                    'If dtEndoso2.Rows(0)("DC_ENDO").ToString.Length > 10 Then
                    '    strDC = dtEndoso2.Rows(0)("DC_ENDO").ToString.Substring(0, 10)
                    'Else
                    '    strDC = dtEndoso2.Rows(0)("DC_ENDO")
                    'End If
                    page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                    page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                    page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                    page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                    page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                    page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                    page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                    page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                    page.Add(60, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO"))
                    page.Add(100, intYTop + 250, New RepString(fp_Titulo, "DE DEPOSITO"))

                    'If dtEndoso2.Rows(0)("FLG_DBLENDO") Then
                    '    Dia = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Day)
                    '    Mes = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Month)
                    '    Anio = "0000" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Year)
                    '    Select Case Mes.Substring(Mes.Length - 2)
                    '        Case "01"
                    '            Mes = "Enero"
                    '        Case "02"
                    '            Mes = "Febrero"
                    '        Case "03"
                    '            Mes = "Marzo"
                    '        Case "04"
                    '            Mes = "Abril"
                    '        Case "05"
                    '            Mes = "Mayo"
                    '        Case "06"
                    '            Mes = "Junio"
                    '        Case "07"
                    '            Mes = "Julio"
                    '        Case "08"
                    '            Mes = "Agosto"
                    '        Case "09"
                    '            Mes = "Setiembre"
                    '        Case "10"
                    '            Mes = "Octubre"
                    '        Case "11"
                    '            Mes = "Noviembre"
                    '        Case "12"
                    '            Mes = "Diciembre"
                    '    End Select

                    '    page.Add(44, intYTop + 327, New RepString(fp, "Toca y Pertenece a "))
                    '    If dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Length > 40 Then
                    '        page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(0, 40)))
                    '        page.Add(44, intYTop + 347, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(40)))
                    '    Else
                    '        page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                    '    End If
                    '    'page.Add(44, intYTop + 342, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                    '    page.Add(44, intYTop + 357, New RepString(fp, "Lima, " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)))

                    '    If blnFirmas = True Then
                    '        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                    '        stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '        stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                    '        stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '        stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '    End If
                    '    page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                    '    page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                    '    page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                    'Else
                    '    page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                    '    page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                    '    page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                    '    If blnFirmas = True And strTipoDocumento <> "20" Then
                    '        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '    End If
                    'End If

                    'If dtEndoso2.Rows(0)("FLG_EMBA") Then
                    '    page.Add(44, intYTop + 440, New RepString(fp, "Endoso para embarque a favor de los se�ores"))
                    '    page.Add(44, intYTop + 452, New RepString(fp, dtEndoso2.Rows(0)("NOMBREALMACENERA").ToString))
                    '    If blnFirmas = True Then
                    '        stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(15, 190, New RepImageMM(stream_Firma, Double.NaN, 23))

                    '        stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '        page.AddMM(50, 190, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '    End If
                    'End If

                    'If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length > 40 Then
                    '    page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, 40)))
                    'End If
                    'If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length <= 40 Then
                    '    page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length)))
                    'End If

                    page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                    intYTop += 10
                    fp_Titulo.bBold = True

                    page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                    page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                    page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                    page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                    page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                    page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                    '-------------------------------------------------------------------------------------
                    'If blnFirmas = True Then
                    '    stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '    page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                    '    stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    '    page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                    'End If
                    intYTop += 10
                    page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                    page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                    page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                    page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                    page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))
                'page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("RUC")))
                page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso ha  quedado  registrado en su  matr�cula o libro  talonario,  as� como"))
                page.Add(280, intYTop + 475, New RepString(fp_pi, "en el  respectivo  Certificado de  Dep�sito,  en el caso de haber sido emitido."))
                '--------------------------------------------------------------------------------------
                page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                    page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                    page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                    page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                    page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                    page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                page.Add(380, intYTop + 600, New RepString(fp, "p. ALMACENERA DEL PER�"))
                page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                    page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                    '--------------------------------------------------------------------------------------
                    page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                    page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                    intYTop += 10
                    page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                    page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                    page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                    page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                    page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                    page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                    page.Add(460, intYTop + 654, New RepString(fp, "Firma del Endosatario"))

                    page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                    page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                    page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                    page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(480, intYTop + 662, New RepString(fp, "Del Warrant"))
                    'If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                    '    page.Add(350, 850, New RepString(fp_pi, "N� de Garantia: " & dtEndoso2.Rows(0)("NRO_GARA")))
                    'End If
                    'If blnCopia = True Then
                    '    page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                    '    page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    'End If
                End If
                If CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                    'Pinto la primera hoja del warant
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                    intPaginas = (((dtEndoso.Rows.Count - 17) / 50) + 2)
                    intPaginas = Fix(intPaginas)
                    fp_header.bBold = True

                page.Add(intXLeft + 260, intYTop - 10, New RepString(fp_header, "WARRANT"))
                page.Add(intXLeft + 220, intYTop, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                page.Add(intXLeft + 220, intYTop + 10, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                page.Add(intXLeft + 220, intYTop + 20, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                page.Add(intXLeft + 380, intYTop + 11, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 380, intYTop + 25, New RepRectMM(ppBold, 4, 4))

                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(intXLeft + 383, intYTop + 9, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(intXLeft + 383, intYTop + 23, New RepString(fp_Certificacion, "X"))
                    End If
                    If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                    ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                    Else
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                    End If

                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If

                    'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(intXLeft + 420, intYTop + 10, New RepString(fp_pi, "   N�   " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                    intYTop += 30
                    page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                    page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                    page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                    page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                    page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                    page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                    page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(intXLeft + 91, 164, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 174, New RepString(fp, "con  D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                              " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(intXLeft + 70, 175, New RepString(fp_pi, "_______        __________________________________"))
                    page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                    page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                    page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(intXLeft + 91, 197, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                    page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                    page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                    page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                  " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(intXLeft + 150, 231, New RepString(fp_pi, "_____________________________________________________________________________________________"))
                    intYTop -= 10
                    page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                    page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                    page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                    page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                    page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                    If intPaginas = 1 Then
                        page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                        page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                        page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If



                If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                    page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) ' 12 certificado deposito -- ok
                End If
                page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If dtEndoso.Rows.Count <= 16 Then
                        K = dtEndoso.Rows.Count - 1
                    Else
                        K = 15
                    End If
                    'Pinto el detalle de los items
                    For j As Integer = 0 To K
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 347, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intYTop += 10
                    Next

                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))
                    intYTop = 312
                    If intPaginas > 2 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                    page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                    page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))
                    '-------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                    End If
                    '-------------------------------------------
                    page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))
                    '--------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                    End If
                    '--------------------------------------------
                    page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intYTop += 25
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                    page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                    page.Add(intXLeft + 40, (intYTop + 350) / 2.84 + 455, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                        page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                    page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-056-GW"))

                If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    End If

                    'Pinto el Anexo al Warrat si es que tiene
                    If intPaginas > 1 Then
                        For j As Integer = 2 To intPaginas
                            page = New page(report)
                            page.rWidthMM = 210
                            page.rHeightMM = 314
                            intYTop = 110

                        page.Add(210, 20, New RepString(fp_header, "ANEXO AL WARRANT"))
                        page.Add(207, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                        page.Add(207, 50, New RepString(fp_header, "Warrant con certificado de Dep�sito emitido"))
                        page.Add(207, 60, New RepString(fp_header, "Warrant sin certificado de Dep�sito emitido"))
                        page.Add(342, 51, New RepRectMM(ppBold, 4, 4))
                        page.Add(342, 65, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                            ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(344, 49, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(344, 63, New RepString(fp_Certificacion, "X"))
                        End If
                            If blnCopia = True Then
                                page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                                page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                            End If
                            'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                            page.Add(365, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(383, 41, New RepString(fp_pi, "___________________________________"))
                            page.Add(125, 140, New RepString(fp, "Pagina N�       " & j & ""))
                            page.Add(165, 141, New RepString(fp_pi, "_________________"))
                            page.Add(40, 200, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(350, 201, New RepString(fp_pi, "_______________________________"))
                            page.Add(320, 217, New RepString(fp, "     " & dtEndoso.Rows(0)("DIA TITULO") & "             de      " & dtEndoso.Rows(0)("MES TITULO") & "         de             " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                            page.Add(325, 218, New RepString(fp_pi, "__________"))
                            page.Add(390, 218, New RepString(fp_pi, "____________"))
                            page.Add(462, 218, New RepString(fp_pi, "_____________"))
                        page.Add(400, 228, New RepString(fp, " ALMACENERA DEL PER�"))
                        page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 150, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                            page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 160, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                        'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 175, New RepString(fp, "DE-R-061-GW"))
                        If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(22, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(60, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(110, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            End If

                            page.Add(42, intYTop + 236, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la pagina N� "))
                            page.Add(400, intYTop + 237, New RepString(fp_pi, "_________________"))
                            page.Add(intXLeft + 30, intYTop + 250, New RepString(fp_hc, "Cantidad"))
                            page.Add(intXLeft + 84, intYTop + 250, New RepString(fp_hc, "Unidad y/o peso"))
                            page.Add(intXLeft + 145, intYTop + 250, New RepString(fp_hc, "Descripcion de los bienes (clases,especie)y marcas de los bultos"))
                            page.Add(intXLeft + 370, intYTop + 246, New RepString(fp_hc, "Calidad y estado"))
                            page.Add(intXLeft + 370, intYTop + 256, New RepString(fp_hc, "de conservacion"))
                            page.Add(intXLeft + 430, intYTop + 246, New RepString(fp_hc, "Valor patrimonial con indicacion"))
                            page.Add(intXLeft + 430, intYTop + 256, New RepString(fp_hc, "del criterio de valorizacion"))
                            page.Add(intXLeft + 115, intYTop + 690, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))

                            page.AddMM(intXLeft, intYTop + 13, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, intYTop + 20, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 19, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 39, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 120, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 140, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 180, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.Add(intXLeft + 115, intYTop + 680, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                            page.Add(intXLeft + 115, intYTop + 700, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                            If j = intPaginas Then
                                page.AddMM(intX + 140, intYTop + 170, New RepLineMM(ppBold, 40, 0)) '----
                                page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                                page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                                page.AddRight(rPosLeft + 420 + 30, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                            End If

                            If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                                var = (j - 1) * (50) + 15
                            Else
                                var = (dtEndoso.Rows.Count - 1)
                            End If
                            For m As Integer = (j - 2) * (50) + 16 To var
                                If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                Else
                                    If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                        page.Add(intXLeft + 30, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                        page.Add(intXLeft + 84, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                        page.Add(intXLeft + 145, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 370, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 430, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                        page.AddRight(intXLeft + 510, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                    End If
                                    intYTop += 8
                                End If
                            Next
                        Next
                    End If
                    If blnEndoso = True Then
                        page = New page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intYTop = 10

                        page.Add(180, intYTop + 10, New RepString(fp_p, "TRANSCRIPCION DEL PRIMER ENDOSO"))
                        'page.Add(400, intYTop + 10, New RepString(fp_p, strProtesto))
                        page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                        page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                        page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                        page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                        page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                        page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                        page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                        page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                        page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                        page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                        page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                        page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                        page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                        page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                        page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                        page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                        page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                        page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                        page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                        page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                        page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                        intYTop += 20
                        page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        intYTop -= 20
                        page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))

                        page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                        page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                        page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                        page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                        page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                        page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                        page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                        page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                        page.Add(60, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO"))
                        page.Add(100, intYTop + 250, New RepString(fp_Titulo, "DE DEPOSITO"))

                        page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                        intYTop += 10
                        fp_Titulo.bBold = True

                        page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                        page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                        page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                        page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                        page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                        page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                        intYTop += 10
                        page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                        page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                        page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                        page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                        page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))
                    page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                    page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso ha quedado  registrado en su  matr�cula o  libro  talonario,  as� como"))
                    page.Add(280, intYTop + 475, New RepString(fp_pi, "en el respectivo  Certificado de  Dep�sito,  en el caso de haber sido  emitido."))
                    '--------------------------------------------------------------------------------------
                    page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                        page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                        page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                        page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                        page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                        page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                    page.Add(360, intYTop + 600, New RepString(fp, "p. ALMACENERA DEL PER�"))
                    page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                        page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                        '--------------------------------------------------------------------------------------
                        page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                        page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                        intYTop += 10
                        page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                        page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                        page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                        page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                        page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                        page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                        page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                        page.Add(460, intYTop + 654, New RepString(fp, "Firma del Representante"))

                        page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                        page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                        page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                        page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(480, intYTop + 662, New RepString(fp, "Legal del AGD"))
                    End If
                End If
                If blnFirmas = True Then
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                page.AddCB(intYTop - 10, New RepString(fp_header, "TALON"))
                page.Add(intXLeft + 220, intYTop, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                    page.Add(intXLeft + 230, intYTop + 10, New RepString(fp_hc, "Certificado de Dep�sito con Warrant emitido"))
                    page.Add(intXLeft + 230, intYTop + 20, New RepString(fp_hc, "Certificado de Dep�sito sin Warrant emitido"))
                    page.Add(intXLeft + 380, intYTop + 11, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 380, intYTop + 25, New RepRectMM(ppBold, 4, 4))

                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(intXLeft + 383, intYTop + 9, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(intXLeft + 383, intYTop + 23, New RepString(fp_Certificacion, "X"))
                    End If
                    If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                    ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                    Else
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                    End If

                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If

                    'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(intXLeft + 420, intYTop + 10, New RepString(fp_pi, "   N�   " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                    intYTop += 30
                    page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                    page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                    page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                    page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                    page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                    page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                    page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(intXLeft + 91, 164, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 174, New RepString(fp, "con  D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                              " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(intXLeft + 70, 175, New RepString(fp_pi, "_______        __________________________________"))
                    page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                    page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                    page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(intXLeft + 91, 197, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                    page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                    page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                    page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                  " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(intXLeft + 150, 231, New RepString(fp_pi, "_____________________________________________________________________________________________"))
                    intYTop -= 10
                    page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                    page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                    page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                    page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                    page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                    If intPaginas = 1 Then
                        page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                        page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                        page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If



                If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                    page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) 'ok 1 TALON
                End If
                page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                    page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                    page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If dtEndoso.Rows.Count <= 16 Then
                        K = dtEndoso.Rows.Count - 1
                    Else
                        K = 15
                    End If
                    'Pinto el detalle de los items
                    For j As Integer = 0 To K
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 347, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intYTop += 10
                    Next

                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))
                    intYTop = 312
                    If intPaginas > 2 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                    page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                    page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))
                    '-------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                    End If
                    '-------------------------------------------
                    page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))
                    '--------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                    End If
                    '--------------------------------------------
                    page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intYTop += 25
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                    page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                page.Add(intXLeft + 40, (intYTop + 350) / 2.84 + 455, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                        page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                    page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-058-GW"))

                If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    End If

                    'Pinto el Anexo al Warrat si es que tiene
                    If intPaginas > 1 Then
                        For j As Integer = 2 To intPaginas
                            page = New page(report)
                            page.rWidthMM = 210
                            page.rHeightMM = 314
                            intYTop = 110

                        page.Add(210, 20, New RepString(fp_header, "ANEXO AL TALON"))
                        page.Add(207, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                        page.Add(207, 50, New RepString(fp_header, "Warrant con certificado de Dep�sito emitido"))
                        page.Add(207, 60, New RepString(fp_header, "Warrant sin certificado de Dep�sito emitido"))
                        page.Add(342, 51, New RepRectMM(ppBold, 4, 4))
                        page.Add(342, 65, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                            ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(344, 49, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(344, 63, New RepString(fp_Certificacion, "X"))
                        End If
                            If blnCopia = True Then
                                page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                                page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                            End If
                            'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                            page.Add(365, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(383, 41, New RepString(fp_pi, "___________________________________"))
                            page.Add(125, 140, New RepString(fp, "Pagina N�       " & j & ""))
                            page.Add(165, 141, New RepString(fp_pi, "_________________"))
                            page.Add(40, 200, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(350, 201, New RepString(fp_pi, "_______________________________"))
                            page.Add(320, 217, New RepString(fp, "     " & dtEndoso.Rows(0)("DIA TITULO") & "             de      " & dtEndoso.Rows(0)("MES TITULO") & "         de             " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                            page.Add(325, 218, New RepString(fp_pi, "__________"))
                            page.Add(390, 218, New RepString(fp_pi, "____________"))
                            page.Add(462, 218, New RepString(fp_pi, "_____________"))
                        page.Add(370, 228, New RepString(fp, " ALMACENERA DEL PER�"))
                        page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 150, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                            page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 160, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                        'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 175, New RepString(fp, "DE-R-061-GW"))
                        If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(22, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(60, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(110, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            End If

                            page.Add(42, intYTop + 236, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la pagina N� "))
                            page.Add(400, intYTop + 237, New RepString(fp_pi, "_________________"))
                            page.Add(intXLeft + 30, intYTop + 250, New RepString(fp_hc, "Cantidad"))
                            page.Add(intXLeft + 84, intYTop + 250, New RepString(fp_hc, "Unidad y/o peso"))
                            page.Add(intXLeft + 145, intYTop + 250, New RepString(fp_hc, "Descripcion de los bienes (clases,especie)y marcas de los bultos"))
                            page.Add(intXLeft + 370, intYTop + 246, New RepString(fp_hc, "Calidad y estado"))
                            page.Add(intXLeft + 370, intYTop + 256, New RepString(fp_hc, "de conservacion"))
                            page.Add(intXLeft + 430, intYTop + 246, New RepString(fp_hc, "Valor patrimonial con indicacion"))
                            page.Add(intXLeft + 430, intYTop + 256, New RepString(fp_hc, "del criterio de valorizacion"))
                            page.Add(intXLeft + 115, intYTop + 690, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))

                            page.AddMM(intXLeft, intYTop + 13, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, intYTop + 20, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intXLeft, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 19, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 39, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 120, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 140, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.AddMM(intXLeft + 180, (intYTop + 180), New RepLineMM(ppBold, 0, 167))
                            page.Add(intXLeft + 115, intYTop + 680, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                            page.Add(intXLeft + 115, intYTop + 700, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                            If j = intPaginas Then
                                page.AddMM(intX + 140, intYTop + 170, New RepLineMM(ppBold, 40, 0)) '----
                                page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                                page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                                page.AddRight(rPosLeft + 420 + 30, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                            End If

                            If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                                var = (j - 1) * (50) + 15
                            Else
                                var = (dtEndoso.Rows.Count - 1)
                            End If
                            For m As Integer = (j - 2) * (50) + 16 To var
                                If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                Else
                                    If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                        page.Add(intXLeft + 30, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                        page.Add(intXLeft + 84, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                        page.Add(intXLeft + 145, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 370, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 430, intYTop + 275, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                        page.AddRight(intXLeft + 510, intYTop + 275, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                    End If
                                    intYTop += 8
                                End If
                            Next
                        Next
                    End If
                End If
            End If
            'FIN----------------------------------------------------------------------------------------------------------------

            If (objDocumento.strTipoDocumentoGenerado = "01" Or objDocumento.strTipoDocumentoGenerado = "10") And objDocumento.strResultado = "0" Then
                'Pinto la primera hoja del warant
                intPaginas = (((dtEndoso.Rows.Count - 17) / 50) + 2)
                intPaginas = Fix(intPaginas)
                fp_header.bBold = True
                '--------------------------------------
                If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                page.Add(intXLeft + 250, intYTop - 10, New RepString(fp_header, "WARRANT"))
            Else
                page.Add(intXLeft + 220, intYTop - 10, New RepString(fp_header, "WARRANT ADUANERO"))
            End If
            '-----------------------------------------

            page.Add(intXLeft + 220, intYTop + 10, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
            page.Add(intXLeft + 220, intYTop + 20, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
            page.Add(intXLeft + 220, intYTop + 30, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
            page.Add(intXLeft + 370, intYTop + 21, New RepRectMM(ppBold, 4, 4))
            page.Add(intXLeft + 370, intYTop + 35, New RepRectMM(ppBold, 4, 4))

            If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                page.Add(intXLeft + 373, intYTop + 19, New RepString(fp_Certificacion, "X"))
            ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                page.Add(intXLeft + 373, intYTop + 33, New RepString(fp_Certificacion, "X"))
            End If
                If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                    strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                Else
                    strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                End If

                If blnCopia = True Then
                    page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                    page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                End If
            page.Add(intXLeft + 100, 95, New RepString(fp_TALMA, "  A L M A C E N E R A  D E L  P E R U  S . A ."))
            page.Add(intXLeft + 170, 105, New RepString(fp_e, "J r .  G a s p a r  H e r n a n d e z  # 7 6 0  -  R U C :  2 0 1 0 0 0 0 0 6 8 8"))


            'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
            page.Add(intXLeft + 390, intYTop + 10, New RepString(fp_pi, "   N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                intYTop += 30
                page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                page.Add(intXLeft + 91, 164, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 174, New RepString(fp, "con  D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                              " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                page.Add(intXLeft + 70, 175, New RepString(fp_pi, "_______        __________________________________"))
                page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                page.Add(intXLeft + 91, 197, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                  " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                page.Add(intXLeft + 150, 231, New RepString(fp_pi, "_____________________________________________________________________________________________"))
                intYTop -= 10
                page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                If intPaginas = 1 Then
                    page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                    page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                    page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                    page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                End If


            If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) 'ok 1 warrant
            End If
            page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
            page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
            page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

            If dtEndoso.Rows.Count <= 16 Then
                    K = dtEndoso.Rows.Count - 1
                Else
                    K = 15
                End If
                'Pinto el detalle de los items
                For j As Integer = 0 To K
                    If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                        page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                        page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                        page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                        page.Add(rPosLeft + 340, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                        page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                        page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                    End If
                    intYTop += 10
                Next
                xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                number = NumPalabra(Val(xnum))
                intYTop = 312
                If intPaginas > 1 Then
                    strNumber2 = "02"
                Else
                    strNumber2 = ""
                End If
                page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))

                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))
                '-------------------------------------------
                If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                Else
                    page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                End If
                '-------------------------------------------
                page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))
                '--------------------------------------------
                If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                Else
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                End If
                '--------------------------------------------
                page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                intYTop += 25
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                    page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
            page.Add(intXLeft + 40, (intYTop + 350) / 2.84 + 455, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
            If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                    page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                    page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
            'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-056-GW"))

            If blnFirmas = True Then
                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                End If
                'Pinto el Anexo al Warrat si es que tiene
                If intPaginas > 1 Then
                    For j As Integer = 2 To intPaginas
                        page = New Page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intYTop = 110

                    page.Add(210, 20, New RepString(fp_header, "ANEXO AL WARRANT"))
                    page.Add(202, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                    page.Add(202, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                    page.Add(202, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                    page.Add(342, 50, New RepRectMM(ppBold, 4, 4))
                    page.Add(342, 64, New RepRectMM(ppBold, 4, 4))
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(344, 48, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(344, 62, New RepString(fp_Certificacion, "X"))
                    End If
                        'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                        page.Add(365, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(383, 41, New RepString(fp_pi, "___________________________________"))
                        page.Add(125, 140, New RepString(fp, "Pagina N�       " & j & ""))
                        page.Add(165, 141, New RepString(fp_pi, "_________________"))

                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                        End If

                        page.Add(42, intYTop + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                        page.Add(365, intYTop + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                        page.Add(450, intYTop + 52, New RepString(fp_pi, "_________________"))
                        page.Add(rPosLeft - 5, intYTop + 69, New RepString(fp_hc, "Cantidad"))
                        page.Add(rPosLeft + 48, intYTop + 69, New RepString(fp_hc, "Unidad y/o peso"))
                        page.Add(rPosLeft + 110, intYTop + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                        page.Add(rPosLeft + 334, intYTop + 64, New RepString(fp_hc, "Calidad y estado"))
                        page.Add(rPosLeft + 334, intYTop + 74, New RepString(fp_hc, "de conservacion"))
                        page.Add(rPosLeft + 394, intYTop + 64, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                        page.Add(rPosLeft + 394, intYTop + 74, New RepString(fp_hc, "del criterio de valorizaci�n"))
                        page.Add(rPosLeft + 115, intYTop + 525, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                        intYTop -= 65
                        page.AddMM(intX, intYTop + 14, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, intYTop + 21, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 180, 0))

                        page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                        page.AddMM(intX + 19, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                        page.AddMM(intX + 39, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                        page.AddMM(intX + 120, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                        page.AddMM(intX + 140, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                        page.AddMM(intX + 180, (intYTop + 184), New RepLineMM(ppBold, 0, 170))

                        page.Add(rPosLeft + 115, intYTop + 580, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                        page.Add(rPosLeft + 115, intYTop + 600, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                        If j = intPaginas Then
                            page.AddMM(intX + 140, intYTop + 175, New RepLineMM(ppBold, 40, 0)) '----
                            page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                            page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                            page.AddRight(rPosLeft + 420 + 55, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                        End If

                        If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                            var = (j - 1) * (50) + 15
                        Else
                            var = (dtEndoso.Rows.Count - 1)
                        End If
                        For m As Integer = (j - 2) * (50) + 16 To var
                            If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                            Else
                                If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                    page.Add(intXLeft + 30, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                    page.Add(intXLeft + 84, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                    page.Add(intXLeft + 145, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                    page.Add(intXLeft + 365, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                    page.Add(intXLeft + 430, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                End If
                                If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                    page.AddRight(intXLeft + 510, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                End If
                                intYTop += 8
                            End If
                        Next

                        page.Add(40, 680, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                        page.Add(340, 680, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(350, 681, New RepString(fp_pi, "_________________________________________"))
                        page.Add(320, 697, New RepString(fp, "                       de                         de"))
                        page.Add(320, 697, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                        " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                        page.Add(325, 698, New RepString(fp_pi, "__________"))
                        page.Add(390, 698, New RepString(fp_pi, "____________"))
                        page.Add(465, 698, New RepString(fp_pi, "__________"))
                    page.Add(400, 715, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                        page.Add(intX + 46, (intY + 350) / 2.84 + 680, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                    'page.Add(40, (intY + 350) / 2.84 + 695, New RepString(fp, "DE-R-061-GW"))
                    If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(20, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(55, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(120, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(155, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                        End If
                    Next
                End If

                If blnEndoso = True Then
                    page = New Page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 10

                    page.Add(240, intYTop + 10, New RepString(fp_p, "PRIMER ENDOSO"))
                page.Add(400, intYTop + 10, New RepString(fp_e, strProtesto))
                page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                    page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                    If strDblEndoso = 0 Then
                        page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREFINANCIERA")))
                    Else
                        page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                    End If

                    page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                    page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                    page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                    If strDblEndoso = 0 Then
                        page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("Direc_FINA")))
                    Else
                        page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("DIRECCIONASOCIADO")))
                    End If

                    page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                    page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                    If dtEndoso2.Rows(0)("RUC") <> "" Then
                        page.Add(400, intYTop + 47, New RepString(fp_Certificacion, "R.U.C."))
                    End If
                    page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                    page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                    If strDblEndoso = 0 Then
                        page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("RUC")))
                    Else
                        page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                    End If

                    page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                    page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 50 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 50 And CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 150 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                        page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 50)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 150 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                        page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, 100)))
                        page.Add(40, intYTop + 80, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(150, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 150)))
                    End If
                    page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                    page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                    page.Add(135, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("SIM_MONE") & " " & String.Format("{0:##,##0.00}", dtEndoso2.Rows(0)("VAL_ARCH2"))))
                    page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                    page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                    page.Add(482, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("VEN_DOCU")))
                    page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                    page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                    page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 26 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 26 And CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 130 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                        page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length - 26)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 130 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                        page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, 102)))
                    End If
                    page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                    page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                    page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                    intYTop += 20
                    page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    intYTop -= 20
                    page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))
                    If strDblEndoso = 0 Then
                        If dtEndoso2.Rows(0)("BAN_ENDO").ToString.Length > 25 Then
                            strBanco = dtEndoso2.Rows(0)("BAN_ENDO").ToString.Substring(0, 25)
                        Else
                            strBanco = dtEndoso2.Rows(0)("BAN_ENDO")
                        End If
                        If dtEndoso2.Rows(0)("OFI_ENDO").ToString.Length > 20 Then
                            strOfi = dtEndoso2.Rows(0)("OFI_ENDO").ToString.Substring(0, 20)
                        Else
                            strOfi = dtEndoso2.Rows(0)("OFI_ENDO")
                        End If
                    End If

                    If dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Length > 15 Then
                        strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Substring(0, 15)
                    Else
                        strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO")
                    End If
                    If dtEndoso2.Rows(0)("DC_ENDO").ToString.Length > 10 Then
                        strDC = dtEndoso2.Rows(0)("DC_ENDO").ToString.Substring(0, 10)
                    Else
                        strDC = dtEndoso2.Rows(0)("DC_ENDO")
                    End If
                    page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                    page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                    page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                    page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                    page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                    page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                    page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                    page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                    page.Add(44, intYTop + 240, New RepString(fp_Titulo, "SIGUIENTES ENDOSOS DEL WARRANT"))
                    If dtEndoso2.Rows(0)("FLG_DBLENDO") Then
                        Dia = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Day)
                        Mes = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Month)
                        Anio = "0000" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Year)
                        Select Case Mes.Substring(Mes.Length - 2)
                            Case "01"
                                Mes = "Enero"
                            Case "02"
                                Mes = "Febrero"
                            Case "03"
                                Mes = "Marzo"
                            Case "04"
                                Mes = "Abril"
                            Case "05"
                                Mes = "Mayo"
                            Case "06"
                                Mes = "Junio"
                            Case "07"
                                Mes = "Julio"
                            Case "08"
                                Mes = "Agosto"
                            Case "09"
                                Mes = "Setiembre"
                            Case "10"
                                Mes = "Octubre"
                            Case "11"
                                Mes = "Noviembre"
                            Case "12"
                                Mes = "Diciembre"
                        End Select
                        If strDblEndoso = 0 Then
                            page.Add(44, intYTop + 327, New RepString(fp, "Toca y Pertenece a "))
                        End If

                        If dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Length > 40 Then
                            page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(0, 40)))
                            page.Add(44, intYTop + 347, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(40)))
                        Else
                            page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                        End If
                        'page.Add(44, intYTop + 342, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                        'page.Add(44, intYTop + 357, New RepString(fp, "Lima, " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)))

                        If blnFirmas = True Then
                            If strDblEndoso = 0 Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                            Else
                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If
                        If strDblEndoso = 0 Then
                            page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                            page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                            page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                        Else
                            page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                            page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                            page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                        End If
                    Else
                        page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                        page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                        page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                        If blnFirmas = True And strTipoDocumento <> "20" Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                    End If

                    If dtEndoso2.Rows(0)("FLG_EMBA") Then
                        page.Add(44, intYTop + 440, New RepString(fp, "Endoso para embarque a favor de los se�ores"))
                        page.Add(44, intYTop + 452, New RepString(fp, dtEndoso2.Rows(0)("NOMBREALMACENERA").ToString))
                        If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(15, 190, New RepImageMM(stream_Firma, Double.NaN, 23))

                            stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(50, 190, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                    End If

                    If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length > 40 Then
                        page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, 40)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length <= 40 Then
                        page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length)))
                    End If

                    page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                    intYTop += 10
                    fp_Titulo.bBold = True

                    page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                    page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                    page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                    page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                    page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                    page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                    '-------------------------------------------------------------------------------------
                    If blnFirmas = True Then
                        If strDblEndoso = 0 Then
                            stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                        Else
                            stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                    End If
                    intYTop += 10
                    page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                    page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                    page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                    page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                    page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))
                    If strDblEndoso = 0 Then
                        page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("RUC")))
                    Else
                        page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                    End If

                page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso  ha quedado  registrado en su  matr�cula o libro  talonario,  as� como"))
                page.Add(280, intYTop + 475, New RepString(fp_pi, "en el  respectivo  Certificado de  Dep�sito,  en el caso de haber sido emitido."))
                '--------------------------------------------------------------------------------------
                page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                    page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                    page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                    page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                    page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                    page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                page.Add(360, intYTop + 600, New RepString(fp, "ALMACENERA DEL PER�"))
                page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                    page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                    '--------------------------------------------------------------------------------------
                    page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                    page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                    intYTop += 10
                    page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                    page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                    page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                    page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                    page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                    page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                    page.Add(460, intYTop + 654, New RepString(fp, "Firma del Endosatario"))

                    page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                    page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                    page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                    page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(480, intYTop + 662, New RepString(fp, "Del Warrant"))
                    If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                        page.Add(350, 850, New RepString(fp_pi, "N� de Garantia: " & dtEndoso2.Rows(0)("NRO_GARA")))
                    End If
                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If
                End If
                'Pinto el Certificado del warrant si es que tiene
                If dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO") = "S" Then
                    page = New Page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                    '--------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.Add(intXLeft + 210, intYTop - 10, New RepString(fp_header, "CERTIFICADO DE DEPOSITO"))
                Else
                    page.Add(intXLeft + 180, intYTop - 10, New RepString(fp_header, "CERTIFICADO DE DEPOSITO ADUANERO"))
                End If
                '-----------------------------------------
                page.Add(intXLeft + 220, intYTop + 10, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                page.Add(intXLeft + 220, intYTop + 20, New RepString(fp_hc, "Certificado de Dep�sito con Warrant emitido"))
                page.Add(intXLeft + 220, intYTop + 30, New RepString(fp_hc, "Certificado de Dep�sito sin Warrant emitido"))

                page.Add(intXLeft + 100, 95, New RepString(fp_TALMA, "  A L M A C E N E R A  D E L  P E R U  S . A ."))
                page.Add(intXLeft + 170, 105, New RepString(fp_e, "J r .  G a s p a r  H e r n a n d e z  # 7 6 0  -  R U C :  2 0 1 0 0 0 0 0 6 8 8"))

                page.Add(intXLeft + 370, intYTop + 20, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 370, intYTop + 34, New RepRectMM(ppBold, 4, 4))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                    page.Add(intXLeft + 373, intYTop + 18, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                    page.Add(intXLeft + 373, intYTop + 32, New RepString(fp_Certificacion, "X"))
                End If

                    If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                    ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                    Else
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                    End If

                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If

                    'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(intXLeft + 420, intYTop + 10, New RepString(fp_pi, "   N�   " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                    intYTop += 30
                    page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                    page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                    page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                    page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                    page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                    page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                    page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(intXLeft + 91, 164, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 174, New RepString(fp, "con D.O.I:  R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                                  " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(intXLeft + 70, 175, New RepString(fp_pi, "______        ____________________________________"))
                    page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                    page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                    page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(intXLeft + 91, 197, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                    page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                    page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                    page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                   " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(intXLeft + 140, 231, New RepString(fp_pi, "_______________________________________________________________________________________________"))
                    intYTop -= 10
                    page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                    page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                    page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                    page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                    page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                    If intPaginas = 1 Then
                        page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                        page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                        page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If


                If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                    page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) 'ok 2 cert
                End If
                page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                    page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                    page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If dtEndoso.Rows.Count <= 16 Then
                        K = dtEndoso.Rows.Count - 1
                    Else
                        K = 15
                    End If
                    'Pinto el detalle de los items
                    For j As Integer = 0 To K
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 340, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intYTop += 10
                    Next

                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))
                    intYTop = 312
                    If intPaginas > 1 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                    page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                    page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))

                    '-------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                    End If
                    '-------------------------------------------
                    page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))

                    '--------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                    End If
                    '--------------------------------------------

                    page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intYTop += 25
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                    page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                        page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                    page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-057-GW"))

                If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    End If
                    If intPaginas > 1 Then
                        For j As Integer = 2 To intPaginas
                            page = New Page(report)
                            page.rWidthMM = 210
                            page.rHeightMM = 314
                            intYTop = 110

                        page.Add(210, 20, New RepString(fp_header, "ANEXO AL CERTIFICADO DEL WARRANT"))
                        page.Add(207, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                        page.Add(207, 50, New RepString(fp_hc, "Certificado de Dep�sito con Warrant emitido"))
                        page.Add(207, 60, New RepString(fp_hc, "Certificado de Dep�sito sin Warrant emitido"))
                        page.Add(347, 50, New RepRectMM(ppBold, 4, 4))
                        page.Add(347, 64, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                            ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(349, 48, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(349, 62, New RepString(fp_Certificacion, "X"))
                        End If
                            'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                            page.Add(370, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(388, 41, New RepString(fp_pi, "___________________________________"))
                            page.Add(125, 140, New RepString(fp, "Pagina N�   " & j & ""))
                            page.Add(165, 141, New RepString(fp_pi, "_________________"))

                            If blnCopia = True Then
                                page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                                page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                            End If

                            page.Add(40, 680, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                            page.Add(340, 680, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(350, 681, New RepString(fp_pi, "_________________________________________"))
                            page.Add(320, 697, New RepString(fp, "                       de                         de"))
                            page.Add(320, 697, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                        " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                            page.Add(325, 698, New RepString(fp_pi, "__________"))
                            page.Add(390, 698, New RepString(fp_pi, "____________"))
                            page.Add(465, 698, New RepString(fp_pi, "__________"))
                        page.Add(370, 715, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                        page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                            page.Add(intX + 46, (intY + 350) / 2.84 + 680, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                        'page.Add(40, (intY + 350) / 2.84 + 695, New RepString(fp, "DE-R-061-GW"))
                        If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(20, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(55, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(120, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(155, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                            End If

                            page.Add(42, intYTop + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                            page.Add(365, intYTop + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                            page.Add(450, intYTop + 52, New RepString(fp_pi, "_________________"))
                            page.Add(rPosLeft - 5, intYTop + 69, New RepString(fp_hc, "Cantidad"))
                            page.Add(rPosLeft + 48, intYTop + 69, New RepString(fp_hc, "Unidad y/o peso"))
                            page.Add(rPosLeft + 110, intYTop + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                            page.Add(rPosLeft + 334, intYTop + 64, New RepString(fp_hc, "Calidad y estado"))
                            page.Add(rPosLeft + 334, intYTop + 74, New RepString(fp_hc, "de conservacion"))
                            page.Add(rPosLeft + 394, intYTop + 64, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                            page.Add(rPosLeft + 394, intYTop + 74, New RepString(fp_hc, "del criterio de valorizaci�n"))
                            page.Add(rPosLeft + 115, intYTop + 525, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                            intYTop -= 65
                            page.AddMM(intX, intYTop + 14, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intX, intYTop + 21, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 180, 0))

                            page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 19, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 39, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 120, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 140, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 180, (intYTop + 184), New RepLineMM(ppBold, 0, 170))

                            page.Add(rPosLeft + 115, intYTop + 580, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                            page.Add(rPosLeft + 115, intYTop + 600, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                            If j = intPaginas Then
                                page.AddMM(intX + 140, intYTop + 175, New RepLineMM(ppBold, 40, 0)) '----
                                page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                                page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                                page.AddRight(rPosLeft + 420 + 55, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                            End If

                            If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                                var = (j - 1) * (50) + 15
                            Else
                                var = (dtEndoso.Rows.Count - 1)
                            End If

                            For m As Integer = (j - 2) * (50) + 16 To var
                                If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then

                                Else
                                    If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                        page.Add(intXLeft + 30, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                        page.Add(intXLeft + 84, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                        page.Add(intXLeft + 145, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 365, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 430, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                        page.AddRight(intXLeft + 510, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                    End If
                                    intYTop += 8
                                End If
                            Next
                        Next
                    End If
                    If blnEndoso = True Then
                        page = New Page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intYTop = 10

                        page.Add(180, intYTop + 10, New RepString(fp_p, "TRANSCRIPCION DEL PRIMER ENDOSO"))
                    page.Add(400, intYTop + 10, New RepString(fp_e, strProtesto))
                    page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                        page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                        If strDblEndoso = 0 Then
                            page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREFINANCIERA")))
                        Else
                            page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                        End If

                        page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                        page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                        page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                        If strDblEndoso = 0 Then
                            page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("Direc_FINA")))
                        Else
                            page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("DIRECCIONASOCIADO")))
                        End If

                        page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                        page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                        If dtEndoso2.Rows(0)("RUC") <> "" Then
                            page.Add(400, intYTop + 47, New RepString(fp_Certificacion, "R.U.C."))
                        End If
                        page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                        page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                        If strDblEndoso = 0 Then
                            page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("RUC")))
                        Else
                            page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                        End If

                        page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                        page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 50 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 50 And CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 150 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                            page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 50)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 150 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                            page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, 100)))
                            page.Add(40, intYTop + 80, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(150, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 150)))
                        End If
                        page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                        page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                        page.Add(135, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("SIM_MONE") & " " & String.Format("{0:##,##0.00}", dtEndoso2.Rows(0)("VAL_ARCH2"))))
                        page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                        page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                        page.Add(482, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("VEN_DOCU")))
                        page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                        page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                        page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 26 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 26 And CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 130 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                            page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length - 26)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 130 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                            page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, 102)))
                        End If
                        page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                        page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                        page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                        intYTop += 20
                        page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        intYTop -= 20
                        page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))
                        If strDblEndoso = 0 Then
                            If dtEndoso2.Rows(0)("BAN_ENDO").ToString.Length > 25 Then
                                strBanco = dtEndoso2.Rows(0)("BAN_ENDO").ToString.Substring(0, 25)
                            Else
                                strBanco = dtEndoso2.Rows(0)("BAN_ENDO")
                            End If
                            If dtEndoso2.Rows(0)("OFI_ENDO").ToString.Length > 20 Then
                                strOfi = dtEndoso2.Rows(0)("OFI_ENDO").ToString.Substring(0, 20)
                            Else
                                strOfi = dtEndoso2.Rows(0)("OFI_ENDO")
                            End If
                        End If

                        If dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Length > 15 Then
                            strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Substring(0, 15)
                        Else
                            strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO")
                        End If
                        If dtEndoso2.Rows(0)("DC_ENDO").ToString.Length > 10 Then
                            strDC = dtEndoso2.Rows(0)("DC_ENDO").ToString.Substring(0, 10)
                        Else
                            strDC = dtEndoso2.Rows(0)("DC_ENDO")
                        End If
                        page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                        page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                        page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                        page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                        page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                        page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                        page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                        page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                        'page.Add(44, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO DE DEPOSITO"))
                        page.Add(60, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO"))
                        page.Add(100, intYTop + 250, New RepString(fp_Titulo, "DE DEPOSITO"))

                        If dtEndoso2.Rows(0)("FLG_DBLENDO") Then
                            Dia = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Day)
                            Mes = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Month)
                            Anio = "0000" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Year)
                            Select Case Mes.Substring(Mes.Length - 2)
                                Case "01"
                                    Mes = "Enero"
                                Case "02"
                                    Mes = "Febrero"
                                Case "03"
                                    Mes = "Marzo"
                                Case "04"
                                    Mes = "Abril"
                                Case "05"
                                    Mes = "Mayo"
                                Case "06"
                                    Mes = "Junio"
                                Case "07"
                                    Mes = "Julio"
                                Case "08"
                                    Mes = "Agosto"
                                Case "09"
                                    Mes = "Setiembre"
                                Case "10"
                                    Mes = "Octubre"
                                Case "11"
                                    Mes = "Noviembre"
                                Case "12"
                                    Mes = "Diciembre"
                            End Select
                            If strDblEndoso = 0 Then
                                page.Add(44, intYTop + 327, New RepString(fp, "Toca y Pertenece a "))
                                If dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Length > 40 Then
                                    page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(0, 40)))
                                    page.Add(44, intYTop + 347, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(40)))
                                Else
                                    page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                                End If
                                'page.Add(44, intYTop + 342, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                                page.Add(44, intYTop + 357, New RepString(fp, "Lima, " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)))
                            End If

                            If blnFirmas = True Then
                                If strDblEndoso = 0 Then
                                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                    stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                    stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                    stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                                    stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                                Else
                                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                    page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                End If
                            End If
                            If strDblEndoso = 0 Then
                                page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                                page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                                page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                            Else
                                page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                                page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                                page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                            End If
                        Else
                            page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                            page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                            page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                            If blnFirmas = True And strTipoDocumento <> "20" Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If

                        If dtEndoso2.Rows(0)("FLG_EMBA") Then
                            page.Add(44, intYTop + 440, New RepString(fp, "Endoso para embarque a favor de los se�ores"))
                            page.Add(44, intYTop + 452, New RepString(fp, dtEndoso2.Rows(0)("NOMBREALMACENERA").ToString))
                            If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 190, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 190, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If

                        If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length > 40 Then
                            page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, 40)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length <= 40 Then
                            page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length)))
                        End If

                        page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                        intYTop += 10
                        fp_Titulo.bBold = True

                        page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                        page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                        page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                        page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                        page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                        page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                        '-------------------------------------------------------------------------------------
                        If blnFirmas = True Then
                            If strDblEndoso = 0 Then
                                stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                            Else
                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If
                        intYTop += 10
                        page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                        page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                        page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                        page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                        page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))

                        If strDblEndoso = 0 Then
                            page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("RUC")))
                        Else
                            page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                        End If

                    page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                    page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso  ha quedado  registrado en su  matr�cula o libro  talonario,  as� como"))
                    page.Add(280, intYTop + 475, New RepString(fp_pi, "en el  respectivo Certificado de  Dep�sito,  en el caso de haber sido  emitido."))
                    '--------------------------------------------------------------------------------------
                    page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                        page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                        page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                        page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                        page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                        page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                    page.Add(360, intYTop + 600, New RepString(fp, "ALMACENERA DEL PER�"))
                    page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                        page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                        '--------------------------------------------------------------------------------------
                        page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                        page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                        intYTop += 10
                        page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                        page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                        page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                        page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                        page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                        page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                        page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                        page.Add(460, intYTop + 654, New RepString(fp, "Firma del Representante"))

                        page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                        page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                        page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                        page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(480, intYTop + 662, New RepString(fp, "Legal del AGD"))
                        If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                            page.Add(350, 850, New RepString(fp_pi, "N� de Garantia: " & dtEndoso2.Rows(0)("NRO_GARA")))
                        End If
                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                        End If
                    End If
                End If

                If blnFirmas = True Then
                    page = New Page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                    '--------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                    page.AddCB(intYTop - 10, New RepString(fp_header, "TALON"))
                Else
                    page.AddCB(intYTop - 10, New RepString(fp_header, "TALON ADUANERO"))
                End If
                '-----------------------------------------

                page.Add(intXLeft + 220, intYTop + 10, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                page.Add(intXLeft + 220, intYTop + 20, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                page.Add(intXLeft + 220, intYTop + 30, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))

                page.Add(intXLeft + 100, 95, New RepString(fp_TALMA, "  A L M A C E N E R A  D E L  P E R U  S . A ."))
                page.Add(intXLeft + 170, 105, New RepString(fp_e, "J r .  G a s p a r  H e r n a n d e z  # 7 6 0  -  R U C :  2 0 1 0 0 0 0 0 6 8 8"))

                page.Add(intXLeft + 370, intYTop + 21, New RepRectMM(ppBold, 4, 4))
                page.Add(intXLeft + 370, intYTop + 35, New RepRectMM(ppBold, 4, 4))

                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        strError = "El campo CERT. DEPOSITO EMITIDO su valor es nulo"
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                    page.Add(intXLeft + 373, intYTop + 19, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                    page.Add(intXLeft + 373, intYTop + 33, New RepString(fp_Certificacion, "X"))
                End If
                    If IsDBNull(dtEndoso.Rows(0)("DIRECCION. CLIENTE")) Then
                    ElseIf dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Length > 70 Then
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE").ToString.Substring(0, 70)
                    Else
                        strDirCliente = dtEndoso.Rows(0)("DIRECCION. CLIENTE")
                    End If

                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If

                    'page.Add(intXLeft + 445, intYTop - 10, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(intXLeft + 390, intYTop + 10, New RepString(fp_pi, "   N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(intXLeft + 410, intYTop + 11, New RepString(fp_pi, "___________________________"))
                    intYTop += 30
                    page.Add(intXLeft + 26, 140, New RepString(fp, "En                  el d�a                           de                                       de                                     , se expide el siguiente t�tulo a la orden de"))
                    page.Add(intXLeft + 26, 140, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                            " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(intXLeft + 41, 141, New RepString(fp_pi, "_________"))
                    page.Add(intXLeft + 106, 141, New RepString(fp_pi, "______________"))
                    page.Add(intXLeft + 178, 141, New RepString(fp_pi, "______________________"))
                    page.Add(intXLeft + 286, 141, New RepString(fp_pi, "__________________"))

                    page.Add(intXLeft + 26, 150, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(intXLeft + 26, 151, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 163, New RepString(fp, "domiciliado en"))
                    page.Add(intXLeft + 26, 163, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(intXLeft + 91, 164, New RepString(fp_pi, "__________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 174, New RepString(fp, "con  D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(intXLeft + 26, 174, New RepString(fp_pi, "                                              " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(intXLeft + 70, 175, New RepString(fp_pi, "_______        __________________________________"))
                    page.Add(intXLeft + 26, 185, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(intXLeft + 26, 185, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(intXLeft + 26, 186, New RepString(fp_pi, "_________________________________"))
                    page.Add(intXLeft + 246, 186, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(intXLeft + 26, 196, New RepString(fp, "con domicilio en"))
                    page.Add(intXLeft + 26, 196, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(intXLeft + 91, 197, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, 207, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(intXLeft + 26, 218, New RepString(fp, "El plazo del dep�sito es de                              y vence el                            de                                              de"))
                    page.Add(intXLeft + 26, 218, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                             " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                               " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(intXLeft + 131, 219, New RepString(fp_pi, "_________________"))
                    page.Add(intXLeft + 246, 219, New RepString(fp_pi, "_____________"))
                    page.Add(intXLeft + 326, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 450, 219, New RepString(fp_pi, "_____________________"))
                    page.Add(intXLeft + 26, 230, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(intXLeft + 26, 230, New RepString(fp_pi, "                                                                  " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(intXLeft + 150, 231, New RepString(fp_pi, "_____________________________________________________________________________________________"))
                    intYTop -= 10
                    page.Add(intXLeft + 26, intYTop + 200, New RepString(fp_e, "Bienes en dep�sito"))
                    page.Add(intXLeft + 38, intYTop + 217, New RepString(fp_hc, "Cantidad"))
                    page.Add(intXLeft + 83, intYTop + 217, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(intXLeft + 143, intYTop + 217, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie) y marcas de los bultos"))
                    page.Add(intXLeft + 368, intYTop + 212, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(intXLeft + 368, intYTop + 221, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(intXLeft + 428, intYTop + 212, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(intXLeft + 428, intYTop + 221, New RepString(fp_hc, "del criterio de valorizaci�n"))

                    page.AddMM(intXLeft, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 47, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intXLeft, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 19, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 39, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 120, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 140, intYTop + 127, New RepLineMM(ppBold, 0, 87))
                    page.AddMM(intXLeft + 180, intYTop + 127, New RepLineMM(ppBold, 0, 87))

                    If intPaginas = 1 Then
                        page.AddMM(intXLeft + 140, intYTop + 120, New RepLineMM(ppBold, 40, 0))
                        page.Add(intXLeft + 375, intYTop + 445, New RepString(fp_pi, "TOTAL"))
                        page.Add(intXLeft + 430, intYTop + 445, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(intXLeft + 510, intYTop + 445, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If


                If dtEndoso.Rows(0)("ST_MERC_PERE") = "S" Then
                    page.Add(intXLeft + 143, intYTop + 415, New RepString(fp_hc, "PRODUCTO PERECIBLE")) 'ok 3 talon
                End If


                page.Add(intXLeft + 143, intYTop + 435, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                    page.Add(intXLeft + 143, intYTop + 425, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                    page.Add(intXLeft + 143, intYTop + 445, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If dtEndoso.Rows.Count <= 16 Then
                        K = dtEndoso.Rows.Count - 1
                    Else
                        K = 15
                    End If
                    'Pinto el detalle de los items
                    For j As Integer = 0 To K
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.Add(intXLeft + 35, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(intXLeft + 100, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(intXLeft + 143, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 340, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(intXLeft + 430, intYTop + 240, New RepString(fp_pi, dtEndoso.Rows(j)("SIMBOLO MONEDA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(intXLeft + 510, intYTop + 240, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intYTop += 10
                    Next
                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))
                    intYTop = 312
                    If intPaginas > 1 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                    page.Add(intXLeft + 26, (intYTop + 348) / 2.84 + 289, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�                 )"))
                    page.Add(240, (intYTop + 348) / 2.84 + 289, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intYTop + 348) / 2.84 + 290, New RepString(fp_hc, "________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 301, New RepString(fp_pi, "                                                                                     " & dtEndoso.Rows(0)("DIRECCION ALMACEN")))
                    page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 302, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 313, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intXLeft + 230, (intYTop + 350) / 2.84 + 314, New RepString(fp, "______________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 325, New RepString(fp, "Datos adicionales para operaciones en Dep�sito Aduanero Autorizado:"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 340, New RepString(fp, "D.U.A. N�                                                                                               Fecha de vencimiento"))
                    '-------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 120, (intYTop + 350) / 2.84 + 340, New RepString(fp_pi, dtEndoso.Rows(0)("D.U.A. N�") + "                                                                                                                              " + dtEndoso.Rows(0)("FEC. VENC. D.U.A.").ToString.Substring(0, 10)))
                    End If
                    '-------------------------------------------
                    page.Add(intXLeft + 70, (intYTop + 350) / 2.84 + 341, New RepString(fp, "_____________________________________________                                    ________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 352, New RepString(fp, "Certificado de Dep�sito Aduanero N�"))
                    '--------------------------------------------
                    If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, "----------"))
                    Else
                        page.Add(intXLeft + 200, (intYTop + 350) / 2.84 + 352, New RepString(fp_pi, dtEndoso.Rows(0)("N� CERT. DEP. ADUANERO")))
                    End If
                    '--------------------------------------------
                    page.Add(intXLeft + 170, (intYTop + 350) / 2.84 + 353, New RepString(fp, "___________________________________________________________________________"))

                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 367, New RepString(fp_pi, "                                                                                                                                                  " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intXLeft + 310, (intYTop + 350) / 2.84 + 368, New RepString(fp, "______________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 379, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 380, New RepString(fp, "________________________________________________________________________________________________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 391, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 403, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 415, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 427, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intYTop += 25
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intXLeft + 155, (intYTop + 350) / 2.84 + 431, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 431, New RepString(fp, "SI "))
                    page.Add(intXLeft + 136, (intYTop + 350) / 2.84 + 431, New RepString(fp, "NO "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 153, (intYTop + 350) / 2.84 + 433, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 445, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                page.Add(intXLeft + 40, (intYTop + 350) / 2.84 + 455, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 465, New RepString(fp_Certificacion, "X"))
                        page.Add(intXLeft + 112, (intYTop + 350) / 2.84 + 465, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intXLeft + 44, (intYTop + 350) / 2.84 + 485, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Si "))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 467, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 42, (intYTop + 350) / 2.84 + 487, New RepRectMM(ppBold, 4, 4))
                    page.Add(intXLeft + 96, (intYTop + 350) / 2.84 + 465, New RepString(fp, "Importe"))
                    page.Add(intXLeft + 145, (intYTop + 350) / 2.84 + 466, New RepString(fp, "_______________________"))
                    page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 485, New RepString(fp, "NO "))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 570, New RepString(fp, "  ____________________________________________                 ____________________________________________"))
                    page.Add(intXLeft + 46, (intYTop + 350) / 2.84 + 580, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                'page.Add(intXLeft + 26, (intYTop + 350) / 2.84 + 590, New RepString(fp_pi, "DE-R-058-GW"))

                If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(67, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(110, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(153, 286, New RepImageMM(stream_Firma, Double.NaN, 30))
                    End If
                    'Pinto el Anexo al Warrat si es que tiene
                    If intPaginas > 1 Then
                        For j As Integer = 2 To intPaginas
                            page = New Page(report)
                            page.rWidthMM = 210
                            page.rHeightMM = 314
                            intYTop = 110

                            '--------------------------------------
                            If dtEndoso.Rows(0)("D.U.A. N�") = "" Then
                            page.Add(210, 20, New RepString(fp_header, "ANEXO AL TALON"))
                        Else
                            page.Add(210, 20, New RepString(fp_header, "ANEXO AL TALON ADUNERO"))
                        End If
                        '-----------------------------------------
                        page.Add(202, 40, New RepString(fp_e, "Ley 27287, Resolucion SBS 019-2001"))
                        page.Add(202, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                        page.Add(202, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                        page.Add(342, 50, New RepRectMM(ppBold, 4, 4))
                        page.Add(342, 64, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                            ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(344, 48, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(344, 62, New RepString(fp_Certificacion, "X"))
                        End If
                            'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                            page.Add(365, 40, New RepString(fp_pi, "  N�            " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(383, 41, New RepString(fp_pi, "___________________________________"))
                            page.Add(125, 140, New RepString(fp, "Pagina N�       " & j & ""))
                            page.Add(165, 141, New RepString(fp_pi, "_________________"))

                            If blnCopia = True Then
                                page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                                page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                            End If

                            page.Add(42, intYTop + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                            page.Add(365, intYTop + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                            page.Add(450, intYTop + 52, New RepString(fp_pi, "_________________"))
                            page.Add(rPosLeft - 5, intYTop + 69, New RepString(fp_hc, "Cantidad"))
                            page.Add(rPosLeft + 48, intYTop + 69, New RepString(fp_hc, "Unidad y/o peso"))
                            page.Add(rPosLeft + 110, intYTop + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                            page.Add(rPosLeft + 334, intYTop + 64, New RepString(fp_hc, "Calidad y estado"))
                            page.Add(rPosLeft + 334, intYTop + 74, New RepString(fp_hc, "de conservacion"))
                            page.Add(rPosLeft + 394, intYTop + 64, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                            page.Add(rPosLeft + 394, intYTop + 74, New RepString(fp_hc, "del criterio de valorizaci�n"))
                            page.Add(rPosLeft + 115, intYTop + 525, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                            intYTop -= 65
                            page.AddMM(intX, intYTop + 14, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intX, intYTop + 21, New RepLineMM(ppBold, 180, 0))
                            page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 180, 0))

                            page.AddMM(intX, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 19, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 39, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 120, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 140, (intYTop + 184), New RepLineMM(ppBold, 0, 170))
                            page.AddMM(intX + 180, (intYTop + 184), New RepLineMM(ppBold, 0, 170))

                            page.Add(rPosLeft + 115, intYTop + 580, New RepString(fp_hc, "" & dtEndoso.Rows(0)(51) & ""))
                            page.Add(rPosLeft + 115, intYTop + 600, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                            If j = intPaginas Then
                                page.AddMM(intX + 140, intYTop + 175, New RepLineMM(ppBold, 40, 0)) '----
                                page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                                page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                                page.AddRight(rPosLeft + 420 + 55, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                            End If

                            If (j - 1) * (50) + 16 < dtEndoso.Rows.Count Then
                                var = (j - 1) * (50) + 15
                            Else
                                var = (dtEndoso.Rows.Count - 1)
                            End If
                            For m As Integer = (j - 2) * (50) + 16 To var
                                If IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                Else
                                    If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                        page.Add(intXLeft + 30, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                        page.Add(intXLeft + 84, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                        page.Add(intXLeft + 145, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 365, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                        page.Add(intXLeft + 430, intYTop + 150, New RepString(fp_pi, dtEndoso.Rows(m)("SIMBOLO MONEDA")))
                                    End If
                                    If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                        page.AddRight(intXLeft + 510, intYTop + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                                    End If
                                    intYTop += 8
                                End If
                            Next

                            page.Add(40, 680, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                            page.Add(340, 680, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                            page.Add(350, 681, New RepString(fp_pi, "_________________________________________"))
                            page.Add(320, 697, New RepString(fp, "                       de                         de"))
                            page.Add(320, 697, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                        " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                            page.Add(325, 698, New RepString(fp_pi, "__________"))
                            page.Add(390, 698, New RepString(fp_pi, "____________"))
                            page.Add(465, 698, New RepString(fp_pi, "__________"))
                        page.Add(400, 715, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                        page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                            page.Add(intX + 46, (intY + 350) / 2.84 + 680, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                        'page.Add(40, (intY + 350) / 2.84 + 695, New RepString(fp, "DE-R-061-GW"))
                        If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(20, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(55, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(120, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                                stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(155, 280, New RepImageMM(stream_Firma, Double.NaN, 25))
                            End If
                        Next
                    End If
                End If
            End If

            'Pinto el WIP
            If objDocumento.strTipoDocumentoGenerado = "04" And objDocumento.strResultado = "0" Then
                intPaginas = ((dtEndoso.Rows.Count - 20) / 60) + 2
                intPaginas = Fix(intPaginas)
                intY = 110
                fp_header.bBold = True
            page.Add(190, 20, New RepString(fp_header, "WARRANT INSUMO - PRODUCTO"))
            page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                page.Add(217, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                page.Add(217, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                page.Add(358, 51, New RepRectMM(ppBold, 4, 4))
                page.Add(358, 65, New RepRectMM(ppBold, 4, 4))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                    page.Add(360, 49, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                    page.Add(360, 63, New RepString(fp_Certificacion, "X"))
                End If
                If blnCopia = True Then
                    page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                    page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                End If
                'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                page.Add(370, 40, New RepString(fp_pi, "  N�"))
                page.Add(365, 40, New RepString(fp_pi, "                      " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                page.Add(381, 41, New RepString(fp_pi, "_____________________________"))
                page.Add(40, 135, New RepString(fp, "En                  el d�a                           de                                       de                                      , se expide el siguiente t�tulo a la orden de"))
                page.Add(40, 135, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                         " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                page.Add(55, 136, New RepString(fp_pi, "_________"))
                page.Add(120, 136, New RepString(fp_pi, "______________"))
                page.Add(192, 136, New RepString(fp_pi, "______________________"))
                page.Add(300, 136, New RepString(fp_pi, "__________________"))

                page.Add(40, 145, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                page.Add(40, 146, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                page.Add(40, 158, New RepString(fp, "domiciliado en"))
                page.Add(40, 158, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                page.Add(105, 159, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                page.Add(40, 169, New RepString(fp, "con D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                page.Add(40, 169, New RepString(fp_pi, "                                                   " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                page.Add(83, 170, New RepString(fp_pi, "______        ____________________________________"))
                page.Add(40, 180, New RepString(fp, "                                                         En la Empresa"))
                page.Add(40, 180, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                page.Add(40, 181, New RepString(fp_pi, "_________________________________"))
                page.Add(260, 181, New RepString(fp_pi, "_____________________________________________________________________"))
                page.Add(40, 191, New RepString(fp, "con domicilio en"))
                page.Add(40, 191, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                page.Add(105, 192, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                page.Add(40, 202, New RepString(fp, "por su valor en riesgo correspondiente."))
                page.Add(40, 213, New RepString(fp, "El plazo del dep�sito es de                              y vence el                                   de                                                       de"))
                page.Add(40, 213, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                                 " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                                        " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                               " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                page.Add(145, 214, New RepString(fp_pi, "_________________"))
                page.Add(260, 214, New RepString(fp_pi, "__________________"))
                page.Add(352, 214, New RepString(fp_pi, "______________________________"))
                page.Add(495, 214, New RepString(fp_pi, "____________"))
                page.Add(40, 225, New RepString(fp, "La tarifa de almacenaje es de"))
                page.Add(40, 225, New RepString(fp_pi, "                                                                   " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                page.Add(154, 226, New RepString(fp_pi, "______________________________________________________________________________________________"))
                intY -= 12
                page.Add(42, intY + 138, New RepString(fp_e, "Bienes en dep�sito(Insumos)"))
                page.Add(rPosLeft + 3, intY + 156, New RepString(fp_hc, "Cantidad"))
                page.Add(rPosLeft + 47, intY + 156, New RepString(fp_hc, "Unidad y/o peso"))
                page.Add(rPosLeft + 108, intY + 156, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                page.Add(rPosLeft + 333, intY + 152, New RepString(fp_hc, "Calidad y estado"))
                page.Add(rPosLeft + 333, intY + 161, New RepString(fp_hc, "de conservaci�n"))
                page.Add(rPosLeft + 393, intY + 152, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                page.Add(rPosLeft + 393, intY + 161, New RepString(fp_hc, "del criterio de valorizaci�n"))
                page.Add(rPosLeft + 110, intY + 280, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))
                page.Add(rPosLeft + 110, intY + 287, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                page.Add(rPosLeft + 110, intY + 294, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                If intPaginas = 1 Then
                    page.AddMM(intX + 140, intY + 33, New RepLineMM(ppBold, 40, 0)) '----
                    page.Add(rPosLeft + 345, intY + 285, New RepString(fp_pi, "TOTAL"))
                    page.Add(rPosLeft + 390, intY + 285, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                    page.AddRight(rPosLeft + 480, intY + 285, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                End If

                page.AddMM(intX, intY - 4, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intX, intY - 13, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 180, 0))

                page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.AddMM(intX + 19, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.AddMM(intX + 39, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.AddMM(intX + 120, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.AddMM(intX + 140, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.AddMM(intX + 180, (intY + 42), New RepLineMM(ppBold, 0, 55))
                page.Add(rPosLeft + 110, intY + 176, New RepString(fp_h, "MATERIA PRIMA"))

                If intPaginas > 2 Then
                    strNumber2 = "02"
                Else
                    strNumber2 = ""
                End If
            'intY += 10
            'page.Add(40, (intY + 330) / 2.84 + 678, New RepString(fp, "DE-R-056-GW"))
            page.Add(40, (intY + 348) / 2.84 + 249, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�"))
                page.Add(240, (intY + 348) / 2.84 + 249, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                page.Add(330, (intY + 352) / 2.84 + 248, New RepString(fp_hc, "________)"))
                page.Add(40, (intY + 348) / 2.84 + 258, New RepString(fp_hc, "Los insumos antes se�alados podr�n ser sustitu�dos por los siguientes productos finales o terminados a los cuales se incorporen,conforme las"))
                page.Add(40, (intY + 348) / 2.84 + 268, New RepString(fp_hc, "instrucciones y condiciones que reciba la almacenera en documento debidamente firmado por el depositante y el tenedor del warrant,quedando la"))
                page.Add(40, (intY + 348) / 2.84 + 278, New RepString(fp_hc, "sustituci�n bajo control de la almacenera."))
                intY += 3
                page.Add(42, (intY + 348) / 2.84 + 290, New RepString(fp_e, "Productos"))
                page.Add(rPosLeft + 3, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Cantidad"))
                page.Add(rPosLeft + 47, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Unidad y/o peso"))
                page.Add(rPosLeft + 108, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Caracter�sticas de los productos"))
                page.Add(rPosLeft + 340, intY + 365, New RepString(fp_hc, "T�rminos de"))
                page.Add(rPosLeft + 340, intY + 374, New RepString(fp_hc, "Sustituci�n"))
                page.Add(rPosLeft + 413, intY + 370, New RepString(fp_hc, "Observaciones"))
                page.AddMM(intX, intY + 60, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intX, intY + 68, New RepLineMM(ppBold, 180, 0))
                page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 180, 0))

                page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.AddMM(intX + 19, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.AddMM(intX + 39, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.AddMM(intX + 120, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.AddMM(intX + 140, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.AddMM(intX + 180, (intY + 100), New RepLineMM(ppBold, 0, 40))
                page.Add(rPosLeft + 390, intY + 181, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))

                page.Add(rPosLeft + 110, intY + 385, New RepString(fp_h, "PRODUCTO TERMINADO"))
                intY -= 3
                intY += 6
                'Pinto el detalle del WIP
                For j As Integer = 0 To 11
                    If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                        page.AddRight(rPosLeft + 40, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                        page.Add(rPosLeft + 66, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                        page.Add(rPosLeft + 110, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                        page.Add(rPosLeft + 340, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                        page.AddRight(rPosLeft + 480, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                    End If
                    intY += 8
                Next

                For j As Integer = 12 To 18
                    If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                        page.AddRight(rPosLeft + 40, intY + 293, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                        page.Add(rPosLeft + 70, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("UNIDAD Y/O PESO") & ""))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                        page.Add(rPosLeft + 110, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES") & ""))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("TERM. SUST.")) Then
                        page.Add(rPosLeft + 335, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("TERM. SUST.") & ""))
                    End If
                    If Not IsDBNull(dtEndoso.Rows(j)("OBSERVACIONES")) Then
                        page.Add(rPosLeft + 389, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("OBSERVACIONES") & ""))
                    End If
                    intY += 8
                Next
                xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                number = NumPalabra(Val(xnum))
                intY += 125

                page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                If dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Length > 70 Then
                    strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Substring(0, 70)
                Else
                    strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN")
                End If
                page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp_pi, "                                                                                     " & strDireccionAlmacen))
                page.Add(intX + 200, (intY + 350) / 2.84 + 327, New RepString(fp, "____________________________________________________________________"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                page.Add(intX + 230, (intY + 350) / 2.84 + 339, New RepString(fp, "______________________________________________________________"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp_pi, "                                                                                                                                             " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                page.Add(intX + 310, (intY + 350) / 2.84 + 357, New RepString(fp, "______________________________________________"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 370, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                page.Add(intX + 26, (intY + 350) / 2.84 + 371, New RepString(fp, "_______________________________________________________________________________________________________"))
                intY -= 60
                page.Add(intX + 26, (intY + 350) / 2.84 + 410, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 422, New RepString(fp, "el correspondiente t�tulo."))
                page.Add(intX + 26, (intY + 350) / 2.84 + 434, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 446, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                intY += 15
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                    page.Add(intX + 46, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                    page.Add(intX + 155, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intX + 26, (intY + 350) / 2.84 + 456, New RepString(fp, "SI "))
                page.Add(intX + 136, (intY + 350) / 2.84 + 456, New RepString(fp, "NO "))
                page.Add(intX + 40, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                page.Add(intX + 153, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
            page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
            If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                    page.Add(intX + 40, (intY + 350) / 2.84 + 490, New RepString(fp_Certificacion, "X"))
                    page.Add(intX + 102, (intY + 350) / 2.84 + 490, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                    page.Add(intX + 40, (intY + 350) / 2.84 + 510, New RepString(fp_Certificacion, "X"))
                End If
                page.Add(intX + 26, (intY + 350) / 2.84 + 490, New RepString(fp, "Si "))
                page.Add(intX + 39, (intY + 350) / 2.84 + 492, New RepRectMM(ppBold, 4, 4))
                page.Add(intX + 39, (intY + 350) / 2.84 + 512, New RepRectMM(ppBold, 4, 4))
                page.Add(intX + 96, (intY + 350) / 2.84 + 490, New RepString(fp, "Importe"))
                page.Add(intX + 145, (intY + 350) / 2.84 + 491, New RepString(fp, "_______________________"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 510, New RepString(fp, "No "))
                page.Add(intX + 46, (intY + 350) / 2.84 + 562, New RepString(fp, "  ____________________________________________                            ____________________________________________"))
                page.Add(intX + 46, (intY + 350) / 2.84 + 572, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))

                If blnFirmas = True Then
                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(25, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(60, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                    stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(120, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                    stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(155, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                End If
                'Pinto el anexo del WIP
                For j As Integer = 2 To intPaginas
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intY = 110

                page.Add(170, 20, New RepString(fp_header, "ANEXO AL WARRANT INSUMO - PRODUCTO"))
                page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                    page.Add(217, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                    page.Add(217, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                    page.Add(358, 51, New RepRectMM(ppBold, 4, 4))
                    page.Add(358, 65, New RepRectMM(ppBold, 4, 4))
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(360, 49, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(360, 63, New RepString(fp_Certificacion, "X"))
                    End If
                    'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(370, 40, New RepString(fp_pi, "  N�"))
                    page.Add(365, 40, New RepString(fp_pi, "                         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(383, 41, New RepString(fp_pi, "_____________________________"))

                    page.Add(125, 140, New RepString(fp, "Pagina N�"))
                    page.Add(165, 140, New RepString(fp_pi, "         " & j & ""))
                    page.Add(165, 141, New RepString(fp_pi, "_________________"))

                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))

                    End If
                    page.Add(40, 720, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                    page.Add(340, 720, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(350, 721, New RepString(fp_pi, "_________________________________________"))
                    page.Add(320, 737, New RepString(fp, "                       de                         de"))
                    page.Add(320, 737, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                     " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(325, 738, New RepString(fp_pi, "__________"))
                    page.Add(390, 738, New RepString(fp_pi, "____________"))
                    page.Add(465, 738, New RepString(fp_pi, "__________"))
                page.Add(400, 747, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                page.Add(intX + 46, (intY + 350) / 2.84 + 660, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                'page.Add(40, (intY + 350) / 2.84 + 688, New RepString(fp, "DE-R-061-GW"))

                If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(20, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(55, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(120, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(155, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                    End If

                    page.Add(42, intY + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                    page.Add(365, intY + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                    page.Add(450, intY + 52, New RepString(fp_pi, "_________________"))
                    page.Add(rPosLeft - 5, intY + 69, New RepString(fp_hc, "Cantidad"))
                    page.Add(rPosLeft + 49, intY + 69, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(rPosLeft + 110, intY + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                    page.Add(rPosLeft + 335, intY + 64, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(rPosLeft + 335, intY + 74, New RepString(fp_hc, "de conservacion"))
                    page.Add(rPosLeft + 395, intY + 64, New RepString(fp_hc, "del criterio de valorizaci�n"))
                    page.Add(rPosLeft + 390, intY + 74, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(rPosLeft + 125, intY + 575, New RepString(fp_hc, "ADQUISICION SEGUN FACTURA INCLUYE I.G.V."))
                    intY -= 65
                    page.AddMM(intX, intY + 14, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, intY + 21, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 0, 184))
                    page.AddMM(intX + 19, (intY + 198), New RepLineMM(ppBold, 0, 184))
                    page.AddMM(intX + 39, (intY + 198), New RepLineMM(ppBold, 0, 184))
                    page.AddMM(intX + 120, (intY + 198), New RepLineMM(ppBold, 0, 184))
                    page.AddMM(intX + 140, (intY + 198), New RepLineMM(ppBold, 0, 184))
                    page.AddMM(intX + 180, (intY + 198), New RepLineMM(ppBold, 0, 184))

                    page.Add(rPosLeft + 125, intY + 630, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))

                    If j = intPaginas Then
                        page.AddMM(intX + 140, intY + 190, New RepLineMM(ppBold, 40, 0)) '----
                        page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                        page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(rPosLeft + 480, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If

                    If ((j - 1) * (60)) + 19 < dtEndoso.Rows.Count Then
                        var = (j - 1) * (60) + 19
                    Else
                        var = (dtEndoso.Rows.Count - 1)
                    End If

                    For m As Integer = (j - 2) * (60) + 19 To var - 1
                        If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                            page.AddRight(rPosLeft + 40, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                            page.Add(rPosLeft + 49, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("UNIDAD Y/O PESO") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(rPosLeft + 110, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 335, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("ESTADO MERCADERIA") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                            page.AddRight(rPosLeft + 480, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                        End If
                        intY += 8
                    Next
                Next
                If blnEndoso = True Then
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 10

                    page.Add(240, intYTop + 10, New RepString(fp_p, "PRIMER ENDOSO"))
                page.Add(400, intYTop + 10, New RepString(fp_e, strProtesto))
                page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                    page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                    page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREFINANCIERA")))
                    page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                    page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                    page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                    page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("Direc_FINA")))
                    page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                    page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                    If dtEndoso2.Rows(0)("RUC") <> "" Then
                        page.Add(400, intYTop + 47, New RepString(fp_Certificacion, "R.U.C."))
                    End If
                    page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                    page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                    page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("RUC")))
                    page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                    page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 50 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 50 And CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 150 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                        page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 50)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 150 Then
                        page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                        page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, 100)))
                        page.Add(40, intYTop + 80, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(150, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 150)))
                    End If
                    page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                    page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                    page.Add(135, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("SIM_MONE") & " " & String.Format("{0:##,##0.00}", dtEndoso2.Rows(0)("VAL_ARCH2"))))
                    page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                    page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                    page.Add(482, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("VEN_DOCU")))
                    page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                    page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                    page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 26 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 26 And CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 130 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                        page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length - 26)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 130 Then
                        page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                        page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, 102)))
                    End If
                    page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                    page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                    page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                    page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                    intYTop += 20
                    page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                    page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                    intYTop -= 20
                    page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))

                    If dtEndoso2.Rows(0)("BAN_ENDO").ToString.Length > 25 Then
                        strBanco = dtEndoso2.Rows(0)("BAN_ENDO").ToString.Substring(0, 25)
                    Else
                        strBanco = dtEndoso2.Rows(0)("BAN_ENDO")
                    End If
                    If dtEndoso2.Rows(0)("OFI_ENDO").ToString.Length > 20 Then
                        strOfi = dtEndoso2.Rows(0)("OFI_ENDO").ToString.Substring(0, 20)
                    Else
                        strOfi = dtEndoso2.Rows(0)("OFI_ENDO")
                    End If
                    If dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Length > 15 Then
                        strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Substring(0, 15)
                    Else
                        strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO")
                    End If
                    If dtEndoso2.Rows(0)("DC_ENDO").ToString.Length > 10 Then
                        strDC = dtEndoso2.Rows(0)("DC_ENDO").ToString.Substring(0, 10)
                    Else
                        strDC = dtEndoso2.Rows(0)("DC_ENDO")
                    End If
                    page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                    page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                    page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                    page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                    page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                    page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                    page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                    page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                    page.Add(44, intYTop + 240, New RepString(fp_Titulo, "SIGUIENTES ENDOSOS DEL WARRANT"))
                    If dtEndoso2.Rows(0)("FLG_DBLENDO") Then
                        Dia = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Day)
                        Mes = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Month)
                        Anio = "0000" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Year)
                        Select Case Mes.Substring(Mes.Length - 2)
                            Case "01"
                                Mes = "Enero"
                            Case "02"
                                Mes = "Febrero"
                            Case "03"
                                Mes = "Marzo"
                            Case "04"
                                Mes = "Abril"
                            Case "05"
                                Mes = "Mayo"
                            Case "06"
                                Mes = "Junio"
                            Case "07"
                                Mes = "Julio"
                            Case "08"
                                Mes = "Agosto"
                            Case "09"
                                Mes = "Setiembre"
                            Case "10"
                                Mes = "Octubre"
                            Case "11"
                                Mes = "Noviembre"
                            Case "12"
                                Mes = "Diciembre"
                        End Select

                        page.Add(44, intYTop + 327, New RepString(fp, "Toca y Pertenece a "))
                        If dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Length > 40 Then
                            page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(0, 40)))
                            page.Add(44, intYTop + 347, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(40)))
                        Else
                            page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                        End If
                        'page.Add(44, intYTop + 342, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                        page.Add(44, intYTop + 357, New RepString(fp, "Lima, " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)))

                        If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                            stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                            stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                        page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                        page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                        page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                    Else
                        page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                        page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                        page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                        If blnFirmas = True And strTipoDocumento <> "20" Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                    End If

                    If dtEndoso2.Rows(0)("FLG_EMBA") Then
                        page.Add(44, intYTop + 440, New RepString(fp, "Endoso para embarque a favor de los se�ores"))
                        page.Add(44, intYTop + 452, New RepString(fp, dtEndoso2.Rows(0)("NOMBREALMACENERA").ToString))
                        If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(15, 190, New RepImageMM(stream_Firma, Double.NaN, 23))

                            stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(50, 190, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                    End If

                    If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length > 40 Then
                        page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, 40)))
                    End If
                    If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length <= 40 Then
                        page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length)))
                    End If

                    page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                    intYTop += 10
                    fp_Titulo.bBold = True

                    page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                    page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                    page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                    page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                    page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                    page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                    '-------------------------------------------------------------------------------------
                    If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                        stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                    End If
                    intYTop += 10
                    page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                    page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                    page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                    page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                    page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))
                    page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("RUC")))
                page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso  ha quedado  registrado en su  matr�cula o libro  talonario,  as� como"))
                page.Add(280, intYTop + 475, New RepString(fp_pi, "en el  respectivo  Certificado de Dep�sito,  en el caso de haber sido  emitido."))
                '--------------------------------------------------------------------------------------
                page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                    page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                    page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                    page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                    page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                    page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                page.Add(360, intYTop + 600, New RepString(fp, "ALMACENERA DEL PER�"))
                page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                    page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                    '--------------------------------------------------------------------------------------
                    page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                    page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                    intYTop += 10
                    page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                    page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                    page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                    page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                    page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                    page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                    page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                    page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                    page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                    page.Add(460, intYTop + 654, New RepString(fp, "Firma del Endosatario"))

                    page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                    page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                    page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                    page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                    page.Add(480, intYTop + 662, New RepString(fp, "Del Warrant"))
                    If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                        page.Add(350, 850, New RepString(fp_pi, "N� de Garantia: " & dtEndoso2.Rows(0)("NRO_GARA")))
                    End If
                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If
                End If
                'Pinto el Certificado del WIP si es que tiene
                If dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO") = "S" Then
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                    intY = 110

                page.Add(180, 20, New RepString(fp_header, "CERTIFICADO INSUMO - PRODUCTO"))
                page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                    page.Add(217, 50, New RepString(fp_hc, "Certificado de Dep�sito con Warrant emitido"))
                    page.Add(217, 60, New RepString(fp_hc, "Certificado de Dep�sito sin Warrant emitido"))
                    page.Add(358, 52, New RepRectMM(ppBold, 4, 4))
                    page.Add(358, 66, New RepRectMM(ppBold, 4, 4))
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(360, 50, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(360, 64, New RepString(fp_Certificacion, "X"))
                    End If
                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If
                    'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(370, 40, New RepString(fp_pi, "  N�"))
                    page.Add(365, 40, New RepString(fp_pi, "                      " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(381, 41, New RepString(fp_pi, "_____________________________"))
                    intYTop += 30
                    page.Add(40, 135, New RepString(fp, "En                  el d�a                           de                                       de                                       , se expide el siguiente t�tulo a la orden de"))
                    page.Add(40, 135, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                             " & dtEndoso.Rows(0)("MES TITULO") & "                                         " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(55, 136, New RepString(fp_pi, "_________"))
                    page.Add(120, 136, New RepString(fp_pi, "______________"))
                    page.Add(192, 136, New RepString(fp_pi, "______________________"))
                    page.Add(300, 136, New RepString(fp_pi, "__________________"))

                    page.Add(40, 145, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(40, 146, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(40, 158, New RepString(fp, "domiciliado en"))
                    page.Add(40, 158, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(105, 159, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(40, 169, New RepString(fp, "con D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(40, 169, New RepString(fp_pi, "                                                 " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(83, 170, New RepString(fp_pi, "______         ____________________________________"))
                    page.Add(40, 180, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(40, 180, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(40, 181, New RepString(fp_pi, "_________________________________"))
                    page.Add(260, 181, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(40, 191, New RepString(fp, "con domicilio en"))
                    page.Add(40, 191, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(105, 192, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(40, 202, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(40, 213, New RepString(fp, "El plazo del dep�sito es de                              y vence el                                   de                                                       de"))
                    page.Add(40, 213, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                                 " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                                        " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                                 " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(145, 214, New RepString(fp_pi, "_________________"))
                    page.Add(260, 214, New RepString(fp_pi, "__________________"))
                    page.Add(352, 214, New RepString(fp_pi, "______________________________"))
                    page.Add(495, 214, New RepString(fp_pi, "____________"))
                    page.Add(40, 225, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(40, 225, New RepString(fp_pi, "                                                                   " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(153, 226, New RepString(fp_pi, "______________________________________________________________________________________________"))
                    intY -= 12
                    page.Add(42, intY + 137, New RepString(fp_e, "Bienes en dep�sito(Insumos)"))
                    page.Add(rPosLeft + 3, intY + 156, New RepString(fp_hc, "Cantidad"))
                    page.Add(rPosLeft + 47, intY + 156, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(rPosLeft + 108, intY + 156, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                    page.Add(rPosLeft + 333, intY + 152, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(rPosLeft + 333, intY + 161, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(rPosLeft + 393, intY + 152, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(rPosLeft + 393, intY + 161, New RepString(fp_hc, "del criterio de valorizaci�n"))
                    page.Add(rPosLeft + 108, intY + 280, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))
                    page.Add(rPosLeft + 108, intY + 287, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                    page.Add(rPosLeft + 108, intY + 294, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If intPaginas = 1 Then
                        page.AddMM(intX + 140, intY + 33, New RepLineMM(ppBold, 40, 0)) '----
                        page.Add(rPosLeft + 345, intY + 285, New RepString(fp_pi, "TOTAL"))
                        page.Add(rPosLeft + 390, intY + 285, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(rPosLeft + 480, intY + 285, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If

                    page.AddMM(intX, intY - 4, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, intY - 13, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 19, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 39, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 120, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 140, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 180, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.Add(rPosLeft + 110, intY + 175, New RepString(fp_h, "MATERIA PRIMA"))

                    If intPaginas > 2 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                'page.Add(40, (intY + 350) / 2.84 + 678, New RepString(fp, "DE-R-056-GW"))
                page.Add(40, (intY + 348) / 2.84 + 249, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�"))
                    page.Add(240, (intY + 348) / 2.84 + 249, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intY + 352) / 2.84 + 248, New RepString(fp_hc, "________)"))
                    page.Add(40, (intY + 348) / 2.84 + 258, New RepString(fp_hc, "Los insumos antes se�alados podr�n ser sustitu�dos por los siguientes productos finales o terminados a los cuales se incorporen,conforme las"))
                    page.Add(40, (intY + 348) / 2.84 + 268, New RepString(fp_hc, "instrucciones y condiciones que reciba la almacenera en documento debidamente firmado por el depositante y el tenedor del warrant,quedando la"))
                    page.Add(40, (intY + 348) / 2.84 + 278, New RepString(fp_hc, "sustituci�n bajo control de la almacenera."))
                    intY += 3
                    page.Add(42, (intY + 348) / 2.84 + 290, New RepString(fp_e, "Productos"))
                    page.Add(rPosLeft + 3, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Cantidad"))
                    page.Add(rPosLeft + 47, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(rPosLeft + 108, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Caracter�sticas de los productos"))
                    page.Add(rPosLeft + 340, intY + 365, New RepString(fp_hc, "T�rminos de"))
                    page.Add(rPosLeft + 340, intY + 374, New RepString(fp_hc, "Sustituci�n"))
                    page.Add(rPosLeft + 413, intY + 370, New RepString(fp_hc, "Observaciones"))
                    page.AddMM(intX, intY + 60, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, intY + 68, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 19, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 39, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 120, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 140, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 180, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.Add(rPosLeft + 390, intY + 181, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))

                    page.Add(rPosLeft + 110, intY + 385, New RepString(fp_h, "PRODUCTO TERMINADO"))
                    'Pinto el detalle del WIP
                    intY += 3

                    For j As Integer = 0 To 11
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.AddRight(rPosLeft + 40, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(rPosLeft + 66, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(rPosLeft + 110, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 340, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(rPosLeft + 480, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intY += 8
                    Next

                    For j As Integer = 12 To 18
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.AddRight(rPosLeft + 40, intY + 293, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(rPosLeft + 70, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("UNIDAD Y/O PESO") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(rPosLeft + 110, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("TERM. SUST.")) Then
                            page.Add(rPosLeft + 335, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("TERM. SUST.") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("OBSERVACIONES")) Then
                            page.Add(rPosLeft + 389, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("OBSERVACIONES") & ""))
                        End If
                        intY += 8
                    Next
                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))

                    intY += 125
                    If dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Length > 70 Then
                        strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Substring(0, 70)
                    Else
                        strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN")
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp_pi, "                                                                                     " & strDireccionAlmacen))
                    page.Add(intX + 200, (intY + 350) / 2.84 + 327, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intX + 230, (intY + 350) / 2.84 + 339, New RepString(fp, "______________________________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp_pi, "                                                                                                                                           " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intX + 310, (intY + 350) / 2.84 + 357, New RepString(fp, "______________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 370, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 371, New RepString(fp, "_______________________________________________________________________________________________________"))

                    intY -= 60
                    page.Add(intX + 26, (intY + 350) / 2.84 + 410, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 422, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 434, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 446, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intY += 15
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intX + 46, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intX + 155, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 456, New RepString(fp, "SI "))
                    page.Add(intX + 136, (intY + 350) / 2.84 + 456, New RepString(fp, "NO "))
                    page.Add(intX + 40, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 153, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp_pi, "                                                                                                                                                                           ALMACENERA DEL PER�"))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intX + 40, (intY + 350) / 2.84 + 490, New RepString(fp_Certificacion, "X"))
                        page.Add(intX + 102, (intY + 350) / 2.84 + 490, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intX + 40, (intY + 350) / 2.84 + 510, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 490, New RepString(fp, "Si "))
                    page.Add(intX + 39, (intY + 350) / 2.84 + 492, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 39, (intY + 350) / 2.84 + 512, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 96, (intY + 350) / 2.84 + 490, New RepString(fp, "Importe"))
                    page.Add(intX + 145, (intY + 350) / 2.84 + 491, New RepString(fp, "_______________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 510, New RepString(fp, "No "))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 562, New RepString(fp, "  _______________________________________                                   _______________________________________"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 572, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))

                    If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(60, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(120, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(160, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                    End If
                    'Pinto el Anexo del Certificado del warrant si es que tiene
                    For j As Integer = 2 To intPaginas
                        page = New page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intY = 110

                    page.Add(170, 20, New RepString(fp_header, "ANEXO AL CERTIFICADO INSUMO - PRODUCTO"))
                    page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                        page.Add(217, 50, New RepString(fp_hc, "Certificado de dep�sito con Warrant emitido"))
                        page.Add(217, 60, New RepString(fp_hc, "Certificado de dep�sito sin Warrant emitido"))
                        page.Add(358, 50, New RepRectMM(ppBold, 4, 4))
                        page.Add(358, 64, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(360, 48, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(360, 62, New RepString(fp_Certificacion, "X"))
                        End If
                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                        End If
                        'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                        page.Add(370, 40, New RepString(fp_pi, "  N�"))
                        page.Add(365, 40, New RepString(fp_pi, "                         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(383, 41, New RepString(fp_pi, "_____________________________"))

                        page.Add(125, 140, New RepString(fp, "Pagina N�"))
                        page.Add(165, 140, New RepString(fp_pi, "         " & j & ""))
                        page.Add(165, 141, New RepString(fp_pi, "_________________"))

                        page.Add(40, 720, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                        page.Add(340, 720, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(350, 721, New RepString(fp_pi, "_________________________________________"))
                        page.Add(320, 737, New RepString(fp, "                       de                         de"))
                        page.Add(320, 737, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                     " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                        page.Add(325, 738, New RepString(fp_pi, "__________"))
                        page.Add(390, 738, New RepString(fp_pi, "____________"))
                        page.Add(465, 738, New RepString(fp_pi, "__________"))
                    page.Add(370, 747, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 660, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                        page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                    'page.Add(40, (intY + 350) / 2.84 + 688, New RepString(fp, "DE-R-061-GW"))
                    If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(20, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(55, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(120, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(155, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        End If

                        page.Add(42, intY + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                        page.Add(365, intY + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                        page.Add(450, intY + 52, New RepString(fp_pi, "_________________"))
                        page.Add(rPosLeft - 5, intY + 69, New RepString(fp_hc, "Cantidad"))
                        page.Add(rPosLeft + 49, intY + 69, New RepString(fp_hc, "Unidad y/o peso"))
                        page.Add(rPosLeft + 110, intY + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                        page.Add(rPosLeft + 335, intY + 64, New RepString(fp_hc, "Calidad y estado"))
                        page.Add(rPosLeft + 335, intY + 74, New RepString(fp_hc, "de conservacion"))
                        page.Add(rPosLeft + 395, intY + 64, New RepString(fp_hc, "del criterio de valorizaci�n"))
                        page.Add(rPosLeft + 390, intY + 74, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                        page.Add(rPosLeft + 125, intY + 575, New RepString(fp_hc, "ADQUISICION SEGUN FACTURA INCLUYE I.G.V."))
                        intY -= 65
                        page.AddMM(intX, intY + 14, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, intY + 21, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 180, 0))

                        page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 19, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 39, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 120, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 140, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 180, (intY + 198), New RepLineMM(ppBold, 0, 184))

                        page.Add(rPosLeft + 125, intY + 630, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))

                        If j = intPaginas Then
                            page.AddMM(intX + 140, intY + 190, New RepLineMM(ppBold, 40, 0)) '----
                            page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                            page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                            page.AddRight(rPosLeft + 480, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                        End If

                        If ((j - 1) * (60)) + 19 < dtEndoso.Rows.Count Then
                            var = (j - 1) * (60) + 19
                        Else
                            var = (dtEndoso.Rows.Count - 1)
                        End If

                        For m As Integer = (j - 2) * (60) + 19 To var - 1
                            If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                page.AddRight(rPosLeft + 40, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                page.Add(rPosLeft + 49, intY + 150, New RepString(fp_pi, dtEndoso.Rows(m)("UNIDAD Y/O PESO")))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                page.Add(rPosLeft + 110, intY + 150, New RepString(fp_pi, dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                page.Add(rPosLeft + 335, intY + 150, New RepString(fp_pi, dtEndoso.Rows(m)("ESTADO MERCADERIA")))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                page.AddRight(rPosLeft + 480, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                            End If
                            intY += 8
                        Next
                    Next
                    If blnEndoso = True Then
                        page = New page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intYTop = 10

                        page.Add(180, intYTop + 10, New RepString(fp_p, "TRANSCRIPCION DEL PRIMER ENDOSO"))
                    page.Add(400, intYTop + 10, New RepString(fp_e, strProtesto))
                    page.Add(40, intYTop + 25, New RepString(fp, "Declaro (amos)  en  la  fecha  que  endoso (amos)  este  Warrant  que  representa   los   bienes  detallados   en   el  anverso,  a  la"))
                        page.Add(40, intYTop + 36, New RepString(fp, "orden  de  "))
                        page.Add(83, intYTop + 36, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("NOMBREFINANCIERA")))
                        page.Add(79, intYTop + 38, New RepString(fp_Certificacion, "......................................................................................................................................................................................"))
                        page.Add(485, intYTop + 36, New RepString(fp, "  con  domicilio"))
                        page.Add(40, intYTop + 47, New RepString(fp, "en  "))
                        page.Add(55, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("Direc_FINA")))
                        page.Add(55, intYTop + 49, New RepString(fp_Certificacion, "......................................................................................................................................"))
                        page.Add(350, intYTop + 47, New RepString(fp, "   con D.O.I.  "))
                        If dtEndoso2.Rows(0)("RUC") <> "" Then
                            page.Add(400, intYTop + 47, New RepString(fp_Certificacion, "R.U.C."))
                        End If
                        page.Add(400, intYTop + 49, New RepString(fp_Certificacion, "............."))
                        page.Add(430, intYTop + 47, New RepString(fp, "   N�  "))
                        page.Add(455, intYTop + 47, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("RUC")))
                        page.Add(452, intYTop + 49, New RepString(fp_Certificacion, ".........................................."))
                        page.Add(40, intYTop + 58, New RepString(fp, "para  garantizar el  cumplimiento de  obligacion(es) derivada(s) de "))
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 50 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 50 And CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length <= 150 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                            page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 50)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length > 150 Then
                            page.Add(300, intYTop + 58, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(0, 50)))
                            page.Add(40, intYTop + 69, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(50, 100)))
                            page.Add(40, intYTop + 80, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Substring(150, CStr(dtEndoso2.Rows(0)("OPE_ARCH2")).Length - 150)))
                        End If
                        page.Add(296, intYTop + 60, New RepString(fp_Certificacion, "................................................................................................................."))
                        page.Add(40, intYTop + 71, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 82, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 91, New RepString(fp, "hasta  por la  suma  de "))
                        page.Add(135, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("SIM_MONE") & " " & String.Format("{0:##,##0.00}", dtEndoso2.Rows(0)("VAL_ARCH2"))))
                        page.Add(130, intYTop + 93, New RepString(fp_Certificacion, "........................................................................................................................."))
                        page.Add(390, intYTop + 91, New RepString(fp, "   con  vencimiento  al  "))
                        page.Add(482, intYTop + 91, New RepString(fp_Certificacion, dtEndoso2.Rows(0)("VEN_DOCU")))
                        page.Add(477, intYTop + 93, New RepString(fp_Certificacion, "................................"))
                        page.Add(40, intYTop + 102, New RepString(fp, "sujeta(s) a las tasas de inter�s y demas condiciones pactadas en los respectivos contratos de cr�dito"))
                        page.Add(430, intYTop + 103, New RepString(fp_Certificacion, "......................................................"))
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 26 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 26 And CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length <= 130 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                            page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length - 26)))
                        End If
                        If CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Length > 130 Then
                            page.Add(430, intYTop + 102, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(0, 26)))
                            page.Add(40, intYTop + 113, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("TAS_DOCU")).Substring(26, 102)))
                        End If
                        page.Add(40, intYTop + 115, New RepString(fp_Certificacion, "......................................................................................................................................................................................................................................."))
                        page.Add(40, intYTop + 130, New RepString(fp, "En  el  caso  que  el  endosatario  fuese   una  empresa  del  sistema  financiero,  son  aplicables  las  estipulaciones  contenidas  en"))
                        page.Add(40, intYTop + 141, New RepString(fp, "los  Articulos  132.8�.  y  172�.  de  la  Ley  N� 26702  y  sus  modificatorias."))

                        page.Add(42, intYTop + 166, New RepString(fp_e, "Lugar de Pago de Cr�dito"))
                        intYTop += 20
                        page.AddMM(14, intYTop + 28, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 34, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 40, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 46, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(14, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        page.AddMM(59, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(104, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(149, intYTop + 52, New RepLineMM(ppBold, 0, 12))
                        page.AddMM(194, intYTop + 52, New RepLineMM(ppBold, 0, 24))
                        intYTop -= 20
                        page.Add(190, intYTop + 184, New RepString(fp_e, "Cargo en la cuenta del depositante (primer endosante)"))

                        If dtEndoso2.Rows(0)("BAN_ENDO").ToString.Length > 25 Then
                            strBanco = dtEndoso2.Rows(0)("BAN_ENDO").ToString.Substring(0, 25)
                        Else
                            strBanco = dtEndoso2.Rows(0)("BAN_ENDO")
                        End If
                        If dtEndoso2.Rows(0)("OFI_ENDO").ToString.Length > 20 Then
                            strOfi = dtEndoso2.Rows(0)("OFI_ENDO").ToString.Substring(0, 20)
                        Else
                            strOfi = dtEndoso2.Rows(0)("OFI_ENDO")
                        End If
                        If dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Length > 15 Then
                            strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO").ToString.Substring(0, 15)
                        Else
                            strCta = dtEndoso2.Rows(0)("NRO_CTA_ENDO")
                        End If
                        If dtEndoso2.Rows(0)("DC_ENDO").ToString.Length > 10 Then
                            strDC = dtEndoso2.Rows(0)("DC_ENDO").ToString.Substring(0, 10)
                        Else
                            strDC = dtEndoso2.Rows(0)("DC_ENDO")
                        End If
                        page.Add(intXLeft + 65, intYTop + 200, New RepString(fp_hc, "Banco"))
                        page.Add(intXLeft + 30, intYTop + 217, New RepString(fp_h, strBanco))
                        page.Add(intXLeft + 193, intYTop + 200, New RepString(fp_hc, "Oficina"))
                        page.Add(intXLeft + 160, intYTop + 217, New RepString(fp_h, strOfi))
                        page.Add(intXLeft + 310, intYTop + 200, New RepString(fp_hc, "N�. de Cuenta"))
                        page.Add(intXLeft + 300, intYTop + 217, New RepString(fp_h, strCta))
                        page.Add(intXLeft + 460, intYTop + 200, New RepString(fp_hc, "DC"))
                        page.Add(intXLeft + 450, intYTop + 217, New RepString(fp_h, strDC))

                        'page.Add(44, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO DE DEPOSITO"))
                        page.Add(60, intYTop + 240, New RepString(fp_Titulo, "ENDOSOS DEL CERTIFICADO"))
                        page.Add(100, intYTop + 250, New RepString(fp_Titulo, "DE DEPOSITO"))

                        If dtEndoso2.Rows(0)("FLG_DBLENDO") Then
                            Dia = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Day)
                            Mes = "00" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Month)
                            Anio = "0000" + CStr(CType(dtEndoso2.Rows(0)("FCH_CREA"), DateTime).Year)
                            Select Case Mes.Substring(Mes.Length - 2)
                                Case "01"
                                    Mes = "Enero"
                                Case "02"
                                    Mes = "Febrero"
                                Case "03"
                                    Mes = "Marzo"
                                Case "04"
                                    Mes = "Abril"
                                Case "05"
                                    Mes = "Mayo"
                                Case "06"
                                    Mes = "Junio"
                                Case "07"
                                    Mes = "Julio"
                                Case "08"
                                    Mes = "Agosto"
                                Case "09"
                                    Mes = "Setiembre"
                                Case "10"
                                    Mes = "Octubre"
                                Case "11"
                                    Mes = "Noviembre"
                                Case "12"
                                    Mes = "Diciembre"
                            End Select

                            page.Add(44, intYTop + 327, New RepString(fp, "Toca y Pertenece a "))
                            If dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Length > 40 Then
                                page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(0, 40)))
                                page.Add(44, intYTop + 347, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString.Substring(40)))
                            Else
                                page.Add(44, intYTop + 337, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                            End If
                            'page.Add(44, intYTop + 342, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO").ToString))
                            page.Add(44, intYTop + 357, New RepString(fp, "Lima, " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)))

                            If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaClieAsociado1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClieAsociado2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 155, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                            page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBREASOCIADO")))
                            page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                            page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_ASOCIADO")))
                        Else
                            page.Add(380, intYTop + 343, New RepString(fp, dtEndoso2.Rows(0)("NOMBRECLIENTE")))
                            page.Add(330, intYTop + 356, New RepString(fp, "RUC"))
                            page.Add(430, intYTop + 356, New RepString(fp, dtEndoso2.Rows(0)("NRO_RUC_CLIE")))
                            If blnFirmas = True And strTipoDocumento <> "20" Then
                                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(100, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(150, 115, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If

                        If dtEndoso2.Rows(0)("FLG_EMBA") Then
                            page.Add(44, intYTop + 440, New RepString(fp, "Endoso para embarque a favor de los se�ores"))
                            page.Add(44, intYTop + 452, New RepString(fp, dtEndoso2.Rows(0)("NOMBREALMACENERA").ToString))
                            If blnFirmas = True Then
                                stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(15, 190, New RepImageMM(stream_Firma, Double.NaN, 23))

                                stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                                page.AddMM(50, 190, New RepImageMM(stream_Firma, Double.NaN, 23))
                            End If
                        End If
                        If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length > 40 Then
                            page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, 40)))

                        End If
                        If CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length <= 40 Then
                            page.Add(320, intYTop + 233, New RepString(fp_Certificacion, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Substring(0, CStr(dtEndoso2.Rows(0)("LUG_ENDO")).Length)))
                        End If

                        page.AddMM(96, intYTop + 77, New RepLineMM(pp, 98, 0))
                        intYTop += 10
                        fp_Titulo.bBold = True

                        page.Add(370, intYTop + 322, New RepString(fp, "Firma del Endosante"))
                        page.AddMM(96, intYTop + 97, New RepLineMM(pp, 98, 0))
                        page.Add(387, intYTop + 240, New RepString(fp, "Lugar y Fecha"))
                        page.Add(280, intYTop + 335, New RepString(fp, "Nombre del Endosante ............................................................................."))

                        page.Add(280, intYTop + 348, New RepString(fp, "DOI ............................................."))
                        page.Add(410, intYTop + 348, New RepString(fp, "N�. ......................................................."))
                        '-------------------------------------------------------------------------------------
                        If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(100, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                            stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(150, 157, New RepImageMM(stream_Firma, Double.NaN, 23))
                        End If
                        intYTop += 10
                    page.Add(370, intYTop + 425, New RepString(fp, "Firma del Endosatario"))
                    page.AddMM(96, intYTop + 128, New RepLineMM(pp, 98, 0))
                        page.Add(280, intYTop + 440, New RepString(fp, "DOI ............................................."))
                        page.Add(330, intYTop + 438, New RepString(fp, "RUC"))
                        page.Add(410, intYTop + 440, New RepString(fp, "N�. .................................................."))
                        page.Add(430, intYTop + 438, New RepString(fp, dtEndoso2.Rows(0)("RUC")))
                    page.Add(280, intYTop + 455, New RepString(fp_pi, "Certificaci�n: ALMACENERA DEL PER� S.A., certifica que el presente primer"))
                    page.Add(280, intYTop + 465, New RepString(fp_pi, "endoso  ha quedado  registrado en su  matr�cula o libro  talonario,  as� como"))
                    page.Add(280, intYTop + 475, New RepString(fp_pi, "en el  respectivo  Certificado de  Dep�sito,  en el caso de haber sido emitido."))
                    '--------------------------------------------------------------------------------------
                    page.Add(345, intYTop + 505, New RepString(fp, "Fecha del registro del primer endoso"))
                        page.Add(390, intYTop + 493, New RepString(fp, strFechaFolio))
                        page.AddMM(111, intYTop + 155, New RepLineMM(pp, 70, 0))
                        page.Add(375, intYTop + 533, New RepString(fp, "N� de Folio y Tomo"))
                        page.Add(380, intYTop + 522, New RepString(fp, strNroFolio))

                        page.AddMM(111, intYTop + 166, New RepLineMM(pp, 70, 0))

                    page.Add(360, intYTop + 600, New RepString(fp, "ALMACENERA DEL PER�"))
                    page.Add(325, intYTop + 612, New RepString(fp, "Firma de identificaci�n del Represent. Legal"))
                        page.AddMM(111, intYTop + 189, New RepLineMM(pp, 70, 0))
                        '--------------------------------------------------------------------------------------
                        page.AddMM(14, intYTop + 53, New RepLineMM(ppBold, 80, 0))
                        page.AddMM(94, intYTop + 200, New RepLineMM(ppBold, 0, 147))

                        intYTop += 10
                        page.AddLT_MM(14, intYTop + 195, New RepRectMM(ppBold, 180, 60))
                        page.AddMM(14, intYTop + 200, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(38, intYTop + 205, New RepLineMM(ppBold, 123, 0))
                        page.AddMM(14, intYTop + 210, New RepLineMM(ppBold, 180, 0))

                        page.AddMM(38, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(69, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(100, intYTop + 255, New RepLineMM(ppBold, 0, 55))
                        page.AddMM(131, intYTop + 255, New RepLineMM(ppBold, 0, 50))
                        page.AddMM(161, intYTop + 255, New RepLineMM(ppBold, 0, 55))

                        page.Add(185, intYTop + 637, New RepString(fp, "AMORTIZACIONES Y RETIRO DE MERCADERIAS"))
                        page.Add(180, intYTop + 652, New RepString(fp, "BIENES"))
                        page.Add(340, intYTop + 652, New RepString(fp, "OBLIGACIONES"))
                        page.Add(460, intYTop + 654, New RepString(fp, "Firma del Representante"))

                        page.Add(60, intYTop + 659, New RepString(fp, "Fecha"))
                        page.Add(132, intYTop + 666, New RepString(fp, "V/Retirado"))
                        page.Add(225, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(304, intYTop + 666, New RepString(fp, "V/Abonado"))
                        page.Add(403, intYTop + 666, New RepString(fp, "Saldo"))
                        page.Add(480, intYTop + 662, New RepString(fp, "Legal del AGD"))
                        If dtEndoso2.Rows(0)("NRO_GARA") <> "" Then
                            page.Add(350, 850, New RepString(fp_pi, "N� de Garantia: " & dtEndoso2.Rows(0)("NRO_GARA")))
                        End If
                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                        End If
                    End If
                End If
                If blnFirmas = True Then
                    page = New page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 314
                    intYTop = 30
                    intY = 110
                    fp_header.bBold = True
                page.AddCB(20, New RepString(fp_header, "TALON DE WARRANT INSUMO PRODUCTO"))
                page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                    page.Add(217, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                    page.Add(217, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                    page.Add(358, 51, New RepRectMM(ppBold, 4, 4))
                    page.Add(358, 65, New RepRectMM(ppBold, 4, 4))
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                        page.Add(360, 49, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                        page.Add(360, 63, New RepString(fp_Certificacion, "X"))
                    End If
                    If blnCopia = True Then
                        page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                        page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))
                    End If
                    'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                    page.Add(370, 40, New RepString(fp_pi, "  N�"))
                    page.Add(365, 40, New RepString(fp_pi, "                      " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                    page.Add(381, 41, New RepString(fp_pi, "_____________________________"))
                    page.Add(40, 135, New RepString(fp, "En                  el d�a                           de                                       de                                      , se expide el siguiente t�tulo a la orden de"))
                    page.Add(40, 135, New RepString(fp_pi, "        " & dtEndoso.Rows(0)("UNIDAD") & "                              " & dtEndoso.Rows(0)("DIA TITULO") & "                           " & dtEndoso.Rows(0)("MES TITULO") & "                                         " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                    page.Add(55, 136, New RepString(fp_pi, "_________"))
                    page.Add(120, 136, New RepString(fp_pi, "______________"))
                    page.Add(192, 136, New RepString(fp_pi, "______________________"))
                    page.Add(300, 136, New RepString(fp_pi, "__________________"))

                    page.Add(40, 145, New RepString(fp_pi, "                                                      " & dtEndoso.Rows(0)("NOMBRE CLIENTE") & ""))

                    page.Add(40, 146, New RepString(fp_pi, "_________________________________________________________________________________________________________________________"))
                    page.Add(40, 158, New RepString(fp, "domiciliado en"))
                    page.Add(40, 158, New RepString(fp_pi, "                                   " & dtEndoso.Rows(0)("DIRECCION. CLIENTE") & "     "))
                    page.Add(105, 159, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(40, 169, New RepString(fp, "con D.O.I: R.U.C. N�                                                                   sobre los bienes abajo especificados, asegurados contra el riesgo de"))
                    page.Add(40, 169, New RepString(fp_pi, "                                                   " & dtEndoso.Rows(0)("CLIENTE FAC.") & ""))
                    page.Add(83, 170, New RepString(fp_pi, "______        ____________________________________"))
                    page.Add(40, 180, New RepString(fp, "                                                         En la Empresa"))
                    page.Add(40, 180, New RepString(fp_pi, "   INCENDIO Y LINEAS ALIADAS                                           " & dtEndoso.Rows(0)("NOMBRE CIA. SEGURO") & ""))
                    page.Add(40, 181, New RepString(fp_pi, "_________________________________"))
                    page.Add(260, 181, New RepString(fp_pi, "_____________________________________________________________________"))
                    page.Add(40, 191, New RepString(fp, "con domicilio en"))
                    page.Add(40, 191, New RepString(fp_pi, "                                               " & dtEndoso.Rows(0)("DIR. CIA. SEGURO") & ""))
                    page.Add(105, 192, New RepString(fp_pi, "_________________________________________________________________________________________________________"))
                    page.Add(40, 202, New RepString(fp, "por su valor en riesgo correspondiente."))
                    page.Add(40, 213, New RepString(fp, "El plazo del dep�sito es de                              y vence el                                   de                                                       de"))
                    page.Add(40, 213, New RepString(fp_pi, "                                                        " & dtEndoso.Rows(0)("DIAS PLAZO") & " Dias                                                 " & dtEndoso.Rows(0)("DIA VENCIMIENTO") & "                                        " & dtEndoso.Rows(0)("MES VENCIMIENTO") & "                                               " & dtEndoso.Rows(0)("A�O VENCIMIENTO") & ""))
                    page.Add(145, 214, New RepString(fp_pi, "_________________"))
                    page.Add(260, 214, New RepString(fp_pi, "__________________"))
                    page.Add(352, 214, New RepString(fp_pi, "______________________________"))
                    page.Add(495, 214, New RepString(fp_pi, "____________"))
                    page.Add(40, 225, New RepString(fp, "La tarifa de almacenaje es de"))
                    page.Add(40, 225, New RepString(fp_pi, "                                                                   " & dtEndoso.Rows(0)("VALOR DE LA TARIFA") & ""))
                    page.Add(154, 226, New RepString(fp_pi, "______________________________________________________________________________________________"))
                    intY -= 12
                    page.Add(42, intY + 138, New RepString(fp_e, "Bienes en dep�sito(Insumos)"))
                    page.Add(rPosLeft + 3, intY + 156, New RepString(fp_hc, "Cantidad"))
                    page.Add(rPosLeft + 47, intY + 156, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(rPosLeft + 108, intY + 156, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                    page.Add(rPosLeft + 333, intY + 152, New RepString(fp_hc, "Calidad y estado"))
                    page.Add(rPosLeft + 333, intY + 161, New RepString(fp_hc, "de conservaci�n"))
                    page.Add(rPosLeft + 393, intY + 152, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                    page.Add(rPosLeft + 393, intY + 161, New RepString(fp_hc, "del criterio de valorizaci�n"))
                    page.Add(rPosLeft + 110, intY + 280, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))
                    page.Add(rPosLeft + 110, intY + 287, New RepString(fp_hc, dtEndoso.Rows(0)("DESCRIPCION CRITERIO")))
                    page.Add(rPosLeft + 110, intY + 294, New RepString(fp_hc, dtEndoso.Rows(0)("REFERENCIAS")))

                    If intPaginas = 1 Then
                        page.AddMM(intX + 140, intY + 33, New RepLineMM(ppBold, 40, 0)) '----
                        page.Add(rPosLeft + 345, intY + 285, New RepString(fp_pi, "TOTAL"))
                        page.Add(rPosLeft + 390, intY + 285, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                        page.AddRight(rPosLeft + 480, intY + 285, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                    End If

                    page.AddMM(intX, intY - 4, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, intY - 13, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intX, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 19, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 39, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 120, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 140, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.AddMM(intX + 180, (intY + 42), New RepLineMM(ppBold, 0, 55))
                    page.Add(rPosLeft + 110, intY + 176, New RepString(fp_h, "MATERIA PRIMA"))

                    If intPaginas > 2 Then
                        strNumber2 = "02"
                    Else
                        strNumber2 = ""
                    End If
                'intY += 10
                'page.Add(40, (intY + 330) / 2.84 + 678, New RepString(fp, "DE-R-058-GW"))
                page.Add(40, (intY + 348) / 2.84 + 249, New RepString(fp_hc, "(la relaci�n de bienes contin�a en el anexo para detalle adicional de bienes en la p�gina N�"))
                    page.Add(240, (intY + 348) / 2.84 + 249, New RepString(fp_pi, "                                            " & strNumber2 & ""))
                    page.Add(330, (intY + 352) / 2.84 + 248, New RepString(fp_hc, "________)"))
                    page.Add(40, (intY + 348) / 2.84 + 258, New RepString(fp_hc, "Los insumos antes se�alados podr�n ser sustitu�dos por los siguientes productos finales o terminados a los cuales se incorporen,conforme las"))
                    page.Add(40, (intY + 348) / 2.84 + 268, New RepString(fp_hc, "instrucciones y condiciones que reciba la almacenera en documento debidamente firmado por el depositante y el tenedor del warrant,quedando la"))
                    page.Add(40, (intY + 348) / 2.84 + 278, New RepString(fp_hc, "sustituci�n bajo control de la almacenera."))
                    intY += 3
                    page.Add(42, (intY + 348) / 2.84 + 290, New RepString(fp_e, "Productos"))
                    page.Add(rPosLeft + 3, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Cantidad"))
                    page.Add(rPosLeft + 47, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Unidad y/o peso"))
                    page.Add(rPosLeft + 108, (intY + 348) / 2.84 + 310, New RepString(fp_hc, "Caracter�sticas de los productos"))
                    page.Add(rPosLeft + 340, intY + 365, New RepString(fp_hc, "T�rminos de"))
                    page.Add(rPosLeft + 340, intY + 374, New RepString(fp_hc, "Sustituci�n"))
                    page.Add(rPosLeft + 413, intY + 370, New RepString(fp_hc, "Observaciones"))
                    page.AddMM(intX, intY + 60, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, intY + 68, New RepLineMM(ppBold, 180, 0))
                    page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 180, 0))

                    page.AddMM(intX, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 19, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 39, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 120, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 140, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.AddMM(intX + 180, (intY + 100), New RepLineMM(ppBold, 0, 40))
                    page.Add(rPosLeft + 390, intY + 181, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))

                    page.Add(rPosLeft + 110, intY + 385, New RepString(fp_h, "PRODUCTO TERMINADO"))
                    intY -= 3
                    intY += 6
                    'Pinto el detalle del WIP
                    For j As Integer = 0 To 11
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.AddRight(rPosLeft + 40, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(rPosLeft + 66, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("UNIDAD Y/O PESO")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(rPosLeft + 110, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("ESTADO MERCADERIA")) Then
                            page.Add(rPosLeft + 340, intY + 178, New RepString(fp_pi, dtEndoso.Rows(j)("ESTADO MERCADERIA")))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("IMPORTE")) Then
                            page.AddRight(rPosLeft + 480, intY + 178, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(j)("IMPORTE"))))
                        End If
                        intY += 8
                    Next

                    For j As Integer = 12 To 18
                        If Not IsDBNull(dtEndoso.Rows(j)("CANTIDAD")) Then
                            page.AddRight(rPosLeft + 40, intY + 293, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(j)("CANTIDAD"))))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("UNIDAD Y/O PESO")) Then
                            page.Add(rPosLeft + 70, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("UNIDAD Y/O PESO") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES")) Then
                            page.Add(rPosLeft + 110, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("DESCRIPCION DE LOS BIENES") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("TERM. SUST.")) Then
                            page.Add(rPosLeft + 335, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("TERM. SUST.") & ""))
                        End If
                        If Not IsDBNull(dtEndoso.Rows(j)("OBSERVACIONES")) Then
                            page.Add(rPosLeft + 389, intY + 293, New RepString(fp_pi, "" & dtEndoso.Rows(j)("OBSERVACIONES") & ""))
                        End If
                        intY += 8
                    Next
                    xnum = dtEndoso.Rows(0)("IMPORTE TITULO")
                    number = NumPalabra(Val(xnum))
                    intY += 125

                    page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp, "Lugar donde est�n almacenados los bienes"))
                    If dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Length > 70 Then
                        strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN").ToString.Substring(0, 70)
                    Else
                        strDireccionAlmacen = dtEndoso.Rows(0)("DIRECCION ALMACEN")
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 326, New RepString(fp_pi, "                                                                                     " & strDireccionAlmacen))
                    page.Add(intX + 200, (intY + 350) / 2.84 + 327, New RepString(fp, "____________________________________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp, "Modalidad de dep�sito:(Almac�n propio o de campo) "))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 338, New RepString(fp_pi, "                                                                                                      " & dtEndoso.Rows(0)("DESCRIPCION TIPO ALMACEN") & " "))
                    page.Add(intX + 230, (intY + 350) / 2.84 + 339, New RepString(fp, "______________________________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp, "El Valor patrimonial de los bienes depositados al emitir este t�tulo es de"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 356, New RepString(fp_pi, "                                                                                                                                             " & dtEndoso.Rows(0)("DESCRIPCION MONEDA") & " "))
                    page.Add(intX + 310, (intY + 350) / 2.84 + 357, New RepString(fp, "______________________________________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 370, New RepString(fp_pi, "                                 " & UCase(number) & ""))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 371, New RepString(fp, "_______________________________________________________________________________________________________"))
                    intY -= 60
                    page.Add(intX + 26, (intY + 350) / 2.84 + 410, New RepString(fp, "Sujeto a las disminuciones  correspondientes a los  retiros   que se  detallan   al  dorso, que  ser�n   anotadas    por   el almac�n   en"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 422, New RepString(fp, "el correspondiente t�tulo."))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 434, New RepString(fp, "Los bienes amparados por este t�tulo, estan afectos al pago de los derechos de aduana, tributos u otras cargas y se"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 446, New RepString(fp, "encuentran gravados con prenda a favor del Fisco hasta por el monto del adeudo que causa su despacho."))
                    intY += 15
                    If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "S" Then
                        page.Add(intX + 46, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                    ElseIf CStr(dtEndoso.Rows(0)("AFECTO AL PAGO ADUANA")) = "N" Then
                        page.Add(intX + 155, (intY + 350) / 2.84 + 456, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 456, New RepString(fp, "SI "))
                    page.Add(intX + 136, (intY + 350) / 2.84 + 456, New RepString(fp, "NO "))
                    page.Add(intX + 40, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 153, (intY + 350) / 2.84 + 458, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp, "Adeudos por el almacenaje, conservaci�n y operaciones conexas"))
                page.Add(intX + 26, (intY + 350) / 2.84 + 470, New RepString(fp_pi, "                                                                                                                                                                  ALMACENERA DEL PER�"))
                If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "S" Then
                        page.Add(intX + 40, (intY + 350) / 2.84 + 490, New RepString(fp_Certificacion, "X"))
                        page.Add(intX + 102, (intY + 350) / 2.84 + 490, New RepString(fp_pi, "                 " & dtEndoso.Rows(0)("MONEDA DEUDA") & " " & String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE ADEUDO"))))
                    ElseIf CStr(dtEndoso.Rows(0)("ADEUDO X ALMAC.")) = "N" Then
                        page.Add(intX + 40, (intY + 350) / 2.84 + 510, New RepString(fp_Certificacion, "X"))
                    End If
                    page.Add(intX + 26, (intY + 350) / 2.84 + 490, New RepString(fp, "Si "))
                    page.Add(intX + 39, (intY + 350) / 2.84 + 492, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 39, (intY + 350) / 2.84 + 512, New RepRectMM(ppBold, 4, 4))
                    page.Add(intX + 96, (intY + 350) / 2.84 + 490, New RepString(fp, "Importe"))
                    page.Add(intX + 145, (intY + 350) / 2.84 + 491, New RepString(fp, "_______________________"))
                    page.Add(intX + 26, (intY + 350) / 2.84 + 510, New RepString(fp, "No "))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 562, New RepString(fp, "  ____________________________________________                            ____________________________________________"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 572, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))

                    If blnFirmas = True Then
                        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(25, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(60, 283, New RepImageMM(stream_Firma, Double.NaN, 23))
                        stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(120, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                        stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                        page.AddMM(155, 283, New RepImageMM(stream_Firma, Double.NaN, 25))
                    End If
                    'Pinto el anexo del WIP
                    For j As Integer = 2 To intPaginas
                        page = New page(report)
                        page.rWidthMM = 210
                        page.rHeightMM = 314
                        intY = 110

                    page.Add(170, 20, New RepString(fp_header, "ANEXO AL TALON DE WARRANT INSUMO PRODUCTO"))
                    page.Add(209, 40, New RepString(fp_e, "Ley 27287, Resoluci�n SBS 019-2001"))
                        page.Add(217, 50, New RepString(fp_hc, "Warrant con certificado de Dep�sito emitido"))
                        page.Add(217, 60, New RepString(fp_hc, "Warrant sin certificado de Dep�sito emitido"))
                        page.Add(358, 51, New RepRectMM(ppBold, 4, 4))
                        page.Add(358, 65, New RepRectMM(ppBold, 4, 4))
                        If IsDBNull(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) Then
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "S" Then
                            page.Add(360, 49, New RepString(fp_Certificacion, "X"))
                        ElseIf CStr(dtEndoso.Rows(0)("CERT. DEPOSITO EMITIDO")) = "N" Then
                            page.Add(360, 63, New RepString(fp_Certificacion, "X"))
                        End If
                        'page.Add(460, 20, New RepString(fp_p, "   N�   " & CType(dtEndoso.Rows(0)("NUMERO TITULO"), Integer) & ""))
                        page.Add(370, 40, New RepString(fp_pi, "  N�"))
                        page.Add(365, 40, New RepString(fp_pi, "                         " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(383, 41, New RepString(fp_pi, "_____________________________"))

                        page.Add(125, 140, New RepString(fp, "Pagina N�"))
                        page.Add(165, 140, New RepString(fp_pi, "         " & j & ""))
                        page.Add(165, 141, New RepString(fp_pi, "_________________"))

                        If blnCopia = True Then
                            page.AddLT_MM(intXLeft + 35, 130, New RepRectMM(ppBoldSello, 110, 12))
                            page.Add(intXLeft + 150, 394, New RepString(fp_Sello, "COPIA SIN VALOR LEGAL"))

                        End If
                        page.Add(40, 720, New RepString(fp, " El presente detalle adicional de bienes forma parte integramente del warrant N�"))
                        page.Add(340, 720, New RepString(fp_pi, "       " & dtEndoso.Rows(0)("NUMERO TITULO") & ""))
                        page.Add(350, 721, New RepString(fp_pi, "_________________________________________"))
                        page.Add(320, 737, New RepString(fp, "                       de                         de"))
                        page.Add(320, 737, New RepString(fp_pi, "          " & dtEndoso.Rows(0)("DIA TITULO") & "                     " & dtEndoso.Rows(0)("MES TITULO") & "                     " & dtEndoso.Rows(0)("A�O TITULO") & ""))
                        page.Add(325, 738, New RepString(fp_pi, "__________"))
                        page.Add(390, 738, New RepString(fp_pi, "____________"))
                        page.Add(465, 738, New RepString(fp_pi, "__________"))
                    page.Add(400, 747, New RepString(fp_pi, " ALMACENERA DEL PER�"))
                    page.Add(intX + 46, (intY + 350) / 2.84 + 660, New RepString(fp, "  _______________________________________                                   _________________________________________"))
                        page.Add(intX + 46, (intY + 350) / 2.84 + 670, New RepString(fp, "                 Firma del Depositante                                                                              Firma y sello del representante Legal"))
                    'page.Add(40, (intY + 350) / 2.84 + 688, New RepString(fp, "DE-R-061-GW"))

                    If blnFirmas = True Then
                            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(20, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(55, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(120, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                            stream_Firma = New FileStream(strDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(155, 290, New RepImageMM(stream_Firma, Double.NaN, 25))
                        End If

                        page.Add(42, intY + 51, New RepString(fp_e, "Bienes en dep�sito(Insumos)                                                                                                       viene del T�tulo/de la p�gina N�"))
                        page.Add(365, intY + 51, New RepString(fp_pi, "                                                    " & (j - 1) & ""))
                        page.Add(450, intY + 52, New RepString(fp_pi, "_________________"))
                        page.Add(rPosLeft - 5, intY + 69, New RepString(fp_hc, "Cantidad"))
                        page.Add(rPosLeft + 49, intY + 69, New RepString(fp_hc, "Unidad y/o peso"))
                        page.Add(rPosLeft + 110, intY + 69, New RepString(fp_hc, "Descripci�n de los bienes (clases,especie)y marcas de los bultos"))
                        page.Add(rPosLeft + 335, intY + 64, New RepString(fp_hc, "Calidad y estado"))
                        page.Add(rPosLeft + 335, intY + 74, New RepString(fp_hc, "de conservacion"))
                        page.Add(rPosLeft + 395, intY + 64, New RepString(fp_hc, "del criterio de valorizaci�n"))
                        page.Add(rPosLeft + 390, intY + 74, New RepString(fp_hc, "Valor patrimonial con indicaci�n"))
                        page.Add(rPosLeft + 125, intY + 575, New RepString(fp_hc, "ADQUISICION SEGUN FACTURA INCLUYE I.G.V."))
                        intY -= 65
                        page.AddMM(intX, intY + 14, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, intY + 21, New RepLineMM(ppBold, 180, 0))
                        page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 180, 0))

                        page.AddMM(intX, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 19, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 39, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 120, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 140, (intY + 198), New RepLineMM(ppBold, 0, 184))
                        page.AddMM(intX + 180, (intY + 198), New RepLineMM(ppBold, 0, 184))

                        page.Add(rPosLeft + 125, intY + 630, New RepString(fp_hc, "" & dtEndoso.Rows(0)(53) & ""))

                        If j = intPaginas Then
                            page.AddMM(intX + 140, intY + 190, New RepLineMM(ppBold, 40, 0)) '----
                            page.Add(rPosLeft + 341, intY + 640, New RepString(fp_pi, "TOTAL"))
                            page.Add(rPosLeft + 390, intY + 640, New RepString(fp_pi, "" & dtEndoso.Rows(0)("SIMBOLO MONEDA") & ""))
                            page.AddRight(rPosLeft + 480, intY + 640, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(0)("IMPORTE TITULO"))))
                        End If

                        If ((j - 1) * (60)) + 19 < dtEndoso.Rows.Count Then
                            var = (j - 1) * (60) + 19
                        Else
                            var = (dtEndoso.Rows.Count - 1)
                        End If

                        For m As Integer = (j - 2) * (60) + 19 To var - 1
                            If Not IsDBNull(dtEndoso.Rows(m)("CANTIDAD")) Then
                                page.AddRight(rPosLeft + 40, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.000}", dtEndoso.Rows(m)("CANTIDAD"))))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("UNIDAD Y/O PESO")) Then
                                page.Add(rPosLeft + 49, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("UNIDAD Y/O PESO") & ""))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES")) Then
                                page.Add(rPosLeft + 110, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("DESCRIPCION DE LOS BIENES") & ""))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("ESTADO MERCADERIA")) Then
                                page.Add(rPosLeft + 335, intY + 150, New RepString(fp_pi, "" & dtEndoso.Rows(m)("ESTADO MERCADERIA") & ""))
                            End If
                            If Not IsDBNull(dtEndoso.Rows(m)("IMPORTE")) Then
                                page.AddRight(rPosLeft + 480, intY + 150, New RepString(fp_pi, String.Format("{0:##,##0.00}", dtEndoso.Rows(m)("IMPORTE"))))
                            End If
                            intY += 8
                        Next
                    Next
                End If
            End If
            'Next
            'Grabamos PDF por Documnto en la carperta filemaster-------------------------------------------
            If GrabarPDFxDocumento = True Then
            report.Save(strPathPDF & strNomPdf)
        Else
            '------ Vemos PDF con Varios Documentos  ------------------------
            RT.ViewPDF(report, strPathPDF & LstrNombrePDF)
        End If

        If blnFirmas = True Then
            stream_Firma.Close()
        End If
        dtEndoso2 = Nothing
        dtEndoso = Nothing
        dtFirmantes = Nothing
        Return "0"
    End Function

    'Public Function Letras(ByVal numero As String) As String
    '    '********Declara variables de tipo cadena************
    '    Dim palabras, entero, dec, flag As String

    '    '********Declara variables de tipo entero***********
    '    Dim num, x, y As Integer

    '    flag = "N"

    '    '**********N�mero Negativo***********
    '    If Mid(numero, 1, 1) = "-" Then
    '        numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
    '        palabras = "menos "
    '    End If

    '    '**********Si tiene ceros a la izquierda*************
    '    For x = 1 To numero.ToString.Length
    '        If Mid(numero, 1, 1) = "0" Then
    '            numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
    '            If Trim(numero.ToString.Length) = 0 Then palabras = ""
    '        Else
    '            Exit For
    '        End If
    '    Next

    '    '*********Dividir parte entera y decimal************
    '    For y = 1 To Len(numero)
    '        'If Mid(numero, y, 1) = "," Then
    '        If Mid(numero, y, 1) = "." Then
    '            flag = "S"
    '        Else
    '            If flag = "N" Then
    '                entero = entero + Mid(numero, y, 1)
    '            Else
    '                dec = dec + Mid(numero, y, 1)
    '            End If
    '        End If
    '    Next y

    '    If Len(dec) = 1 Then dec = dec & "0"

    '    '**********proceso de conversi�n***********
    '    flag = "N"

    '    If Val(numero) <= 999999999 Then
    '        For y = Len(entero) To 1 Step -1
    '            num = Len(entero) - (y - 1)
    '            Select Case y
    '                Case 3, 6, 9
    '                    '**********Asigna las palabras para las centenas***********
    '                    Select Case Mid(entero, num, 1)
    '                        Case "1"
    '                            If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
    '                                palabras = palabras & "cien "
    '                            Else
    '                                palabras = palabras & "ciento "
    '                            End If
    '                        Case "2"
    '                            palabras = palabras & "doscientos "
    '                        Case "3"
    '                            palabras = palabras & "trescientos "
    '                        Case "4"
    '                            palabras = palabras & "cuatrocientos "
    '                        Case "5"
    '                            palabras = palabras & "quinientos "
    '                        Case "6"
    '                            palabras = palabras & "seiscientos "
    '                        Case "7"
    '                            palabras = palabras & "setecientos "
    '                        Case "8"
    '                            palabras = palabras & "ochocientos "
    '                        Case "9"
    '                            palabras = palabras & "novecientos "
    '                    End Select
    '                Case 2, 5, 8
    '                    '*********Asigna las palabras para las decenas************
    '                    Select Case Mid(entero, num, 1)
    '                        Case "1"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                flag = "S"
    '                                palabras = palabras & "diez "
    '                            End If
    '                            If Mid(entero, num + 1, 1) = "1" Then
    '                                flag = "S"
    '                                palabras = palabras & "once "
    '                            End If
    '                            If Mid(entero, num + 1, 1) = "2" Then
    '                                flag = "S"
    '                                palabras = palabras & "doce "
    '                            End If
    '                            If Mid(entero, num + 1, 1) = "3" Then
    '                                flag = "S"
    '                                palabras = palabras & "trece "
    '                            End If
    '                            If Mid(entero, num + 1, 1) = "4" Then
    '                                flag = "S"
    '                                palabras = palabras & "catorce "
    '                            End If
    '                            If Mid(entero, num + 1, 1) = "5" Then
    '                                flag = "S"
    '                                palabras = palabras & "quince "
    '                            End If
    '                            If Mid(entero, num + 1, 1) > "5" Then
    '                                flag = "N"
    '                                palabras = palabras & "dieci"
    '                            End If
    '                        Case "2"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "veinte "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "veinti"
    '                                flag = "N"
    '                            End If
    '                        Case "3"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "treinta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "treinta y "
    '                                flag = "N"
    '                            End If
    '                        Case "4"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "cuarenta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "cuarenta y "
    '                                flag = "N"
    '                            End If
    '                        Case "5"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "cincuenta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "cincuenta y "
    '                                flag = "N"
    '                            End If
    '                        Case "6"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "sesenta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "sesenta y "
    '                                flag = "N"
    '                            End If
    '                        Case "7"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "setenta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "setenta y "
    '                                flag = "N"
    '                            End If
    '                        Case "8"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "ochenta "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "ochenta y "
    '                                flag = "N"
    '                            End If
    '                        Case "9"
    '                            If Mid(entero, num + 1, 1) = "0" Then
    '                                palabras = palabras & "noventa "
    '                                flag = "S"
    '                            Else
    '                                palabras = palabras & "noventa y "
    '                                flag = "N"
    '                            End If
    '                    End Select
    '                Case 1, 4, 7
    '                    '*********Asigna las palabras para las unidades*********
    '                    Select Case Mid(entero, num, 1)
    '                        Case "1"
    '                            If flag = "N" Then
    '                                If y = 1 Then
    '                                    palabras = palabras & "uno "
    '                                Else
    '                                    palabras = palabras & "un "
    '                                End If
    '                            End If
    '                        Case "2"
    '                            If flag = "N" Then palabras = palabras & "dos "
    '                        Case "3"
    '                            If flag = "N" Then palabras = palabras & "tres "
    '                        Case "4"
    '                            If flag = "N" Then palabras = palabras & "cuatro "
    '                        Case "5"
    '                            If flag = "N" Then palabras = palabras & "cinco "
    '                        Case "6"
    '                            If flag = "N" Then palabras = palabras & "seis "
    '                        Case "7"
    '                            If flag = "N" Then palabras = palabras & "siete "
    '                        Case "8"
    '                            If flag = "N" Then palabras = palabras & "ocho "
    '                        Case "9"
    '                            If flag = "N" Then palabras = palabras & "nueve "
    '                    End Select
    '            End Select

    '            '***********Asigna la palabra mil***************
    '            'If y = 4 Then
    '            '    If Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or _
    '            '    (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
    '            '    Len(entero) <= 6) Then palabras = palabras & "mil "
    '            'End If
    '            If y = 4 Then
    '                'Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or _
    '                Select Case Len(entero)
    '                    Case 5
    '                        If Mid(entero, Len(entero) - 3, 1) <> "0" Or Mid(entero, Len(entero) - 4, 1) <> "0" Then
    '                            palabras = palabras & "mil "
    '                            flag = "N"
    '                        End If
    '                    Case 4
    '                        If Mid(entero, Len(entero) - 3, 1) <> "0" Then
    '                            palabras = palabras & "mil "
    '                            flag = "N"
    '                        End If
    '                    Case Is > 6
    '                        If Mid(entero, Len(entero) - 3, 1) <> "0" Or Mid(entero, Len(entero) - 4, 1) <> "0" Or Mid(entero, Len(entero) - 5, 1) <> "0" Then
    '                            palabras = palabras & "mil "
    '                            flag = "N"
    '                        End If
    '                End Select
    '            'If Mid(entero, Len(entero) - 3, 1) <> "0" Or Mid(entero, Len(entero) - 4, 1) <> "0" Or Mid(entero, Len(entero) - 5, 1) <> "0" Then palabras = palabras & "mil "

    '            'If (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
    '            'Mid(entero, 2, 1) <> "0" And Len(entero) = 7) Or _
    '            '(Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
    '            'Mid(entero, 3, 1) <> "0" And Len(entero) = 7) Or _
    '            '(Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
    '            'Mid(entero, 3, 1) <> "0" And Len(entero) = 8) Or _
    '            '(Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
    '            'Len(entero) <= 6) Then palabras = palabras & "mil "
    '            End If
    '            '**********Asigna la palabra mill�n*************
    '            If y = 7 Then
    '                If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
    '                    palabras = palabras & "mill�n "
    '                Else
    '                    palabras = palabras & "millones "
    '                End If
    '            End If
    '        Next y

    '        '**********Une la parte entera y la parte decimal*************

    '        If dec <> "" Then
    '            Letras = palabras & "CON " & dec.Substring(0, 2) & " /100"
    '        Else
    '            Letras = palabras & "CON 00/100"
    '        End If
    '    Else
    '        Letras = ""
    '    End If
    'End Function

    Dim cMontext As String
    Public Function NumPalabra(ByVal Numero As Double) As String
        Dim Numerofmt As String
        Dim centenas As Integer
        Dim pos As Integer
        Dim cen As Integer
        Dim dec As Integer
        Dim uni As Integer
        Dim textuni As String
        Dim textdec As String
        Dim textcen As String
        Dim milestxt As String
        Dim monedatxt As String
        Dim txtPalabra As String

        Numerofmt = Format(Numero, "000000000000000.00") 'Le da un formato fijo
        centenas = 1
        pos = 1
        txtPalabra = ""
        Do While centenas <= 5
            ' extrae series de Centena, Decena, Unidad
            cen = Val(Mid(Numerofmt, pos, 1))
            dec = Val(Mid(Numerofmt, pos + 1, 1))
            uni = Val(Mid(Numerofmt, pos + 2, 1))
            pos = pos + 3
            textcen = Centena(uni, dec, cen)
            textdec = Decena(uni, dec)
            textuni = Unidad(uni, dec, pos)
            ' determina separador de miles/millones
            Select Case centenas
                Case 1
                    If cen + dec + uni = 1 Then
                        milestxt = "Billon "
                    ElseIf cen + dec + uni > 1 Then
                        milestxt = "Billones "
                    End If
                Case 2
                    If cen + dec + uni >= 1 And Val(Mid(Numerofmt, 7, 3)) = 0 Then
                        milestxt = "Mil Millones "
                    ElseIf cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 3
                    If cen + dec = 0 And uni = 1 Then
                        milestxt = "Millon "
                    ElseIf cen > 0 Or dec > 0 Or uni > 1 Then
                        milestxt = "Millones "
                    End If
                Case 4
                    If cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 5
                    If cen + dec + uni >= 1 Then
                        milestxt = ""
                    End If
            End Select
            centenas = centenas + 1
            'va formando el texto del importe en palabras
            txtPalabra = txtPalabra + textcen + textdec + textuni + milestxt
            milestxt = ""
            textuni = ""
            textdec = ""
            textcen = ""
        Loop
        ' agrega denominacion de moneda
        'Select Case Val(Numerofmt)
        '    Case 0
        '        monedatxt = "Cero Pesos "
        '    Case 1
        '        monedatxt = "Peso "
        '    Case Is < 1000000
        '        monedatxt = "Pesos "
        '    Case Is >= 1000000
        '        monedatxt = "de Pesos "
        'End Select
        'txtPalabra = txtPalabra & monedatxt & "con " & Mid(Numerofmt, 17) & "/100"
        txtPalabra = txtPalabra & "con " & Mid(Numerofmt, 17) & "/100"
        Return txtPalabra.ToUpper
    End Function

    Private Function Centena(ByVal uni As Integer, ByVal dec As Integer, _
        ByVal cen As Integer) As String
        Select Case cen
            Case 1
                If dec + uni = 0 Then
                    cMontext = "cien "
                Else
                    cMontext = "ciento "
                End If
            Case 2 : cMontext = "doscientos "
            Case 3 : cMontext = "trescientos "
            Case 4 : cMontext = "cuatrocientos "
            Case 5 : cMontext = "quinientos "
            Case 6 : cMontext = "seiscientos "
            Case 7 : cMontext = "setecientos "
            Case 8 : cMontext = "ochocientos "
            Case 9 : cMontext = "novecientos "
            Case Else : cMontext = ""
        End Select
        Centena = cMontext
        cMontext = ""
    End Function

    Private Function Decena(ByVal uni As Integer, ByVal dec As Integer) As String
        Select Case dec
            Case 1
                Select Case uni
                    Case 0 : cMontext = "diez "
                    Case 1 : cMontext = "once "
                    Case 2 : cMontext = "doce "
                    Case 3 : cMontext = "trece "
                    Case 4 : cMontext = "catorce "
                    Case 5 : cMontext = "quince "
                    Case 6 To 9 : cMontext = "dieci"
                End Select
            Case 2
                If uni = 0 Then
                    cMontext = "veinte "
                ElseIf uni > 0 Then
                    cMontext = "veinti"
                End If
            Case 3 : cMontext = "treinta "
            Case 4 : cMontext = "cuarenta "
            Case 5 : cMontext = "cincuenta "
            Case 6 : cMontext = "sesenta "
            Case 7 : cMontext = "setenta "
            Case 8 : cMontext = "ochenta "
            Case 9 : cMontext = "noventa "
            Case Else : cMontext = ""
        End Select
        If uni > 0 And dec > 2 Then cMontext = cMontext + "y "
        Decena = cMontext
        cMontext = ""
    End Function

    Private Function Unidad(ByVal uni As Integer, ByVal dec As Integer, ByVal pos As Integer) As String
        If dec <> 1 Then
            Select Case uni
                Case 1
                    If pos = 16 Then
                        cMontext = "uno "
                    Else
                        cMontext = "un "
                    End If
                Case 2 : cMontext = "dos "
                Case 3 : cMontext = "tres "
                Case 4 : cMontext = "cuatro "
                Case 5 : cMontext = "cinco "
            End Select
        End If
        Select Case uni
            Case 6 : cMontext = "seis "
            Case 7 : cMontext = "siete "
            Case 8 : cMontext = "ocho "
            Case 9 : cMontext = "nueve "
        End Select
        Unidad = cMontext
        cMontext = ""
    End Function

    Public Function gCNGetListarFirmas(ByVal strCodEntidad As String) As DataTable
        Return objDocumento.gCADGetListarFirmas(strCodEntidad)
    End Function

    Public Function gCNGetDetalleWarrant(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strTipTitulo As String, ByVal strNumTitulo As String) As DataSet
        'ByVal strCodFinanciera As String) As DataSet
        Return objDocumento.gCADGetDetalleWarrant(strCodEmpresa, strCodUnidad, strTipTitulo, strNumTitulo)
    End Function

    Public Function GetReportePDF(ByVal strCod As String, ByVal strPathPDF As String,
                            ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String, ByVal strIdUsuario As String,
                            ByVal ObjTrans As SqlTransaction) As String
        Dim dtFirmantes As DataTable
        Dim Imagen1 As String = String.Empty
        Dim Imagen2 As String = String.Empty

        Dim strTipDocFirm As String

        dtFirmantes = objDocumento.GetFirmantes(strCod, ObjTrans) 'ok(sin sessiones internas)
        '=================== si el pdf no fue vizualizado, creamos el pdf =========================

        strTipDocFirm = CType(dtFirmantes.Rows(0)("COD_TIPDOC"), String).Trim
        If strTipDocFirm = "09" Or strTipDocFirm = "17" Or strTipDocFirm = "18" Or strTipDocFirm = "19" Then
            If IsDBNull(dtFirmantes.Rows(0)("DOC_PDFORIG")) Then
                'InsGrabaPDF(CType(dtFirmantes.Rows(0)("NOM_PDF"), String).Trim)
                InsGrabaPDF(CType(dtFirmantes.Rows(0)("NOM_PDF"), String).Trim, strIdUsuario, CType(dtFirmantes.Rows(0)("NRO_DOCU"), String).Trim,
                            CType(dtFirmantes.Rows(0)("NRO_LIBE"), String).Trim, CType(dtFirmantes.Rows(0)("PRD_DOCU"), String).Trim, CType(dtFirmantes.Rows(0)("COD_TIPDOC"), String).Trim)
                'volver a consultar el pdf creado
                dtFirmantes = objDocumento.GetFirmantes(strCod, ObjTrans) 'ok(sin sessiones internas)
            End If
        End If
        '==========================================================================================


        If strTipDoc = "19" Then
            LstrNombrePDF = dtFirmantes.Rows(0)("NOM_PDF")
        Else
            LstrNombrePDF = "DCR" & dtFirmantes.Rows(0)("NOM_PDF")
        End If

        If blnFirmas = True Then
            If dtFirmantes.Rows(0)("FIR_DEPSA1") = "" Then
                Imagen1 = strPathFirmas & "aspl.JPG"
            Else
                Imagen1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA1")
            End If
            If dtFirmantes.Rows(0)("FIR_DEPSA2") = "" Then
                Imagen2 = strPathFirmas & "aspl.JPG"
            Else
                Imagen2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA2")
            End If
        End If

        Dim pdfNuevo As String = strPathPDF & LstrNombrePDF
        Dim pdfOriginal As Byte() = CType(dtFirmantes.Rows(0)("DOC_PDFORIG"), Byte())
        Dim pdfReader As PdfReader = New PdfReader(pdfOriginal)
        Dim pdfStamper As PdfStamper = New PdfStamper(pdfReader, New FileStream(pdfNuevo, FileMode.Create))
        Dim pdfFormFields As AcroFields = pdfStamper.AcroFields

        Dim chartImg1 As Image = Image.GetInstance(Imagen1)
        Dim chartImg2 As Image = Image.GetInstance(Imagen2)
        Dim underContent As pdf.PdfContentByte
        Dim rect As Rectangle
        Dim i As Integer

        Dim X, Y As Single
        Dim pageCount As Integer = 0
        X = 70
        Y = 80
        chartImg1.ScalePercent(75)
        chartImg1.SetAbsolutePosition(X, Y)

        X = 250
        Y = 80
        chartImg2.ScalePercent(75)
        chartImg2.SetAbsolutePosition(X, Y)

        pageCount = pdfReader.NumberOfPages
        For i = 1 To pageCount
            If strTipDoc = "19" And i = pageCount Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImg1)
                underContent.AddImage(chartImg2)
            End If
            If strTipDoc <> "19" Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImg1)
                underContent.AddImage(chartImg2)
            End If
        Next
        'underContent.PdfDocument.NewPage()
        pdfStamper.Close()
        pdfReader.Close()
        Return "0"
    End Function

    Private Sub InsGrabaPDF(ByVal strNombArch As String, strIdUsuario As String, strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strIdTipoDocumento As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, Contenido, strIdTipoDocumento)
        fs.Close()
    End Sub

    Public Function GetReporteRetiroPDF(ByVal strCod As String, ByVal strPathPDF As String, _
                            ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String, _
                            ByVal ObjTrans As SqlTransaction) As String
        Dim dtFirmantes As DataTable
        Dim ImagenClie1 As String = String.Empty
        Dim ImagenClie2 As String = String.Empty
        Dim ImagenDepsa1 As String = String.Empty
        Dim ImagenDepsa2 As String = String.Empty
        Dim ImagenVBcob As String = String.Empty
        Dim Contenido As Byte()
        Dim resultado As Byte()
        Dim strDescriptado As String
        Dim Parametro As String

        dtFirmantes = objDocumento.GetFirmantes(strCod, ObjTrans)
        LstrNombrePDF = Trim(dtFirmantes.Rows(0)("NOM_PDF"))
        If strTipDoc = "22" Then
            If blnFirmas = True Then
                If dtFirmantes.Rows(0)("FIR_CLIE1") = "" Then
                    ImagenClie1 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenClie1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE1")
                End If

                If dtFirmantes.Rows(0)("FIR_CLIE2") = "" Then
                    ImagenClie2 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenClie2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE2")
                End If
                ImagenDepsa1 = strPathFirmas & "aspl.JPG"
                ImagenDepsa2 = strPathFirmas & "aspl.JPG"
            End If
        End If

        If strTipDoc = "23" Then
            If blnFirmas = True Then
                If dtFirmantes.Rows(0)("FIR_CLIE1") = "" Then
                    ImagenClie1 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenClie1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE1")
                End If

                If dtFirmantes.Rows(0)("FIR_CLIE2") = "" Then
                    ImagenClie2 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenClie2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE2")
                End If

                If dtFirmantes.Rows(0)("FIR_DEPSA1") = "" Then
                    ImagenDepsa1 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenDepsa1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA1")
                End If

                If dtFirmantes.Rows(0)("FIR_DEPSA2") = "" Then
                    ImagenDepsa2 = strPathFirmas & "aspl.JPG"
                Else
                    ImagenDepsa2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA2")
                End If
            End If
        End If
        ImagenVBcob = strPathFirmas & "VBCOB.JPG"
        Dim pdfNuevo As String = strPathPDF & LstrNombrePDF
        Contenido = CType(dtFirmantes.Rows(0)("DOC_PDFORIG"), Byte())
        Dim pdfReader As pdfReader = New pdfReader(Contenido)
        Dim pdfStamper As pdfStamper = New pdfStamper(pdfReader, New FileStream(pdfNuevo, FileMode.Create))

        Dim pdfFormFields As AcroFields = pdfStamper.AcroFields
        Dim chartImgClie1 As Image = Image.GetInstance(ImagenClie1)
        Dim chartImgClie2 As Image = Image.GetInstance(ImagenClie2)
        Dim chartImgDepsa1 As Image = Image.GetInstance(ImagenDepsa1)
        Dim chartImgDepsa2 As Image = Image.GetInstance(ImagenDepsa2)
        Dim chartImgVBcob As Image = Image.GetInstance(ImagenVBcob)

        Dim underContent As iTextSharp.text.pdf.PdfContentByte
        Dim rect As iTextSharp.text.Rectangle
        Dim i As Integer

        Dim X, Y As Single
        Dim pageCount As Integer = 0
        If strTipDoc = "22" Then
            X = 65
            Y = 90
            chartImgClie1.ScalePercent(50)
            chartImgClie1.SetAbsolutePosition(X, Y)

            X = 200
            Y = 90
            chartImgClie2.ScalePercent(50)
            chartImgClie2.SetAbsolutePosition(X, Y)
        End If

        If strTipDoc = "23" Then
            X = 65
            Y = 90
            chartImgClie1.ScalePercent(50)
            chartImgClie1.SetAbsolutePosition(X, Y)

            X = 200
            Y = 90
            chartImgClie2.ScalePercent(50)
            chartImgClie2.SetAbsolutePosition(X, Y)

            X = 65
            Y = 180
            chartImgDepsa1.ScalePercent(55)
            chartImgDepsa1.SetAbsolutePosition(X, Y)

            X = 200
            Y = 180
            chartImgDepsa2.ScalePercent(55)
            chartImgDepsa2.SetAbsolutePosition(X, Y)
        End If

        X = 55
        Y = 65
        chartImgVBcob.ScalePercent(65)
        chartImgVBcob.SetAbsolutePosition(X, Y)


        pageCount = pdfReader.NumberOfPages
        For i = 1 To pageCount
            If strTipDoc = "22" Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImgClie1)
                underContent.AddImage(chartImgClie2)
            End If

            If strTipDoc = "23" Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImgClie1)
                underContent.AddImage(chartImgClie2)
                underContent.AddImage(chartImgDepsa1)
                underContent.AddImage(chartImgDepsa2)
            End If
            underContent.AddImage(chartImgVBcob)
        Next
        pdfStamper.Close()
        pdfReader.Close()
        Return "0"
    End Function

    Public Function GetReporteWarrantCustodiaPDF(ByVal strCod As String, ByVal strPathPDF As String,
                           ByVal strPathFirmas As String, ByVal blnFirmas As Boolean, ByVal strTipDoc As String,
                           ByVal ObjTrans As SqlTransaction) As String
        Dim dtFirmantes As DataTable
        Dim Imagen1 As String = String.Empty
        Dim Imagen2 As String = String.Empty
        Dim Imagen3 As String = String.Empty
        Dim Imagen4 As String = String.Empty

        dtFirmantes = objDocumento.GetFirmantes(strCod, ObjTrans)
        LstrNombrePDF = dtFirmantes.Rows(0)("NOM_PDF")

        If blnFirmas = True Then
            If dtFirmantes.Rows(0)("FIR_DEPSA1") = "" Then
                Imagen1 = strPathFirmas & "aspl.JPG"
            Else
                Imagen1 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA1")
            End If

            If dtFirmantes.Rows(0)("FIR_DEPSA2") = "" Then
                Imagen2 = strPathFirmas & "aspl.JPG"
            Else
                Imagen2 = strPathFirmas & dtFirmantes.Rows(0)("FIR_DEPSA2")
            End If

            If dtFirmantes.Rows(0)("FIR_CLIE1") = "" Then
                Imagen3 = strPathFirmas & "aspl.JPG"
            Else
                Imagen3 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE1")
            End If

            If dtFirmantes.Rows(0)("FIR_CLIE2") = "" Then
                Imagen4 = strPathFirmas & "aspl.JPG"
            Else
                Imagen4 = strPathFirmas & dtFirmantes.Rows(0)("FIR_CLIE2")
            End If
        End If

        Dim pdfNuevo As String = strPathPDF & LstrNombrePDF
        Dim pdfOriginal As Byte() = CType(dtFirmantes.Rows(0)("DOC_PDFORIG"), Byte())
        Dim pdfReader As pdfReader = New pdfReader(pdfOriginal)
        Dim pdfStamper As pdfStamper = New pdfStamper(pdfReader, New FileStream(pdfNuevo, FileMode.Create))
        Dim pdfFormFields As AcroFields = pdfStamper.AcroFields

        Dim chartImg1 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Imagen1)
        Dim chartImg2 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Imagen2)
        Dim chartImg3 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Imagen3)
        Dim chartImg4 As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Imagen4)

        Dim underContent As iTextSharp.text.pdf.PdfContentByte
        Dim rect As iTextSharp.text.Rectangle
        Dim i As Integer

        Dim X, Y As Single
        Dim pageCount As Integer = 0
        X = 315
        Y = 57
        chartImg1.ScalePercent(42)
        chartImg1.SetAbsolutePosition(X, Y)

        X = 435
        Y = 57
        chartImg2.ScalePercent(42)
        chartImg2.SetAbsolutePosition(X, Y)

        X = 102
        Y = 57
        chartImg3.ScalePercent(32)
        chartImg3.SetAbsolutePosition(X, Y)

        X = 180
        Y = 57
        chartImg4.ScalePercent(32)
        chartImg4.SetAbsolutePosition(X, Y)

        pageCount = pdfReader.NumberOfPages
        For i = 1 To pageCount
            If strTipDoc = "20" And i = 1 Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImg1)
                underContent.AddImage(chartImg2)
                underContent.AddImage(chartImg3)
                underContent.AddImage(chartImg4)
            End If
            If strTipDoc = "20" And i = 2 Then
                underContent = pdfStamper.GetOverContent(i)
                underContent.AddImage(chartImg1)
                underContent.AddImage(chartImg2)
                underContent.AddImage(chartImg3)
                underContent.AddImage(chartImg4)
            End If
        Next
        pdfStamper.Close()
        pdfReader.Close()
        Return "0"
    End Function






    'Public Function GetReporteTraslado(ByVal strCodDoc As Integer, ByVal strPathPDF As String) As String
    '    Dim report As report = New report(New PdfFormatter)
    '    Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
    '    Dim fd_arial As FontDef = New FontDef(report, "Arial")
    '    Dim fp_hc As FontProp = New FontPropMM(fd, 1.8)
    '    Dim fp_pi As FontProp = New FontPropMM(fd_arial, 1.9)
    '    Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.0)
    '    Dim fp As FontProp = New FontPropMM(fd, 2.2)
    '    Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.4)
    '    Dim fp_header As FontProp = New FontPropMM(fd, 3.5)
    '    Dim fp_e As FontProp = New FontPropMM(fd, 1.8)
    '    Dim fp_h As FontProp = New FontPropMM(fd, 1.9)
    '    Dim fp_p As FontProp = New FontPropMM(fd, 2.8)
    '    Dim pp As PenProp = New PenProp(report, 0.8)
    '    Dim ppBold As PenProp = New PenProp(report, 1.1)
    '    Dim fp_b As FontProp = New FontPropMM(fd, 2.1)
    '    Dim ppBoldSello As PenProp = New PenProp(report, 1.6, System.Drawing.Color.LightGray)
    '    Dim fp_Sello As FontProp = New FontPropMM(fd, 5.5, System.Drawing.Color.LightGray)
    '    fp_Certificacion.bBold = True
    '    fp_p.bBold = True
    '    fp_e.bBold = True
    '    'fp_pi.bBold = True
    '    Dim dtEndoso As DataTable
    '    Dim page As page

    '    Dim strError As String
    '    Dim intPaginas As Decimal
    '    Dim intXLeft As Integer
    '    Dim intXRigth As Integer
    '    Dim intYTop As Integer
    '    Dim number As String
    '    Dim strNumLinea As String
    '    '***********************************************
    '    Dim strCliente As String
    '    Dim strBanco As String
    '    Dim intNumReg As Integer
    '    Dim intRegDsc As Integer
    '    Dim intTotPaginas As Integer
    '    Dim intNumDsc As Integer
    '    Dim intContadorGeneral As Integer
    '    '***********************************************
    '    dtEndoso = objDocumento.GetInformacionTraslado(strCodDoc)
    '    If dtEndoso.Rows.Count = 0 Then
    '        Return "1"
    '    End If


    '    For u As Integer = 0 To dtEndoso.Rows.Count - 1
    '        Dim NumDsc As String
    '        number = Len(dtEndoso.Rows(u)("DE_MERC")) / 40
    '        intTotPaginas = intTotPaginas + number
    '    Next

    '    If intTotPaginas > 14 Then
    '        If intTotPaginas / 14 - CInt(intTotPaginas / 14) > 0 Then
    '            intPaginas = CInt(intTotPaginas / 14) + 1
    '        Else
    '            intPaginas = CInt(intTotPaginas / 14)
    '        End If
    '    Else
    '        intPaginas = 1
    '    End If


    '    fp_header.bBold = True

    '    strCliente = dtEndoso.Rows(0)("NO_CLIE_REPO")
    '    strBanco = dtEndoso.Rows(0)("DE_LARG_ENFI")
    '    intNumReg = dtEndoso.Rows.Count

    '    For p As Int16 = 1 To intPaginas
    '        page = New page(report)
    '        page.rWidthMM = 210
    '        page.rHeightMM = 297
    '        page.SetLandscape()
    '        intYTop = 120
    '        intXLeft = 25


    '        page.Add(intXLeft + 300, intYTop - 60, New RepString(fp_p, "SOLICITUD DE TRASLADO N� " & strCodDoc))
    '        page.Add(intXLeft + 700, intYTop - 10, New RepString(fp_Titulo, "Pag." & p))

    '        If p = 1 Then
    '            page.Add(intXLeft + 55, intYTop, New RepString(fp_Titulo, "Los Se�or(es) de la empresa "))

    '            page.Add(intXLeft + 180, intYTop, New RepString(fp_pi, Trim(strCliente)))
    '            page.Add(intXLeft + 298, intYTop, New RepString(fp_Titulo, "han solicitado el traslado de los siguientes �tems:"))
    '        End If

    '        intYTop = 80

    '        page.Add(intXLeft + 48, intYTop + 70, New RepString(fp_Titulo, "N� Warrant"))
    '        page.Add(intXLeft + 100, intYTop + 70, New RepString(fp_Titulo, "�tem"))
    '        page.Add(intXLeft + 150, intYTop + 70, New RepString(fp_Titulo, "Descripci�n"))
    '        page.Add(intXLeft + 298, intYTop + 70, New RepString(fp_Titulo, "Unidad"))
    '        page.Add(intXLeft + 380, intYTop + 70, New RepString(fp_Titulo, "Alm. Origen"))
    '        page.Add(intXLeft + 580, intYTop + 70, New RepString(fp_Titulo, "Alm. Destino"))

    '        page.AddMM(intXLeft, 47, New RepLineMM(ppBold, 240, 0))
    '        page.AddMM(intXLeft, 56, New RepLineMM(ppBold, 240, 0))

    '        If (p - 1) * (14) + 14 < dtEndoso.Rows.Count Then
    '            intPaginas = (p - 1) * (14) + 14
    '        Else
    '            intPaginas = dtEndoso.Rows.Count
    '        End If

    '        'For j As Int16 = (p - 1) * (20) To intPaginas - 1

    '        If p = 1 Then
    '            intContadorGeneral = 0
    '        End If

    '        'For j As Int16 = (p - 1) * (14) To intPaginas - 1
    '        For j As Int16 = intContadorGeneral To intPaginas - 1
    '            Dim cont As Integer

    '            If Not IsDBNull(dtEndoso.Rows(j)("nro_docu")) Then
    '                page.Add(intXLeft + 50, intYTop + 90, New RepString(fp_pi, dtEndoso.Rows(j)("nro_docu")))
    '            End If


    '            If Not IsDBNull(dtEndoso.Rows(j)("NRO_ITEM")) Then
    '                page.AddRight(intXLeft + 104, intYTop + 90, New RepString(fp_pi, dtEndoso.Rows(j)("NRO_ITEM")))
    '            End If

    '            'If Not IsDBNull(dtEndoso.Rows(j)("DE_MERC")) Then
    '            '    page.Add(intXLeft + 130, intYTop + 90, New RepString(fp_pi, Mid((dtEndoso.Rows(j)("DE_MERC")), 1, 30)))

    '            'End If

    '            If Not IsDBNull(dtEndoso.Rows(j)("CO_TIPO_BULT")) Then
    '                page.Add(intXLeft + 302, intYTop + 90, New RepString(fp_pi, dtEndoso.Rows(j)("CO_TIPO_BULT")))
    '            End If

    '            If Not IsDBNull(dtEndoso.Rows(j)("ubi_orig")) Then
    '                page.Add(intXLeft + 320, intYTop + 90, New RepString(fp_pi, dtEndoso.Rows(j)("ubi_orig")))
    '            End If


    '            If Not IsDBNull(dtEndoso.Rows(j)("ubi_fina")) Then
    '                page.Add(intXLeft + 555, intYTop + 90, New RepString(fp_pi, dtEndoso.Rows(j)("ubi_fina")))
    '            End If

    '            '***********************************************
    '            ' Calcula partes de la descrcipci�n
    '            '***********************************************
    '            intNumDsc = gGetNumeroDsc(dtEndoso.Rows(j)("DE_MERC"))
    '            For w As Integer = 1 To UBound(strDes, 1)
    '                If Not IsDBNull(dtEndoso.Rows(j)("DE_MERC")) Then
    '                    page.Add(intXLeft + 116, intYTop + 90, New RepString(fp_pi, strDes(w)))
    '                    intYTop += 12
    '                End If

    '                intRegDsc = intRegDsc + 1
    '                If intRegDsc > 14 Then
    '                    Exit For
    '                End If
    '            Next
    '            '***********************************************
    '            cont = cont + 1
    '            If intRegDsc > 14 Then
    '                intRegDsc = 0
    '                intContadorGeneral = cont
    '                Exit For
    '            End If
    '            intYTop += 12
    '        Next
    '        intYTop = 90
    '        page.Add(intXLeft + 55, intYTop + 360, New RepString(fp_Titulo, "Autorizan el traslado:"))
    '        page.AddMM(80, 180, New RepLineMM(ppBold, 140, 0))
    '        page.AddCB(intYTop + 430, New RepString(fp_p, strBanco))
    '    Next

    '    RT.ViewPDF(report, strPathPDF & strCodDoc & ".pdf")
    '    dtEndoso = Nothing
    '    Return "0"
    'End Function

    Public Function gGetNumeroDsc(ByVal strDsc As String) As Integer
        Dim NumReg As Integer
        Dim NumDsc As Integer

        Dim strDescripcion As String
        Dim strNuevaDescripcion As String

        NumReg = Len(strDsc) / 40
        For i As Integer = 1 To NumReg
            NumDsc = i * 40
            strDescripcion = Mid(strDsc, NumDsc - 39, 40)
            strNuevaDescripcion = strNuevaDescripcion & "-*" & strDescripcion
        Next
        strDes = Split(strNuevaDescripcion, "-*")
        gGetNumeroDsc = NumReg
        Return gGetNumeroDsc
    End Function

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
