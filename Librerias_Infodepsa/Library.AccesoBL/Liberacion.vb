Imports System.IO
Imports Root.Reports
Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports System.Web.HttpContext

Public Class Liberacion
    Private cnn As New SqlConnection
    Private objDocumento As Documento = New Documento
    Dim sNU_DOCU_RECE = ""
    Public Function GetReporteLiberacion(ByVal strNombreReporte As String, ByVal strCoEmpresa As String, _
                                       ByVal strCoUnidad As String, ByVal strTipoDocumento As String, _
                                       ByVal strNroDocumento As String, ByVal strEntidad As String, _
                                       ByVal dtResumen As DataTable, ByVal strTipoTitulo As String, _
                                       ByVal blnEmbarque As Boolean, ByVal blnContable As Boolean, _
                                       ByVal blnTraslado As Boolean, Optional ByVal strRutaFirmas As String = "") As String
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        Dim page As page
        Dim stream_Firma As Stream
        Dim dtLiberacion As DataTable
        Dim dtLiberacionWIP As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As DataSet
        Dim dsTRDIRE_CLIE As DataSet
        Dim dsTDRETI_MERC_0001 As DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim sDE_DIRE As String
        Dim sTI_TITU As String
        Dim sNU_TITU As String
        Dim sDE_LARG_ENFI As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String

        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_EMIS As String
        Dim sFE_CANC As String

        Dim strFirmaClie1 As String
        Dim strFirmaClie2 As String
        Dim strFechaFact As String()
        Dim dtFirmas As New DataTable
        Dim intCuadro As Int16 = 0
        Dim intLine As Int16 = 0

        Dim intNroItem As Integer = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        dtLiberacion = fn_TDRETI_MERC_Q02(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        '-----------------------------------------------------------------------------
        If dtLiberacion.Rows.Count > 0 Then
            sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
            sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
            sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
            sTI_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO WARRANT")), "", dtLiberacion.Rows(0)("TIPO WARRANT"))
            sNU_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO WARRANT")), "", dtLiberacion.Rows(0)("NUMERO WARRANT"))
            sNU_DOCU_RECE = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO COMPROBANTE")), "", dtLiberacion.Rows(0)("NUMERO COMPROBANTE"))
            sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
            sDE_LARG_ENFI = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE FINANCIADOR")), "", dtLiberacion.Rows(0)("NOMBRE FINANCIADOR"))
            If sDE_MODA_MOVI = "WARRANT ADUANERO" Then
                If Len(sDE_LARG_ENFI) >= 20 Then sDE_LARG_ENFI = Mid(sDE_LARG_ENFI, 1, 27)
            End If
            sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
            sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
            sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
            sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
            sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))

            sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
            sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
            sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
            sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
            sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
            sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
            sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
            sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
            sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
            sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
            sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))

            If sDE_MODA_MOVI = "WARRANT SIMPLE" Or sDE_MODA_MOVI = "WARRANT ADUANERO" Or sDE_MODA_MOVI.Trim = "WIP WARRANT INSUMO PRODUCT0" Then
                dsTDRETI_MERC_0001 = fn_TDRETI_MERC_Q06(strCoEmpresa, strCoUnidad, strTipoDocumento, _
                           strNroDocumento).Tables(0)
                If Not dsTDRETI_MERC_0001 Is Nothing Then
                    If dsTDRETI_MERC_0001.Rows.Count > 0 Then
                        Dim dwTDRETI_MERC_0001 As DataRow = dsTDRETI_MERC_0001.Rows(0)
                        Dim sTI_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("TI_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("TI_DOCU_RECE"))
                        Dim sNU_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("NU_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("NU_DOCU_RECE"))
                        Dim sFE_FACT As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("FE_FACT")), "", dwTDRETI_MERC_0001.Item("FE_FACT"))
                        Dim sCO_CLIE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU")), "", dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU"))
                        Dim sCO_TARI As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_TARI")), "", dwTDRETI_MERC_0001.Item("CO_TARI"))
                        Dim sCO_ALMA As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_ALMA")), "", dwTDRETI_MERC_0001.Item("CO_ALMA"))

                        sFE_FACT_SIGU = fn_FACT_SIGU_Q01(strCoEmpresa, strCoUnidad, sTI_DOCU_RECE, sNU_DOCU_RECE, sFE_FACT, sCO_CLIE, sCO_TARI, sCO_ALMA)

                    End If
                End If
            End If
        End If
        '-------------------------------------------------------------------------------
        'Obtiene nombre y ruc del cliente 
        dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
        If dsTMCLIE.Tables(0).Rows.Count > 0 Then
            sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
            sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
        End If

        'Obtiene direcci�n y tel�fono del cliente 
        dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, strCoUnidad)

        If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
            sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
            sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
        End If

        fp_header.bBold = True
        '--------------
        If dtResumen.Rows.Count > 20 Then
            If dtResumen.Rows.Count / 20 - CInt(dtResumen.Rows.Count / 20) > 0 Then
                intPaginas = CInt(dtResumen.Rows.Count / 20) + 1
            Else
                intPaginas = CInt(dtResumen.Rows.Count / 20)
            End If
        Else
            intPaginas = 1
        End If

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New page(report)
            page.rWidthMM = 210
            page.rHeightMM = 314

            strFirmaDepsa1 = strRutaFirmas & "Logo_Alma.JPG"
            'strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"

            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
            'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            'page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))
            If dtResumen.Rows(0)("FLG_FIRM") = True Then
                page.AddCB(intAltura + 15, New RepString(fp_Titulo, "(ELECTR�NICO)"))
            End If

            Select Case sDE_MODA_MOVI
                Case "WARRANT SIMPLE"
                    strTipoTitulo = "DEPOSITO FINANCIERO"
                Case "WARRANT ADUANERO"
                    strTipoTitulo = "DEPOSITO ADUANERO - FINANCIERO"
                Case "WIP WARRANT INSUMO PRODUCT0"
                    strTipoTitulo = "DEPOSITO FINANCIERO"
            End Select

            page.AddCB(intAltura, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS(DOR)"))
            page.Add(450, intAltura + 40, New RepString(fp_header, strNroDocumento))
            page.AddCB(intAltura + 40, New RepString(fp_Titulo, strTipoTitulo))
            intAltura += 30
            page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
            page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
            page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))
            page.Add(500, intAltura + 50, New RepString(fp_b, "Pag. " & p.ToString))
            page.Add(150, intAltura + 60, New RepString(fp, sFE_EMIS))
            page.Add(150, intAltura + 70, New RepString(fp, "R.U.C. 20100000688"))

            If blnEmbarque = True Then
                page.AddCB(intAltura + 90, New RepString(fp_Sello, "WARRANT ENDOSADO PARA EMBARQUE"))
            End If
            '---------------------------------------------------------------------
            If strTipoTitulo = "DEPOSITO ADUANERO - FINANCIERO" Then
                page.Add(50, intAltura + 100, New RepString(fp_b, "Pedido Dep�sito"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Conocimiento"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "P�liza Nro"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Fecha Numeraci�n"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Comprobante Pago"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Cod. Agente Ad."))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Direcci�n Almacen"))
                page.Add(50, intAltura + 170, New RepString(fp_b, "Tipo Retiro"))

                page.Add(140, intAltura + 100, New RepString(fp, ": " & sNU_PEDI_DEPO))
                page.Add(140, intAltura + 110, New RepString(fp, ": " & sNU_CONO_EMBA))
                page.Add(140, intAltura + 120, New RepString(fp, ": " & sNU_POLI_ADUA))
                page.Add(140, intAltura + 130, New RepString(fp, ": " & sFE_POLI_ADUA))
                page.Add(140, intAltura + 140, New RepString(fp, ": " & sNU_COMP_PAGO))
                page.Add(140, intAltura + 150, New RepString(fp, ": " & sCO_ENTI_AGAD))
                page.Add(140, intAltura + 160, New RepString(fp, ": " & sDE_DIRE_ALMA))
                page.Add(140, intAltura + 170, New RepString(fp, ": " & sTI_RETI_GIRA))

                page.Add(320, intAltura + 100, New RepString(fp_b, "Manifiesto"))
                page.Add(320, intAltura + 110, New RepString(fp_b, "Vapor"))
                page.Add(320, intAltura + 120, New RepString(fp_b, "Moneda Nacional"))
                page.Add(320, intAltura + 130, New RepString(fp_b, "Tipo Cambio"))
                page.Add(320, intAltura + 140, New RepString(fp_b, "Fecha de Cancelac"))
                page.Add(320, intAltura + 150, New RepString(fp_b, "N� de Warrant"))
                page.Add(320, intAltura + 170, New RepString(fp_b, "F. Pr�x. Fact"))

                page.Add(410, intAltura + 100, New RepString(fp, ": " & sNU_MANI))
                page.Add(410, intAltura + 110, New RepString(fp, ": " & sNO_VAPO))
                page.Add(410, intAltura + 120, New RepString(fp, ": " & sIM_ADUA_SIST))
                page.Add(410, intAltura + 130, New RepString(fp, ": " & sFA_CAMB_ADUA))
                page.Add(410, intAltura + 140, New RepString(fp, ": " & sFE_CANC))
                page.Add(410, intAltura + 150, New RepString(fp, ": " & sTI_TITU & " " & sNU_TITU))
                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If
                page.Add(410, intAltura + 170, New RepString(fp, ": " & sFE_FACT_SIGU))
                intAltura += 20
            Else
                page.Add(50, intAltura + 100, New RepString(fp_b, "Warrant                  :"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Ubicaci�n              :"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "Financiador           :"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Tipo Retiro             :"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Prox. Facturaci�n :"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Observaciones      :"))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Almac�n Destino   :"))

                page.Add(140, intAltura + 100, New RepString(fp, sTI_TITU & " " & sNU_TITU))
                page.Add(140, intAltura + 110, New RepString(fp, sDE_DIRE_ALMA))
                page.Add(140, intAltura + 120, New RepString(fp, sDE_LARG_ENFI))
                page.Add(140, intAltura + 130, New RepString(fp, sTI_RETI_GIRA))

                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If

                page.Add(140, intAltura + 140, New RepString(fp, sFE_FACT_SIGU))
                page.Add(140, intAltura + 150, New RepString(fp, sDE_OBSE))
                page.Add(140, intAltura + 160, New RepString(fp, dtResumen.Rows(0)("ALM_DEST")))
            End If
            '-----------------------------------------------------------------------------------
            intLine = (intAltura + 187) / 2.84
            page.AddMM(15, intLine - 5, New RepLineMM(ppBold, 175, 0))
            page.Add(50, intAltura + 187, New RepString(fp, "DCR"))
            page.Add(97, intAltura + 187, New RepString(fp, "Itm"))
            page.Add(115, intAltura + 187, New RepString(fp, "Bodega"))
            page.Add(150, intAltura + 187, New RepString(fp, "Cnt Unid."))
            page.Add(190, intAltura + 187, New RepString(fp, "Unidad"))
            page.Add(220, intAltura + 187, New RepString(fp, "Cnt Bulto"))
            page.Add(260, intAltura + 187, New RepString(fp, "Bulto"))
            page.Add(285, intAltura + 187, New RepString(fp, "Dsc de Mercader�a"))
            page.Add(435, intAltura + 187, New RepString(fp, "Prec.Unit."))
            page.Add(495, intAltura + 187, New RepString(fp, "Valor"))
            Dim decTotal As Decimal

            If (p - 1) * (20) + 20 < dtResumen.Rows.Count Then
                intNroPag = (p - 1) * (20) + 20
            Else
                intNroPag = dtResumen.Rows.Count
            End If

            For i As Int16 = (p - 1) * (20) To intNroPag - 1
                intAltura += 10
                If intNroItem <> dtResumen.Rows(i)("NRO_ITEM") Then
                    page.Add(50, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_COMP")))
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_ITEM")))
                    page.Add(120, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("COD_BODE")))
                    page.AddRight(180, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETI"))))
                    page.Add(195, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_ITEM")))
                    page.AddRight(255, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETBULT"))))
                    page.Add(260, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_BULT")))
                    page.AddRight(475, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("PRC_UNIT"))))
                    page.AddRight(535, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("TOTAL"))))
                    decTotal += Convert.ToDecimal(dtResumen.Rows(i)("TOTAL"))
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                Else
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                End If
                intNroItem = dtResumen.Rows(i)("NRO_ITEM")
            Next

            intLine = (intAltura + 197) / 2.84
            page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            If sDE_MODA_MOVI = "WIP WARRANT INSUMO PRODUCT0" And p = intPaginas Then
                intAltura += 20
                dtLiberacionWIP = objDocumento.gGetDetalleLiberacionWIP(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
                page.Add(120, intAltura + 187, New RepString(fp, "MATERIA PRIMA TRANSFORMADA            EN EL SGTE PRODUCTO TERMINADO"))
                For i As Int16 = 0 To dtLiberacionWIP.Rows.Count - 1
                    intAltura += 10
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("NU_SECU")))
                    page.AddRight(190, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_RETI"))))
                    page.Add(200, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("UNI_ITEM")))
                    page.Add(230, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_BULT"))))
                    If dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC")))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length > 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    End If
                Next
                intLine = (intAltura + 197) / 2.84
                page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            End If
            If blnContable = True And blnTraslado = True Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 63, 10))
                'page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para efectos contables por traslado de"))
                'page.Add(50, intAltura + 230, New RepString(fp_Sello, "mercader�a y para la emisi�n de un nuevo Warrant.'"))
                page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para traslado de mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y la emisi�n de un nuevo Warrant.'"))
            End If
            If blnContable = True And blnTraslado = False Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 73, 10))
                'page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para efectos contables sin movimiento f�sico de la"))
                'page.Add(50, intAltura + 230, New RepString(fp_Sello, "mercader�a y para la emisi�n de un nuevo Warrant.'"))
                page.Add(60, intAltura + 220, New RepString(fp_Sello, "'Sin entrega f�sica de la mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y para la emisi�n de un nuevo Warrant.'"))
            End If

            intLine = (intAltura + 210) / 2.84

            strFirmaDepsa1 = strRutaFirmas & "CLAUDIARODRIGUEZO.JPG"
            strFirmaDepsa2 = strRutaFirmas & "MARIOANDRADET.JPG"

            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(20, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))
            stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(70, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))

            If dtResumen.Rows(0)("FLG_CUST") = False Then
                dtFirmas = objDocumento.GetFirmantesLiberacion(strEntidad)
                Dim cfirmdep As Integer
                cfirmdep = objDocumento.gGetEntAprubCli(strEntidad).Rows.Count()
                If dtFirmas.Rows(0)("FIR_CLIE1") = "" Or cfirmdep <> "0" Then
                    strFirmaClie1 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaClie1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE1")
                End If
                If dtFirmas.Rows(0)("FIR_CLIE2") = "" Or cfirmdep <> "0" Then
                    strFirmaClie2 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaClie2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE2")
                End If
                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(20, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(70, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
            End If

            page.AddMM(15, intLine + 85, New RepLineMM(ppBold, 175, 0))
            If sNO_CLIE_REPO.Length > 40 And sNO_CLIE_REPO.Length <= 80 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, sNO_CLIE_REPO.Length - 40)))
            ElseIf sNO_CLIE_REPO.Length > 80 And sNO_CLIE_REPO.Length <= 120 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, 40)))
                page.Add(70, intAltura + 480, New RepString(fp_b, sNO_CLIE_REPO.Substring(80, sNO_CLIE_REPO.Length - 80)))
            Else
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO))
            End If

            If sDE_LARG_ENFI.Length > 40 And sDE_LARG_ENFI.Length <= 80 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, sDE_LARG_ENFI.Length - 40)))
            ElseIf sDE_LARG_ENFI.Length > 80 And sDE_LARG_ENFI.Length <= 120 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, 40)))
                page.Add(300, intAltura + 480, New RepString(fp_b, sDE_LARG_ENFI.Substring(80, sDE_LARG_ENFI.Length - 80)))
            Else
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI))
            End If

            page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de ALMACENERA DEL PER� S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
            page.Add(50, intAltura + 505, New RepString(fp, "del Warrant"))

            'page.Add(55, intAltura + 525, New RepString(fp, "V�B� COB."))
            'page.Add(55, intAltura + 540, New RepString(fp, "V�B� WAR."))
            ''--------------------------
            'page.Add(55, intAltura + 555, New RepString(fp, "DE-R-018-GAP"))
            '----------------
            If p <> intPaginas Then
                intAltura = 120
            Else
                page.AddRight(535, intAltura + 220, New RepString(fp_table, "Total: " & dtResumen.Rows(0)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", decTotal)))
            End If
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        dtLiberacionWIP = Nothing
        dtFirmas = Nothing
        stream_Firma = Nothing
        Return sNO_CLIE_REPO
    End Function

    Public Function fn_TDRETI_MERC_Q05(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
       ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String) As String

        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = "Select isnull(Max(t2.CO_MODA_MOVI), '') AS CODIGO_MODALIDAD " &
                    " From TDRETI_MERC t1 INNER JOIN  TCALMA_MERC t2 " &
                    " ON  t2.CO_EMPR = t1.CO_EMPR " &
                    " And t2.CO_UNID = t1.CO_UNID " &
                    " And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE " &
                    " And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE " &
                    " INNER JOIN TTMODA_MOVI t3 " &
                    " ON t3.CO_EMPR = t2.CO_EMPR " &
                    " And t3.CO_MODA_MOVI = t2.CO_MODA_MOVI " &
                    " Where  " &
                    " t1.CO_EMPR = '" & sCO_EMPR & "' " &
                    " And t1.CO_UNID ='" & sCO_UNID & "' " &
                    " And t1.TI_DOCU_RETI = '" & sTI_DOCU_RETI & "' " &
                    " And t1.NU_DOCU_RETI = '" & sNU_DOCU_RETI & "' " &
                    " And t1.TI_SITU = 'ACT' " &
                    " And t2.TI_SITU = 'ACT' " &
                    " And t3.ST_VALI_ADUA = 'S' " &
                    " And t3.ST_VALI_FINA = 'S' "

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        Dim sCO_MODA_MOVI As String = cmTDRETI_MERC.ExecuteScalar
        cnn.Close()
        Return sCO_MODA_MOVI
    End Function

    Private Function fn_TDRETI_MERC_Q02(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
        ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String) As DataTable
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sSQL As String
        sSQL = "Select TI_DIRE_FISC From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "' "
        Dim cmTDRETI_MERC As New SqlCommand(sSQL, cnn)
        cmTDRETI_MERC.CommandTimeout = 0
        Dim sTI_DIRE_FISC As String = cmTDRETI_MERC.ExecuteScalar
        sSQL = "Select Max(t4.DE_MODA_MOVI) 'MODALIDAD', " &
                "Max(t2.CO_CLIE_ACTU) 'COD CLIENTE', " &
                "NULL, " &
                "Max(t9.NU_RUCS_0001) 'RUC', " &
                "NULL, " &
                "Max(t1.NU_DOCU_RETI) 'NUMERO O/R', " &
                "CONVERT(VARCHAR(10),Max(t5.FE_USUA_CREA),103) + ' ' + CONVERT(VARCHAR(5), " &
                "Max(t5.FE_USUA_CREA),108)'FECHA O/R', " &
                "Max(t2.TI_TITU) 'TIPO WARRANT', " &
                "Max(t2.NU_TITU) 'NUMERO WARRANT', " &
                "Max(t1.NU_DOCU_RECE) 'NUMERO COMPROBANTE', " &
                "Substring(Max(t6.DE_DIRE_ALMA),1,80) 'DIRECCION DEL ALMACEN', " &
                "(Select Substring(DE_LARG_ENFI,1,50) From TMENTI_FINA Where CO_ENTI_FINA = Max(t3.CO_ENTI_FINA))'NOMBRE FINANCIADOR', " &
                "Max(Case When t5.TI_RETI_GIRA = 'T' Then 'TOTAL' Else 'PARCIAL' End) 'TIPO RETIRO', " &
                "MAX(ISNULL(T5.DE_OBSE_0001,'') + ' ' + ISNULL(T5.DE_OBSE_0002,'')) AS DE_OBSE, " &
                "Max(T5.NO_CHOF) as NO_CHOF, " &
                "Max(T5.NU_DNNI) as NU_DNNI, " &
                "Max(T5.NU_PLAC) as NU_PLAC, " &
                "Max(t7.NU_DUAS) AS PEDIDO_DEPOSITO, " &
                "Max(t7.NU_CONO_EMBA) AS CONOCIMIENTO_EMBARQUE, " &
                "Max(t5.NU_POLI_ADUA) 'P�LIZA', " &
                "Max(t5.FE_POLI_ADUA) 'FECHA NUMERACI�N', " &
                "Max(t5.NU_COMP_PAGO) 'COMPROBANTE PAGO', " &
                "Max(t5.CO_ENTI_AGAD) 'COD. AG. ADUANA', " &
                "Max(t7.NU_MANI) 'N�MERO MANIFIESTO', " &
                "Max(t7.NO_VAPO) 'NOMBRE VAPOR', " &
                "Max(t5.IM_ADUA_SIST) 'IMPORTE MONEDA NACIONAL', " &
                "Max(t5.FA_CAMB_ADUA) 'FACTOR CAMBIO', " &
                "Max(t5.FE_CANC) 'FECHA CANCELACI�N', " &
                "Max(t7.NU_PEDI_DEPO) AS CERTIFICADO_ADUANERO " &
                "From TDRETI_MERC t1 INNER JOIN TCRETI_MERC t5 " &
                "ON t5.CO_EMPR = t1.CO_EMPR " &
                "And t5.CO_UNID = t1.CO_UNID " &
                "And t5.TI_DOCU_RETI = t1.TI_DOCU_RETI " &
                "And t5.NU_DOCU_RETI = t1.NU_DOCU_RETI " &
                "INNER JOIN TCALMA_MERC t2  " &
                "ON t2.CO_EMPR = t1.CO_EMPR " &
                "And t2.CO_UNID = t1.CO_UNID " &
                "And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE " &
                "And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE " &
                "LEFT JOIN TCTITU t3 " &
                "ON t3.CO_EMPR = t2.CO_EMPR " &
                "And t3.TI_TITU = t2.TI_TITU " &
                "And t3.NU_TITU = t2.NU_TITU " &
                "LEFT JOIN TTMODA_MOVI t4  " &
                "ON t4.CO_EMPR = t2.CO_EMPR " &
                "And t4.CO_MODA_MOVI = t2.CO_MODA_MOVI " &
                "LEFT JOIN TMALMA t6 " &
                "ON t6.CO_EMPR = t5.CO_EMPR " &
                "And t6.CO_ALMA = t5.CO_ALMA " &
                "LEFT JOIN TMCLIE t9 " &
                "ON t9.CO_CLIE = t2.CO_CLIE_ACTU  " &
                "LEFT JOIN TCALME_ADUA t7 " &
                "ON t7.CO_EMPR = t2.CO_EMPR " &
                "And t7.CO_UNID = t2.CO_UNID " &
                "And t7.TI_DOCU_RECE = t2.TI_DOCU_RECE " &
                "And t7.NU_DOCU_RECE = t2.NU_DOCU_RECE " &
                "Where t1.CO_EMPR = '" & sCO_EMPR & "' " &
                "And t1.CO_UNID = '" & sCO_UNID & "' " &
                "And t1.TI_DOCU_RETI = '" & sTI_DOCU_RECE & "' " &
                "And t1.NU_DOCU_RETI = '" & sNU_DOCU_RECE & "' " &
                "And t1.TI_SITU = 'ACT' "

        cmTDRETI_MERC = New SqlCommand(sSQL, cnn)
        cmTDRETI_MERC.CommandTimeout = 0
        Dim daTDRETI_MERC As New SqlDataAdapter
        daTDRETI_MERC.SelectCommand = cmTDRETI_MERC
        Dim dsTDRETI_MERC As New DataSet
        daTDRETI_MERC.Fill(dsTDRETI_MERC)
        cnn.Close()
        Return dsTDRETI_MERC.Tables(0)
    End Function

    Private Function fn_TDRETI_MERC_Q02_Tran(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
        ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String, ByVal ObjTrans As SqlTransaction) As DataTable
        'If cnn.State = ConnectionState.Closed Then
        '    cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
        '    cnn.Open()
        'End If

        Dim sSQL As String
        '--------------------------------------------------------------------------
        Dim ds As DataSet
        sSQL =  "Select Max(t4.DE_MODA_MOVI) 'MODALIDAD', " &
                "Max(t2.CO_CLIE_ACTU) 'COD CLIENTE', " &
                "NULL, " &
                "Max(t9.NU_RUCS_0001) 'RUC', " &
                "NULL, " &
                "Max(t1.NU_DOCU_RETI) 'NUMERO O/R', " &
                "CONVERT(VARCHAR(10),Max(t5.FE_USUA_CREA),103) + ' ' + CONVERT(VARCHAR(5),Max(t5.FE_USUA_CREA),108)'FECHA O/R', " &
                "Max(t2.TI_TITU) 'TIPO WARRANT', " &
                "Max(t2.NU_TITU) 'NUMERO WARRANT', " &
                "Max(t1.NU_DOCU_RECE) 'NUMERO COMPROBANTE', " &
                "Substring(Max(t6.DE_DIRE_ALMA),1,80) 'DIRECCION DEL ALMACEN', " &
                "(Select Substring(DE_LARG_ENFI,1,50) From OFIOPER.DBO.TMENTI_FINA Where CO_ENTI_FINA = Max(t3.CO_ENTI_FINA))'NOMBRE FINANCIADOR', " &
                "Max(Case When t5.TI_RETI_GIRA = 'T' Then 'TOTAL' Else 'PARCIAL' End) 'TIPO RETIRO', " &
                "MAX(ISNULL(T5.DE_OBSE_0001,'') + ' ' + ISNULL(T5.DE_OBSE_0002,'')) AS DE_OBSE, " &
                "Max(T5.NO_CHOF) as NO_CHOF, " &
                "Max(T5.NU_DNNI) as NU_DNNI, " &
                "Max(T5.NU_PLAC) as NU_PLAC, " &
                "Max(t7.NU_DUAS) AS PEDIDO_DEPOSITO, " &
                "Max(t7.NU_CONO_EMBA) AS CONOCIMIENTO_EMBARQUE, " &
                "Max(t5.NU_POLI_ADUA) 'P�LIZA', " &
                "Max(t5.FE_POLI_ADUA) 'FECHA NUMERACI�N', " &
                "Max(t5.NU_COMP_PAGO) 'COMPROBANTE PAGO', " &
                "Max(t5.CO_ENTI_AGAD) 'COD. AG. ADUANA', " &
                "Max(t7.NU_MANI) 'N�MERO MANIFIESTO', " &
                "Max(t7.NO_VAPO) 'NOMBRE VAPOR', " &
                "Max(t5.IM_ADUA_SIST) 'IMPORTE MONEDA NACIONAL', " &
                "Max(t5.FA_CAMB_ADUA) 'FACTOR CAMBIO', " &
                "Max(t5.FE_CANC) 'FECHA CANCELACI�N', " &
                "Max(t7.NU_PEDI_DEPO) AS CERTIFICADO_ADUANERO " &
                "From OFIOPER.DBO.TDRETI_MERC t1 INNER JOIN OFIOPER.DBO.TCRETI_MERC t5 " &
                "ON t5.CO_EMPR = t1.CO_EMPR " &
                "And t5.CO_UNID = t1.CO_UNID " &
                "And t5.TI_DOCU_RETI = t1.TI_DOCU_RETI " &
                "And t5.NU_DOCU_RETI = t1.NU_DOCU_RETI " &
                "INNER JOIN OFIOPER.DBO.TCALMA_MERC t2 " &
                "ON t2.CO_EMPR = t1.CO_EMPR " &
                "And t2.CO_UNID = t1.CO_UNID " &
                "And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE " &
                "And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE " &
                "LEFT JOIN OFIOPER.DBO.TCTITU t3 " &
                "ON t3.CO_EMPR = t2.CO_EMPR " &
                "And t3.TI_TITU = t2.TI_TITU " &
                "And t3.NU_TITU = t2.NU_TITU " &
                "LEFT JOIN OFIOPER.DBO.TTMODA_MOVI t4 " &
                "ON t4.CO_EMPR = t2.CO_EMPR " &
                "And t4.CO_MODA_MOVI = t2.CO_MODA_MOVI " &
                "LEFT JOIN OFIOPER.DBO.TMALMA t6  " &
                "ON t6.CO_EMPR = t5.CO_EMPR " &
                "And t6.CO_ALMA = t5.CO_ALMA " &
                "LEFT JOIN OFIOPER.DBO.TMCLIE t9 " &
                "ON t9.CO_CLIE = t2.CO_CLIE_ACTU " &
                "LEFT JOIN OFIOPER.DBO.TCALME_ADUA t7 " &
                "ON t7.CO_EMPR = t2.CO_EMPR " &
                "And t7.CO_UNID = t2.CO_UNID " &
                "And t7.TI_DOCU_RECE = t2.TI_DOCU_RECE " &
                "And t7.NU_DOCU_RECE = t2.NU_DOCU_RECE " &
                "Where t1.CO_EMPR = '" & sCO_EMPR & "' " &
                "And t1.CO_UNID = '" & sCO_UNID & "' " &
                "And t1.TI_DOCU_RETI = '" & sTI_DOCU_RECE & "' " &
                "And t1.NU_DOCU_RETI = '" & sNU_DOCU_RECE & "' " &
                "And t1.TI_SITU = 'ACT' "
        ds = SqlHelper.ExecuteDataset(ObjTrans, CommandType.Text, sSQL)
        '  LstrNombrePDF = arParms(2).Value().ToString()
        Return ds.Tables(0)
        '-----------------------------------------------------------------------------
        'sSQL = "Select TI_DIRE_FISC From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "' "
        'Dim cmTDRETI_MERC As SqlCommand
        'Dim cmTDRETI_MERC As New SqlCommand(sSQL, cnn)
        'cmTDRETI_MERC.CommandTimeout = 0
        'Dim sTI_DIRE_FISC As String = cmTDRETI_MERC.ExecuteScalar
        'sSQL = "Select Max(t4.DE_MODA_MOVI) 'MODALIDAD', Max(t2.CO_CLIE_ACTU) 'COD CLIENTE', " & _
        '       "NULL, " & _
        '       "Max(t9.NU_RUCS_0001) 'RUC', " & _
        '       "NULL, " & _
        '       "Max(t1.NU_DOCU_RETI) 'NUMERO O/R', " & _
        '       "CONVERT(VARCHAR(10),Max(t5.FE_USUA_CREA),103) + ' ' + CONVERT(VARCHAR(5),Max(t5.FE_USUA_CREA),108)'FECHA O/R', Max(t2.TI_TITU) 'TIPO WARRANT', Max(t2.NU_TITU) 'NUMERO WARRANT', " & _
        '       "Max(t1.NU_DOCU_RECE) 'NUMERO COMPROBANTE', Substring(Max(t6.DE_DIRE_ALMA),1,80) 'DIRECCION DEL ALMACEN', " & _
        '       "(Select Substring(DE_LARG_ENFI,1,50) From TMENTI_FINA Where CO_ENTI_FINA = Max(t3.CO_ENTI_FINA))'NOMBRE FINANCIADOR', " & _
        '       "Max(Case When t5.TI_RETI_GIRA = 'T' Then 'TOTAL' Else 'PARCIAL' End) 'TIPO RETIRO', " & _
        '       "MAX(ISNULL(T5.DE_OBSE_0001,'') + ' ' + ISNULL(T5.DE_OBSE_0002,'')) AS DE_OBSE, Max(T5.NO_CHOF) as NO_CHOF, Max(T5.NU_DNNI) as NU_DNNI, Max(T5.NU_PLAC) as NU_PLAC, " & _
        '       "Max(t7.NU_DUAS) AS PEDIDO_DEPOSITO, " & _
        '       "Max(t7.NU_CONO_EMBA) AS CONOCIMIENTO_EMBARQUE, " & _
        '       "Max(t5.NU_POLI_ADUA) 'P�LIZA', " & _
        '       "Max(t5.FE_POLI_ADUA) 'FECHA NUMERACI�N', " & _
        '       "Max(t5.NU_COMP_PAGO) 'COMPROBANTE PAGO', " & _
        '       "Max(t5.CO_ENTI_AGAD) 'COD. AG. ADUANA', " & _
        '       "Max(t7.NU_MANI) 'N�MERO MANIFIESTO', " & _
        '       "Max(t7.NO_VAPO) 'NOMBRE VAPOR', " & _
        '       "Max(t5.IM_ADUA_SIST) 'IMPORTE MONEDA NACIONAL', " & _
        '       "Max(t5.FA_CAMB_ADUA) 'FACTOR CAMBIO', " & _
        '       "Max(t5.FE_CANC) 'FECHA CANCELACI�N', " & _
        '       "Max(t7.NU_PEDI_DEPO) AS CERTIFICADO_ADUANERO " & _
        '       "From OFIOPER.DBO.TDRETI_MERC t1, OFIOPER.DBO.TCRETI_MERC t5, OFIOPER.DBO.TCALMA_MERC t2, OFIOPER.DBO.TCTITU t3, OFIOPER.DBO.TTMODA_MOVI t4, OFIOPER.DBO.TMALMA t6, OFIOPER.DBO.TMCLIE t9, OFIOPER.DBO.TCALME_ADUA t7 " & _
        '       "Where t1.CO_EMPR = '" & sCO_EMPR & "' " & _
        '       "And t1.CO_UNID = '" & sCO_UNID & "' " & _
        '       "And t1.TI_DOCU_RETI = '" & sTI_DOCU_RECE & "' " & _
        '       "And t1.NU_DOCU_RETI = '" & sNU_DOCU_RECE & "' " & _
        '       "And t1.TI_SITU = 'ACT' " & _
        '       "And t5.CO_EMPR = t1.CO_EMPR " & _
        '       "And t5.CO_UNID = t1.CO_UNID " & _
        '       "And t5.TI_DOCU_RETI = t1.TI_DOCU_RETI " & _
        '       "And t5.NU_DOCU_RETI = t1.NU_DOCU_RETI " & _
        '       "And t2.CO_EMPR = t1.CO_EMPR " & _
        '       "And t2.CO_UNID = t1.CO_UNID " & _
        '       "And t2.TI_DOCU_RECE = t1.TI_DOCU_RECE " & _
        '       "And t2.NU_DOCU_RECE = t1.NU_DOCU_RECE " & _
        '       "And t3.CO_EMPR =* t2.CO_EMPR " & _
        '       "And t3.TI_TITU =* t2.TI_TITU " & _
        '       "And t3.NU_TITU =* t2.NU_TITU " & _
        '       "And t4.CO_EMPR =* t2.CO_EMPR " & _
        '       "And t4.CO_MODA_MOVI =* t2.CO_MODA_MOVI " & _
        '       " And t9.CO_CLIE =* t2.CO_CLIE_ACTU " & _
        '       " And t7.CO_EMPR =* t2.CO_EMPR " & _
        '       " And t7.CO_UNID =* t2.CO_UNID " & _
        '       " And t7.TI_DOCU_RECE =* t2.TI_DOCU_RECE " & _
        '       " And t7.NU_DOCU_RECE =* t2.NU_DOCU_RECE " & _
        '       "And t6.CO_EMPR =* t5.CO_EMPR " & _
        '       "And t6.CO_ALMA =* t5.CO_ALMA "

        'cmTDRETI_MERC = New SqlCommand(sSQL, cnn, ObjTrans)
        'cmTDRETI_MERC.CommandTimeout = 0
        'Dim daTDRETI_MERC As New SqlDataAdapter
        'daTDRETI_MERC.SelectCommand = cmTDRETI_MERC
        'Dim dsTDRETI_MERC As New DataSet
        'daTDRETI_MERC.Fill(dsTDRETI_MERC)
        'cnn.Close()
        'Return dsTDRETI_MERC.Tables(0)
    End Function

    Public Function fn_TDRETI_MERC_Q06(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
       ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String) As DataSet

        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = " Select Max(t3.TI_DOCU_RECE) as TI_DOCU_RECE, "
        sDE_SELE = sDE_SELE & " Max(t3.NU_DOCU_RECE) as NU_DOCU_RECE, "
        sDE_SELE = sDE_SELE & " Convert(varchar, Max(t3.FE_FACT), 101) as FE_FACT, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_CLIE_ACTU) as CO_CLIE_ACTU, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_TARI) as CO_TARI, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_ALMA) as CO_ALMA "
        sDE_SELE = sDE_SELE & " From TDRETI_MERC t1 INNER JOIN TCALMA_MERC t3 "
        sDE_SELE = sDE_SELE & " ON t3.CO_EMPR = t1.CO_EMPR "
        sDE_SELE = sDE_SELE & " And t3.CO_UNID = t1.CO_UNID "
        sDE_SELE = sDE_SELE & " And t3.TI_DOCU_RECE = t1.TI_DOCU_RECE "
        sDE_SELE = sDE_SELE & " And t3.NU_DOCU_RECE = t1.NU_DOCU_RECE "
        sDE_SELE = sDE_SELE & " INNER JOIN TCTITU t4 "
        sDE_SELE = sDE_SELE & " ON t4.CO_EMPR = t3.CO_EMPR "
        sDE_SELE = sDE_SELE & " And t4.TI_TITU = t3.TI_TITU "
        sDE_SELE = sDE_SELE & " And t4.NU_TITU = t3.NU_TITU "
        sDE_SELE = sDE_SELE & " LEFT JOIN TTMONE t2 "
        sDE_SELE = sDE_SELE & " ON t2.CO_MONE = t1.CO_MONE "
        sDE_SELE = sDE_SELE & " Where t1.CO_EMPR = '" & sCO_EMPR & "' "
        sDE_SELE = sDE_SELE & " And t1.CO_UNID = '" & sCO_UNID & "' "
        sDE_SELE = sDE_SELE & " And t1.TI_DOCU_RETI = '" & sTI_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And t1.NU_DOCU_RETI = '" & sNU_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And t1.TI_SITU = 'ACT' "

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        cmTDRETI_MERC.CommandTimeout = 0

        Dim daTDRETI_MERC As New SqlDataAdapter
        daTDRETI_MERC.SelectCommand = cmTDRETI_MERC

        Dim dsTDRETI_MERC As New DataSet
        daTDRETI_MERC.Fill(dsTDRETI_MERC)

        cnn.Close()
        Return dsTDRETI_MERC
    End Function

    Private Function fn_FACT_SIGU_Q01(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
       ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String,
       ByVal sFE_FACT As String, ByVal sCO_CLIE As String,
       ByVal sCO_TARI As String, ByVal sCO_ALMA As String) As String

        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOficome")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = "Select dbo.FU_FACT_SIGU('" & sCO_EMPR & "', '" & sCO_UNID & "', '" & sTI_DOCU_RECE & "', '"
        sDE_SELE = sDE_SELE & sNU_DOCU_RECE & "', '" & sFE_FACT & "', '" & sCO_CLIE & "', '"
        sDE_SELE = sDE_SELE & sCO_TARI & "', '" & sCO_ALMA & "')"

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        Dim sFE_FACT_SIGU As String = cmTDRETI_MERC.ExecuteScalar

        cnn.Close()
        Return sFE_FACT_SIGU
    End Function

    Public Function fn_TMCLIE_Q03(ByVal sCO_CLIE As String) As DataSet
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sSQL As String
        sSQL = "Select NO_CLIE_REPO, NU_RUCS_0001 " &
               "From TMCLIE " &
               "Where CO_CLIE='" & sCO_CLIE & "'"

        Dim cmTMCLIE As New SqlCommand(sSQL, cnn)
        cmTMCLIE.CommandTimeout = 0

        Dim daTMCLIE As New SqlDataAdapter
        daTMCLIE.SelectCommand = cmTMCLIE

        Dim dsTMCLIE As New DataSet
        daTMCLIE.Fill(dsTMCLIE)
        cnn.Close()
        Return dsTMCLIE
    End Function

    Public Function fn_TRDIRE_CLIE_Q03(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String, ByVal sCO_UNID As String) As DataSet
        Dim sDE_SELE As String
        Dim nNU_SECU_DIRE As Integer = fn_TCCONF_CLIE_Q01(sCO_EMPR, sCO_CLIE, sCO_UNID)

        If nNU_SECU_DIRE < 1 Then
            nNU_SECU_DIRE = fn_TRDIRE_CLIE_Q03(sCO_EMPR, sCO_CLIE)
        End If

        sDE_SELE = "Select DE_DIRE,NU_TELE_0001 From TRDIRE_CLIE Where CO_CLIE = '" & sCO_CLIE & "' " &
                       "And TI_DIRE =(Select IsNull(TI_DIRE_FISC, '') From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "') " &
                       "And NU_SECU =" & nNU_SECU_DIRE & "  And TI_SITU = 'ACT'"

        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If
        Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
        cmTRDIRE_CLIE.CommandTimeout = 0

        Dim daTCALMA_MERC As New SqlDataAdapter
        daTCALMA_MERC.SelectCommand = cmTRDIRE_CLIE

        Dim dsTMCALMA_MERC As New DataSet
        daTCALMA_MERC.Fill(dsTMCALMA_MERC)
        cnn.Close()
        Return dsTMCALMA_MERC
    End Function

    Private Function fn_TRDIRE_CLIE_Q03(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String) As Integer
        Try
            Dim sDE_SELE As String
            sDE_SELE = "Select IsNull(Min(NU_SECU),1) 'NU_SECU_DIRE' From TRDIRE_CLIE Where CO_CLIE ='" & sCO_CLIE & "' " &
                       "And TI_DIRE = (Select IsNull(TI_DIRE_FISC, '') From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "') " &
                       "And TI_SITU = 'ACT' "

            If cnn.State = ConnectionState.Closed Then
                cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                cnn.Open()
            End If
            Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
            Dim nNU_SECU_DIRE As Integer = cmTRDIRE_CLIE.ExecuteScalar
            cnn.Close()
            Return nNU_SECU_DIRE
        Catch ex As Exception
            Dim sCA_ERRO As String = ex.Message
        End Try
    End Function

    Private Function fn_TCCONF_CLIE_Q01(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String, ByVal sCO_UNID As String) As Integer
        Try
            Dim sDE_SELE As String
            sDE_SELE = "Select NU_DIRE_FACT From TCCONF_CLIE Where CO_EMPR='" & sCO_EMPR & "' " &
                       "And CO_UNID='" & sCO_UNID & "' And CO_CLIE='" & sCO_CLIE & "'"

            If cnn.State = ConnectionState.Closed Then
                cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                cnn.Open()
            End If
            Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
            Dim nNU_SECU_DIRE As Integer = cmTRDIRE_CLIE.ExecuteScalar
            cnn.Close()
            Return nNU_SECU_DIRE
        Catch ex As Exception
            Dim sCA_ERRO As String = ex.Message
        End Try
    End Function

    Public Function GetReporteLiberacionFirmas(ByVal strNombreReporte As String, ByVal strCoEmpresa As String, _
                                   ByVal strCoUnidad As String, ByVal strTipoDocumento As String, _
                                   ByVal strNroDocumento As String, ByVal strEntidad As String, _
                                   ByVal dtResumen As DataTable, ByVal strTipOtro As String, _
                                   ByVal blnEmbarque As Boolean, ByVal blnContable As Boolean, _
                                   ByVal blnTraslado As Boolean, ByVal strTipDocumento As String, _
                                   ByVal ObjTrans As SqlTransaction, Optional ByVal strRutaFirmas As String = "")
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_Time As FontDef = New FontDef(report, FontDef.StandardFont.TimesRoman)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd_Time, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim fp_Sello_Contabilizado As FontProp = New FontPropMM(fd, 2.6, System.Drawing.Color.Red)
        Dim fp_Sello_Adm As FontProp = New FontPropMM(fd, 2.0, System.Drawing.Color.Red)
        Dim page As page
        Dim rPosLeft As Double = 49
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        Dim ppBoldSellop As BrushProp = New BrushProp(report, System.Drawing.Color.Transparent)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        fp_Sello_Contabilizado.bBold = True
        fp_Sello_Adm.bBold = True
        Dim dtLiberacion As New DataTable
        Dim dtLiberacionWIP As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As New DataSet
        Dim dsTRDIRE_CLIE As New DataSet
        Dim dsTDRETI_MERC_0001 As New DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim sDE_DIRE As String
        Dim sTI_TITU As String
        Dim sNU_TITU As String
        Dim sDE_LARG_ENFI As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String

        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_CANC As String
        Dim sFE_EMIS As String

        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim strFechaFact As String()
        Dim dtFirmas As New DataTable
        Dim stream_Firma As Stream

        Dim intNroItem As Integer = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        dtLiberacion = fn_TDRETI_MERC_Q02_Tran(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento, ObjTrans)
        '-----------------------------------------------------------------------------
        If dtLiberacion.Rows.Count > 0 Then
            sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
            sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
            sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
            sTI_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO WARRANT")), "", dtLiberacion.Rows(0)("TIPO WARRANT"))
            sNU_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO WARRANT")), "", dtLiberacion.Rows(0)("NUMERO WARRANT"))
            sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
            sDE_LARG_ENFI = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE FINANCIADOR")), "", dtLiberacion.Rows(0)("NOMBRE FINANCIADOR"))
            If sDE_MODA_MOVI = "WARRANT ADUANERO" Then
                If Len(sDE_LARG_ENFI) >= 20 Then sDE_LARG_ENFI = Mid(sDE_LARG_ENFI, 1, 27)
            End If
            sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
            sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
            sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
            sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
            sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))

            sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
            sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
            sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
            sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
            sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
            sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
            sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
            sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
            sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
            sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
            sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))

            If sDE_MODA_MOVI = "WARRANT SIMPLE" Or sDE_MODA_MOVI = "WARRANT ADUANERO" Or sDE_MODA_MOVI.Trim = "WIP WARRANT INSUMO PRODUCT0" Then
                dsTDRETI_MERC_0001 = fn_TDRETI_MERC_Q06(strCoEmpresa, strCoUnidad, strTipoDocumento, _
                           strNroDocumento).Tables(0)
                If Not dsTDRETI_MERC_0001 Is Nothing Then
                    If dsTDRETI_MERC_0001.Rows.Count > 0 Then
                        Dim dwTDRETI_MERC_0001 As DataRow = dsTDRETI_MERC_0001.Rows(0)
                        Dim sTI_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("TI_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("TI_DOCU_RECE"))
                        Dim sNU_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("NU_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("NU_DOCU_RECE"))
                        Dim sFE_FACT As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("FE_FACT")), "", dwTDRETI_MERC_0001.Item("FE_FACT"))
                        Dim sCO_CLIE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU")), "", dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU"))
                        Dim sCO_TARI As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_TARI")), "", dwTDRETI_MERC_0001.Item("CO_TARI"))
                        Dim sCO_ALMA As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_ALMA")), "", dwTDRETI_MERC_0001.Item("CO_ALMA"))

                        sFE_FACT_SIGU = fn_FACT_SIGU_Q01(strCoEmpresa, strCoUnidad, sTI_DOCU_RECE, sNU_DOCU_RECE, sFE_FACT, sCO_CLIE, sCO_TARI, sCO_ALMA)

                    End If
                End If
            End If
        End If
        '-------------------------------------------------------------------------------
        'Obtiene nombre y ruc del cliente 
        dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
        If dsTMCLIE.Tables(0).Rows.Count > 0 Then
            sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
            sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
        End If

        'Obtiene direcci�n y tel�fono del cliente 
        dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, strCoUnidad)

        If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
            sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
            sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
        End If

        fp_header.bBold = True
        '--------------
        If dtResumen.Rows.Count > 20 Then
            If dtResumen.Rows.Count / 20 - CInt(dtResumen.Rows.Count / 20) > 0 Then
                intPaginas = CInt(dtResumen.Rows.Count / 20) + 1
            Else
                intPaginas = CInt(dtResumen.Rows.Count / 20)
            End If
        Else
            intPaginas = 1
        End If

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New page(report)

            page.rWidthMM = 210
            page.rHeightMM = 314
            'strFirmaDepsa1 = strRutaFirmas & "Logo_Depsa.JPG"
            strFirmaDepsa1 = strRutaFirmas & "Logo_Alma.JPG"
            'strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"

            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
            'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            'page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))
            If dtResumen.Rows(0)("FLG_FIRM") = True Then
                page.AddCB(intAltura + 15, New RepString(fp_Titulo, "(ELECTR�NICO)"))
            End If

            Select Case sDE_MODA_MOVI
                Case "WARRANT SIMPLE"
                    strTipOtro = "DEPOSITO FINANCIERO"
                Case "WARRANT ADUANERO"
                    strTipOtro = "DEPOSITO ADUANERO - FINANCIERO"
                Case "WIP WARRANT INSUMO PRODUCT0"
                    strTipOtro = "DEPOSITO FINANCIERO"
            End Select

            page.AddCB(intAltura, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS (DOR)"))
            page.Add(450, intAltura + 40, New RepString(fp_header, strNroDocumento))
            page.AddCB(intAltura + 40, New RepString(fp_Titulo, strTipOtro))
            intAltura += 30
            page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
            page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
            page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))
            page.Add(500, intAltura + 50, New RepString(fp_b, "Pag. " & p.ToString))
            page.Add(150, intAltura + 60, New RepString(fp, sFE_EMIS))
            page.Add(150, intAltura + 70, New RepString(fp, "R.U.C. 20100000688"))

            If blnEmbarque = True Then
                page.AddCB(intAltura + 90, New RepString(fp_Sello, "WARRANT ENDOSADO PARA EMBARQUE"))
            End If
            '----------------------------------------------------------------------------------------
            If strTipOtro = "DEPOSITO ADUANERO - FINANCIERO" Then
                page.Add(50, intAltura + 100, New RepString(fp_b, "Pedido Dep�sito"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Conocimiento"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "P�liza Nro"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Fecha Numeraci�n"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Comprobante Pago"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Cod. Agente Ad."))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Direcci�n Almacen"))
                page.Add(50, intAltura + 170, New RepString(fp_b, "Tipo Retiro"))

                page.Add(140, intAltura + 100, New RepString(fp, ": " & sNU_PEDI_DEPO))
                page.Add(140, intAltura + 110, New RepString(fp, ": " & sNU_CONO_EMBA))
                page.Add(140, intAltura + 120, New RepString(fp, ": " & sNU_POLI_ADUA))
                page.Add(140, intAltura + 130, New RepString(fp, ": "))
                'page.Add(140, intAltura + 130, New RepString(fp, ": " & sFE_POLI_ADUA))
                page.Add(140, intAltura + 140, New RepString(fp, ": " & sNU_COMP_PAGO))
                page.Add(140, intAltura + 150, New RepString(fp, ": " & sCO_ENTI_AGAD))
                page.Add(140, intAltura + 160, New RepString(fp, ": " & sDE_DIRE_ALMA))
                page.Add(140, intAltura + 170, New RepString(fp, ": " & sTI_RETI_GIRA))

                page.Add(320, intAltura + 100, New RepString(fp_b, "Manifiesto"))
                page.Add(320, intAltura + 110, New RepString(fp_b, "Vapor"))
                page.Add(320, intAltura + 120, New RepString(fp_b, "Moneda Nacional"))
                page.Add(320, intAltura + 130, New RepString(fp_b, "Tipo Cambio"))
                page.Add(320, intAltura + 140, New RepString(fp_b, "Fecha de Cancelac"))
                page.Add(320, intAltura + 150, New RepString(fp_b, "N� de Warrant"))
                page.Add(320, intAltura + 170, New RepString(fp_b, "F. Pr�x. Fact"))

                page.Add(410, intAltura + 100, New RepString(fp, ": " & sNU_MANI))
                page.Add(410, intAltura + 110, New RepString(fp, ": " & sNO_VAPO))
                page.Add(410, intAltura + 120, New RepString(fp, ": " & sIM_ADUA_SIST))
                page.Add(410, intAltura + 130, New RepString(fp, ": " & sFA_CAMB_ADUA))
                page.Add(410, intAltura + 140, New RepString(fp, ": " ))
                ' page.Add(410, intAltura + 140, New RepString(fp, ": " & sFE_CANC))
                page.Add(410, intAltura + 150, New RepString(fp, ": " & sTI_TITU & " " & sNU_TITU))
                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If
                page.Add(410, intAltura + 170, New RepString(fp, ": " & sFE_FACT_SIGU))
                intAltura += 20
            Else
                page.Add(50, intAltura + 100, New RepString(fp_b, "Warrant                  :"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Ubicaci�n              :"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "Financiador           :"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Tipo Retiro             :"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Prox. Facturaci�n :"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Observaciones      :"))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Almac�n Destino   :"))

                page.Add(140, intAltura + 100, New RepString(fp, sTI_TITU & " " & sNU_TITU))
                page.Add(140, intAltura + 110, New RepString(fp, sDE_DIRE_ALMA))
                page.Add(140, intAltura + 120, New RepString(fp, sDE_LARG_ENFI))
                page.Add(140, intAltura + 130, New RepString(fp, sTI_RETI_GIRA))

                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If

                page.Add(140, intAltura + 140, New RepString(fp, sFE_FACT_SIGU))
                page.Add(140, intAltura + 150, New RepString(fp, sDE_OBSE))
                page.Add(140, intAltura + 160, New RepString(fp, dtResumen.Rows(0)("ALM_DEST")))
            End If
            '-----------------------------------------------------------------------------------------
            Dim intLine As Int16

            intLine = (intAltura + 187) / 2.84
            page.AddMM(15, intLine - 5, New RepLineMM(ppBold, 175, 0))
            page.Add(50, intAltura + 187, New RepString(fp, "DCR"))
            page.Add(97, intAltura + 187, New RepString(fp, "Itm"))
            page.Add(115, intAltura + 187, New RepString(fp, "Bodega"))
            page.Add(150, intAltura + 187, New RepString(fp, "Cnt Unid."))
            page.Add(190, intAltura + 187, New RepString(fp, "Unidad"))
            page.Add(220, intAltura + 187, New RepString(fp, "Cnt Bulto"))
            page.Add(260, intAltura + 187, New RepString(fp, "Bulto"))
            page.Add(285, intAltura + 187, New RepString(fp, "Dsc de Mercader�a"))
            page.Add(435, intAltura + 187, New RepString(fp, "Prec.Unit."))
            page.Add(495, intAltura + 187, New RepString(fp, "Valor"))
            Dim decTotal As Decimal
            If (p - 1) * (20) + 20 < dtResumen.Rows.Count Then
                intNroPag = (p - 1) * (20) + 20
            Else
                intNroPag = dtResumen.Rows.Count
            End If

            For i As Int16 = (p - 1) * (20) To intNroPag - 1
                intAltura += 10
                If intNroItem <> dtResumen.Rows(i)("NRO_ITEM") Then
                    page.Add(50, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_COMP")))
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_ITEM")))
                    page.Add(120, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("COD_BODE")))
                    page.AddRight(180, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETI"))))
                    page.Add(195, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_ITEM")))
                    page.AddRight(255, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETBULT"))))
                    page.Add(260, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_BULT")))
                    page.AddRight(475, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("PRC_UNIT"))))
                    page.AddRight(535, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("TOTAL"))))
                    decTotal += Convert.ToDecimal(dtResumen.Rows(i)("TOTAL"))
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                Else
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                End If
                intNroItem = dtResumen.Rows(i)("NRO_ITEM")
            Next

            intLine = (intAltura + 197) / 2.84
            page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            If sDE_MODA_MOVI = "WIP WARRANT INSUMO PRODUCT0" And p = intPaginas Then
                intAltura += 20
                dtLiberacionWIP = objDocumento.gGetDetalleLiberacionWIP(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
                page.Add(120, intAltura + 187, New RepString(fp, "MATERIA PRIMA TRANSFORMADA            EN EL SGTE PRODUCTO TERMINADO"))
                For i As Int16 = 0 To dtLiberacionWIP.Rows.Count - 1
                    intAltura += 10
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("NU_SECU")))
                    page.AddRight(190, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_RETI"))))
                    page.Add(200, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("UNI_ITEM")))
                    page.Add(230, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_BULT"))))
                    If dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC")))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length > 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    End If
                Next
                intLine = (intAltura + 197) / 2.84
                page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            End If
            If blnContable = True And blnTraslado = True Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 63, 10))
                page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para traslado de mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y la emisi�n de un nuevo Warrant.'"))
            End If
            If blnContable = True And blnTraslado = False Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 73, 10))
                page.Add(60, intAltura + 220, New RepString(fp_Sello, "'Sin entrega f�sica de la mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y para la emisi�n de un nuevo Warrant.'"))
            End If
            intLine = (intAltura + 210) / 2.84

            If strRutaFirmas <> "" Then
                Dim strFirmaClie1 As String
                Dim strFirmaClie2 As String
                Dim strFirmaClie3 As String
                Dim strFirmaFina1 As String
                Dim strFirmaFina2 As String
                Dim strFirmaFina3 As String
                dtFirmas = objDocumento.gGenerarDataLiberacion(dtResumen.Rows(0)("NRO_DOCU"), dtResumen.Rows(0)("NRO_LIBE"), dtResumen.Rows(0)("PRD_DOCU"), strTipDocumento, ObjTrans)


                If dtFirmas.Rows(0)("ALM_CAMPO") <> 0 Then
                    '-----------------------------------------INICIO CONTABILIZADO---------------------------------------------------
                    'page.AddLT_MM(150, 72, New RepCircle(ppBoldSello, ppBoldSellop, 50))
                    'page.Add(433, 240, New RepString(fp_Sello_Contabilizado, "CONTABILIZADO"))
                    'page.Add(450, 255, New RepString(fp_Sello_Adm, Format(Now.Day, "00") & " / " & Format(Now.Month, "00") & " / " & Now.Year.ToString))
                    'page.Add(440, 270, New RepString(fp_Sello_Adm, "ADMINISTRACION"))
                    'page.Add(442, 280, New RepString(fp_Sello_Adm, "DOCUMENTARIA"))
                    '-----------------------------------------INICIO CONTABILIZADO---------------------------------------------------
                    page.AddLT_MM(168, 72, New RepCircle(ppBoldSello, ppBoldSellop, 50))
                    page.Add(483, 240, New RepString(fp_Sello_Contabilizado, "CONTABILIZADO"))
                    page.Add(500, 255, New RepString(fp_Sello_Adm, Format(Now.Day, "00") & " / " & Format(Now.Month, "00") & " / " & Now.Year.ToString))
                    page.Add(490, 270, New RepString(fp_Sello_Adm, "OPERACIONES"))


                    '---------------------------------- manual ---------------------------------------
                    'page.AddLT_MM(150, 72, New RepCircle(ppBoldSello, ppBoldSellop, 50))
                    'page.Add(433, 240, New RepString(fp_Sello_Contabilizado, "CONTABILIZADO"))
                    'page.Add(450, 255, New RepString(fp_Sello_Adm, "02" & " / " & "08" & " / " & "2019"))
                    'page.Add(440, 270, New RepString(fp_Sello_Adm, "ADMINISTRACION"))
                    'page.Add(442, 280, New RepString(fp_Sello_Adm, "DOCUMENTARIA"))

                    'page.AddLT_MM(168, 72, New RepCircle(ppBoldSello, ppBoldSellop, 50))
                    'page.Add(483, 240, New RepString(fp_Sello_Contabilizado, "CONTABILIZADO"))
                    'page.Add(500, 255, New RepString(fp_Sello_Adm, "31" & " / " & "01" & " / " & "2019"))
                    'page.Add(490, 270, New RepString(fp_Sello_Adm, "OPERACIONES"))

                    'page.Add(492, 280, New RepString(fp_Sello_Adm, "DOCUMENTARIA"))
                    '-----------------------------------------FIN CONTABILIZADO---------------------------------------------------
                End If

                If dtFirmas.Rows(0)("FIR_CLIE1") = "" Then
                        strFirmaClie1 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaClie1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE1")
                    End If
                    If dtFirmas.Rows(0)("FIR_CLIE2") = "" Then
                        strFirmaClie2 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaClie2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE2")
                    End If
                    If dtFirmas.Rows(0)("FIR_FINA1") = "" Then
                        strFirmaFina1 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaFina1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_FINA1")
                    End If
                    If dtFirmas.Rows(0)("FIR_FINA2") = "" Then
                        strFirmaFina2 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaFina2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_FINA2")
                    End If
                    strFirmaDepsa1 = strRutaFirmas & "CLAUDIARODRIGUEZO.JPG"
                    strFirmaDepsa2 = strRutaFirmas & "MARIOANDRADET.JPG"

                    stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(20, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))
                    stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(60, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))

                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(20, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(60, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                    stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(110, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                    stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(150, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                End If
                page.AddMM(15, intLine + 85, New RepLineMM(ppBold, 175, 0))
            If sNO_CLIE_REPO.Length > 40 And sNO_CLIE_REPO.Length <= 80 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, sNO_CLIE_REPO.Length - 40)))
            ElseIf sNO_CLIE_REPO.Length > 80 And sNO_CLIE_REPO.Length <= 120 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, 40)))
                page.Add(70, intAltura + 480, New RepString(fp_b, sNO_CLIE_REPO.Substring(80, sNO_CLIE_REPO.Length - 80)))
            Else
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO))
            End If

            If sDE_LARG_ENFI.Length > 40 And sDE_LARG_ENFI.Length <= 80 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, sDE_LARG_ENFI.Length - 40)))
            ElseIf sDE_LARG_ENFI.Length > 80 And sDE_LARG_ENFI.Length <= 120 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, 40)))
                page.Add(300, intAltura + 480, New RepString(fp_b, sDE_LARG_ENFI.Substring(80, sDE_LARG_ENFI.Length - 80)))
            Else
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI))
            End If

            page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de ALMACENERA DEL PER� S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
            page.Add(50, intAltura + 505, New RepString(fp, "del Warrant"))

            'page.Add(55, intAltura + 525, New RepString(fp, "V�B� COB."))

            'page.Add(55, intAltura + 540, New RepString(fp, "DE-R-018-GAP"))

            '----------------
            If p <> intPaginas Then
                intAltura = 120
            Else
                page.AddRight(535, intAltura + 220, New RepString(fp_table, "Total:  " & dtResumen.Rows(0)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", decTotal)))
            End If
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        dtLiberacionWIP = Nothing
        dtFirmas = Nothing
        stream_Firma = Nothing
    End Function

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Public Function GetReporteLiberacionCustodia(ByVal strNombreReporte As String, ByVal strCoEmpresa As String, _
                                                                    ByVal strTipoDocumento As String, ByVal strNroDocumento As String, _
                                                                    ByVal strEntidad As String, ByVal dtCab As DataTable, _
                                                                    ByVal blnContable As Boolean, ByVal blnTraslado As Boolean, _
                                                                    ByVal blnConFirmas As Boolean, Optional ByVal strRutaFirmas As String = "") As String
        Dim report As report = New report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        Dim page As page
        Dim stream_Firma As Stream
        Dim dtLiberacion As DataTable
        Dim dtResumen As DataTable
        Dim dtLiberacionWIP As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As DataSet
        Dim dsTRDIRE_CLIE As DataSet
        Dim dsTDRETI_MERC_0001 As DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim sDE_DIRE As String
        Dim sTI_TITU As String
        Dim sNU_TITU As String
        Dim sDE_LARG_ENFI As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String
        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_EMIS As String
        Dim sFE_CANC As String
        Dim strFirmaClie1 As String
        Dim strFirmaClie2 As String
        Dim strFechaFact As String()
        Dim dtFirmas As New DataTable
        Dim intCuadro As Int16 = 0
        Dim intLine As Int16 = 0
        Dim intNroItem As Integer = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        Dim strTipoTitulo As String
        '  sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        'dtLiberacion = fn_TDRETI_MERC_Q02(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        For n As Int16 = 0 To dtCab.Rows.Count - 1
            dtLiberacion = fn_TDRETI_MERC_Q02(strCoEmpresa, dtCab.Rows(n)("COD_UNID"), strTipoDocumento, dtCab.Rows(n)("NRO_LIBE"))
            '----------------------------------------------------------------------------
            If dtLiberacion.Rows.Count > 0 Then
                sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
                sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
                sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
                sTI_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO WARRANT")), "", dtLiberacion.Rows(0)("TIPO WARRANT"))
                sNU_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO WARRANT")), "", dtLiberacion.Rows(0)("NUMERO WARRANT"))
                sNU_DOCU_RECE = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO COMPROBANTE")), "", dtLiberacion.Rows(0)("NUMERO COMPROBANTE"))
                sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
                sDE_LARG_ENFI = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE FINANCIADOR")), "", dtLiberacion.Rows(0)("NOMBRE FINANCIADOR"))
                If sDE_MODA_MOVI = "WARRANT ADUANERO" Then
                    If Len(sDE_LARG_ENFI) >= 20 Then sDE_LARG_ENFI = Mid(sDE_LARG_ENFI, 1, 27)
                End If
                sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
                sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
                sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
                sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
                sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))

                sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
                sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
                sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
                sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
                sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
                sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
                sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
                sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
                sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
                sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
                sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))

                If sDE_MODA_MOVI = "WARRANT SIMPLE" Or sDE_MODA_MOVI = "WARRANT ADUANERO" Or sDE_MODA_MOVI.Trim = "WIP WARRANT INSUMO PRODUCT0" Then
                    dsTDRETI_MERC_0001 = fn_TDRETI_MERC_Q06(strCoEmpresa, dtCab.Rows(n)("COD_UNID"), strTipoDocumento, _
                               dtCab.Rows(n)("NRO_LIBE")).Tables(0)
                    If Not dsTDRETI_MERC_0001 Is Nothing Then
                        If dsTDRETI_MERC_0001.Rows.Count > 0 Then
                            Dim dwTDRETI_MERC_0001 As DataRow = dsTDRETI_MERC_0001.Rows(0)
                            Dim sTI_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("TI_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("TI_DOCU_RECE"))
                            Dim sNU_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("NU_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("NU_DOCU_RECE"))
                            Dim sFE_FACT As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("FE_FACT")), "", dwTDRETI_MERC_0001.Item("FE_FACT"))
                            Dim sCO_CLIE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU")), "", dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU"))
                            Dim sCO_TARI As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_TARI")), "", dwTDRETI_MERC_0001.Item("CO_TARI"))
                            Dim sCO_ALMA As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_ALMA")), "", dwTDRETI_MERC_0001.Item("CO_ALMA"))

                            sFE_FACT_SIGU = fn_FACT_SIGU_Q01(strCoEmpresa, dtCab.Rows(n)("COD_UNID"), sTI_DOCU_RECE, sNU_DOCU_RECE, sFE_FACT, sCO_CLIE, sCO_TARI, sCO_ALMA)

                        End If
                    End If
                End If
            End If
            '-------------------------------------------------------------------------------
            'Obtiene nombre y ruc del cliente 
            dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
            If dsTMCLIE.Tables(0).Rows.Count > 0 Then
                sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
                sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
            End If

            'Obtiene direcci�n y tel�fono del cliente 
            dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, dtCab.Rows(n)("COD_UNID"))

            If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
                sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
                sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
            End If

            fp_header.bBold = True
            dtResumen = objDocumento.gGetDetalleLiberacion(strCoEmpresa, dtCab.Rows(n)("COD_UNID"), dtCab.Rows(n)("NRO_LIBE"), dtCab.Rows(n)("COD_TIPDOC"), dtCab.Rows(n)("NRO_DOCU"))

            If dtResumen.Rows.Count > 20 Then
                If dtResumen.Rows.Count / 20 - CInt(dtResumen.Rows.Count / 20) > 0 Then
                    intPaginas = CInt(dtResumen.Rows.Count / 20) + 1
                Else
                    intPaginas = CInt(dtResumen.Rows.Count / 20)
                End If
            Else
                intPaginas = 1
            End If

            For p As Int16 = 1 To intPaginas
                intAltura = 120
                page = New page(report)
                page.rWidthMM = 210
                page.rHeightMM = 314

                strFirmaDepsa1 = strRutaFirmas & "Logo_Depsa.JPG"
                'strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"

                stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
                'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                'page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))

                If dtResumen.Rows(0)("FLG_FIRM") = True Then
                    page.AddCB(intAltura + 15, New RepString(fp_Titulo, "(ELECTR�NICO)"))
                End If

                Select Case sDE_MODA_MOVI
                    Case "WARRANT SIMPLE"
                        strTipoTitulo = "DEPOSITO FINANCIERO"
                    Case "WARRANT ADUANERO"
                        strTipoTitulo = "DEPOSITO ADUANERO - FINANCIERO"
                    Case "WIP WARRANT INSUMO PRODUCT0"
                        strTipoTitulo = "DEPOSITO FINANCIERO"
                End Select

                page.AddCB(intAltura, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS(DOR)"))
                page.Add(450, intAltura + 40, New RepString(fp_header, dtResumen.Rows(0)("NRO_LIBE")))
                page.AddCB(intAltura + 40, New RepString(fp_Titulo, strTipoTitulo))
                intAltura += 30
                page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
                page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
                page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))
                page.Add(500, intAltura + 50, New RepString(fp_b, "Pag. " & p.ToString))
                page.Add(150, intAltura + 60, New RepString(fp, sFE_EMIS))
                page.Add(150, intAltura + 70, New RepString(fp, "R.U.C. 20100000688"))

                'If blnEmbarque = True Then
                '    page.AddCB(intAltura + 90, New RepString(fp_Sello, "WARRANT ENDOSADO PARA EMBARQUE"))
                'End If
                '---------------------------------------------------------------------
                If strTipoTitulo = "DEPOSITO ADUANERO - FINANCIERO" Then
                    page.Add(50, intAltura + 100, New RepString(fp_b, "Pedido Dep�sito"))
                    page.Add(50, intAltura + 110, New RepString(fp_b, "Conocimiento"))
                    page.Add(50, intAltura + 120, New RepString(fp_b, "P�liza Nro"))
                    page.Add(50, intAltura + 130, New RepString(fp_b, "Fecha Numeraci�n"))
                    page.Add(50, intAltura + 140, New RepString(fp_b, "Comprobante Pago"))
                    page.Add(50, intAltura + 150, New RepString(fp_b, "Cod. Agente Ad."))
                    page.Add(50, intAltura + 160, New RepString(fp_b, "Direcci�n Almacen"))
                    page.Add(50, intAltura + 170, New RepString(fp_b, "Tipo Retiro"))

                    page.Add(140, intAltura + 100, New RepString(fp, ": " & sNU_PEDI_DEPO))
                    page.Add(140, intAltura + 110, New RepString(fp, ": " & sNU_CONO_EMBA))
                    page.Add(140, intAltura + 120, New RepString(fp, ": " & sNU_POLI_ADUA))
                    page.Add(140, intAltura + 130, New RepString(fp, ": " & sFE_POLI_ADUA))
                    page.Add(140, intAltura + 140, New RepString(fp, ": " & sNU_COMP_PAGO))
                    page.Add(140, intAltura + 150, New RepString(fp, ": " & sCO_ENTI_AGAD))
                    page.Add(140, intAltura + 160, New RepString(fp, ": " & sDE_DIRE_ALMA))
                    page.Add(140, intAltura + 170, New RepString(fp, ": " & sTI_RETI_GIRA))

                    page.Add(320, intAltura + 100, New RepString(fp_b, "Manifiesto"))
                    page.Add(320, intAltura + 110, New RepString(fp_b, "Vapor"))
                    page.Add(320, intAltura + 120, New RepString(fp_b, "Moneda Nacional"))
                    page.Add(320, intAltura + 130, New RepString(fp_b, "Tipo Cambio"))
                    page.Add(320, intAltura + 140, New RepString(fp_b, "Fecha de Cancelac"))
                    page.Add(320, intAltura + 150, New RepString(fp_b, "N� de Warrant"))
                    page.Add(320, intAltura + 170, New RepString(fp_b, "F. Pr�x. Fact"))

                    page.Add(410, intAltura + 100, New RepString(fp, ": " & sNU_MANI))
                    page.Add(410, intAltura + 110, New RepString(fp, ": " & sNO_VAPO))
                    page.Add(410, intAltura + 120, New RepString(fp, ": " & sIM_ADUA_SIST))
                    page.Add(410, intAltura + 130, New RepString(fp, ": " & sFA_CAMB_ADUA))
                    page.Add(410, intAltura + 140, New RepString(fp, ": " & sFE_CANC))
                    page.Add(410, intAltura + 150, New RepString(fp, ": " & sTI_TITU & " " & sNU_TITU))
                    If IsDBNull(sFE_FACT_SIGU) Then
                        sFE_FACT_SIGU = ""
                    ElseIf sFE_FACT_SIGU = "" Then
                        sFE_FACT_SIGU = ""
                    Else
                        strFechaFact = sFE_FACT_SIGU.Split("/")
                        sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                    End If
                    page.Add(410, intAltura + 170, New RepString(fp, ": " & sFE_FACT_SIGU))
                    intAltura += 20
                Else
                    page.Add(50, intAltura + 100, New RepString(fp_b, "Warrant                  :"))
                    page.Add(50, intAltura + 110, New RepString(fp_b, "Ubicaci�n              :"))
                    page.Add(50, intAltura + 120, New RepString(fp_b, "Financiador           :"))
                    page.Add(50, intAltura + 130, New RepString(fp_b, "Tipo Retiro             :"))
                    page.Add(50, intAltura + 140, New RepString(fp_b, "Prox. Facturaci�n :"))
                    page.Add(50, intAltura + 150, New RepString(fp_b, "Observaciones      :"))
                    page.Add(50, intAltura + 160, New RepString(fp_b, "Almac�n Destino   :"))

                    page.Add(140, intAltura + 100, New RepString(fp, sTI_TITU & " " & sNU_TITU))
                    page.Add(140, intAltura + 110, New RepString(fp, sDE_DIRE_ALMA))
                    page.Add(140, intAltura + 120, New RepString(fp, sDE_LARG_ENFI))
                    page.Add(140, intAltura + 130, New RepString(fp, sTI_RETI_GIRA))

                    If IsDBNull(sFE_FACT_SIGU) Then
                        sFE_FACT_SIGU = ""
                    ElseIf sFE_FACT_SIGU = "" Then
                        sFE_FACT_SIGU = ""
                    Else
                        strFechaFact = sFE_FACT_SIGU.Split("/")
                        sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                    End If

                    page.Add(140, intAltura + 140, New RepString(fp, sFE_FACT_SIGU))
                    page.Add(140, intAltura + 150, New RepString(fp, sDE_OBSE))
                    page.Add(140, intAltura + 160, New RepString(fp, dtResumen.Rows(0)("ALM_DEST")))
                End If
                '-----------------------------------------------------------------------------------

                intLine = (intAltura + 187) / 2.84
                page.AddMM(15, intLine - 5, New RepLineMM(ppBold, 175, 0))
                page.Add(50, intAltura + 187, New RepString(fp, "DCR"))
                page.Add(97, intAltura + 187, New RepString(fp, "Itm"))
                page.Add(115, intAltura + 187, New RepString(fp, "Bodega"))
                page.Add(150, intAltura + 187, New RepString(fp, "Cnt Unid."))
                page.Add(190, intAltura + 187, New RepString(fp, "Unidad"))
                page.Add(220, intAltura + 187, New RepString(fp, "Cnt Bulto"))
                page.Add(260, intAltura + 187, New RepString(fp, "Bulto"))
                page.Add(285, intAltura + 187, New RepString(fp, "Dsc de Mercader�a"))
                page.Add(435, intAltura + 187, New RepString(fp, "Prec.Unit."))
                page.Add(495, intAltura + 187, New RepString(fp, "Valor"))
                Dim decTotal As Decimal

                If (p - 1) * (20) + 20 < dtResumen.Rows.Count Then
                    intNroPag = (p - 1) * (20) + 20
                Else
                    intNroPag = dtResumen.Rows.Count
                End If

                For i As Int16 = (p - 1) * (20) To intNroPag - 1
                    intAltura += 10
                    If intNroItem <> dtResumen.Rows(i)("NRO_ITEM") Then
                        page.Add(50, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_COMP")))
                        page.Add(100, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_ITEM")))
                        page.Add(120, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("COD_BODE")))
                        page.AddRight(180, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETI"))))
                        page.Add(195, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_ITEM")))
                        page.AddRight(255, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETBULT"))))
                        page.Add(260, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_BULT")))
                        page.AddRight(475, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("PRC_UNIT"))))
                        page.AddRight(535, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("TOTAL"))))
                        decTotal += Convert.ToDecimal(dtResumen.Rows(i)("TOTAL"))
                        page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                    Else
                        page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                    End If
                    intNroItem = dtResumen.Rows(i)("NRO_ITEM")
                Next

                intLine = (intAltura + 197) / 2.84
                page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
                If sDE_MODA_MOVI = "WIP WARRANT INSUMO PRODUCT0" And p = intPaginas Then
                    intAltura += 20
                    dtLiberacionWIP = objDocumento.gGetDetalleLiberacionWIP(strCoEmpresa, dtResumen.Rows(0)("COD_UNID"), strTipoDocumento, dtResumen.Rows(0)("NRO_LIBE"))
                    page.Add(120, intAltura + 187, New RepString(fp, "MATERIA PRIMA TRANSFORMADA            EN EL SGTE PRODUCTO TERMINADO"))
                    For i As Int16 = 0 To dtLiberacionWIP.Rows.Count - 1
                        intAltura += 10
                        page.Add(100, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("NU_SECU")))
                        page.AddRight(190, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_RETI"))))
                        page.Add(200, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("UNI_ITEM")))
                        page.Add(230, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_BULT"))))
                        If dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC")))
                        ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30)))
                        ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60)))
                        ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90)))
                        ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length > 120 Then
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                            intAltura += 10
                            page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                        End If
                    Next
                    intLine = (intAltura + 197) / 2.84
                    page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
                End If
                If blnContable = True And blnTraslado = True Then
                    page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 63, 10))
                    page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para traslado de mercader�a"))
                    page.Add(50, intAltura + 230, New RepString(fp_Sello, "y la emisi�n de un nuevo Warrant.'"))
                End If
                If blnContable = True And blnTraslado = False Then
                    page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 73, 10))
                    page.Add(60, intAltura + 220, New RepString(fp_Sello, "'Sin entrega f�sica de la mercader�a"))
                    page.Add(50, intAltura + 230, New RepString(fp_Sello, "y para la emisi�n de un nuevo Warrant.'"))
                End If

                intLine = (intAltura + 210) / 2.84
                strFirmaDepsa1 = strRutaFirmas & "CLAUDIARODRIGUEZO.JPG"
                strFirmaDepsa2 = strRutaFirmas & "MARIOANDRADET.JPG"

                stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(20, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(70, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))

                '------------------------------------------------------------------------------------------------------------------------------
                If blnConFirmas Then
                    dtFirmas = objDocumento.GetFirmantesLiberacion(strEntidad)

                    If dtFirmas.Rows(0)("FIR_CLIE1") = "" Then
                        strFirmaClie1 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaClie1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE1")
                    End If
                    If dtFirmas.Rows(0)("FIR_CLIE2") = "" Then
                        strFirmaClie2 = strRutaFirmas & "aspl.JPG"
                    Else
                        strFirmaClie2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE2")
                    End If

                    stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(20, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                    stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                    page.AddMM(70, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                End If
                '--------------------------------------------------------------------------------------------------------------------------------
                page.AddMM(15, intLine + 85, New RepLineMM(ppBold, 175, 0))
                If sNO_CLIE_REPO.Length > 40 And sNO_CLIE_REPO.Length <= 80 Then
                    page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                    page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, sNO_CLIE_REPO.Length - 40)))
                ElseIf sNO_CLIE_REPO.Length > 80 And sNO_CLIE_REPO.Length <= 120 Then
                    page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                    page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, 40)))
                    page.Add(70, intAltura + 480, New RepString(fp_b, sNO_CLIE_REPO.Substring(80, sNO_CLIE_REPO.Length - 80)))
                Else
                    page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO))
                End If

                If sDE_LARG_ENFI.Length > 40 And sDE_LARG_ENFI.Length <= 80 Then
                    page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                    page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, sDE_LARG_ENFI.Length - 40)))
                ElseIf sDE_LARG_ENFI.Length > 80 And sDE_LARG_ENFI.Length <= 120 Then
                    page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                    page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, 40)))
                    page.Add(300, intAltura + 480, New RepString(fp_b, sDE_LARG_ENFI.Substring(80, sDE_LARG_ENFI.Length - 80)))
                Else
                    page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI))
                End If

                page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de ALMACENERA DEL PER� S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
                page.Add(50, intAltura + 505, New RepString(fp, "del Warrant"))

                'page.Add(55, intAltura + 525, New RepString(fp, "V�B� COB."))

                'page.Add(55, intAltura + 540, New RepString(fp, "DE-R-018-GAP"))
                '----------------
                If p <> intPaginas Then
                    intAltura = 120
                Else
                    page.AddRight(535, intAltura + 220, New RepString(fp_table, "Total: " & dtResumen.Rows(0)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", decTotal)))
                End If
            Next
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        dtResumen = Nothing
        dtCab = Nothing
        dtLiberacionWIP = Nothing
        dtFirmas = Nothing
        stream_Firma = Nothing
        Return sNO_CLIE_REPO
    End Function

    Public Function GetReporteLiberacionActFirmas(ByVal strNombreReporte As String, ByVal strCoEmpresa As String, _
                                       ByVal strCoUnidad As String, ByVal strTipoDocumento As String, _
                                       ByVal strNroDocumento As String, ByVal strEntidad As String, _
                                       ByVal dtResumen As DataTable, ByVal strTipOtro As String, _
                                       ByVal blnEmbarque As Boolean, ByVal blnContable As Boolean, _
                                       ByVal blnTraslado As Boolean, ByVal strTipDocumento As String, _
                                       ByVal ObjTrans As SqlTransaction, Optional ByVal strRutaFirmas As String = "")
        Dim report As Report = New Report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_Time As FontDef = New FontDef(report, FontDef.StandardFont.TimesRoman)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd_Time, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim page As Page
        Dim rPosLeft As Double = 49
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        Dim ppBoldSellop As BrushProp = New BrushProp(report, System.Drawing.Color.Transparent)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        Dim dtLiberacion As New DataTable
        Dim dtLiberacionWIP As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As New DataSet
        Dim dsTRDIRE_CLIE As New DataSet
        Dim dsTDRETI_MERC_0001 As New DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim sDE_DIRE As String
        Dim sTI_TITU As String
        Dim sNU_TITU As String
        Dim sDE_LARG_ENFI As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String

        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_CANC As String
        Dim sFE_EMIS As String

        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim strFechaFact As String()
        Dim dtFirmas As New DataTable
        Dim stream_Firma As Stream

        Dim intNroItem As Integer = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
        dtLiberacion = fn_TDRETI_MERC_Q02_Tran(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento, ObjTrans)
        '-----------------------------------------------------------------------------
        If dtLiberacion.Rows.Count > 0 Then
            sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
            sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
            sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
            sTI_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO WARRANT")), "", dtLiberacion.Rows(0)("TIPO WARRANT"))
            sNU_TITU = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO WARRANT")), "", dtLiberacion.Rows(0)("NUMERO WARRANT"))
            sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
            sDE_LARG_ENFI = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE FINANCIADOR")), "", dtLiberacion.Rows(0)("NOMBRE FINANCIADOR"))
            If sDE_MODA_MOVI = "WARRANT ADUANERO" Then
                If Len(sDE_LARG_ENFI) >= 20 Then sDE_LARG_ENFI = Mid(sDE_LARG_ENFI, 1, 27)
            End If
            sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
            sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
            sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
            sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
            sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))

            sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
            sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
            sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
            sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
            sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
            sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
            sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
            sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
            sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
            sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
            sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))

            If sDE_MODA_MOVI = "WARRANT SIMPLE" Or sDE_MODA_MOVI = "WARRANT ADUANERO" Or sDE_MODA_MOVI.Trim = "WIP WARRANT INSUMO PRODUCT0" Then
                dsTDRETI_MERC_0001 = fn_TDRETI_MERC_Q06(strCoEmpresa, strCoUnidad, strTipoDocumento, _
                           strNroDocumento).Tables(0)
                If Not dsTDRETI_MERC_0001 Is Nothing Then
                    If dsTDRETI_MERC_0001.Rows.Count > 0 Then
                        Dim dwTDRETI_MERC_0001 As DataRow = dsTDRETI_MERC_0001.Rows(0)
                        Dim sTI_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("TI_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("TI_DOCU_RECE"))
                        Dim sNU_DOCU_RECE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("NU_DOCU_RECE")), "", dwTDRETI_MERC_0001.Item("NU_DOCU_RECE"))
                        Dim sFE_FACT As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("FE_FACT")), "", dwTDRETI_MERC_0001.Item("FE_FACT"))
                        Dim sCO_CLIE As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU")), "", dwTDRETI_MERC_0001.Item("CO_CLIE_ACTU"))
                        Dim sCO_TARI As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_TARI")), "", dwTDRETI_MERC_0001.Item("CO_TARI"))
                        Dim sCO_ALMA As String = IIf(IsDBNull(dwTDRETI_MERC_0001.Item("CO_ALMA")), "", dwTDRETI_MERC_0001.Item("CO_ALMA"))

                        sFE_FACT_SIGU = fn_FACT_SIGU_Q01(strCoEmpresa, strCoUnidad, sTI_DOCU_RECE, sNU_DOCU_RECE, sFE_FACT, sCO_CLIE, sCO_TARI, sCO_ALMA)

                    End If
                End If
            End If
        End If
        '-------------------------------------------------------------------------------
        'Obtiene nombre y ruc del cliente 
        dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
        If dsTMCLIE.Tables(0).Rows.Count > 0 Then
            sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
            sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
        End If

        'Obtiene direcci�n y tel�fono del cliente 
        dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, strCoUnidad)

        If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
            sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
            sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
        End If

        fp_header.bBold = True
        '--------------
        If dtResumen.Rows.Count > 20 Then
            If dtResumen.Rows.Count / 20 - CInt(dtResumen.Rows.Count / 20) > 0 Then
                intPaginas = CInt(dtResumen.Rows.Count / 20) + 1
            Else
                intPaginas = CInt(dtResumen.Rows.Count / 20)
            End If
        Else
            intPaginas = 1
        End If

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New Page(report)

            page.rWidthMM = 210
            page.rHeightMM = 314
            strFirmaDepsa1 = strRutaFirmas & "Logo_Alma.JPG"
            'strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"

            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
            'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            'page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))
            If dtResumen.Rows(0)("FLG_FIRM") = True Then
                page.AddCB(intAltura + 15, New RepString(fp_Titulo, "(ELECTR�NICO)"))
            End If

            Select Case sDE_MODA_MOVI
                Case "WARRANT SIMPLE"
                    strTipOtro = "DEPOSITO FINANCIERO"
                Case "WARRANT ADUANERO"
                    strTipOtro = "DEPOSITO ADUANERO - FINANCIERO"
                Case "WIP WARRANT INSUMO PRODUCT0"
                    strTipOtro = "DEPOSITO FINANCIERO"
            End Select

            page.AddCB(intAltura, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS (DOR)"))
            page.Add(450, intAltura + 40, New RepString(fp_header, strNroDocumento))
            page.AddCB(intAltura + 40, New RepString(fp_Titulo, strTipOtro))
            intAltura += 30
            page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
            page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
            page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))
            page.Add(500, intAltura + 50, New RepString(fp_b, "Pag. " & p.ToString))
            page.Add(150, intAltura + 60, New RepString(fp, sFE_EMIS))
            page.Add(150, intAltura + 70, New RepString(fp, "R.U.C. 20100000688"))

            If blnEmbarque = True Then
                page.AddCB(intAltura + 90, New RepString(fp_Sello, "WARRANT ENDOSADO PARA EMBARQUE"))
            End If
            '----------------------------------------------------------------------------------------
            If strTipOtro = "DEPOSITO ADUANERO - FINANCIERO" Then
                page.Add(50, intAltura + 100, New RepString(fp_b, "Pedido Dep�sito"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Conocimiento"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "P�liza Nro"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Fecha Numeraci�n"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Comprobante Pago"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Cod. Agente Ad."))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Direcci�n Almacen"))
                page.Add(50, intAltura + 170, New RepString(fp_b, "Tipo Retiro"))

                page.Add(140, intAltura + 100, New RepString(fp, ": " & sNU_PEDI_DEPO))
                page.Add(140, intAltura + 110, New RepString(fp, ": " & sNU_CONO_EMBA))
                page.Add(140, intAltura + 120, New RepString(fp, ": " & sNU_POLI_ADUA))
                page.Add(140, intAltura + 130, New RepString(fp, ": " & sFE_POLI_ADUA))
                page.Add(140, intAltura + 140, New RepString(fp, ": " & sNU_COMP_PAGO))
                page.Add(140, intAltura + 150, New RepString(fp, ": " & sCO_ENTI_AGAD))
                page.Add(140, intAltura + 160, New RepString(fp, ": " & sDE_DIRE_ALMA))
                page.Add(140, intAltura + 170, New RepString(fp, ": " & sTI_RETI_GIRA))

                page.Add(320, intAltura + 100, New RepString(fp_b, "Manifiesto"))
                page.Add(320, intAltura + 110, New RepString(fp_b, "Vapor"))
                page.Add(320, intAltura + 120, New RepString(fp_b, "Moneda Nacional"))
                page.Add(320, intAltura + 130, New RepString(fp_b, "Tipo Cambio"))
                page.Add(320, intAltura + 140, New RepString(fp_b, "Fecha de Cancelac"))
                page.Add(320, intAltura + 150, New RepString(fp_b, "N� de Warrant"))
                page.Add(320, intAltura + 170, New RepString(fp_b, "F. Pr�x. Fact"))

                page.Add(410, intAltura + 100, New RepString(fp, ": " & sNU_MANI))
                page.Add(410, intAltura + 110, New RepString(fp, ": " & sNO_VAPO))
                page.Add(410, intAltura + 120, New RepString(fp, ": " & sIM_ADUA_SIST))
                page.Add(410, intAltura + 130, New RepString(fp, ": " & sFA_CAMB_ADUA))
                page.Add(410, intAltura + 140, New RepString(fp, ": " & sFE_CANC))
                page.Add(410, intAltura + 150, New RepString(fp, ": " & sTI_TITU & " " & sNU_TITU))
                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If
                page.Add(410, intAltura + 170, New RepString(fp, ": " & sFE_FACT_SIGU))
                intAltura += 20
            Else
                page.Add(50, intAltura + 100, New RepString(fp_b, "Warrant                  :"))
                page.Add(50, intAltura + 110, New RepString(fp_b, "Ubicaci�n              :"))
                page.Add(50, intAltura + 120, New RepString(fp_b, "Financiador           :"))
                page.Add(50, intAltura + 130, New RepString(fp_b, "Tipo Retiro             :"))
                page.Add(50, intAltura + 140, New RepString(fp_b, "Prox. Facturaci�n :"))
                page.Add(50, intAltura + 150, New RepString(fp_b, "Observaciones      :"))
                page.Add(50, intAltura + 160, New RepString(fp_b, "Almac�n Destino   :"))

                page.Add(140, intAltura + 100, New RepString(fp, sTI_TITU & " " & sNU_TITU))
                page.Add(140, intAltura + 110, New RepString(fp, sDE_DIRE_ALMA))
                page.Add(140, intAltura + 120, New RepString(fp, sDE_LARG_ENFI))
                page.Add(140, intAltura + 130, New RepString(fp, sTI_RETI_GIRA))

                If IsDBNull(sFE_FACT_SIGU) Then
                    sFE_FACT_SIGU = ""
                ElseIf sFE_FACT_SIGU = "" Then
                    sFE_FACT_SIGU = ""
                Else
                    strFechaFact = sFE_FACT_SIGU.Split("/")
                    sFE_FACT_SIGU = strFechaFact(0) & "/" & strFechaFact(1) & "/" & strFechaFact(2)
                End If

                page.Add(140, intAltura + 140, New RepString(fp, sFE_FACT_SIGU))
                page.Add(140, intAltura + 150, New RepString(fp, sDE_OBSE))
                page.Add(140, intAltura + 160, New RepString(fp, dtResumen.Rows(0)("ALM_DEST")))
            End If
            '-----------------------------------------------------------------------------------------
            Dim intLine As Int16

            intLine = (intAltura + 187) / 2.84
            page.AddMM(15, intLine - 5, New RepLineMM(ppBold, 175, 0))
            page.Add(50, intAltura + 187, New RepString(fp, "DCR"))
            page.Add(97, intAltura + 187, New RepString(fp, "Itm"))
            page.Add(115, intAltura + 187, New RepString(fp, "Bodega"))
            page.Add(150, intAltura + 187, New RepString(fp, "Cnt Unid."))
            page.Add(190, intAltura + 187, New RepString(fp, "Unidad"))
            page.Add(220, intAltura + 187, New RepString(fp, "Cnt Bulto"))
            page.Add(260, intAltura + 187, New RepString(fp, "Bulto"))
            page.Add(285, intAltura + 187, New RepString(fp, "Dsc de Mercader�a"))
            page.Add(435, intAltura + 187, New RepString(fp, "Prec.Unit."))
            page.Add(495, intAltura + 187, New RepString(fp, "Valor"))
            Dim decTotal As Decimal
            If (p - 1) * (20) + 20 < dtResumen.Rows.Count Then
                intNroPag = (p - 1) * (20) + 20
            Else
                intNroPag = dtResumen.Rows.Count
            End If

            For i As Int16 = (p - 1) * (20) To intNroPag - 1
                intAltura += 10
                If intNroItem <> dtResumen.Rows(i)("NRO_ITEM") Then
                    page.Add(50, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_COMP")))
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("NRO_ITEM")))
                    page.Add(120, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("COD_BODE")))
                    page.AddRight(180, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETI"))))
                    page.Add(195, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_ITEM")))
                    page.AddRight(255, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("CNT_RETBULT"))))
                    page.Add(260, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("UNI_BULT")))
                    page.AddRight(475, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("PRC_UNIT"))))
                    page.AddRight(535, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("TOTAL"))))
                    decTotal += Convert.ToDecimal(dtResumen.Rows(i)("TOTAL"))
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                Else
                    page.Add(285, intAltura + 190, New RepString(fp_table, dtResumen.Rows(i)("DSC_ITEM")))
                End If
                intNroItem = dtResumen.Rows(i)("NRO_ITEM")
            Next

            intLine = (intAltura + 197) / 2.84
            page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            If sDE_MODA_MOVI = "WIP WARRANT INSUMO PRODUCT0" And p = intPaginas Then
                intAltura += 20
                dtLiberacionWIP = objDocumento.gGetDetalleLiberacionWIP(strCoEmpresa, strCoUnidad, strTipoDocumento, strNroDocumento)
                page.Add(120, intAltura + 187, New RepString(fp, "MATERIA PRIMA TRANSFORMADA            EN EL SGTE PRODUCTO TERMINADO"))
                For i As Int16 = 0 To dtLiberacionWIP.Rows.Count - 1
                    intAltura += 10
                    page.Add(100, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("NU_SECU")))
                    page.AddRight(190, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_RETI"))))
                    page.Add(200, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("UNI_ITEM")))
                    page.Add(230, intAltura + 190, New RepString(fp_table, String.Format("{0:##,##0.000}", dtLiberacionWIP.Rows(i)("CNT_BULT"))))
                    If dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC")))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90)))
                    ElseIf dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Length > 120 Then
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                        intAltura += 10
                        page.Add(300, intAltura + 190, New RepString(fp_table, dtLiberacionWIP.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    End If
                Next
                intLine = (intAltura + 197) / 2.84
                page.AddMM(15, intLine, New RepLineMM(ppBold, 175, 0))
            End If
            If blnContable = True And blnTraslado = True Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 63, 10))
                page.Add(50, intAltura + 220, New RepString(fp_Sello, "'S�lo para traslado de mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y la emisi�n de un nuevo Warrant.'"))
            End If
            If blnContable = True And blnTraslado = False Then
                page.AddLT_MM(15, intLine + 4, New RepRectMM(ppBoldSello, 73, 10))
                page.Add(60, intAltura + 220, New RepString(fp_Sello, "'Sin entrega f�sica de la mercader�a"))
                page.Add(50, intAltura + 230, New RepString(fp_Sello, "y para la emisi�n de un nuevo Warrant.'"))
            End If
            intLine = (intAltura + 210) / 2.84

            If strRutaFirmas <> "" Then
                Dim strFirmaClie1 As String
                Dim strFirmaClie2 As String
                Dim strFirmaClie3 As String
                Dim strFirmaFina1 As String
                Dim strFirmaFina2 As String
                Dim strFirmaFina3 As String
                dtFirmas = objDocumento.gGenerarDataLiberacion(dtResumen.Rows(0)("NRO_DOCU"), dtResumen.Rows(0)("NRO_LIBE"), dtResumen.Rows(0)("PRD_DOCU"), strTipDocumento, ObjTrans)



                If dtFirmas.Rows(0)("FIR_CLIE1") = "" Then
                    strFirmaClie1 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaClie1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE1")
                End If
                If dtFirmas.Rows(0)("FIR_CLIE2") = "" Then
                    strFirmaClie2 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaClie2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_CLIE2")
                End If
                If dtFirmas.Rows(0)("FIR_FINA1") = "" Then
                    strFirmaFina1 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaFina1 = strRutaFirmas & dtFirmas.Rows(0)("FIR_FINA1")
                End If
                If dtFirmas.Rows(0)("FIR_FINA2") = "" Then
                    strFirmaFina2 = strRutaFirmas & "aspl.JPG"
                Else
                    strFirmaFina2 = strRutaFirmas & dtFirmas.Rows(0)("FIR_FINA2")
                End If
                strFirmaDepsa1 = strRutaFirmas & "CLAUDIARODRIGUEZO.JPG"
                strFirmaDepsa2 = strRutaFirmas & "MARIOANDRADET.JPG"

                stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(20, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(60, intLine + 40, New RepImageMM(stream_Firma, Double.NaN, 27))

                stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(20, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(60, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(110, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
                stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
                page.AddMM(150, intLine + 80, New RepImageMM(stream_Firma, Double.NaN, 27))
            End If
            page.AddMM(15, intLine + 85, New RepLineMM(ppBold, 175, 0))
            If sNO_CLIE_REPO.Length > 40 And sNO_CLIE_REPO.Length <= 80 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, sNO_CLIE_REPO.Length - 40)))
            ElseIf sNO_CLIE_REPO.Length > 80 And sNO_CLIE_REPO.Length <= 120 Then
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO.Substring(0, 40)))
                page.Add(70, intAltura + 470, New RepString(fp_b, sNO_CLIE_REPO.Substring(40, 40)))
                page.Add(70, intAltura + 480, New RepString(fp_b, sNO_CLIE_REPO.Substring(80, sNO_CLIE_REPO.Length - 80)))
            Else
                page.Add(70, intAltura + 460, New RepString(fp_b, sNO_CLIE_REPO))
            End If

            If sDE_LARG_ENFI.Length > 40 And sDE_LARG_ENFI.Length <= 80 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, sDE_LARG_ENFI.Length - 40)))
            ElseIf sDE_LARG_ENFI.Length > 80 And sDE_LARG_ENFI.Length <= 120 Then
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI.Substring(0, 40)))
                page.Add(300, intAltura + 470, New RepString(fp_b, sDE_LARG_ENFI.Substring(40, 40)))
                page.Add(300, intAltura + 480, New RepString(fp_b, sDE_LARG_ENFI.Substring(80, sDE_LARG_ENFI.Length - 80)))
            Else
                page.Add(300, intAltura + 460, New RepString(fp_b, sDE_LARG_ENFI))
            End If

            page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de ALMACENERA DEL PER� S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
            page.Add(50, intAltura + 505, New RepString(fp, "del Warrant"))

            'page.Add(55, intAltura + 525, New RepString(fp, "V�B� COB."))

            'page.Add(55, intAltura + 540, New RepString(fp, "DE-R-018-GAP"))

            '----------------
            If p <> intPaginas Then
                intAltura = 120
            Else
                page.AddRight(535, intAltura + 220, New RepString(fp_table, "Total: " & dtResumen.Rows(0)("TIP_MONE") & "  " & String.Format("{0:##,##0.00}", decTotal)))
            End If
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        dtLiberacionWIP = Nothing
        dtFirmas = Nothing
        stream_Firma = Nothing
    End Function
End Class
