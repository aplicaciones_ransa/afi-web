'Imports System.Math

Public Class Conversion
    Dim cMontext As String
    Public Function NumPalabra(ByVal Numero As Double) As String
        Dim Numerofmt As String
        Dim centenas As Integer
        Dim pos As Integer
        Dim cen As Integer
        Dim dec As Integer
        Dim uni As Integer
        Dim textuni As String
        Dim textdec As String
        Dim textcen As String
        Dim milestxt As String
        Dim monedatxt As String
        Dim txtPalabra As String

        Numerofmt = Format(Numero, "000000000000000.00") 'Le da un formato fijo
        centenas = 1
        pos = 1
        txtPalabra = ""
        Do While centenas <= 5
            ' extrae series de Centena, Decena, Unidad
            cen = Val(Mid(Numerofmt, pos, 1))
            dec = Val(Mid(Numerofmt, pos + 1, 1))
            uni = Val(Mid(Numerofmt, pos + 2, 1))
            pos = pos + 3
            textcen = Centena(uni, dec, cen)
            textdec = Decena(uni, dec)
            textuni = Unidad(uni, dec, pos)
            ' determina separador de miles/millones
            Select Case centenas
                Case 1
                    If cen + dec + uni = 1 Then
                        milestxt = "Billon "
                    ElseIf cen + dec + uni > 1 Then
                        milestxt = "Billones "
                    End If
                Case 2
                    If cen + dec + uni >= 1 And Val(Mid(Numerofmt, 7, 3)) = 0 Then
                        milestxt = "Mil Millones "
                    ElseIf cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 3
                    If cen + dec = 0 And uni = 1 Then
                        milestxt = "Millon "
                    ElseIf cen > 0 Or dec > 0 Or uni > 1 Then
                        milestxt = "Millones "
                    End If
                Case 4
                    If cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 5
                    If cen + dec + uni >= 1 Then
                        milestxt = ""
                    End If
            End Select
            centenas = centenas + 1
            'va formando el texto del importe en palabras
            txtPalabra = txtPalabra + textcen + textdec + textuni + milestxt
            milestxt = ""
            textuni = ""
            textdec = ""
            textcen = ""
        Loop
        ' agrega denominacion de moneda
        'Select Case Val(Numerofmt)
        '    Case 0
        '        monedatxt = "Cero Pesos "
        '    Case 1
        '        monedatxt = "Peso "
        '    Case Is < 1000000
        '        monedatxt = "Pesos "
        '    Case Is >= 1000000
        '        monedatxt = "de Pesos "
        'End Select
        'txtPalabra = txtPalabra & monedatxt & "con " & Mid(Numerofmt, 17) & "/100"
        txtPalabra = txtPalabra & "con " & Mid(Numerofmt, 17) & "/100"
        Return txtPalabra.ToUpper
    End Function

    Private Function Centena(ByVal uni As Integer, ByVal dec As Integer, _
        ByVal cen As Integer) As String
        Select Case cen
            Case 1
                If dec + uni = 0 Then
                    cMontext = "cien "
                Else
                    cMontext = "ciento "
                End If
            Case 2 : cMontext = "doscientos "
            Case 3 : cMontext = "trescientos "
            Case 4 : cMontext = "cuatrocientos "
            Case 5 : cMontext = "quinientos "
            Case 6 : cMontext = "seiscientos "
            Case 7 : cMontext = "setecientos "
            Case 8 : cMontext = "ochocientos "
            Case 9 : cMontext = "novecientos "
            Case Else : cMontext = ""
        End Select
        Centena = cMontext
        cMontext = ""
    End Function

    Private Function Decena(ByVal uni As Integer, ByVal dec As Integer) As String
        Select Case dec
            Case 1
                Select Case uni
                    Case 0 : cMontext = "diez "
                    Case 1 : cMontext = "once "
                    Case 2 : cMontext = "doce "
                    Case 3 : cMontext = "trece "
                    Case 4 : cMontext = "catorce "
                    Case 5 : cMontext = "quince "
                    Case 6 To 9 : cMontext = "dieci"
                End Select
            Case 2
                If uni = 0 Then
                    cMontext = "veinte "
                ElseIf uni > 0 Then
                    cMontext = "veinti"
                End If
            Case 3 : cMontext = "treinta "
            Case 4 : cMontext = "cuarenta "
            Case 5 : cMontext = "cincuenta "
            Case 6 : cMontext = "sesenta "
            Case 7 : cMontext = "setenta "
            Case 8 : cMontext = "ochenta "
            Case 9 : cMontext = "noventa "
            Case Else : cMontext = ""
        End Select
        If uni > 0 And dec > 2 Then cMontext = cMontext + "y "
        Decena = cMontext
        cMontext = ""
    End Function

    Private Function Unidad(ByVal uni As Integer, ByVal dec As Integer, ByVal pos As Integer) As String
        If dec <> 1 Then
            Select Case uni
                Case 1
                    If pos = 16 Then
                        cMontext = "uno "
                    Else
                        cMontext = "un "
                    End If
                Case 2 : cMontext = "dos "
                Case 3 : cMontext = "tres "
                Case 4 : cMontext = "cuatro "
                Case 5 : cMontext = "cinco "
            End Select
        End If
        Select Case uni
            Case 6 : cMontext = "seis "
            Case 7 : cMontext = "siete "
            Case 8 : cMontext = "ocho "
            Case 9 : cMontext = "nueve "
        End Select
        Unidad = cMontext
        cMontext = ""
    End Function

    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    Me.Dispose(True)
    'End Sub
    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    TextBox2.Text = NumPalabra(Val(TextBox1.Text))
    'End Sub
End Class
