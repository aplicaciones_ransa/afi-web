Imports System.Web.Mail
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Security.Cryptography
Imports Root.Reports
Imports System
Imports System.Drawing
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports Library.AccesoDB
Imports System.Web.HttpContext
Imports System.Configuration

Public Class Funciones
    Private objEntidad As Entidad = New Entidad
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objLiberacion As Liberacion = New Liberacion
    Private objReportes As Reportes = New Reportes
    Private strConn As String = ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private strAGD As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadAGD")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPDFPath2 As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strEndosoPath As String = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strSessMensaje As String
    Private strSessResultado As String
    'Public strMensaje As String

    Public Function SendMail(ByVal strCorreoDe As String, ByVal strCorreoPara As String,
                                ByVal strAsunto As String, ByVal strMensaje As String, ByVal strFile As String) As Boolean
        Try
            '    Dim mmCorreo As New MailMessage
            '    With mmCorreo
            '        Const ConfigNamespace As String = "http://schemas.microsoft.com/cdo/configuration/"
            '        Dim Flds As System.Collections.IDictionary
            '        Flds = mmCorreo.Fields

            '        With Flds
            '            .Add(ConfigNamespace & "smtpserver", System.Configuration.ConfigurationManager.AppSettings("smtpserver"))
            '            .Add(ConfigNamespace & "smtpserverport", System.Configuration.ConfigurationManager.AppSettings("smtpserverport"))
            '            .Add(ConfigNamespace & "sendusing", System.Configuration.ConfigurationManager.AppSettings("sendusing"))
            '            '.Add(ConfigNamespace & "sendusername", System.Configuration.ConfigurationManager.AppSettings("sendusername"))
            '            '.Add(ConfigNamespace & "sendpassword", System.Configuration.ConfigurationManager.AppSettings("sendpassword"))
            '            '.Add(ConfigNamespace & "smtpauthenticate", System.Configuration.ConfigurationManager.AppSettings("smtpauthenticate"))
            '        End With
            '        'Se Indica la Direcci�n de correo que envia
            '        .From = strCorreoDe
            '        'Se Indica la Direcci�n de correo que recibira
            '        .To = strCorreoPara
            '        'Se Indica el Asunto del correo a enviar
            '        .Subject = strAsunto
            '        'El Mensaje del Correo
            '        .Body = strMensaje
            '        'establece el tipo de contenido del texto del mensaje de correo electr�nico.
            '        'MailFormat.Html o MailFormat.Text
            '        .BodyFormat = MailFormat.Html
            '        'Establece la prioridad del mensaje de correo electr�nico
            '        'MailPriority.High, MailPriority.Normal o MailPriority.Low
            '        .Priority = MailPriority.High
            '        'Establece el nombre del servidor de transmisi�n de correo SMTP 
            '        'que se va a utilizar para enviar los mensajes de correo electr�nico.mail.servidor.com 
            '        'SmtpMail.SmtpServer = "161.132.96.92" '"System.Configuration.ConfigurationManager.AppSettings("SMTPServerName")
            '        'Env�a un mensaje de correo electr�nico utilizando argumentos 
            '        'suministrados en las propiedades de la clase MailMessage.
            '        If strFile <> "" Then
            '            Dim idsunit As String() = Split(strFile, "-")
            '            For i As Integer = 0 To idsunit.Length - 1
            '                .Attachments.Add(New MailAttachment(idsunit(i)))
            '            Next
            '        End If
            '        SmtpMail.Send(mmCorreo)
            '    End With
            objDocumento.gEnvioCorreo(strCorreoDe, strCorreoPara, strAsunto, strMensaje, strFile)
            'objDocumento.gEnvioCorreo(strCorreoDe, strCorreoPara, strAsunto, strMensaje, "")
            Return True
        Catch ex As Exception
            Dim errorMessage As String = ""
            While Not IsDBNull(ex)
                errorMessage += ex.Message + "\n"
                ex = ex.InnerException
            End While
            Return False
        End Try
    End Function

    Public Shared Function GetSimpleUserPassword(Optional ByVal length As Int16 = 8) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()
        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)
        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Return guidResult
        Else
            'Return the first length bytes
            Return guidResult.Substring(0, length)
        End If
    End Function

    Public Shared Function EncryptString(ByVal InputString As String, ByVal SecretKey As String,
                                        Optional ByVal CyphMode As CipherMode = CipherMode.ECB) As String
        Dim Des As New TripleDESCryptoServiceProvider
        'Put the string into a byte array
        Dim InputbyteArray() As Byte = Encoding.UTF8.GetBytes(InputString)
        'Create the crypto objects, with the key, as passed in
        Dim hashMD5 As New MD5CryptoServiceProvider
        Des.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(SecretKey))
        Des.Mode = CyphMode
        Dim ms As MemoryStream = New MemoryStream
        Dim cs As CryptoStream = New CryptoStream(ms, Des.CreateEncryptor(),
        CryptoStreamMode.Write)
        'Write the byte array into the crypto stream
        '(It will end up in the memory stream)
        cs.Write(InputbyteArray, 0, InputbyteArray.Length)
        cs.FlushFinalBlock()
        'Get the data back from the memory stream, and into a string
        Dim ret As StringBuilder = New StringBuilder
        Dim b() As Byte = ms.ToArray
        ms.Close()
        Dim I As Integer
        For I = 0 To UBound(b)
            'Format as hex
            ret.AppendFormat("{0:X2}", b(I))
        Next
        Return ret.ToString()
    End Function

    Public Shared Function DecryptString(ByVal InputString As String, ByVal SecretKey As String,
                                            Optional ByVal CyphMode As CipherMode = CipherMode.ECB) As String
        If InputString = String.Empty Then
            Return ""
        Else
            Dim Des As New TripleDESCryptoServiceProvider
            'Put the string into a byte array
            Dim InputbyteArray(CType(InputString.Length / 2 - 1, Integer)) As Byte '= Encoding.UTF8.GetBytes(InputString)
            'Create the crypto objects, with the key, as passed in
            Dim hashMD5 As New MD5CryptoServiceProvider
            Des.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(SecretKey))
            Des.Mode = CyphMode
            'Put the input string into the byte array
            Dim X As Integer
            For X = 0 To InputbyteArray.Length - 1
                Dim IJ As Int32 = (Convert.ToInt32(InputString.Substring(X * 2, 2), 16))
                InputbyteArray(X) = New Byte
                InputbyteArray(X) = CType(CType(IJ, Byte), Byte)
            Next
            Dim ms As MemoryStream = New MemoryStream
            Dim cs As CryptoStream = New CryptoStream(ms, Des.CreateDecryptor(),
            CryptoStreamMode.Write)
            'Flush the data through the crypto stream into the memory stream
            cs.Write(InputbyteArray, 0, InputbyteArray.Length)
            cs.FlushFinalBlock()
            '//Get the decrypted data back from the memory stream
            Dim ret As StringBuilder = New StringBuilder
            Dim B() As Byte = ms.ToArray
            ms.Close()
            Dim I As Integer
            For I = 0 To UBound(B)
                ret.Append(Chr(B(I)))
            Next
            Return ret.ToString()
        End If
    End Function
    Public Function GetTipoDocumento(ByVal cboTipoDocumento As System.Web.UI.WebControls.DropDownList, ByVal intTipo As Int16)
        Dim dtTipoEntidad As DataTable = New DataTable
        cboTipoDocumento.Items.Clear()
        dtTipoEntidad = objDocumento.gGetTipoDocumento
        If intTipo = 1 Then
            cboTipoDocumento.Items.Add(New System.Web.UI.WebControls.ListItem("-----Todos-----", 0))
        End If
        For Each dr As DataRow In dtTipoEntidad.Rows
            cboTipoDocumento.Items.Add(New System.Web.UI.WebControls.ListItem(dr("DSC_CORTA").ToString(), CType(dr("COD_TIPDOC"), String).Trim))
        Next
        dtTipoEntidad.Dispose()
        dtTipoEntidad = Nothing
    End Function
    Public Function GetEntidades(ByVal cboEntidad As System.Web.UI.WebControls.DropDownList, ByVal strTipo As String)
        Dim dtEntidad As New DataTable
        cboEntidad.Items.Clear()
        dtEntidad = objEntidad.gGetEntidades(strTipo, "")
        cboEntidad.Items.Add(New System.Web.UI.WebControls.ListItem("-----------------------------Todos-----------------------------", "0"))
        For Each dr As DataRow In dtEntidad.Rows
            cboEntidad.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
        Next
    End Function
    Public Function GetMonedas(ByVal cboMoneda As System.Web.UI.WebControls.DropDownList)
        Dim dtMonedas As New DataTable
        cboMoneda.Items.Clear()
        dtMonedas = objDocumento.gGetTipoMoneda
        cboMoneda.Items.Add(New System.Web.UI.WebControls.ListItem("", 0))
        For Each dr As DataRow In dtMonedas.Rows
            cboMoneda.Items.Add(New System.Web.UI.WebControls.ListItem(dr("SIM_MONE").ToString(), dr("COD_MONE")))
        Next
        dtMonedas.Dispose()
        dtMonedas = Nothing
    End Function
    Public Function GetloadEntidadUsr(ByVal cboEntidadUsr As System.Web.UI.WebControls.DropDownList, ByVal strCodUsuario As String)
        Dim dtTipoEntidadUsr As DataTable = New DataTable
        cboEntidadUsr.Items.Clear()
        dtTipoEntidadUsr = objEntidad.gGetEntidadesUsuario(strCodUsuario)
        For Each dr As DataRow In dtTipoEntidadUsr.Rows
            cboEntidadUsr.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NOM_ENTI").ToString(), CType(dr("COD_SICO"), String).Trim))
        Next
        dtTipoEntidadUsr.Dispose()
        dtTipoEntidadUsr = Nothing
    End Function
    Public Function GetLlenaEstado(ByVal cboEstadoDoc As System.Web.UI.WebControls.DropDownList, ByVal strTipo As String)
        Dim dt As New DataTable
        cboEstadoDoc.Items.Clear()
        dt = objEntidad.gGetEstado(strTipo)
        cboEstadoDoc.Items.Add(New System.Web.UI.WebControls.ListItem("------Todos-----", 0))
        For Each dr As DataRow In dt.Rows
            cboEstadoDoc.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NOM_ESTA").ToString(), CType(dr("COD_ESTA"), String).Trim))
        Next
        dt.Dispose()
        dt = Nothing
    End Function
    Public Function GetTipoEntidad(ByVal cboTipoEntidad As System.Web.UI.WebControls.DropDownList, ByVal intTipo As Int16)
        Dim dtTipoEntidad As DataTable = New DataTable
        cboTipoEntidad.Items.Clear()
        dtTipoEntidad = objEntidad.gGetTipoEntidades
        If intTipo = 1 Then
            cboTipoEntidad.Items.Add(New System.Web.UI.WebControls.ListItem("------Todos------", 0))
        End If
        If intTipo = 2 Then
            cboTipoEntidad.Items.Add(New System.Web.UI.WebControls.ListItem("Todos", 0))
        End If
        For Each dr As DataRow In dtTipoEntidad.Rows
            cboTipoEntidad.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NOM_TIPENTI").ToString(), CType(dr("COD_TIPENTI"), String).Trim))
        Next
        dtTipoEntidad.Dispose()
        dtTipoEntidad = Nothing
    End Function

    Public Function GetDepartamento(ByVal cboDepartamento As System.Web.UI.WebControls.DropDownList)
        Dim dtDepartamento As DataTable = New DataTable
        cboDepartamento.Items.Clear()
        dtDepartamento = objEntidad.gGetDepartamento
        cboDepartamento.Items.Add(New System.Web.UI.WebControls.ListItem("TODOS", "0"))
        For Each dr As DataRow In dtDepartamento.Rows
            cboDepartamento.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NO_UBIC_GEOG").ToString(), CType(dr("CO_UBIC_GEOG"), String).Trim))
        Next
        dtDepartamento = Nothing
    End Function

    Public Function GetUsuarioAGD(ByVal strTipEnti As String, ByVal cboUsuarioADG As System.Web.UI.WebControls.DropDownList)
        Dim dtUsuarioAGD As DataTable
        cboUsuarioADG.Items.Clear()
        dtUsuarioAGD = objUsuario.gGetUsuarioAGD(strTipEnti, "00000001")
        For Each dr As DataRow In dtUsuarioAGD.Rows
            cboUsuarioADG.Items.Add(New System.Web.UI.WebControls.ListItem(dr("Nombres").ToString(), CType(dr("COD_USER"), String).Trim))
        Next
        dtUsuarioAGD.Dispose()
        dtUsuarioAGD = Nothing
    End Function

    Public Function GetReporteProrrogaFolio(ByVal strNombreReporte As String, ByVal strNroDoc As String,
                                                                ByVal strNroLib As String, ByVal strPeriodoAnual As String,
                                                                ByVal strIdTipoDocumento As String, ByVal strRutaFirmas As String,
                                                                ByVal strDepositante As String, ByVal strFinanciador As String,
                                                                ByVal ObjTrans As SqlTransaction)
        Dim report As Report = New Report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_Time As FontDef = New FontDef(report, FontDef.StandardFont.TimesRoman)
        Dim fp As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.0)
        Dim fp_p As FontProp = New FontPropMM(fd_Time, 4.5, Color.Black)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd_Time, 2.7, Color.Black)
        Dim page As Page = New Page(report)
        Dim rPosLeft As Double = 49
        Dim pp As PenProp = New PenProp(report, 0.8, Color.Black)
        Dim ppBold As PenProp = New PenProp(report, 1.1, Color.Black)
        page.rWidthMM = 210
        page.rHeightMM = 314
        fp_b.bBold = True
        fp_Titulo.bBold = True
        fp_p.bBold = True
        Dim dtProrroga As New DataTable
        Dim intAltura As Integer = 70
        Dim strFirmaClie1 As String
        Dim strFirmaClie2 As String
        Dim strFirmaClie3 As String
        Dim strFirmaFina1 As String
        Dim strFirmaFina2 As String
        Dim strFirmaFina3 As String
        dtProrroga = objDocumento.gGenerarDataProrroga(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, ObjTrans)
        If dtProrroga.Rows(0)("FIR_CLIE1") = "" Then
            strFirmaClie1 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaClie1 = strRutaFirmas & dtProrroga.Rows(0)("FIR_CLIE1")
        End If
        If dtProrroga.Rows(0)("FIR_CLIE2") = "" Then
            strFirmaClie2 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaClie2 = strRutaFirmas & dtProrroga.Rows(0)("FIR_CLIE2")
        End If
        If dtProrroga.Rows(0)("FIR_CLIE3") = "" Then
            strFirmaClie3 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaClie3 = strRutaFirmas & dtProrroga.Rows(0)("FIR_CLIE3")
        End If
        If dtProrroga.Rows(0)("FIR_FINA1") = "" Then
            strFirmaFina1 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaFina1 = strRutaFirmas & dtProrroga.Rows(0)("FIR_FINA1")
        End If
        If dtProrroga.Rows(0)("FIR_FINA2") = "" Then
            strFirmaFina2 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaFina2 = strRutaFirmas & dtProrroga.Rows(0)("FIR_FINA2")
        End If
        If dtProrroga.Rows(0)("FIR_FINA3") = "" Then
            strFirmaFina3 = strRutaFirmas & "aspl.JPG"
        Else
            strFirmaFina3 = strRutaFirmas & dtProrroga.Rows(0)("FIR_FINA3")
        End If

        fp_header.bBold = True
        page.Add(160, intAltura, New RepString(fp_Titulo, "ANEXO AL WARRANT __________________ N� ________"))
        page.Add(300, intAltura, New RepString(fp_Titulo, Convert.ToString(dtProrroga.Rows(0)("MOD_PRORR")).ToUpper))
        page.Add(410, intAltura, New RepString(fp_header, strNroDoc))
        page.Add(450, intAltura + 60, New RepString(fp, "N� ___________"))
        page.Add(480, intAltura + 60, New RepString(fp_b, strNroDoc))
        page.Add(200, intAltura + 60, New RepString(fp, "Warrant con Certificado de Dep�sito Emitido"))
        page.AddLT_MM(150, 42, New RepRectMM(ppBold, 5, 5))
        page.Add(200, intAltura + 80, New RepString(fp, "Warrant sin Certificado de Dep�sito Emitido"))
        page.AddLT_MM(150, 49, New RepRectMM(ppBold, 5, 5))

        If dtProrroga.Rows(0)("EMI_CERT") = "S" Then
            page.AddMM(150, 47, New RepLineMM(ppBold, 5, 5))
            page.AddMM(150, 42, New RepLineMM(ppBold, 5, -5))
        Else
            page.AddMM(150, 54, New RepLineMM(ppBold, 5, 5))
            page.AddMM(150, 49, New RepLineMM(ppBold, 5, -5))
        End If

        page.Add(180, intAltura + 120, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
        page.Add(210, intAltura + 135, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
        page.Add(70, intAltura + 170, New RepString(fp, "Prorrogado el plazo de la obligaci�n a que se refiere el endoso del Warrant, hasta el ________ de"))
        page.Add(480, intAltura + 170, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(6, 2)))
        page.Add(70, intAltura + 190, New RepString(fp, "____________________________ de ____________ por el saldo adeudado a la fecha que es de"))
        page.Add(100, intAltura + 190, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(4, 2))))
        page.Add(280, intAltura + 190, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(0, 4)))
        page.Add(70, intAltura + 210, New RepString(fp, "_______________________________________________________________________________"))
        page.Add(70, intAltura + 210, New RepString(fp_b, dtProrroga.Rows(0)("MON_ADEC") & " " & String.Format("{0:##,##0.00}", dtProrroga.Rows(0)("SAL_ADEC")) & " (" & NumPalabra(dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("MON_ADEC")) & ")"))
        page.Add(70, intAltura + 230, New RepString(fp, "y con garant�a del saldo actual de los bienes, que tienen un valor de ________________________"))
        page.Add(70, intAltura + 250, New RepString(fp, "_______________________________________________________________________________"))
        page.Add(400, intAltura + 230, New RepString(fp_b, dtProrroga.Rows(0)("MON_ACTU") & " " & String.Format("{0:##,##0.00}", dtProrroga.Rows(0)("SAL_ACTU"))))
        page.Add(70, intAltura + 250, New RepString(fp_b, " (" & NumPalabra(dtProrroga.Rows(0)("SAL_ACTU"), dtProrroga.Rows(0)("MON_ACTU")) & ")"))
        page.Add(280, intAltura + 300, New RepString(fp, "___________ de _________________ del _______"))
        page.Add(290, intAltura + 300, New RepString(fp_b, "Lima, " & dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(6, 2)))
        page.Add(390, intAltura + 300, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(4, 2))))
        page.Add(490, intAltura + 300, New RepString(fp_b, dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(0, 4)))
        page.AddMM(20, intAltura + 72, New RepLineMM(ppBold, 80, 0))
        page.AddMM(100, intAltura + 182, New RepLineMM(ppBold, 0, 110))
        page.Add(80, intAltura + 350, New RepString(fp_b, "REGISTRADA LA PRORROGA DEL PLAZO"))
        page.Add(130, intAltura + 360, New RepString(fp_b, "DE LA OBLIGACION"))
        page.Add(70, intAltura + 410, New RepString(fp, "____ de _________________ de ______"))

        If Not IsDBNull(dtProrroga.Rows(0)("NRO_FOLIO")) Then
            page.Add(80, intAltura + 410, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Day))
            page.Add(150, intAltura + 410, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Month)))
            page.Add(235, intAltura + 410, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Year))
            page.Add(130, intAltura + 430, New RepString(fp_b, dtProrroga.Rows(0)("NRO_FOLIO")))
        End If

        page.Add(70, intAltura + 450, New RepString(fp, "Almacenajes adeudados desde"))
        page.Add(70, intAltura + 470, New RepString(fp, "_________________________________"))
        page.Add(70, intAltura + 490, New RepString(fp, "Importe ___________________________"))
        page.Add(60, intAltura + 550, New RepString(fp, "ALMACENERA DEL PER� S.A"))
        page.AddMM(20, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(60, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.Add(340, intAltura + 450, New RepString(fp, "Firma del Deudor - Endosante"))
        page.Add(300, intAltura + 325, New RepString(fp, strDepositante))
        page.AddMM(105, intAltura + 108, New RepLineMM(ppBold, 80, 0))
        page.Add(340, intAltura + 610, New RepString(fp, "Firma del Acreedor - Endosatario"))
        page.Add(300, intAltura + 475, New RepString(fp, strFinanciador))
        page.AddMM(105, intAltura + 165, New RepLineMM(ppBold, 80, 0))

        Dim stream_Firma As Stream

        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(105, 177, New RepImageMM(stream_Firma, Double.NaN, 30))
        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(145, 177, New RepImageMM(stream_Firma, Double.NaN, 30))
        stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(105, 234, New RepImageMM(stream_Firma, Double.NaN, 30))
        stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(145, 234, New RepImageMM(stream_Firma, Double.NaN, 30))

        page = New Page(report)

        page.Add(160, intAltura, New RepString(fp_Titulo, "ANEXO AL WARRANT __________________ N� ________"))
        page.Add(300, intAltura, New RepString(fp_Titulo, Convert.ToString(dtProrroga.Rows(0)("MOD_PRORR")).ToUpper))
        page.Add(410, intAltura, New RepString(fp_header, strNroDoc))
        page.Add(210, intAltura + 15, New RepString(fp_header, "Resoluci�n SBS N� 019-2001 de fecha 16.01.2001"))
        page.Add(70, intAltura + 60, New RepString(fp, "N� __________"))
        page.Add(95, intAltura + 60, New RepString(fp_b, strNroDoc))
        page.Add(290, intAltura + 70, New RepString(fp, "Warrant con Certificado de Dep�sito Emitido"))
        page.AddLT_MM(180, 45, New RepRectMM(ppBold, 5, 5))
        page.Add(290, intAltura + 90, New RepString(fp, "Warrant sin Certificado de Dep�sito Emitido"))
        page.AddLT_MM(180, 52, New RepRectMM(ppBold, 5, 5))

        If dtProrroga.Rows(0)("EMI_CERT") = "S" Then
            page.AddMM(180, 50, New RepLineMM(ppBold, 5, 5))
            page.AddMM(180, 45, New RepLineMM(ppBold, 5, -5))
        Else
            page.AddMM(180, 57, New RepLineMM(ppBold, 5, 5))
            page.AddMM(180, 52, New RepLineMM(ppBold, 5, -5))
        End If

        page.Add(180, intAltura + 150, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
        page.Add(210, intAltura + 165, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
        page.Add(70, intAltura + 230, New RepString(fp, "Prorrogado el plazo del dep�sito a que se refiere el Warrant N� ____________________ por"))
        page.Add(410, intAltura + 230, New RepString(fp_b, strNroDoc))
        page.Add(70, intAltura + 250, New RepString(fp, "__________________________________________________________________________"))

        Dim dttFechaAnterior As Date
        Dim dttFchProrroga As Date

        page.Add(100, intAltura + 250, New RepString(fp_b, Num2Text(dtProrroga.Rows(0)("DIFERENCIA")) & " DIAS"))
        page.Add(70, intAltura + 270, New RepString(fp, "mas que vencer� el ________ de _______________________ del ________ en las mismas"))
        page.Add(180, intAltura + 270, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(6, 2)))
        page.Add(280, intAltura + 270, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(4, 2))))
        page.Add(400, intAltura + 270, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(0, 4)))
        page.Add(70, intAltura + 290, New RepString(fp, "condiciones que la operaci�n original."))
        page.Add(280, intAltura + 390, New RepString(fp, "_________ de __________________ del _______"))

        If Not IsDBNull(dtProrroga.Rows(0)("FCH_FOLIO")) Then
            page.Add(290, intAltura + 390, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Day))
            page.Add(390, intAltura + 390, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Month)))
            page.Add(485, intAltura + 390, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Year))
        End If

        page.AddMM(25, intAltura + 120, New RepLineMM(ppBold, 140, 0))
        page.Add(70, intAltura + 490, New RepString(fp_b, "NOTA: FECHA LIMITE DE PRORROGAS DEL PLAZO DEL WARRANT"))
        page.Add(70, intAltura + 510, New RepString(fp_b, "___________ de _____________________ del ________"))
        page.Add(90, intAltura + 510, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Day))
        page.Add(170, intAltura + 510, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Month)))
        page.Add(265, intAltura + 510, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Year))
        page.AddMM(25, intAltura + 140, New RepLineMM(ppBold, 140, 0))

        page.Add(70, intAltura + 538, New RepString(fp, strDepositante))
        page.Add(330, intAltura + 570, New RepString(fp, "ALMACENERA DEL PER� S.A"))
        page.Add(125, intAltura + 650, New RepString(fp, "Firma del Depositante"))
        page.AddMM(25, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(65, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(105, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(145, intAltura + 180, New RepLineMM(ppBold, 35, 0))

        stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(25, 249, New RepImageMM(stream_Firma, Double.NaN, 30))
        stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
        page.AddMM(65, 249, New RepImageMM(stream_Firma, Double.NaN, 30))

        'page.Add(70, intAltura + 680, New RepString(fp_b, "DE-R-045-GO"))

        If dtProrroga.Rows(0)("EMI_CERT") = "S" Then
            page = New Page(report)

            page.Add(180, intAltura, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
            page.Add(210, intAltura + 15, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
            page.Add(100, intAltura + 60, New RepString(fp_Titulo, "ANEXO AL CERTIFICADO DE DEPOSITO __________________ N� ________"))
            page.Add(340, intAltura + 60, New RepString(fp_Titulo, dtProrroga.Rows(0)("MOD_PRORR").ToUpper))
            page.Add(450, intAltura + 60, New RepString(fp_header, strNroDoc))
            page.Add(70, intAltura + 120, New RepString(fp, "N� ___________"))
            page.Add(90, intAltura + 120, New RepString(fp_b, strNroDoc))
            page.Add(270, intAltura + 120, New RepString(fp, "Certificado de Dep�sito con Warrant Emitido"))
            page.AddLT_MM(175, 63, New RepRectMM(ppBold, 5, 5))
            page.AddMM(175, 68, New RepLineMM(ppBold, 5, 5))
            page.AddMM(175, 63, New RepLineMM(ppBold, 5, -5))
            page.Add(70, intAltura + 170, New RepString(fp, "Prorrogado el plazo de la obligaci�n a que se refiere el endoso del Warrant, hasta el ________ de"))
            page.Add(480, intAltura + 170, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(6, 2)))
            page.Add(70, intAltura + 190, New RepString(fp, "____________________________ de ____________ por el saldo adeudado a la fecha que es de"))
            page.Add(100, intAltura + 190, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(4, 2))))
            page.Add(280, intAltura + 190, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(0, 4)))
            page.Add(70, intAltura + 210, New RepString(fp, "_______________________________________________________________________________"))
            page.Add(70, intAltura + 210, New RepString(fp_b, dtProrroga.Rows(0)("MON_ADEC") & " " & String.Format("{0:##,##0.00}", dtProrroga.Rows(0)("SAL_ADEC")) & " (" & NumPalabra(dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("MON_ADEC")) & ")"))
            page.Add(70, intAltura + 230, New RepString(fp, "y con garant�a del saldo actual de los bienes, que tienen un valor de ________________________"))
            page.Add(70, intAltura + 250, New RepString(fp, "_______________________________________________________________________________"))
            page.Add(400, intAltura + 230, New RepString(fp_b, dtProrroga.Rows(0)("MON_ACTU") & " " & String.Format("{0:##,##0.00}", dtProrroga.Rows(0)("SAL_ACTU"))))
            page.Add(70, intAltura + 250, New RepString(fp_b, " (" & NumPalabra(dtProrroga.Rows(0)("SAL_ACTU"), dtProrroga.Rows(0)("MON_ACTU")) & ")"))
            page.Add(280, intAltura + 300, New RepString(fp, "___________ de _________________ del _______"))
            page.Add(290, intAltura + 300, New RepString(fp_b, "Lima, " & dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(6, 2)))
            page.Add(390, intAltura + 300, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(4, 2))))
            page.Add(490, intAltura + 300, New RepString(fp_b, dtProrroga.Rows(0)("FCH_REGISTRO").ToString.Substring(0, 4)))
            page.AddMM(20, intAltura + 72, New RepLineMM(ppBold, 80, 0))
            page.AddMM(100, intAltura + 182, New RepLineMM(ppBold, 0, 110))
            page.Add(80, intAltura + 350, New RepString(fp_b, "REGISTRADA LA PRORROGA DEL PLAZO"))
            page.Add(130, intAltura + 360, New RepString(fp_b, "DE LA OBLIGACION"))
            page.Add(70, intAltura + 410, New RepString(fp, "____ de _________________ de ______"))

            If Not IsDBNull(dtProrroga.Rows(0)("NRO_FOLIO")) Then
                page.Add(80, intAltura + 410, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Day))
                page.Add(150, intAltura + 410, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Month)))
                page.Add(235, intAltura + 410, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Year))
                page.Add(130, intAltura + 430, New RepString(fp_b, dtProrroga.Rows(0)("NRO_FOLIO")))
            End If

            page.Add(70, intAltura + 450, New RepString(fp, "Almacenajes adeudados desde"))
            page.Add(70, intAltura + 470, New RepString(fp, "_________________________________"))
            page.Add(70, intAltura + 490, New RepString(fp, "Importe ___________________________"))
            page.Add(60, intAltura + 550, New RepString(fp, "ALMACENERA DEL PER� S.A"))
            page.AddMM(20, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(60, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.Add(340, intAltura + 450, New RepString(fp, "Firma del Deudor - Endosante"))
            page.Add(300, intAltura + 325, New RepString(fp, strDepositante))
            page.AddMM(105, intAltura + 108, New RepLineMM(ppBold, 80, 0))
            page.Add(340, intAltura + 610, New RepString(fp, "Firma del Acreedor - Endosatario"))
            page.Add(300, intAltura + 475, New RepString(fp, strFinanciador))
            page.AddMM(105, intAltura + 165, New RepLineMM(ppBold, 80, 0))

            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(105, 177, New RepImageMM(stream_Firma, Double.NaN, 30))
            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(145, 177, New RepImageMM(stream_Firma, Double.NaN, 30))
            stream_Firma = New FileStream(strFirmaFina1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(105, 234, New RepImageMM(stream_Firma, Double.NaN, 30))
            stream_Firma = New FileStream(strFirmaFina2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(145, 234, New RepImageMM(stream_Firma, Double.NaN, 30))

            page = New Page(report)

            page.Add(100, intAltura, New RepString(fp_Titulo, "ANEXO AL CERTIFICADO DE DEPOSITO __________________ N� ________"))
            page.Add(340, intAltura, New RepString(fp_Titulo, Convert.ToString(dtProrroga.Rows(0)("MOD_PRORR")).ToUpper))
            page.Add(450, intAltura, New RepString(fp_header, strNroDoc))
            page.Add(210, intAltura + 15, New RepString(fp_header, "Resoluci�n SBS N� 019-2001 de fecha 16.01.2001"))
            page.Add(70, intAltura + 60, New RepString(fp, "N� __________"))
            page.Add(95, intAltura + 60, New RepString(fp_b, strNroDoc))
            page.Add(290, intAltura + 70, New RepString(fp, "Warrant con Certificado de Dep�sito Emitido"))
            page.AddLT_MM(180, 45, New RepRectMM(ppBold, 5, 5))
            page.Add(290, intAltura + 90, New RepString(fp, "Warrant sin Certificado de Dep�sito Emitido"))
            page.AddLT_MM(180, 52, New RepRectMM(ppBold, 5, 5))

            If dtProrroga.Rows(0)("EMI_CERT") = "S" Then
                page.AddMM(180, 50, New RepLineMM(ppBold, 5, 5))
                page.AddMM(180, 45, New RepLineMM(ppBold, 5, -5))
            Else
                page.AddMM(180, 57, New RepLineMM(ppBold, 5, 5))
                page.AddMM(180, 52, New RepLineMM(ppBold, 5, -5))
            End If

            page.Add(180, intAltura + 150, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
            page.Add(210, intAltura + 165, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
            page.Add(70, intAltura + 230, New RepString(fp, "Prorrogado el plazo del dep�sito a que se refiere el Certificado de Dep�sito N� ____________________ por"))
            page.Add(450, intAltura + 230, New RepString(fp_b, strNroDoc))
            page.Add(70, intAltura + 250, New RepString(fp, "__________________________________________________________________________"))
            page.Add(100, intAltura + 250, New RepString(fp_b, Num2Text(dtProrroga.Rows(0)("DIFERENCIA")) & " DIAS"))
            page.Add(70, intAltura + 270, New RepString(fp, "mas que vencer� el ________ de _______________________ del ________ en las mismas"))
            page.Add(180, intAltura + 270, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(6, 2)))
            page.Add(280, intAltura + 270, New RepString(fp_b, Meses(dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(4, 2))))
            page.Add(400, intAltura + 270, New RepString(fp_b, dtProrroga.Rows(0)("FCH_OBLIGACION").ToString.Substring(0, 4)))
            page.Add(70, intAltura + 290, New RepString(fp, "condiciones que la operaci�n original."))
            page.Add(280, intAltura + 390, New RepString(fp, "_________ de __________________ del _______"))

            If Not IsDBNull(dtProrroga.Rows(0)("FCH_FOLIO")) Then
                page.Add(290, intAltura + 390, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Day))
                page.Add(390, intAltura + 390, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Month)))
                page.Add(485, intAltura + 390, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_FOLIO"), Date).Year))
            End If

            page.AddMM(25, intAltura + 120, New RepLineMM(ppBold, 140, 0))
            page.Add(70, intAltura + 490, New RepString(fp_b, "NOTA: FECHA LIMITE DE PRORROGAS DEL PLAZO DEL WARRANT"))
            page.Add(70, intAltura + 510, New RepString(fp_b, "___________ de _____________________ del ________"))
            page.Add(90, intAltura + 510, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Day))
            page.Add(170, intAltura + 510, New RepString(fp_b, Meses(CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Month)))
            page.Add(265, intAltura + 510, New RepString(fp_b, CType(dtProrroga.Rows(0)("FCH_LIM"), Date).Year))
            page.AddMM(25, intAltura + 140, New RepLineMM(ppBold, 140, 0))

            page.Add(70, intAltura + 538, New RepString(fp, strDepositante))
            page.Add(330, intAltura + 570, New RepString(fp, "ALMACENERA DEL PER� S.A"))
            page.Add(125, intAltura + 650, New RepString(fp, "Firma del Depositante"))
            page.AddMM(25, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(65, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(105, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(145, intAltura + 180, New RepLineMM(ppBold, 35, 0))

            stream_Firma = New FileStream(strFirmaClie1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(25, 249, New RepImageMM(stream_Firma, Double.NaN, 30))

            stream_Firma = New FileStream(strFirmaClie2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(65, 249, New RepImageMM(stream_Firma, Double.NaN, 30))
            'page.Add(70, intAltura + 680, New RepString(fp_b, "DE-R-045-GO"))
        End If

        RT.ViewPDF(report, strNombreReporte)
        stream_Firma.Close()
    End Function

    Public Function GetReporteProrroga(ByVal strNombreReporte As String, ByVal strNroDoc As String,
                                        ByVal strFchRegistro As String, ByVal strFchProrroga As String,
                                        ByVal strSaldoAdecuado As String, ByVal strSaldoActual As String,
                                        ByVal strMonedaAdecuado As String, ByVal strMonedaActual As String,
                                        ByVal strFechaLimite As String, ByVal strFechaAnterior As String,
                                        ByVal strConCertificado As String, ByVal strModalidad As String,
                                        ByVal strDepositante As String, ByVal strFinanciador As String)

        Dim report As Report = New Report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fd_Time As FontDef = New FontDef(report, FontDef.StandardFont.TimesRoman)
        Dim fp As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.0)
        Dim fp_p As FontProp = New FontPropMM(fd_Time, 4.5, Color.Black)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Certificacion As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd_Time, 2.7, Color.Black)
        Dim page As Page = New Page(report)
        Dim rPosLeft As Double = 49
        Dim pp As PenProp = New PenProp(report, 0.8, Color.Black)
        Dim ppBold As PenProp = New PenProp(report, 1.1, Color.Black)
        page.rWidthMM = 210
        page.rHeightMM = 314
        fp_b.bBold = True
        fp_Titulo.bBold = True
        fp_p.bBold = True
        Dim dtEndoso As New DataTable
        Dim intAltura As Integer = 70
        'Dim sadecua As Decimal
        'Dim sactual As Decimal

        'sadecua = Convert.ToDecimal(strSaldoAdecuado.Replace(",", "."))
        'sactual = Convert.ToDecimal(strSaldoActual.Replace(",", "."))

        fp_header.bBold = True
        page.Add(160, intAltura, New RepString(fp_Titulo, "ANEXO AL WARRANT __________________ N� ________"))
        'page.Add(300, intAltura, New RepString(fp_Titulo, strModalidad.ToUpper))
        page.Add(410, intAltura, New RepString(fp_header, strNroDoc))
        page.Add(450, intAltura + 60, New RepString(fp, "N� ___________"))
        page.Add(480, intAltura + 60, New RepString(fp_b, strNroDoc))
        page.Add(200, intAltura + 60, New RepString(fp, "Warrant con Certificado de Dep�sito Emitido"))
        page.AddLT_MM(150, 42, New RepRectMM(ppBold, 5, 5))
        page.Add(200, intAltura + 80, New RepString(fp, "Warrant sin Certificado de Dep�sito Emitido"))
        page.AddLT_MM(150, 49, New RepRectMM(ppBold, 5, 5))

        If strConCertificado = "S" Then
            page.AddMM(150, 47, New RepLineMM(ppBold, 5, 5))
            page.AddMM(150, 42, New RepLineMM(ppBold, 5, -5))
        Else
            page.AddMM(150, 54, New RepLineMM(ppBold, 5, 5))
            page.AddMM(150, 49, New RepLineMM(ppBold, 5, -5))
        End If

        page.Add(180, intAltura + 120, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
        page.Add(210, intAltura + 135, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
        page.Add(70, intAltura + 170, New RepString(fp, "Prorrogado el plazo de la obligaci�n a que se refiere el endoso del Warrant, hasta el ________ de"))
        page.Add(480, intAltura + 170, New RepString(fp_b, strFchProrroga.Substring(0, 2)))
        page.Add(70, intAltura + 190, New RepString(fp, "____________________________ de ____________ por el saldo adeudado a la fecha que es de"))
        page.Add(100, intAltura + 190, New RepString(fp_b, Meses(strFchProrroga.Substring(3, 2))))
        page.Add(280, intAltura + 190, New RepString(fp_b, strFchProrroga.Substring(6, 4)))
        page.Add(70, intAltura + 210, New RepString(fp, "_______________________________________________________________________________"))
        'page.Add(70, intAltura + 210, New RepString(fp_b, strMonedaAdecuado & " " & sadecua & " (" & NumPalabra(sadecua, strMonedaAdecuado) & ")"))
        page.Add(70, intAltura + 210, New RepString(fp_b, strMonedaAdecuado & " " & String.Format("{0:##,##0.00}", Convert.ToDecimal(strSaldoAdecuado)) & " (" & NumPalabra(Convert.ToDouble(strSaldoAdecuado), strMonedaAdecuado) & ")"))
        page.Add(70, intAltura + 230, New RepString(fp, "y con garant�a del saldo actual de los bienes, que tienen un valor de ________________________"))
        page.Add(70, intAltura + 250, New RepString(fp, "_______________________________________________________________________________"))
        'page.Add(400, intAltura + 230, New RepString(fp_b, strMonedaActual & " " & sactual))
        page.Add(400, intAltura + 230, New RepString(fp_b, strMonedaActual & " " & String.Format("{0:##,##0.00}", Convert.ToDecimal(strSaldoActual))))
        page.Add(70, intAltura + 250, New RepString(fp_b, " (" & NumPalabra(Convert.ToDouble(strSaldoActual), strMonedaActual) & ")"))
        page.Add(280, intAltura + 300, New RepString(fp, "___________ de _________________ del _______"))
        page.Add(290, intAltura + 300, New RepString(fp_b, "Lima, " & strFchRegistro.Substring(0, 2)))
        page.Add(390, intAltura + 300, New RepString(fp_b, Meses(strFchRegistro.Substring(3, 2))))
        page.Add(490, intAltura + 300, New RepString(fp_b, strFchRegistro.Substring(6, 4)))
        page.AddMM(20, intAltura + 72, New RepLineMM(ppBold, 80, 0))
        page.AddMM(100, intAltura + 182, New RepLineMM(ppBold, 0, 110))
        page.Add(80, intAltura + 350, New RepString(fp_b, "REGISTRADA LA PRORROGA DEL PLAZO"))
        page.Add(130, intAltura + 360, New RepString(fp_b, "DE LA OBLIGACION"))
        page.Add(70, intAltura + 410, New RepString(fp, "____ de _________________ de ______"))
        page.Add(70, intAltura + 450, New RepString(fp, "Almacenajes adeudados desde"))
        page.Add(70, intAltura + 470, New RepString(fp, "_________________________________"))
        page.Add(70, intAltura + 490, New RepString(fp, "Importe ___________________________"))
        page.Add(60, intAltura + 550, New RepString(fp, "ALMACENERA DEL PER� S.A"))
        page.AddMM(20, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(60, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.Add(340, intAltura + 450, New RepString(fp, "Firma del Deudor - Endosante"))
        page.Add(300, intAltura + 325, New RepString(fp, strDepositante))
        page.AddMM(105, intAltura + 108, New RepLineMM(ppBold, 80, 0))
        page.Add(340, intAltura + 610, New RepString(fp, "Firma del Acreedor - Endosatario"))
        page.Add(300, intAltura + 475, New RepString(fp, strFinanciador))
        page.AddMM(105, intAltura + 165, New RepLineMM(ppBold, 80, 0))

        page = New Page(report)

        page.Add(160, intAltura, New RepString(fp_Titulo, "ANEXO AL WARRANT __________________ N� ________"))
        'page.Add(300, intAltura, New RepString(fp_Titulo, strModalidad.ToUpper))
        page.Add(410, intAltura, New RepString(fp_header, strNroDoc))
        page.Add(210, intAltura + 15, New RepString(fp_header, "Resoluci�n SBS N� 019-2001 de fecha 16.01.2001"))
        page.Add(70, intAltura + 60, New RepString(fp, "N� __________"))
        page.Add(95, intAltura + 60, New RepString(fp_b, strNroDoc))
        page.Add(290, intAltura + 70, New RepString(fp, "Warrant con Certificado de Dep�sito Emitido"))
        page.AddLT_MM(180, 45, New RepRectMM(ppBold, 5, 5))
        page.Add(290, intAltura + 90, New RepString(fp, "Warrant sin Certificado de Dep�sito Emitido"))
        page.AddLT_MM(180, 52, New RepRectMM(ppBold, 5, 5))

        If strConCertificado = "S" Then
            page.AddMM(180, 50, New RepLineMM(ppBold, 5, 5))
            page.AddMM(180, 45, New RepLineMM(ppBold, 5, -5))
        Else
            page.AddMM(180, 57, New RepLineMM(ppBold, 5, 5))
            page.AddMM(180, 52, New RepLineMM(ppBold, 5, -5))
        End If

        page.Add(180, intAltura + 150, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
        page.Add(210, intAltura + 165, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
        page.Add(70, intAltura + 230, New RepString(fp, "Prorrogado el plazo del dep�sito a que se refiere el Warrant N� ____________________ por"))
        page.Add(410, intAltura + 230, New RepString(fp_b, strNroDoc))
        page.Add(70, intAltura + 250, New RepString(fp, "__________________________________________________________________________"))

        Dim dttFechaAnterior As Date
        Dim dttFchProrroga As Date

        dttFechaAnterior = CType(ConvertirFechaFormatoServidorStr(strFechaAnterior), Date)
        dttFchProrroga = CType(ConvertirFechaFormatoServidorStr(strFchProrroga), Date)

        page.Add(100, intAltura + 250, New RepString(fp_b, Num2Text(DateDiff(("d"), dttFechaAnterior, dttFchProrroga)) & " DIAS"))
        page.Add(70, intAltura + 270, New RepString(fp, "mas que vencer� el ________ de _______________________ del ________ en las mismas"))
        page.Add(180, intAltura + 270, New RepString(fp_b, strFchProrroga.Substring(0, 2)))
        page.Add(280, intAltura + 270, New RepString(fp_b, Meses(strFchProrroga.Substring(3, 2))))
        page.Add(400, intAltura + 270, New RepString(fp_b, strFchProrroga.Substring(6, 4)))
        page.Add(70, intAltura + 290, New RepString(fp, "condiciones que la operaci�n original."))
        page.Add(280, intAltura + 390, New RepString(fp, "_________ de __________________ del _______"))
        page.AddMM(25, intAltura + 120, New RepLineMM(ppBold, 140, 0))
        page.Add(70, intAltura + 490, New RepString(fp_b, "NOTA: FECHA LIMITE DE PRORROGAS DEL PLAZO DEL WARRANT"))
        page.Add(70, intAltura + 510, New RepString(fp_b, "___________ de _____________________ del ________"))
        page.Add(90, intAltura + 510, New RepString(fp_b, strFechaLimite.Substring(0, 2)))
        page.Add(170, intAltura + 510, New RepString(fp_b, Meses(strFechaLimite.Substring(3, 2))))
        page.Add(265, intAltura + 510, New RepString(fp_b, strFechaLimite.Substring(6, 4)))
        page.AddMM(25, intAltura + 140, New RepLineMM(ppBold, 140, 0))

        page.Add(70, intAltura + 538, New RepString(fp, strDepositante))
        page.Add(330, intAltura + 570, New RepString(fp, "ALMACENERA DEL PER� S.A"))
        page.Add(125, intAltura + 650, New RepString(fp, "Firma del Depositante"))
        page.AddMM(25, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(65, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(105, intAltura + 180, New RepLineMM(ppBold, 35, 0))
        page.AddMM(145, intAltura + 180, New RepLineMM(ppBold, 35, 0))

        'page.Add(70, intAltura + 680, New RepString(fp_b, "DE-R-045-GO"))

        If strConCertificado = "S" Then
            page = New Page(report)

            page.Add(180, intAltura, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
            page.Add(210, intAltura + 15, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
            page.Add(100, intAltura + 60, New RepString(fp_Titulo, "ANEXO AL CERTIFICADO DE DEPOSITO __________________ N� ________"))
            'page.Add(340, intAltura + 60, New RepString(fp_Titulo, strModalidad.ToUpper))
            page.Add(450, intAltura + 60, New RepString(fp_header, strNroDoc))
            page.Add(70, intAltura + 120, New RepString(fp, "N� ___________"))
            page.Add(90, intAltura + 120, New RepString(fp_b, strNroDoc))
            page.Add(270, intAltura + 120, New RepString(fp, "Certificado de Dep�sito con Warrant Emitido"))
            page.AddLT_MM(175, 63, New RepRectMM(ppBold, 5, 5))
            page.AddMM(175, 68, New RepLineMM(ppBold, 5, 5))
            page.AddMM(175, 63, New RepLineMM(ppBold, 5, -5))
            page.Add(70, intAltura + 170, New RepString(fp, "Prorrogado el plazo de la obligaci�n a que se refiere el endoso del Warrant, hasta el ________ de"))
            page.Add(480, intAltura + 170, New RepString(fp_b, strFchProrroga.Substring(0, 2)))
            page.Add(70, intAltura + 190, New RepString(fp, "____________________________ de ____________ por el saldo adeudado a la fecha que es de"))
            page.Add(100, intAltura + 190, New RepString(fp_b, Meses(strFchProrroga.Substring(3, 2))))
            page.Add(280, intAltura + 190, New RepString(fp_b, strFchProrroga.Substring(6, 4)))
            page.Add(70, intAltura + 210, New RepString(fp, "_______________________________________________________________________________"))
            page.Add(70, intAltura + 210, New RepString(fp_b, strMonedaAdecuado & " " & String.Format("{0:##,##0.00}", Convert.ToDecimal(strSaldoAdecuado)) & " (" & NumPalabra(Convert.ToDouble(strSaldoAdecuado), strMonedaAdecuado) & ")"))
            'page.Add(70, intAltura + 210, New RepString(fp_b, strMonedaAdecuado & " " & sadecua & " (" & NumPalabra(sadecua, strMonedaAdecuado) & ")"))

            page.Add(70, intAltura + 230, New RepString(fp, "y con garant�a del saldo actual de los bienes, que tienen un valor de ________________________"))
            page.Add(70, intAltura + 250, New RepString(fp, "_______________________________________________________________________________"))
            page.Add(400, intAltura + 230, New RepString(fp_b, strMonedaActual & " " & String.Format("{0:##,##0.00}", Convert.ToDecimal(strSaldoActual))))
            ' page.Add(400, intAltura + 230, New RepString(fp_b, strMonedaActual & " " & sactual))

            page.Add(70, intAltura + 250, New RepString(fp_b, " (" & NumPalabra(Convert.ToDouble(strSaldoActual), strMonedaActual) & ")"))
            'page.Add(70, intAltura + 250, New RepString(fp_b, " (" & NumPalabra(sactual, strMonedaActual) & ")"))

            page.Add(280, intAltura + 300, New RepString(fp, "___________ de _________________ del _______"))
            page.Add(290, intAltura + 300, New RepString(fp_b, "Lima, " & strFchRegistro.Substring(0, 2)))
            page.Add(390, intAltura + 300, New RepString(fp_b, Meses(strFchRegistro.Substring(3, 2))))
            page.Add(490, intAltura + 300, New RepString(fp_b, strFchRegistro.Substring(6, 4)))
            page.AddMM(20, intAltura + 72, New RepLineMM(ppBold, 80, 0))
            page.AddMM(100, intAltura + 182, New RepLineMM(ppBold, 0, 110))
            page.Add(80, intAltura + 350, New RepString(fp_b, "REGISTRADA LA PRORROGA DEL PLAZO"))
            page.Add(130, intAltura + 360, New RepString(fp_b, "DE LA OBLIGACION"))
            page.Add(70, intAltura + 410, New RepString(fp, "____ de _________________ de ______"))
            page.Add(70, intAltura + 450, New RepString(fp, "Almacenajes adeudados desde"))
            page.Add(70, intAltura + 470, New RepString(fp, "_________________________________"))
            page.Add(70, intAltura + 490, New RepString(fp, "Importe ___________________________"))
            page.Add(60, intAltura + 550, New RepString(fp, "ALMACENERA DEL PER� S.A"))
            page.AddMM(20, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(60, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.Add(340, intAltura + 450, New RepString(fp, "Firma del Deudor - Endosante"))
            page.Add(300, intAltura + 325, New RepString(fp, strDepositante))
            page.AddMM(105, intAltura + 108, New RepLineMM(ppBold, 80, 0))
            page.Add(340, intAltura + 610, New RepString(fp, "Firma del Acreedor - Endosatario"))
            page.Add(290, intAltura + 475, New RepString(fp, strFinanciador))
            page.AddMM(105, intAltura + 165, New RepLineMM(ppBold, 80, 0))

            page = New Page(report)

            page.Add(100, intAltura, New RepString(fp_Titulo, "ANEXO AL CERTIFICADO DE DEPOSITO __________________ N� ________"))
            'page.Add(340, intAltura, New RepString(fp_Titulo, strModalidad.ToUpper))
            page.Add(450, intAltura, New RepString(fp_header, strNroDoc))
            page.Add(210, intAltura + 15, New RepString(fp_header, "Resoluci�n SBS N� 019-2001 de fecha 16.01.2001"))
            page.Add(70, intAltura + 60, New RepString(fp, "N� __________"))
            page.Add(95, intAltura + 60, New RepString(fp_b, strNroDoc))
            page.Add(290, intAltura + 70, New RepString(fp, "Certificado de Dep�sito con Warrant Emitido"))
            page.AddLT_MM(180, 45, New RepRectMM(ppBold, 5, 5))
            page.Add(290, intAltura + 90, New RepString(fp, "Certificado de Dep�sito sin Warrant Emitido"))
            page.AddLT_MM(180, 52, New RepRectMM(ppBold, 5, 5))
            page.AddMM(180, 50, New RepLineMM(ppBold, 5, 5))
            page.AddMM(180, 45, New RepLineMM(ppBold, 5, -5))
            page.Add(180, intAltura + 150, New RepString(fp_p, "ALMACENERA DEL PER� S.A"))
            page.Add(210, intAltura + 165, New RepString(fp_header, "Jr. Gaspar Hern�ndez 760-Lima. RUC 20100000688"))
            page.Add(70, intAltura + 230, New RepString(fp, "Prorrogado el plazo del dep�sito a que se refiere el Certificado de Dep�sito N� _____________ por"))
            page.Add(450, intAltura + 230, New RepString(fp_b, strNroDoc))
            page.Add(70, intAltura + 250, New RepString(fp, "__________________________________________________________________________"))
            page.Add(100, intAltura + 250, New RepString(fp_b, Num2Text(DateDiff(("d"), dttFechaAnterior, dttFchProrroga)) & " DIAS"))
            page.Add(70, intAltura + 270, New RepString(fp, "mas que vencer� el ________ de _______________________ del ________ en las mismas"))
            page.Add(180, intAltura + 270, New RepString(fp_b, strFchProrroga.Substring(0, 2)))
            page.Add(280, intAltura + 270, New RepString(fp_b, Meses(strFchProrroga.Substring(3, 2))))
            page.Add(400, intAltura + 270, New RepString(fp_b, strFchProrroga.Substring(6, 4)))
            page.Add(70, intAltura + 290, New RepString(fp, "condiciones que la operaci�n original."))
            page.Add(280, intAltura + 390, New RepString(fp, "_________ de __________________ del _______"))
            page.AddMM(25, intAltura + 120, New RepLineMM(ppBold, 140, 0))
            page.Add(70, intAltura + 490, New RepString(fp_b, "NOTA: FECHA LIMITE DE PRORROGAS DEL PLAZO DEL WARRANT"))
            page.Add(70, intAltura + 510, New RepString(fp_b, "___________ de _____________________ del ________"))
            page.Add(90, intAltura + 510, New RepString(fp_b, strFechaLimite.Substring(0, 2)))
            page.Add(170, intAltura + 510, New RepString(fp_b, Meses(strFechaLimite.Substring(3, 2))))
            page.Add(265, intAltura + 510, New RepString(fp_b, strFechaLimite.Substring(6, 4)))
            page.AddMM(25, intAltura + 140, New RepLineMM(ppBold, 140, 0))

            page.Add(70, intAltura + 538, New RepString(fp, strDepositante))
            page.Add(330, intAltura + 570, New RepString(fp, "ALMACENERA DEL PER� S.A"))
            page.Add(125, intAltura + 650, New RepString(fp, "Firma del Depositante"))
            page.AddMM(25, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(65, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(105, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            page.AddMM(145, intAltura + 180, New RepLineMM(ppBold, 35, 0))
            'page.Add(70, intAltura + 680, New RepString(fp_b, "DE-R-045-GO"))
        End If
        RT.ViewPDF(report, strNombreReporte)

    End Function
    Public Function Meses(ByVal value As Double) As String
        Select Case value
            Case 1 : Return "Enero"
            Case 2 : Return "Febrero"
            Case 3 : Return "Marzo"
            Case 4 : Return "Abril"
            Case 5 : Return "Mayo"
            Case 6 : Return "Junio"
            Case 7 : Return "Julio"
            Case 8 : Return "Agosto"
            Case 9 : Return "Septiembre"
            Case 10 : Return "Octubre"
            Case 11 : Return "Noviembre"
            Case 12 : Return "Diciembre"
        End Select
    End Function
    Public Function Num2Text(ByVal value As Double) As String
        Select Case value
            Case 0 : Num2Text = "CERO"
            Case 1 : Num2Text = "UN"
            Case 2 : Num2Text = "DOS"
            Case 3 : Num2Text = "TRES"
            Case 4 : Num2Text = "CUATRO"
            Case 5 : Num2Text = "CINCO"
            Case 6 : Num2Text = "SEIS"
            Case 7 : Num2Text = "SIETE"
            Case 8 : Num2Text = "OCHO"
            Case 9 : Num2Text = "NUEVE"
            Case 10 : Num2Text = "DIEZ"
            Case 11 : Num2Text = "ONCE"
            Case 12 : Num2Text = "DOCE"
            Case 13 : Num2Text = "TRECE"
            Case 14 : Num2Text = "CATORCE"
            Case 15 : Num2Text = "QUINCE"
            Case Is < 20 : Num2Text = "DIECI" & Num2Text(value - 10)
            Case 20 : Num2Text = "VEINTE"
            Case Is < 30 : Num2Text = "VEINTI" & Num2Text(value - 20)
            Case 30 : Num2Text = "TREINTA"
            Case 40 : Num2Text = "CUARENTA"
            Case 50 : Num2Text = "CINCUENTA"
            Case 60 : Num2Text = "SESENTA"
            Case 70 : Num2Text = "SETENTA"
            Case 80 : Num2Text = "OCHENTA"
            Case 90 : Num2Text = "NOVENTA"
            Case Is < 100 : Num2Text = Num2Text(Int(value \ 10) * 10) & " Y " & Num2Text(value Mod 10)
            Case 100 : Num2Text = "CIEN"
            Case Is < 200 : Num2Text = "CIENTO " & Num2Text(value - 100)
            Case 200, 300, 400, 600, 800 : Num2Text = Num2Text(Int(value \ 100)) & "CIENTOS"
            Case 500 : Num2Text = "QUINIENTOS"
            Case 700 : Num2Text = "SETECIENTOS"
            Case 900 : Num2Text = "NOVECIENTOS"
            Case Is < 1000 : Num2Text = Num2Text(Int(value \ 100) * 100) & " " & Num2Text(value Mod 100)
            Case 1000 : Num2Text = "MIL"
            Case Is < 2000 : Num2Text = "MIL " & Num2Text(value Mod 1000)
            Case Is < 1000000 : Num2Text = Num2Text(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then Num2Text = Num2Text & " " & Num2Text(value Mod 1000)
            Case 1000000 : Num2Text = "UN MILLON"
            Case Is < 2000000 : Num2Text = "UN MILLON " & Num2Text(value Mod 1000000)
            Case Is < 1000000000000.0# : Num2Text = Num2Text(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000) * 1000000)
            Case 1000000000000.0# : Num2Text = "UN BILLON"
            Case Is < 2000000000000.0# : Num2Text = "UN BILLON " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : Num2Text = Num2Text(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

    'Public Function gFirmarDocumento(ByVal strUsuarioId As String, ByVal strNroDoc As String,
    '                         ByVal strNroLib As String, ByVal strPeriodoAnual As String,
    '                         ByVal strTipoDoc As String, ByVal strPag As String,
    '                         ByVal strIdSico As String, ByVal byPDF As Byte(),
    '                         ByVal ObjTrans As SqlTransaction) As String

    Public Function gFirmarDocumento(ByVal strUsuarioId As String, strUsuarioLogin As String, ByVal strNroDoc As String,
                         ByVal strNroLib As String, ByVal strPeriodoAnual As String, ByVal strTipoDoc As String, strCertDigital As String,
                             ByVal strCod_Doc As String, strCod_TipOtro As String, ByVal strIdSico As String, strIdTipoEntidad As String,
                             strCodUnidad As String, ByVal byPDF As Byte(), ByVal strPag As String, ByVal ObjTrans As SqlTransaction) As String
        Try
            Dim blnFirmo As Boolean = True
            Dim dtEntidad As New DataTable
            Dim dtSecu As New DataTable
            'Dim strEmails As String
            Dim strBody As String = ""
            'Dim strAprobaron As String
            Dim strEmailsConAdjunto As String = ""
            Dim strEmailsSinAdjunto As String = ""
            Dim strDestinatarioCarta As String = ""
            Dim strPath As String = ""
            If blnFirmo Then
                strSessResultado = "1"
                ''---ini--- prueba enviar datos ingresados
                'Dim strCadena As String = strUsuarioId & " , " & strUsuarioLogin & " , " & strNroDoc & " , " & strNroLib & " , " & strPeriodoAnual & " , " & strTipoDoc & " , " & strCertDigital & " , " & strCod_Doc & " , " & strCod_TipOtro & " , " & strIdSico & " , " & strIdTipoEntidad & " , " & strCodUnidad & " , " & strPag
                'SendMail("infodepsaWS_prueba(gFirmarDocumento)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(gFirmarDocumento)", "  strCadena : " & strCadena, "")
                ''---fin--- prueba enviar datos ingresados

                If objDocumento.gGetValidarFirmantes(strUsuarioId, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strIdSico, ObjTrans) = 0 Then 'ok(sin sessiones internas)
                    If strCertDigital = True Then
                        'strSessResultado = "2"
                        objDocumento.gRegistraArchivoFirmado(strNroDoc, strNroLib, strPeriodoAnual, byPDF, strTipoDoc, strPag, ObjTrans) 'ok(sin sessiones internas)
                    End If
                    If strPag = "3" Then  ' RECHAZAR
                        'dtEntidad = objDocumento.gUpdRechazarDocumentoRetiro(strUsuarioId, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strPag, strIdSico, ObjTrans)
                    Else
                        'CerrarComprobante 06/12/2018
                        'strSessResultado = "3"
                        If strTipoDoc = "01" Or strTipoDoc = "04" Or strTipoDoc = "10" Then 'warrant y wip
                            'strSessResultado = "4"
                            dtSecu = objDocumento.gBuscarSecuencia(strTipoDoc, strNroDoc, strNroLib, strUsuarioId) 'ok(sin sessiones internas)
                            If dtSecu.Rows.Count <> 0 Then
                                'strSessResultado = "5"
                                If strIdTipoEntidad = strAGD And dtSecu.Rows(0)("NRO_SECU") = dtSecu.Rows(0)("NRO_SECU_MIN") Then
                                    If objDocumento.gCerrarComprobante(strTipoDoc, strNroDoc, strUsuarioLogin) = False Then 'ok(sin sessiones internas)
                                        Return objDocumento.strResultado
                                    End If
                                End If
                            End If
                        End If
                        'strSessResultado = "6"
                        dtEntidad = objDocumento.gUpdDocumento(strUsuarioId, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strPag, strIdSico, ObjTrans) 'ok(sin sessiones internas)
                    End If
                    'strSessResultado = "7"

                    If dtEntidad.Rows.Count > 0 Then
                        For i As Integer = 0 To dtEntidad.Rows.Count - 1
                            If dtEntidad.Rows(i)("EMAI") <> "" Then
                                If dtEntidad.Rows(i)("FLG_ADJU") Then
                                    strEmailsConAdjunto = strEmailsConAdjunto & dtEntidad.Rows(i)("EMAI") & ","
                                End If
                                If dtEntidad.Rows(0)("ACT_ADJU") = "1" Then
                                    If dtEntidad.Rows(i)("FLG_NOTI") And dtEntidad.Rows(i)("FLG_ADJU") = False Then
                                        strEmailsSinAdjunto = strEmailsSinAdjunto & dtEntidad.Rows(i)("EMAI") & ","
                                    End If
                                Else
                                    If dtEntidad.Rows(i)("FLG_NOTI") Then
                                        strEmailsSinAdjunto = strEmailsSinAdjunto & dtEntidad.Rows(i)("EMAI") & ","
                                    End If
                                End If
                            End If
                        Next
                        strSessResultado = "8"

                        If dtEntidad.Rows(0)("ACT_ADJU") = "1" Then
                            Dim dtDetalle As DataTable
                            Dim strNombrePDF As String

                            If strTipoDoc = "02" Or strTipoDoc = "13" Or strTipoDoc = "14" Then
                                strSessResultado = "9"
                                If objDocumento.gGetEstadoImpresion(strCod_Doc, ObjTrans) = False Then 'ok(sin sessiones internas)
                                    strPath = strEndosoPath
                                    strNombrePDF = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
                                    dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCodUnidad, strNroLib,
                                                                                            strCod_TipOtro, strNroDoc, ObjTrans) 'ok(sin sessiones internas)
                                    '=== Generar Liberaci�n PDF
                                    objLiberacion.GetReporteLiberacionFirmas(strNombrePDF, strEmpresa, strCodUnidad, "DOR",
                                                                        dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle,
                                                                        strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"),
                                                                        dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"),
                                                                        strTipoDoc, ObjTrans, strPathFirmas) 'ok(sin sessiones internas)
                                    GrabaDocumento(strNombrePDF, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, ObjTrans) 'ok(sin sessiones internas)
                                    dtDetalle.Dispose()
                                    strTipoDoc = "02"
                                End If
                                strSessResultado = "10"
                            ElseIf strTipoDoc = "09" Or strTipoDoc = "17" Or strTipoDoc = "18" Or strTipoDoc = "19" Then

                                objReportes.GeneraDCR(strCod_Doc, strPDFPath, strPathFirmas, True, strTipoDoc, strUsuarioId, ObjTrans) 'ok(sin sessiones internas)
                                strDestinatarioCarta = objDocumento.gCADGetTraerDestinatarioCarta(strCod_Doc, ObjTrans) 'ok(sin sessiones internas)
                                If strDestinatarioCarta <> "" Then
                                    strEmailsConAdjunto = strDestinatarioCarta
                                    strEmailsSinAdjunto = ""
                                End If
                                strPath = strPDFPath
                            ElseIf strTipoDoc = "21" Then

                                strPath = strEndosoPath
                                strNombrePDF = strEndosoPath & "21" & strNroDoc & strNroLib & ".pdf"

                                Dim dtCustodia As New DataTable
                                dtCustodia = objDocumento.gGetLiberacionWarCustodia(strNroDoc, ObjTrans) 'ok(sin sessiones internas)

                                objLiberacion.GetReporteLiberacionCustodia(strNombrePDF, strEmpresa, "DOR", strNroLib, strIdSico, dtCustodia, False, False, True, strPathFirmas)
                                GrabaDocumento(strNombrePDF, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, ObjTrans) 'ok(sin sessiones internas)
                                dtCustodia.Dispose()
                            ElseIf (strTipoDoc = "22" Or strTipoDoc = "23") And strPag <> "3" Then
                                Dim strNombreCliente As String
                                objReportes.LstrNombrePDF = strPDFPath & strCodUnidad & strNroLib & ".PDF"
                                objReportes.GeneraRetiro(strCod_Doc, strPDFPath, strPathFirmas, True, strTipoDoc, ObjTrans) 'ok(sin sessiones internas)
                                strPath = strPDFPath
                            ElseIf strTipoDoc = "12" Then
                                Dim strDblEndoso As String
                                strNombrePDF = "12" & strNroDoc & strNroLib & ".pdf"
                                objReportes.GrabarPDFxDocumento = True
                                'objReportes.GeneraPDFWarrantFirmasXDocumento(Current.Session.Item("Cod_Doc"), Current.Session.Item("Cod_Doc"), strEndosoPath, strPathFirmas, True, True, strUsuarioId, strTipoDoc, strNombrePDF, ObjTrans)
                                objReportes.GetReporteWar(strCod_Doc, strEndosoPath, strPathFirmas, True, True, ObjTrans, strDblEndoso, False, strTipoDoc, strCod_Doc, strNombrePDF) 'ok(sin sessiones internas)
                                strPath = strEndosoPath
                            End If
                        End If
                        strSessResultado = "11"
                        If strBody = "" Then
                            strBody = "Estimados Se�ores:<br><br>"
                            If strTipoDoc = "01" Or strTipoDoc = "03" Or strTipoDoc = "04" Or strTipoDoc = "10" Or strTipoDoc = "12" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>Warrant N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "02" Or strTipoDoc = "13" Or strTipoDoc = "14" Or strTipoDoc = "11" Or strTipoDoc = "15" Or strTipoDoc = "16" Or strTipoDoc = "25" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>" & dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_LIBE") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtEntidad.Rows(0)("MONTO")) & "</FONT></STRONG><br>" &
                            "Warrant N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "21" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>" & dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("DSC_MERC") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtEntidad.Rows(0)("MONTO")) & "</FONT></STRONG><br>" &
                            "Warrant N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "05" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>" & dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_LIBE") & "</FONT></STRONG><br>" &
                            "Warrant N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtEntidad.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "09" Or strTipoDoc = "17" Or strTipoDoc = "18" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>" & dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtEntidad.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "19" Then
                                strBody += dtEntidad.Rows(0)("DSC_SECU") & ":<br><br>" & dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("DSC_MERC") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencial: <STRONG><FONT color='#330099'>" & CStr(dtEntidad.Rows(0)("NRO_DOCU")).Trim & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "20" Then
                                strBody += dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                            End If
                            If strTipoDoc = "22" Or strTipoDoc = "23" Then
                                strBody += dtEntidad.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NRO_LIBE") & "</FONT></STRONG><br>"
                            End If
                            strBody += "Depositante: <STRONG><FONT color='#330099'>" & dtEntidad.Rows(0)("NOM_ENTI") & "</FONT></STRONG>"
                            strBody += "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>"
                        End If
                        strSessResultado = "12"
                        If strTipoDoc = "22" Or strTipoDoc = "23" Then
                            strPDFPath = strPath & strCodUnidad & strNroLib & ".pdf"
                        Else
                            strPDFPath = strPath & strTipoDoc & strNroDoc & strNroLib & ".pdf"
                        End If
                        strSessResultado = "13"
                        '-------------------****************** Codigo Agregado *********************---------------------
                        If strIdTipoEntidad = "03" And (strTipoDoc = "02" Or strTipoDoc = "11") Then
                            Dim strNombrePDF As String
                            Dim dtDetalle As New DataTable
                            strPath = strEndosoPath
                            strNombrePDF = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
                            strSessResultado = "14"
                            dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCodUnidad, strNroLib,
                                                                                        strCod_TipOtro, strNroDoc, ObjTrans) 'ok(sin sessiones internas)
                            objLiberacion.GetReporteLiberacionActFirmas(strNombrePDF, strEmpresa, strCodUnidad, "DOR",
                                                                    dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle,
                                                                    strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"),
                                                                    dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"),
                                                                    strTipoDoc, ObjTrans, strPathFirmas) 'ok(sin sessiones internas)
                            UpdDocumento(strNombrePDF, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, ObjTrans) 'ok(sin sessiones internas)
                            dtDetalle.Dispose()
                            strSessResultado = "15"
                            strPDFPath = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
                        End If
                        '-------------------****************** Codigo Agregado *********************---------------------
                        strSessResultado = "16"
                        If strPag = "3" Then
                            If strEmailsConAdjunto <> "" And strPath <> "" Then
                                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                           "Se rechaz� el retiro  - " & CStr(dtEntidad.Rows(0)("NOM_ENTI")).Trim,
                           strBody, strPDFPath)
                            End If

                            If strEmailsSinAdjunto <> "" Then
                                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                             "Se rechaz� el retiro - " & CStr(dtEntidad.Rows(0)("NOM_ENTI")).Trim,
                           strBody, "")
                            End If
                        Else
                            If strEmailsConAdjunto <> "" And strPath <> "" Then
                                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                           CStr(dtEntidad.Rows(0)("DSC_SECU")).Trim & " - " & CStr(dtEntidad.Rows(0)("NOM_ENTI")).Trim,
                           strBody, strPDFPath)
                            End If

                            If strEmailsSinAdjunto <> "" Then
                                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                             CStr(dtEntidad.Rows(0)("DSC_SECU")).Trim & " - " & CStr(dtEntidad.Rows(0)("NOM_ENTI")).Trim,
                           strBody, "")
                            End If
                        End If
                    End If
                    strSessResultado = "17"
                    '-------------------****************** Codigo Agregado *********************---------------------
                    'If Current.Session.Item("IdTipoEntidad") = "03" And (strTipoDoc = "02" Or strTipoDoc = "11") Then
                    '    Dim strNombrePDF As String
                    '    Dim dtDetalle As New DataTable
                    '    strPath = strPath
                    '    strNombrePDF = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
                    '    dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, Current.Session.Item("Cod_Unidad"), Current.Session.Item("NroLib"), _
                    '                                                                                Current.Session.Item("Cod_TipOtro"), Current.Session.Item("NroDoc"), ObjTrans)
                    '    objLiberacion.GetReporteLiberacionActFirmas(strNombrePDF, strEmpresa, Current.Session.Item("Cod_Unidad"), "DOR", _
                    '                                                            dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, _
                    '                                                            Current.Session.Item("Cod_TipOtro"), dtDetalle.Rows(0)("EMB_LIBE"), _
                    '                                                            dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), _
                    '                                                            Current.Session.Item("IdTipoDocumento"), ObjTrans, strPathFirmas)
                    '    UpdDocumento(strNombrePDF, ObjTrans)
                    '    dtDetalle.Dispose()
                    '    'strTipoDoc = "02"
                    'End If
                    '-------------------****************** Codigo Agregado *********************---------------------
                    strSessResultado = "18"
                    dtEntidad.Dispose()
                    dtEntidad = Nothing
                    If strPag = "3" Then

                        strSessResultado = "I" : strSessMensaje = "Se Rechaz� con exito"

                        '---ini--- prueba enviar datos ingresados
                        'Dim strCadena1 As String = strUsuarioId & " , " & strUsuarioLogin & " , " & strNroDoc & " , " & strNroLib & " , " & strPeriodoAnual & " , " & strTipoDoc & " , " & strCertDigital & " , " & strCod_Doc & " , " & strCod_TipOtro & " , " & strIdSico & " , " & strIdTipoEntidad & " , " & strCodUnidad & " , " & strPag
                        'SendMail("infodepsaWS_prueba(gFirmarDocumento)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(gFirmarDocumento)", "  strCadena : " & strCadena1 & "<br> Resultado : " & strSessMensaje, "")
                        '---fin--- prueba enviar datos ingresados

                        Return strSessMensaje
                    Else
                        strSessResultado = "I" : strSessMensaje = "Se Firm� con exito"

                        '---ini--- prueba enviar datos ingresados
                        'Dim strCadena2 As String = strUsuarioId & " , " & strUsuarioLogin & " , " & strNroDoc & " , " & strNroLib & " , " & strPeriodoAnual & " , " & strTipoDoc & " , " & strCertDigital & " , " & strCod_Doc & " , " & strCod_TipOtro & " , " & strIdSico & " , " & strIdTipoEntidad & " , " & strCodUnidad & " , " & strPag
                        'SendMail("infodepsaWS_prueba(gFirmarDocumento)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(gFirmarDocumento)", "  strCadena : " & strCadena2 & "<br> Resultado : " & strSessMensaje, "")
                        '---fin--- prueba enviar datos ingresados

                        Return strSessMensaje
                    End If
                Else
                    strSessResultado = "X" : strSessMensaje = "Ya firm� el documento, evite de hacer multiples click"

                    '---ini--- prueba enviar datos ingresados
                    'Dim strCadena3 As String = strUsuarioId & " , " & strUsuarioLogin & " , " & strNroDoc & " , " & strNroLib & " , " & strPeriodoAnual & " , " & strTipoDoc & " , " & strCertDigital & " , " & strCod_Doc & " , " & strCod_TipOtro & " , " & strIdSico & " , " & strIdTipoEntidad & " , " & strCodUnidad & " , " & strPag
                    'SendMail("infodepsaWS_prueba(gFirmarDocumento)<" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "fmalavers@depsa.com.pe", "infodepsaWS_prueba(gFirmarDocumento)", "  strCadena : " & strCadena3 & "<br> Resultado : " & strSessMensaje, "")
                    '---fin--- prueba enviar datos ingresados

                    Return strSessMensaje
                End If
            End If
        Catch ex As Exception
            'strMensaje = ex.Message
            strSessResultado = "X" : strSessMensaje = ex.Message.ToString
            Return strSessResultado
        End Try
    End Function

    Sub gEnvioMailConAdjunto(ByVal strNroDoc As String, ByVal strNroLib As String,
                        ByVal strPeriodoAnual As String, ByVal strTipoDocumento As String,
                        ByVal strBody As String, ByVal strPath As String, ByVal strFlgTodos As String, ByVal strFlgAnulacion As String,
                        ByVal ObjTrans As SqlTransaction)
        Dim dtMail As New DataTable
        Dim strEmailsConAdjunto As String = ""
        Dim strEmailsSinAdjunto As String = ""
        Dim strCuerpo As String
        dtMail = objDocumento.gEnviarMail(strNroDoc, strNroLib, strPeriodoAnual, strTipoDocumento, strFlgTodos, ObjTrans)
        If dtMail.Rows.Count > 0 Then
            For i As Integer = 0 To dtMail.Rows.Count - 1
                If dtMail.Rows(i)("FLG_ADJU") And strPath <> "" Then
                    strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("EMAI") & ","
                Else
                    strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("EMAI") & ","
                End If
            Next
            If strFlgAnulacion = "S" Then
                If strTipoDocumento = "01" Or strTipoDocumento = "03" Or strTipoDocumento = "04" Or strTipoDocumento = "10" Then
                    strCuerpo = "<br><br>Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "02" Or strTipoDocumento = "13" Or strTipoDocumento = "14" Or strTipoDocumento = "11" Or strTipoDocumento = "15" Or strTipoDocumento = "16" Then
                    strCuerpo = "<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>" &
                                   "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "05" Then
                    strCuerpo = "<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG><br>" &
                                "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "09" Or strTipoDocumento = "17" Or strTipoDocumento = "18" Then
                    strCuerpo = dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "19" Then
                    strCuerpo = ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("DSC_MERC") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencial: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "20" Then
                    strCuerpo = dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                strCuerpo = "Estimados Se�ores:<br><br>" & strBody & strCuerpo
                strCuerpo += "Depositante: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NOM_ENTI") & "</FONT></STRONG>"
                strCuerpo += "<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� � Sistema AFI</A>"

                If strEmailsSinAdjunto <> "" Then
                    SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                                strBody & " - " & CStr(dtMail.Rows(0)("NOM_ENTI")).Trim,
                                strCuerpo, "")
                End If
                dtMail.Dispose()
                dtMail = Nothing
                Exit Sub
            End If

            If strBody = "" Then
                strBody = "Estimados Se�ores:<br><br>"
                If strTipoDocumento = "01" Or strTipoDocumento = "03" Or strTipoDocumento = "04" Or strTipoDocumento = "10" Then
                    strBody += dtMail.Rows(0)("DSC_SECU") & ":<br><br>Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "02" Or strTipoDocumento = "13" Or strTipoDocumento = "14" Or strTipoDocumento = "11" Or strTipoDocumento = "15" Or strTipoDocumento = "16" Then
                    strBody += dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>" &
                                   "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "05" Then
                    strBody += dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG><br>" &
                                "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "09" Or strTipoDocumento = "17" Or strTipoDocumento = "18" Then
                    strBody += dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "19" Then
                    strBody += ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("DSC_MERC") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencial: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "20" Then
                    strBody += dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                strBody += "Depositante: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NOM_ENTI") & "</FONT></STRONG>"
                strBody += "<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>"
            Else
                If strTipoDocumento = "01" Or strTipoDocumento = "03" Or strTipoDocumento = "04" Or strTipoDocumento = "10" Then
                    strCuerpo = dtMail.Rows(0)("DSC_SECU") & ":<br><br>Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "02" Or strTipoDocumento = "13" Or strTipoDocumento = "14" Or strTipoDocumento = "11" Or strTipoDocumento = "15" Or strTipoDocumento = "16" Then
                    strCuerpo = dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>" &
                                   "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "05" Then
                    strCuerpo = dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_LIBE") & "</FONT></STRONG><br>" &
                                "Warrant N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "09" Or strTipoDocumento = "17" Or strTipoDocumento = "18" Then
                    strCuerpo = dtMail.Rows(0)("DSC_SECU") & ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("MONEDA") & " " & String.Format("{0:##,##0.00}", dtMail.Rows(0)("MONTO")) & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "19" Then
                    strCuerpo = ":<br><br>" & dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("DSC_MERC") & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secuencial: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                If strTipoDocumento = "20" Then
                    strCuerpo = dtMail.Rows(0)("NOM_TIPDOC") & " N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NRO_DOCU") & "</FONT></STRONG><br>"
                End If
                strBody = "Estimados Se�ores:<br><br>" & strBody & strCuerpo
                strBody += "Depositante: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("NOM_ENTI") & "</FONT></STRONG>"
                strBody += "<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>"
            End If

            If strEmailsConAdjunto <> "" Then
                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                           CStr(dtMail.Rows(0)("DSC_SECU")).Trim & " - " & CStr(dtMail.Rows(0)("NOM_ENTI")).Trim,
                           strBody, strPath & strTipoDocumento & strNroDoc & strNroLib & ".pdf")
            End If

            If strEmailsSinAdjunto <> "" Then
                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                             CStr(dtMail.Rows(0)("DSC_SECU")).Trim & " - " & CStr(dtMail.Rows(0)("NOM_ENTI")).Trim,
                           strBody, "")
            End If
        End If
        dtMail.Dispose()
        dtMail = Nothing
    End Sub

    Private Sub GrabaDocumento(ByVal strNombArch As String, ByVal strNroDoc As String, ByVal strNroLib As String, ByVal strPeriodoAnual As String, ByVal strTipoDoc As String, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strNombArch)
        FilePath = strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegEndosoFolio(strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strNombre, Contenido, ObjTrans) 'ok(sin sessiones internas)
        fs.Close()
    End Sub

    Public Sub EnviaMailEmpresa(ByVal strIdEntidad As String, ByVal strIdEntidadFinanciador As String, ByVal strAsunto As String,
                                            ByVal strBody As String, Optional ByVal strNombrePDF As String = "", Optional ByVal strIdCliente As String = "")
        Dim dtMail As New DataTable
        Dim strEmails As String = ""
        Dim strEmailsConAdjunto As String = ""
        Dim strEmailsSinAdjunto As String = ""
        dtMail = objUsuario.gGetEmailAprobadores(strIdEntidad, strIdCliente, "01")
        If dtMail.Rows.Count > 0 Then
            If strNombrePDF = "" Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("EMAI") <> "" Then
                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
                SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                       strAsunto, strBody, "")
            Else
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("FLG_ADJU") = True Then
                        strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("EMAI") & ","
                    Else
                        strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
                If strEmailsConAdjunto <> "" Then
                    SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                               strAsunto, strBody, strNombrePDF)
                End If
                If strEmailsSinAdjunto <> "" Then
                    SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                                 strAsunto, strBody, "")
                End If
            End If
        End If
        If strIdEntidadFinanciador <> "" Then
            strEmails = ""
            strEmailsConAdjunto = ""
            strEmailsSinAdjunto = ""
            dtMail = objUsuario.gGetEmailAprobadores(strIdEntidadFinanciador, strIdEntidad, "01")
            If dtMail.Rows.Count > 0 Then
                If strNombrePDF = "" Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                    SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                           strAsunto, strBody, "")
                Else
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("FLG_ADJU") = True Then
                            strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("EMAI") & ","
                        Else
                            strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                    If strEmailsConAdjunto <> "" Then
                        SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                                   strAsunto, strBody, strNombrePDF)
                    End If
                    If strEmailsSinAdjunto <> "" Then
                        SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                                     strAsunto, strBody, "")
                    End If
                End If
            End If
        End If
        dtMail.Dispose()
        dtMail = Nothing
    End Sub

    Public Sub EnviaMailTraslado(ByVal strIdAGD As String, ByVal strIdDepositante As String, ByVal strIdFinanciador As String, ByVal strAsunto As String,
                                           ByVal strBody As String, Optional ByVal strNombrePDF As String = "")
        Dim dtMail As New DataTable
        Dim strEmails As String = ""
        dtMail = objUsuario.gGetEmailTraslado(strIdAGD, strIdDepositante, strIdFinanciador)
        If dtMail.Rows.Count > 0 Then
            For i As Integer = 0 To dtMail.Rows.Count - 1
                If dtMail.Rows(i)("EMAI") <> "" Then
                    strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                End If
            Next
            SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                   strAsunto, strBody, "")
        End If
        dtMail.Dispose()
        dtMail = Nothing
    End Sub

    Function ConvertirFechaFormatoServidorStr(ByVal strFecha As String) As String
        'la fecha llega siempre con formato dd/mm/aaaa
        Dim formatoServidor As String
        Dim fechaConvertida As String
        Dim dia, mes, anio As String
        dia = strFecha.Substring(0, 2)
        mes = strFecha.Substring(3, 2)
        anio = strFecha.Substring(6, 4)
        formatoServidor = System.Configuration.ConfigurationManager.AppSettings("FormatoFechaServidor")
        If formatoServidor = "dma" Then
            fechaConvertida = dia & "/" & mes & "/" & anio
        ElseIf formatoServidor = "dam" Then
            fechaConvertida = dia & "/" & anio & "/" & mes
        ElseIf formatoServidor = "mda" Then
            fechaConvertida = mes & "/" & dia & "/" & anio
        ElseIf formatoServidor = "mad" Then
            fechaConvertida = mes & "/" & anio & "/" & dia
        ElseIf formatoServidor = "adm" Then
            fechaConvertida = anio & "/" & dia & "/" & mes
        ElseIf formatoServidor = "amd" Then
            fechaConvertida = anio & "/" & mes & "/" & dia
        End If
        Return fechaConvertida
    End Function

    Dim cMontext As String

    Public Function NumPalabra(ByVal Numero As Double, ByVal Moneda As String) As String
        Dim Numerofmt As String
        Dim centenas As Integer
        Dim pos As Integer
        Dim cen As Integer
        Dim dec As Integer
        Dim uni As Integer
        Dim textuni As String
        Dim textdec As String
        Dim textcen As String
        Dim milestxt As String
        Dim monedatxt As String
        Dim txtPalabra As String

        Numerofmt = Format(Numero, "000000000000000.00") 'Le da un formato fijo
        centenas = 1
        pos = 1
        txtPalabra = ""
        Do While centenas <= 5
            ' extrae series de Centena, Decena, Unidad
            cen = Val(Mid(Numerofmt, pos, 1))
            dec = Val(Mid(Numerofmt, pos + 1, 1))
            uni = Val(Mid(Numerofmt, pos + 2, 1))
            pos = pos + 3
            textcen = Centena(uni, dec, cen)
            textdec = Decena(uni, dec)
            textuni = Unidad(uni, dec)
            ' determina separador de miles/millones
            Select Case centenas
                Case 1
                    If cen + dec + uni = 1 Then
                        milestxt = "Billon "
                    ElseIf cen + dec + uni > 1 Then
                        milestxt = "Billones "
                    End If
                Case 2
                    If cen + dec + uni >= 1 And Val(Mid(Numerofmt, 7, 3)) = 0 Then
                        milestxt = "Mil Millones "
                    ElseIf cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 3
                    If cen + dec = 0 And uni = 1 Then
                        milestxt = "Millon "
                    ElseIf cen > 0 Or dec > 0 Or uni > 1 Then
                        milestxt = "Millones "
                    End If
                Case 4
                    If cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 5
                    If cen + dec + uni >= 1 Then
                        milestxt = ""
                    End If
            End Select
            centenas = centenas + 1
            'va formando el texto del importe en palabras
            txtPalabra = txtPalabra + textcen + textdec + textuni + milestxt
            milestxt = ""
            textuni = ""
            textdec = ""
            textcen = ""
        Loop
        ' agrega denominacion de moneda
        Select Case Moneda
            Case "US$"
                monedatxt = "DOLARES AMERICANOS "
            Case "S/."
                monedatxt = "NUEVOS SOLES "
            Case "�"
                monedatxt = "EUROS "
            Case "DOL"
                monedatxt = "DOLARES AMERICANOS "
            Case "SOL"
                monedatxt = "NUEVO SOL "
            Case "EUR"
                monedatxt = "EURO "
        End Select
        txtPalabra = txtPalabra & "con " & Mid(Numerofmt, 17) & "/100 " & monedatxt
        Return txtPalabra.ToUpper
    End Function
    Private Function Centena(ByVal uni As Integer, ByVal dec As Integer,
        ByVal cen As Integer) As String
        Select Case cen
            Case 1
                If dec + uni = 0 Then
                    cMontext = "cien "
                Else
                    cMontext = "ciento "
                End If
            Case 2 : cMontext = "doscientos "
            Case 3 : cMontext = "trescientos "
            Case 4 : cMontext = "cuatrocientos "
            Case 5 : cMontext = "quinientos "
            Case 6 : cMontext = "seiscientos "
            Case 7 : cMontext = "setecientos "
            Case 8 : cMontext = "ochocientos "
            Case 9 : cMontext = "novecientos "
            Case Else : cMontext = ""
        End Select
        Centena = cMontext
        cMontext = ""
    End Function

    Private Function Decena(ByVal uni As Integer, ByVal dec As Integer) As String
        Select Case dec
            Case 1
                Select Case uni
                    Case 0 : cMontext = "diez "
                    Case 1 : cMontext = "once "
                    Case 2 : cMontext = "doce "
                    Case 3 : cMontext = "trece "
                    Case 4 : cMontext = "catorce "
                    Case 5 : cMontext = "quince "
                    Case 6 To 9 : cMontext = "dieci"
                End Select
            Case 2
                If uni = 0 Then
                    cMontext = "veinte "
                ElseIf uni > 0 Then
                    cMontext = "veinti"
                End If
            Case 3 : cMontext = "treinta "
            Case 4 : cMontext = "cuarenta "
            Case 5 : cMontext = "cincuenta "
            Case 6 : cMontext = "sesenta "
            Case 7 : cMontext = "setenta "
            Case 8 : cMontext = "ochenta "
            Case 9 : cMontext = "noventa "
            Case Else : cMontext = ""
        End Select
        If uni > 0 And dec > 2 Then cMontext = cMontext + "y "
        Decena = cMontext
        cMontext = ""
    End Function

    Private Function Unidad(ByVal uni As Integer, ByVal dec As Integer) As String
        If dec <> 1 Then
            Select Case uni
                Case 1 : cMontext = "un "
                Case 2 : cMontext = "dos "
                Case 3 : cMontext = "tres "
                Case 4 : cMontext = "cuatro "
                Case 5 : cMontext = "cinco "
            End Select
        End If
        Select Case uni
            Case 6 : cMontext = "seis "
            Case 7 : cMontext = "siete "
            Case 8 : cMontext = "ocho "
            Case 9 : cMontext = "nueve "
        End Select
        Unidad = cMontext
        cMontext = ""
    End Function

    Public Function InsertarSecuencia(ByVal objDatagrid As DataGrid, ByVal strAlmacen As String,
                                      ByVal strCliente As String, ByVal strIdUsuario As String) As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strCodUser As String
            Dim strCodSico As String
            Dim strCodTipEnti As String
            Dim strCorrelativo As String

            Dim blnDocTitu As Boolean
            Dim blnDocLibe As Boolean
            Dim blnDocSimp As Boolean
            Dim blnDocAdua As Boolean

            Dim dgItem As DataGridItem
            strCodUser = strAlmacen
            For i As Integer = 0 To objDatagrid.Items.Count - 1
                dgItem = objDatagrid.Items(i)
                If dgItem.Cells(9).Text() = "01" Then
                    strAlmacen = dgItem.Cells(0).Text()
                Else
                    strCodUser = dgItem.Cells(0).Text()
                End If
                strCodSico = dgItem.Cells(8).Text()
                strCodTipEnti = dgItem.Cells(9).Text()
                strCorrelativo = dgItem.Cells(10).Text()

                blnDocTitu = CType(dgItem.FindControl("chkTitulo"), CheckBox).Checked
                blnDocLibe = CType(dgItem.FindControl("chkLiberacion"), CheckBox).Checked
                blnDocSimp = CType(dgItem.FindControl("chkSimple"), CheckBox).Checked
                blnDocAdua = CType(dgItem.FindControl("chkAduana"), CheckBox).Checked

                objEntidad.gInsUsrxTipDoc(strCorrelativo, strCodUser, strCodSico, strCodTipEnti,
                strAlmacen, strCliente, strIdUsuario, blnDocTitu, blnDocLibe,
                blnDocSimp, blnDocAdua, objTrans)
            Next i
            objTrans.Commit()
            objTrans.Dispose()
            Return "Se actualiz� correctamente"
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Return "Se produjo el siguiente error: " & ex.Message
        End Try
    End Function

    Private Sub UpdDocumento(ByVal strNombArch As String, strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strTipoDoc As String, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strNombArch)
        FilePath = strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gUpdDocLib(strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strNombre, Contenido, ObjTrans)
        fs.Close()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objLiberacion Is Nothing) Then
            objLiberacion = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Public Function SendMail2(ByVal strCorreoDe As String, ByVal strCorreoPara As String,
                                ByVal strAsunto As String, ByVal strMensaje As String, ByVal strFile As String) As Boolean
        Try
            Dim mmCorreo As New MailMessage
            With mmCorreo
                Const ConfigNamespace As String = "http://schemas.microsoft.com/cdo/configuration/"
                Dim Flds As System.Collections.IDictionary
                Flds = mmCorreo.Fields

                With Flds
                    .Add(ConfigNamespace & "smtpserver", "smtp.office365.com")
                    .Add(ConfigNamespace & "smtpserverport", "587")
                    '.Add(ConfigNamespace & "sendusing", "2")
                    .Add(ConfigNamespace & "sendusername", "testdepsa2@ransa.net")
                    .Add(ConfigNamespace & "sendpassword", "Depsa20172")
                    .Add(ConfigNamespace & "smtpauthenticate", "2")
                    .Add(ConfigNamespace & "smtpusessl", "true")
                End With
                'Se Indica la Direcci�n de correo que envia
                .From = "testdepsa2@ransa.net"
                'Se Indica la Direcci�n de correo que recibira
                .To = strCorreoPara
                'Se Indica el Asunto del correo a enviar
                .Subject = strAsunto
                'El Mensaje del Correo
                .Body = strMensaje
                'establece el tipo de contenido del texto del mensaje de correo electr�nico.
                'MailFormat.Html o MailFormat.Text
                .BodyFormat = MailFormat.Html
                'Establece la prioridad del mensaje de correo electr�nico
                'MailPriority.High, MailPriority.Normal o MailPriority.Low
                .Priority = MailPriority.High
                'Establece el nombre del servidor de transmisi�n de correo SMTP 
                'que se va a utilizar para enviar los mensajes de correo electr�nico.mail.servidor.com 
                'SmtpMail.SmtpServer = "smtp.office365.com" '"System.Configuration.ConfigurationManager.AppSettings("SMTPServerName")

                'Env�a un mensaje de correo electr�nico utilizando argumentos 
                'suministrados en las propiedades de la clase MailMessage.
                If strFile <> "" Then
                    Dim idsunit As String() = Split(strFile, "-")
                    For i As Integer = 0 To idsunit.Length - 1
                        .Attachments.Add(New MailAttachment(idsunit(i)))
                    Next
                End If

                SmtpMail.Send(mmCorreo)
            End With
            'objDocumento.gEnvioCorreo(strCorreoDe, strCorreoPara, strAsunto, strMensaje, strFile)
            Return True
        Catch ex As Exception
            Dim errorMessage As String = ""
            While Not IsDBNull(ex)
                errorMessage += ex.Message + "\n"
                ex = ex.InnerException
            End While
            Return False
        End Try

        'Dim server As SmtpClient = New SmtpClient("ServerAddress")
        'server.Port = 587
        'server.EnableSsl = True
        'server.Credentials = New System.Net.NetworkCredential("username@mydomain.com", "password")
        'server.Timeout = 5000
        'server.UseDefaultCredentials = False

        'Dim mail As MailMessage = New MailMessage
        'mail.From = "recipent@anyaddress"
        'mail.To = "username@mydomain.com"
        'mail.Subject = "test out message sending"
        'mail.Body = "this is my message body"
        'mail.BodyFormat = MailFormat.Html

        'server.Send(mail)
    End Function

    Public ReadOnly Property FnSessResultado() As String
        Get
            Return strSessResultado
        End Get
    End Property

    Public ReadOnly Property FnSessMensaje() As String
        Get
            Return strSessMensaje
        End Get
    End Property

End Class