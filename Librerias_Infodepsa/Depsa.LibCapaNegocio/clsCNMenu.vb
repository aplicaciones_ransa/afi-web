Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports Depsa.LibCapaAccesoDatos

Public Class clsCNMenu
    Private strCadena As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionString")
    Private pstrco_sist As String
    Private pstrco_modu As String
    Private pstrco_menu As String
    Private pstrco_grup As String
    Private pstrst_adic As String
    Private pstrst_modi As String
    Private pstrst_anul As String
    Private pstrst_repo As String
    Private pstrco_usua_crea As String
    Private dtTemporal As DataTable
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvesistema() As String
        Return pstrco_sist
    End Function

    Public Function fn_devuelvemodulo() As String
        Return pstrco_modu
    End Function

    Public Function fn_devuelvemenu() As String
        Return pstrco_menu
    End Function

    Public Function fn_devuelvegrupo() As String
        Return pstrco_grup
    End Function

    Public Function fn_devuelveadicionar() As String
        Return pstrst_adic
    End Function

    Public Function fn_devuelvemodificar() As String
        Return pstrst_modi
    End Function

    Public Function fn_devuelveanular() As String
        Return pstrst_anul
    End Function

    Public Function fn_devuelvereporte() As String
        Return pstrst_repo
    End Function

    Public Function fn_devuelveerrorsql() As String
        Return pstrErrorSql
    End Function

    Public Function fn_devuelveFilasAfect() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveresultado() As String
        Return pstrResultado
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Sub gCNGrabarPlantillaMenu(ByVal objGrid As UI.WebControls.DataGrid, ByVal strco_sist As String, _
                                      ByVal strco_grup As String, ByVal strco_usua_crea As String, ByVal strti_sele As String)
        Dim dgItem As UI.WebControls.DataGridItem
        Dim objMenu As New clsCADMenu
        For Each dgItem In objGrid.Items
            With objMenu
                If CType(dgItem.FindControl("chkActivo"), UI.WebControls.CheckBox).Checked = True Then
                    .gCADInsertarMenuxGrupo(strco_sist, dgItem.Cells(3).Text, dgItem.Cells(5).Text, strco_grup, strco_usua_crea, strti_sele)
                ElseIf CType(dgItem.FindControl("chkActivo"), UI.WebControls.CheckBox).Checked = False Then
                    .gCADEliminarMenuxGrupo(strco_sist, dgItem.Cells(3).Text, dgItem.Cells(5).Text, strco_grup, strti_sele)
                End If
            End With
        Next
    End Sub

    Public Sub gCNTraerMenusxSistema(ByVal strco_sist As String, ByVal strco_grup As String, ByVal strti_sele As String)
        Dim objMenu As New clsCADMenu
        With objMenu
            objMenu.gCADTraerMenusxSistema(strco_sist, strco_grup, strti_sele)
            dtTemporal = .fn_DevolverDataTable
        End With
    End Sub
End Class
