Imports Depsa.LibCapaAccesoDatos
Public Class clsCNParametro
    Dim objParametro As clsCADParametro
    Private strCadena As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionString")
    Private pstrco_domin As String
    Private pstrno_domin As String
    Private pstrde_larg_domin As String
    Private pstrvalor1domin As String
    Private pstrvalor2domin As String
    Private pstrvalor3domin As String
    Private pstrvalor4domin As String
    Private dtTemporal As DataTable
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvecoddomin() As String
        Return pstrco_domin
    End Function

    Public Function fn_devuelvenombdomin() As String
        Return pstrno_domin
    End Function

    Public Function fn_devuelvedescdomin() As String
        Return pstrde_larg_domin
    End Function

    Public Function fn_devuelvevalor1domin() As String
        Return pstrvalor1domin
    End Function

    Public Function fn_devuelvevalor2domin() As String
        Return pstrvalor2domin
    End Function

    Public Function fn_devuelvevalor3domin() As String
        Return pstrvalor3domin
    End Function

    Public Function fn_devuelvevalor4domin() As String
        Return pstrvalor4domin
    End Function

    Public Function fn_devuelveDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Function fn_devuelveErrorSql() As String
        Return pstrErrorSql
    End Function

    Public Function fn_devuelveFilasAfect() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveResultado() As String
        Return pstrResultado
    End Function

    Public Sub gCNMostrarDominios(ByVal strti_sele As String, ByVal intco_domin As Integer)
        objParametro = New clsCADParametro
        With objParametro
            .gCADMostrarDominios(strti_sele, intco_domin)
            dtTemporal = .fn_devuelveDataTable
        End With
    End Sub

    Public Sub gCNValidarDominio(ByVal strno_domin As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADValidarDominio(strno_domin)
            dtTemporal = .fn_devuelveDataTable
        End With
    End Sub

    Public Sub gCNValidarParametro(ByVal strno_parm As String, ByVal strno_domin As Integer)
        objParametro = New clsCADParametro
        With objParametro
            .gCADValidarParametro(strno_parm, strno_domin)
            dtTemporal = .fn_devuelveDataTable
        End With
    End Sub

    Public Sub gCADMostrarParametros(ByVal intco_par As Integer, ByVal intco_dom As Integer, _
                                                    ByVal strti_sele As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADMostrarParametros(intco_par, intco_dom, strti_sele)
            dtTemporal = .fn_devuelveDataTable
        End With
    End Sub

    Public Sub gCNEliminarParametros(ByVal intco_par As Integer, ByVal intco_dom As Integer)
        objParametro = New clsCADParametro
        With objParametro
            .gCADEliminarParametros(intco_par, intco_dom)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Sub gCNInsertarParametros(ByVal intco_dom As Integer, ByVal strno_par As String, _
                                                    ByVal strde_larg As String, ByVal strvalor1 As String, _
                                                    ByVal strvalor2 As String, ByVal strvalor3 As String, _
                                                    ByVal strvalor4 As String, ByVal strvalor5 As String, _
                                                    ByVal strco_usua_crea As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADInsertarParametros(intco_dom, strno_par, strde_larg, strvalor1, strvalor2, strvalor3, strvalor2, strvalor3, strco_usua_crea)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Sub gCNActualizarParametro(ByVal intco_dom As Integer, ByVal intco_par As Integer, _
                                                    ByVal strno_par As String, ByVal strde_larg As String, _
                                                    ByVal strvalor1 As String, ByVal strvalor2 As String, _
                                                    ByVal strvalor3 As String, ByVal strvalor4 As String, _
                                                    ByVal strvalor5 As String, ByVal strco_usua_modi As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADActualizarParametro(intco_dom, intco_par, strno_par, strde_larg, strvalor1, strvalor2, strvalor3, strvalor4, strvalor5, strco_usua_modi)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Sub gCNEliminarDominio(ByVal intco_domin As Integer)
        objParametro = New clsCADParametro
        With objParametro
            .gCADEliminarDominio(intco_domin)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Sub gCNInsertarDominio(ByVal strno_domin As String, ByVal strde_larg As String, _
                                            ByVal strvalor1 As String, ByVal strvalor2 As String, _
                                            ByVal strvalor3 As String, ByVal strvalor4 As String, _
                                            ByVal strco_usua_crea As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADInsertarDominio(strno_domin, strde_larg, strvalor1, strvalor2, strvalor3, strvalor4, strco_usua_crea)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Sub gCNActualizarDominio(ByVal intco_domin As Integer, ByVal strno_domin As String, _
                                                ByVal strde_larg As String, ByVal strvalor1 As String, _
                                                ByVal strvalor2 As String, ByVal strvalor3 As String, _
                                                ByVal strvalor4 As String, ByVal strco_usua_modi As String)
        objParametro = New clsCADParametro
        With objParametro
            .gCADActualizarDominio(intco_domin, strno_domin, strde_larg, strvalor1, strvalor2, strvalor3, strvalor4, strco_usua_modi)
            pstrResultado = .fn_devuelveResultado
        End With
    End Sub

    Public Function gCNListarReferencia(ByVal cboReferencia As System.Web.UI.WebControls.DropDownList, ByVal strTipoFiltro As String)
        objParametro = New clsCADParametro
        Dim dtReferencia As DataTable
        cboReferencia.Items.Clear()
        dtReferencia = objParametro.gCADListarReferencia(strTipoFiltro)
        For Each dr As DataRow In dtReferencia.Rows
            cboReferencia.Items.Add(New System.Web.UI.WebControls.ListItem(dr("NO_PAR").ToString(), dr("CO_PAR")))
        Next
        dtReferencia.Dispose()
        dtReferencia = Nothing
    End Function

    Protected Overrides Sub Finalize()
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
