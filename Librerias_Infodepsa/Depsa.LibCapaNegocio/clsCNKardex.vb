Imports Depsa.LibCapaAccesoDatos
Public Class clsCNKardex
    Inherits System.Web.UI.Page
    Private pstrco_alma As String
    Private pstrti_docu_movi As String
    Private pstrnu_docu_movi As String
    Private pstrti_docu_rece As String
    Private pstrnu_docu_rece As String
    Private pintnu_secu_merc As Integer
    Private pintnu_secu_ubic As Integer
    Private pdtfe_movi As Date
    Private pdtfe_actu As Date
    Private pstrco_tipo_bult As String
    Private pdblnu_unid_ingr As Double
    Private pdblnu_unid_reto As Double
    Private pdblnu_unid_comp As Double
    Private pdblnu_unid_reco As Double
    Private pdblnu_unid_real As Double
    Private pdblnu_unid_sald As Double
    Private pstrco_unme_peso As String
    Private pdblnu_peso_ingr As Double
    Private pdblnu_peso_reto As Double
    Private pdblnu_peso_comp As Double
    Private pdblnu_peso_reco As Double
    Private pdblnu_peso_real As Double
    Private pdblnu_peso_sald As Double
    Private pstrco_mone As String
    Private pdblim_remo_ingr As Double
    Private pdblim_remo_reto As Double
    Private pdblim_remo_comp As Double
    Private pdblim_remo_reco As Double
    Private pdblim_remo_real As Double
    Private pdblim_remo_sald As Double
    Private pdblim_resi_ingr As Double
    Private pdblim_resi_reto As Double
    Private pdblim_resi_comp As Double
    Private pdblim_resi_reco As Double
    Private pdblim_resi_real As Double
    Private pdblim_resi_sald As Double
    Private pstrco_unme_area As String
    Private pdblnu_area_ingr As Double
    Private pdblnu_area_reto As Double
    Private pdblnu_area_comp As Double
    Private pdblnu_area_reco As Double
    Private pdblnu_area_real As Double
    Private pdblnu_area_sald As Double
    Private pdblco_unme_volu As String
    Private pdblnu_volu_ingr As Double
    Private pdblnu_volu_reto As Double
    Private pdblnu_volu_comp As Double
    Private pdblnu_volu_reco As Double
    Private pdblnu_volu_real As Double
    Private pdblnu_volu_sald As Double
    Private pstrco_clie_actu As String
    Private pstrco_clie_fact As String
    Private pstrco_tipo_merc As String
    Private pstrco_stip_merc As String
    Private pstrti_situ As String
    Private pstrnu_titu As String
    Private pbst_warr As Boolean
    Private pstrnu_mani As String
    Private pstrco_usua_crea As String
    Private pdtfe_usua_crea As Date
    Private pstrco_usua_modi As String
    Private pdtfe_usua_modi As Date
    Private pintnu_corr_warr As Integer
    Private pstrst_tipo_item As String
    Private dtTemporal As DataTable
    Private pstrErrorSql As String
    Private pintFilasAfectadas As Integer
    Private pintResultado As Integer
    Private dsTemporal As DataSet

    Public Function devolverco_alma() As String
        Return pstrco_alma
    End Function

    Public Function devolverpstrti_docu_movi() As String
        Return pstrti_docu_movi
    End Function

    Public Function devolverpstrnu_docu_movi() As String
        Return pstrnu_docu_movi
    End Function

    Public Function devolverpstrti_docu_rece() As String
        Return pstrti_docu_rece
    End Function

    Public Function devolverpstrnu_docu_rece() As String
        Return pstrnu_docu_rece
    End Function

    Public Function devolverpintnu_secu_merc() As String
        Return pintnu_secu_merc
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dttemporal
    End Function

    Public Function fn_devuelveDataSet() As DataSet
        Return dsTemporal
    End Function

    Public Function gCNListarKardex(ByVal strco_empr As String, ByVal strco_clie As String, _
                                    ByVal strti_moda As String, ByVal strflg_sald As String, _
                                    ByVal strfe_inic As String, ByVal strfe_fina As String, _
                                    ByVal strti_refe As String, ByVal strnu_refe As String, _
                                    ByVal blndor_pend As Boolean, ByVal strco_alma As String) As DataView
        Session.Add("dsKardex1", Nothing)
        Dim objKardex As New clsCADKardex
        With objKardex
            .gCADListarKardex(strco_empr, strco_clie, strti_moda, strflg_sald, strfe_inic, strfe_fina, strti_refe, _
             strnu_refe, blndor_pend, strco_alma)
            dsTemporal = .fn_DevolverDataSet
            Session.Item("dsKardex1") = dsTemporal
        End With

        Dim dvKardexCabe As DataView = getDataView0001(dsTemporal)
        Dim dvKardexTot As DataView = getDataView0002(dvKardexCabe, dsTemporal)
        Return dvKardexTot
    End Function

    Private Function getDataView0001(ByVal dsKardex As DataSet) As DataView
        Dim dtCol1 As New DataTable
        Dim dtrow1 As DataRow
        Dim dvKardex As DataView = New DataView
        Dim drvKardex As DataRowView
        dvKardex = dsKardex.Tables(0).DefaultView
        dtCol1.Columns.Add("TI_DOCU_RECE", GetType(System.String))
        dtCol1.Columns.Add("NU_DOCU_RECE", GetType(System.String))
        dtCol1.Columns.Add("DE_MODA_MOVI", GetType(System.String))
        dtCol1.Columns.Add("DE_MERC", GetType(System.String))
        dtCol1.Columns.Add("NU_SECU_MERC", GetType(System.String))
        dtCol1.Columns.Add("IM_UNIT", GetType(System.Double))
        dtCol1.Columns.Add("DE_MONE", GetType(System.String))
        dtCol1.Columns.Add("CO_PROD_CLIE", GetType(System.String))
        dtCol1.Columns.Add("NU_GUIA", GetType(System.String))
        dtCol1.Columns.Add("DE_ALMA", GetType(System.String))
        dtCol1.Columns.Add("NU_TITU", GetType(System.String))
        For Each drvKardex In dvKardex
            If drvKardex("NU_ORDE") = "1" Then
                dtrow1 = dtCol1.NewRow
                dtrow1("TI_DOCU_RECE") = drvKardex("TI_DOCU_RECE")
                dtrow1("NU_DOCU_RECE") = drvKardex("NU_DOCU_RECE")
                dtrow1("DE_MODA_MOVI") = drvKardex("DE_MODA_MOVI")
                dtrow1("DE_MERC") = drvKardex("DE_MERC")
                dtrow1("NU_SECU_MERC") = drvKardex("NU_SECU_MERC")
                dtrow1("IM_UNIT") = drvKardex("IM_UNIT")
                dtrow1("DE_MONE") = drvKardex("DE_MONE")
                dtrow1("CO_PROD_CLIE") = drvKardex("CO_PROD_CLIE")
                dtrow1("NU_GUIA") = drvKardex("NU_GUIA")
                dtrow1("DE_ALMA") = drvKardex("DE_ALMA")
                dtrow1("NU_TITU") = drvKardex("NU_TITU")
                dtCol1.Rows.Add(dtrow1)
            End If
        Next
        Return dtCol1.DefaultView
    End Function

    Private Function getDataView0002(ByVal dvKardexCabe As DataView, ByVal dsKardexDeta As DataSet) As DataView
        Dim drvTMSALD_ALMA As DataRowView
        Dim drvKardexCabe As DataRowView
        Dim drvKardexDeta As DataRowView
        Dim drNU_REGI As DataRow
        Dim decUnidades As Double
        Dim decBulto As Double
        Dim decImporte As Double
        Dim dvKardexDeta = dsKardexDeta.Tables(0).DefaultView
        Dim dtTMSALD_ALMA As New DataTable
        dtTMSALD_ALMA.Columns.Add("FE_MOVI", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_DOCU_MOVI", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_UNME_PESO", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_UNID_RECI", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("NU_UNID_RETI", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("NU_UNID_SALD", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("DE_TIPO_BULT", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_PESO_RECI", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("NU_PESO_RETI", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("NU_PESO_SALD", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("IM_REMO_INGR", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("IM_REMO_RETI", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("IM_REMO_SALD", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("TI_DOCU_RECE", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_DOCU_RECE", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_MODA_MOVI", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_MERC", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_SECU_MERC", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("IM_UNIT", GetType(System.Double))
        dtTMSALD_ALMA.Columns.Add("DE_MONE", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("CO_PROD_CLIE", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_OBSE", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("NU_GUIA", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_ALMA", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("ST_SALI_ALMA", GetType(System.String))
        dtTMSALD_ALMA.Columns.Add("DE_REFE_0002", GetType(System.String))

        For Each drvKardexCabe In dvKardexCabe
            drNU_REGI = dtTMSALD_ALMA.NewRow
            drNU_REGI("FE_MOVI") = "MODALIDAD: " & drvKardexCabe("DE_MODA_MOVI") & " - " & drvKardexCabe("NU_TITU") & "<BR> DESC.PRODUCTO: " & drvKardexCabe("DE_MERC")
            drNU_REGI("NU_DOCU_RECE") = drvKardexCabe("TI_DOCU_RECE") & ": " & drvKardexCabe("NU_DOCU_RECE") & " - " & _
            "ITEM: " & drvKardexCabe("NU_SECU_MERC") & "<BR> P.UNIT: " & drvKardexCabe("IM_UNIT") & " - " & "MON: " & drvKardexCabe("DE_MONE")
            drNU_REGI("DE_TIPO_BULT") = "COD.PRODUCTO: " & drvKardexCabe("CO_PROD_CLIE") & "<BR> NRO.GUIA.INGR: " & _
            drvKardexCabe("NU_GUIA") & " - " & "ALMACEN: " & drvKardexCabe("DE_ALMA")
            drNU_REGI("DE_UNME_PESO") = "0"
            dtTMSALD_ALMA.Rows.Add(drNU_REGI)
            dvKardexDeta.RowFilter = "NU_DOCU_RECE='" & drvKardexCabe("NU_DOCU_RECE") & "' and " & "NU_SECU_MERC='" & drvKardexCabe("NU_SECU_MERC") & "'"

            For Each drvKardexDeta In dvKardexDeta
                If drvKardexDeta("NU_ORDE") = "1" Then
                    decUnidades = drvKardexDeta("NU_UNID_SALD")
                    decBulto = drvKardexDeta("NU_PESO_SALD")
                    decImporte = drvKardexDeta("IM_REMO_SALD")
                Else
                    decUnidades = decUnidades - drvKardexDeta("NU_UNID_RETI")
                    decBulto = decBulto - drvKardexDeta("NU_PESO_RETI")
                    decImporte = decImporte - drvKardexDeta("IM_REMO_RETI")
                End If
                drNU_REGI = dtTMSALD_ALMA.NewRow
                drNU_REGI("FE_MOVI") = drvKardexDeta("FE_MOVI")
                drNU_REGI("NU_DOCU_RECE") = drvKardexDeta("NU_DOCU_MOVI")
                drNU_REGI("DE_TIPO_BULT") = drvKardexDeta("DE_TIPO_BULT")
                drNU_REGI("NU_UNID_RECI") = drvKardexDeta("NU_UNID_RECI")
                drNU_REGI("NU_UNID_RETI") = drvKardexDeta("NU_UNID_RETI")
                drNU_REGI("NU_UNID_SALD") = decUnidades
                drNU_REGI("DE_UNME_PESO") = drvKardexDeta("DE_UNME_PESO")
                drNU_REGI("NU_PESO_RECI") = drvKardexDeta("NU_PESO_RECI")
                drNU_REGI("NU_PESO_RETI") = drvKardexDeta("NU_PESO_RETI")
                drNU_REGI("NU_PESO_SALD") = decBulto
                drNU_REGI("IM_REMO_INGR") = drvKardexDeta("IM_REMO_INGR")
                drNU_REGI("IM_REMO_RETI") = drvKardexDeta("IM_REMO_RETI")
                drNU_REGI("IM_REMO_SALD") = decImporte
                drNU_REGI("NU_GUIA") = drvKardexDeta("NU_GUIA")
                drNU_REGI("DE_OBSE") = drvKardexDeta("DE_OBSE")
                drNU_REGI("ST_SALI_ALMA") = drvKardexDeta("ST_SALI_ALMA")
                drNU_REGI("DE_REFE_0002") = drvKardexDeta("DE_REFE_0002")
                dtTMSALD_ALMA.Rows.Add(drNU_REGI)
            Next
        Next
        Return dtTMSALD_ALMA.DefaultView
    End Function

    Public Function gCNListarKardexWMS(ByVal strCodigo As String, ByVal strCodprod As String, ByVal strDescMerc As String, ByVal strLote As String, ByVal strFechIni As String, ByVal strFechFin As String, ByVal strAlmacen As String, ByVal strIdSico As String) As DataTable
        strCodigo = strCodigo.Replace("'", "")
        strCodprod = strCodprod.Replace("'", "")
        strDescMerc = strDescMerc.Replace("'", "")
        Dim dt As DataTable
        Dim objKardex As New clsCADKardex
        With objKardex
            dt = .gCADListarKardexWMS(strCodigo, strCodprod, strDescMerc, strLote, strFechIni, strFechFin, strAlmacen, strIdSico)
            Session.Item("dtTemporal") = dt
        End With
        Return dt
    End Function
End Class
