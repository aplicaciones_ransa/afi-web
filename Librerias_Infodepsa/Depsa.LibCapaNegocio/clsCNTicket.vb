Imports Depsa.LibCapaAccesoDatos
Public Class clsCNTicket
    Dim objTicket As clsCADTicket

    '-----------------------------------------------------'
    '--Funcion           : fn_TCBALA_Sel_ReportePesadas --'
    '--Descripcion       : Reporte de Pesadas           --'
    '--Parametro Entrada :                              --'
    '--Desarrollado por  : Julio Francisco Salazar      --'
    '--Fecha Creacion    : 12/10/2007                   --'
    '-----------------------------------------------------'
    Public Function fn_TCBALA_Sel_ReportePesadas(ByVal sCO_EMPR As String, ByVal sCO_ENTI_TRAB As String, _
                                                ByVal sFE_EMIS_INIC As String, ByVal sFE_EMIS_FINA As String, _
                                                Optional ByVal sTipoTicket As String = "", Optional ByVal sReferencia As String = "", _
                                                Optional ByVal sNroReferencia As String = "", Optional ByVal sCodAlmacen As String = "", _
                                                Optional ByVal sModalidad As String = "") As DataTable

        objTicket = New clsCADTicket
        Return objTicket.fn_TCBALA_Sel_ReportePesadas(sCO_EMPR, sCO_ENTI_TRAB, sFE_EMIS_INIC, sFE_EMIS_FINA, _
                                                       sTipoTicket, sReferencia, sNroReferencia, sCodAlmacen, sModalidad)
    End Function

    Public Function gCNListar_Vencimientos(ByVal sCO_EMPR As String, ByVal sCO_ENTI_TRAB As String) As DataSet
        objTicket = New clsCADTicket
        Return objTicket.gCADListar_Vencimientos(sCO_EMPR, sCO_ENTI_TRAB)
    End Function

    Public Function fn_TCBALA_Sel_ReportePesadasPDF(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, ByVal sCO_ALMA As String, ByVal sTI_DOCU As String, ByVal sNU_DOCU As String, ByVal sTipoTicket As String) As DataTable
        objTicket = New clsCADTicket
        Return objTicket.fn_TCBALA_Sel_ReportePesadasPDF(sCO_EMPR, sCO_UNID, sCO_ALMA, sTI_DOCU, sNU_DOCU, sTipoTicket)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objTicket Is Nothing) Then
            objTicket = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
