Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports Depsa.LibCapaAccesoDatos
Public Class clsCNPerfil
    Private strCodGrupo As String
    Private strNombGrupo As String
    Private dtTemporal As DataTable
    Private strErrorSql As String
    Private intFilasAfectadas As Integer
    Private strResultado As String

    Public Function fn_DevolverCodGrupo() As String
        Return strCodGrupo
    End Function

    Public Function fn_DevolverNombGrupo() As String
        Return strNombGrupo
    End Function

    Public Function fn_DevolverDataTable() As DataTable
        Return dtTemporal
    End Function

    Public Function fn_DevolverResultado() As String
        Return strResultado
    End Function

    Public Sub gCNLlenarComboGrupos(ByVal strco_sist As String)
        Dim objPerfil As New clsCADPerfil
        With objPerfil
            .gCADLlenarComboGrupos(strco_sist)
            dtTemporal = .fn_DevolverDataTable
        End With
    End Sub

    Public Sub gCNLlenarSistemas()
        Dim objParametro As New clsCADParametro
        With objParametro
            dtTemporal = .gCADLlenarSistemas()
        End With
    End Sub
End Class
