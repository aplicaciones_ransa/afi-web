﻿Imports Depsa.LibCapaAccesoDatos
Imports System.Data
Public Class clsCNInventario
    Private dt As DataTable
    Private ds As DataSet
    Private objInventario As New clsCADInventario
    Private mstrErrorSql As String
    Private mintFilasAfectadas As Integer
    Private mstrResultado As String
    Private LstrNombrePDF As String
    Public ReadOnly Property strResultado() As String
        Get
            Return mstrResultado
        End Get
    End Property
    Public Function gCNGetListarClienteInventario(ByVal strcodAlma As String) As DataTable
        dt = objInventario.gCADGetListarClienteInventario(strcodAlma)
        mstrErrorSql = objInventario.strErrorSql
        mintFilasAfectadas = objInventario.intFilasAfectadas
        mstrResultado = objInventario.strResultado
        Return dt
    End Function


    Public Function gCNgetlistaralmacen(ByVal strcodcliente As String) As DataTable
        dt = objInventario.gCADGetListarAlmacen(strcodcliente)
        mstrErrorSql = objInventario.strErrorSql
        mintFilasAfectadas = objInventario.intFilasAfectadas
        mstrResultado = objInventario.strResultado
        Return dt
    End Function

    Public Function gCNListarInventarioXDocumento(ByVal CodDocumento As String) As DataTable
        Return objInventario.gCADListarInventarioXDocumento(CodDocumento)
    End Function

    Public Function gCNListarInventarioDetalleCardex(ByVal CodDocumento As String) As DataTable
        Return objInventario.gCADListarInventarioDetalleCardex(CodDocumento)
    End Function

    Public Function gCNGetbuscarDocInventario(ByVal FechaDesde As String, ByVal FechaHasta As String,
                                           ByVal vapor As String, ByVal Cliente As String,
                                           ByVal Almacen As String, ByVal Estado As String) As DataTable
        Return objInventario.gCADGetbuscarDocInventario(FechaDesde, FechaHasta, vapor, Cliente, Almacen, Estado)
        mstrErrorSql = objInventario.strErrorSql
        mintFilasAfectadas = objInventario.intFilasAfectadas
        mstrResultado = objInventario.strResultado
    End Function

    Public Function gCNListarUnidadMedida() As DataTable
        dt = objInventario.gCADListarUnidadMedida()
        mstrErrorSql = objInventario.strErrorSql
        mintFilasAfectadas = objInventario.intFilasAfectadas
        mstrResultado = objInventario.strResultado
        Return dt
    End Function

    Public Function gCNGetListarConductor(ByVal strCodAlmacen As String) As String
        Return objInventario.gCADGetListarConductor(strCodAlmacen)
        'mstrErrorSql = objReq.strErrorSql
        'mintFilasAfectadas = objReq.intFilasAfectadas
        'mstrResultado = objReq.strResultado
        'Return dt
    End Function

End Class
