Imports Depsa.LibCapaAccesoDatos
Imports System.Data
Public Class clsCNTraders
    Private dt As DataTable
    Private ds As DataSet
    Private objReq As New clsCADTraders
    Private mstrErrorSql As String
    Private mintFilasAfectadas As Integer
    Private mstrResultado As String
    Private LstrNombrePDF As String
    Public ReadOnly Property strResultado() As String
        Get
            Return mstrResultado
        End Get
    End Property
    Public Function gCNgetlistaralmacen(ByVal strcodcliente As String) As DataTable
        dt = objReq.gCADGetListarAlmacen(strcodcliente)
        mstrErrorSql = objReq.strErrorSql
        mintFilasAfectadas = objReq.intFilasAfectadas
        mstrResultado = objReq.strResultado
        Return dt
    End Function
    Public Function gCNGetListarClienteXConductor(ByVal strcodAlma As String) As DataTable
        dt = objReq.gCADGetListarClienteXConductor(strcodAlma)
        mstrErrorSql = objReq.strErrorSql
        mintFilasAfectadas = objReq.intFilasAfectadas
        mstrResultado = objReq.strResultado
        Return dt
    End Function
    Public Function gCNGetbuscarDocTraders(ByVal FechaDesde As String, ByVal FechaHasta As String,
                                            ByVal vapor As String, ByVal Cliente As String,
                                            ByVal Almacen As String, ByVal Estado As String, ByVal EstadoWarr As String) As DataTable
        Return objReq.gCADGetbuscarDocTrader(FechaDesde, FechaHasta, vapor, Cliente, Almacen, Estado, EstadoWarr)
        mstrErrorSql = objReq.strErrorSql
        mintFilasAfectadas = objReq.intFilasAfectadas
        mstrResultado = objReq.strResultado
    End Function
    Public Function gCNGetListarClienteTraders() As DataTable
        dt = objReq.gCADGetListarClienteTraders
        mstrErrorSql = objReq.strErrorSql
        mintFilasAfectadas = objReq.intFilasAfectadas
        mstrResultado = objReq.strResultado
        Return dt
    End Function
    Public Function gCNListarUnidadMedida() As DataTable
        dt = objReq.gCADListarUnidadMedida()
        mstrErrorSql = objReq.strErrorSql
        mintFilasAfectadas = objReq.intFilasAfectadas
        mstrResultado = objReq.strResultado
        Return dt
    End Function

    Public Function gCNListarTradersXDocumento(ByVal CodDocumento As String) As DataTable
        Return objReq.gCADListarTradersXDocumento(CodDocumento)
    End Function
    Public Function gCNListarTradersDetalleCardex(ByVal CodDocumento As String) As DataTable
        Return objReq.gCADListarTradersDetalleCardex(CodDocumento)
    End Function
    Public Function gCNGetListarConductor(ByVal strCodAlmacen As String) As String
        Return objReq.gCADGetListarConductor(strCodAlmacen)
    End Function
    Public Sub gCNFinalizarDetalleTraders(ByVal CodDocu As String, ByVal CodSecu As String, ByVal CodUser As String)
        objReq.gCADFinalizarDetalleTraders(CodDocu, CodSecu, CodUser)
    End Sub
End Class
