Imports Depsa.LibCapaAccesoDatos
Imports System.Data.SqlClient

Public Class clsCNWMS
    Private objWMS As clsCADWMS
    Public Function gCNGetInventario(ByVal strCodCliente As String, ByVal strCodProducto As String, _
                                    ByVal strDscProducto As String, ByVal strLote As String, _
                                    ByVal strFecVenc As String, ByVal strTipReporte As String) As DataTable
        objWMS = New clsCADWMS
        Return objWMS.gCADGetInventario(strCodCliente, strCodProducto, strDscProducto, strLote, strFecVenc, strTipReporte)
    End Function

    Public Function gCNGetPedidosPreparados(ByVal strNroInterno As String, ByVal strFechaInicio As String, _
                                        ByVal strFechaFin As String, ByVal strEstado As String, _
                                        ByVal strCodCliente As String, ByVal strPedido As String) As DataTable
        objWMS = New clsCADWMS
        Return objWMS.gCADGetPedidosPreparados(strNroInterno, strFechaInicio, strFechaFin, strEstado, strCodCliente, strPedido)
    End Function

    Public Function gGetExisteWMS(ByVal strCodCliente As String, ByVal strCodAlmacen As String) As String
        objWMS = New clsCADWMS
        Return objWMS.gGetExisteWMS(strCodCliente, strCodAlmacen)
    End Function

    Public Function gCNGetBuscarPedidos(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strTipoEntidad As String, ByVal strNroPedido As String, _
                                        ByVal strFecInicial As String, ByVal strFecFinal As String, ByVal strEstSitu As String, ByVal strIdEstado As String, ByVal strCodAlmacen As String) As DataTable
        objWMS = New clsCADWMS
        Return objWMS.gCADGetBuscarPedidos(strCodEmpresa, strCodCliente, strTipoEntidad, _
                                                strNroPedido, strFecInicial, strFecFinal, strEstSitu, strIdEstado, strCodAlmacen)
    End Function

    Public Function gCNGetBuscarMercaderia_WMS(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                            ByVal strCodAlmacen As String, ByVal strCodProducto As String, _
                                                            ByVal strLote As String, ByVal strReferencia As String, ByVal strNroReferencia As String, _
                                                            ByVal strFecVenc As String, ByVal strIdUsuario As String) As DataTable
        objWMS = New clsCADWMS
        Return objWMS.gCADGetBuscarMercaderia_WMS(strCodEmpresa, strCodCliente, strCodAlmacen, _
                                                strCodProducto, strLote, strReferencia, strNroReferencia, strFecVenc, _
                                                strIdUsuario)
    End Function

    Public Function gCNGetListarDetallePedido(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strCodAlma As String, _
                                                  ByVal strNroPedido As String) As DataTable
        objWMS = New clsCADWMS
        Return objWMS.gCADGetListarDetallePedido(strCodEmpresa, strCodCliente, strCodAlma, strNroPedido)
    End Function

    Public Function gCNGenNumRetiro(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                    ByVal pstrCodUsuario As String, ByVal pstrCliente As String, _
                                    ByVal pstrFechaRecojo As String, ByVal pstrNPedido As String, _
                                    ByVal pstrRetiradoX As String, ByVal Co_AlmaW As String, _
                                    ByVal pstrTipUsua As String, ByVal objTransaction As SqlTransaction) As String
        objWMS = New clsCADWMS
        Try
            Return objWMS.gCADAddCABRetiroWMS(pstrCodEmpresa, pstrCodCliente, pstrCodUsuario, pstrCliente, _
                            pstrFechaRecojo, pstrNPedido, pstrRetiradoX, Co_AlmaW, pstrTipUsua, objTransaction)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return -1
        End Try
    End Function

    Public Function gCNAddItemRetiroWMS(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                        ByVal pstrCodUsuario As String, ByVal Co_AlmaW As String, _
                                        ByVal dtMercaderia As DataTable, ByVal strNroRetiro As String, _
                                        ByVal objTransaction As SqlTransaction) As Boolean
        objWMS = New clsCADWMS
        Dim dr As DataRow
        With objWMS
            Try
                For intI As Integer = 0 To dtMercaderia.Rows.Count - 1
                    dr = dtMercaderia.Rows(intI)
                    If .gCADAddDETRetiroWMS(pstrCodEmpresa, pstrCodCliente, pstrCodUsuario, strNroRetiro, dr("CO_PROD_CLIE"), dr("DSC_PROD"), dr("LOTE"), _
                        dr("FCH_VENC"), dr("COD_RETE"), dr("UNI_MEDI"), dr("NU_UNID_SALD"), dr("NU_UNID_RETI"), Co_AlmaW, objTransaction) <> "" Then
                        Return False
                        Exit For
                    End If
                Next
                Return True
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return False
            End Try
        End With
    End Function

    Public Function gCNAddItemIngresoWMS(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                       ByVal strCodUsuario As String, ByVal strCod_Almacen As String, _
                                       ByVal dtMercaderia As DataTable, ByVal strNroGuia As String, _
                                       ByVal objTransaction As SqlTransaction) As Boolean
        objWMS = New clsCADWMS
        Dim dr As DataRow
        With objWMS
            Try
                For intI As Integer = 0 To dtMercaderia.Rows.Count - 1
                    dr = dtMercaderia.Rows(intI)
                    If .gCADAddIngresoWMS(strCodEmpresa, strCodCliente, strCodUsuario, strNroGuia, dr("CO_PROD"), dr("DE_PROD"), dr("NU_CANT"), _
                        dr("NU_LOTE"), strCod_Almacen, objTransaction) <> "" Then
                        Return False
                        Exit For
                    End If
                Next
                Return True
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return False
            End Try
        End With
    End Function

    Public Function gCNGENE_SALI_INFO_WMS(ByVal strNroRetiro As String, ByVal objTransaction As SqlTransaction) As DataTable
        objWMS = New clsCADWMS
        Try
            Return objWMS.gCADGENE_SALI_INFO_WMS(strNroRetiro, objTransaction)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function gCNValidarDeuda(ByVal strNroRetiro As String, ByVal objTransaction As SqlTransaction) As String
        objWMS = New clsCADWMS
        Try
            Return objWMS.gCADValidarDeuda(strNroRetiro, objTransaction)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function gCNCrearPedido(ByVal strNu_Docu_Pedi As String) As String
        objWMS = New clsCADWMS
        Return objWMS.gCADCrearPedido(strNu_Docu_Pedi)
    End Function

    Public Function gCNEliminarPedidoWMS(ByVal nropedido As String, ByVal sCodUsuario As String) As String
        objWMS = New clsCADWMS
        Return objWMS.gCADEliminarPedidoWMS(nropedido, sCodUsuario)
    End Function

    Public Function gCNACTNumRetiro(ByVal pstrCodUsuario As String, ByVal pNumSecu As Integer, ByVal NroPedido As String, ByVal objTransaction As SqlTransaction) As String
        objWMS = New clsCADWMS
        Dim dr As DataRow
        Dim strMensaje As String
        With objWMS
            Try
                strMensaje = .gCADActCABRetiroWMS(pstrCodUsuario, pNumSecu, NroPedido, objTransaction)
                Return strMensaje
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return "-1"
            End Try
        End With
    End Function

    Public Function gCNValidaProducto(ByVal CodCliente As String, ByVal CodProducto As String) As String
        objWMS = New clsCADWMS
        Return objWMS.gCADValidaProducto(CodCliente, CodProducto)
    End Function
End Class
