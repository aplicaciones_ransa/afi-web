Imports Depsa.LibCapaAccesoDatos
Public Class clsCNWarrant
    Private objWarrant As clsCADWarrant

    Public Function gCNGetListarFinanciadores(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                            ByVal strTipoEntidad As String) As DataTable
        objWarrant = New clsCADWarrant
        Return objWarrant.gCADGetListarFinanciadores(strCodEmpresa, strCodCliente, strTipoEntidad)
    End Function

    Public Function gCNGetListarTipoWarrant(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                            ByVal strTipoEntidad As String, ByVal strFlgFinan As String) As DataTable
        objWarrant = New clsCADWarrant
        Return objWarrant.gCADGetListarTipoWarrant(strCodEmpresa, strCodCliente, strTipoEntidad, strFlgFinan)
    End Function

    Public Function gCNGetBuscarWarrant(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                        ByVal strEstado As String, ByVal strCodFina As String, _
                                        ByVal strTipWar As String, ByVal strNroWar As String, _
                                        ByVal strTipoEntidad As String) As DataTable
        objWarrant = New clsCADWarrant
        Return objWarrant.gCADGetBuscarWarrant(strCodEmpresa, strCodCliente, strEstado, strCodFina, strTipWar, strNroWar, strTipoEntidad)
    End Function

    Public Function gCNGetListarRetiros(ByVal strCodEmpresa As String, ByVal strTipWar As String, _
                                        ByVal strNroWar As String) As DataTable
        objWarrant = New clsCADWarrant
        Return objWarrant.gCADGetListarRetiros(strCodEmpresa, strTipWar, strNroWar)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
