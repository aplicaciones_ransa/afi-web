Imports Depsa.LibCapaAccesoDatos
Public Class clsCNMapa
    Dim objMapa As clsCADMapa

    Public Function GetAlmacenes(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                    ByVal strCodCliente As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetAlmacenes(strCodEmpresa, strCodUnidad, strCodCliente)
    End Function

    Public Function GetModalidades(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetModalidades(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen)
    End Function

    Public Function GetModelos(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                        ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetModelos(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen)
    End Function

    Public Function GetColores(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                        ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetColores(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen)
    End Function

    Public Function GetBodegas(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                            ByVal strCodCliente As String, ByVal strCodAlmacen As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetBodegas(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen)
    End Function

    Public Function GetUbicaciones(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                   ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                   ByVal strCodBodega As String, ByVal strModalidad As String, _
                                   ByVal strModelo As String, ByVal strColor As String, _
                                   ByVal strNroChasis As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.GetUbicaciones(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen, _
                                        strCodBodega, strModalidad, strModelo, strColor, strNroChasis)
    End Function

    Public Function GetTotal(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                   ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                   ByVal strCodBodega As String, ByVal strModalidad As String, _
                                   ByVal strModelo As String, ByVal strColor As String, _
                                   ByVal strNroChasis As String) As String
        objMapa = New clsCADMapa
        Return objMapa.GetTotal(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen, _
                                strCodBodega, strModalidad, strModelo, strColor, strNroChasis)
    End Function

    Public Function gGetVehiculosxUbicacion(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                        ByVal strCodCliente As String, ByVal strCodAlmacen As String, _
                                                        ByVal strCodBodega As String, ByVal strCodUbicacion As String, _
                                                        ByVal strModalidad As String, ByVal strModelo As String, _
                                                        ByVal strColor As String, ByVal strNroChasis As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.gGetVehiculosxUbicacion(strCodEmpresa, strCodUnidad, strCodCliente, strCodAlmacen, _
                                                strCodBodega, strCodUbicacion, strModalidad, strModelo, _
                                                strColor, strNroChasis)
    End Function

    Public Function gGetUbicacionesVehiculo(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                            ByVal strTipDocu As String, ByVal strNroDocu As String, _
                                                            ByVal strNroSecu As String) As DataTable
        objMapa = New clsCADMapa
        Return objMapa.gGetUbicacionesVehiculo(strCodEmpresa, strCodUnidad, strTipDocu, strNroDocu, strNroSecu)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objMapa Is Nothing) Then
            objMapa = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
