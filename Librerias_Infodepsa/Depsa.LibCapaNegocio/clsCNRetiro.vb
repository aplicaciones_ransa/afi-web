Imports System.Data.SqlClient
Imports Depsa.LibCapaAccesoDatos
Imports Root.Reports
Imports System.IO
Imports System.Web.UI.WebControls
Imports Library.AccesoDB
Public Class clsCNRetiro
    Private objRetiro As clsCADRetiro
    Private cnn As New SqlConnection
    Private objSICO As SICO = New SICO
    Dim sNU_DOCU_RECE = ""
    Public strCodAlmacen As String
    Private lstrMensajeError As String

    Public Function gCNGetBuscarRetiros(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                        ByVal strTipoEntidad As String, ByVal strNroPedido As String, _
                                        ByVal strFecInicial As String, ByVal strFecFinal As String, _
                                        ByVal strTipoRetiro As String, ByVal strIdEstado As String, _
                                        ByVal strCodAlmacen As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetBuscarRetiros(strCodEmpresa, strCodCliente, strTipoEntidad, _
                                                strNroPedido, strFecInicial, strFecFinal, _
                                                strTipoRetiro, strIdEstado, strCodAlmacen)
    End Function

    Public Function gCNGetListarAlmacenesWMS(ByVal strCodCliente As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetListarAlmacenesWMS(strCodCliente)
    End Function

    Public Function gCNGetBuscarRetirosIngresadosWMS(ByVal strCodCliente As String, ByVal strNroGuia As String, _
                                                    ByVal strFecInicial As String, ByVal strFecFinal As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetBuscarRetirosIngresadosWMS(strCodCliente, strNroGuia, strFecInicial, strFecFinal)
    End Function

    Public Function gCNGetBuscarDetaRetirosIngresadosWMS(ByVal strCodCliente As String, ByVal strNroGuia As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetBuscarDetaRetirosIngresadosWMS(strCodCliente, strNroGuia)
    End Function

    Public Function gCNSetEnviaIngresoWMS(ByVal strCodEmpresa As String, ByVal strNoArchSali As String, _
                                            ByVal strNuRefe As String, ByVal strCoClie As String, _
                                            ByVal strCoAlma As String) As DataSet
        objRetiro = New clsCADRetiro
        Dim ds As DataSet
        ds = objRetiro.gCADSetEnviaIngresoWMS(strCodEmpresa, strNoArchSali, strNuRefe, strCoClie, strCoAlma)
        lstrMensajeError = objRetiro.strMensajeError
        Return ds
    End Function

    Public Function gCNGetBuscarMercaderia(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                            ByVal strCodAlmacen As String, ByVal strReferencia As String, _
                                                            ByVal strNroReferencia As String, ByVal strFecVenc As String, _
                                                            ByVal strIdUsuario As String, ByVal strTipoRetiro As String, ByVal strEstado As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetBuscarMercaderia(strCodEmpresa, strCodCliente, strCodAlmacen, _
                                                strReferencia, strNroReferencia, strFecVenc, _
                                                strIdUsuario, strTipoRetiro, strEstado)
    End Function

    Public Function gCNGetBuscarTranp(ByVal strNroBrevete As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetBuscarTransp(strNroBrevete)
    End Function

    Public Function gCNGetListarAlmacenes(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strTipoMercaderia As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetListarAlmacenes(strCodEmpresa, strCodCliente, strTipoMercaderia)
    End Function

    Public Function gCNGetPredespacho(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, _
                                                    ByVal strCodAlmacen As String, ByVal strCodCliente As String) As Integer
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADGetPredespacho(strCodEmpresa, strCodUnidad, strCodAlmacen, strCodCliente)
    End Function

    Public Function gCNAddItemRetiro(ByVal pstrCodEmpresa As String, ByVal pstrCodCliente As String, _
                                                    ByVal pstrCodUsuario As String, ByVal dtMercaderia As DataTable, _
                                                    ByVal objTransaction As SqlTransaction) As Boolean
        objRetiro = New clsCADRetiro
        Dim dr As DataRow
        With objRetiro
            Try
                .gCADDelItemRetiro(pstrCodCliente, objTransaction)
                For intI As Integer = 0 To dtMercaderia.Rows.Count - 1
                    dr = dtMercaderia.Rows(intI)
                    If objSICO.fn_TCALMA_MERC_Q07(pstrCodEmpresa, dr("CO_UNID"), _
                                                              dr("TI_DOCU_RECE"), dr("NU_DOCU_RECE"), dr("NU_SECU"), _
                                                              CInt(dr("NU_SECU_UBIC")), "N", dr("NU_UNID_RETI"), dr("NU_PESO_RETI")) = True Then
                        If .gCADAddItemRetiro(pstrCodEmpresa, pstrCodCliente, pstrCodUsuario, dr("NU_DOCU_RECE"), dr("NU_SECU"), dr("DE_MERC"), _
                            dr("CO_PROD_CLIE"), dr("LOTE"), dr("FE_VCTO_MERC"), dr("CO_TIPO_BULT"), dr("NU_UNID_SALD"), dr("NU_UNID_RETI"), _
                            dr("ADUANERO"), dr("CO_ALMA_ACTU"), dr("CO_UNID"), dr("FLG_SEL"), dr("CO_BODE_ACTU"), dr("CO_UBIC_ACTU"), _
                            dr("DE_TIPO_MERC"), dr("DE_STIP_MERC"), dr("TI_DOCU_RECE"), dr("FE_REGI_INGR"), dr("NU_SECU_UBIC"), dr("CO_TIPO_MERC"), _
                            dr("CO_STIP_MERC"), dr("DE_TIPO_BULT"), dr("CO_UNME_PESO"), dr("DE_UNME"), dr("NU_PESO_RETI"), dr("NU_UNID_PESO"), _
                            dr("CO_MONE"), dr("TI_TITU"), dr("NU_TITU"), dr("CO_UNME_AREA"), dr("NU_AREA_USAD"), dr("CO_UNME_VOLU"), _
                            dr("NU_VOLU_USAD"), dr("ST_VALI_RETI"), dr("NU_UNID_RECI"), dr("NU_PESO_RECI"), dr("NU_PESO_UNID"), dr("TI_UNID_PESO"), _
                            dr("NU_MANI"), dr("ST_TEAL"), objTransaction) = False Then
                            Return False
                            Exit For
                        End If
                    Else
                        Return False
                        Exit For
                    End If
                Next
                Return True
            Catch ex As Exception
                Throw New Exception(ex.Message)
                Return False
            End Try
        End With
    End Function

    Public Function gCNUpdRetiro(ByVal pstrCodEmpresa As String, ByVal pstrCodUnidad As String, ByVal strCodUsuario As String, _
                                 ByVal strTipoDocRece As String, ByVal sRetiro As String, _
                                 ByVal objTransaction As SqlTransaction) As String
        objRetiro = New clsCADRetiro
        Return objRetiro.gCNUpdRetiro(pstrCodEmpresa, pstrCodUnidad, strCodUsuario, strTipoDocRece, sRetiro, objTransaction)
    End Function

    Public Function gCNCrearRetiroAprobado(ByVal strCodEmpresa As String, ByVal strCodUnid As String, _
                                        ByVal strTiDocuReti As String, ByVal strNu_Docu_Reti As String) As DataTable
        objRetiro = New clsCADRetiro
        Return objRetiro.gCNCrearRetiroAprobado(strCodEmpresa, strCodUnid, strTiDocuReti, strNu_Docu_Reti)
    End Function
    'MODIFICADO
    Public Function gCNCrearRetiro(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                            ByVal strCodUsuario As String, ByVal strBrevete As String, _
                                            ByVal strChofer As String, ByVal strCiaTransporte As String, _
                                            ByVal strDNI As String, ByVal strPlaca As String, _
                                            ByVal blnPreDespacho As Boolean, ByVal blnPagoElectronico As Boolean, _
                                            ByVal strDestino As String, ByVal objTransaction As SqlTransaction, ByVal sST_VALI As String) As String
        objRetiro = New clsCADRetiro
        Return objRetiro.gCADCrearRetiro(strCodEmpresa, strCodCliente, strCodUsuario, strBrevete, strChofer, strCiaTransporte, strDNI, strPlaca, blnPreDespacho, blnPagoElectronico, strDestino, objTransaction, sST_VALI)
    End Function

    Public Function fn_TDRETI_MERC_Q05(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                                        ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String) As String
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = "SELECT ISNULL(MAX(T2.CO_MODA_MOVI), '') AS CODIGO_MODALIDAD " &
                    " FROM TDRETI_MERC T1 " &
                    " INNER JOIN TCALMA_MERC T2 ON " &
                    " T1.CO_EMPR = T2.CO_EMPR " &
                    " AND T1.CO_UNID = T2.CO_UNID " &
                    " AND T1.TI_DOCU_RECE = T2.TI_DOCU_RECE " &
                    " AND T1.NU_DOCU_RECE = T2.NU_DOCU_RECE " &
                    " INNER JOIN TTMODA_MOVI T3 ON " &
                    " T2.CO_EMPR = T3.CO_EMPR " &
                    " AND T2.CO_MODA_MOVI = T3.CO_MODA_MOVI " &
                    " WHERE " &
                    " T1.CO_EMPR = '" & sCO_EMPR & "' " &
                    " AND T1.CO_UNID = '" & sCO_UNID & "' " &
                    " AND T1.TI_DOCU_RETI = '" & sTI_DOCU_RETI & "' " &
                    " AND T1.NU_DOCU_RETI = '" & sNU_DOCU_RETI & "' " &
                    " AND T1.TI_SITU = 'ACT' " &
                    " AND T2.TI_SITU = 'ACT' " &
                    " AND T3.ST_VALI_ADUA = 'S' " &
                    " AND T3.ST_VALI_FINA = 'S' "

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        Dim sCO_MODA_MOVI As String = cmTDRETI_MERC.ExecuteScalar
        cnn.Close()
        Return sCO_MODA_MOVI
    End Function

    Private Function fn_TDRETI_MERC_Q02(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
                                                        ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String) As DataTable
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sSQL As String
        sSQL = "Select TI_DIRE_FISC From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "' "
        Dim cmTDRETI_MERC As New SqlCommand(sSQL, cnn)
        cmTDRETI_MERC.CommandTimeout = 0
        Dim sTI_DIRE_FISC As String = cmTDRETI_MERC.ExecuteScalar

        sSQL = "SELECT MAX(T4.DE_MODA_MOVI) 'MODALIDAD', " &
                " MAX(T2.CO_CLIE_ACTU) 'COD CLIENTE', " &
                " NULL, " &
                " MAX(T9.NU_RUCS_0001) 'RUC', " &
                " NULL, " &
                " MAX(T1.NU_DOCU_RETI) 'NUMERO O/R', " &
                " CONVERT(VARCHAR(10),MAX(T5.FE_USUA_CREA),103) + ' ' + CONVERT(VARCHAR(5),MAX(T5.FE_USUA_CREA),108)'FECHA O/R', " &
                " MAX(T2.TI_TITU) 'TIPO WARRANT', " &
                " MAX(T2.NU_TITU) 'NUMERO WARRANT', " &
                " MAX(T1.NU_DOCU_RECE) 'NUMERO COMPROBANTE', " &
                " SUBSTRING(MAX(T6.DE_DIRE_ALMA),1,80) 'DIRECCION DEL ALMACEN', " &
                " (SELECT SUBSTRING(DE_LARG_ENFI,1,50) FROM TMENTI_FINA WHERE CO_ENTI_FINA = MAX(T3.CO_ENTI_FINA))'NOMBRE FINANCIADOR', " &
                " MAX(CASE WHEN T5.TI_RETI_GIRA = 'T' THEN 'TOTAL' ELSE 'PARCIAL' END) 'TIPO RETIRO', " &
                " MAX(ISNULL(T5.DE_OBSE_0001,'') + ' ' + ISNULL(T5.DE_OBSE_0002,'')) AS DE_OBSE, " &
                " UPPER(MAX(T5.NO_CHOF)) AS NO_CHOF, " &
                " MAX(T5.NU_DNNI) AS NU_DNNI, " &
                " MAX(T5.NU_PLAC) AS NU_PLAC, " &
                " MAX(T5.NU_BREV_CHOF) AS NU_BREV_CHOF, " &
                " MAX(T7.NU_DUAS) AS PEDIDO_DEPOSITO, " &
                " MAX(T7.NU_CONO_EMBA) AS CONOCIMIENTO_EMBARQUE, " &
                " MAX(T5.NU_POLI_ADUA) 'P�LIZA', " &
                " CONVERT(VARCHAR(10),MAX(T5.FE_POLI_ADUA),103) 'FECHA NUMERACI�N', " &
                " MAX(T5.NU_COMP_PAGO) 'COMPROBANTE PAGO', " &
                " MAX(T5.CO_ENTI_AGAD) 'COD. AG. ADUANA', " &
                " MAX(T7.NU_MANI) 'N�MERO MANIFIESTO', " &
                " MAX(T7.NO_VAPO) 'NOMBRE VAPOR', " &
                " MAX(T5.IM_ADUA_SIST) 'IMPORTE MONEDA NACIONAL', " &
                " MAX(T5.FA_CAMB_ADUA) 'FACTOR CAMBIO', " &
                " CONVERT(VARCHAR(10),MAX(T5.FE_CANC),103) 'FECHA CANCELACI�N', " &
                " MAX(T7.NU_PEDI_DEPO) AS CERTIFICADO_ADUANERO " &
                " FROM TDRETI_MERC T1 INNER JOIN TCRETI_MERC T5 ON " &
                " T5.CO_EMPR = T1.CO_EMPR " &
                " AND T5.CO_UNID = T1.CO_UNID " &
                " AND T5.TI_DOCU_RETI = T1.TI_DOCU_RETI " &
                " AND T5.NU_DOCU_RETI = T1.NU_DOCU_RETI " &
                " INNER JOIN  TCALMA_MERC T2 ON " &
                " T2.CO_EMPR = T1.CO_EMPR " &
                " AND T2.CO_UNID = T1.CO_UNID " &
                " AND T2.TI_DOCU_RECE = T1.TI_DOCU_RECE " &
                " AND T2.NU_DOCU_RECE = T1.NU_DOCU_RECE " &
                " LEFT JOIN TCTITU T3 " &
                " ON  T3.CO_EMPR = T2.CO_EMPR " &
                " AND T3.TI_TITU = T2.TI_TITU " &
                " AND T3.NU_TITU = T2.NU_TITU " &
                " LEFT JOIN TTMODA_MOVI T4 " &
                " ON T4.CO_EMPR = T2.CO_EMPR " &
                " AND T4.CO_MODA_MOVI = T2.CO_MODA_MOVI " &
                " LEFT JOIN TMALMA T6 " &
                " ON T6.CO_EMPR = T5.CO_EMPR " &
                " AND T6.CO_ALMA = T5.CO_ALMA " &
                " LEFT JOIN TMCLIE T9 " &
                " ON T9.CO_CLIE = T2.CO_CLIE_ACTU " &
                " LEFT JOIN TCALME_ADUA T7 " &
                " ON T7.CO_EMPR = T2.CO_EMPR " &
                " AND T7.CO_UNID = T2.CO_UNID " &
                " AND T7.TI_DOCU_RECE = T2.TI_DOCU_RECE " &
                " AND T7.NU_DOCU_RECE = T2.NU_DOCU_RECE " &
                " WHERE " &
                " T1.CO_EMPR = '" & sCO_EMPR & "' " &
                " AND T1.CO_UNID = '" & sCO_UNID & "' " &
                " AND T1.TI_DOCU_RETI =  '" & sTI_DOCU_RECE & "' " &
                " AND T1.NU_DOCU_RETI = '" & sNU_DOCU_RECE & "' " &
                " AND T1.TI_SITU = 'ACT' "

        cmTDRETI_MERC = New SqlCommand(sSQL, cnn)
        cmTDRETI_MERC.CommandTimeout = 0
        Dim daTDRETI_MERC As New SqlDataAdapter
        daTDRETI_MERC.SelectCommand = cmTDRETI_MERC
        Dim dsTDRETI_MERC As New DataSet
        daTDRETI_MERC.Fill(dsTDRETI_MERC)
        cnn.Close()
        Return dsTDRETI_MERC.Tables(0)
    End Function

    Private Function fn_TRMODA_CLIE(ByVal sCO_EMPR As String, ByVal sCO_MODA As String, ByVal sCO_CLIE As String) As Integer
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = "Select count(CO_EMPR) From TRMODA_CLIE Where CO_EMPR='" & sCO_EMPR & "' AND CO_MODA_MOVI= '" & sCO_MODA & "' AND CO_CLIE= '" & sCO_CLIE & "' AND TI_SITU='ACT' AND TI_DOCU = 'DOR'"

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        Dim intSinImporte As Integer = cmTDRETI_MERC.ExecuteScalar
        cnn.Close()
        Return intSinImporte
    End Function

    Public Function fn_TDRETI_MERC_Q06(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
                                                        ByVal sTI_DOCU_RETI As String, ByVal sNU_DOCU_RETI As String) As DataSet
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If
        Dim sDE_SELE As String
        sDE_SELE = " Select Max(t3.TI_DOCU_RECE) as TI_DOCU_RECE, "
        sDE_SELE = sDE_SELE & " Max(t3.NU_DOCU_RECE) as NU_DOCU_RECE, "
        sDE_SELE = sDE_SELE & " Convert(varchar, Max(t3.FE_FACT), 101) as FE_FACT, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_CLIE_ACTU) as CO_CLIE_ACTU, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_TARI) as CO_TARI, "
        sDE_SELE = sDE_SELE & " Max(t3.CO_ALMA) as CO_ALMA "
        sDE_SELE = sDE_SELE & " From TDRETI_MERC t1 INNER JOIN TCALMA_MERC t3 "
        sDE_SELE = sDE_SELE & " ON t3.CO_EMPR = t1.CO_EMPR "
        sDE_SELE = sDE_SELE & " And t3.CO_UNID = t1.CO_UNID "
        sDE_SELE = sDE_SELE & " And t3.TI_DOCU_RECE = t1.TI_DOCU_RECE "
        sDE_SELE = sDE_SELE & " And t3.NU_DOCU_RECE = t1.NU_DOCU_RECE "
        sDE_SELE = sDE_SELE & " INNER JOIN TCTITU t4 "
        sDE_SELE = sDE_SELE & " ON t4.CO_EMPR = t3.CO_EMPR "
        sDE_SELE = sDE_SELE & " And t4.TI_TITU = t3.TI_TITU "
        sDE_SELE = sDE_SELE & " And t4.NU_TITU = t3.NU_TITU "
        sDE_SELE = sDE_SELE & " LEFT JOIN TTMONE t2 "
        sDE_SELE = sDE_SELE & " ON t2.CO_MONE = t1.CO_MONE "
        sDE_SELE = sDE_SELE & " Where "
        sDE_SELE = sDE_SELE & " t1.CO_EMPR = '" & sCO_EMPR & "' "
        sDE_SELE = sDE_SELE & " And t1.CO_UNID = '" & sCO_UNID & "' "
        sDE_SELE = sDE_SELE & " And t1.TI_DOCU_RETI = '" & sTI_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And t1.NU_DOCU_RETI = '" & sNU_DOCU_RETI & "' "
        sDE_SELE = sDE_SELE & " And t1.TI_SITU = 'ACT' "

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        cmTDRETI_MERC.CommandTimeout = 0

        Dim daTDRETI_MERC As New SqlDataAdapter
        daTDRETI_MERC.SelectCommand = cmTDRETI_MERC

        Dim dsTDRETI_MERC As New DataSet
        daTDRETI_MERC.Fill(dsTDRETI_MERC)

        cnn.Close()
        Return dsTDRETI_MERC
    End Function

    Private Function fn_FACT_SIGU_Q01(ByVal sCO_EMPR As String, ByVal sCO_UNID As String,
                                                        ByVal sTI_DOCU_RECE As String, ByVal sNU_DOCU_RECE As String,
                                                        ByVal sFE_FACT As String, ByVal sCO_CLIE As String,
                                                        ByVal sCO_TARI As String, ByVal sCO_ALMA As String) As String
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringClientes")
            cnn.Open()
        End If

        Dim sDE_SELE As String
        sDE_SELE = "Select dbo.FU_FACT_SIGU('" & sCO_EMPR & "', '" & sCO_UNID & "', '" & sTI_DOCU_RECE & "', '"
        sDE_SELE = sDE_SELE & sNU_DOCU_RECE & "', '" & sFE_FACT & "', '" & sCO_CLIE & "', '"
        sDE_SELE = sDE_SELE & sCO_TARI & "', '" & sCO_ALMA & "')"

        Dim cmTDRETI_MERC As New SqlCommand(sDE_SELE, cnn)
        Dim sFE_FACT_SIGU As String = cmTDRETI_MERC.ExecuteScalar

        cnn.Close()
        Return sFE_FACT_SIGU
    End Function

    Public Function fn_TMCLIE_Q03(ByVal sCO_CLIE As String) As DataSet
        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If

        Dim sSQL As String
        sSQL = "Select NO_CLIE_REPO, NU_RUCS_0001 " &
               "From TMCLIE " &
               "Where CO_CLIE='" & sCO_CLIE & "'"

        Dim cmTMCLIE As New SqlCommand(sSQL, cnn)
        cmTMCLIE.CommandTimeout = 0

        Dim daTMCLIE As New SqlDataAdapter
        daTMCLIE.SelectCommand = cmTMCLIE

        Dim dsTMCLIE As New DataSet
        daTMCLIE.Fill(dsTMCLIE)
        cnn.Close()
        Return dsTMCLIE
    End Function

    Private Function fn_TCCONF_CLIE_Q01(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String,
                                                        ByVal sCO_UNID As String) As Integer
        Try
            Dim sDE_SELE As String
            sDE_SELE = "Select NU_DIRE_FACT From TCCONF_CLIE Where CO_EMPR='" & sCO_EMPR & "' " &
                       "And CO_UNID='" & sCO_UNID & "' And CO_CLIE='" & sCO_CLIE & "'"

            If cnn.State = ConnectionState.Closed Then
                cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                cnn.Open()
            End If
            Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
            Dim nNU_SECU_DIRE As Integer = cmTRDIRE_CLIE.ExecuteScalar
            cnn.Close()
            Return nNU_SECU_DIRE
        Catch ex As Exception
            Dim sCA_ERRO As String = ex.Message
        End Try
    End Function

    Private Function fn_TRDIRE_CLIE_Q03(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String) As Integer
        Try
            Dim sDE_SELE As String
            sDE_SELE = "Select IsNull(Min(NU_SECU),1) 'NU_SECU_DIRE' From TRDIRE_CLIE Where CO_CLIE ='" & sCO_CLIE & "' " &
                       "And TI_DIRE = (Select IsNull(TI_DIRE_FISC, '') From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "') " &
                       "And TI_SITU = 'ACT' "

            If cnn.State = ConnectionState.Closed Then
                cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
                cnn.Open()
            End If
            Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
            Dim nNU_SECU_DIRE As Integer = cmTRDIRE_CLIE.ExecuteScalar
            cnn.Close()
            Return nNU_SECU_DIRE
        Catch ex As Exception
            Dim sCA_ERRO As String = ex.Message
        End Try
    End Function

    Public Function fn_TRDIRE_CLIE_Q03(ByVal sCO_EMPR As String, ByVal sCO_CLIE As String, ByVal sCO_UNID As String) As DataSet
        Dim sDE_SELE As String
        Dim nNU_SECU_DIRE As Integer = fn_TCCONF_CLIE_Q01(sCO_EMPR, sCO_CLIE, sCO_UNID)

        If nNU_SECU_DIRE < 1 Then
            nNU_SECU_DIRE = fn_TRDIRE_CLIE_Q03(sCO_EMPR, sCO_CLIE)
        End If

        sDE_SELE = "Select DE_DIRE,NU_TELE_0001 From TRDIRE_CLIE Where CO_CLIE = '" & sCO_CLIE & "' " &
                       "And TI_DIRE =(Select IsNull(TI_DIRE_FISC, '') From TMPARA_COME Where CO_EMPR = '" & sCO_EMPR & "') " &
                       "And NU_SECU =" & nNU_SECU_DIRE & "  And TI_SITU = 'ACT'"

        If cnn.State = ConnectionState.Closed Then
            cnn.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
            cnn.Open()
        End If
        Dim cmTRDIRE_CLIE As New SqlCommand(sDE_SELE, cnn)
        cmTRDIRE_CLIE.CommandTimeout = 0

        Dim daTCALMA_MERC As New SqlDataAdapter
        daTCALMA_MERC.SelectCommand = cmTRDIRE_CLIE

        Dim dsTMCALMA_MERC As New DataSet
        daTCALMA_MERC.Fill(dsTMCALMA_MERC)
        cnn.Close()
        Return dsTMCALMA_MERC
    End Function

    Public Function GetReporteRetiro(ByVal strCoEmpresa As String, ByVal strTipoDocumento As String,
                                                ByVal strNroDocumento As String, ByVal strEntidad As String,
                                                ByVal dtNew As DataTable, ByVal strRutaFirmas As String,
                                                ByVal strNombreReporte As String, ByVal strUsuario As String,
                                                ByVal blnImprimeTotales As Boolean, ByVal strIdSico As String,
                                                ByVal strDestino As String, ByVal sST_VALI As String) As String
        Dim report As Report = New Report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        Dim page As Page
        Dim stream_Firma As Stream
        Dim dtLiberacion As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As DataSet
        Dim dsTRDIRE_CLIE As DataSet
        Dim dsTDRETI_MERC_0001 As DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim sDE_DIRE As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String
        Dim sNU_BREV_CHOF As String
        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_EMIS As String
        Dim sFE_CANC As String
        Dim strFechaFact As String()
        Dim intCuadro As Int16 = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        Dim rows As DataRow()
        Dim intSinImportes As Integer
        Dim drDE_MERC As DataRow
        Dim sDE_MERC As String
        Dim dtResumen As DataTable
        dtResumen = dtNew.Clone()
        rows = dtNew.Select("", "NU_DOCU_RECE ASC, NU_SECU ASC")
        For Each dr As DataRow In rows
            sDE_MERC = dr("DE_MERC")
            If dr("DE_MERC").ToString.Length <= 30 Then
                dtResumen.ImportRow(dr)
            ElseIf dr("DE_MERC").ToString.Length <= 60 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 90 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 120 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 150 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 180 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(150)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length > 180 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(150, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(180)
                dtResumen.Rows.Add(drDE_MERC)
            End If
            'dtResumen.ImportRow(dr)
        Next
        intSinImportes = fn_TRMODA_CLIE(strCoEmpresa, "001", strIdSico)
        sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, dtResumen.Rows(0)("CO_UNID"), strTipoDocumento, strNroDocumento)
        dtLiberacion = fn_TDRETI_MERC_Q02(strCoEmpresa, dtResumen.Rows(0)("CO_UNID"), strTipoDocumento, strNroDocumento)
        '-----------------------------------------------------------------------------
        If dtLiberacion.Rows.Count > 0 Then
            sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
            sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
            sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
            sNU_DOCU_RECE = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO COMPROBANTE")), "", dtLiberacion.Rows(0)("NUMERO COMPROBANTE"))
            sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
            sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
            sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
            sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
            sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
            sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))
            sNU_BREV_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_BREV_CHOF")), "", dtLiberacion.Rows(0)("NU_BREV_CHOF"))

            sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
            sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
            sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
            sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
            sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
            sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
            sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
            sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
            sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
            sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
            sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))
        End If
        '-------------------------------------------------------------------------------
        'Obtiene nombre y ruc del cliente 
        dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
        If dsTMCLIE.Tables(0).Rows.Count > 0 Then
            sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
            sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
        End If

        'Obtiene direcci�n y tel�fono del cliente 
        dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, dtResumen.Rows(0)("CO_UNID"))

        If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
            sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
            sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
        End If

        strCodAlmacen = dtResumen.Rows(0)("CO_ALMA_ACTU")
        fp_header.bBold = True
        '--------------
        If dtResumen.Rows.Count > 34 Then
            'intPaginas = ((dtResumen.Rows.Count / 30) + 1)
            intPaginas = Fix(((dtResumen.Rows.Count / 34) + 1))
        Else
            intPaginas = 1
        End If

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New Page(report)
            page.rWidthMM = 210
            page.rHeightMM = 314

            strFirmaDepsa1 = strRutaFirmas & "Logo_Depsa.JPG"
            strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"
            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
            stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))

            page.AddCB(intAltura - 10, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS(DOR)"))
            page.Add(460, intAltura, New RepString(fp_Titulo, strNroDocumento))
            page.AddCB(intAltura, New RepString(fp_Titulo, "DEPOSITO SIMPLE"))
            page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
            If sDE_DIRE.Length > 60 Then
                page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE.Substring(0, 60)))
            Else
                page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
            End If
            page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))

            page.Add(50, intAltura + 80, New RepString(fp_b, "Tipo Retiro             :"))
            page.Add(50, intAltura + 90, New RepString(fp_b, "Ubicaci�n               :"))
            page.Add(500, intAltura + 90, New RepString(fp_b, "Pag. " & p.ToString))

            page.Add(140, intAltura + 80, New RepString(fp, sTI_RETI_GIRA))
            page.Add(140, intAltura + 90, New RepString(fp, sDE_DIRE_ALMA))

            page.Add(460, intAltura + 40, New RepString(fp, sFE_EMIS))
            '-----------------------------------------------------------------------------------
            page.AddMM(17, 79, New RepLineMM(ppBold, 170, 0))
            page.AddMM(17, 85, New RepLineMM(ppBold, 170, 0))

            page.Add(50, intAltura + 116, New RepString(fp, "DCR"))
            page.Add(95, intAltura + 116, New RepString(fp, "�tem"))
            page.Add(115, intAltura + 116, New RepString(fp, "Bodega"))
            page.Add(155, intAltura + 116, New RepString(fp, "Cnt Unid."))
            page.Add(195, intAltura + 116, New RepString(fp, "Unidad"))
            page.Add(225, intAltura + 116, New RepString(fp, "Cnt Bulto"))
            page.Add(265, intAltura + 116, New RepString(fp, "Bulto"))
            page.Add(300, intAltura + 116, New RepString(fp, "Dsc de Mercader�a"))
            page.Add(430, intAltura + 116, New RepString(fp, "Prec.Unit."))
            page.Add(485, intAltura + 116, New RepString(fp, "Valor"))

            Dim decTotal As Decimal
            Dim decTotalUnidad As Decimal
            Dim decTotalBulto As Decimal
            Dim strMoneda As String
            If (p - 1) * (34) + 34 < dtResumen.Rows.Count Then
                intNroPag = (p - 1) * (34) + 34
            Else
                intNroPag = dtResumen.Rows.Count
            End If

            For i As Int16 = (p - 1) * (34) To intNroPag - 1
                intAltura += 10
                If IsDBNull(dtResumen.Rows(i)("NU_DOCU_RECE")) Then
                    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                Else
                    Select Case dtResumen.Rows(i)("CO_MONE")
                        Case "DOL"
                            strMoneda = "US$"
                        Case "SOL"
                            strMoneda = "S/."
                    End Select
                    page.Add(50, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("NU_DOCU_RECE")))
                    page.Add(100, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("NU_SECU")))
                    page.AddRight(145, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("CO_BODE_ACTU") & "-" & dtResumen.Rows(i)("CO_UBIC_ACTU")))
                    page.AddRight(185, intAltura + 123, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("NU_UNID_RETI"))))
                    page.Add(200, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("CO_TIPO_BULT")))
                    page.AddRight(260, intAltura + 123, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("NU_PESO_RETI"))))
                    page.Add(265, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("CO_UNME_PESO")))
                    decTotalUnidad += Convert.ToDecimal(dtResumen.Rows(i)("NU_UNID_RETI"))
                    decTotalBulto += Convert.ToDecimal(dtResumen.Rows(i)("NU_PESO_RETI"))
                    If intSinImportes = 1 Then
                        page.AddRight(470, intAltura + 123, New RepString(fp_table, strMoneda & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("IM_UNIT"))))
                        page.AddRight(530, intAltura + 123, New RepString(fp_table, strMoneda & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("NU_UNID_RETI") * dtResumen.Rows(i)("IM_UNIT"))))
                    End If
                    decTotal += Convert.ToDecimal(dtResumen.Rows(i)("NU_UNID_RETI") * dtResumen.Rows(i)("IM_UNIT"))
                    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                    'If dtResumen.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 150 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 180 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(150)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length > 180 Then
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(150, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 123, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(180)))
                    'End If
                End If
            Next
            If blnImprimeTotales Then
                page.AddRight(180, 600, New RepString(fp_b, "Total: " & String.Format("{0:##,##0.000}", decTotalUnidad)))
                page.AddRight(260, 600, New RepString(fp_b, String.Format("{0:##,##0.000}", decTotalBulto)))
            End If

            strFirmaDepsa1 = strRutaFirmas & "20100044626D005.JPG"
            'strFirmaDepsa2 = strRutaFirmas & "20100044626D004.JPG"

            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(20, 250, New RepImageMM(stream_Firma, Double.NaN, 27))
            'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            'page.AddMM(70, 240, New RepImageMM(stream_Firma, Double.NaN, 27))
            page.Add(50, 610, New RepString(fp_b, "Destino  :"))
            page.Add(110, 610, New RepString(fp, strDestino))

            page.Add(50, 620, New RepString(fp_b, "Observaciones    :"))
            page.Add(140, 620, New RepString(fp, sDE_OBSE))

            page.Add(280, 630, New RepString(fp, "RECIBI CONFORME"))
            page.AddMM(130, 224, New RepLineMM(ppBold, 40, 0))

            page.AddMM(20, 251, New RepLineMM(ppBold, 90, 0))
            page.Add(150, 721, New RepString(fp_b, "ALMACENERA DEL PER� S.A."))

            page.AddMM(20, 285, New RepLineMM(ppBold, 90, 0))
            page.Add(150, 818, New RepString(fp_b, "DEPOSITANTE"))

            'page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de DEPOSITOS S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
            page.Add(350, 700, New RepString(fp, strUsuario.ToUpper))
            page.Add(350, 730, New RepString(fp, "VEHICULO  " & sNU_PLAC))
            page.Add(350, 740, New RepString(fp, "CHOFER  " & sNO_CHOF))
            page.Add(350, 750, New RepString(fp, "BREVETE  " & sNU_BREV_CHOF))
            page.Add(350, 760, New RepString(fp, "D.N.I  " & sNU_DNNI))
            'If intSinImportes = 0 Then
            '    page.Add(350, 770, New RepString(fp, "D.N.I  " & sNU_DNNI))
            'End If
            If sST_VALI = "S" Then
                page.Add(55, 820, New RepString(fp_b, "V�B� COB."))
            End If

            page.Add(50, 840, New RepString(fp_b, "DE-R-018-GAP"))
            '----------------
            If p <> intPaginas Then
                intAltura = 120
            Else
                If intSinImportes = 0 Then
                    page.Add(350, 770, New RepString(fp, "COD.M" & CStr(Int(decTotal)) & "/" & Right(String.Format("{0:##,##0.00}", decTotal), 2) & Left(dtResumen.Rows(0)("CO_MONE"), 1)))
                    'page.AddRight(530, 600, New RepString(fp_b, strMoneda & "  " & String.Format("{0:##,##0.00}", decTotal)))
                Else
                    page.AddRight(530, 600, New RepString(fp_b, strMoneda & "  " & String.Format("{0:##,##0.00}", decTotal)))
                End If
            End If
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        stream_Firma = Nothing
        Return sNO_CLIE_REPO
    End Function

    Public Function GetReporteRetiroAduanero(ByVal strCoEmpresa As String, ByVal strTipoDocumento As String,
                                                ByVal strNroDocumento As String, ByVal strEntidad As String,
                                                ByVal dtNew As DataTable, ByVal strRutaFirmas As String,
                                                ByVal strNombreReporte As String, ByVal strUsuario As String,
                                                ByVal blnImprimeTotales As Boolean, ByVal strIdSico As String,
                                                ByVal strDestino As String, ByVal sST_VALI As String) As String
        Dim report As Report = New Report(New PdfFormatter)
        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
        Dim fp As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
        Dim fp_b As FontProp = New FontPropMM(fd, 2.2)
        Dim fp_header As FontProp = New FontPropMM(fd, 2.1)
        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
        Dim fp_Sello As FontProp = New FontPropMM(fd, 2.7, System.Drawing.Color.Red)
        Dim ppBold As PenProp = New PenProp(report, 1.1)
        Dim ppBoldSello As PenProp = New PenProp(report, 1.4, System.Drawing.Color.Red)
        fp_b.bBold = True
        fp_Titulo.bBold = True
        Dim page As Page
        Dim stream_Firma As Stream
        Dim dtLiberacion As DataTable
        Dim sFE_FACT_SIGU As String
        Dim intAltura As Integer = 120
        Dim dsTMCLIE As DataSet
        Dim dsTRDIRE_CLIE As DataSet
        Dim dsTDRETI_MERC_0001 As DataTable
        Dim sNO_CLIE_REPO As String
        Dim sNU_RUCS_0001 As String
        Dim strFirmaDepsa1 As String
        Dim strFirmaDepsa2 As String
        Dim sDE_DIRE As String
        Dim sDE_DIRE_ALMA As String
        Dim sTI_RETI_GIRA As String
        Dim sDE_OBSE As String
        Dim sNU_TELE_0001 As String
        Dim sDE_MODA_MOVI As String
        Dim sCO_MODA As String
        Dim sNO_CHOF As String
        Dim sNU_DNNI As String
        Dim sNU_PLAC As String
        Dim sNU_BREV_CHOF As String
        Dim sNU_PEDI_DEPO As String
        Dim sNU_CONO_EMBA As String
        Dim sNU_POLI_ADUA As String
        Dim sFE_POLI_ADUA As String
        Dim sNU_COMP_PAGO As String
        Dim sCO_ENTI_AGAD As String
        Dim sNU_MANI As String
        Dim sNO_VAPO As String
        Dim sIM_ADUA_SIST As String
        Dim sFA_CAMB_ADUA As String
        Dim sFE_EMIS As String
        Dim sFE_CANC As String
        Dim sDE_MERC As String
        Dim strFechaFact As String()
        Dim intCuadro As Int16 = 0
        Dim intPaginas As Int16 = 0
        Dim intNroPag As Int16 = 0
        Dim rows As DataRow()
        Dim intSinImportes As Integer
        Dim drDE_MERC As DataRow
        Dim dtResumen As New DataTable
        dtResumen = dtNew.Clone()
        rows = dtNew.Select("", "NU_DOCU_RECE ASC, NU_SECU ASC")
        For Each dr As DataRow In rows
            sDE_MERC = dr("DE_MERC")
            If dr("DE_MERC").ToString.Length <= 30 Then
                dtResumen.ImportRow(dr)
            ElseIf dr("DE_MERC").ToString.Length <= 60 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 90 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 120 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 150 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length <= 180 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(150)
                dtResumen.Rows.Add(drDE_MERC)
            ElseIf dr("DE_MERC").ToString.Length > 180 Then
                dr.AcceptChanges()
                dr.BeginEdit()
                dr.Item(2) = sDE_MERC.ToString.Substring(0, 30)
                dr.EndEdit()
                dtResumen.ImportRow(dr)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(30, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(60, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(90, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(120, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(150, 30)
                dtResumen.Rows.Add(drDE_MERC)
                drDE_MERC = dtResumen.NewRow
                drDE_MERC(2) = sDE_MERC.ToString.Substring(180)
                dtResumen.Rows.Add(drDE_MERC)
            End If
            'dtResumen.ImportRow(dr)
        Next
        intSinImportes = fn_TRMODA_CLIE(strCoEmpresa, "001", strIdSico)
        sCO_MODA = fn_TDRETI_MERC_Q05(strCoEmpresa, dtResumen.Rows(0)("CO_UNID"), strTipoDocumento, strNroDocumento)
        dtLiberacion = fn_TDRETI_MERC_Q02(strCoEmpresa, dtResumen.Rows(0)("CO_UNID"), strTipoDocumento, strNroDocumento)
        '-----------------------------------------------------------------------------
        If dtLiberacion.Rows.Count > 0 Then
            sDE_MODA_MOVI = IIf(IsDBNull(dtLiberacion.Rows(0)("MODALIDAD")), "", dtLiberacion.Rows(0)("MODALIDAD"))
            sDE_MODA_MOVI = sDE_MODA_MOVI.Trim
            sFE_EMIS = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA O/R")), "", dtLiberacion.Rows(0)("FECHA O/R"))
            sNU_DOCU_RECE = IIf(IsDBNull(dtLiberacion.Rows(0)("NUMERO COMPROBANTE")), "", dtLiberacion.Rows(0)("NUMERO COMPROBANTE"))
            sDE_DIRE_ALMA = IIf(IsDBNull(dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN")), "", dtLiberacion.Rows(0)("DIRECCION DEL ALMACEN"))
            sTI_RETI_GIRA = IIf(IsDBNull(dtLiberacion.Rows(0)("TIPO RETIRO")), "", dtLiberacion.Rows(0)("TIPO RETIRO"))
            sDE_OBSE = IIf(IsDBNull(dtLiberacion.Rows(0)("DE_OBSE")), "", dtLiberacion.Rows(0)("DE_OBSE"))
            sNO_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NO_CHOF")), "", dtLiberacion.Rows(0)("NO_CHOF"))
            sNU_DNNI = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_DNNI")), "", dtLiberacion.Rows(0)("NU_DNNI"))
            sNU_PLAC = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_PLAC")), "", dtLiberacion.Rows(0)("NU_PLAC"))
            sNU_BREV_CHOF = IIf(IsDBNull(dtLiberacion.Rows(0)("NU_BREV_CHOF")), "", dtLiberacion.Rows(0)("NU_BREV_CHOF"))

            sNU_PEDI_DEPO = IIf(IsDBNull(dtLiberacion.Rows(0)("PEDIDO_DEPOSITO")), "", dtLiberacion.Rows(0)("PEDIDO_DEPOSITO"))
            sNU_CONO_EMBA = IIf(IsDBNull(dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE")), "", dtLiberacion.Rows(0)("CONOCIMIENTO_EMBARQUE"))
            sNU_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("P�LIZA")), "", dtLiberacion.Rows(0)("P�LIZA"))
            sFE_POLI_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA NUMERACI�N")), "", dtLiberacion.Rows(0)("FECHA NUMERACI�N"))
            sNU_COMP_PAGO = IIf(IsDBNull(dtLiberacion.Rows(0)("COMPROBANTE PAGO")), "", dtLiberacion.Rows(0)("COMPROBANTE PAGO"))
            sCO_ENTI_AGAD = IIf(IsDBNull(dtLiberacion.Rows(0)("COD. AG. ADUANA")), "", dtLiberacion.Rows(0)("COD. AG. ADUANA"))
            sNU_MANI = IIf(IsDBNull(dtLiberacion.Rows(0)("N�MERO MANIFIESTO")), "", dtLiberacion.Rows(0)("N�MERO MANIFIESTO"))
            sNO_VAPO = IIf(IsDBNull(dtLiberacion.Rows(0)("NOMBRE VAPOR")), "", dtLiberacion.Rows(0)("NOMBRE VAPOR"))
            sIM_ADUA_SIST = IIf(IsDBNull(dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL")), "", dtLiberacion.Rows(0)("IMPORTE MONEDA NACIONAL"))
            sFA_CAMB_ADUA = IIf(IsDBNull(dtLiberacion.Rows(0)("FACTOR CAMBIO")), "", dtLiberacion.Rows(0)("FACTOR CAMBIO"))
            sFE_CANC = IIf(IsDBNull(dtLiberacion.Rows(0)("FECHA CANCELACI�N")), "", dtLiberacion.Rows(0)("FECHA CANCELACI�N"))
        End If
        '-------------------------------------------------------------------------------
        If strEntidad = System.Configuration.ConfigurationManager.AppSettings("CodClieDEPSA") Then
            'Obtiene nombre y ruc del cliente 
            dsTMCLIE = fn_TMCLIE_Q03(dtResumen.Rows(0)("CLIENTE"))
        Else
            'Obtiene nombre y ruc del cliente 
            dsTMCLIE = fn_TMCLIE_Q03(strEntidad)
        End If

        If dsTMCLIE.Tables(0).Rows.Count > 0 Then
            sNO_CLIE_REPO = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO")), "", dsTMCLIE.Tables(0).Rows(0)("NO_CLIE_REPO"))
            sNU_RUCS_0001 = IIf(IsDBNull(dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001")), "", dsTMCLIE.Tables(0).Rows(0)("NU_RUCS_0001"))
        End If

        If strEntidad = System.Configuration.ConfigurationManager.AppSettings("CodClieDEPSA") Then
            'Obtiene direcci�n y tel�fono del cliente 
            dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, dtResumen.Rows(0)("CLIENTE"), dtResumen.Rows(0)("CO_UNID"))
        Else
            'Obtiene direcci�n y tel�fono del cliente 
            dsTRDIRE_CLIE = fn_TRDIRE_CLIE_Q03(strCoEmpresa, strEntidad, dtResumen.Rows(0)("CO_UNID"))
        End If

        If dsTRDIRE_CLIE.Tables(0).Rows.Count > 0 Then
            sDE_DIRE = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("DE_DIRE"))
            sNU_TELE_0001 = IIf(IsDBNull(dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001")), "", dsTRDIRE_CLIE.Tables(0).Rows(0)("NU_TELE_0001"))
        End If

        strCodAlmacen = dtResumen.Rows(0)("CO_ALMA_ACTU")
        fp_header.bBold = True
        '--------------
        If dtResumen.Rows.Count > 28 Then
            'intPaginas = ((dtResumen.Rows.Count / 9) + 1)
            intPaginas = Fix(((dtResumen.Rows.Count / 28) + 1))
        Else
            intPaginas = 1
        End If

        For p As Int16 = 1 To intPaginas
            '---------------------
            page = New Page(report)
            page.rWidthMM = 210
            page.rHeightMM = 314

            strFirmaDepsa1 = strRutaFirmas & "Logo_Depsa.JPG"
            strFirmaDepsa2 = strRutaFirmas & "Logo_ISO.JPG"
            stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(3, intAltura - 70, New RepImageMM(stream_Firma, Double.NaN, 50))
            stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
            page.AddMM(170, intAltura - 90, New RepImageMM(stream_Firma, Double.NaN, 30))

            page.AddCB(intAltura - 10, New RepString(fp_Titulo, "ORDEN DE RETIRO DE MERCADERIAS(DOR)"))
            page.Add(460, intAltura, New RepString(fp_Titulo, strNroDocumento))
            page.AddCB(intAltura, New RepString(fp_Titulo, "DEPOSITO ADUANERO"))
            page.Add(150, intAltura + 30, New RepString(fp, sNO_CLIE_REPO))
            If sDE_DIRE.Length > 60 Then
                page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE.Substring(0, 60)))
            Else
                page.Add(150, intAltura + 40, New RepString(fp, sDE_DIRE))
            End If
            page.Add(150, intAltura + 50, New RepString(fp, "R.U.C. " & sNU_RUCS_0001 & "  TLF. " & sNU_TELE_0001))

            page.Add(50, intAltura + 80, New RepString(fp_b, "Pedido Dep�sito       :"))
            page.Add(50, intAltura + 90, New RepString(fp_b, "Conocimiento           :"))
            page.Add(50, intAltura + 100, New RepString(fp_b, "P�liza Nro.                :"))
            page.Add(50, intAltura + 110, New RepString(fp_b, "Fecha Numeraci�n   :"))
            page.Add(50, intAltura + 120, New RepString(fp_b, "Comprobante Pago  :"))
            page.Add(50, intAltura + 130, New RepString(fp_b, "Cod. Agente Ad.       :"))
            page.Add(50, intAltura + 140, New RepString(fp_b, "Direcci�n Almac�n   :"))
            page.Add(50, intAltura + 150, New RepString(fp_b, "Tipo Retiro                :"))

            page.Add(140, intAltura + 80, New RepString(fp, sNU_PEDI_DEPO))
            page.Add(140, intAltura + 90, New RepString(fp, sNU_CONO_EMBA))
            page.Add(140, intAltura + 100, New RepString(fp, sNU_POLI_ADUA))
            page.Add(140, intAltura + 110, New RepString(fp, sFE_POLI_ADUA))
            page.Add(140, intAltura + 120, New RepString(fp, sNU_COMP_PAGO))
            page.Add(140, intAltura + 130, New RepString(fp, sCO_ENTI_AGAD))
            page.Add(140, intAltura + 140, New RepString(fp, sDE_DIRE_ALMA))
            page.Add(140, intAltura + 150, New RepString(fp, sTI_RETI_GIRA))

            page.Add(250, intAltura + 80, New RepString(fp_b, "Manifiesto                :"))
            page.Add(250, intAltura + 90, New RepString(fp_b, "Vapor                        :"))
            page.Add(250, intAltura + 100, New RepString(fp_b, "Moneda Nacional    :"))
            page.Add(250, intAltura + 110, New RepString(fp_b, "Tipo de Cambio       :"))
            page.Add(250, intAltura + 120, New RepString(fp_b, "Fecha Cancelaci�n :"))

            page.Add(350, intAltura + 80, New RepString(fp, sNU_MANI))
            page.Add(350, intAltura + 90, New RepString(fp, sNO_VAPO))
            page.Add(350, intAltura + 100, New RepString(fp, sIM_ADUA_SIST))
            page.Add(350, intAltura + 110, New RepString(fp, sFA_CAMB_ADUA))
            page.Add(350, intAltura + 120, New RepString(fp, sFE_CANC))

            page.Add(500, intAltura + 150, New RepString(fp_b, "Pag. " & p.ToString))
            page.Add(460, intAltura + 40, New RepString(fp, sFE_EMIS))
            '-----------------------------------------------------------------------------------
            page.AddMM(17, 98, New RepLineMM(ppBold, 170, 0))
            page.AddMM(17, 105, New RepLineMM(ppBold, 170, 0))

            page.Add(50, intAltura + 170, New RepString(fp, "DCR"))
            page.Add(95, intAltura + 170, New RepString(fp, "�tem"))
            page.Add(115, intAltura + 170, New RepString(fp, "Bodega"))
            page.Add(155, intAltura + 170, New RepString(fp, "Cnt Unid."))
            page.Add(195, intAltura + 170, New RepString(fp, "Unidad"))
            page.Add(225, intAltura + 170, New RepString(fp, "Cnt Bulto"))
            page.Add(265, intAltura + 170, New RepString(fp, "Bulto"))
            page.Add(300, intAltura + 170, New RepString(fp, "Dsc de Mercader�a"))
            page.Add(430, intAltura + 170, New RepString(fp, "Prec.Unit."))
            page.Add(485, intAltura + 170, New RepString(fp, "Valor"))

            Dim decTotal As Decimal
            Dim decTotalUnidad As Decimal
            Dim decTotalBulto As Decimal
            Dim strMoneda As String
            If (p - 1) * (28) + 28 < dtResumen.Rows.Count Then
                intNroPag = (p - 1) * (28) + 28
            Else
                intNroPag = dtResumen.Rows.Count
            End If

            'For i As Int16 = (p - 1) * (28) To intNroPag - 1
            '    intAltura += 10
            '    If Not IsDBNull(dtResumen.Rows(i)("NU_DOCU_RECE")) Then
            '        page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("NU_DOCU_RECE")))
            '    End If
            '    If Not IsDBNull(dtResumen.Rows(i)("DE_MERC")) Then
            '        page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
            '    End If
            'Next
            For i As Int16 = (p - 1) * (28) To intNroPag - 1
                intAltura += 10
                If IsDBNull(dtResumen.Rows(i)("NU_DOCU_RECE")) Then
                    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                Else
                    Select Case dtResumen.Rows(i)("CO_MONE")
                        Case "DOL"
                            strMoneda = "US$"
                        Case "SOL"
                            strMoneda = "S/."
                    End Select

                    page.Add(50, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("NU_DOCU_RECE")))
                    page.Add(100, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("NU_SECU")))
                    page.AddRight(145, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("CO_BODE_ACTU") & "-" & dtResumen.Rows(i)("CO_UBIC_ACTU")))
                    page.AddRight(185, intAltura + 180, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("NU_UNID_RETI"))))
                    page.Add(200, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("CO_TIPO_BULT")))
                    page.AddRight(260, intAltura + 180, New RepString(fp_table, String.Format("{0:##,##0.000}", dtResumen.Rows(i)("NU_PESO_RETI"))))
                    page.Add(265, intAltura + 180, New RepString(fp_table, IIf(IsDBNull(dtResumen.Rows(i)("CO_UNME_PESO")) = True, "", dtResumen.Rows(i)("CO_UNME_PESO"))))

                    decTotalUnidad += Convert.ToDecimal(dtResumen.Rows(i)("NU_UNID_RETI"))
                    decTotalBulto += Convert.ToDecimal(dtResumen.Rows(i)("NU_PESO_RETI"))

                    page.AddRight(470, intAltura + 180, New RepString(fp_table, strMoneda & "  " & String.Format("{0:##,##0.000}", dtResumen.Rows(i)("IM_UNIT"))))
                    page.AddRight(530, intAltura + 180, New RepString(fp_table, strMoneda & "  " & String.Format("{0:##,##0.00}", dtResumen.Rows(i)("NU_UNID_RETI") * dtResumen.Rows(i)("IM_UNIT"))))

                    decTotal += Convert.ToDecimal(dtResumen.Rows(i)("NU_UNID_RETI") * dtResumen.Rows(i)("IM_UNIT"))
                    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                    'If dtResumen.Rows(i)("DE_MERC").ToString.Length <= 30 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC")))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 60 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 90 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 120 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 150 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length <= 180 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(150)))
                    'ElseIf dtResumen.Rows(i)("DE_MERC").ToString.Length > 180 Then
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(0, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(30, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(60, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(90, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(120, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(150, 30)))
                    '    intAltura += 10
                    '    page.Add(290, intAltura + 180, New RepString(fp_table, dtResumen.Rows(i)("DE_MERC").ToString.Substring(180)))
                    'End If
                End If
            Next
            If blnImprimeTotales Then
                page.AddRight(180, 600, New RepString(fp_b, "Total: " & String.Format("{0:##,##0.000}", decTotalUnidad)))
                page.AddRight(260, 600, New RepString(fp_b, String.Format("{0:##,##0.000}", decTotalBulto)))
            End If

            'If strEntidad = System.Configuration.ConfigurationManager.AppSettings("CodClieDEPSA") Then
            '    strFirmaDepsa1 = strRutaFirmas & "20100044626D005.JPG"
            '    'strFirmaDepsa2 = strRutaFirmas & "20100044626D004.JPG"

            '    stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
            '    page.AddMM(20, 250, New RepImageMM(stream_Firma, Double.NaN, 27))
            'End If

            page.Add(50, 610, New RepString(fp_b, "Destino  :"))
            page.Add(110, 610, New RepString(fp, strDestino))

            page.Add(50, 620, New RepString(fp_b, "Observaciones    :"))
            page.Add(140, 620, New RepString(fp, sDE_OBSE))

            page.Add(280, 650, New RepString(fp, "RECIBI CONFORME"))
            page.AddMM(130, 230, New RepLineMM(ppBold, 40, 0))

            page.AddMM(20, 251, New RepLineMM(ppBold, 90, 0))
            page.Add(150, 721, New RepString(fp_b, "ALMACENERA DEL PER� S.A."))

            page.AddMM(20, 285, New RepLineMM(ppBold, 90, 0))
            page.Add(150, 818, New RepString(fp_b, "DEPOSITANTE"))

            'page.Add(50, intAltura + 490, New RepString(fp, "La pte. orden de retiro solo ser� valida con las firmas de DEPOSITOS S.A., el DEPOSITANTE y la ENTIDAD ENDOSATARIA"))
            page.Add(350, 700, New RepString(fp, strUsuario.ToUpper))
            page.Add(350, 730, New RepString(fp, "VEHICULO  " & sNU_PLAC))
            page.Add(350, 740, New RepString(fp, "CHOFER  " & sNO_CHOF))
            page.Add(350, 750, New RepString(fp, "BREVETE  " & sNU_BREV_CHOF))
            page.Add(350, 760, New RepString(fp, "D.N.I  " & sNU_DNNI))

            If sST_VALI = "S" Then
                page.Add(55, 820, New RepString(fp_b, "V�B� COB."))
            End If

            page.Add(50, 840, New RepString(fp_b, "DE-R-018-GAP"))
            '----------------
            If p <> intPaginas Then
                intAltura = 120
            Else
                page.AddRight(525, 600, New RepString(fp_b, "Total: " & strMoneda & "  " & String.Format("{0:##,##0.00}", decTotal)))
            End If
        Next
        '--------------
        RT.ViewPDF(report, strNombreReporte)
        dsTMCLIE = Nothing
        dsTRDIRE_CLIE = Nothing
        dsTDRETI_MERC_0001 = Nothing
        dtLiberacion = Nothing
        stream_Firma = Nothing
        Return sNO_CLIE_REPO
    End Function

    Public Function gCNCrearPedido(ByVal strNu_Docu_Pedi As String) As String
        Return objRetiro.gCADCrearPedido(strNu_Docu_Pedi)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Public Property strMensajeError() As String
        Get
            Return lstrMensajeError
        End Get
        Set(ByVal Value As String)
            lstrMensajeError = Value
        End Set
    End Property
End Class
