Imports Depsa.LibCapaAccesoDatos
Imports System.Data.SqlClient

Public Class clsCNMercaderia
    Private objMercaderia As clsCADMercaderia

    Public Function gCNGetListarInventario(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                                        ByVal strReferencia As String, ByVal strNroReferencia As String, _
                                                        ByVal strModalidad As String, ByVal strFecVenc As String, _
                                                        ByVal strFecCierre As String, ByVal blnNoCerrado As Boolean, _
                                                        ByVal strTipoModulo As String, ByVal strCodAlmacen As String, ByVal strEstado As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetListarInventario(strCodEmpresa, strCodCliente, strReferencia, _
                                                    strNroReferencia, strModalidad, strFecVenc, _
                                                strFecCierre, blnNoCerrado, strTipoModulo, strCodAlmacen, strEstado)
    End Function

    Public Function gCNGetListarInventarioGlabal(ByVal strCodEmpresa As String, ByVal strCodCliente As String, ByVal strTipoEntidad As String, ByVal strCodEntidad As String, _
                                                    ByVal strReferencia As String, ByVal strNroReferencia As String, _
                                                    ByVal strModalidad As String, ByVal strEstado As String, ByVal strCodAlmacen As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetListarInventarioGlobal(strCodEmpresa, strCodCliente, strTipoEntidad, strCodEntidad, strReferencia, _
                                                    strNroReferencia, strModalidad, strEstado, strCodAlmacen)
    End Function

    Public Function gCNGetListarSolicitud(ByVal strNroWarrant As String, ByVal strFechaInicial As String, _
                                                 ByVal strFechaFinal As String, ByVal strTipoEntidad As String, _
                                                 ByVal strCodSico As String, ByVal strEstado As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetListarSolicitud(strNroWarrant, strFechaInicial, strFechaFinal, _
                                                    strTipoEntidad, strCodSico, strEstado)
    End Function

    Public Function gCNGetListarAlmacen(ByVal strCodCliente As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCNGetListarAlmacen(strCodCliente)
    End Function

    Public Function gCNCrearTranladoGlobal(ByVal strCodEmpresa As String, ByVal strCodUnid As String, _
                                           ByVal strNroComp As String, ByVal strNroItem As Integer, _
                                            ByVal strNroDocu As String, ByVal strUbiOri As String, _
                                            ByVal strUbiDes As String, ByVal strEst As String, _
                                            ByVal strUsuCrea As String, _
                                            ByVal objTransaction As SqlTransaction) As String

        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCNCrearTranladoGlobal(strCodEmpresa, strCodUnid, strNroComp, strNroItem, _
        strNroDocu, strUbiOri, strUbiDes, strEst, strUsuCrea, objTransaction)
    End Function

    Public Function gCNInsAlmacenNuevo(ByVal strAlmacen As String, ByVal strCodCliente As String, ByVal strCodUsuario As String, ByVal objTransaction As SqlTransaction) As String
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCNInsAlmacenNuevo(strAlmacen, strCodCliente, strCodUsuario, objTransaction)
    End Function

    Public Sub gCNUpdEstadoSolicitud(ByVal strNroSolicitud As String, ByVal strCodUsuario As String, ByVal strAccion As String)
        objMercaderia = New clsCADMercaderia
        objMercaderia.gCNUpdEstadoSolicitud(strNroSolicitud, strCodUsuario, strAccion)
    End Sub

    Public Function gCNGetListarRetiros(ByVal strCodEmpresa As String, ByVal strCodUnidad As String, ByVal strNroDCR As String, _
                                                    ByVal strNroItem As String, ByVal strEstado As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetListarRetiros(strCodEmpresa, strCodUnidad, strNroDCR, strNroItem, strEstado)
    End Function

    Public Function gCNGetListarInventarioWMS(ByVal strCodigo As String, ByVal strDescMerc As String, _
                                                ByVal strLote As String, ByVal strFechaInicial As String, _
                                                ByVal strFechaFinal As String, ByVal strFechaVenc As String, _
                                                ByVal strAlmacen As String, ByVal strCodCliente As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetListarInventarioWMS(strCodigo, strDescMerc, strLote, strFechaInicial, strFechaFinal, strFechaVenc, strAlmacen, strCodCliente)
    End Function

    Public Function gCNGetGeneraRepoBulxPal(ByVal strCodEmpr As String, ByVal strCodUnid As String, ByVal strFechaInicio As String, ByVal strCodAlma As String, ByVal strCodCliente As String) As DataTable
        objMercaderia = New clsCADMercaderia
        Return objMercaderia.gCADGetGeneraRepoBulxPal(strCodEmpr, strCodUnid, strFechaInicio, strCodAlma, strCodCliente)
    End Function
End Class
