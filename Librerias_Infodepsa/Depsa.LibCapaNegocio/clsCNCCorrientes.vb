Imports Depsa.LibCapaAccesoDatos
Public Class clsCNCCorrientes
    Private pstrti_docu As String
    Private pstrno_docu As String
    Private dttemporal As DataTable
    Private dstemporal As DataSet
    Private pstrerrorSql As String
    Private pintFilasAfectadas As Integer
    Private pstrResultado As String

    Public Function fn_devuelvetidocu() As String
        Return pstrti_docu
    End Function

    Public Function fn_devuelvenodocu() As String
        Return pstrno_docu
    End Function

    Public Function fn_devuelvedatatable() As DataTable
        Return dttemporal
    End Function

    Public Function fn_devuelveErrorSql() As String
        Return pstrerrorSql
    End Function

    Public Function fn_devuelveFilas() As Integer
        Return pintFilasAfectadas
    End Function

    Public Function fn_devuelveResultado() As String
        Return pstrResultado
    End Function

    Public Function fn_devuelvedataset() As DataSet
        Return dstemporal
    End Function

    Public Sub gCNGetTipoDocumentos()
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            .gCADGetTipoDocumentos()
            dttemporal = .fn_devuelvedatatable
        End With
    End Sub

    Public Sub gCNGetMonedas()
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            .gCADGetMonedas()
            dttemporal = .fn_devuelvedatatable
        End With
    End Sub

    Public Sub gCADGetCancelaciones(ByVal strco_empr As String, ByVal strco_clie As String, ByVal strti_docu As String, _
                                   ByVal strfe_inic As String, ByVal strfe_fina As String)
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            .gCADGetCancelaciones(strco_empr, strco_clie, strti_docu, strfe_inic, strfe_fina)
            dttemporal = .fn_devuelvedatatable
        End With
    End Sub

    Public Sub gCADGetEstadodeCuenta(ByVal strco_empr As String, ByVal strfe_proc As String, ByVal strco_clie As String)
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            .gCADGetEstadodeCuenta(strco_empr, strfe_proc, strco_clie)
            dstemporal = .fn_devuelveDataset
        End With
    End Sub

    Public Function gCADGetDocumentoDetalle(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                      ByVal strnu_docu As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetDocumentoDetalle(strco_empr, strco_unid, strti_docu, strnu_docu)
        End With
    End Function

    Public Function gCADGetDocumentoDetalleDOT(ByVal strco_empr As String, ByVal strco_unid As String, _
                                                ByVal strnu_docu As String, ByVal strco_clie As String, _
                                                ByVal strco_alma As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetDocumentoDetalleDOT(strco_empr, strco_unid, strnu_docu, strco_clie, strco_alma)
        End With
    End Function

    Public Function gCADGetSustentoDCR(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                    ByVal strnu_docu As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetSustentoDCR(strco_empr, strco_unid, strti_docu, strnu_docu)
        End With
    End Function

    Public Function gCADGetMostrarSustento(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                         ByVal strnu_docu As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetMostrarSustento(strco_empr, strco_unid, strti_docu, strnu_docu)
        End With
    End Function

    Public Function gCADGetAbono(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                               ByVal strnu_docu As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetAbono(strco_empr, strco_unid, strti_docu, strnu_docu)
        End With
    End Function

    Public Function gCADGetDocumentoDetalleWAR(ByVal strco_empr As String, ByVal strco_unid As String, _
                                            ByVal strnu_docu As String, ByVal strti_docu As String) As DataTable
        Dim objCCorrientes As New clsCADCCorrientes
        With objCCorrientes
            Return .gCADGetDocumentoDetalleWAR(strco_empr, strco_unid, strnu_docu, strti_docu)
        End With
    End Function
End Class
