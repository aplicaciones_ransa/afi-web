Imports Depsa.LibCapaAccesoDatos

Public Class clsCNMovimiento
    Inherits System.Web.UI.Page
    Private dtTemporal As DataTable

    Public Function gCNReporteDCR(ByVal strCoEmpr As String, ByVal strCoUnid As String, _
                                                ByVal strTiDocuRece As String, ByVal strNuDocuRece As String) As DataTable
        Dim objMovimiento As New clsCADMovimiento
        With objMovimiento
            Return .gCADReporteDCR(strCoEmpr, strCoUnid, strTiDocuRece, strNuDocuRece)
        End With
    End Function

    Public Function gCNListarKardex(ByVal strco_empr As String, ByVal strco_clie As String, _
                                                ByVal strti_movi As String, ByVal strti_refe As String, _
                                                ByVal strnu_refe As String, ByVal strti_repo As String, _
                                                ByVal strti_moda As String, ByVal strfe_inic As String, _
                                                ByVal strfe_fina As String, ByVal strno_cerr As String, _
                                                ByVal strfe_venc As String, ByVal strco_alma As String) As DataTable
        Dim objMovimiento As New clsCADMovimiento
        With objMovimiento
            dtTemporal = .gCADListarMovimiento(strco_empr, strco_clie, strti_movi, strti_refe, _
                                                strnu_refe, strti_repo, strti_moda, strfe_inic, _
                                                strfe_fina, strno_cerr, strfe_venc, strco_alma)
            Session.Item("dtTemporal") = dtTemporal
        End With
        If strti_movi = "I" And strti_repo = "2" Then
            Return fn_Quiebre(dtTemporal)
        ElseIf strti_movi = "R" And strti_repo = "2" Then
            Return fn_Quiebre2(dtTemporal)
        Else
            Return dtTemporal
        End If
    End Function

    Private Function fn_Quiebre(ByVal dtMovimiento As DataTable) As DataTable
        'Try
        Dim dr As DataRow
        Dim drNU_REGI As DataRow
        Dim decTotal As Decimal
        Dim strNroDCR As String = "&nbsp;"
        Dim dtTCDOCU_CLIE As New DataTable
        Dim strFechIng As String = ""
        Dim strFechCierre As String = ""
        dtTCDOCU_CLIE.Columns.Add("TI_DOCU_RECE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_DOCU_RECE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MODA_MOVI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("FE_REGI_INGR", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("FE_ACTU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_SECU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MERC_GENE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_TIPO_BULT", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_UNID_RECI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_UNME_PESO", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_PESO_RECI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("IM_TOTA_NACI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_PROD_CLIE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_PROD_CLIE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_ALMA", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_TARI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_UNID", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_TITU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_REFE_0002", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_UNME_VOLU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_VOLU_USAD", GetType(System.String))

        For i As Integer = 0 To dtMovimiento.Rows.Count - 1
            If strNroDCR <> dtMovimiento.Rows(i)("NU_DOCU_RECE") Then
                If i > 0 Then
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("CO_MONE") = "TOTAL : "
                    drNU_REGI("IM_TOTA_NACI") = decTotal
                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    decTotal = 0
                End If
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                If dtMovimiento.Rows(i)("FE_REGI_INGR") <> "" Then
                    strFechIng = "FE.INGR.:&nbsp;&nbsp;" & dtMovimiento.Rows(i)("FE_REGI_INGR")
                End If
                If dtMovimiento.Rows(i)("FE_ACTU") <> "" Then
                    strFechCierre = "FE.CIER.:&nbsp;&nbsp;" & dtMovimiento.Rows(i)("FE_ACTU")
                End If

                drNU_REGI("NU_SECU") = "DCR : " & dtMovimiento.Rows(i)("NU_DOCU_RECE") & "<BR>" & strFechIng & "&nbsp;&nbsp;&nbsp;" & strFechCierre
                drNU_REGI("CO_TIPO_BULT") = "MODALIDAD : " & dtMovimiento.Rows(i)("DE_MODA_MOVI") & "<BR>TARIFA : " & dtMovimiento.Rows(i)("DE_TARI")
                'drNU_REGI("DE_PROD_CLIE") = "ALMACEN : " & dtMovimiento.Rows(i)("DE_ALMA") & "<BR>NRO WARR : " & dtMovimiento.Rows(i)("NU_TITU")
                drNU_REGI("DE_PROD_CLIE") = "NRO WARR : " & dtMovimiento.Rows(i)("NU_TITU") & "<BR>&nbsp;"

                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                strNroDCR = dtMovimiento.Rows(i)("NU_DOCU_RECE")
            End If
            decTotal = decTotal + CType(dtMovimiento.Rows(i)("IM_TOTA_NACI"), Decimal)
            drNU_REGI = dtTCDOCU_CLIE.NewRow()
            drNU_REGI("TI_DOCU_RECE") = dtMovimiento.Rows(i)("TI_DOCU_RECE")
            drNU_REGI("NU_DOCU_RECE") = dtMovimiento.Rows(i)("NU_DOCU_RECE")
            drNU_REGI("DE_MODA_MOVI") = dtMovimiento.Rows(i)("DE_MODA_MOVI")
            drNU_REGI("FE_REGI_INGR") = dtMovimiento.Rows(i)("FE_REGI_INGR")
            drNU_REGI("FE_ACTU") = dtMovimiento.Rows(i)("FE_ACTU")
            drNU_REGI("NU_SECU") = dtMovimiento.Rows(i)("NU_SECU")
            drNU_REGI("DE_MERC_GENE") = dtMovimiento.Rows(i)("DE_MERC_GENE")
            drNU_REGI("DE_MERC") = dtMovimiento.Rows(i)("DE_MERC")
            drNU_REGI("CO_TIPO_BULT") = dtMovimiento.Rows(i)("CO_TIPO_BULT")
            drNU_REGI("NU_UNID_RECI") = CType(dtMovimiento.Rows(i)("NU_UNID_RECI"), Decimal)
            drNU_REGI("CO_UNME_PESO") = dtMovimiento.Rows(i)("CO_UNME_PESO")
            drNU_REGI("NU_PESO_RECI") = CType(dtMovimiento.Rows(i)("NU_PESO_RECI"), Decimal)
            drNU_REGI("CO_MONE") = dtMovimiento.Rows(i)("CO_MONE")
            drNU_REGI("IM_TOTA_NACI") = CType(dtMovimiento.Rows(i)("IM_TOTA_NACI"), Decimal)
            drNU_REGI("CO_PROD_CLIE") = dtMovimiento.Rows(i)("CO_PROD_CLIE")
            drNU_REGI("DE_PROD_CLIE") = dtMovimiento.Rows(i)("DE_PROD_CLIE")
            drNU_REGI("DE_ALMA") = dtMovimiento.Rows(i)("DE_ALMA")
            drNU_REGI("DE_TARI") = dtMovimiento.Rows(i)("DE_TARI")
            drNU_REGI("CO_UNID") = dtMovimiento.Rows(i)("CO_UNID")
            drNU_REGI("NU_TITU") = dtMovimiento.Rows(i)("NU_TITU")
            drNU_REGI("DE_REFE_0002") = dtMovimiento.Rows(i)("DE_REFE_0002")
            drNU_REGI("CO_UNME_VOLU") = dtMovimiento.Rows(i)("CO_UNME_VOLU")
            drNU_REGI("NU_VOLU_USAD") = dtMovimiento.Rows(i)("NU_VOLU_USAD")

            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            If i = dtMovimiento.Rows.Count - 1 Then
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("CO_MONE") = "TOTAL : "
                drNU_REGI("IM_TOTA_NACI") = decTotal
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                decTotal = 0
            End If
        Next
        Return dtTCDOCU_CLIE
        'Catch e1 As Exception
        '    Dim sCA_ERRO As String = e1.Message
        'End Try
    End Function

    Private Function fn_Quiebre2(ByVal dtMovimiento As DataTable) As DataTable
        'Try
        Dim dr As DataRow
        Dim drNU_REGI As DataRow
        Dim decTotal As Decimal
        Dim strNroDOR As String = "&nbsp;"
        Dim dtTCDOCU_CLIE As New DataTable
        Dim strFechSoli As String = ""
        Dim strFechCont As String = ""
        dtTCDOCU_CLIE.Columns.Add("NU_DOCU_RETI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("FE_DOCU_RETI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MODA_MOVI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_DOCU_RECE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_SECU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_ESTA_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_PROD_CLIE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_TIPO_BULT", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_UNID_RETI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_UNME_PESO", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_PESO_RETI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("IM_TOTA_NACI", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("DE_ALMA", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_GUIA_SALI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_OBSE_0001", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_TITU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_REFE_0002", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("TI_DOCU_RETI", GetType(System.String))

        For i As Integer = 0 To dtMovimiento.Rows.Count - 1
            If strNroDOR <> dtMovimiento.Rows(i)("NU_DOCU_RETI") Then
                If i > 0 Then
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("CO_MONE") = "TOTAL : "
                    drNU_REGI("IM_TOTA_NACI") = decTotal
                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    decTotal = 0
                End If
                If dtMovimiento.Rows(i)("FE_DOCU_RETI") <> "" Then
                    strFechSoli = "FE.SOLI.:&nbsp;&nbsp;" & dtMovimiento.Rows(i)("FE_DOCU_RETI")
                End If

                If dtMovimiento.Rows(i)("FE_SALI_ALMA") <> "" Then
                    strFechCont = "FE.CONT.:&nbsp;&nbsp;" & dtMovimiento.Rows(i)("FE_SALI_ALMA")
                End If

                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("DE_MODA_MOVI") = "DOR : " & dtMovimiento.Rows(i)("NU_DOCU_RETI") & " - NRO WARR : " & dtMovimiento.Rows(i)("NU_TITU")
                drNU_REGI("NU_SECU") = strFechSoli & "&nbsp;&nbsp;&nbsp;" & strFechCont
                'drNU_REGI("CO_PROD_CLIE") = "ALMACEN : " & dtMovimiento.Rows(i)("DE_ALMA")
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                strNroDOR = dtMovimiento.Rows(i)("NU_DOCU_RETI")
            End If
            decTotal = decTotal + CType(dtMovimiento.Rows(i)("IM_TOTA_NACI"), Decimal)
            drNU_REGI = dtTCDOCU_CLIE.NewRow()
            drNU_REGI("NU_DOCU_RETI") = dtMovimiento.Rows(i)("NU_DOCU_RETI")
            drNU_REGI("FE_DOCU_RETI") = dtMovimiento.Rows(i)("FE_DOCU_RETI")
            drNU_REGI("DE_MODA_MOVI") = dtMovimiento.Rows(i)("DE_MODA_MOVI")
            drNU_REGI("NU_DOCU_RECE") = dtMovimiento.Rows(i)("NU_DOCU_RECE")
            drNU_REGI("NU_SECU") = dtMovimiento.Rows(i)("NU_SECU")
            drNU_REGI("DE_ESTA_MERC") = dtMovimiento.Rows(i)("DE_ESTA_MERC")
            drNU_REGI("CO_PROD_CLIE") = dtMovimiento.Rows(i)("CO_PROD_CLIE")
            drNU_REGI("DE_MERC") = dtMovimiento.Rows(i)("DE_MERC")
            drNU_REGI("CO_TIPO_BULT") = dtMovimiento.Rows(i)("CO_TIPO_BULT")
            drNU_REGI("NU_UNID_RETI") = CType(dtMovimiento.Rows(i)("NU_UNID_RETI"), Decimal)
            drNU_REGI("CO_UNME_PESO") = dtMovimiento.Rows(i)("CO_UNME_PESO")
            drNU_REGI("NU_PESO_RETI") = CType(dtMovimiento.Rows(i)("NU_PESO_RETI"), Decimal)
            drNU_REGI("CO_MONE") = dtMovimiento.Rows(i)("CO_MONE")
            drNU_REGI("IM_TOTA_NACI") = CType(dtMovimiento.Rows(i)("IM_TOTA_NACI"), Decimal)
            drNU_REGI("DE_ALMA") = dtMovimiento.Rows(i)("DE_ALMA")
            drNU_REGI("NU_GUIA_SALI") = ""
            drNU_REGI("DE_OBSE_0001") = dtMovimiento.Rows(i)("DE_OBSE_0001")
            drNU_REGI("NU_TITU") = dtMovimiento.Rows(i)("NU_TITU")
            drNU_REGI("DE_REFE_0002") = dtMovimiento.Rows(i)("DE_REFE_0002")
            drNU_REGI("TI_DOCU_RETI") = dtMovimiento.Rows(i)("TI_DOCU_RETI")
            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            If i = dtMovimiento.Rows.Count - 1 Then
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("CO_MONE") = "TOTAL : "
                drNU_REGI("IM_TOTA_NACI") = decTotal
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                decTotal = 0
            End If
        Next
        Return dtTCDOCU_CLIE
    End Function

    Public Function gCNListarMoviMercWMS(ByVal strTipMovi As String, ByVal strTipRepo As String, _
                                        ByVal strCodigo As String, ByVal strDescMerc As String, _
                                        ByVal strLote As String, ByVal strFechaIni As String, _
                                        ByVal strFechaFina As String, ByVal strAlmacen As String, _
                                        ByVal strcodProd As String, ByVal strCodCliente As String) As DataTable
        strCodigo = strCodigo.Replace("'", "")
        strcodProd = strcodProd.Replace("'", "")
        strDescMerc = strDescMerc.Replace("'", "")
        Dim objMovimiento As New clsCADMovimiento
        With objMovimiento
            dtTemporal = .gCADListarMoviMercWMS(strTipMovi, strTipRepo, strCodigo, strDescMerc, strLote, strFechaIni, strFechaFina, strAlmacen, strcodProd, strCodCliente)
            Session.Item("dtTemporal") = dtTemporal
        End With
        Return dtTemporal
    End Function

    Public Function gCNReporteDOR(ByVal strCoEmpr As String, ByVal strCoUnid As String, _
                                            ByVal strTiDocuRece As String, ByVal strNuDocuRece As String) As DataTable
        'Dim objMovimiento As New clsCADMovimiento
        'With objMovimiento
        '    Return .gCADReporteDOR(strCoEmpr, strCoUnid, strTiDocuRece, strNuDocuRece)
        'End With
    End Function

    Public Function gCADGetDesModalidad(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                       ByVal strnu_docu As String) As DataTable
        Dim objMovimiento As New clsCADMovimiento
        With objMovimiento
            Return .gCADGGetModalidad(strco_empr, strco_unid, strti_docu, strnu_docu)
        End With
    End Function
End Class
