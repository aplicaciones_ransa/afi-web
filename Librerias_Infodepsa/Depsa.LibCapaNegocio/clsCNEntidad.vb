Imports Depsa.LibCapaAccesoDatos
Public Class clsCNEntidad
    Private objEntidad As clsCADEntidad

    Public Function gCNGetDataEmpresa(ByVal sCO_ENTI_TRAB As String) As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetDataEmpresa(sCO_ENTI_TRAB)
    End Function

    Public Function gCNGetDataDirecciones(ByVal sCO_ENTI_TRAB As String) As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetDataDirecciones(sCO_ENTI_TRAB)
    End Function

    Public Function gCNGetDataContactos(ByVal sCO_ENTI_TRAB As String) As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetDataContactos(sCO_ENTI_TRAB)
    End Function

    Public Function gCNGetTipoDireccion() As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetTipoDireccion()
    End Function

    Public Function gCNGetPais() As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetPais()
    End Function

    Public Function gCNGetTipoContacto() As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetTipoContacto()
    End Function

    Public Function gCNGetTipoDoc() As DataTable
        objEntidad = New clsCADEntidad
        Return objEntidad.gCADGetTipoDoc()
    End Function

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
