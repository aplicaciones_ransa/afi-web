Imports Depsa.LibCapaAccesoDatos
Public Class clsCNFactura
    Dim objFactura As clsCADFactura

    Public Function Lista_Facturas(ByVal strCodEmpresa As String, ByVal strCodCliente As String, _
                                    ByVal strCodUnidad As String, ByVal strNroDocumento As String, _
                                    ByVal strFechaInicial As String, ByVal strFechaFinal As String, _
                                    ByRef objGrid As System.Web.UI.WebControls.DataGrid, ByVal strNroDCR As String) As String
        Dim dsFactura As DataSet
        Dim dvTCDOCU_CLIE As DataView
        objFactura = New clsCADFactura
        With objFactura
            dsFactura = .GetFacturas(strCodEmpresa, strCodCliente, strCodUnidad, strNroDocumento, strFechaInicial, strFechaFinal, strNroDCR)
            If IsDBNull(dsFactura) Then
                Return ("Ocurrio un error")
                Exit Function
            End If
            Dim dvTTDOCU As DataView = fn_OBTE_DATA_VIEW_0001(dsFactura)
            Dim dvTTCOMP As DataView = fn_OBTE_DATA_VIEW_0002(dsFactura, dvTTDOCU)
            dvTCDOCU_CLIE = fn_OBTE_QUIE_0001(dsFactura.Tables(0).DefaultView, dvTTDOCU, dvTTCOMP)

            objGrid.DataSource = dvTCDOCU_CLIE
            objGrid.DataBind()
            System.Web.HttpContext.Current.Session.Add("dvRE_GENE", dvTCDOCU_CLIE)
        End With
    End Function

    Private Function fn_OBTE_DATA_VIEW_0002(ByVal dsTCDOCU_CLIE As DataSet, ByVal dvTTDOCU As DataView) As DataView
        Try
            Dim dtCOL1 As New DataTable
            Dim drCOL1 As DataRow
            Dim dvTCDOCU_CLIE As DataView = New DataView
            Dim drvTCDOCU_CLIE As DataRowView
            Dim drvTT_DOCU As DataRowView
            dvTCDOCU_CLIE = dsTCDOCU_CLIE.Tables(0).DefaultView
            dtCOL1.Columns.Add("CO_EMPR", GetType(System.String))
            dtCOL1.Columns.Add("CO_UNID", GetType(System.String))
            dtCOL1.Columns.Add("CO_CLIE", GetType(System.String))
            dtCOL1.Columns.Add("NU_DOCU", GetType(System.String))
            dtCOL1.Columns.Add("TI_COMP", GetType(System.String))
            dtCOL1.Columns.Add("NU_COMP", GetType(System.String))
            dtCOL1.Columns.Add("FE_INIC_PERI", GetType(System.String))
            dtCOL1.Columns.Add("FE_FINA_PERI", GetType(System.String))

            Dim sNU_COMP As String
            Dim sFE_INIC_PERI As String
            Dim sFE_FINA_PERI As String
            For Each drvTT_DOCU In dvTTDOCU
                dvTCDOCU_CLIE.RowFilter = "NU_DOCU='" & drvTT_DOCU("NU_DOCU") & "' "
                sNU_COMP = Nothing
                sFE_INIC_PERI = Nothing
                sFE_FINA_PERI = Nothing
                For Each drvTCDOCU_CLIE In dvTCDOCU_CLIE
                    If sNU_COMP <> drvTCDOCU_CLIE("NU_COMP") Or (sFE_INIC_PERI <> drvTCDOCU_CLIE("FE_INIC_PERI") And sFE_FINA_PERI <> drvTCDOCU_CLIE("FE_FINA_PERI")) Then
                        sNU_COMP = drvTCDOCU_CLIE("NU_COMP")
                        sFE_INIC_PERI = drvTCDOCU_CLIE("FE_INIC_PERI")
                        sFE_FINA_PERI = drvTCDOCU_CLIE("FE_FINA_PERI")
                        drCOL1 = dtCOL1.NewRow()
                        drCOL1("CO_EMPR") = drvTCDOCU_CLIE("CO_EMPR")
                        drCOL1("CO_UNID") = drvTCDOCU_CLIE("CO_UNID")
                        drCOL1("CO_CLIE") = drvTCDOCU_CLIE("CO_CLIE")
                        drCOL1("NU_DOCU") = drvTCDOCU_CLIE("NU_DOCU")
                        drCOL1("TI_COMP") = drvTCDOCU_CLIE("TI_COMP")
                        drCOL1("NU_COMP") = drvTCDOCU_CLIE("NU_COMP")
                        drCOL1("FE_INIC_PERI") = drvTCDOCU_CLIE("FE_INIC_PERI")
                        drCOL1("FE_FINA_PERI") = drvTCDOCU_CLIE("FE_FINA_PERI")
                        dtCOL1.Rows.Add(drCOL1)
                    End If
                Next
            Next
            Return dtCOL1.DefaultView
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Function

    Private Function fn_OBTE_DATA_VIEW_0001(ByVal dsTCDOCU_CLIE As DataSet) As DataView
        Try
            Dim dtCOL1 As New DataTable
            Dim drCOL1 As DataRow
            Dim dvTCDOCU_CLIE As DataView = New DataView
            Dim drvTCDOCU_CLIE As DataRowView
            dvTCDOCU_CLIE = dsTCDOCU_CLIE.Tables(0).DefaultView
            dtCOL1.Columns.Add("CO_EMPR", GetType(System.String))
            dtCOL1.Columns.Add("CO_UNID", GetType(System.String))
            dtCOL1.Columns.Add("CO_CLIE", GetType(System.String))
            dtCOL1.Columns.Add("DE_DOCU", GetType(System.String))
            dtCOL1.Columns.Add("NU_DOCU", GetType(System.String))
            Dim sNU_DOCU As String = Nothing
            For Each drvTCDOCU_CLIE In dvTCDOCU_CLIE
                If sNU_DOCU <> drvTCDOCU_CLIE("NU_DOCU") Then
                    sNU_DOCU = drvTCDOCU_CLIE("NU_DOCU")
                    drCOL1 = dtCOL1.NewRow()
                    drCOL1("CO_EMPR") = drvTCDOCU_CLIE("CO_EMPR")
                    drCOL1("CO_UNID") = drvTCDOCU_CLIE("CO_UNID")
                    drCOL1("CO_CLIE") = drvTCDOCU_CLIE("CO_CLIE")
                    drCOL1("DE_DOCU") = drvTCDOCU_CLIE("DE_DOCU")
                    drCOL1("NU_DOCU") = drvTCDOCU_CLIE("NU_DOCU")
                    dtCOL1.Rows.Add(drCOL1)
                End If
            Next
            Return dtCOL1.DefaultView
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Function

    'Private Function fn_OBTE_QUIE_0001(ByVal dvTCDOCU_CLIE As DataView, ByVal dvTTDOCU As DataView, ByVal dvTTCOMP As DataView) As DataView
    '    Try
    '        Dim drvTCDOCU_CLIE As DataRowView
    '        Dim drvTTDOCU As DataRowView
    '        Dim drvTTCOMP As DataRowView
    '        Dim drNU_REGI As DataRow
    '        Dim dtTCDOCU_CLIE As New DataTable
    '        dtTCDOCU_CLIE.Columns.Add("DE_DOCU", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("NU_DOCU", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("TI_COMP", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("NU_COMP", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("NU_SECU", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("NO_SIMB", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("MO_VALO", GetType(System.Decimal))
    '        dtTCDOCU_CLIE.Columns.Add("FE_INIC_PERI", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("FE_FINA_PERI", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("DE_TIPO_BULT", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("NU_UNID", GetType(System.Decimal))
    '        dtTCDOCU_CLIE.Columns.Add("MO_TARI", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("CO_UNME_PARA", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("IM_BRUT_AFEC", GetType(System.Decimal))
    '        dtTCDOCU_CLIE.Columns.Add("MO_TOTA", GetType(System.Decimal))
    '        dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("DE_MONE", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("CO_ALMA", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("FE_EMIS", GetType(System.String))
    '        dtTCDOCU_CLIE.Columns.Add("DE_SSER", GetType(System.String))

    '        Dim nMO_CANT_COMP, nMO_MERC_COMP, nIM_COMP, nMO_CANT_DOCU, nMO_MERC_DOCU, nIM_DOCU As Decimal
    '        Dim sNO_TIPO_DOCU As String = "FACTURA:"

    '        For Each drvTTDOCU In dvTTDOCU
    '            drNU_REGI = dtTCDOCU_CLIE.NewRow()
    '            drNU_REGI("DE_DOCU") = drvTTDOCU("DE_DOCU") & " : " & drvTTDOCU("NU_DOCU")
    '            drNU_REGI("NU_DOCU") = drvTTDOCU("NU_DOCU")
    '            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
    '            dvTTCOMP.RowFilter = "NU_DOCU='" & drvTTDOCU("NU_DOCU") & "' "
    '            nMO_CANT_DOCU = 0
    '            nMO_MERC_DOCU = 0
    '            nIM_DOCU = 0
    '            For Each drvTTCOMP In dvTTCOMP
    '                drNU_REGI = dtTCDOCU_CLIE.NewRow()
    '                drNU_REGI("TI_COMP") = "DOCUMENTO : " & drvTTCOMP("TI_COMP") & " - " & drvTTCOMP("NU_COMP")
    '                drNU_REGI("NU_COMP") = drvTTCOMP("NU_COMP")
    '                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
    '                dvTCDOCU_CLIE.RowFilter = "NU_DOCU='" & drvTTDOCU("NU_DOCU") & "' AND NU_COMP='" & drvTTCOMP("NU_COMP") & "'  AND FE_INIC_PERI='" & drvTTCOMP("FE_INIC_PERI") & "'  AND FE_FINA_PERI='" & drvTTCOMP("FE_FINA_PERI") & "' "
    '                nMO_CANT_COMP = 0
    '                nMO_MERC_COMP = 0
    '                nIM_COMP = 0
    '                For Each drvTCDOCU_CLIE In dvTCDOCU_CLIE
    '                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
    '                    drNU_REGI("NU_SECU") = drvTCDOCU_CLIE("NU_SECU")
    '                    drNU_REGI("FE_INIC_PERI") = drvTCDOCU_CLIE("FE_INIC_PERI")
    '                    drNU_REGI("FE_FINA_PERI") = drvTCDOCU_CLIE("FE_FINA_PERI")
    '                    drNU_REGI("DE_TIPO_BULT") = drvTCDOCU_CLIE("DE_TIPO_BULT")
    '                    drNU_REGI("NU_UNID") = Decimal.Round(CType(drvTCDOCU_CLIE("NU_UNID"), Decimal), 2)
    '                    drNU_REGI("NO_SIMB") = drvTCDOCU_CLIE("NO_SIMB")
    '                    drNU_REGI("MO_VALO") = Decimal.Round(CType(drvTCDOCU_CLIE("MO_VALO"), Decimal), 2)
    '                    drNU_REGI("DE_MERC") = drvTCDOCU_CLIE("DE_MERC")
    '                    drNU_REGI("MO_TARI") = CType(Decimal.Round(CType(drvTCDOCU_CLIE("MO_TARI"), Decimal), 4), String)
    '                    drNU_REGI("CO_UNME_PARA") = drvTCDOCU_CLIE("CO_UNME_PARA")
    '                    drNU_REGI("CO_MONE") = drvTCDOCU_CLIE("CO_MONE")
    '                    drNU_REGI("MO_TOTA") = Decimal.Round(CType(drvTCDOCU_CLIE("MO_TOTA"), Decimal), 2)
    '                    drNU_REGI("CO_ALMA") = drvTCDOCU_CLIE("CO_ALMA")
    '                    drNU_REGI("FE_EMIS") = drvTCDOCU_CLIE("FE_EMIS")
    '                    nMO_CANT_COMP = nMO_CANT_COMP + CType(drNU_REGI("NU_UNID"), Decimal)
    '                    nMO_MERC_COMP = nMO_MERC_COMP + CType(drNU_REGI("MO_VALO"), Decimal)
    '                    nIM_COMP = nIM_COMP + CType(drNU_REGI("MO_TOTA"), Decimal)
    '                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
    '                Next
    '                drNU_REGI = dtTCDOCU_CLIE.NewRow()
    '                drNU_REGI("DE_TIPO_BULT") = "TOT. DOCUMENTO:"
    '                drNU_REGI("NU_UNID") = Decimal.Round(nMO_CANT_COMP, 2)
    '                drNU_REGI("MO_VALO") = Decimal.Round(nMO_MERC_COMP, 2)
    '                'If Decimal.Round(nIM_COMP, 2) < drvTCDOCU_CLIE("IM_TARI_MINI") Then
    '                '    drNU_REGI("SI_MONE") = "TARIFA M�NIMA "
    '                '    nIM_COMP = drvTCDOCU_CLIE("IM_TARI_MINI")
    '                'End If
    '                drNU_REGI("MO_TOTA") = Decimal.Round(nIM_COMP, 2)
    '                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)

    '                nMO_CANT_DOCU = nMO_CANT_DOCU + nMO_CANT_COMP
    '                nMO_MERC_DOCU = nMO_MERC_DOCU + nMO_MERC_COMP
    '                nIM_DOCU = nIM_DOCU + nIM_COMP
    '            Next
    '            drNU_REGI = dtTCDOCU_CLIE.NewRow()
    '            drNU_REGI("DE_TIPO_BULT") = "TOT. " & sNO_TIPO_DOCU
    '            drNU_REGI("NU_UNID") = Decimal.Round(nMO_CANT_DOCU, 2)
    '            drNU_REGI("MO_VALO") = Decimal.Round(nMO_MERC_DOCU, 2)
    '            drNU_REGI("MO_TOTA") = Decimal.Round(nIM_DOCU, 2)
    '            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
    '        Next
    '        Return dtTCDOCU_CLIE.DefaultView
    '    Catch e1 As Exception
    '        Dim sCA_ERRO As String = e1.Message
    '    End Try
    'End Function

    Private Function fn_OBTE_QUIE_0001(ByVal dvTCDOCU_CLIE As DataView, ByVal dvTTDOCU As DataView, ByVal dvTTCOMP As DataView) As DataView
        Try
            Dim drvTCDOCU_CLIE As DataRowView
            Dim drvTTDOCU As DataRowView
            Dim drvTTCOMP As DataRowView
            Dim drNU_REGI As DataRow
            Dim dtTCDOCU_CLIE As New DataTable
            dtTCDOCU_CLIE.Columns.Add("CO_EMPR", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_UNID", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_CLIE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_DOCU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_DOCU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("TI_COMP", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_COMP", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_SECU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NO_SIMB", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("MO_VALO", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("FE_INIC_PERI", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_FINA_PERI", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_TIPO_BULT", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_UNID", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("MO_TARI", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_UNME_PARA", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("IM_BRUT_AFEC", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("MO_TOTA", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_MONE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_ALMA", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_EMIS", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_SSER", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FLG_XML", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_DOCU_XML", GetType(System.String))

            Dim nMO_CANT_COMP, nMO_MERC_COMP, nIM_COMP, nMO_CANT_DOCU, nMO_MERC_DOCU, nIM_DOCU As Decimal
            Dim sNO_TIPO_DOCU As String = "FACTURA:"

            For Each drvTTDOCU In dvTTDOCU
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                If (drvTTDOCU("NU_DOCU")).Substring(0, 1) = "F" Then
                    drNU_REGI("FLG_XML") = "1"
                    drNU_REGI("NU_DOCU_XML") = drvTTDOCU("NU_DOCU")
                End If
                drNU_REGI("DE_DOCU") = drvTTDOCU("DE_DOCU") & " : " & drvTTDOCU("NU_DOCU")
                drNU_REGI("NU_DOCU") = drvTTDOCU("NU_DOCU")
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                dvTTCOMP.RowFilter = "NU_DOCU='" & drvTTDOCU("NU_DOCU") & "' "
                nMO_CANT_DOCU = 0
                nMO_MERC_DOCU = 0
                nIM_DOCU = 0
                For Each drvTTCOMP In dvTTCOMP
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    'drNU_REGI("TI_COMP") = "DOCUMENTO : " & drvTTCOMP("TI_COMP") & " - " & drvTTCOMP("NU_COMP")
                    'drNU_REGI("NU_COMP") = drvTTCOMP("NU_COMP")
                    'dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    dvTCDOCU_CLIE.RowFilter = "NU_DOCU='" & drvTTDOCU("NU_DOCU") & "' AND NU_COMP='" & drvTTCOMP("NU_COMP") & "'  AND FE_INIC_PERI='" & drvTTCOMP("FE_INIC_PERI") & "'  AND FE_FINA_PERI='" & drvTTCOMP("FE_FINA_PERI") & "' "
                    nMO_CANT_COMP = 0
                    nMO_MERC_COMP = 0
                    nIM_COMP = 0
                    For Each drvTCDOCU_CLIE In dvTCDOCU_CLIE
                        drNU_REGI = dtTCDOCU_CLIE.NewRow()
                        drNU_REGI("CO_EMPR") = drvTTCOMP("CO_EMPR")
                        drNU_REGI("CO_UNID") = drvTTCOMP("CO_UNID")
                        drNU_REGI("CO_CLIE") = drvTTCOMP("CO_CLIE")
                        drNU_REGI("TI_COMP") = drvTTCOMP("TI_COMP")
                        drNU_REGI("NU_COMP") = drvTTCOMP("NU_COMP")
                        drNU_REGI("NU_SECU") = drvTCDOCU_CLIE("NU_SECU")
                        drNU_REGI("DE_SSER") = drvTCDOCU_CLIE("DE_SSER")
                        drNU_REGI("FE_INIC_PERI") = drvTCDOCU_CLIE("FE_INIC_PERI")
                        drNU_REGI("FE_FINA_PERI") = drvTCDOCU_CLIE("FE_FINA_PERI")
                        drNU_REGI("DE_TIPO_BULT") = drvTCDOCU_CLIE("DE_TIPO_BULT")
                        drNU_REGI("NU_UNID") = Decimal.Round(CType(drvTCDOCU_CLIE("NU_UNID"), Decimal), 2)
                        drNU_REGI("NO_SIMB") = drvTCDOCU_CLIE("NO_SIMB")
                        drNU_REGI("MO_VALO") = Decimal.Round(CType(drvTCDOCU_CLIE("MO_VALO"), Decimal), 2)
                        drNU_REGI("DE_MERC") = drvTCDOCU_CLIE("DE_MERC")
                        drNU_REGI("MO_TARI") = CType(Decimal.Round(CType(drvTCDOCU_CLIE("MO_TARI"), Decimal), 4), String)
                        drNU_REGI("CO_UNME_PARA") = drvTCDOCU_CLIE("CO_UNME_PARA")
                        drNU_REGI("CO_MONE") = drvTCDOCU_CLIE("CO_MONE")
                        drNU_REGI("IM_BRUT_AFEC") = Decimal.Round(CType(drvTCDOCU_CLIE("IM_BRUT_AFEC"), Decimal), 2)
                        drNU_REGI("MO_TOTA") = Decimal.Round(CType(drvTCDOCU_CLIE("MO_TOTA"), Decimal), 2)
                        drNU_REGI("CO_ALMA") = drvTCDOCU_CLIE("CO_ALMA")
                        drNU_REGI("FE_EMIS") = drvTCDOCU_CLIE("FE_EMIS")
                        nMO_CANT_COMP = nMO_CANT_COMP + CType(drNU_REGI("NU_UNID"), Decimal)
                        nMO_MERC_COMP = nMO_MERC_COMP + CType(drNU_REGI("MO_VALO"), Decimal)
                        nIM_COMP = nIM_COMP + CType(drNU_REGI("MO_TOTA"), Decimal)
                        dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    Next
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("DE_TIPO_BULT") = "TOT. DOCUMENTO:"
                    drNU_REGI("NU_UNID") = Decimal.Round(nMO_CANT_COMP, 2)
                    drNU_REGI("MO_VALO") = Decimal.Round(nMO_MERC_COMP, 2)
                    'If Decimal.Round(nIM_COMP, 2) < drvTCDOCU_CLIE("IM_TARI_MINI") Then
                    '    drNU_REGI("SI_MONE") = "TARIFA M�NIMA "
                    '    nIM_COMP = drvTCDOCU_CLIE("IM_TARI_MINI")
                    'End If
                    'drNU_REGI("MO_TOTA") = Decimal.Round(nIM_COMP, 2)
                    'dtTCDOCU_CLIE.Rows.Add(drNU_REGI)

                    nMO_CANT_DOCU = nMO_CANT_DOCU + nMO_CANT_COMP
                    nMO_MERC_DOCU = nMO_MERC_DOCU + nMO_MERC_COMP
                    nIM_DOCU = nIM_DOCU + nIM_COMP
                Next
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("DE_TIPO_BULT") = "TOT. " & sNO_TIPO_DOCU
                drNU_REGI("NU_UNID") = Decimal.Round(nMO_CANT_DOCU, 2)
                drNU_REGI("MO_VALO") = Decimal.Round(nMO_MERC_DOCU, 2)
                drNU_REGI("MO_TOTA") = Decimal.Round(nIM_DOCU, 2)
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            Next
            Return dtTCDOCU_CLIE.DefaultView
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Function

    Public Function gCADGetDocumentoDetalleXML(ByVal strco_empr As String, ByVal strco_unid As String, ByVal strti_docu As String, _
                                  ByVal strnu_docu As String, ByVal strco_clie As String) As DataTable
        objFactura = New clsCADFactura
        With objFactura
            Return objFactura.gCADGetDocumentoDetalleXML(strco_empr, strco_unid, strti_docu, strnu_docu, strco_clie)
        End With
    End Function

    Public Function fn_GetDetalleDCR(ByVal sCO_EMPR As String, ByVal sNU_DOCU_RECE As String) As DataSet
        objFactura = New clsCADFactura
        Return objFactura.GetDetalleDCR(sCO_EMPR, sNU_DOCU_RECE)
    End Function

    Public Function fn_listarUnidades(ByVal sCO_EMPR As String) As DataSet
        objFactura = New clsCADFactura
        Return objFactura.fn_listarUnidades(sCO_EMPR)
    End Function

    Public Function fn_GetFactura(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                            ByVal sCoGrup As String, ByVal sTI_DOC As String, _
                                            ByVal sNU_FACT As String) As DataTable
        objFactura = New clsCADFactura
        Return objFactura.fn_GetFactura(sCO_EMPR, sCO_UNID, sCoGrup, sTI_DOC, sNU_FACT).Tables(0)
    End Function

    Public Function fn_GetOrdenTrabajo(ByVal sCO_EMPR As String, ByVal sCO_UNID As String, _
                                        ByVal sTI_DOC As String, ByVal sNU_DOC As String, _
                                        ByVal sCO_CLIE As String) As DataTable
        objFactura = New clsCADFactura
        Return objFactura.fn_GetOrdenTrabajo(sCO_EMPR, sCO_UNID, sTI_DOC, sNU_DOC, sCO_CLIE).Tables(0)
    End Function

    Public Function fn_GetBoleta(ByVal sTipoCon As String, ByVal strTipoDoc As String, ByVal strCorrelativo As String, ByVal strSerie As String, ByVal strFechaInicial As String, ByVal strMonto As String) As DataTable
        objFactura = New clsCADFactura
        Return objFactura.fn_GetBoleta(sTipoCon, strTipoDoc, strCorrelativo, strSerie, strFechaInicial, strMonto)
    End Function

    Protected Overrides Sub Finalize()
        If Not (objFactura Is Nothing) Then
            objFactura = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
