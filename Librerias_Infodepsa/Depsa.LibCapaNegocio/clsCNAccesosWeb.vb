Imports Depsa.LibCapaAccesoDatos
Public Class clsCNAccesosWeb
    Private pdtTemporal As DataTable
    Private pstrresultado As String

    Public Function fn_DevuelveDataTable() As DataTable
        Return pdtTemporal
    End Function

    Public Function fn_DevuelveResultado() As String
        Return pstrresultado
    End Function

    Public Sub gCNMostrarAccesosWeb(ByVal strfchini As String, ByVal strfchfin As String, ByVal strco_usua As String)
        Dim objAcceso As New clsCADAccesosWeb
        objAcceso.gCADMostrarAccesosWeb(strfchini, strfchfin, strco_usua)
        pdtTemporal = objAcceso.fn_devuelveDataTable
    End Sub

    Public Sub gCNMostrarLogueos(ByVal strfchini As String, ByVal strfchfin As String, ByVal strco_usua As String)
        Dim objAcceso As New clsCADAccesosWeb
        objAcceso.gCADMostrarLogueos(strfchini, strfchfin, strco_usua)
        pdtTemporal = objAcceso.fn_devuelveDataTable
    End Sub
    Public Function gCNInsAccesosWeb(ByVal sCO_USUA As String, ByVal sTI_ENTI_TRAB As String,
                                                    ByVal sCO_ENTI_TRAB As String, ByVal sNO_ENTI_TRAB As String,
                                                    ByVal sTI_MENU As String, ByVal sCO_MENU As String,
                                                    ByVal sNO_MENU As String, ByVal sIP_PETI As String,
                                                    ByVal sIP_REAL As String) As String
        Dim objAcceso As New clsCADAccesosWeb
        If objAcceso.gCADInsAccesosWeb(sCO_USUA, sTI_ENTI_TRAB, sCO_ENTI_TRAB, sNO_ENTI_TRAB, sTI_MENU, sCO_MENU, sNO_MENU, sIP_PETI, sIP_REAL) = 1 Then
            Return ""
        Else
            Return "Fallo el registro de acceso"
        End If
    End Function

    Public Function gCNMostrarDatosUsuario(ByVal strCod_user As String, strCod_sico As String) As DataTable
        Dim objAcceso As New clsCADAccesosWeb
        Return objAcceso.gCADMostrarDatosUsuario(strCod_user, strCod_sico)
    End Function

End Class
