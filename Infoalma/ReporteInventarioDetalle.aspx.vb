﻿Imports DEPSA.LibCapaNegocio
Imports System.Text
Imports System.IO
Imports System.Data
Imports Infodepsa
Partial Class ReporteInventarioDetalle
    Inherits System.Web.UI.Page
    Private objInventario As New clsCNInventario
    Dim dt As DataTable
    Dim dtAlmacen As DataTable
    Dim dtDetalle As DataTable
    Dim sValidaVacios As Boolean

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "43") Then
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "40"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Detalle del Inventario"
                Me.btnExportar.Attributes.Add("onclick", "javascript:if(confirm('¿Está seguro de exportar el documento?')== false) return false;")

                ListaClientes()
                ListarAlmacen()
                ListaUnidadMedida()
                Bindatagrid()

            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ListaClientes()
        Try
            Me.cboCliente.Items.Clear()
            dt = objInventario.gCNGetListarClienteInventario(Session.Item("IdSico"))
            Me.cboCliente.Items.Add(New ListItem("TODOS", "0"))
            For Each dr As DataRow In dt.Rows
                Me.cboCliente.Items.Add(New ListItem(dr("Nombre").ToUpper(), dr("Codigo")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Private Sub ListarAlmacen()
        Try
            Me.cboAlmacen.Items.Clear()
            dtAlmacen = objInventario.gCNgetlistaralmacen(Me.cboCliente.SelectedValue)
            Me.cboAlmacen.Items.Add(New ListItem("TODOS", "0"))
            For Each dr As DataRow In dtAlmacen.Rows
                Me.cboAlmacen.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Private Sub ListaUnidadMedida()
        Try
            Me.cboUnidadMedida.Items.Clear()
            dt = objInventario.gCNListarUnidadMedida()
            For Each dr As DataRow In dt.Rows
                Me.cboUnidadMedida.Items.Add(New ListItem(dr("DE_UNME").ToString(), dr("CO_UNME")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub ListarConductor()
        Try
            Me.lblConductor.Text = objInventario.gCNGetListarConductor(Me.cboAlmacen.SelectedValue)
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub Bindatagrid()
        Try

            ListaClientes()
            ListarAlmacen()
            If Session.Item("TIP_USUA") = "SUPER" Then
                Me.txtDCR.Enabled = True
                Me.txtWarrant.Enabled = True
                Me.ChckConfPeso.Enabled = True
                gvTraders.Columns(19).Visible = False

                Me.cboCliente.Enabled = False
                Me.cboAlmacen.Enabled = False
                Me.cboUnidadMedida.Enabled = False
                Me.txtEmprTransporte.Enabled = False
                Me.txtMercaderia.Enabled = False
                Me.txtCantSolicitada.Enabled = False
                'Me.txtTempIngreso.Disabled = True
                'Me.txtHumedadIngreso.Disabled = True
                Me.txtVapor.Enabled = False
                Me.lblConductor.Enabled = False
                Me.txtDua.Enabled = False
            End If


            If Session("NU_DOCU_INVE") IsNot Nothing Then 'nuevo documento
                dt = objInventario.gCNListarInventarioXDocumento(Session.Item("NU_DOCU_INVE"))
                Me.lblTrader.Text = Session.Item("NU_DOCU_INVE")
                Me.cboCliente.SelectedValue = CType(dt.Rows(0)("CO_CLIE"), String).Trim
                ListarAlmacen()
                Me.cboAlmacen.SelectedValue = CType(dt.Rows(0)("CO_ALMA"), String).Trim
                Me.lblConductor.Text = CType(dt.Rows(0)("DE_COND_LOCA"), String).Trim
                Me.txtEmprTransporte.Text = CType(dt.Rows(0)("DE_TRAN"), String).Trim
                Me.txtMercaderia.Text = CType(dt.Rows(0)("DE_MERC"), String).Trim
                txtCantSolicitada.Text = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_PESO_SOLI")))
                'Me.txtTempIngreso.Value = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_TEMP_INGR")))
                'Me.txtHumedadIngreso.Value = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_HUME_INGR")))
                Me.txtFechaIniRecep.Value = CType(dt.Rows(0)("FE_INI_RECE"), String).Trim
                Me.txtFechaFinRecep.Value = CType(dt.Rows(0)("FE_FINA_RECE"), String).Trim
                Me.txtVapor.Text = CType(dt.Rows(0)("DE_VAPO"), String).Trim
                Me.txtDua.Text = CType(dt.Rows(0)("NU_DUA"), String).Trim
                Me.txtDCR.Text = CType(dt.Rows(0)("NU_DOCU_RECE"), String).Trim
                Me.txtWarrant.Text = CType(dt.Rows(0)("NU_TITU"), String).Trim
                Me.cboUnidadMedida.SelectedValue = CType(dt.Rows(0)("CO_UNME"), String).Trim
                Me.lblEstado.Text = CType(dt.Rows(0)("DE_ESTA"), String).Trim
                Me.ChckConfPeso.Checked = CType(dt.Rows(0)("ST_CONF_PESO"), String).Trim
                If CType(dt.Rows(0)("CO_ESTA"), String).Trim = "1" Or CType(dt.Rows(0)("CO_ESTA"), String).Trim = "2" Or CType(dt.Rows(0)("CO_ESTA"), String).Trim = "X" Then

                    Me.cboCliente.Enabled = False
                    Me.cboAlmacen.Enabled = False
                    Me.cboUnidadMedida.Enabled = False
                    Me.txtEmprTransporte.Enabled = False
                    Me.txtMercaderia.Enabled = False
                    Me.txtCantSolicitada.Enabled = False
                    'Me.txtTempIngreso.Disabled = True
                    Me.txtFechaIniRecep.Disabled = False
                    Me.txtFechaFinRecep.Disabled = False
                    'Me.txtHumedadIngreso.Disabled = True
                    Me.txtVapor.Enabled = False
                    Me.lblConductor.Enabled = False
                    Me.txtDua.Enabled = False
                    gvTraders.Columns(19).Visible = False

                    Me.ChckConfPeso.Enabled = False

                End If
                dtDetalle = objInventario.gCNListarInventarioDetalleCardex(Session.Item("NU_DOCU_INVE"))

                gvTraders.Columns(1).Visible = True
                Me.gvTraders.DataSource = dtDetalle
                Me.gvTraders.DataBind()
                Session.Add("dtINVENT", dtDetalle)

                gvTraders.Columns(1).Visible = False
                gvTraders.Columns(18).Visible = False
                gvTraders.Columns(19).Visible = False
                'Dim strNroSecuencia1 As String

                'Dim dgItem1 As GridViewRow
                ''For i As Integer = Me.gvTraders.Rows.Count To Me.gvTraders.Rows.Count - 1
                'dgItem1 = Me.gvTraders.Rows(((dtDetalle.Rows.Count) - 1))
                ''strNroSecuencia1 = dgItem1.Cells(0).Text()

                'CType(dgItem1.FindControl("imgAnular"), ImageButton).ImageUrl = "~/Images/AzulOscuro.gif"
                'CType(dgItem1.FindControl("imgAnular"), ImageButton).Enabled = False
                'CType(dgItem1.FindControl("chkCerrar"), CheckBox).Enabled = False
                ''CType(dgItem1.FindControl("imgFinalizar"), ImageButton).ImageUrl = "~/Images/transparente.png"
                ''CType(dgItem1.FindControl("imgFinalizar"), ImageButton).Enabled = False

                'Next i
                Me.lblError.Text = "Total de registros: " & ((dtDetalle.Rows.Count) - 1)

            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("ReporteInventario.aspx")
    End Sub
    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        ListarConductor()
    End Sub

    Private Sub gvTraders_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvTraders.ItemDataBound


        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            ' CType(e.Item.FindControl("imgAnular"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('¿Está seguro de anular el registro?')== false) return false;")
            '   CType(e.Row.FindControl("imgFinalizar"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('¿Está seguro de cerrar el registro?')== false) return false;")

            'documento finalizado
            If e.Item.DataItem("CO_ESTA_FINA").ToString.Trim = "0" Then
                Dim dtext As String
                dtext = "inventario"
            Else
                e.Item.BackColor = System.Drawing.Color.FromName("#c8dcf0")
            End If

            'anulados
            If e.Item.DataItem("CO_ESTA_ANUL").ToString = "1" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#dcdcdc")
            End If


            'header inferior de la grilla
            If e.Item.DataItem("NU_DOCU_CLIE").ToString = "Total :" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#5D7B9D")
                e.Item.ForeColor = System.Drawing.Color.White
                e.Item.Font.Bold = True
            End If

        End If
    End Sub

    Protected Sub cboCliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliente.SelectedIndexChanged
        ListarAlmacen()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim page As Page = New Page
        Dim form As HtmlForm = New HtmlForm
        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtINVENT"), DataTable)
        dg.DataBind()
        dg.EnableViewState = False
        page.DesignerInitialize()
        page.Controls.Add(form)
        form.Controls.Add(dg)
        page.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=ServInventarioDetalle.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
        dg = Nothing
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

End Class
