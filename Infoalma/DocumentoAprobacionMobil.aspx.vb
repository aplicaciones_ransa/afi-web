Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class DocumentoAprobacionMobil
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strEndosoPath = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strConn As String = ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathScanflow = ConfigurationManager.AppSettings.Item("RutaScanflow")
    Private objLiberacion As Liberacion = New Liberacion
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    Private objReportes As Reportes = New Reportes
    Dim var As Integer
    Dim strIdUsuario As String
    Dim strNroDoc As String
    Dim strNomEnti As String
    Dim strNroLib As String
    Dim strPeriodoAnual As String
    Dim strIdTipoDocumento As String
    Dim strIdTipoEntidad As String
    Dim strIdTipoUsuario As String
    Dim strIdSico As String
    Dim strcontrasenia As String
    Dim strstrfecha As String
    Dim strCertDigital As String
    Dim strCod_TipOtro As String
    Dim strCod_Doc As String
    Dim strDscMerc As String
    Dim strCod_Unidad As String
    Dim strNavMin As Integer
    Dim strNavSel As Integer
    Dim strNavMax As Integer

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents hlkDescripcion As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboTipoDocumento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboSituacion As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If


        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                If Session.Item("strMensaje") <> Nothing Then
                    pr_IMPR_MENS(Session.Item("strMensaje"))
                    Session.Remove("strMensaje")
                End If
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime
        If Request.QueryString("var2") = 1 Then
            objDocumento.gUpdAccesoFirmar(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"))
        End If
        If Request.Item("Estado") <> "" Then
            Session.Add("EstadoDoc", Request.Item("Estado"))
        End If
        Dim strResult As String()
        Select Case Session.Item("EstadoDoc") 'Request.Item("Estado") 
            'Case "01"
            '    Me.lblOpcion.Text = "Consultar Documentos Creados"
            '    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
            Case "02"
                Me.lblOpcion.Text = "Consultar Documentos Emitidos"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "8"), "-")
            Case "03"
                Me.lblOpcion.Text = "Consultar Documentos Endosados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "9"), "-")
            Case "04"
                Me.lblOpcion.Text = "Consultar Documentos Registrados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "10"), "-")
                'Case "05"
                '    Me.lblOpcion.Text = "Consultar Documentos Anulados"
                '    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
            Case "06"
                Me.lblOpcion.Text = "Consultar Documentos Rechazados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "12"), "-")
        End Select
        Session.Item("Page") = strResult(0)
        Session.Item("Opcion") = strResult(1)
        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(180))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        loadTipoDocumento()
        loadDepositante()
        loadFinanciador()
        InicializaBusqueda()
        If Session.Item("IdTipoEntidad") = "03" Then
            Me.cboDepositante.SelectedValue = Session.Item("IdSico")
            Me.cboDepositante.Enabled = False
        End If
        If Session.Item("IdTipoEntidad") = "02" Then
            Me.cboFinanciador.SelectedValue = Session.Item("IdSico")
            Me.cboFinanciador.Enabled = False
        End If
        BindDatagrid()
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusqueda") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusqueda").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.txtNroWarrant.Text = strFiltros(0)
            txtFechaInicial.Value = strFiltros(1)
            txtFechaFinal.Value = strFiltros(2)
            Me.cboTipoDocumento.SelectedValue = strFiltros(3)
            Me.cboDepositante.SelectedValue = strFiltros(4)
            Me.cboFinanciador.SelectedValue = strFiltros(5)
            Me.cboSituacion.SelectedValue = strFiltros(6)
        End If
    End Sub

    Private Sub loadDepositante()
        Dim dtEntidad As DataTable
        Try
            Me.cboDepositante.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("03", "")
            Me.cboDepositante.Items.Add(New ListItem("---------------Todos---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositante.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR:  " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadFinanciador()
        Dim dtEntidad As DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "")
            Me.cboFinanciador.Items.Add(New ListItem("---------------Todos---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub loadTipoDocumento()
        objfunciones.GetTipoDocumento(Me.cboTipoDocumento, 1)
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objDocumento.gGetBusquedaDocumentos(Me.txtNroWarrant.Text.Replace("'", ""), FechaInicial, FechaFinal,
            Session.Item("EstadoDoc"), Me.cboTipoDocumento.SelectedValue, Session.Item("IdTipoEntidad"), Session.Item("IdSico"),
            Me.cboDepositante.SelectedValue, Me.cboFinanciador.SelectedValue, Me.cboSituacion.SelectedValue, Session.Item("IdUsuario"))
            Session.Add("dtDocumentoAprobacion", dt)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            If Session.Item("IdTipoEntidad") = "01" Then
                Me.dgdResultado.Columns(6).Visible = True
                Me.dgdResultado.Columns(7).Visible = True
            ElseIf Session.Item("IdTipoEntidad") = "03" Then
                Me.dgdResultado.Columns(6).Visible = True
            ElseIf Session.Item("IdTipoEntidad") = "02" Then
                Me.dgdResultado.Columns(7).Visible = True
            End If
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Try
        '    Dim imgEditar As ImageButton = CType(sender, ImageButton)
        '    Dim dgi As DataGridItem
        '    dgi = CType(imgEditar.Parent.Parent, DataGridItem)
        '    Session.Add("NroDoc", dgi.Cells(1).Text())
        '    Session.Add("NroLib", dgi.Cells(17).Text())
        '    Session.Add("IdTipoDocumento", dgi.Cells(3).Text())
        '    Session.Add("PeriodoAnual", dgi.Cells(14).Text())
        '    Session.Add("Cod_TipOtro", dgi.Cells(15).Text())
        '    Session.Add("Cod_Doc", dgi.Cells(18).Text())
        '    Session.Add("Cod_Unidad", dgi.Cells(19).Text())

        '    Response.Redirect("DocumentoPagDos.aspx", False)
        'Catch ex As Exception
        '    Me.lblError.Text = "ERROR " + ex.Message
        'End Try
        '----------------------------------------------------------

        Try
            Dim NombreReporte As String
            Dim strError As String = ""
            'Dim dgItem As DataGridItem
            Dim satValida As Boolean = False
            Dim strCont As Integer = 1
            Dim strCadCodigoDocu As String
            Dim dtIngreso001 As DataTable = New DataTable

            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)

            '=== Crear Tabla Temporal ===
            If Not dtIngreso001.Rows.Count = 0 Then
                dtIngreso001.Rows.Clear()
            Else
                dtIngreso001 = crearTabla()
            End If

            '=== Concatenar numero de documento y tipo de documento. ===
            If strCont = 1 Then
                strCadCodigoDocu = "" & dgi.Cells(1).Text.ToString & dgi.Cells(3).Text.ToString
            Else
                strCadCodigoDocu = "," & dgi.Cells(1).Text.ToString & dgi.Cells(3).Text.ToString
            End If

            '=== Asignando Numero de reporte ===
            NombreReporte = strEndosoPath & dgi.Cells(3).Text.ToString & dgi.Cells(1).Text() & dgi.Cells(17).Text() & ".pdf"

            '=== llenar datos a las variables
            var = 0
            strNroDoc = "" : strNomEnti = "" : strNroLib = "" : strIdTipoDocumento = "" : strPeriodoAnual = "" : strCod_TipOtro = "" : strCod_Doc = ""
            strDscMerc = "" : strCod_Unidad = ""
            strIdUsuario = ""

            strNroDoc = dgi.Cells(1).Text()
            strNomEnti = dgi.Cells(7).Text()
            strNroLib = dgi.Cells(17).Text()
            strIdTipoDocumento = dgi.Cells(3).Text()
            strPeriodoAnual = dgi.Cells(14).Text()
            strCod_TipOtro = dgi.Cells(15).Text()
            strCod_Doc = dgi.Cells(18).Text()
            strDscMerc = dgi.Cells(20).Text()
            strCod_Unidad = dgi.Cells(19).Text()
            strIdUsuario = Session.Item("IdUsuario")

            '=== cargar Datos a Tabla Temporal ===
            Dim dr As DataRow
            dr = dtIngreso001.NewRow
            dr(0) = strCont
            dr(1) = strNroDoc
            dr(2) = strNomEnti
            dr(3) = strNroLib
            dr(4) = strIdTipoDocumento
            dr(5) = strPeriodoAnual
            dr(6) = strCod_TipOtro
            dr(7) = strCod_Doc
            dr(8) = strDscMerc
            dr(9) = strCod_Unidad
            dtIngreso001.Rows.Add(dr)

            '=== Copiar Documentos al Filemaster ==
            'Si el documento esta en la opcion del menu "Registrado" EstadoDoc = "04" 
            If Session.Item("EstadoDoc") = "04" Then
                Dim dtProrroga As DataTable
                Dim objConexion As SqlConnection
                Dim objTrans As SqlTransaction
                Dim strIds As String
                Dim dtDetalle As DataTable
                objConexion = New SqlConnection(strConn)
                objConexion.Open()
                objTrans = objConexion.BeginTransaction()
                Try
                    If strIdTipoDocumento = "02" Or strIdTipoDocumento = "13" Or strIdTipoDocumento = "14" Then
                        If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                            objTrans.Commit()
                            objTrans.Dispose()
                            Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                            objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                            objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                            Exit Try
                        End If

                        dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCod_Unidad, strNroLib, strCod_TipOtro, strNroDoc, objTrans)
                        objLiberacion.GetReporteLiberacionFirmas(NombreReporte, strEmpresa, strCod_Unidad, "DOR", dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"), dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), strIdTipoDocumento, objTrans, strPathFirmas) 'Server.MapPath(strPathFirmas)
                        GrabaDocumento(NombreReporte, objTrans)
                        objDocumento.gInsLogDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, "I", strIdTipoDocumento, objTrans)
                        dtDetalle.Dispose()
                        objTrans.Commit()
                        objTrans.Dispose()
                        strError = "X"
                    ElseIf strIdTipoDocumento = "05" Then
                        If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                            objTrans.Commit()
                            objTrans.Dispose()
                            Exit Try
                        End If

                        objfunciones.GetReporteProrrogaFolio(NombreReporte, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strPathFirmas, dgi.Cells(7).Text(), dgi.Cells(6).Text(), objTrans) 'Server.MapPath(strPathFirmas)
                        GrabaDocumento(NombreReporte, objTrans)
                        objTrans.Commit()
                        objTrans.Dispose()
                        strError = "X"
                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                    ElseIf strIdTipoDocumento = "20" Then
                        objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans)
                        objTrans.Commit()
                        objTrans.Dispose()
                    ElseIf strIdTipoDocumento = "03" Or strIdTipoDocumento = "10" Or strIdTipoDocumento = "12" Then
                        If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                            objTrans.Commit()
                            objTrans.Dispose()
                            Exit Try
                        End If
                        dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans)
                        For i As Integer = 0 To dtProrroga.Rows.Count - 1
                            If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                                objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans)
                            Else
                                strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                                objReportes.GeneraPDFWarrantFirmasXDocumento(strCod_Doc, dtProrroga.Rows(i)("COD_DOC"), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, dtProrroga.Rows(i)("NOM_PDF"), objTrans)
                            End If
                        Next

                        If strIds = "" Then
                            objTrans.Rollback()
                            objTrans.Dispose()
                            pr_IMPR_MENS("No se encontr� el warrant relacionado a este documento")
                            Exit Sub
                        End If
                        If strIdTipoDocumento <> "12" Then
                            objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans)
                        End If
                        objTrans.Commit()
                        objTrans.Dispose()
                        strError = "X"

                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaRegistrados(strCod_Doc)
                    ElseIf strIdTipoDocumento = "09" Or strIdTipoDocumento = "17" Or strIdTipoDocumento = "18" Or strIdTipoDocumento = "19" Then
                        objReportes.GeneraDCR(strCod_Doc, strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans)
                        objTrans.Commit()
                        objTrans.Dispose()
                    ElseIf strIdTipoDocumento = "11" Or strIdTipoDocumento = "15" Or strIdTipoDocumento = "16" Then
                        If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                            objTrans.Commit()
                            objTrans.Dispose()
                            Exit Try
                        End If

                        dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCod_Unidad, strNroLib, strCod_TipOtro, strNroDoc, objTrans)
                        objLiberacion.GetReporteLiberacionFirmas(NombreReporte, strEmpresa, strCod_Unidad, "DOR", dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"), dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), strIdTipoDocumento, objTrans, strPathFirmas) 'Server.MapPath(strPathFirmas)
                        GrabaDocumento(NombreReporte, objTrans)
                        objDocumento.gInsLogDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, "I", strIdTipoDocumento, objTrans)
                        dtDetalle.Dispose()
                        objTrans.Commit()
                        objTrans.Dispose()
                        strError = "X"
                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                    End If
                Catch ex As Exception
                    If strError = "" Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                    End If
                    Me.lblError.Text = " Se cancel� el proceso debido al siguiente error:  " & ex.ToString
                    Exit Sub
                End Try
            ElseIf Session.Item("EstadoDoc") = "02" And strIdTipoDocumento = "12" Then
                If objDocumento.gGetEstadoImpresionEmitido(strCod_Doc) = False Then
                    Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                    objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                    objWSFileMaster.CopiaDocumentosInfodepsaCreados(strCod_Doc)
                End If
            End If
            If objDocumento.gGetEstadoImpresionCreados(strCod_Doc) = False Then
                '-------------------------------------------------------------------
                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster.CopiaDocumentosInfodepsaCreados(strCod_Doc)
            End If
            '--------------------------------------------
            Session.Add("dtIngreso001", dtIngreso001)
            Response.Redirect("DocumentoPDF.aspx?var=1", False)



            '------------------------------------------------------
            'Session.Add("NroDoc", dgi.Cells(1).Text())
            'Session.Add("NroLib", dgi.Cells(17).Text())
            'Session.Add("IdTipoDocumento", dgi.Cells(3).Text())
            'Session.Add("PeriodoAnual", dgi.Cells(14).Text())
            'Session.Add("Cod_TipOtro", dgi.Cells(15).Text())
            'Session.Add("Cod_Doc", dgi.Cells(18).Text())
            'Session.Add("Cod_Unidad", dgi.Cells(19).Text())
            ''Response.Redirect("DocumentoPagDos.aspx", False)
            ''Response.Redirect("DocumentoPagDos.aspx?Pag=0", False)

            'Response.Redirect("DocumentoPagUnoWarrant.aspx?var=1", False)
            '---------------------------------------------------------
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try

    End Sub

    Private Sub GrabaDocumento(ByVal strNombArch As String, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strNombArch)
        FilePath = strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegEndosoFolio(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strNombre, Contenido, ObjTrans)
        fs.Close()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusqueda", Me.txtNroWarrant.Text.Replace("'", "") & "-" &
        txtFechaInicial.Value & "-" & txtFechaFinal.Value & "-" & Me.cboTipoDocumento.SelectedValue & "-" &
        Me.cboDepositante.SelectedValue & "-" & Me.cboFinanciador.SelectedValue & "-" & Me.cboSituacion.SelectedValue)
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            'CType(e.Item.FindControl("chkSel"), CheckBox).Enabled = False  --comentado por prueba

            If e.Item.DataItem("COD_TIPDOC") = "11" Or e.Item.DataItem("COD_TIPDOC") = "15" Or e.Item.DataItem("COD_TIPDOC") = "16" Then
                e.Item.BackColor = System.Drawing.Color.Thistle
            ElseIf e.Item.DataItem("LUG_ENDO") = 0 Then
                e.Item.BackColor = System.Drawing.Color.White
            Else
                e.Item.BackColor = System.Drawing.Color.LightGoldenrodYellow
            End If
            e.Item.Cells(9).Text = e.Item.DataItem("CO_MONE") & String.Format("{0:##,##0.00}", e.Item.DataItem("IM_RETI"))
            If Left(e.Item.DataItem("FLG_ALMA").ToString.Trim, 4) = Left(Session.Item("IdSico"), 4) And Session.Item("IdTipoEntidad") = "02" Then
                e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                CType(e.Item.FindControl("chkSel"), CheckBox).Enabled = True
            End If
            If e.Item.DataItem("FLG_ALMA").ToString.Trim = Session.Item("IdSico") And (Session.Item("IdTipoEntidad") = "01" Or Session.Item("IdTipoEntidad") = "03") Then
                e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                CType(e.Item.FindControl("chkSel"), CheckBox).Enabled = True
            End If
            If e.Item.DataItem("st_sali_alma").ToString.Trim = "S" Then
                e.Item.BackColor = System.Drawing.Color.FromArgb(173, 213, 230)
                'CType(e.Item.FindControl("chkSel"), CheckBox).Enabled = False --comentado por prueba
            End If
            If e.Item.DataItem("TI_SITU").ToString.Trim = "ANU" Then
                e.Item.BackColor = System.Drawing.Color.Silver
            End If
        End If
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objfunciones Is Nothing) Then
            objfunciones = Nothing
        End If
        If Not (objLiberacion Is Nothing) Then
            objLiberacion = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgSeguimiento As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgSeguimiento.DataSource = Session.Item("dtDocumentoAprobacion")
        dgSeguimiento.DataBind()
        dgSeguimiento.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub
    Protected Sub btnFirmar_Click(sender As Object, e As EventArgs) Handles btnFirmar.Click

        Try
            Dim NombreReporte As String
            Dim strError As String = ""
            Dim dgItem As DataGridItem
            Dim satValida As Boolean = False
            Dim strCont As Integer = 1
            Dim strCadCodigoDocu As String
            Dim dtIngreso001 As DataTable = New DataTable

            '=== Crear Tabla Temporal ===
            If Not dtIngreso001.Rows.Count = 0 Then
                dtIngreso001.Rows.Clear()
            Else
                dtIngreso001 = crearTabla()
            End If

            'Session.Add("dtIngreso001", crearTabla)
            '    
            'End If
            'crearSessionTabla()

            For Each dgItem In dgdResultado.Items
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True And CType(dgItem.FindControl("chkSel"), CheckBox).Enabled = True Then
                    satValida = True
                    '=== Concatenar numero de documento y tipo de documento. ===
                    If strCont = 1 Then
                        strCadCodigoDocu = "" & dgItem.Cells(1).Text.ToString & dgItem.Cells(3).Text.ToString
                    Else
                        strCadCodigoDocu = "," & dgItem.Cells(1).Text.ToString & dgItem.Cells(3).Text.ToString
                    End If

                    '=== Asignando Numero de reporte ===
                    NombreReporte = strEndosoPath & dgItem.Cells(3).Text.ToString & dgItem.Cells(1).Text() & dgItem.Cells(17).Text() & ".pdf"

                    '=== llenar datos a las variables
                    var = 0
                    strNroDoc = "" : strNomEnti = "" : strNroLib = "" : strIdTipoDocumento = "" : strPeriodoAnual = "" : strCod_TipOtro = "" : strCod_Doc = ""
                    strDscMerc = "" : strCod_Unidad = ""
                    strIdUsuario = ""

                    strNroDoc = dgItem.Cells(1).Text()
                    strNomEnti = dgItem.Cells(7).Text()
                    strNroLib = dgItem.Cells(17).Text()
                    strIdTipoDocumento = dgItem.Cells(3).Text()
                    strPeriodoAnual = dgItem.Cells(14).Text()
                    strCod_TipOtro = dgItem.Cells(15).Text()
                    strCod_Doc = dgItem.Cells(18).Text()
                    strDscMerc = dgItem.Cells(20).Text()
                    strCod_Unidad = dgItem.Cells(19).Text()
                    strIdUsuario = Session.Item("IdUsuario")

                    '=== cargar Datos a Tabla Temporal ===
                    Dim dr As DataRow
                    dr = dtIngreso001.NewRow
                    dr(0) = strCont
                    dr(1) = strNroDoc
                    dr(2) = strNomEnti
                    dr(3) = strNroLib
                    dr(4) = strIdTipoDocumento
                    dr(5) = strPeriodoAnual
                    dr(6) = strCod_TipOtro
                    dr(7) = strCod_Doc
                    dr(8) = strDscMerc
                    dr(9) = strCod_Unidad
                    dtIngreso001.Rows.Add(dr)

                    '=== Copiar Documentos al Filemaster ==
                    'Si el documento esta en la opcion del menu "Registrado" EstadoDoc = "04" 
                    If Session.Item("EstadoDoc") = "04" Then
                        Dim dtProrroga As DataTable
                        Dim objConexion As SqlConnection
                        Dim objTrans As SqlTransaction
                        Dim strIds As String
                        Dim dtDetalle As DataTable
                        objConexion = New SqlConnection(strConn)
                        objConexion.Open()
                        objTrans = objConexion.BeginTransaction()
                        Try
                            If strIdTipoDocumento = "02" Or strIdTipoDocumento = "13" Or strIdTipoDocumento = "14" Then
                                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                                    objTrans.Commit()
                                    objTrans.Dispose()
                                    Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                                    objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                                    objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                                    Exit Try
                                End If

                                dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCod_Unidad, strNroLib, strCod_TipOtro, strNroDoc, objTrans)
                                objLiberacion.GetReporteLiberacionFirmas(NombreReporte, strEmpresa, strCod_Unidad, "DOR", dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"), dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), strIdTipoDocumento, objTrans, strPathFirmas) 'Server.MapPath(strPathFirmas)
                                GrabaDocumento(NombreReporte, objTrans)
                                objDocumento.gInsLogDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, "I", strIdTipoDocumento, objTrans)
                                dtDetalle.Dispose()
                                objTrans.Commit()
                                objTrans.Dispose()
                                strError = "X"
                            ElseIf strIdTipoDocumento = "05" Then
                                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                                    objTrans.Commit()
                                    objTrans.Dispose()
                                    Exit Try
                                End If

                                objfunciones.GetReporteProrrogaFolio(NombreReporte, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strPathFirmas, dgItem.Cells(7).Text(), dgItem.Cells(6).Text(), objTrans) 'Server.MapPath(strPathFirmas)
                                GrabaDocumento(NombreReporte, objTrans)
                                objTrans.Commit()
                                objTrans.Dispose()
                                strError = "X"
                                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                            ElseIf strIdTipoDocumento = "20" Then
                                objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans)
                                objTrans.Commit()
                                objTrans.Dispose()
                            ElseIf strIdTipoDocumento = "03" Or strIdTipoDocumento = "10" Or strIdTipoDocumento = "12" Then
                                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                                    objTrans.Commit()
                                    objTrans.Dispose()
                                    Exit Try
                                End If
                                dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans)
                                For i As Integer = 0 To dtProrroga.Rows.Count - 1
                                    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                                        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans)
                                    Else
                                        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                                        objReportes.GeneraPDFWarrantFirmasXDocumento(strCod_Doc, dtProrroga.Rows(i)("COD_DOC"), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, dtProrroga.Rows(i)("NOM_PDF"), objTrans)
                                    End If
                                Next

                                If strIds = "" Then
                                    objTrans.Rollback()
                                    objTrans.Dispose()
                                    pr_IMPR_MENS("No se encontr� el warrant relacionado a este documento")
                                    Exit Sub
                                End If
                                If strIdTipoDocumento <> "12" Then
                                    objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans)
                                End If
                                objTrans.Commit()
                                objTrans.Dispose()
                                strError = "X"

                                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster.CopiaDocumentosInfodepsaRegistrados(strCod_Doc)
                            ElseIf strIdTipoDocumento = "09" Or strIdTipoDocumento = "17" Or strIdTipoDocumento = "18" Or strIdTipoDocumento = "19" Then
                                objReportes.GeneraDCR(strCod_Doc, strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans)
                                objTrans.Commit()
                                objTrans.Dispose()
                            ElseIf strIdTipoDocumento = "11" Or strIdTipoDocumento = "15" Or strIdTipoDocumento = "16" Then
                                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                                    objTrans.Commit()
                                    objTrans.Dispose()
                                    Exit Try
                                End If

                                dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCod_Unidad, strNroLib, strCod_TipOtro, strNroDoc, objTrans)
                                objLiberacion.GetReporteLiberacionFirmas(NombreReporte, strEmpresa, strCod_Unidad, "DOR", dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"), dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), strIdTipoDocumento, objTrans, strPathFirmas) 'Server.MapPath(strPathFirmas)
                                GrabaDocumento(NombreReporte, objTrans)
                                objDocumento.gInsLogDocumento(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, "I", strIdTipoDocumento, objTrans)
                                dtDetalle.Dispose()
                                objTrans.Commit()
                                objTrans.Dispose()
                                strError = "X"
                                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                                objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                            End If
                        Catch ex As Exception
                            If strError = "" Then
                                objTrans.Rollback()
                                objTrans.Dispose()
                            End If
                            Me.lblError.Text = " Se cancel� el proceso debido al siguiente error:  " & ex.ToString
                            Exit Sub
                        End Try
                    ElseIf Session.Item("EstadoDoc") = "02" And strIdTipoDocumento = "12" Then
                        If objDocumento.gGetEstadoImpresionEmitido(strCod_Doc) = False Then
                            Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                            objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                            objWSFileMaster.CopiaDocumentosInfodepsaCreados(strCod_Doc)
                        End If
                    End If
                    If objDocumento.gGetEstadoImpresionCreados(strCod_Doc) = False Then
                        '-------------------------------------------------------------------
                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaCreados(strCod_Doc)
                    End If
                    '--------------------------------------------
                    strCont = strCont + 1
                End If
            Next

            If satValida = False Then
                Me.lblError.Text = "Seleccione al menos un documentos a firmar"
                Exit Sub
            End If

            'dtIngreso001 = Nothing
            'Session.Remove("dtIngreso001")
            'Me.lblMensaje.Text = "Se gener� el ingreso n�mero " & "I" & dsIngreso.Tables(1).Rows(0)(0)

            Session.Add("dtIngreso001", dtIngreso001)
            Response.Redirect("DocumentoPDF.aspx?var=1", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Function crearTabla() As DataTable
        Dim dtIngreso001 As New DataTable
        'dtIngreso.Rows.Clear()
        Dim Column0 As New DataColumn("ITEM")
        Column0.DataType = GetType(Integer)
        Dim Column1 As New DataColumn("NRO_DOCU")
        Column1.DataType = GetType(String)
        Dim Column2 As New DataColumn("NOM_ENT")
        Column2.DataType = GetType(String)
        Dim Column3 As New DataColumn("NRO_LIBE")
        Column3.DataType = GetType(String)
        Dim Column4 As New DataColumn("COD_TIPDOC")
        Column4.DataType = GetType(String)
        Dim Column5 As New DataColumn("PER_ANU")
        Column5.DataType = GetType(String)
        Dim Column6 As New DataColumn("COD_TIPOTRO")
        Column6.DataType = GetType(String)
        Dim Column7 As New DataColumn("COD_DOCU")
        Column7.DataType = GetType(String)
        Dim CoLumn8 As New DataColumn("DSC_MERC")
        CoLumn8.DataType = GetType(String)
        Dim Column9 As New DataColumn("COD_UNIDAD")
        Column9.DataType = GetType(String)
        dtIngreso001.Columns.Add(Column0)
        dtIngreso001.Columns.Add(Column1)
        dtIngreso001.Columns.Add(Column2)
        dtIngreso001.Columns.Add(Column3)
        dtIngreso001.Columns.Add(Column4)
        dtIngreso001.Columns.Add(Column5)
        dtIngreso001.Columns.Add(Column6)
        dtIngreso001.Columns.Add(Column7)
        dtIngreso001.Columns.Add(CoLumn8)
        dtIngreso001.Columns.Add(Column9)
        Session.Add("dtIngreso001", dtIngreso001)
        Return dtIngreso001
    End Function
End Class
