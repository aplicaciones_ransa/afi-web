Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class Cliente
    Inherits System.Web.UI.Page
    Private objFunciones As Library.AccesoBL.Funciones
    Private objAcceso As Acceso = New Acceso
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents cboCliente As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAceptar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) Then
            If Not IsPostBack Then
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        objFunciones = New Library.AccesoBL.Funciones
        objFunciones.GetloadEntidadUsr(Me.cboCliente, Session.Item("IdUsuario"))
        If Me.cboCliente.Items.Count = 0 Then
            Me.btnAceptar.Enabled = False
            Me.cboCliente.Enabled = False
            Me.lblMensaje.Text = "Usted no cuenta con ninguna entidad asignada, comuniquese con el administrador"
        ElseIf Me.cboCliente.Items.Count >= 1 Then
            Me.btnAceptar.Enabled = True
            Ingreso()
        Else
            Me.btnAceptar.Enabled = True
        End If
    End Sub

    Private Sub Ingreso()
        Dim dt As DataTable = New DataTable
        Try
            objAcceso.gInicioSesionEmpresa(Session.Item("IdUsuario"), Me.cboCliente.SelectedItem.Value)
            If objAcceso.blnEstado Then
                Session.Add("Cliente", Me.cboCliente.SelectedItem.Value)
                Session.Add("IdTipoUsuario", objAcceso.strIdTipoUsuario)
                Session.Add("IdSico", objAcceso.strIdSico)
                Session.Add("Pendiente", objAcceso.strPendiente)
                Session.Add("IdTipoEntidad", objAcceso.strIdTipoEntidad)
                Session.Add("NombreEntidad", objAcceso.strNomEntidad)
                Session.Add("CoSist", objAcceso.strCoSist)
                Session.Add("CoGrup", objAcceso.strCoGrup)
                Session.Add("CertDigital", objAcceso.blnCert)
                Session.Add("LibMultiple", objAcceso.blnLibMult)
                Session.Add("CodSAP", objAcceso.strCodSAP)
                Dim strIdPag As String = ""
                Dim strRutaPag As String = ""
                dt = objAcceso.gGetMenuxGrupo(objAcceso.strCoGrup, objAcceso.strCoSist)
                For i As Integer = 0 To dt.Rows.Count - 1
                    strIdPag = strIdPag & dt.Rows(i)("CO_MENU") & ","
                    strRutaPag = strRutaPag & dt.Rows(i)("NO_URL") & ","
                Next

                If strIdPag.Length < 1 Then
                    lblMensaje.Text = "Su usuario no tiene asignado opciones en el sistema"
                    Exit Sub
                Else
                    strIdPag = strIdPag.Remove(strIdPag.Length - 1, 1)
                    Session.Add("PagId", strIdPag)
                    PoliticaCache()
                    If Request.QueryString("sPagina") = "" Then
                        Dim strNoUrl As String = "Salir.aspx?caduco=1"
                        For j As Integer = 0 To dt.Rows.Count - 1
                            If dt.Rows(j)("NO_URL") <> "#" Then
                                strNoUrl = dt.Rows(j)("NO_URL")
                                Exit For
                            End If
                        Next
                        Response.Redirect(strNoUrl, False)
                        '======= fin  validar si es cliente  ===========
                    ElseIf InStr(strRutaPag, Request.QueryString("sPagina")) Then
                        Response.Redirect(Request.QueryString("sPagina"), False)
                    Else
                        Session.Add("strMensaje", "Usted no tiene acceso a la opci�n seleccionada")
                        Response.Redirect("Login.aspx", False)
                    End If
                    Exit Sub
                End If
            Else
                lblMensaje.Text = "Su usuario no esta activo para la entidad seleccionada, consulte con el administrador"
            End If
        Catch ex As Exception
            lblMensaje.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objAcceso Is Nothing) Then
            objAcceso = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Ingreso()
    End Sub

    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub
End Class
