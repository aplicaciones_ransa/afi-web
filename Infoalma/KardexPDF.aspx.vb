Imports Library.AccesoDB
Imports System.IO
Imports System.Data

Public Class KardexPDF
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
                Dim strNroWarr As String = Request.QueryString("sNU_TITU")
                Dim strNroLibe As String = Request.QueryString("sNU_DOCU_RETI")
                Dim strCodTipDoc As String = Request.QueryString("sCOD_TIPDOC")
                Dim strPrdDoc As String = Request.QueryString("sPRD_DOCU")
                Dim objStream As Stream
                Dim var As Integer
                Dim drPDF As DataRow

                var = Request.QueryString("var")
                drPDF = objDocumento.gSelArchivoPDF(strNroWarr, strNroLibe, strPrdDoc, strCodTipDoc, 1)

                If drPDF Is Nothing Then
                    Response.Write("No se encontr� el archivo PDF")
                    Exit Try
                Else
                    If drPDF("DOC_PDFFINA") Is DBNull.Value Then
                        If drPDF("DOC_PDFORIG") Is DBNull.Value Then
                            objStream = New FileStream(strPath & "02" & strNroWarr & strNroLibe & ".pdf", FileMode.Open)
                            Dim FileSize As Long
                            FileSize = objStream.Length

                            Dim Buffer(CInt(FileSize)) As Byte
                            objStream.Read(Buffer, 0, CInt(FileSize))
                            objStream.Close()

                            Response.Clear()
                            Response.Buffer = True
                            Response.ContentType = "application/pdf"
                            Response.BinaryWrite(Buffer)
                        Else
                            Response.Clear()
                            Response.Buffer = True
                            Response.ContentType = "application/pdf"
                            Response.BinaryWrite(CType(drPDF("DOC_PDFORIG"), Byte()))
                        End If
                    Else
                        Response.Clear()
                        Response.Buffer = True
                        Response.ContentType = "application/pdf"
                        Response.BinaryWrite(CType(drPDF("DOC_PDFFINA"), Byte()))
                    End If
                End If
                Response.Flush()
                Response.Close()
            Else
                Response.Redirect("Salir.aspx?caduco=1", False)
            End If
        Catch ex As Exception
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub
End Class
