<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="RutaConfigurar.aspx.vb" Inherits="RutaConfigurar" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Configurar Ruta</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="francisco_uni@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">    
       function AbrirMail(Secu,Ruta,TipDoc,Desc) 
		{    
		if (Ruta=="")
		{
		alert("Grabe las modificaciones hechas");
		}                 
		else
		{
		window.open("Popup/EnviarMailEntidad.aspx?Secu="+Secu+"&Ruta="+Ruta+"&TipDoc="+TipDoc+"&Desc="+Desc,"",'left=240,top=200,width=355,height=560,toolbar=0,resizable=0'); //'status:no;dialogWidth:355px;dialogHeight:540px;dialogHide:true;help:no;scroll:no;resizable=no;toolbar=no;menubar=no;center=yes');//'dialogLeft=240,dialogTop=100,dialogWidth=400,dialogHeight=530,toolbar=0,resizable=0');
		}
		}		 
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD class="Titulo1" width="100%">Configurar Flujo de Aprobaciones
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="690" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" width="20%">Tipo Documento:</TD>
														<TD width="80%">
															<asp:dropdownlist id="cboTipoDocumento" runat="server" AutoPostBack="True" CssClass="Text"></asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">Flujo:</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" AutoGenerateColumns="False" Width="100%"
										BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NRO_SECU" HeaderText="Secu.">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Tipo Entidad">
												<ItemTemplate>
													<asp:DropDownList id=cboTipoEntidad runat="server" CssClass="Text" Width="90px" DataSource="<%# GetTipoEntidad() %>" DataTextField="NOM_TIPENTI" DataValueField="COD_TIPENTI" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.COD_TIPENTI") %>'>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n">
												<ItemTemplate>
													<asp:TextBox id=txtDescripcion runat="server" CssClass="Text" Width="260px" Text='<%# DataBinder.Eval(Container, "DataItem.DSC_SECU") %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Estado Ubic">
												<ItemTemplate>
													<asp:DropDownList id=cboEstado runat="server" CssClass="Text" Width="80px" DataSource="<%# GetEstado() %>" DataTextField="NOM_ESTA" DataValueField="COD_ESTA" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.COD_ESTA") %>'>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Config.">
												<HeaderStyle Width="80px"></HeaderStyle>
												<ItemTemplate>
													<A class=error style="FONT-WEIGHT: bold; FONT-SIZE: 7pt; FONT-FAMILY: Verdana" onclick='AbrirMail("<%# DataBinder.Eval(Container.DataItem, "NRO_SECU") %>","<%# DataBinder.Eval(Container.DataItem, "NRO_RUTA") %>","<%# DataBinder.Eval(Container.DataItem, "COD_TIPDOC") %>","<%# DataBinder.Eval(Container.DataItem, "DSC_SECU") %>");' href="#">
														Opciones</A>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Elim.">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table13" border="0" cellSpacing="0" cellPadding="0" width="30">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" CausesValidation="False"
																	ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.JPG"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Firma">
												<ItemTemplate>
													<TABLE id="Table15" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id="chkFirma" runat="server" AutoPostBack="True" CssClass="Text" OnCheckedChanged="chkFirma_shange"></asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_FIRM"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="DblEnd">    
                                                
												<ItemTemplate>
													<TABLE id="Table12" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id="chkDblEndo" runat="server" AutoPostBack="True" CssClass="Text" OnCheckedChanged="chkDblEndo_shange"></asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>

											<asp:BoundColumn Visible="False" DataField="FLG_DBLENDO"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="690" border="0">
										<TR>
											<TD>
												<asp:button id="btnAgregar" runat="server" CssClass="btn" Width="80px" Text="Agregar"></asp:button></TD>
											<TD>
												<asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar"></asp:button></TD>
											<TD></TD>
											<TD></TD>
											<TD width="550"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
