<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarAproTrasladoAlmacen.aspx.vb" Inherits="WarAproTrasladoAlmacen" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarAproTrasladoAlmacen</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script type="text/javascript">

        
        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });


    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table id="Table1" border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="6">
                                            <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td align="center">
                                                        <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td style="z-index: 0" class="Text">Nro&nbsp;Warrant :</td>
                                                                <td>
                                                                    <asp:TextBox Style="z-index: 0" ID="txtNroWarrant" runat="server" CssClass="text" Width="80px"></asp:TextBox></td>
                                                                <td class="Text"></td>
                                                                <td style="z-index: 0" class="Text">Estado:</td>
                                                                <td class="Text">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboEstado" runat="server" CssClass="text" Width="140px">
                                                                        <asp:ListItem Value="0">Todos</asp:ListItem>
                                                                        <asp:ListItem Value="P">Pendiente aprobaci&#243;n</asp:ListItem>
                                                                        <asp:ListItem Value="C">Pendiente confirmaci&#243;n</asp:ListItem>
                                                                        <asp:ListItem Value="A">Aprobado y confirmado</asp:ListItem>
                                                                        <asp:ListItem Value="R">Rechazado</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text">Desde:</td>
                                                                <td class="Text">
                                                                    <input style="z-index: 0" id="txtFechaInicial" class="text" onkeypress="validarcharfecha()"
                                                                        onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="6" name="txtFechaInicial" runat="server"><input style="z-index: 0" id="btnFechaInicial" class="Text" 
                                                                            value="..." type="button" name="btnFecha"></td>
                                                                <td class="Text">Hasta:</td>
                                                                <td class="Text">
                                                                    <input style="z-index: 0" id="txtFechaFinal" class="text" onkeypress="validarcharfecha()"
                                                                        onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="6" name="txtFechaFinal" runat="server"><input style="z-index: 0" id="btnFechaFinal" class="Text" 
                                                                            value="..." type="button" name="btnFecha"></td>
                                                                <td class="Text"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:Button><asp:Button Style="z-index: 0" ID="btnAprobar" runat="server" CssClass="Text" Width="80px" Text="Aprobar"
                                                Visible="False"></asp:Button><asp:Button Style="z-index: 0" ID="btnConfirmar" runat="server" CssClass="Text" Width="80px"
                                                    Text="Confirmar" Visible="False"></asp:Button><asp:Button Style="z-index: 0" ID="btnRechazar" runat="server" CssClass="Text" Width="80px"
                                                        Text="Rechazar" Visible="False"></asp:Button></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="15" AllowPaging="True"
                                                OnPageIndexChanged="Change_Page" BorderColor="Gainsboro" AutoGenerateColumns="False" BorderWidth="1px">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Sel.">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSeleccion" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NRO_TRAS" HeaderText="NRO_TRAS"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro Warrant"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NRO_ITEM" HeaderText="Nro &#205;tem"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC" HeaderText="Desc. Merc."></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NO_CLIE_REPO" HeaderText="Depositante"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_UBI_ORIG" HeaderText="Almac&#233;n Orig."></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_UBI_FINA" HeaderText="Almac&#233;n Dest."></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FCH_CREA" HeaderText="Fecha Emisi&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ESTADO" HeaderText="Estado">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NOMBRE" HeaderText="Usr. Apro."></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FCH_FINA" HeaderText="Fch. Apro."></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="COD_CLIE"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="COD_ENTI_FINA"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="tblLeyenda" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="td" colspan="6"></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f5dc8c" width="15%"></td>
                                                    <td class="Leyenda" align="center">Pendiente aprobación</td>
                                                    <td bgcolor="#c8dcf0" width="15%"></td>
                                                    <td class="Leyenda" align="center">Pendiente Traslado</td>
                                                    <td bgcolor="#ffffff" width="15%"></td>
                                                    <td class="Leyenda" align="center">Aprobado y trasladado</td>
                                                    <td bgcolor="scrollbar" width="15%"></td>
                                                    <td class="Leyenda" align="center">Rechazado</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
