<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SolicitarNuevosIngresos.aspx.vb" Inherits="SolicitarNuevosIngresos" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Solicitar Nuevos Ingresos</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
							<TR>
								<TD colSpan="2">
									<uc1:header id="Header2" runat="server"></uc1:header></TD>
							</TR>
							<TR>
								<TD vAlign="top" width="125">
									<uc1:Menu id="Menu1" runat="server"></uc1:Menu></TD>
								<TD vAlign="top">
									<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="Titulo1" height="20">
												<P align="center">&nbsp;SOLICITAR NUEVOS INGRESOS</P>
											</TD>
										</TR>
										<TR>
											<TD class="td" style="HEIGHT: 236px">
												<TABLE id="Table26" cellSpacing="0" cellPadding="0" width="630" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD align="center">
															<TABLE id="Table2" style="HEIGHT: 214px" cellSpacing="3" cellPadding="3" width="408" bgColor="whitesmoke"
																border="0" class="Text">
																<TR>
																	<TD class="text" style="WIDTH: 135px">Solicitado por
																	</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtSolicitante" style="TEXT-TRANSFORM :uppercase" runat="server" Width="180px"
																			CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtSolicitante" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">�rea</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:dropdownlist id="cboArea" runat="server" Width="180px" DataTextField="DES" DataValueField="COD"
																			CssClass="Text"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Cantidad de Cajas</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtNroCajas" style="TEXT-TRANSFORM :uppercase" runat="server" Width="50px" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtNroCajas" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Direcci�n de Recojo</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtDirEntrega" style="TEXT-TRANSFORM :uppercase" runat="server" TextMode="MultiLine"
																			Width="180px" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtDirEntrega" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Autorizado por</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtAutorizador" style="TEXT-TRANSFORM :uppercase" runat="server" Width="180px"
																			ReadOnly="True" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ControlToValidate="txtAutorizador" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 135px; HEIGHT: 9px"></TD>
																	<TD style="WIDTH: 16px; HEIGHT: 9px"></TD>
																	<TD style="HEIGHT: 9px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 160px; HEIGHT: 9px" align="right" colSpan="2"><INPUT TYPE="reset" value="Limpiar" class="btn"></TD>
																	<TD style="HEIGHT: 9px">
																		<asp:button id="btnEnviar" runat="server" Text="Enviar" CssClass="btn"></asp:button></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD align="center">
												<asp:label id="lblError" runat="server" CssClass="Error"></asp:label>
											</TD>
										</TR>
										<TR>
											<TD></TD>
										</TR>
										<TR>
											<TD class="td"></TD>
										</TR>
										<TR>
											<TD class="td" id="Estado"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
							</TR>
						</TABLE>
		</form>
	</body>
</HTML>
