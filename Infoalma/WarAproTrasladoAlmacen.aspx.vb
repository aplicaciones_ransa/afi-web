Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class WarAproTrasladoAlmacen
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnAprobar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnRechazar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnConfirmar As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objMercaderia As New clsCNMercaderia
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objAccesoWeb As clsCNAccesosWeb
    Private intContador As Integer = 0
    Private strCodFinanciador As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "35") Then
            Try
                Me.lblOpcion.Text = "Traslado de Mercader�a"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    Dim dttFecha As DateTime
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "35"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    dttFecha = Date.Today.Subtract(TimeSpan.FromDays(180))
                    Dia = "00" + CStr(dttFecha.Day)
                    Mes = "00" + CStr(dttFecha.Month)
                    Anio = "0000" + CStr(dttFecha.Year)
                    Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    BindDatagrid()
                    btnAprobar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de aprobar el traslado?')== false) return false;")
                    btnRechazar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de rechazar el traslado?')== false) return false;")
                    If Session("IdTipoEntidad") = "02" Then
                        Me.btnAprobar.Visible = True
                        Me.btnRechazar.Visible = True
                    End If
                    If Session("IdTipoEntidad") = "01" Then
                        Me.btnConfirmar.Visible = True
                        Me.btnRechazar.Visible = True
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objMercaderia.gCNGetListarSolicitud(Me.txtNroWarrant.Text.Replace("'", ""), FechaInicial, FechaFinal,
            Session.Item("IdTipoEntidad"), Session.Item("IdSico"), Me.cboEstado.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblMensaje.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim chkSeleccion As CheckBox
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            chkSeleccion = CType(e.Item.FindControl("chkSeleccion"), System.Web.UI.WebControls.CheckBox)
            If e.Item.DataItem("EST_TRAS") = "P" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")
                If Session("IdTipoEntidad") = "02" And Session("IdTipoUsuario") = "03" Then
                    chkSeleccion.Enabled = True
                End If
            End If

            If e.Item.DataItem("EST_TRAS") = "C" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
            End If

            If e.Item.DataItem("EST_TRAS") = "R" Then
                e.Item.BackColor = System.Drawing.Color.FromName("scrollbar")
            End If

            If e.Item.DataItem("EST_TRAS") = "A" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#c8dcf0")
                If Session("IdTipoEntidad") = "01" Then
                    chkSeleccion.Enabled = True
                End If
            End If
        End If
    End Sub

    Public Sub btnAprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAprobar.Click
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "35", "APROBAR TRASLADO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Dim strScript As String
            Dim strBody As String
            Dim strDepositante As String
            Dim strCodDepositante As String

            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    intContador += 1
                End If
            Next

            If intContador = 0 Then
                strScript = "<script language='javascript'>alert('Seleccione al menos un registro');</script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If

            strBody = "<table border=1 style='FONT-FAMILY: Tahoma; FONT-SIZE: 10pt' width='600px'><tr><td>NRO WARRANT</td><td>NRO �TEM</td><td>DESCRIPCI�N</td><td>ALMAC�N DESTINO</td></tr>"
            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    objMercaderia.gCNUpdEstadoSolicitud(dgItem.Cells(1).Text(), Session.Item("IdUsuario"), "A")
                    strBody += "<tr><td>" & dgItem.Cells(2).Text & "</td><td>" & dgItem.Cells(3).Text & "</td><td>" & dgItem.Cells(4).Text & "</td><td>" & dgItem.Cells(7).Text & "</td></tr>"
                    strDepositante = dgItem.Cells(5).Text
                    strCodDepositante = dgItem.Cells(12).Text
                    strCodFinanciador = dgItem.Cells(13).Text
                End If
            Next
            strBody += "</table>"
            strBody = "<FONT face='Tahoma' size='2'>Estimados Se�ores:<br><br>" &
                    Session("NombreEntidad") & " aprob� los siguientes traslados :</FONT><br><br>" & strBody &
                    "<br><FONT face='Tahoma' size='2'>Depositante: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & strDepositante & "</FONT></STRONG>" &
                     "<br><FONT face='Tahoma' size='2'>Aprobado por: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & UCase(Session("UsuarioLogin")) & "</FONT></STRONG>" &
                    "<br><FONT face='Tahoma' size='2'>Alma Per� supervisar� el traslado de las unidades solicitadas.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A></FONT>"

            objfunciones.EnviaMailTraslado("00000001", strCodDepositante, Session.Item("IdSico"), "Aprobaci�n de traslado - " & strDepositante, strBody)
            pr_IMPR_MENS("Se aprob� correctamente ")
            BindDatagrid()
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Public Sub btnRechazar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "35", "RECHAZAR TRASLADO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Dim strScript As String
            Dim strBody As String
            Dim strDepositante As String
            Dim strCodDepositante As String
            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    intContador += 1
                End If
            Next

            If intContador = 0 Then
                strScript = "<script language='javascript'>alert('Seleccione al menos un registro');</script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If

            strBody = "<table border=1 style='FONT-FAMILY: Tahoma; FONT-SIZE: 10pt' width='600px'><tr><td>NRO WARRANT</td><td>NRO �TEM</td><td>DESCRIPCI�N</td><td>ALMAC�N DESTINO</td></tr>"
            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    objMercaderia.gCNUpdEstadoSolicitud(dgItem.Cells(1).Text(), Session.Item("IdUsuario"), "R")
                    strBody += "<tr><td>" & dgItem.Cells(2).Text & "</td><td>" & dgItem.Cells(3).Text & "</td><td>" & dgItem.Cells(4).Text & "</td><td>" & dgItem.Cells(7).Text & "</td></tr>"
                    strDepositante = dgItem.Cells(5).Text
                    strCodDepositante = dgItem.Cells(12).Text
                    strCodFinanciador = dgItem.Cells(13).Text
                End If
            Next
            strBody += "</table>"

            strBody = "<FONT face='Tahoma' size='2'>Estimados Se�ores:<br><br>" &
                    Session("NombreEntidad") & " rechaz� el traslado de los siguiente �tems :</FONT><br><br>" & strBody &
                    "<br><FONT face='Tahoma' size='2'>Depositante: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & strDepositante & "</FONT></STRONG>" &
                     "<br><FONT face='Tahoma' size='2'>Rechazado por: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & UCase(Session("UsuarioLogin")) & "</FONT></STRONG>" &
                    "<br><FONT face='Tahoma' size='2'>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A></FONT>"

            objfunciones.EnviaMailTraslado("00000001", strCodDepositante, strCodFinanciador, "Rechazo de traslado - " & strDepositante, strBody)
            pr_IMPR_MENS("Se rechaz� correctamente ")
            BindDatagrid()
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "35", "CONFIRMAR TRASLADO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Dim strScript As String
            Dim strBody As String
            Dim strDepositante As String
            Dim strCodDepositante As String

            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    intContador += 1
                End If
            Next
            If intContador = 0 Then
                strScript = "<script language='javascript'>alert('Seleccione al menos un registro');</script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If

            strBody = "<table border=1 style='FONT-FAMILY: Tahoma; FONT-SIZE: 10pt' width='600px'><tr><td>NRO WARRANT</td><td>NRO �TEM</td><td>DESCRIPCI�N</td><td>ALMAC�N DESTINO</td></tr>"
            For Each dgItem As DataGridItem In Me.dgdResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                If myCheckbox.Checked Then
                    objMercaderia.gCNUpdEstadoSolicitud(dgItem.Cells(1).Text(), Session.Item("IdUsuario"), "C")
                    strBody += "<tr><td>" & dgItem.Cells(2).Text & "</td><td>" & dgItem.Cells(3).Text & "</td><td>" & dgItem.Cells(4).Text & "</td><td>" & dgItem.Cells(7).Text & "</td></tr>"
                    strDepositante = dgItem.Cells(5).Text
                    strCodDepositante = dgItem.Cells(12).Text
                    strCodFinanciador = dgItem.Cells(13).Text
                End If
            Next
            strBody += "</table>"
            strBody = "<FONT face='Tahoma' size='2'>Estimados Se�ores:<br><br>" &
                    Session("NombreEntidad") & " confirm� el traslado de los siguiente �tems :</FONT><br><br>" & strBody &
                    "<br><FONT face='Tahoma' size='2'>Depositante: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & strDepositante & "</FONT></STRONG>" &
                     "<br><FONT face='Tahoma' size='2'>Confirmado por: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & UCase(Session("UsuarioLogin")) & "</FONT></STRONG>" &
                    "<br><FONT face='Tahoma' size='2'>Alma Per� supervisar� el traslado de las unidades solicitadas.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A></FONT>"

            objfunciones.EnviaMailTraslado(Session.Item("IdSico"), strCodDepositante, strCodFinanciador, "Confirmaci�n de traslado - " & strDepositante, strBody)
            pr_IMPR_MENS("Se confirm� correctamente ")
            BindDatagrid()
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub
End Class
