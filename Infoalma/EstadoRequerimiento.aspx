<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EstadoRequerimiento.aspx.vb" Inherits="EstadoRequerimiento" %>

<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Todos los Requerimientos</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaDesde").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button3").click(function () {
                $("#txtFechaDesde").datepicker('show');
            });

            $("#txtFechaHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button1").click(function () {
                $("#txtFechaHasta").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            Estado.style.display = 'none';
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="113">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">ESTADO DEL REQUERIMIENTO</td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table16" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="text" style="width: 61px">�rea</td>
                                                    <td class="text" style="width: 15px" align="center">:</td>
                                                    <td class="text" style="width: 207px" colspan="3">
                                                        <asp:DropDownList ID="cboArea" runat="server" CssClass="Text" AutoPostBack="True" Width="140px" DataValueField="COD"
                                                            DataTextField="DES">
                                                            <asp:ListItem Value="Presidencia del Directorio">Presidencia del Directorio</asp:ListItem>
                                                            <asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
                                                            <asp:ListItem Value="Tesoreria">Tesoreria</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;</td>
                                                    <td class="text" style="width: 136px">Persona Autorizada</td>
                                                    <td class="text" style="width: 11px">:</td>
                                                    <td style="width: 149px">
                                                        <asp:DropDownList ID="cboPersonaAutorizada" runat="server" CssClass="Text" Width="140px" DataValueField="COD"
                                                            DataTextField="DES">
                                                            <asp:ListItem Value="1 - Caja">Luisa Alejos Roman</asp:ListItem>
                                                            <asp:ListItem Value="2 - Libro">Carlos Dextre</asp:ListItem>
                                                            <asp:ListItem Value="3 - Tubo">Julio Francisco Salazar</asp:ListItem>
                                                            <asp:ListItem Value="Todas" Selected="True">Todas</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 61px">Requerimiento
                                                    </td>
                                                    <td class="text" style="width: 15px" align="center">:</td>
                                                    <td style="width: 143px">
                                                        <asp:TextBox ID="txtNroRequerimiento" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Width="140px" MaxLength="20"></asp:TextBox></td>
                                                    <td class="text" style="width: 45px"></td>
                                                    <td style="width: 11px"></td>
                                                    <td class="text" style="width: 136px">Fecha</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 149px"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 61px">Estado</td>
                                                    <td class="text" style="width: 15px" align="center">:</td>
                                                    <td style="width: 143px">
                                                        <asp:DropDownList ID="cboEstado" runat="server" CssClass="Text" Width="140px" DataValueField="COD"
                                                            DataTextField="DES">
                                                            <asp:ListItem Value="En Proceso">En Proceso</asp:ListItem>
                                                            <asp:ListItem Value="Pendiente">Pendiente</asp:ListItem>
                                                            <asp:ListItem Value="Terminado">Terminado</asp:ListItem>
                                                            <asp:ListItem Value="Todas" Selected="True">Todas</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 45px"></td>
                                                    <td style="width: 11px"></td>
                                                    <td class="text" style="width: 136px">Desde
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaDesde" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaDesde" runat="server"><input class="Text" id="Button3" type="button" value="..."
                                                                name="btnFecha1"></td>
                                                    <td style="width: 11px"></td>
                                                    <td class="text" style="width: 149px">Hasta&nbsp;&nbsp;&nbsp; &nbsp;<input class="Text" onkeypress="validarcharfecha()" id="txtFechaHasta" onkeyup="this.value=formateafecha(this.value);"
                                                        maxlength="10" size="6" name="txtFechaHasta" runat="server"><input class="Text" id="Button1" type="button" value="..."
                                                            name="btnFecha"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="CampoObliga" colspan="3"></td>
                                                    <td style="width: 45px"></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 136px"></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 149px" align="right">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" height="20">Resultado :
									<asp:Label ID="lblRegistros" runat="server" CssClass="text"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    AutoGenerateColumns="False" OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="30"
                                    AllowSorting="True">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="NU_REQU" HeaderText="Nro Requerimiento">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_RECE" HeaderText="Fecha"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_NOMB_PERS" HeaderText="Persona Autor."></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TOTA_CONS" HeaderText="Nro. Cons.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_CONS_PEND" HeaderText="Nro.Cons.Pend.">
                                            <HeaderStyle Width="5px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_TIPO_CONS" HeaderText="Tipo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="estado" HeaderText="Estado"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Ver">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
                                                    CausesValidation="False"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td" id="Estado"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
