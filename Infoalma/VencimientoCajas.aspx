<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="VencimientoCajas.aspx.vb" Inherits="VencimientoCajas" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Vencimiento de Unidades</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaHasta").datepicker('show');
            });


        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            Estado.style.display = 'none';
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="117">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">VENCIMIENTO DE CAJAS</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table id="Table26" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr class="Texto9pt">
                                                    <td class="text" style="width: 40px; height: 18px" width="40">�rea</td>
                                                    <td class="text" style="width: 14px; height: 18px" width="14">:</td>
                                                    <td class="text" style="width: 154px; height: 18px" width="154">
                                                        <asp:DropDownList ID="cboArea" runat="server" DataValueField="COD" DataTextField="DES" Width="140px"
                                                            CssClass="Text">
                                                            <asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 144px; height: 18px" width="144" align="center">Fecha 
															de Vencimiento al&nbsp;</td>
                                                    <td class="text" style="width: 10px; height: 18px" align="center">:</td>
                                                    <td class="text" style="width: 132px; height: 18px" align="center">
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaHasta" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaHasta" runat="server"><input class="Text" id="btnFechaFinal" type="button"
                                                                value="..." name="btnFecha"></td>
                                                    <td class="text" style="height: 18px">
                                                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn"></asp:Button></td>
                                                    <td class="text" style="height: 18px" width="20"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" valign="top">Resultados :
									<asp:Label ID="lblRegistros" runat="server" CssClass="text"></asp:Label><asp:DataGrid ID="dgdResultado" runat="server" Width="100%" CellPadding="0" AutoGenerateColumns="False"
                                        AllowPaging="True" PageSize="20" AllowSorting="True" CssClass="gv" BorderColor="Gainsboro">
                                        <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                        <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                        <ItemStyle CssClass="gvRow"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Nro. Caja">
                                                <HeaderStyle Width="40%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "DocumentosxUnidad.aspx?CA=" &amp; DataBinder.Eval(Container.DataItem, "CO_AREA") &amp; "&amp;IU=" &amp; DataBinder.Eval(Container.DataItem, "ID_UNID") %>'>
														<%# DataBinder.Eval(Container.DataItem, "ID_UNID") %>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="FE_INGR" HeaderText="Fecha Ingreso" DataFormatString="{0:d}">
                                                <HeaderStyle Width="30%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="FE_VENC" HeaderText="Fecha Vencimiento" DataFormatString="{0:d}">
                                                <HeaderStyle Width="30%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="ST_UBIC" HeaderText="Ubicaci&#243;n">
                                                <HeaderStyle Width="120px"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td align="right" width="50%">
                                            <asp:Button ID="btnVerReporte" runat="server" Text="Ver Reporte" CssClass="btn"></asp:Button></td>
                                        <td align="left">
                                            <asp:Button ID="btnExportar" runat="server" Text="Exportar" CssClass="btn"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td" id="Estado"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
