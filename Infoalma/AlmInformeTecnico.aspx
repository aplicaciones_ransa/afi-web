<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmInformeTecnico.aspx.vb" Inherits="AlmInformeTecnico" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DocumentoAprobacion</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });


        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha


        function vistaPreliminar(sRuta) {
            strReturn = window.showModalDialog('Popup/AlmInformacionTecnicaPDF.aspx?sRuta=' + sRuta, null, 'dialogWidth:800px;dialogHeight:800px;status:no;resizable:no;scroll:no;center:yes');
            return false;
        }



    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table9"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="630" align="center">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                        <td>
                                            <table id="Table4" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="text">Tipo:</td>
                                                    <td class="text">
                                                        <asp:DropDownList ID="dboTipoInforme" runat="server" CssClass ="Text">
                                                            <asp:ListItem Selected="True" Value="1">Informe T�cnico</asp:ListItem>
                                                            <asp:ListItem Value="2">Informe Merma</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="text">Nro Vapor</td>
                                                    <td class="text">
                                                        <asp:TextBox Style="z-index: 0" ID="txtNroVapor" runat="server" Width="100px" class="text" ></asp:TextBox></td>
                                                    <td class="text">Rango de fecha:</td>
                                                    <td class="text">Desde:</td>
                                                    <td>
                                                        <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaInicial" runat="server"></td>
                                                    <td>
                                                        <input id="btnFechaInicial" class="Text" value="..."
                                                                type="button" name="btnFecha1"></td>
                                                    <td class="text">Hasta:</td>
                                                    <td>
                                                        <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaFinal" runat="server"></td>
                                                    <td>
                                                        <input id="btnFechaFinal" class="Text" value="..."
                                                                type="button" name="btnFecha"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td width="80">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar" Width="80px"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    BorderWidth="1px" PageSize="15" AllowPaging="True" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="CODIGO" HeaderText="C&#243;digo">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NOMBRE" HeaderText="Descripci&#243;n">
                                            <HeaderStyle HorizontalAlign="Center" Width="70%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NRO_VAPOR" HeaderText="Nro Vapor">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FCH_CREA" HeaderText="Fecha informe">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Ver">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hplDescipcion" runat="server" ToolTip="Click para abrir PDF" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.NOM_PDF") %>' Target="_blank">
														<img src="Images/Ver.JPG" border="0">
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
