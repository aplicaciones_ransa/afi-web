Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AdmParametro
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objParametro As clsCNParametro
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnEditar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnNuevoPar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnNuevoDom As System.Web.UI.WebControls.Button
    'Protected WithEvents cboDominio As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnEliminar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        Me.lblOpcion.Text = "Mantenimiento de Par�metros"
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_PARAMETROS") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                Me.btnEliminar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de eliminar el registro?')== false) return false;")
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_PARAMETROS"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Session.Add("CodDomin", "")
                Session.Add("CodParm", "")
                llenarDominios()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnNuevoPar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoPar.Click
        Session.Item("Codparm") = ""
        Session.Item("CodDomin") = Me.cboDominio.SelectedValue
        Response.Redirect("AdmParametroNuevo.aspx")
    End Sub

    Private Sub BindDataGrid()
        objParametro = New clsCNParametro
        Dim dtTemporal As DataTable

        Try
            With objParametro
                .gCADMostrarParametros(0, Val(Me.cboDominio.SelectedItem.Value), "1")
                dtTemporal = .fn_devuelveDataTable
            End With

            Me.dgResultado.DataSource = dtTemporal
            Me.dgResultado.DataBind()
            Me.lblError.Text = ""
        Catch ex As Exception
            Me.lblError.Text = "No se pudieron mostrar los par�metros"
        End Try
    End Sub

    Private Sub llenarDominios()
        objParametro = New clsCNParametro
        Dim dttemporal As DataTable
        Me.cboDominio.Items.Clear()
        Try
            With objParametro
                .gCNMostrarDominios("1", 0)
                dttemporal = .fn_devuelveDataTable
            End With
            For Each dr As DataRow In dttemporal.Rows
                Me.cboDominio.Items.Add(New ListItem(dr("DE_LARG_DOMIN"), dr("CO_DOMIN")))
            Next
            Me.cboDominio.SelectedIndex = 0
            BindDataGrid()
        Catch ex As Exception
            Me.lblError.Text = "No se pudieron mostrar los dominios"
            Exit Sub
        End Try
    End Sub

    Private Sub cboDominio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDominio.SelectedIndexChanged
        BindDataGrid()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Session.Item("CodDomin") = Me.cboDominio.SelectedValue
        Response.Redirect("AdmDominioNuevo.aspx")
    End Sub

    Private Sub btnNuevoDom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoDom.Click
        Response.Redirect("AdmDominioNuevo.aspx")
    End Sub

    Public Sub imgeditar_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgeditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgeditar.Parent.Parent, DataGridItem)
            Session.Item("CodDomin") = dgi.Cells(0).Text()
            Session.Item("CodParm") = dgi.Cells(1).Text()
            Response.Redirect("AdmParametroNuevo.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "No se puede modificar al usuario"
        End Try
    End Sub

    Public Sub imgEliminar_click(ByVal sender As Object, ByVal es As System.Web.UI.ImageClickEventArgs)
        Try
            objParametro = New clsCNParametro
            Dim imgEstado As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim intco_par As Integer
            Dim intco_domin As Integer
            dgi = CType(imgEstado.Parent.Parent, DataGridItem)
            intco_par = Val(dgi.Cells(1).Text)
            intco_domin = Val(dgi.Cells(0).Text)
            With objParametro
                .gCNEliminarParametros(intco_par, intco_domin)
                If .fn_devuelveResultado = "X" Then
                    BindDataGrid()
                    Me.lblError.Text = "Se elimin� correctamente el registro"
                End If
            End With
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_PARAMETROS", "ELIMINAR PARAMETRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            Me.lblError.Text = "No se pudo eliminar el registro"
        End Try
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            objParametro = New clsCNParametro
            With objParametro
                .gCNEliminarDominio(Val(Me.cboDominio.SelectedValue))
                If .fn_devuelveResultado = "X" Then
                    Me.lblError.Text = "Se elimin� correctamente el dominio"
                    Me.dgResultado.DataSource = Nothing
                    Me.dgResultado.DataBind()
                    llenarDominios()
                Else
                    Me.lblError.Text = "Error al eliminar el dominio"
                End If
            End With
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_PARAMETROS", "ELIMINAR DOMINIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            Me.lblError.Text = "Error al eliminar el dominio"
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
