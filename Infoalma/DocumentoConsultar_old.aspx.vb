Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoConsultar_old
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objReportes As Reportes = New Reportes
    Private strPathPDF = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strAGD As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadAGD")
    Private strCliente As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadCliente")
    Private strFinanciera As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadFinanciera")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboTipoDocumento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnVerificar As RoderoLib.BotonEnviar
    'Protected WithEvents btnGenerarPDF As RoderoLib.BotonEnviar
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Me.lblError.Text = ""
            PoliticaCache()
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("strMensaje") <> Nothing Then
                    pr_IMPR_MENS(Session.Item("strMensaje"))
                    Session.Remove("strMensaje")
                End If
                If Session.Item("IdTipoEntidad") = "03" And Session.Item("Vencimiento") = 0 Then
                    'Response.Write("<script language='Javascript'>window.open('Popup/AvisoPagarFacturas.aspx','Medios','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=655,height=365');</script>")
                    'Response.Write("<script language='Javascript'>window.open('Popup/Vencimientos.aspx','Vencimientos','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=800,height=600');</script>")
                    Session.Item("Vencimiento") = 1
                End If
                Inicializar()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Inicializar()
        'Dim Dia As String
        'Dim Mes As String
        'Dim Anio As String
        'Dim dttFecha As DateTime
        Session.Add("EstadoDoc", Request.Item("Estado"))
        Me.lblOpcion.Text = "Consultar Documentos Creados"
        'dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
        'Dia = "00" + CStr(dttFecha.Day)
        'Mes = "00" + CStr(dttFecha.Month)
        'Anio = "0000" + CStr(dttFecha.Year)
        'Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        'Dia = "00" + CStr(Now.Day)
        'Mes = "00" + CStr(Now.Month)
        'Anio = "0000" + CStr(Now.Year)
        'Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Me.btnVerificar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de verificar los documentos seleccionados?')== false) return false;")
        Me.btnGenerarPDF.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de generar el Warrant multiple de los documentos seleccionados?')== false) return false;")
        loadTipoDocumento()
        BindDatagrid()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub loadTipoDocumento()
        objfunciones.GetTipoDocumento(Me.cboTipoDocumento, 1)
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable = New DataTable
        Try
            dt = objDocumento.gGetBusquedaDocumentosCreados(Me.txtNroWarrant.Text.Replace("'", ""),
            Me.cboTipoDocumento.SelectedValue, Session.Item("IdTipoEntidad"), Session.Item("IdSico"), Session.Item("IdUsuario"))
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            If Session.Item("IdTipoEntidad") = "03" Then
                Me.dgdResultado.Columns(6).Visible = True
            Else
                Me.dgdResultado.Columns(5).Visible = True
            End If
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Public Sub imgWarrant_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(1).Text())
            Session.Add("NroLib", dgi.Cells(16).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(3).Text())
            Session.Add("PeriodoAnual", dgi.Cells(13).Text())
            Session.Add("Cod_TipOtro", dgi.Cells(14).Text())
            Session.Add("Cod_Doc", dgi.Cells(17).Text())
            Session.Add("IdSico_depositante", dgi.Cells(20).Text())
            Response.Redirect("DocumentoPagUnoWarrant.aspx?Pag=0", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgEndoso_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(1).Text())
            Session.Add("NroLib", dgi.Cells(16).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(3).Text())
            Session.Add("PeriodoAnual", dgi.Cells(13).Text())
            Session.Add("Cod_TipOtro", dgi.Cells(14).Text())
            Response.Redirect("DocumentoPagDos.aspx?Pag=0", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub lnkDescripcion_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim imgEditar As LinkButton = CType(sender, LinkButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(1).Text())
            Session.Add("NroLib", dgi.Cells(16).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(3).Text())
            Session.Add("PeriodoAnual", dgi.Cells(13).Text())
            Session.Add("Cod_TipOtro", dgi.Cells(14).Text())
            Session.Add("Cod_Doc", dgi.Cells(17).Text())

            Dim objConexion As SqlConnection
            Dim objTrans As SqlTransaction
            objConexion = New SqlConnection(strConn)
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()
            Try
                objReportes.GeneraSello(dgi.Cells(17).Text(), strPDFPath, strPathFirmas, objTrans)
                Me.lblError.Text = objReportes.strMensaje()
                If Me.lblError.Text <> "" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Exit Sub
                End If
                objTrans.Commit()
                objTrans.Dispose()
            Catch ex As Exception
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = " Primero debes revisar el DCR antes de dar tu VB:  " & ex.ToString
                Exit Sub
            End Try

            If objDocumento.gGetEstadoImpresionCreados(Session.Item("Cod_Doc")) = False Then
                '-------------------------------------------------------------------
                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster.CopiaDocumentosInfodepsaCreados(Session.Item("Cod_Doc"))
            End If
            Response.Redirect("DocumentoPDF.aspx?var=2", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim blnFlgAlmacenera As Boolean
        Dim blnFlgCliente As Boolean
        Dim blnFlgFinanciera As Boolean
        Dim blnFlgPDFGenerado As Boolean
        Dim strCodAsociado As String
        Dim strTipoEntidad As String
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            blnFlgAlmacenera = e.Item.DataItem("FLG_APRALMC")
            blnFlgCliente = e.Item.DataItem("FLG_APRCLIE")
            blnFlgFinanciera = e.Item.DataItem("FLG_APRFINA")
            blnFlgPDFGenerado = e.Item.DataItem("FLG_PDFGENE")
            strTipoEntidad = Session.Item("IdTipoEntidad")
            strCodAsociado = e.Item.DataItem("COD_ENTASOC")
            CType(e.Item.FindControl("chkDepsa"), CheckBox).Attributes.Add("onclick", "ActivaBoton()")
            CType(e.Item.FindControl("chkFinanciador"), CheckBox).Attributes.Add("onclick", "ActivaBoton()")
            CType(e.Item.FindControl("chkCliente"), CheckBox).Attributes.Add("onclick", "ActivaBoton()")
            CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Attributes.Add("onclick", "ActivaGeneraPDF()")
            If strTipoEntidad = strAGD And blnFlgAlmacenera = False And blnFlgFinanciera = False And blnFlgPDFGenerado = False And
            blnFlgCliente = False And Session.Item("IdTipoUsuario") = "03" Then
                CType(e.Item.FindControl("chkDepsa"), CheckBox).Enabled = True
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = False
                e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
            ElseIf strTipoEntidad = strCliente And blnFlgCliente = False And blnFlgAlmacenera = True And blnFlgFinanciera = False And
                blnFlgPDFGenerado = False And (Session.Item("IdTipoUsuario") = "01" Or Session.Item("IdTipoUsuario") = "03") Then
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = False
                CType(e.Item.FindControl("chkCliente"), CheckBox).Enabled = True
                e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
            ElseIf strTipoEntidad = strFinanciera And blnFlgFinanciera = False And blnFlgCliente = True And blnFlgAlmacenera = True And
            blnFlgPDFGenerado = False And (Session.Item("IdTipoUsuario") = "01" Or
            Session.Item("IdTipoUsuario") = "03") And e.Item.DataItem("FLG_GRBENDO") = False Then
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = False
                e.Item.BackColor = System.Drawing.Color.FromArgb(255, 190, 120)
                CType(e.Item.FindControl("imgEditar2"), ImageButton).Visible = True
            ElseIf strTipoEntidad = strFinanciera And blnFlgFinanciera = False And blnFlgCliente = True And blnFlgAlmacenera = True And
                blnFlgPDFGenerado = False And (Session.Item("IdTipoUsuario") = "01" Or
                Session.Item("IdTipoUsuario") = "03") And e.Item.DataItem("FLG_GRBENDO") = True Then
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = False
                CType(e.Item.FindControl("chkFinanciador"), CheckBox).Enabled = True
                e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
            ElseIf strTipoEntidad = strCliente And blnFlgFinanciera = True And blnFlgCliente = True And blnFlgAlmacenera = True And
                blnFlgPDFGenerado = False And (Session.Item("IdTipoUsuario") = "01" Or Session.Item("IdTipoUsuario") = "03") And Session.Item("IdSico") = strCodAsociado Then
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = True
                CType(e.Item.FindControl("chkCliente"), CheckBox).Enabled = False
                e.Item.BackColor = System.Drawing.Color.FromArgb(210, 230, 250)
            ElseIf strTipoEntidad = strCliente And blnFlgFinanciera = True And blnFlgCliente = True And blnFlgAlmacenera = True And
                blnFlgPDFGenerado = False And (Session.Item("IdTipoUsuario") = "01" Or Session.Item("IdTipoUsuario") = "03") And strCodAsociado = "" Then
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = True
                CType(e.Item.FindControl("chkCliente"), CheckBox).Enabled = False
                e.Item.BackColor = System.Drawing.Color.FromArgb(210, 230, 250)
            Else
                CType(e.Item.FindControl("chkItemGenerado"), CheckBox).Enabled = False
                CType(e.Item.FindControl("chkDepsa"), CheckBox).Enabled = False
                CType(e.Item.FindControl("chkCliente"), CheckBox).Enabled = False
                CType(e.Item.FindControl("chkFinanciador"), CheckBox).Enabled = False
            End If

            CType(e.Item.FindControl("imgEditar1"), ImageButton).Visible = False

            If e.Item.DataItem("FLG_GRBENDO") = True Then
                CType(e.Item.FindControl("imgEditar2"), ImageButton).Visible = True
            End If
        End If
    End Sub

    Private Sub btnGenerarPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarPDF.Click
        Try
            Dim strIds As String = ""
            Dim strNrosWarrants As String = ""
            Dim strCodEntFina As String = ""
            Dim strFlgDobleEndoso As String = ""
            Dim strCliente As String
            Dim strCodigoCliente As String
            Dim strCodFina As String()
            Dim strDobleEndoso As String()
            Dim dgItem As DataGridItem
            Dim dt As New DataTable
            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim strBody As String
            objReportes = New Reportes
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl("chkItemGenerado"), CheckBox).Checked = True And CType(dgItem.FindControl("chkItemGenerado"), CheckBox).Enabled = True Then
                    strIds += dgItem.Cells(17).Text & "-"
                    strNrosWarrants += dgItem.Cells(1).Text.Trim & "-"
                    strCodEntFina += dgItem.Cells(18).Text.Trim & "-"
                    strFlgDobleEndoso += dgItem.Cells(19).Text.Trim & "-"
                    strCliente += dgItem.Cells(5).Text.Trim
                    strCodigoCliente += dgItem.Cells(20).Text.Trim
                End If
            Next
            If strIds = "" Then
                pr_IMPR_MENS("No ha seleccionado ningun documento")
                Exit Sub
            End If
            strDobleEndoso = Split(strFlgDobleEndoso.Substring(0, strFlgDobleEndoso.Length - 1), "-")
            If strDobleEndoso.Length > 1 Then
                For i As Integer = 0 To strDobleEndoso.Length - 1
                    If strDobleEndoso(i) = True Then
                        pr_IMPR_MENS("Debe seleccionar solo un Warrant con doble endoso")
                        Exit Sub
                    End If
                Next
            End If
            If strDobleEndoso(0) = True Then
                strFlgDobleEndoso = "1"
            Else
                strFlgDobleEndoso = "0"
            End If

            strCodFina = Split(strCodEntFina.Substring(0, strCodEntFina.Length - 1), "-")
            For i As Integer = 0 To strCodFina.Length - 1
                If strCodFina(0) <> strCodFina(i) Then
                    pr_IMPR_MENS("Debe seleccionar Warrants con el mismo financiador para poder generar PDF")
                    Exit Sub
                End If
            Next
            If strIds = "" Then
                objDocumento.strResultado = "No ha seleccionado ningun documento"
            Else
                objReportes.GeneraPDFWarrant(strIds.Substring(0, strIds.Length - 1), strPathPDF, strPathFirmas,
                True, False, "03", Session.Item("IdUsuario"), strFlgDobleEndoso,
                strNrosWarrants.Substring(0, strNrosWarrants.Length - 1))
                If objReportes.strMensaje = "0" Then
                    dt = objDocumento.gVerificarDocumentos(True, Session.Item("IdTipoEntidad"), strIds.Substring(0, strIds.Length - 1), Session.Item("IdUsuario"))
                    strBody = "Estimados Se�ores:<br><br>" &
                    "Tiene documento(s) pendiente(s) de aprobaci�n" &
                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNrosWarrants & "</FONT></STRONG><br>"
                    strBody += "Depositante: <STRONG><FONT color='#330099'>" & strCliente & "</FONT></STRONG>"
                    strBody += "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>"
                    If strFlgDobleEndoso = 1 Then
                        objfunciones.EnviaMailEmpresa(strCodigoCliente, "", "Tiene Warrant(s). pendiente(s) de aprobaci�n.  - " & strCliente, strBody)
                    End If
                Else
                    pr_IMPR_MENS(objReportes.strMensaje)
                    dt = Nothing
                    Exit Sub
                End If
            End If
            pr_IMPR_MENS("Se gener� correctamente, pas� a Emitidos")
            dt = Nothing
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "7", "GENERAR WARRANT MULTIPLE", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Catch ex As Exception
            pr_IMPR_MENS("Se cancel� el proceso debido al siguiente error: " & ex.Message)
        Finally
            BindDatagrid()
        End Try
    End Sub

    Private Sub btnVerificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerificar.Click
        Try
            If CStr(Session.Item("IdTipoEntidad")) = strAGD Then
                IngresarConsultas("chkDepsa")
            End If
            If CStr(Session.Item("IdTipoEntidad")) = strFinanciera Then
                IngresarConsultas("chkFinanciador")
            End If
            If CStr(Session.Item("IdTipoEntidad")) = strCliente Then
                IngresarConsultas("chkCliente")
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "7", "VERIFICAR WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub IngresarConsultas(ByVal strTipoCheck As String)
        Dim strIds As String = ""
        Dim strNroWarrant As String = ""
        Dim strMontoWarrant As String = ""
        Dim strNombrePDFs As String = ""
        Dim strIdEntidad As String
        Dim dgItem As DataGridItem
        Dim dt As New DataTable
        Try
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl(strTipoCheck), CheckBox).Checked = True And CType(dgItem.FindControl(strTipoCheck), CheckBox).Enabled = True Then
                    strIds += dgItem.Cells(17).Text & "-"
                    '--------------------------------------------------------------------------------------------
                    If strTipoCheck = "chkDepsa" Then
                        Dim objConexion As SqlConnection
                        Dim objTrans As SqlTransaction
                        objConexion = New SqlConnection(strConn)
                        objConexion.Open()
                        objTrans = objConexion.BeginTransaction()
                        Try
                            objReportes.GeneraVB(dgItem.Cells(17).Text, strPDFPath, strPathFirmas, Session.Item("IdUsuario"), "2", objTrans)
                            Me.lblError.Text = objReportes.strMensaje()
                            'Me.chkDCR.Enabled = False
                            objTrans.Commit()
                            objTrans.Dispose()
                        Catch ex As Exception
                            objTrans.Rollback()
                            objTrans.Dispose()
                            Me.lblError.Text = " Primero debes revisar el DCR antes de dar tu VB:  " & ex.ToString
                            Exit Sub
                        End Try
                        If objDocumento.gCerrarComprobante(dgItem.Cells(3).Text, dgItem.Cells(1).Text, Session.Item("UsuarioLogin")) = False Then
                            pr_IMPR_MENS(objDocumento.strResultado)
                            BindDatagrid()
                            Exit Sub
                        End If
                    End If
                    '--------------------------------------------------------------------------------------
                End If
            Next
            If strIds = "" Then
                objDocumento.strResultado = "No ha seleccionado ningun documento"
            Else
                dt = objDocumento.gVerificarDocumentos(False, Session.Item("IdTipoEntidad"), strIds.Substring(0, strIds.Length - 1), Session.Item("IdUsuario"))
                Select Case objDocumento.strResultado
                    Case "0"
                        objDocumento.strResultado = "Se verific� correctamente"
                        strIdEntidad = dt.Rows(0)("IdEntidad")
                        For i As Integer = 0 To dt.Rows.Count - 1
                            If strIdEntidad = dt.Rows(i)("IdEntidad") Then
                                strNroWarrant += dt.Rows(i)("NRO_DOCU") & ", "
                                strMontoWarrant += dt.Rows(i)("MON_ENDO") & " " & String.Format("{0:##,##0.00}", dt.Rows(i)("VAL_ENDO")) & ", "
                                strNombrePDFs += strPathPDF & dt.Rows(i)("NOM_PDF") & "-"
                            Else
                                If strTipoCheck = "chkFinanciador" Then
                                    objfunciones.EnviaMailEmpresa(strIdEntidad, "", " Generar Warrants - " & dt.Rows(i - 1)("NOM_ENTI"),
                                    "Estimados Se�ores:<br><br>" &
                                    "Los siguientes Warrants estan a la espera de su Generaci�n (En documentos creados)" &
                                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto financiado: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                                    "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(i - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>")
                                End If
                                If strTipoCheck = "chkCliente" Then
                                    objfunciones.EnviaMailEmpresa(strIdEntidad, "", "Endoso y Verificaci�n de Warrants - " & dt.Rows(i - 1)("NOM_ENTI"),
                                    "Estimados Se�ores:<br><br>" &
                                    "Los siguientes Warrants estan a la espera de su Endoso y Verificaci�n (En documentos creados)" &
                                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto Warrant: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                                    "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(i - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombrePDFs.Substring(0, strNombrePDFs.Length - 1), Session.Item("IdSico"))
                                End If
                                If strTipoCheck = "chkDepsa" Then
                                    objfunciones.EnviaMailEmpresa(strIdEntidad, "", " Verificaci�n de Warrants - " & dt.Rows(i - 1)("NOM_ENTI"),
                                    "Estimados Se�ores:<br><br>" &
                                    "Los siguientes Warrants estan a la espera de su verificaci�n (En documentos creados)" &
                                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto Warrant: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                                    "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(i - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>")
                                End If
                                strNroWarrant = dt.Rows(i)("NRO_DOCU") & ", "
                                strMontoWarrant = dt.Rows(i)("MON_ENDO") & " " & String.Format("{0:##,##0.00}", dt.Rows(i)("VAL_ENDO")) & ", "
                                strNombrePDFs = strPathPDF & dt.Rows(i)("NOM_PDF") & "-"
                                strIdEntidad = dt.Rows(i)("IDENTIDAD")
                            End If
                        Next
                        If strTipoCheck = "chkFinanciador" Then
                            objfunciones.EnviaMailEmpresa(strIdEntidad, "", " Generar Warrants - " & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI"),
                            "Estimados Se�ores:<br><br>" &
                            "Los siguientes Warrants estan a la espera de su Generaci�n (En documentos creados)" &
                            ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto financiado: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                            "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                            "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI>")
                        End If
                        If strTipoCheck = "chkCliente" Then
                            objfunciones.EnviaMailEmpresa(strIdEntidad, "", " Endoso y Verificaci�n de Warrants - " & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI"),
                           "Estimados Se�ores:<br><br>" &
                            "Los siguientes Warrants estan a la espera de su Endoso y Verificaci�n (En documentos creados)" &
                            ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto Warrant: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                            "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                            "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombrePDFs.Substring(0, strNombrePDFs.Length - 1), Session.Item("IdSico"))
                        End If
                        If strTipoCheck = "chkDepsa" Then
                            objfunciones.EnviaMailEmpresa(strIdEntidad, "", " Verificaci�n de Warrants - " & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI"),
                            "Estimados Se�ores:<br><br>" &
                            "Los siguientes Warrants estan a la espera de su verificaci�n (En documentos creados)" &
                            ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarrant.Substring(0, strNroWarrant.Length - 2) & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto Warrant: <STRONG><FONT color='#330099'>" & strMontoWarrant.Substring(0, strMontoWarrant.Length - 2) & "</FONT></STRONG><br>" &
                            "Depositante: <STRONG><FONT color='#330099'>" & dt.Rows(dt.Rows.Count - 1)("NOM_ENTI") & "</FONT></STRONG>" &
                            "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>")
                        End If
                    Case "1"
                        objDocumento.strResultado = "Ocurrio un error en la funci�n split"
                    Case "2"
                        objDocumento.strResultado = "No se pudo insertar en la tabla temporal"
                    Case "3"
                        objDocumento.strResultado = "No se pudo actualizar en la BD"
                    Case "4"
                        objDocumento.strResultado = "Alguno de los registros ya fuer�n verificados o les falta asignar una entidad financiera"
                    Case "5"
                        objDocumento.strResultado = "Falta el visto bueno en un DCR"
                End Select
            End If
            pr_IMPR_MENS(objDocumento.strResultado)
            BindDatagrid()
            dt = Nothing
        Catch ex As Exception
            lblError.Text &= " " & ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objfunciones Is Nothing) Then
            objfunciones = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
