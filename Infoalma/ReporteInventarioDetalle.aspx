﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReporteInventarioDetalle.aspx.vb" Inherits="ReporteInventarioDetalle" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Reporte Inevntario Detalle</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="JavaScript">

        $(function () {
            $("#txtFechaIniRecep").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIniRecep").click(function () {
                $("#txtFechaIniRecep").datepicker('show');
            });

            $("#txtFechaFinRecep").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinRecep").click(function () {
                $("#txtFechaFinRecep").datepicker('show');
            });
        });

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }

        function Todos(ckall, citem) {
            var actVar = ckall.checked;
            for (i = 0; i < Form1.length; i++) {
                if (Form1.elements[i].type == "checkbox") {
                    //si los check estan habilitados
                    if (Form1.elements[i].disabled == false) {
                        //Checker all 
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
        }

    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table9"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="6">
                                <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%" align="center">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" background="Images/table_r2_c1.gif" width="6"></td>
                                        <td style="height: 19px">


                                            <table id="Table2" border="0" cellpadding="0" cellspacing="4" width="100%">
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Número Operación</td>
                                                    <td class="Text" style="width: 1%">
                                                        :</td>
                                                    <td align="left" colspan="4">
                                                        <asp:Label ID="lblTrader" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Cliente</td>
                                                    <td class="Text" style="width: 1%">
                                                        :</td>
                                                    <td align="left" colspan="4">
                                                        <asp:DropDownList ID="cboCliente" runat="server" CssClass="Text" Width="500px" Enabled="False" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Almacén</td>
                                                    <td class="Text" style="width: 1%;">
                                                        :</td>
                                                    <td align="left" colspan="4">
                                                        <asp:DropDownList ID="cboAlmacen"  Enabled="False" runat="server" CssClass="Text" Width="500px">
                                                        </asp:DropDownList>
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text" style="height: 13px">
                                                        Conductor</td>
                                                    <td class="Text" style="height: 13px">
                                                        :</td>
                                                    <td align="left" colspan="4" style="height: 13px">
                                                        <asp:Label ID="lblConductor" runat="server" Enabled="False" CssClass="Text" Width="500px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Empresa Transporte</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left" colspan="4">
                                                        <asp:TextBox ID="txtEmprTransporte" runat="server" Enabled="False" MaxLength="100" CssClass="TxtMayuscula" Width="500px"></asp:TextBox>                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Mercadería</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtMercaderia" runat="server" Enabled="False" CssClass="TxtMayuscula" MaxLength="200" Width="250px"></asp:TextBox>
                                                    </td>
                                                    <td align="left" class="Text">
                                                        Vapor</td>
                                                    <td align="left" style="width: 1%;">
                                                        :</td>
                                                    <td align="left">
                                                        &nbsp;<asp:TextBox ID="txtVapor" runat="server" Enabled="False" CssClass="TxtMayuscula" MaxLength="50" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Cantidad Solicitada</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtCantSolicitada" runat="server" Enabled="False" CssClass="TxtMayuscula" MaxLength="50" Width="100px"></asp:TextBox>
                                                        
                                                    </td>
                                                    <td align="left" class="Text">
                                                        Dua</td>
                                                    <td align="left">
                                                        :</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtDua" runat="server" CssClass="TxtMayuscula" Enabled="False" MaxLength="50" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Fecha&nbsp;Inicio&nbsp;Ingreso</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        &nbsp;<input id="txtFechaIniRecep" Enabled="False" runat="server" class="Text" maxlength="10" name="txtFechaIniRecep" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"  size="8" />
                                                        <input id="btnFechaIniRecep" Enabled="False" runat="server" class="Text" name="btnFechaIniRecep" onclick="CapturaFecha(txtFechaIniRecep)" type="button" value="..." />
                                                    </td>
                                                    <td align="left" class="Text">
                                                        Fecha&nbsp;Termino&nbsp;Ingreso</td>
                                                    <td align="left" class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        <input id="txtFechaFinRecep" runat="server" Enabled="False" class="Text" maxlength="10" name="txtFechaFinRecep" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);" size="8" />
                                                        <input id="btnFechaFinRecep" Enabled="False" class="Text" runat="server" name="btnFechaFinRecep" onclick="CapturaFecha(txtFechaFinRecep)" type="button" value="..." />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Unidad de Medida</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        <asp:DropDownList ID="cboUnidadMedida" Enabled="False" runat="server" CssClass="Text" Width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" class="Text">
                                                        Estado</td>
                                                    <td align="left" class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        <asp:Label ID="lblEstado" Enabled="False" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="Text">
                                                        Número DCR</td>
                                                    <td class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        <asp:TextBox ID="txtDCR" runat="server" Enabled="False" CssClass="TxtMayuscula"  MaxLength="100" Width="100px"></asp:TextBox>&nbsp; Conf.Peso :
                                                        <asp:CheckBox ID="ChckConfPeso" Enabled="False" runat="server" /></td>
                                                    <td align="left" class="Text">
                                                        Número Warrant</td>
                                                    <td align="left" class="Text">
                                                        :</td>
                                                    <td align="left" class="Text">
                                                        <asp:TextBox ID="txtWarrant" runat="server" Enabled="False" CssClass="TxtMayuscula"  MaxLength="100" Width="100px"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                            



                                            &nbsp;
                                        </td>
                                        <td style="height: 19px" background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center">
                                <asp:Button ID="btnExportar" runat="server" CssClass="Text" Width="80px" Text="Exportar"></asp:Button><asp:Button ID="btnRegresar" runat="server" CssClass="Text" Width="80px" Text="Regresar" CausesValidation="False"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" valign="top" align="left">Resultado:</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                
                                
                                
                                <asp:DataGrid ID="gvTraders" runat="server" CssClass="gv" Width="100%" BorderWidth="1px" PageSize="30"
                                    AutoGenerateColumns="False" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    
                                    <Columns>
                                        <asp:BoundColumn DataField="NU_SECU" HeaderText="Sec">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SECU" HeaderText="Sec para Anular">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_ING_MERC" HeaderText="Fecha Ingreso">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_SALI_MERC" HeaderText="Fecha Salida">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TEMP" HeaderText="Temperatura" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_HUME" HeaderText="Humedad" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_PLAC" HeaderText="Placa">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_DOCU_CLIE" HeaderText="Numero Gu&#237;a">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TICK" HeaderText="Ticket Conductor">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_INGR_CLIE" HeaderText="Ingreso Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SALI_CLIE" HeaderText="Salida Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_INGR_COND" HeaderText="Ingreso Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SALI_COND" HeaderText="Salida Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_ACU_CLIE" HeaderText="Acumulado Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_ACU_COND" HeaderText="Acumulado Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_DOCU_RETI" HeaderText="DOR">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_USUA_MODI" HeaderText="Fecha Modif.">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_USUA_CREA" HeaderText="Ult. Usuario Modif.">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_ESTA_ANUL" HeaderText="Anulado">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_ESTA_FINA" HeaderText="Finalizado">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                     <%--   <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR Generado">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>             --%>                 
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table style="width: 376px; height: 37px" id="Table4" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td style="height: 16px" class="td" colspan="8">Leyenda</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="gainsboro" width="30%"></td>
                                        <td class="Leyenda" width="20%">Anulado.</td>
                                        <td bgcolor="#c8dcf0" width="30%"></td>
                                        <td class="Leyenda" width="30%">Finalizado.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
