<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmPedidoNuevo_W.aspx.vb" Inherits="AlmPedidoNuevo_W" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Retiro Nuevo</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaVenc").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVenc").click(function () {
                $("#txtFechaVenc").datepicker('show');
            });

        });

        function CalcularPeso(sCntRetirar, sBultoRetirar, sCntDisponible, sBultoDisponible, sTextboxOriginal) {
            if (parseFloat(sCntRetirar) > parseFloat(sCntDisponible)) {
                alert("La cantidad a retirar debe ser menor o igual a: " + sCntDisponible);
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
                return
            }
            if (parseFloat(sCntRetirar) < 0) {
                alert("La cantidad a retirar debe ser mayor a cero ");
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
                return
            }
            //if (sCntRetirar.length == 0)
            //{
            //alert("La cantidad a retirar debe ser mayor a cero ");
            //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
            //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
            //return
            //}
            //document.getElementById(sBultoRetirar).disabled = false;
            //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible)*parseFloat(sCntRetirar)/parseFloat(sCntDisponible);
        }
        function CalcularCantidad(sCntRetirar, sBultoRetirar, sCntDisponible, sBultoDisponible, sTextboxOriginal) {
            if (parseFloat(sCntRetirar) > parseFloat(sBultoDisponible)) {
                alert("El bulto a retirar debe ser menor o igual a: " + sBultoDisponible);
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = sCntDisponible;
                //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
                return
            }
            if (parseFloat(sCntRetirar) < 0) {
                alert("El bulto a retirar debe ser mayor a cero ");
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
                return
            }
            //if (sCntRetirar.length == 0)
            //{
            //alert("El bulto a retirar debe ser mayor a cero ");
            //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible);
            //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
            //return
            //}
            //document.getElementById(sBultoRetirar).disabled = false;
            //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible)*parseFloat(sCntRetirar)/parseFloat(sBultoDisponible);
        }
    </script>
    <script type="text/javascript">
            if (history.forward(1)) {
                location.replace(history.forward(1));
            }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <!-- Text='<%# String.Format("{0:N3}",DataBinder.Eval(Container, "DataItem.NU_UNID_SALD")) %>'-->
                    <!--Text='<%# String.Format("{0:N3}",DataBinder.Eval(Container, "DataItem.NU_UNID_PESO")) %>'-->
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td width="100%">
                                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" rowspan="6">
                                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                            </td>
                                            <td valign="top" width="100%" align="center">
                                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670">
                                                    <tr>
                                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                                        <td>
                                                            <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="Text" width="10%">Almacen :</td>
                                                                    <td width="62%" colspan="4">
                                                                        <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" AutoPostBack="True" Width="454px"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text" width="10%">C�d. Producto:</td>
                                                                    <td width="13%">
                                                                        <asp:TextBox ID="txtCodProducto" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                                    <td class="Text" width="10%">Referencia :</td>
                                                                    <td width="13%">
                                                                        <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text" Width="104px">
                                                                            <asp:ListItem Value="0">Estado</asp:ListItem>
                                                                            <asp:ListItem Value="1">Desc. Mercaderia</asp:ListItem>
                                                                        </asp:DropDownList><asp:TextBox Style="z-index: 0" ID="txtNroReferencia" runat="server" CssClass="Text" Width="96px"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text" width="10%">Lote&nbsp;:</td>
                                                                    <td width="15%">
                                                                        <asp:TextBox ID="txtLote" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                                    <td class="Text" width="10%">Fech Venc &lt;=</td>
                                                                    <td width="15%">
                                                                        <input id="txtFechaVenc" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                            maxlength="10" size="6" name="txtFechaVenc" runat="server">
                                                                        <input id="btnFechaVenc" class="text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <asp:HyperLink ID="hlkSolicitud" runat="server" CssClass="Text" NavigateUrl="AlmRetiroSolicitud_W.aspx"
                                                    Target="_parent">Ver solicitud antes de enviar</asp:HyperLink></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center">
                                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                    <tr>
                                                        <td width="80">
                                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar" CausesValidation="False"></asp:Button></td>
                                                        <td width="80">
                                                            <asp:Button ID="btnAgregar" runat="server" CssClass="Text" Text="Agregar Items"></asp:Button></td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td height="200" valign="top">
                                                <!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
                                                <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" OnPageIndexChanged="Change_Page"
                                                    PageSize="30" AllowPaging="True" BorderColor="Gainsboro" AutoGenerateColumns="False">
                                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Sel">
                                                            <HeaderStyle Width="30px"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <table id="Table5" cellspacing="0" cellpadding="0" width="30" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkItem" runat="server" CssClass="Text"></asp:CheckBox></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="COD_PROD" HeaderText="Cod.Producto">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DSC_PROD" HeaderText="Dsc.Mercader&#237;a"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="LOTE" HeaderText="Lote">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="FCH_VENC" HeaderText="Fecha Vencimiento">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="COD_RETE" HeaderText="Cod. Retencion">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DSC_RETE" HeaderText="Estado">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="UNI_MEDI" HeaderText="Uni. Medida">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CNT_SALD" HeaderText="Cnt. Disponible">
                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Cnt. Retirar">
                                                            <HeaderStyle Width="80px"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtUnidRetirar" runat="server" CssClass="Text" Width="80px" Font-Bold="True"
                                                                    ForeColor="blue"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid><!--</div>--></td>
                                        </tr>
                                        <tr>
                                            <td class="Text" valign="top" align="center">
                                                <table id="tblLeyenda" border="0" cellspacing="4" cellpadding="0" width="50%">
                                                    <tr>
                                                        <td class="td" colspan="4">Leyenda</td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="thistle" width="30%"></td>
                                                        <td class="Leyenda" width="20%" align="center">�tem observado
                                                        </td>
                                                        <td bgcolor="silver" width="30%" align="center"></td>
                                                        <td class="Leyenda" width="20%" align="center">�tem solicitado</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
