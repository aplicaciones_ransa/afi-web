Public Class DocumentoReporte
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents ltrReporte As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnCerrar As System.Web.UI.WebControls.Button


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Me.ltrReporte.Text = "<iframe id='fraReporte' style='WIDTH: 750px; HEIGHT: 500px' name='fraReporte' src='visor.aspx?var=2' runat='server'></iframe>"
        Else
            Response.Redirect("Salir.aspx?caduco=1", False)
        End If
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Response.Redirect("DocumentoPagUno.aspx", False)
    End Sub
End Class
