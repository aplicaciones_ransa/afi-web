<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoAprobacion.aspx.vb" Inherits="DocumentoAprobacion" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DocumentoAprobacion</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
		//-----------------------------------Fin formatear fecha	
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table9"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="630" align="center">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                        <td>
                                            <table id="Table4" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="text">TipoDoc:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoDocumento" runat="server" CssClass="Text" Width="120px"></asp:DropDownList></td>
                                                    <td class="text">Nro.Warrant:</td>
                                                    <td class="text">
                                                        <asp:TextBox ID="txtNroWarrant" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                    <td class="text">Desde:</td>
                                                    <td>
                                                        <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="Text" value="..."
                                                                type="button" name="btnFecha"></td>
                                                    <td class="text">Hasta:</td>
                                                    <td>
                                                        <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaFinal" runat="server"><input id="btnFechaFinal" class="Text" value="..."
                                                                type="button" name="btnFecha"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Deposita:</td>
                                                    <td colspan="4">
                                                        <asp:DropDownList ID="cboDepositante" runat="server" CssClass="Text" Width="320px"></asp:DropDownList></td>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Financ:</td>
                                                    <td colspan="4">
                                                        <asp:DropDownList ID="cboFinanciador" runat="server" CssClass="Text" Width="320px"></asp:DropDownList></td>
                                                    <td class="text">Situaci�n:</td>
                                                    <td colspan="2">
                                                        <asp:DropDownList ID="cboSituacion" runat="server" CssClass="Text">
                                                            <asp:ListItem Value="T">---------Todos-----------</asp:ListItem>
                                                            <asp:ListItem Value="C">Contabilizados</asp:ListItem>
                                                            <asp:ListItem Value="N">No Contabilizados</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td width="80">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:Button></td>
                                        <td width="80">
                                            <asp:Button ID="btnExportar" runat="server" CssClass="btn" Width="80px" Text="Exportar"></asp:Button></td>
                                        <td width="80">
                                            <asp:Button ID="btnFirmar" runat="server" CssClass="btn" Width="80px" Text="Firmar"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="300" valign="top">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
                                    OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="15" BorderWidth="1px" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>

                                          <asp:TemplateColumn HeaderText="Sel">
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSel" runat="server" CssClass="Text" Enabled="True"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>                                       

                                        <asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro Warrant">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NRO_LIBE1" HeaderText="Nro Libe/Secu">
                                             <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="COD_TIPDOC" HeaderText="Cod Tipo Documento"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NOM_TIPDOC" HeaderText="Tipo Documento">
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FCH_FIRM" HeaderText="Fecha Aprobaci&#243;n">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="NOM_ENTFINA" HeaderText="Financiador"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="NOM_ENTI" HeaderText="Depositante"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DSC_MERC" HeaderText="Dsc Mercader&#237;a">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <%--<asp:TemplateColumn HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDescripcion" OnClick="lnkDescripcion_Click" runat="server" CssClass="Text"
                                                    ToolTip="Ver PDF">
														<%# DataBinder.Eval(Container, "DataItem.DSC_MERC") %>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>--%>
                                        <asp:BoundColumn HeaderText="Monto">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unidad">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_UNID" HeaderText="Cantidad">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHACREACION" HeaderText="Fecha Creaci&#243;n">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PLANTA" HeaderText="PLANTA">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="AGD">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDepsa" runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRALMC") %>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Cliente">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkCliente" runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRCLIE") %>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Banco">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkFinanciador" runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRFINA") %>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        
                                        <asp:TemplateColumn HeaderText="Ver">
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                            <ItemTemplate>
                                                <table id="Table3" cellspacing="0" cellpadding="0" width="20" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
                                                                CausesValidation="False"></asp:ImageButton></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        
                                        
                                        <asp:BoundColumn Visible="False" DataField="PRD_DOCU"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="COD_TIPOTRO"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="LUG_ENDO"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="NRO_LIBE"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="COD_DOC"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="CO_UNID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="DSC_MERC"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="FLG_FIRMA"></asp:BoundColumn>                              
                                          <asp:TemplateColumn HeaderText="Impr" Visible="False">
                                               <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                              <ItemTemplate>
                                                  <asp:CheckBox ID="cbox_impr" runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_VAL_IMPR") %>'></asp:CheckBox>
                                              </ItemTemplate>
                                          </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table id="Table2" border="0" cellspacing="4" cellpadding="0" width="630">
                                    <tr>
                                        <td class="td" colspan="8">Leyenda</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="thistle" width="8%"></td>
                                        <td class="Leyenda" width="25%">Retiro Web.</td>
                                        <td bgcolor="#f5dc8c" width="8%"></td>
                                        <td class="Leyenda" width="25%">Documentos por Firma</td>
                                        <td bgcolor="lightblue" width="8%"></td>
                                        <td class="Leyenda" width="25%">Documentos Cerrados</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
