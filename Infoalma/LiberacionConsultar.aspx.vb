Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class LiberacionConsultar
    Inherits System.Web.UI.Page
    Private objUsuario As Usuario = New Usuario
    Private objDocumento As Documento = New Documento
    Private objSICO As SICO = New SICO
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroLiberacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnEliminar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "20") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "20"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("NroRetiro") <> Nothing Then
                    pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("NroRetiro"))
                    Session.Remove("NroRetiro")
                End If
                Me.btnEliminar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de eliminar los registros seleccionados?')== false) return false;")
                Me.lblOpcion.Text = "Consultar Liberaciones"
                loadEstado()
                Me.cboEstado.SelectedValue = "07"
                VistoBuenoXUsuario()
                BindDatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub loadEstado()
        objfunciones.GetLlenaEstado(Me.cboEstado, "G")
    End Sub

    Private Sub BindDatagrid()
        dt = New DataTable
        Try
            dt = objDocumento.gGetBusquedaLiberacion(Me.txtNroWarrant.Text.Replace("'", ""), Me.cboEstado.SelectedValue, Me.txtNroLiberacion.Text.Replace("'", ""), Session.Item("IdTipoEntidad"), Session.Item("IdSico"))
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)

            Dim satValida As Boolean = False
            Dim strCont As Integer = 1
            Dim strCadCodigoDocu As String
            Dim dtIngreso001 As DataTable = New DataTable

            dgi = CType(imgEditar.Parent.Parent, DataGridItem)

            '=== Crear Tabla Temporal ===
            If Not dtIngreso001.Rows.Count = 0 Then
                dtIngreso001.Rows.Clear()
            Else
                dtIngreso001 = crearTabla()
            End If

            '=== Concatenar numero de documento y tipo de documento. ===
            If strCont = 1 Then
                strCadCodigoDocu = "" & dgi.Cells(1).Text.ToString & dgi.Cells(3).Text.ToString
            Else
                strCadCodigoDocu = "," & dgi.Cells(1).Text.ToString & dgi.Cells(3).Text.ToString
            End If



            '=== cargar Datos a Tabla Temporal ===
            Dim dr As DataRow
            dr = dtIngreso001.NewRow
            dr(0) = strCont
            dr(1) = dgi.Cells(0).Text()
            dr(2) = ""
            dr(3) = dgi.Cells(2).Text()
            dr(4) = "13" 'LIBERACION CONTABLE
            dr(5) = Now.Year.ToString  'PERIODO DE DOCUMENTO
            dr(6) = dgi.Cells(12).Text() 'WARRANT
            dr(7) = ""
            dr(8) = ""
            dr(9) = ""
            dr(10) = "N"
            dtIngreso001.Rows.Add(dr)

            'Session.Add("NroDoc", dgi.Cells(0).Text())
            'Session.Add("IdTipoDocumento", dgi.Cells(23).Text())
            'Session.Add("TipoComprobante", dgi.Cells(17).Text())
            'Session.Add("NroComprobante", dgi.Cells(18).Text())
            'Session.Add("CodUnidad", dgi.Cells(15).Text())
            'Session.Add("CodEntiFina", dgi.Cells(14).Text())
            'Session.Add("Virtual", dgi.Cells(16).Text())
            'Session.Add("Embarque", dgi.Cells(9).Text())
            'Session.Add("IdSicoDepositante", dgi.Cells(13).Text())
            Session.Add("SecContable", dgi.Cells(27).Text())
            'Session.Add("FlgContable", dgi.Cells(7).Text())
            'Session.Add("FlgVisto", dgi.Cells(29).Text())
            'Session.Add("CodAlmaDest", dgi.Cells(32).Text())
            Session.Add("dtIngreso001", dtIngreso001)
            'Session.Add("CKIMPRE", CType(dgi.FindControl("cbox_impr"), CheckBox).Checked)
            Response.Redirect("DocumentoContablePDF.aspx?var=1", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Function crearTabla() As DataTable
        Dim dtIngreso001 As New DataTable
        'dtIngreso.Rows.Clear()
        Dim Column0 As New DataColumn("ITEM")
        Column0.DataType = GetType(Integer)
        Dim Column1 As New DataColumn("NRO_DOCU")
        Column1.DataType = GetType(String)
        Dim Column2 As New DataColumn("NOM_ENT")
        Column2.DataType = GetType(String)
        Dim Column3 As New DataColumn("NRO_LIBE")
        Column3.DataType = GetType(String)
        Dim Column4 As New DataColumn("COD_TIPDOC")
        Column4.DataType = GetType(String)
        Dim Column5 As New DataColumn("PER_ANU")
        Column5.DataType = GetType(String)
        Dim Column6 As New DataColumn("COD_TIPOTRO")
        Column6.DataType = GetType(String)
        Dim Column7 As New DataColumn("COD_DOCU")
        Column7.DataType = GetType(String)
        Dim CoLumn8 As New DataColumn("DSC_MERC")
        CoLumn8.DataType = GetType(String)
        Dim Column9 As New DataColumn("COD_UNIDAD")
        Column9.DataType = GetType(String)
        Dim Column10 As New DataColumn("FLG_VISFIRM")
        Column10.DataType = GetType(String)
        dtIngreso001.Columns.Add(Column0)
        dtIngreso001.Columns.Add(Column1)
        dtIngreso001.Columns.Add(Column2)
        dtIngreso001.Columns.Add(Column3)
        dtIngreso001.Columns.Add(Column4)
        dtIngreso001.Columns.Add(Column5)
        dtIngreso001.Columns.Add(Column6)
        dtIngreso001.Columns.Add(Column7)
        dtIngreso001.Columns.Add(CoLumn8)
        dtIngreso001.Columns.Add(Column9)
        dtIngreso001.Columns.Add(Column10)
        Session.Add("dtIngreso001", dtIngreso001)
        Return dtIngreso001
    End Function

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim strContable As String
        Dim intCierre As Boolean
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If Session.Item("IdTipoUsuario") <> "02" Then
                CType(e.Item.FindControl("chkSeleccionar"), CheckBox).Attributes.Add("onclick", "ActivaBoton()")
            End If

            strContable = e.Item.DataItem("FLG_CONT")
            intCierre = e.Item.DataItem("FLG_CIERRE")
            If strContable = "NO" Then
                CType(e.Item.FindControl("chkSeleccionar"), CheckBox).Enabled = False
            Else
                If intCierre = 0 Then
                    CType(e.Item.FindControl("chkSeleccionar"), CheckBox).Enabled = True
                    CType(e.Item.FindControl("imgEditar"), ImageButton).Enabled = True
                Else
                    CType(e.Item.FindControl("chkSeleccionar"), CheckBox).Enabled = False
                    CType(e.Item.FindControl("imgEditar"), ImageButton).Enabled = False
                End If
            End If

            e.Item.Cells(8).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("IM_SALDO"))
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            If intCierre = True Then
                e.Item.BackColor = System.Drawing.Color.Silver
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='Silver'")
            ElseIf e.Item.DataItem("flg_virtual") = "N" Then
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
            Else
                e.Item.BackColor = System.Drawing.Color.LightGoldenrodYellow
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='LightGoldenrodYellow'")
            End If
            If e.Item.DataItem("FLG_VISTO") = 0 And strContable = "SI" And intCierre = 0 Then
                CType(e.Item.FindControl("ImgVisto"), ImageButton).ImageUrl = "~/Images/Activar2.gif"
                'Habilitamos el boton , segun permisos del usuario 
                If Session.Item("sUSR_VISTO") = True Then
                    CType(e.Item.FindControl("ImgVisto"), ImageButton).Enabled = True
                    CType(e.Item.FindControl("ImgVisto"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de dar el visto bueno?')== false) return false;")
                Else
                    CType(e.Item.FindControl("ImgVisto"), ImageButton).Enabled = False
                End If
            Else
                CType(e.Item.FindControl("ImgVisto"), ImageButton).ImageUrl = "~/Images/Activar.gif"
                CType(e.Item.FindControl("ImgVisto"), ImageButton).Enabled = False
            End If
            If e.Item.DataItem("COD_USUARIO") <> "0" Then
                Dim sCA_CELL As String = e.Item.Cells(0).Text
                Dim sCA_LINK As String = "<A onclick=Javascript:AbrirVisor('" & CStr(e.Item.DataItem("NRO_DOCU")) & "'); href='#'>Inven.</a>"
                e.Item.Cells(31).Text = sCA_LINK
            End If
            If cboEstado.SelectedValue = "09" Then
                CType(e.Item.FindControl("chkSeleccionar"), CheckBox).Enabled = False
                CType(e.Item.FindControl("imgEditar"), ImageButton).Enabled = False
                e.Item.BackColor = System.Drawing.Color.Silver
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='Silver'")
            End If

            'DEJAR EN BLANCO EL NUMERO DE RETIRO

            If Session.Item("IdTipoEntidad") <> "01" Then
                e.Item.Cells(2).Text = ""
                CType(e.Item.FindControl("imgEditar"), ImageButton).Enabled = False
            End If


        End If
    End Sub

    Private Sub VistoBuenoXUsuario()
        Dim dtUsuario As DataTable = New DataTable
        Try
            If Session.Item("IdTipoEntidad") = "01" Then
                dtUsuario = objUsuario.gGetDataUsuarioxEntidad(Session.Item("IdUsuario"), Session.Item("Cliente"))
                If dtUsuario.Rows.Count > 0 Then
                    Session.Add("sUSR_VISTO", dtUsuario.Rows(0)("USR_VISTO"))
                End If
            Else
                Session.Add("sUSR_VISTO", False)
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Function Chequeado() As Integer
        Dim nNU_FILA As Integer
        Dim nCONT As Integer = 0
        For nNU_FILA = 0 To Me.dgdResultado.Items.Count - 1
            Dim chST_MARC As CheckBox = CType(Me.dgdResultado.Items(nNU_FILA).FindControl("chkSeleccionar"), CheckBox)
            If chST_MARC.Checked Then
                nCONT += 1
            End If
        Next
        Return nCONT
    End Function

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim nNU_FILA As Integer
        If Chequeado() = 0 Then
            pr_IMPR_MENS("Debe seleccionar almenos 1 registro")
        Else
            'Recorremos la grilla de warrant y ejecutamos la aprobaci�n  solo para los seleccionados
            For nNU_FILA = 0 To Me.dgdResultado.Items.Count - 1
                Dim chST_MARC As CheckBox = CType(Me.dgdResultado.Items(nNU_FILA).FindControl("chkSeleccionar"), CheckBox)
                If chST_MARC.Checked Then
                    Try
                        Dim strCOD_UNID As String = Me.dgdResultado.Items(nNU_FILA).Cells(15).Text '"COD_UNID" 
                        Dim strNRO_DOC_WARR As String = Me.dgdResultado.Items(nNU_FILA).Cells(0).Text '"NRO_DOCU" 
                        Dim strID_TIPO_DOC As String = Me.dgdResultado.Items(nNU_FILA).Cells(12).Text 'COD_TIPDOC() 
                        Dim strCOD_CLIE As String = Me.dgdResultado.Items(nNU_FILA).Cells(13).Text 'COD_CLIE() 
                        Dim strCOD_ENTI_FINA As String = Me.dgdResultado.Items(nNU_FILA).Cells(14).Text 'COD_ENTFINA
                        Dim strCO_ALMA As String = Me.dgdResultado.Items(nNU_FILA).Cells(21).Text 'CO_ALMA() 
                        Dim strCO_MONE As String = Me.dgdResultado.Items(nNU_FILA).Cells(20).Text 'CO_MONE() 
                        Dim strCO_MODA As String = Me.dgdResultado.Items(nNU_FILA).Cells(19).Text 'CO_MODA() 
                        Dim strNU_DOCU_RECE As String = Me.dgdResultado.Items(nNU_FILA).Cells(18).Text 'NU_DOCU_RECE() nro comprobante
                        Dim strTI_DOCU_RECE As String = Me.dgdResultado.Items(nNU_FILA).Cells(17).Text 'TI_DOCU_RECE() tipo comprobante
                        'Verificar que ya este seteado en la grilla
                        Dim strSEC_CONTA As String = Me.dgdResultado.Items(nNU_FILA).Cells(27).Text   'SEC_CONT()
                        Dim strFE_TITU As String = Me.dgdResultado.Items(nNU_FILA).Cells(22).Text 'FE_TITU
                        Dim strHIST_ALMA_SUBI As String = Me.dgdResultado.Items(nNU_FILA).Cells(26).Text 'ST_ALMA_SUBI() tipo comprobante ' Se calcula en el sgte. fragemento de codigo
                        Dim strTI_DOCU_RETI As String = objSICO.fn_TMPARA_OPER_Q01(Session("CoEmpresa")) 'Confirmar el unico valor "DOR" que devuelve es el correcto para este tipo contable
                        Dim strNRO_RETI As String 'Este dato se calcula
                        Dim strDE_OBSE_NUEV As String = ""

                        If strCO_ALMA.Trim <> "" Then
                            If strNRO_DOC_WARR.Trim = "" And strNU_DOCU_RECE.Trim = "" Then
                                If objSICO.fn_TMALMA_Q01(Session("CoEmpresa"), strCO_ALMA) = "S" Then
                                    strHIST_ALMA_SUBI = "N"
                                ElseIf objSICO.fn_TMALMA_Q01(Session("CoEmpresa"), strCO_ALMA) = "N" Then
                                    strHIST_ALMA_SUBI = "S"
                                End If
                            End If
                        End If
                        'Obtenemos el n�mero de liberaci�n despues de actualizar la informaci�n en el SICO
                        pr_IMPR_MENS(objDocumento.gEliminarLiberacion(strSEC_CONTA))
                        Dim dtMail As DataTable
                        Dim strEmails As String = ""
                        Dim objUsuario As Library.AccesoDB.Usuario = New Library.AccesoDB.Usuario
                        dtMail = objUsuario.gGetEmailAprobadores(strCOD_CLIE, "", "02")
                        If dtMail.Rows.Count > 0 Then
                            For i As Integer = 0 To dtMail.Rows.Count - 1
                                If dtMail.Rows(i)("EMAI") <> "" Then
                                    strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                                End If
                            Next
                            objfunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                            "Se ha rechazado una Liberaci�n para la emisi�n de nuevo Warrant - " & Me.dgdResultado.Items(nNU_FILA).Cells(5).Text,
                            "Estimados Se�ores:<br><br>" &
                            "La siguiente Liberaci�n para la emisi�n de nuevo Warrant se ha rechazado" &
                            ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNRO_DOC_WARR & "</FONT></STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto: <STRONG><FONT color='#330099'>" & strCO_MONE & " " & String.Format("{0:##,##0.00}", Me.dgdResultado.Items(nNU_FILA).Cells(8).Text) & "</FONT></STRONG><br>" &
                            "Depositante: <STRONG><FONT color='#330099'>" & Me.dgdResultado.Items(nNU_FILA).Cells(5).Text & "</FONT></STRONG><br>" &
                            "Usuario que rechaza: <STRONG><FONT color='#330099'>" & Session.Item("UsuarioLogin") & "</FONT></STRONG>" &
                            "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
                        End If
                        dtMail.Dispose()
                        dtMail = Nothing
                    Catch ex As Exception
                        Me.lblError.Text = "ERROR " + ex.Message
                    End Try
                End If
            Next
            BindDatagrid()
        End If
    End Sub

    Public Sub ImgVisto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim ImgVisto As ImageButton = CType(sender, ImageButton)
        Dim dgi As DataGridItem
        dgi = CType(ImgVisto.Parent.Parent, DataGridItem)
        Try
            Dim strNRO_DOCU As String = dgi.Cells(0).Text() 'Nro.Warr
            Dim strFCH_CREA As String = dgi.Cells(3).Text() 'Fch.Solicitud
            Dim strNOM_ENTIFINA As String = dgi.Cells(4).Text() 'Endosatario
            Dim strNOM_ENTI As String = dgi.Cells(5).Text() 'Depositante
            Dim strFLG_CONT As String = dgi.Cells(7).Text() 'Nuevo Warrant
            Dim strIM_SALDO As String = dgi.Cells(8).Text() 'Saldo
            Dim strSEC_CONTA As String = dgi.Cells(27).Text() 'Secuencia de WLiberacionContable
            Dim strCOD_CLIE As String = dgi.Cells(13).Text() 'COD_CLIE()
            '----------------------------------------
            If strFLG_CONT = "SI" Then
                Me.lblError.Text = objDocumento.sSetVistoBueno(strSEC_CONTA, Session.Item("IdUsuario"))

                objfunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailInspectoria"),
                           "El �rea de Warrant dio el visto bueno para la Liberaci�n  del nuevo Warrant - " & strNRO_DOCU,
                           "Estimados Se�ores:<br><br>El �rea de Warrant dio el visto bueno para la Liberaci�n del nuevo Warrant: <br>" &
                           "<table  border='1' cellpadding='2' style='FONT-FAMILY: Arial, Helvetica, sans-serif;COLOR:#003366;FONT-SIZE:11px;FONT-WEIGHT:normal;FONT-WEIGHT: bold'>" &
                           "<tr><td>Nro. Warrant</td><td>Fch. Solicitud</td><td>Endosatario</td><td>Depositante</td><td>Saldo</td></tr>" &
                           "<tr><td>" & strNRO_DOCU & "</td><td>" & strFCH_CREA & "</td><td>" & strNOM_ENTIFINA & "</td><td>" & strNOM_ENTI & "</td><td>" + strIM_SALDO + "</td></tr></table><br><br>" &
                           "Visto Bueno de: <STRONG><FONT color='#330099'>" & Session.Item("UsuarioLogin") & "</FONT></STRONG>" &
                           "<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
            Else
                Me.lblError.Text = "Solo puede poner visto a nuevo warrant"
            End If
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objSICO Is Nothing) Then
            objSICO = Nothing
        End If
        If Not (objfunciones Is Nothing) Then
            objfunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class