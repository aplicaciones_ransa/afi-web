Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoPagUnoWarrant
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objReportes As Reportes = New Reportes
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    'Protected WithEvents chkDCR As System.Web.UI.WebControls.CheckBox
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnEndoso As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents lblSICO As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrVisualizar As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblEndoso As System.Web.UI.WebControls.Label
    'Protected WithEvents cboAsociado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents pnlAsociado As System.Web.UI.WebControls.Panel
    'Protected WithEvents chkDobleEndoso As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents pnlDblEndoso As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblOriginal As System.Web.UI.WebControls.Label
    'Protected WithEvents pnlOriginal As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblAlerta As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabarEndo As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEntidadFinanciera As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents pnlEndosatario As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblSecuencia As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMercaderia As System.Web.UI.WebControls.Label
    'Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCliente As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaCreacion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoDocumento As System.Web.UI.WebControls.Label
    'Protected WithEvents txtAsociado As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtTipoDocumento As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents btnGrabarAsociado As System.Web.UI.WebControls.Button
    'Protected WithEvents txtDblEndoso As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lblPara As System.Web.UI.WebControls.Label
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnRechazar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Try
                Me.lblError.Text = ""
                Me.lblAlerta.Text = ""
                Me.lblEndoso.Text = ""
                Me.lblMensaje.Text = ""
                Session.Remove("Mensaje")
                If Not IsPostBack Then
                    Me.lblOpcion.Text = "Datos del Documento"
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Inicializa()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Me.btnGrabarAsociado.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
        Me.btnGrabarEndo.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar, ya no podra modificar el endosatario?')== false) return false;")
        Me.btnRechazar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de rechazar el documento?')== false) return false;")
        If Request.QueryString("var2") = 1 Then
            objDocumento.gUpdAccesoFirmar(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"))
        End If
        Me.chkDobleEndoso.Enabled = False
        Me.pnlAsociado.Visible = False
        Me.cboAsociado.Enabled = False
        If Session("IdTipoDocumento") = "03" Then
            pnlEndosatario.Visible = False
        Else
            pnlEndosatario.Visible = True
            loadEntidad()
        End If
        loadDataDocumento()
        ValidarControles()
        BindDatagrid()
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As New DataTable
        Try
            Me.cboEntidadFinanciera.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "W")
            Me.cboEntidadFinanciera.Items.Add(New ListItem("---------------------------Seleccione uno---------------------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboEntidadFinanciera.Items.Add(New ListItem(dr("NOM_ENTI"), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "Error al llenar entidades: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub ValidarControles()
        Dim dtControles As New DataTable
        dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroDoc"),
                                                    Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), 0)
        Me.btnGrabarEndo.Visible = False
        Me.cboEntidadFinanciera.Enabled = False
        For i As Integer = 0 To dtControles.Rows.Count - 1
            Select Case dtControles.Rows(i)("COD_CTRL")
                Case "03"
                    Me.ltrVisualizar.Text = "<input id='btnVisualizar' style='WIDTH: 80px; CURSOR: hand' onclick='VisorVerificacion();'	type='button' value='Visualizar' name='btnVisualizar' class='btn'>"
                Case "04"
                    Me.btnRechazar.Visible = True
                Case "05"
                    Me.btnEndoso.Visible = True
                Case "10"
                    Me.btnGrabarEndo.Visible = True
                    Me.cboEntidadFinanciera.Enabled = True
                Case "12"
                    If Me.txtDblEndoso.Value = "True" Then
                        Me.btnGrabarAsociado.Visible = True
                        Me.chkDobleEndoso.Enabled = True
                        Me.cboAsociado.Enabled = True
                    End If
            End Select
        Next
        If Me.btnRechazar.Visible = True And objDocumento.gGetValidarCancelacion(Session.Item("NroDoc")) = False And Session.Item("IdTipoDocumento") = "01" Then
            Me.btnRechazar.Visible = False
        End If
        If Session.Item("IdTipoEntidad") = "03" And Me.cboEntidadFinanciera.SelectedValue = "0" And Session.Item("IdTipoEntidad") = "03" Then
            Me.lblAlerta.Text = "Seleccione"
        End If
        dtControles.Dispose()
        dtControles = Nothing
    End Sub

    Private Sub DobleEndoso()
        If chkDobleEndoso.Checked Then
            Me.pnlAsociado.Visible = True
            loadAsociados()
        Else
            Me.pnlAsociado.Visible = False
        End If
    End Sub

    Private Sub loadDataDocumento()
        Dim drUsuario As DataRow
        drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
        Me.txtDblEndoso.Value = CStr(drUsuario("FLG_CONCERT"))
        Me.chkDobleEndoso.Enabled = False
        Me.lblPara.Text = drUsuario("ASOCIADO")
        Me.chkDobleEndoso.Checked = drUsuario("FLG_DBLENDO")
        If CStr(drUsuario("COD_ENTASOC")) <> "" Then
            Me.pnlOriginal.Visible = True
            Me.pnlDblEndoso.Visible = True
            Me.lblOriginal.Text = CType(drUsuario("NOM_ENTI"), String).Trim
            Me.txtAsociado.Value = drUsuario("COD_ENTASOC")
        ElseIf Session.Item("IdTipoEntidad") = "02" Or Session.Item("IdTipoEntidad") = "01" Then
            Me.pnlOriginal.Visible = False
            If drUsuario("DBL_ENDO") Then
                Me.pnlDblEndoso.Visible = True
            Else
                Me.pnlDblEndoso.Visible = False
            End If
        Else
            Me.pnlOriginal.Visible = False
            If drUsuario("DBL_ENDO") Then
                Me.pnlDblEndoso.Visible = True
                DobleEndoso()
                If drUsuario("COD_ENTASOC") <> "" Then
                    Me.cboAsociado.SelectedValue = drUsuario("COD_ENTASOC")
                    Me.lblEndoso.Text = ""
                Else
                    Me.cboAsociado.SelectedIndex = 0
                    Me.lblEndoso.Text = "Seleccione"
                End If
            Else
                Me.pnlDblEndoso.Visible = False
            End If
        End If
        Session.Add("NombrePDF", CType(drUsuario("NOM_PDF"), String).Trim)
        Me.lblSecuencia.Text = CType(drUsuario("DSC_SECU"), String).Trim
        Me.lblCliente.Text = CType(drUsuario("NOM_ENTI"), String).Trim
        Me.lblNroWarrant.Text = CType(drUsuario("NRO_DOCU"), String).Trim
        Me.lblTipoDocumento.Text = CType(drUsuario("NOM_TIPDOC"), String).Trim
        Me.txtTipoDocumento.Value = CType(drUsuario("NOM_TIPDOC"), String).Trim
        Me.lblEstado.Text = CType(drUsuario("NOM_ESTA"), String).Trim
        Me.lblFechaCreacion.Text = CType(drUsuario("FECHACREACION"), String).Trim
        Me.lblMercaderia.Text = CType(drUsuario("DSC_MERC"), String).Trim
        If drUsuario("Error") = "" And drUsuario("ENT_FINA") <> "" Then
            Me.cboEntidadFinanciera.SelectedValue = CType(drUsuario("ENT_FINA"), String).Trim
        ElseIf CStr(Session.Item("IdTipoEntidad")) = "03" Then
            Me.lblMensaje.Text = CType(drUsuario("Error"), String).Trim
        End If
        If IsDBNull(drUsuario("DOC_PDFORIG")) Then
            objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, False, False, Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"), "0", "")
        End If
        If drUsuario("FLG_DCR") Then
            Me.chkDCR.Enabled = False
            Me.chkDCR.Checked = True
        Else
            Me.chkDCR.Enabled = True
        End If
        If Session.Item("IdTipoEntidad") <> "01" Then
            Me.chkDCR.Enabled = False
        End If
        drUsuario = Nothing
    End Sub

    Private Sub BindDatagrid()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim dt As New DataTable
        Try
            dt = objDocumento.gGetLogDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
            dt.Dispose()
            objTrans.Commit()
            objTrans.Dispose()
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
            objConexion.Close()
        End Try
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
        End If
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim srFirmo As String
        Dim chkFirmo As New CheckBox
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            srFirmo = e.Item.DataItem("FLG_EVEN")
            chkFirmo = CType(e.Item.FindControl("chkFirmo"), CheckBox)
            If srFirmo = "F" Then
                chkFirmo.Checked = True
            Else
                chkFirmo.Checked = False
            End If
        End If
    End Sub

    Private Sub chkDobleEndoso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDobleEndoso.CheckedChanged
        'Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), "", Me.chkDobleEndoso.Checked)
        DobleEndoso()
    End Sub

    Private Sub loadAsociados()
        Dim dtAsociados As DataTable = New DataTable
        Try
            Me.cboAsociado.Items.Clear()
            dtAsociados = objEntidad.gGetAsociados(Session.Item("IdSico_depositante"))
            Me.cboAsociado.Items.Add(New ListItem("-------------------------Seleccione una entidad-------------------------", "0"))
            For Each dr As DataRow In dtAsociados.Rows
                Me.cboAsociado.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        Finally
            dtAsociados.Dispose()
            dtAsociados = Nothing
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("DocumentoConsultar.aspx?Estado=" & Session.Item("EstadoDoc"), False)
    End Sub

    Private Sub btnGrabarEndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarEndo.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "GRABAR FINANCIADOR", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), "X", Me.chkDobleEndoso.Checked)
        loadDataDocumento()
        ValidarControles()
    End Sub

    Private Overloads Sub btnEndoso_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEndoso.Click
        Session.Add("IdEntidadFinanciera", Me.cboEntidadFinanciera.SelectedValue)
        Response.Redirect("DocumentoPagDos.aspx?Pag=1")
    End Sub

    Private Sub btnGrabarAsociado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarAsociado.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "GRABAR ASOCIADO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim strAsociado As String
        strAsociado = ""
        If Me.chkDobleEndoso.Checked Then
            If Me.cboAsociado.SelectedIndex <> 0 Then
                strAsociado = Me.cboAsociado.SelectedValue
            End If
        End If
        Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), strAsociado, Me.chkDobleEndoso.Checked)
        loadDataDocumento()
        ValidarControles()
    End Sub

    Private Sub btnRechazar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "RECHAZAR WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            objDocumento.gUpdRechazarDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), "El usuario " & Session.Item("UsuarioLogin") &
            " ha rechazado el documento", "", "S", "S", objTrans)
            objTrans.Commit()
            objTrans.Dispose()
            Session.Add("strMensaje", "Se rechaz� el documento correctamente")
            Response.Redirect("DocumentoConsultar.aspx?Estado=" & Session.Item("EstadoDoc"), False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Error: " & ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub chkDCR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDCR.CheckedChanged
        If Me.chkDCR.Checked Then
            Dim objConexion As SqlConnection
            Dim objTrans As SqlTransaction
            objConexion = New SqlConnection(strConn)
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()
            Try
                objReportes.GeneraVB(Session("Cod_Doc"), strPDFPath, strPathFirmas, Session.Item("IdUsuario"), "1", objTrans)
                Me.lblError.Text = objReportes.strMensaje()
                If objReportes.strMensaje() = "" Then
                    Me.chkDCR.Enabled = False
                End If
                objTrans.Commit()
                objTrans.Dispose()
            Catch ex As Exception
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = " Primero debes revisar el DCR antes de dar tu VB:  " & ex.ToString
                Exit Sub
            End Try
        End If
    End Sub
End Class
