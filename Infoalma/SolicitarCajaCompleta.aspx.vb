Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class SolicitarCajaCompleta
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objUnidad As LibCapaNegocio.clsCNUnidad
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private bolApto As Boolean = False
    Private intTotalReg As Integer
    Private intPagIncre As Integer
    Private intPagDecre As Integer
    Private intContadorReg As Integer
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroCaja As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblNota As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnVerSolicitud As System.Web.UI.WebControls.Button
    'Protected WithEvents btnIngresarDoc As System.Web.UI.WebControls.Button
    'Protected WithEvents lbtnNext As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lbtnPrevious As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lblCurrentPage As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTotalPages As System.Web.UI.WebControls.Label
    Private intContadoAavance As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents txtFechaIngresoDesde As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaIngresoHasta As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG6") Then
            Try
                'If ValidarPagina("PAG6") = False Then Exit Sub
                If Session("intPagIncre") = Nothing Then
                    Session("intPagIncre") = 1
                End If


                Me.lblError.Text = ""
                Inicializar()
                'intPagIncre = 1
                'Session("intPagIncre") = 1

                If Not IsPostBack Then
                    Session("intPagIncre") = 1
                    CargarCombos()
                    InicializaBusqueda()
                    btnIngresarDoc.Attributes.Add("onclick", "return ValidaCheckBox();")
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid(1, 0)
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        If ValidarCriteriosBusqueda() = True Then
            Session("intPagIncre") = 0
            LlenaGrid(1, 0)
            CalcularPaginado()
            RegistrarCadenaBusqueda()
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusqueda", "?ts=C&?ca=" & cboArea.SelectedIndex & "&?nc=" & txtNroCaja.Text & "&?fi=" & txtFechaIngresoDesde.Value &
            "&?ff=" & txtFechaIngresoHasta.Value & "&")
    End Sub

    Private Sub dgdResultado_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    'Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound

        Dim itemType As ListItemType = CType(e.Item.ItemType, ListItemType)
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        If itemType <> ListItemType.Footer And itemType <> ListItemType.Separator Then
            If itemType = ListItemType.Header Then
                Dim chkSelAll As CheckBox = CType(e.Item.FindControl("chkSelectAll"), CheckBox)
                chkSelAll.Attributes.Add("onClick", "Javascript:chkSelectAll_OnClick(" + chkSelAll.ClientID + ")")
            Else
                Dim idFila As String = dgdResultado.ClientID & "_row" & e.Item.ItemIndex.ToString
                e.Item.Attributes.Add("id", idFila)
                Dim chkSel As CheckBox = CType(e.Item.FindControl("chkSel"), CheckBox)
                Dim idCheckBox As String = CType(chkSel, CheckBox).ClientID
                e.Item.Attributes.Add("onMouseMove", "Javascript:chkSelect_OnMouseMove(" & idFila & "," & idCheckBox & ")")
                e.Item.Attributes.Add("onMouseOut", "Javascript:chkSelect_OnMouseOut(" & idFila & "," & idCheckBox & "," & e.Item.ItemIndex & ")")
                chkSel.Attributes.Add("onClick", "Javascript:chkSelect_OnClick('" & idFila & "','" & idCheckBox & "'," & e.Item.ItemIndex & ")")
                If Convert.ToString(e.Item.DataItem("FLG_SEL")) = "1" Then
                    chkSel.Enabled = False
                End If

                If InStr(strSeleccionados, "SCAJ" & Convert.ToString(e.Item.DataItem("ID_UNID"))) <> 0 Or Convert.ToString(e.Item.DataItem("ST_UBIC")) = "TEM" Or Convert.ToString(e.Item.DataItem("ST_UBIC")) = "CON" Then
                    chkSel.Checked = True
                    chkSel.Enabled = False
                End If
            End If
        End If
    End Sub
    'objComun = New LibCapaNegocio.clsCNComun
    'objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")
    End Sub



    'Private Sub LlenaGrid()
    '    Try
    '        objUnidad = New LibCapaNegocio.clsCNUnidad
    '        objFuncion = New LibCapaNegocio.clsFunciones
    '        objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtMostrarUnidades(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
    '                cboArea.SelectedItem.Value, Trim(txtNroCaja.Text), _
    '                Trim(txtFechaIngresoDesde.Value), Trim(txtFechaIngresoHasta.Value), TipoBusqueda(txtNroCaja.Text), "CAJ"))
    '        lblRegistros.Text = objUnidad.intFilasAfectadas.ToString & " registros"
    '        If objUnidad.intFilasAfectadas > 0 Then
    '            lblNota.Visible = True
    '        Else
    '            lblNota.Visible = False
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub

    Private Sub LlenaGrid(ByVal strPagina As Integer, ByVal strInicio As Integer)
        Dim intTotalPaginas As Decimal

        Try
            objUnidad = New LibCapaNegocio.clsCNUnidad
            objFuncion = New LibCapaNegocio.clsFunciones


            objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtMostrarUnidades(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, Trim(txtNroCaja.Text),
                    Trim(txtFechaIngresoDesde.Value), Trim(txtFechaIngresoHasta.Value), TipoBusqueda(txtNroCaja.Text), "CAJ", Session.Item("UsuarioLogin"), strPagina, strInicio))
            lblRegistros.Text = objUnidad.intFilasAfectadas.ToString & " registros"
            intTotalReg = objUnidad.intFilasAfectadas.ToString
            intContadoAavance = objUnidad.intFilasAvance


            intTotalPaginas = intTotalReg / 50

            If intTotalPaginas > Fix(intTotalPaginas) Then
                intTotalPaginas = Fix(intTotalPaginas) + 1
            Else
                intTotalPaginas = Fix(intTotalPaginas)
            End If
            'lblTotalPages.Text = intTotalPaginas.ToString
            'End If

            lblCurrentPage.Text = 1 'lblCurrentPage.Text
            lblTotalPages.Text = intTotalPaginas

            If objUnidad.intFilasAfectadas > 0 Then
                lblNota.Visible = True
            Else
                lblNota.Visible = False
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Private Sub btnIngresarDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIngresarDoc.Click
        If ValidarFormulario() = True Then
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "P", "DEPSAFIL", "SOLICITUD DE CAJA COMPLETA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            IngresarCaja()
        End If
    End Sub

    Private Function TipoBusqueda(ByVal pstrCadena As String) As String
        If pstrCadena.IndexOf(",") <> 0 Then
            Return "S"
        Else
            Return "N"
        End If
    End Function

    Private Function QuitaBlanco(ByVal pstrCadena As String) As String
        Dim strCadena As String
        strCadena = Trim(pstrCadena)
        Return strCadena
    End Function

    Private Sub btnVerSolicitud_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerSolicitud.Click
        VerSolicitud()
    End Sub

    Private Sub VerSolicitud()
        'If CStr(Session.Item("RequerimientoActivo")) = "0" Then
        '    lblError.Text = "No se ha ingresado documentos para ver la solicitud. Para visualizar la solicitud seleccione e ingrese los documentos."
        'Else
        '    If Session.Item("ProcesoCaja").ToString = "PENDIENTE" Then
        '        Session.Add("PaginaAnterior", "SolicitarCajaCompleta.aspx")
        '        Response.Redirect("ConsultasGeneradasUnidades.aspx")
        '    End If
        'End If
        'If CStr(Session.Item("RequerimientoActivo")) = "0" Then
        If CStr(Session.Item("RequerimientoActivoCaja")) = "0" Or Session.Item("RequerimientoActivoCaja") = Nothing Then
            lblError.Text = "No se ha ingresado documentos para ver la solicitud. Para visualizar la solicitud seleccione e ingrese los documentos."
        Else
            If Session.Item("ProcesoCaja").ToString = "PENDIENTE" Then
                Session.Add("PaginaAnterior", "SolicitarCajaCompleta.aspx")
                Response.Redirect("ConsultasGeneradasUnidades.aspx", False)
            End If
        End If
    End Sub

    'Private Sub IngresarCaja()
    '    If CStr(Session.Item("ProcesoDocumento")) = "PENDIENTE" Or CStr(Session.Item("ProcesoOtros")) = "PENDIENTE" Then
    '        Session.Add("RequerimientoActivo", "0")
    '        Session.Add("ProcesoDocumento", "LISTO")
    '        Session.Add("ProcesoOtros", "LISTO")
    '    End If
    '    If CStr(Session.Item("RequerimientoActivo")) = "0" Then
    '        IngresarRequerimiento()
    '        Session.Add("ProcesoCaja", "PENDIENTE")
    '    End If
    '    If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
    '        IngresarConsultas()
    '    End If
    '    If bolApto = True Then
    '        Session.Add("PaginaAnterior", "SolicitarCajaCompleta.aspx")
    '        Response.Redirect("ConsultasGeneradasUnidades.aspx")
    '    End If
    'End Sub

    Private Sub IngresarCaja()
        If CStr(Session.Item("ProcesoDocumento")) = "PENDIENTE" Or CStr(Session.Item("ProcesoOtros")) = "PENDIENTE" Then
            'Session.Add("RequerimientoActivo", "0")
            Session.Add("RequerimientoActivoCaja", "0")
            Session.Add("ProcesoDocumento", "LISTO")
            Session.Add("ProcesoOtros", "LISTO")
        End If
        'If CStr(Session.Item("RequerimientoActivo")) = "0" Then
        If CStr(Session.Item("RequerimientoActivoCaja")) = "0" Or Session.Item("RequerimientoActivoCaja") = Nothing Then
            IngresarRequerimiento()
            Session.Add("ProcesoCaja", "PENDIENTE")
        End If
        'If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
        If CStr(Session.Item("RequerimientoActivoCaja")) <> "0" Then
            'Session.Add("ProcesoCaja", "PENDIENTE")
            IngresarConsultas()
        End If
        If bolApto = True Then
            Session.Add("PaginaAnterior", "SolicitarCajaCompleta.aspx")
            Response.Redirect("ConsultasGeneradasUnidades.aspx", False)
        End If
    End Sub
    'borrar jauris
    'Private Sub IngresarRequerimiento()
    '    objRequ = New LibCapaNegocio.clsCNRequerimiento
    '    Session.Add("CodArea", cboArea.SelectedValue)
    '    If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "", _
    '                CStr(Session.Item("CodClieAdic")), "CAJ", CStr(Session.Item("CodPersAuto")), _
    '                CStr(Session.Item("DesPersAuto")), cboArea.SelectedValue, "dir envio", "N", _
    '               "obsevacion", 0, CStr(Session.Item("UsuarioLogin"))) = True Then
    '        lblError.Text = "El registro se grab� con �xito nroRequTemp: " & objRequ.intNumRequTemp
    '        Session.Add("RequerimientoActivo", objRequ.intNumRequTemp)
    '        Session.Add("Seleccionados", "-")
    '    Else
    '        lblError.Text = objRequ.strMensajeError
    '    End If
    'End Sub

    Private Sub IngresarRequerimiento()
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        Session.Add("CodArea", cboArea.SelectedValue)


        Dim strArea As String
        Dim dgItem As DataGridItem
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString

        objRequ = New LibCapaNegocio.clsCNRequerimiento
        Session.Add("CodArea", cboArea.SelectedValue)

        If cboArea.SelectedValue = "0" Then
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                    If dgItem.Cells(8).Text = "0" Then
                        strArea = dgItem.Cells(7).Text
                        Exit For
                    End If
                End If
            Next
        Else
            strArea = cboArea.SelectedValue

        End If
        If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "",
                    CStr(Session.Item("CodClieAdic")), "CAJ", CStr(Session.Item("CodPersAuto")),
                    CStr(Session.Item("DesPersAuto")), strArea, "dir envio", "N",
                   "obsevacion", 0, CStr(Session.Item("UsuarioLogin"))) = True Then
            lblError.Text = "El registro se grabo con �xito nroRequTemp: " & objRequ.intNumRequTemp
            Session.Add("RequerimientoActivoCaja", objRequ.intNumRequTemp)
            Session.Add("Seleccionados", "-")
        Else
            lblError.Text = objRequ.strMensajeError
        End If
    End Sub


    'Private Sub IngresarConsultas()
    '    Dim dgItem As DataGridItem
    '    Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivo"))
    '    Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultas"))
    '    Dim strSeleccionados As String = Session.Item("Seleccionados").ToString

    '    Try
    '        For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
    '            dgItem = Me.dgdResultado.Items(intI)
    '            If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
    '                If InStr(strSeleccionados, "SCAJ" & dgItem.Cells(1).Text) = 0 And dgItem.Cells(8).Text = "0" Then
    '                    strSeleccionados &= "SCAJ" & dgItem.Cells(1).Text & ","
    '                    objConsulta = New LibCapaNegocio.clsCNConsulta
    '                    objConsulta.intNumRequTemp = intNumRequTemp
    '                    If objConsulta.gbolInsConsultaTemp(CStr(Session.Item("CodCliente")), "CAJ", _
    '                    "Descripcion del envio", dgItem.Cells(1).Text, "loc", "CAJ", "CAJ", _
    '                    dgItem.Cells(3).Text, dgItem.Cells(7).Text, "", dgItem.Cells(5).Text, "", 0, _
    '                     CStr(Session.Item("UsuarioLogin")), "CJA") = True Then
    '                        lblError.Text = lblError.Text & " IDCons : " & objConsulta.intNumConsulta
    '                        intNumConsultas = intNumConsultas + 1
    '                    Else
    '                        lblError.Text = objRequ.strMensajeError
    '                        bolApto = False
    '                        Exit For
    '                    End If
    '                End If
    '            End If
    '        Next
    '        Session.Add("NroConsultas", intNumConsultas)
    '        Session.Add("Seleccionados", strSeleccionados)
    '        bolApto = True
    '    Catch ex As Exception
    '        bolApto = False
    '    End Try
    'End Sub
    Private Sub IngresarConsultas()
        Dim dgItem As DataGridItem
        'Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivo"))
        'Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultas"))
        'Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        'Dim strflag As Integer
        Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivoCaja"))
        Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultasCajas"))
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        Dim strflag As Integer

        Try
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True And CType(dgItem.FindControl("chkSel"), CheckBox).Enabled = True Then
                    If InStr(strSeleccionados, "SCAJ" & dgItem.Cells(1).Text) = 0 And dgItem.Cells(8).Text = "0" Then
                        strSeleccionados &= "SCAJ" & dgItem.Cells(1).Text & ","
                        objConsulta = New LibCapaNegocio.clsCNConsulta
                        objConsulta.intNumRequTemp = intNumRequTemp
                        '*****************************************************************item repetido
                        If objConsulta.gbolConsultaTempApropiados(CStr(Session.Item("CodCliente")), "CAJ", dgItem.Cells(1).Text, "001", "0", "0") = False Then
                            lblError.Text = "El Documento N� " & dgItem.Cells(1).Text & " del Area: " & dgItem.Cells(3).Text & " Esta Utilizado por otro usuario"
                            bolApto = False
                            strflag = 1
                            Exit For
                        End If
                        strflag = 0
                        '*****************************************************************item repetido
                        If objConsulta.gbolInsConsultaTemp(CStr(Session.Item("CodCliente")), "CAJ",
                       "Descripcion del envio", dgItem.Cells(1).Text, "loc", "CAJ", "CAJ",
                        dgItem.Cells(3).Text, dgItem.Cells(7).Text, "", dgItem.Cells(5).Text, "", 0,
                         CStr(Session.Item("UsuarioLogin")), "CJA") = True Then
                            lblError.Text = lblError.Text & " IDCons : " & objConsulta.intNumConsulta
                            intNumConsultas = intNumConsultas + 1
                        Else
                            lblError.Text = objRequ.strMensajeError
                            bolApto = False
                            Exit For
                        End If
                    End If
                End If
            Next

            If strflag = 0 Then
                Session.Add("NroConsultasCajas", intNumConsultas)
                Session.Add("Seleccionados", strSeleccionados)
                bolApto = True
            End If

        Catch ex As Exception
            bolApto = False
        End Try
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            If Session.Item("PaginasPermitidas") = Nothing Then
                Return False
                Exit Function
            End If
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function
    Public Sub Inicializar()
        Me.txtNroCaja.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
    End Sub

    Private Function ValidarFormulario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            If dgdResultado.Items.Count = 0 Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado registro(s). Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            ElseIf dgdResultado.Items.Count > 0 Then
                Dim dgItem As DataGridItem
                For Each dgItem In dgdResultado.Items
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                        bolRetorno = True
                        Exit For
                    End If
                Next
                If bolRetorno = False Then
                    lblError.Text = "No se ha seleccionado registro(s). Seleccione por lo menos un registro para continuar."
                End If
            End If
            If cboArea.SelectedValue = "" Or cboArea.SelectedValue = Nothing Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado un �rea. Seleccione un �rea para poder identificar la solicitud y poder continuar el siguiente paso."
            End If
            Return bolRetorno
        Catch ex As Exception
            lblError.Text = ex.Message
            Return False
        End Try
    End Function

    Public Function EncontrarVariable(ByVal pstrCadenaBusqueda As String, ByVal pstrVar As String) As String
        Dim intPosicion As Integer = InStr(pstrCadenaBusqueda, pstrVar)
        Dim strValor As String = ""
        If intPosicion = 0 And pstrCadenaBusqueda = "" Then
            Return ""
        Else
            intPosicion += pstrVar.Length
            strValor = pstrCadenaBusqueda.Substring(intPosicion, pstrCadenaBusqueda.IndexOf("&", intPosicion) - intPosicion)
            Return strValor
        End If
    End Function

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        If Session.Item("CadenaBusqueda") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusqueda").ToString
            If EncontrarVariable(strCadenaBusqueda, "?ts") = "C" Then
                SeleccionaItemCombo(cboArea, CInt(EncontrarVariable(strCadenaBusqueda, "?ca")))
                txtNroCaja.Text = EncontrarVariable(strCadenaBusqueda, "?nc")
                txtFechaIngresoDesde.Value = EncontrarVariable(strCadenaBusqueda, "?fi")
                txtFechaIngresoHasta.Value = EncontrarVariable(strCadenaBusqueda, "?ff")
            End If
        End If
    End Sub

    Public Sub SeleccionaItemCombo(ByVal objcombo As DropDownList, ByVal intIndex As Integer)
        Dim cboItem As ListItem
        Dim intI As Integer = 0
        For intI = 0 To objcombo.Items.Count - 1
            If (intI = intIndex) Then
                objcombo.Items(intI).Selected = True
            Else
                objcombo.Items(intI).Selected = False
            End If
        Next
    End Sub

    Private Function ValidarCriteriosBusqueda() As Boolean
        Dim blnRetorno As Boolean = False
        If cboArea.SelectedItem.Value = "-" Or cboArea.SelectedItem.Value = "" Then
            lblError.Text = "No se ha seleccionado �rea. Seleccione un �rea para continuar."
            blnRetorno = False
        Else
            blnRetorno = True
        End If
        Return blnRetorno
    End Function

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objUnidad Is Nothing) Then
            objUnidad = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
    Public Sub PreviousPage2(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim intPaginaActual As Integer = CInt(lblCurrentPage.Text)
        'Dim intTotalPaginas As Integer = CInt(lblTotalPages.Text)
        intPagIncre = Session("intPagIncre") - 1
        LlenaGrid(intPagIncre, 0)
        If intPagIncre = 1 Then
            lbtnPrevious.Enabled = False
        End If
        lbtnNext.Enabled = True
        Session("intPagIncre") = intPagIncre
        lblCurrentPage.Text = Session("intPagIncre")
        'lblTotalPages.Text = intTotalReg

    End Sub

    Public Sub NextPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim intPaginaActual As Integer = CInt(lblCurrentPage.Text)
        'Dim intTotalPaginas As Integer = CInt(lblTotalPages.Text)

        'If intContadoAavance >= 50 Then
        'intPagIncre = Session("intPagIncre") + 1
        'Else
        intPagIncre = Session("intPagIncre") + 1
        'End If

        LlenaGrid(intPagIncre, 0)
        If intContadoAavance = intTotalReg Then
            lbtnNext.Enabled = False
        End If
        If intPagIncre > 1 Then
            lbtnPrevious.Enabled = True
        End If
        Session("intPagIncre") = intPagIncre
        lblCurrentPage.Text = Session("intPagIncre")
        'lblTotalPages.Text = intTotalReg

    End Sub

    Sub CalcularPaginado()
        Dim intTotalPaginas As Int16
        lbtnPrevious.Enabled = False
        If intTotalReg > 50 Then
            lbtnNext.Enabled = True
        Else
            lbtnNext.Enabled = False
        End If
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        btnBuscar_Click(0, e)
    End Sub
End Class
