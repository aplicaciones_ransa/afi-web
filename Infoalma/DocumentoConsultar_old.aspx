<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoConsultar_old.aspx.vb" Inherits="DocumentoConsultar_old" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consultar Documento</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="author" content="francisco_uni@hotmail.com">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  

			//------------------------------------------ Inicio Validar Fecha
		var primerslap=false; 
		var segundoslap=false; 
		function IsNumeric(valor) 
		{ 
			var log=valor.length; var sw="S"; 
			for (x=0; x<log; x++) 
			{ v1=valor.substr(x,1); 
			v2 = parseInt(v1); 
			//Compruebo si es un valor num�rico 
			if (isNaN(v2)) { sw= "N";} 
			} 
				if (sw=="S") {return true;} else {return false; } 
		} 	
		function formateafecha(fecha) 
		{ 
			var long = fecha.length; 
			var dia; 
			var mes; 
			var ano; 

			if ((long>=2) && (primerslap==false)){dia=fecha.substr(0,2); 
			if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
			else { fecha=""; primerslap=false;} 
			} 
			else 
			{ dia=fecha.substr(0,1); 
			if (IsNumeric(dia)==false) 
			{fecha="";} 
			if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
			} 
			if ((long>=5) && (segundoslap==false)) 
			{ mes=fecha.substr(3,2); 
			if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
			else { fecha=fecha.substr(0,3);; segundoslap=false;} 
			} 
			else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
			if (long>=7) 
			{ ano=fecha.substr(6,4); 
			if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
			else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
			} 

			if (long>=10) 
			{ 
				fecha=fecha.substr(0,10); 
				dia=fecha.substr(0,2); 
				mes=fecha.substr(3,2); 
				ano=fecha.substr(6,4); 
				// A�o no viciesto y es febrero y el dia es mayor a 28 
				if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
			} 
			return (fecha); 
		} 
		//-----------------------------------Fin formatear fecha	
		function ActivaGeneraPDF()
			{			
			frm=document.forms[0];
			frm.btnGenerarPDF.disabled=false;		
			}	
		function ActivaBoton()
			{			
			frm=document.forms[0];
			frm.btnVerificar.disabled=false;		
			}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table3"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6"><uc1:menuinfo id="Menuinfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table4" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="text">Tipo Doc:</TD>
														<TD><asp:dropdownlist id="cboTipoDocumento" runat="server" CssClass="Text" Width="150px"></asp:dropdownlist></TD>
														<TD class="text">Nro Warrant:</TD>
														<TD><asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="120px"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
										<TR>
											<TD width="80"><asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD height="300" vAlign="top"><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="15" AllowPaging="True"
										OnPageIndexChanged="Change_Page" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="2" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderTemplate>
													Sel
												</HeaderTemplate>
												<ItemTemplate>
													<TABLE id="tblItem" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkItemGenerado runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_PDFGENE") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro Warrant">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NRO_LIBE1" HeaderText="Nro Secu/Libe"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC" HeaderText="Cod Tipo Documento"></asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_TIPDOC" HeaderText="Tipo Documento">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ENTI" HeaderText="Depositante">
												<HeaderStyle Width="200px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ENTFINA" HeaderText="Financiador">
												<HeaderStyle Width="200px"></HeaderStyle>
											</asp:BoundColumn>
										<%--	<asp:TemplateColumn HeaderText="Dsc Mercader&#237;a">
												<HeaderStyle Width="200px"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="lnkDescripcion" onclick="lnkDescripcion_Click" runat="server" CssClass="Text"
														ToolTip="Ver PDF">
														<%# DataBinder.Eval(Container, "DataItem.DSC_MERC") %>
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>--%>
                                            <asp:BoundColumn  DataField="DSC_MERC" HeaderText="Dsc Mercader&#237;a">
												<HeaderStyle Width="200px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Alm">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkDepsa runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRALMC") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Dep">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table11" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkCliente runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRCLIE") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="End">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table12" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkFinanciador runat="server" CssClass="Text" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_APRFINA") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Pag1">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table25" cellSpacing="0" cellPadding="0" width="20" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar1" onclick="imgWarrant_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Ver.JPG" ToolTip="Ver datos del Warrant"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Pag2">
												<HeaderStyle Width="20px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="20" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar2" onclick="imgEndoso_Click" runat="server" Visible="False" ToolTip="Ver datos del Endoso"
																	ImageUrl="Images/Ver.JPG" CausesValidation="False"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="PRD_DOCU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPOTRO"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="LUG_ENDO"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NRO_LIBE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_DOC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="ENT_FINA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_DBLENDO"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_CLIE"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><cc1:botonenviar id="btnGenerarPDF" runat="server" CssClass="btn" Width="100px" Text="Generar Warrant"
										TextoEnviando="Generando..." Enabled="False"></cc1:botonenviar><cc1:botonenviar id="btnVerificar" runat="server" CssClass="btn" Width="100px" Text="Verificar" TextoEnviando="Verificando..."
										Enabled="False"></cc1:botonenviar></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="630">
										<TR>
											<TD class="td" colSpan="8">Leyenda</TD>
										</TR>
										<TR>
											<TD bgColor="#f5dc8c" width="8%"></TD>
											<TD class="Leyenda" width="25%">Warrants pendientes de verificaci�n.</TD>
											<TD bgColor="#ffbe78" width="8%"></TD>
											<TD class="Leyenda" width="25%">Warrants pendientes de Endoso.</TD>
											<TD bgColor="#c8dcf0" width="8%"></TD>
											<TD class="Leyenda" width="25%">Warrants Verificados y endosados.</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
