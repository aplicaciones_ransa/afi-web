<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoAprobacionRetiro_W.aspx.vb" Inherits="DocumentoAprobacionRetiro_W" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Documento de retiro de aprobaci�n</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function AbrirDetalle(sCodCliente, sNroPedido, sCodAlma) {
            hidden = open('Popup/AlmDetallePedido.aspx?sCodCliente=' + sCodCliente + '&sNroPedido=' + sNroPedido + '&sCodAlma=' + sCodAlma, 'NewWindow', 'top=100,left=350,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=750px,height=400px');
        }
        function UrlEjm() {
            var ventana = 'Popup/AlmDetallePedido.aspx';
            window.open(ventana, "Retiros en L�nea", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=550,height=400");
            hidden = open('http://www.microsoft.com', 'NewWindow', 'top=0,left=0, width=800,height=600,status=yes, resizable=yes,scrollbars=yes');
            hidden = open('Popup/AlmDetallePedido.aspx', 'NewWindow', 'top=100,left=350,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=550,height=400');
        }
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() - miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="5">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text">Almac�n :</td>
                                                                <td colspan="7">
                                                                    <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" AutoPostBack="True" Width="454px"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Estado:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboEstSitu" runat="server" CssClass="text">
                                                                        <asp:ListItem Value="0">----Todos----</asp:ListItem>
                                                                        <asp:ListItem Value="001">Simple</asp:ListItem>
                                                                        <asp:ListItem Value="006">Aduanero</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text">Nro Pedido :</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNroPedido" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                                <td class="Text">Del :</td>
                                                                <td>
                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                <td class="Text">Al&nbsp;:</td>
                                                                <td class="Text">
                                                                    <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaFinal" runat="server"><input id="btnFechaFinal" class="text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right">
                                            <asp:HyperLink ID="hlkNuevo" runat="server" CssClass="Text" NavigateUrl="AlmPedidoNuevo_W.aspx"
                                                Target="_parent" Visible="False">Solicitar nuevo retiro</asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="250" valign="top" align="center">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnPageIndexChanged="Change_Page">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="nropedido" HeaderText="Nro. Pedido">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fechapedido" HeaderText="Fecha emisi&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Estado" HeaderText="Estado">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="retiradopor" HeaderText="Retirado por">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="nomcliente" HeaderText="Cliente">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CNT_SOLI" HeaderText="Cnt. Solicitada">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CNT_PREP" HeaderText="Cnt. Preparada">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Ver">
                                                        <HeaderStyle Width="20px"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <table id="Table5" cellspacing="0" cellpadding="0" width="20" border="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:ImageButton ID="imgVer" runat="server" OnClick="imgVer_Click" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
                                                                            CausesValidation="False"></asp:ImageButton></td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn Visible="False" DataField="cod_clie" HeaderText="cod_clie"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="cod_alma" HeaderText="cod_alma"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NRO_LIBE" HeaderText="NRO_LIBE"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="PRD_DOCU" HeaderText="PRD_DOCU"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="COD_TIPDOC" HeaderText="COD_TIPDOC"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table2" border="0" cellspacing="4" cellpadding="0" width="630">
                                                <tr>
                                                    <td class="td" colspan="8">Leyenda</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f5dc8c" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Pedidos&nbsp;pendientes.</td>
                                                    <td bgcolor="#32cd32" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Pedidos&nbsp;en Proceso.</td>
                                                    <td bgcolor="#add8e6" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Pedidos&nbsp;Completados.</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
