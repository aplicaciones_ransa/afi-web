Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class ActualizarContraseñaMobil
    Inherits System.Web.UI.Page
    Private objAcceso As Acceso = New Acceso
    Dim strPassAnterior As String
    Private objFunciones As Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnActualizar As System.Web.UI.WebControls.Button
    'Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassRepNuevo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassNuevo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassAnterior As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkSesion As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim strVar1 As String
            Dim strVar2 As String
            Dim dt As DataTable
            strVar1 = Request.QueryString("strVar1")
            strVar2 = Request.QueryString("strVar2")
            Me.btnActualizar.Visible = True
            ' -----------------------
            objFunciones = New Library.AccesoBL.Funciones

            dt = objAcceso.gDesencryptarSQLMobil(strVar1)

            If dt.Rows.Count <> 0 Then

                Me.lblUsuario.Text = dt.Rows(0)("USR_ACCE").ToString
                Session.Add("strPassAnterior", dt.Rows(0)("USR_PASW").ToString)
                Session.Add("IdUsuario", dt.Rows(0)("COD_USER").ToString)



            End If

        End If
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Dim dtAcceso As DataTable

            objFunciones = New Library.AccesoBL.Funciones
            Session.Add("Intentos", Session.Item("Intentos") + 1)
            If Me.txtPassNuevo.Text.Trim.Length >= 6 And Me.txtPassNuevo.Text.Trim.Length <= 12 Then
                If Session.Item("Intentos") < 4 Then
                    Dim strContraseñaNueva As String
                    Dim strContraseñaAnterior As String

                    strContraseñaNueva = Library.AccesoBL.Funciones.EncryptString(Me.txtPassNuevo.Text.Trim, "©¹ã_[]0")
                    strContraseñaAnterior = Session.Item("strPassAnterior")

                    dtAcceso = objAcceso.gCambiarContraseñaMobil(Session.Item("IdUsuario"), strContraseñaAnterior,
                        strContraseñaNueva, System.Configuration.ConfigurationManager.AppSettings("NroDiasCaducaPass"))

                    If Me.txtPassNuevo.Text = "" Then
                        pr_IMPR_MENS("Ingrese contraseña Nueva")
                        Exit Sub
                    ElseIf Me.txtPassRepNuevo.Text = "" Then
                        pr_IMPR_MENS("Ingrese confirma tu contraseña")
                        Exit Sub
                    End If

                    If dtAcceso.Rows.Count <> 0 Then
                        If dtAcceso.Rows(0)("RESULTADO").ToString = "I" Then
                            Session.Remove("Intentos")
                            pr_IMPR_MENS(dtAcceso.Rows(0)("MENSAJE").ToString)
                            Me.txtPassNuevo.Enabled = False
                            Me.txtPassRepNuevo.Enabled = False
                            Me.btnActualizar.Visible = False
                        ElseIf dtAcceso.Rows(0)("RESULTADO").ToString = "N" Then
                            pr_IMPR_MENS(dtAcceso.Rows(0)("MENSAJE").ToString)
                            Me.txtPassNuevo.Enabled = False
                            Me.txtPassRepNuevo.Enabled = False
                            Me.btnActualizar.Visible = False
                        ElseIf dtAcceso.Rows(0)("RESULTADO").ToString = "E" Then
                            pr_IMPR_MENS(dtAcceso.Rows(0)("MENSAJE").ToString)
                            Me.txtPassNuevo.Enabled = False
                            Me.txtPassRepNuevo.Enabled = False
                            Me.btnActualizar.Visible = False
                        End If
                    End If
                Else
                    pr_IMPR_MENS("A excedido el número de intentos, intente en otra ocasión")
                End If
            Else
                pr_IMPR_MENS("La Contraseña nueva debe de tener como mínimo 6 digitos y como máximo 12 digitos")
            End If



        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.ToString
        End Try
    End Sub


    Protected Overrides Sub Finalize()
        If Not (objAcceso Is Nothing) Then
            objAcceso = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
