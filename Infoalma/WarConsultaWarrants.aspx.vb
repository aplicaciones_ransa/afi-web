Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.Data
Imports Infodepsa

Public Class WarConsultaWarrants
    Inherits System.Web.UI.Page
    Private objWarrant As clsCNWarrant
    Private objAccesoWeb As clsCNAccesosWeb
    Private dvWarrant As DataView
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgVigentes As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoWar As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroWar As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoEntidad As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "CONSULTA_WARRANT") Then
            Me.lblOpcion.Text = "Consulta de Warrants"
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "CONSULTA_WARRANT"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session("IdTipoEntidad") = "03" Then
                    Me.lblTipoEntidad.Text = "Financiador"
                Else
                    Me.lblTipoEntidad.Text = "Depositante"
                End If
                LlenarFinanciador()
                LlenarTipoWar()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenarFinanciador()
        Try
            objWarrant = New clsCNWarrant
            Dim dtFinanciador As DataTable
            cboFinanciador.Items.Clear()
            dtFinanciador = objWarrant.gCNGetListarFinanciadores(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"))
            cboFinanciador.Items.Add(New ListItem("--------------------Todos--------------------", 0))
            If dtFinanciador Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dtFinanciador.Rows
                cboFinanciador.Items.Add(New ListItem(dr("DE_LARG_ENFI"), dr("CO_ENTI_FINA")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarTipoWar()
        Try
            objWarrant = New clsCNWarrant
            Dim dtTipoWar As DataTable
            cboTipoWar.Items.Clear()
            dtTipoWar = objWarrant.gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "S")
            cboTipoWar.Items.Add(New ListItem("-----Todos-----", 0))
            If dtTipoWar Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dtTipoWar.Rows
                cboTipoWar.Items.Add(New ListItem(dr("DE_MODA_MOVI"), dr("CO_MODA_MOVI")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub BindatagridVigentes()
        Try
            objWarrant = New clsCNWarrant
            dvWarrant = objWarrant.gCNGetBuscarWarrant(Session.Item("CoEmpresa"),
                                                Session.Item("IdSico"), Me.cboEstado.SelectedValue,
                                                Me.cboFinanciador.SelectedValue, Me.cboTipoWar.SelectedValue,
                                                Me.txtNroWar.Text, Session("IdTipoEntidad")).DefaultView
            Session.Add("dtMovimiento", dvWarrant)
            Me.dgVigentes.DataSource = fn_Quiebre(dvWarrant)
            Me.dgVigentes.DataBind()
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgVigentes.CurrentPageIndex = 0
        BindatagridVigentes()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "CONSULTA_WARRANT", "CONSULTA DE WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgVigentes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVigentes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text <> "&nbsp;" And e.Item.Cells(1).Text = "&nbsp;" Then
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    e.Item.Cells(0).ColumnSpan = 13
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.Cells(0).Font.Bold = True
                Else
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    Dim sCA_CELL As String = e.Item.Cells(0).Text
                    Dim sCA_LINK As String = "<A onclick=Javascript:AbrirWarr('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(1).Text & "'); href='#'> " &
                                             "" & sCA_CELL & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK
                End If
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgVigentes.CurrentPageIndex = Args.NewPageIndex
        Me.dgVigentes.DataSource = fn_Quiebre(CType(Session.Item("dtMovimiento"), DataView))
        Me.dgVigentes.DataBind()
    End Sub

    Private Function fn_Quiebre(ByVal dvWarrant As DataView) As DataTable
        Try
            Dim dr As DataRowView
            Dim drNU_REGI As DataRow
            Dim decTotal As Decimal = 0
            Dim decSaldo As Decimal = 0
            Dim cantTotal As Decimal = 0
            Dim cantSaldo As Decimal = 0
            Dim strFinanciador As String = "&nbsp;"
            Dim dtTCDOCU_CLIE As New DataTable
            dtTCDOCU_CLIE.Columns.Add("TI_TITU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_TITU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_EMIS", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_VENC_VIGE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_MAXI_VENC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("MONEDA", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("VAL_WARR", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("FE_CANC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("SAL_WARR", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("CO_TIPO_BULT", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("VAL_CANT", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_ALMA", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("SAL_CANT", GetType(System.Decimal))

            For Each dr In dvWarrant
                If strFinanciador <> dr("NO_FINA") Then
                    If strFinanciador <> "&nbsp;" Then
                        drNU_REGI = dtTCDOCU_CLIE.NewRow()
                        drNU_REGI("MONEDA") = "Sub Total"
                        drNU_REGI("VAL_WARR") = decTotal
                        drNU_REGI("SAL_WARR") = decSaldo
                        drNU_REGI("CO_TIPO_BULT") = " "
                        drNU_REGI("VAL_CANT") = cantTotal
                        drNU_REGI("SAL_CANT") = cantSaldo
                        dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                        decTotal = 0
                        decSaldo = 0
                        cantTotal = 0
                        cantSaldo = 0
                    End If
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("TI_TITU") = dr("NO_FINA")
                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    strFinanciador = dr("NO_FINA")
                End If
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("TI_TITU") = dr("TI_TITU")
                drNU_REGI("NU_TITU") = dr("NU_TITU")
                drNU_REGI("FE_EMIS") = dr("FE_EMIS")
                drNU_REGI("FE_VENC_VIGE") = dr("FE_VENC_VIGE")
                drNU_REGI("FE_MAXI_VENC") = dr("FE_MAXI_VENC")
                drNU_REGI("MONEDA") = dr("MONEDA")
                drNU_REGI("VAL_WARR") = CType(dr("VAL_WARR"), Decimal)
                drNU_REGI("FE_CANC") = dr("FE_CANC")
                drNU_REGI("SAL_WARR") = CType(dr("SAL_WARR"), Decimal)
                drNU_REGI("CO_TIPO_BULT") = dr("CO_TIPO_BULT")
                drNU_REGI("VAL_CANT") = CType(dr("VAL_CANT"), Decimal)
                drNU_REGI("DE_MERC") = dr("DE_MERC")
                drNU_REGI("DE_ALMA") = dr("DE_ALMA")
                drNU_REGI("SAL_CANT") = CType(dr("SAL_CANT"), Decimal)
                decTotal += CType(dr("VAL_WARR"), Decimal)
                decSaldo += CType(dr("SAL_WARR"), Decimal)
                cantTotal += CType(dr("VAL_CANT"), Decimal)
                cantSaldo += CType(dr("SAL_CANT"), Decimal)

                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            Next
            drNU_REGI = dtTCDOCU_CLIE.NewRow()
            drNU_REGI("MONEDA") = "Sub Total"
            drNU_REGI("VAL_WARR") = decTotal
            drNU_REGI("SAL_WARR") = decSaldo
            drNU_REGI("CO_TIPO_BULT") = " "
            drNU_REGI("VAL_CANT") = cantTotal
            drNU_REGI("SAL_CANT") = cantSaldo
            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)

            Return dtTCDOCU_CLIE
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Function

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Warrants.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        objWarrant = New clsCNWarrant
        dg.DataSource = objWarrant.gCNGetBuscarWarrant(Session.Item("CoEmpresa"),
                                            Session.Item("IdSico"), Me.cboEstado.SelectedValue,
                                            Me.cboFinanciador.SelectedValue, Me.cboTipoWar.SelectedValue,
                                            Me.txtNroWar.Text, Session("IdTipoEntidad"))
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
