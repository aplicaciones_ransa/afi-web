Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class MensajeRegistro
    Inherits System.Web.UI.Page
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    'Protected WithEvents ltrEnviarCorreo As System.Web.UI.WebControls.Literal
    'Protected WithEvents lnkVerEstado As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroRequerimiento As System.Web.UI.WebControls.Label
    'Protected WithEvents lblArea As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoEnvio As System.Web.UI.WebControls.Label
    'Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    'Protected WithEvents lblSolicitud As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents lblHora As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtDestinatario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtComentario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    Private objFuncion As LibCapaNegocio.clsFunciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents btnEnviarCorreo As System.Web.UI.HtmlControls.HtmlButton

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'pr_VALI_SESI()
        'If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
        '    Try
        '        Me.lblError.Text = ""
        '        If Not IsPostBack Then
        '            Session.Add("ProcesoCaja", "LISTO")
        '            Session.Add("ProcesoDocumento", "LISTO")
        '            Session.Add("ProcesoOtros", "LISTO")
        '            Session.Add("RequerimientoActivo", "0")
        '            Session.Add("NroConsultas", "0")
        '            If Request.Item("CR") <> "" And Request.Item("CT") <> "" And Request.Item("ER") <> "" Then
        '                CargarMensajeRegistro(Request.Item("CR"), Request.Item("CT"), Request.Item("ER"))
        '            Else
        '                Response.Redirect("SolicitarDocumento.aspx", False)
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Me.lblError.Text = ex.Message
        '    End Try
        'Else
        '    Response.Redirect("Salir.aspx?caduco=1", False)
        'End If
        If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
            Try
                'If ValidarPagina("PAG") = False Then Exit Sub
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    'If Request.Item("ActivoCajaxx") = "1" Or Request.Item("ActivoCajaxx") <> Nothing Then
                    If Request.QueryString("ActivoCaja") = "1" Then 'Or Request.QueryString("ActivoCajaxx") <> Nothing Then
                        Session.Add("ProcesoCaja", "LISTO")
                        Session.Add("RequerimientoActivoCaja", "0")
                        'Session.Add("NroConsultas", "0")
                    Else
                        Session.Add("ProcesoDocumento", "LISTO")
                        Session.Add("RequerimientoActivo", "0")
                        Session.Add("ProcesoOtros", "LISTO")
                    End If

                    If Request.Item("CR") <> "" And Request.Item("CT") <> "" And Request.Item("ER") <> "" Then
                        CargarMensajeRegistro(Request.Item("CR"), Request.Item("CT"), Request.Item("ER"))
                    Else
                        Response.Redirect("SolicitarDocumento.aspx", False)
                    End If
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    'Public Sub CargarMensajeRegistro(ByVal pstrNumRequ As String, _
    '    ByVal pstrCodTipo As String, ByVal pstrEstRequ As String)
    '    Dim drFila As System.Data.DataRow
    '    objRequ = New LibCapaNegocio.clsCNRequerimiento
    '    drFila = objRequ.gdrConseguirMensajeRegistro(CStr(Session.Item("CodCliente")), _
    '        pstrNumRequ, pstrCodTipo, pstrEstRequ)
    '    With drFila
    '        lblNroRequerimiento.Text = drFila(0)
    '        lblTipoEnvio.Text = drFila(1)
    '        lblArea.Text = drFila(2)
    '        lblUsuario.Text = drFila(3)
    '        lblSolicitud.Text = drFila(6)
    '        lblFecha.Text = drFila(4)
    '        lblHora.Text = drFila(5)
    '        lblError.Text = drFila(7)
    '    End With
    '    If pstrEstRequ = "TEM" Then
    '        lnkVerEstado.Visible = False
    '        Me.btnEnviar.Attributes.Add("onclick", "javascript:CerrarPanel();")
    '        Me.ltrEnviarCorreo.Text = "<INPUT id='btnEnviarCorreo' style='WIDTH: 80px' onclick='AbrirPanel();' type='button' value='Enviar Correo'>"
    '    End If
    'End Sub

    Public Sub CargarMensajeRegistro(ByVal pstrNumRequ As String,
       ByVal pstrCodTipo As String, ByVal pstrEstRequ As String)
        Dim drFila As System.Data.DataRow
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        objFuncion = New LibCapaNegocio.clsFunciones
        objFuncion.gCargaGrid(dgdResultado, objRequ.gdrConseguirMensajeRegistro(CStr(Session.Item("CodCliente")), pstrNumRequ, pstrCodTipo, pstrEstRequ))

        If pstrEstRequ = "TEM" Then
            lnkVerEstado.Visible = False
            Me.btnEnviar.Attributes.Add("onclick", "javascript:CerrarPanel();")
            Me.ltrEnviarCorreo.Text = "<INPUT id='btnEnviarCorreo' style='WIDTH: 80px' onclick='AbrirPanel();' type='button' value='Enviar Correo'>"
        End If
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    'Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
    '    EnviarCorreo()
    '    lblError.Text = "Se envi� un correo al destinatario " & txtDestinatario.Text & " reportando la generaci�n del requerimiento temporal nro. " & Me.lblNroRequerimiento.Text
    '    objAccesoWeb = New clsCNAccesosWeb
    '    objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
    '                                Session.Item("NombreEntidad"), "P", "DEPSAFIL", "ENVIO DE CORREO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    'End Sub
    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        EnviarCorreo()
        'lblError.Text = "Se envi� un correo al destinatario " & txtDestinatario.Text & " reportando la generaci�n del requerimiento temporal nro. " & Me.lblNroRequerimiento.Text
        lblError.Text = "Se envi� un correo al destinatario " & txtDestinatario.Text & " reportando la generaci�n del requerimiento temporal nro. " & ObtenerNumeros()
    End Sub

    'Sub EnviarCorreo()
    '    Dim objFuncion As LibCapaNegocio.clsFunciones
    '    objFuncion = New LibCapaNegocio.clsFunciones
    '    Try
    '        objFuncion.SendMail("Depsa Files � Sistema de Gesti�n de Archivos <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", _
    '                                    txtDestinatario.Text, _
    '                                    CStr(Session.Item("NomCliente")) & " - Registro para Aprobar", _
    '                                    "Se ha registrado un requerimiento temporal con los siguientes datos: <br>" & _
    '                                    "Nro. Requerimiento Temporal :   " & Me.lblNroRequerimiento.Text & " <br>" & _
    '                                    "Tipo de Env�o       :   " & Me.lblTipoEnvio.Text & "<br>" & _
    '                                    "�rea       :   " & Me.lblArea.Text & "<br>" & _
    '                                    "Usuario       :   " & Me.lblUsuario.Text & "<br>" & _
    '                                    "Solicitud       :   " & Me.lblSolicitud.Text & "<br>" & _
    '                                    "Comentarios    :   " & txtComentario.Text & "<br>" & _
    '                                    "Fecha       :   " & Me.lblFecha.Text & "<br><br><br>" & _
    '                                    "Hora    :   " & Me.lblHora.Text & "<BR> Dep�sitos S.A. - Sistema de Gesti�n de Archivos Depsa Files")
    '        Me.lblError.Text = "Su solicitud fue enviada con �xito!"
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub
    Sub EnviarCorreo()
        Dim objFuncion As LibCapaNegocio.clsFunciones
        objFuncion = New LibCapaNegocio.clsFunciones
        Try
            '"Nro. Requerimiento Temporal :   " & Me.lblNroRequerimiento.Text & " <br>" & _
            '"�rea       :   " & Me.lblArea.Text & "<br>" & _
            objFuncion.SendMail("Depsa Files � Sistema de Gesti�n de Archivos <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                                        txtDestinatario.Text,
                                        CStr(Session.Item("NomCliente")) & " - Registro para Aprobar",
                                        "Se ha registrado un requerimiento temporal con los siguientes datos: <br>" &
                                        "Nro. Requerimiento Temporal :   " & ObtenerNumeros() & " <br>" &
                                        "Tipo de Env�o       :   " & Me.lblTipoEnvio.Text & "<br>" &
                                        "Usuario       :   " & Me.lblUsuario.Text & "<br>" &
                                        "Solicitud       :   " & Me.lblSolicitud.Text & "<br>" &
                                        "Comentarios    :   " & txtComentario.Text & "<br>" &
                                        "Fecha       :   " & Me.lblFecha.Text & "<br><br><br>" &
                                        "Hora    :   " & Me.lblHora.Text & "<BR> Dep�sitos S.A. - Sistema de Gesti�n de Archivos Depsa Files")
            Me.lblError.Text = "Su solicitud fue enviada con �xito!"
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("EstadoRequerimiento.aspx")
    End Sub

    Private Sub btnVerEstado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("EstadoRequerimiento.aspx")
    End Sub

    Private Function ObtenerNumeros() As String
        Dim dgItem As DataGridItem
        Dim strCodRequ As String

        For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
            dgItem = Me.dgdResultado.Items(intI)
            strCodRequ = strCodRequ & "," & dgItem.Cells(0).Text
        Next

        If Me.dgdResultado.Items.Count = 1 Then
            strCodRequ = Mid(strCodRequ, 2)
        End If

        Return strCodRequ
        Exit Function

    End Function

    Protected Overrides Sub Finalize()
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
