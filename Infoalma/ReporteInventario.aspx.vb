﻿Imports DEPSA.LibCapaNegocio
Imports System.Text
Imports System.IO
Imports System.Data
Imports Infodepsa
Partial Class ReporteInventario
    Inherits System.Web.UI.Page
    Private objInventario As New clsCNInventario

    Private dt As New DataTable
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "43") Then
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "43"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                lblOpcion.Text = ""
                lblOpcion.Text = "Servicio de Inventario"
                If Session.Item("IdTipoEntidad") = "01" Or Session.Item("IdTipoEntidad") = "02" Or Session.Item("IdTipoEntidad") = "03" Then
                    Me.lblTipEntidad.Text = "Almacén"
                    'Me.cboAlmacen.Visible = True
                    ListarAlmacen()
                    'Conductor
                ElseIf Session.Item("IdTipoEntidad") = "04" Then
                    Me.lblTipEntidad.Text = "Cliente"
                    'Me.cboCliente.Visible = True
                    ListaClientes()
                End If
                If Session.Item("CadenaBusquedaTraders") = Nothing Then
                    Dim dttFecha As DateTime
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    dttFecha = Date.Today.Subtract(TimeSpan.FromDays(360))
                    Dia = "00" + CStr(dttFecha.Day)
                    Mes = "00" + CStr(dttFecha.Month)
                    Anio = "0000" + CStr(dttFecha.Year)
                    Me.txtFechaDesde.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + (CStr(Now.Year) + 1)
                    Me.txtFechaHasta.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                End If
                InicializaBusqueda()
                Bindatagrid()

            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub


    Private Sub ListaClientes()
        Try
            Me.cboCliente.Items.Clear()
            Dim dtClie As DataTable
            dtClie = objInventario.gCNGetListarClienteInventario(Session.Item("IdSico"))
            If dtClie.Rows.Count = 0 Then
                Me.cboCliente.Items.Clear()
            Else
                For Each dr As DataRow In dtClie.Rows
                    Me.cboCliente.Items.Add(New ListItem(dr("NOMBRE").ToString(), dr("CODIGO")))
                Next
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub ListarAlmacen()
        Try
            Me.cboCliente.Items.Clear()
            Dim dtAlma As DataTable
            dtAlma = objInventario.gCNgetlistaralmacen(Session.Item("IdSico"))
            If dtAlma.Rows.Count = 0 Then
                Me.cboCliente.Items.Clear()
            Else
                Me.cboCliente.Items.Add(New ListItem("TODOS", "0"))
                For Each dr As DataRow In dtAlma.Rows
                    Me.cboCliente.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
                Next
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Bindatagrid()
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaTraders") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaTraders").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.txtFechaDesde.Value = strFiltros(0)
            Me.txtFechaHasta.Value = strFiltros(1)
            Me.cboEstado.SelectedValue = strFiltros(2)
            Me.txtVapor.Text = strFiltros(3)
            Me.cboCliente.SelectedValue = strFiltros(4)
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaTraders", Me.txtFechaDesde.Value & "-" & Me.txtFechaHasta.Value & "-" & Me.cboEstado.SelectedValue & "-" & Me.txtVapor.Text & "-" & Me.cboCliente.SelectedValue)
    End Sub


    Public Sub imgVer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgVer As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgVer.Parent.Parent, DataGridItem)
            Session.Add("NU_DOCU_INVE", dgi.Cells(0).Text())
            Response.Redirect("ReporteInventarioDetalle.aspx")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Bindatagrid()
        Try
            dt = objInventario.gCNGetbuscarDocInventario(Me.txtFechaDesde.Value, Me.txtFechaHasta.Value, Me.txtVapor.Text,
             Session.Item("IdSico"), Me.cboCliente.SelectedValue, Me.cboEstado.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Total de registros: " & dt.Rows.Count
            Session.Add("dtINV", dt)
            RegistrarCadenaBusqueda()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub


    'Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
    '    'If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '    '    If e.Item.DataItem("CO_ESTA").ToString.Trim = "0" Then
    '    '        e.Item.BackColor = System.Drawing.Color.FromName("#FFFFFF")
    '    '    ElseIf e.Item.DataItem("CO_ESTA").ToString.Trim = "1" Then
    '    '        e.Item.BackColor = System.Drawing.Color.FromName("#c8dcf0")
    '    '    ElseIf e.Item.DataItem("CO_ESTA").ToString.Trim = "X" Then
    '    '        e.Item.BackColor = System.Drawing.Color.FromName("#dcdcdc")
    '    '    End If
    '    'End If
    'End Sub

    'Protected Sub cboCliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliente.SelectedIndexChanged
    '    ListarAlmacen()
    'End Sub
    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim page As Page = New Page
        Dim form As HtmlForm = New HtmlForm
        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtINV"), DataTable)
        dg.DataBind()
        dg.EnableViewState = False
        page.DesignerInitialize()
        page.Controls.Add(form)
        form.Controls.Add(dg)
        page.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=ServInventario.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
        dg = Nothing
    End Sub
End Class
