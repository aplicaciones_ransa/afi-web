<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepConsultaWarrant.aspx.vb" Inherits="Infodepsa.RepConsultaWarrant"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepConsultaWarrant</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="TextoRep">
					<TD></TD>
					<TD align="center">
						<asp:label id="lblTitulo" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">REPORTE DE CONSULTA DE WARRANTS</asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="TextoRep">
					<TD>&nbsp;
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
					<TD></TD>
					<TD>
						<asp:label id="lbFE_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD></TD>
					<TD></TD>
					<TD>
						<asp:label id="lbHO_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD align="center" colSpan="3">
						<TABLE id="Table2" style="FONT-SIZE: 8pt; FONT-FAMILY: Tahoma" cellSpacing="4" cellPadding="0"
							width="700" border="0">
							<TR class="TextoRep">
								<TD width="13%">Estado Warrant</TD>
								<TD width="1%">:</TD>
								<TD width="30%">
									<asp:label id="lblEstado" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD width="13%">Tipo Warrant</TD>
								<TD width="1%">:</TD>
								<TD width="42%">
									<asp:label id="lblTipoWar" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD>N�mero Warrant</TD>
								<TD>:</TD>
								<TD>
									<asp:label id="lblNroWarr" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD>Nombre Entidad</TD>
								<TD>:</TD>
								<TD>
									<asp:label id="lblEntidad" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD align="center" colSpan="3">
						<asp:datagrid id="dgdResultado" tabIndex="2" runat="server" PageSize="20" Width="100%" AutoGenerateColumns="False"
							BorderColor="Gainsboro" CssClass="gv">
							<FooterStyle HorizontalAlign="Right"></FooterStyle>
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="TI_TITU" HeaderText="TipoWarrant"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro.Warrant"></asp:BoundColumn>
								<asp:BoundColumn DataField="FE_EMIS" HeaderText="Fec.Emis." DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="FE_VENC_VIGE" HeaderText="Fec.Vcmto" DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="FE_MAXI_VENC" HeaderText="Fec.Max.Vcmto." DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="FE_CANC" HeaderText="Fec.Cancel" DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="MONEDA" HeaderText="Mon."></asp:BoundColumn>
								<asp:BoundColumn DataField="VAL_WARR" HeaderText="Valor" DataFormatString="{0:N2}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NO_FINA" HeaderText="Financiador"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<P align="center"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
								name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:window.close();" type="button" value="Cerrar"
								name="btnCerrar"></P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
