<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepCajasVaciasPoderCliente.aspx.vb" Inherits="Infodepsa.RepCajasVaciasPoderCliente" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepVencimientoCajas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" align="center" width="339"><asp:label id="Label1" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">REPORTE CAJAS VACIAS EN PODER DEL CLIENTE</asp:label></TD>
					<TD width="190"></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"><asp:label id="lbCO_USUA_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="Label2" runat="server" Font-Size="8pt" Font-Names="Tahoma">PAG. 001</asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbFE_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbHO_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="600" border="0" align="center">
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label3" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbNO_CLIE_ADIC" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label9" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">AREA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label8" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbDE_AREA" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label4" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">FECHA AL</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label10" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbFE_EXIS" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD align="center" colSpan="3"><asp:datagrid id="dgdResultado" tabIndex="2" runat="server" Font-Size="8pt" Font-Names="Tahoma"
							AutoGenerateColumns="False" Width="496px" CellPadding="0" AllowSorting="True" PageSize="20" AllowPaging="True">
							<AlternatingItemStyle BorderColor="White" BackColor="White"></AlternatingItemStyle>
							<HeaderStyle Font-Bold="True" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DE_PERS_SOLI" HeaderText="Solicitante">
									<HeaderStyle Width="40%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_CAJA" HeaderText="Cant. Cajas">
									<HeaderStyle Width="20%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_ENVI_UNVA" HeaderText="Fecha Despacho" DataFormatString="{0:d}">
									<HeaderStyle Width="20%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_CIER_ENTR" HeaderText="Fecha Devoluci&#243;n">
									<HeaderStyle Width="20%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="&amp;gt;s" HorizontalAlign="Center" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<HR>
						<TABLE id="Table3" style="WIDTH: 733px; HEIGHT: 17px" cellSpacing="2" cellPadding="0" width="733"
							border="0">
							<TR class="Texto9pt">
								<TD style="WIDTH: 270px" width="270"><asp:label id="Label11" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"
										Width="233px" Text="Total de Cajas en poder del Cliente">Total de Unidades en poder del Cliente</asp:label></TD>
								<TD width="464"><asp:label id="lbTO_CAJA" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
						</TABLE>
						<BR>
						<asp:HyperLink id="hlRegresar" runat="server" Font-Size="8pt" Font-Names="Tahoma" NavigateUrl="../CajasVaciasPoderCliente.aspx"><< Regresar</asp:HyperLink><BR>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
