<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepCajasExistentesxArea.aspx.vb" Inherits="Infodepsa.RepCajasExistentesxArea" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepVencimientoCajas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" align="center" width="339"><asp:label id="Label1" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">REPORTE CONSOLIDADO POR AREA</asp:label></TD>
					<TD width="190"></TD>
				</TR>
				<TR class="TextoRep">
					<TD style="HEIGHT: 18px" width="190">&nbsp;
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
					<TD style="WIDTH: 339px; HEIGHT: 18px" width="339"></TD>
					<TD style="HEIGHT: 18px" width="190"><asp:label id="Label2" runat="server" Font-Size="8pt" Font-Names="Tahoma">PAG. 001</asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbFE_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbHO_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="600" border="0" align="center">
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label3" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbNO_CLIE_ADIC" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label4" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">FECHA AL</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label6" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbFE_EXIS" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 84px"><asp:label id="Label7" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"
										Visible="False">�REA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label8" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"
										Visible="False">:</asp:label></TD>
								<TD><asp:label id="lbDE_CENT_COST" runat="server" Font-Size="8pt" Font-Names="Tahoma" Visible="False"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD align="center" colSpan="3">
						<P align="center"><asp:label id="Label9" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"
								Width="217px">RESUMEN DEL CONSOLIDADO</asp:label><asp:datagrid id="dgdResultado" tabIndex="2" runat="server" Font-Size="8pt" Font-Names="Tahoma"
								AllowPaging="True" PageSize="20" AllowSorting="True" AutoGenerateColumns="False" Width="662px" ShowFooter="True">
								<FooterStyle HorizontalAlign="Right"></FooterStyle>
								<AlternatingItemStyle BorderColor="White"></AlternatingItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea">
										<HeaderStyle Width="55%"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NU_CAJA_DEPS" HeaderText="Cajas Custodia en DEPSA" DataFormatString="{0:N0}">
										<HeaderStyle Width="15%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="NU_CAJA_CLIE" HeaderText="Cajas Custodia en Clientes" DataFormatString="{0:N0}">
										<HeaderStyle Width="15%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle Width="15%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<%# FormatNumber(DataBinder.Eval(Container.DataItem, "NU_CAJA_CLIE") + DataBinder.Eval(Container.DataItem, "NU_CAJA_DEPS"),0) %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></P>
						<P><asp:datagrid id="dgdResultadoTotal" runat="server" Font-Size="8pt" Font-Names="Tahoma" PageSize="5"
								AutoGenerateColumns="False" Width="240px">
								<AlternatingItemStyle BorderColor="White" BackColor="White"></AlternatingItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="Texto8pt"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="Depsa" HeaderText="Depsa Files">
										<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Clientes" HeaderText="Clientes">
										<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Total" HeaderText="Total">
										<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center"></PagerStyle>
							</asp:datagrid></P>
						<P>
							<asp:HyperLink id="hlRegresar" runat="server" Font-Size="8pt" Font-Names="Tahoma" NavigateUrl="../CajasExistentesxArea.aspx"><< Regresar</asp:HyperLink></P>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
