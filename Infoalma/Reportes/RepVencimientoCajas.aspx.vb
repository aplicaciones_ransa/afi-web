Public Class RepVencimientoCajas
    Inherits System.Web.UI.Page
    Private objVenCaj As VencimientoCajas
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbFE_VENC As System.Web.UI.WebControls.Label
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    Protected WithEvents lbDE_AREA As System.Web.UI.WebControls.Label
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lbCO_TIPO_ITEM As System.Web.UI.WebControls.Label
    Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lbNO_CLIE_ADIC As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents hlRegresar As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If ValidarPagina("PAG17") = False Then Exit Sub
        Me.lbCO_USUA_GENE.Text = Session("IdUsuario").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

        If Not Page.IsPostBack Then
            objVenCaj = CType(context.Handler, VencimientoCajas)
            pr_MUES_DATO()
        End If
    End Sub

    Private Sub pr_MUES_DATO()
        Me.lbNO_CLIE_ADIC.Text = CStr(Session("NomClieAdic"))
        Me.lbCO_TIPO_ITEM.Text = objVenCaj.pCO_TIPO_ITEM
        Me.lbDE_AREA.Text = objVenCaj.pDE_AREA
        Me.lbFE_VENC.Text = objVenCaj.pFE_VENC
        LlenaGrid()
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgdResultado.PageIndexChanged
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Private Sub LlenaGrid()
        Dim dvTemporal As DataView
        dvTemporal = CType(Session("dvReporte"), DataView)
        Me.dgdResultado.DataSource = dvTemporal
        Me.dgdResultado.DataBind()
    End Sub
End Class
