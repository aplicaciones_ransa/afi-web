Public Class RepInventario
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblReferencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblNroReferencia As System.Web.UI.WebControls.Label
    Protected WithEvents lblModalidad As System.Web.UI.WebControls.Label
    Protected WithEvents lblFecVencMerc As System.Web.UI.WebControls.Label
    Protected WithEvents lblCerradoAl As System.Web.UI.WebControls.Label
    Protected WithEvents lblDCRnoCerrado As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And (InStr(Session("PagId"), "INVENTARIO_MERCADERI") Or InStr(Session("PagId"), "30")) Then
            If Not Page.IsPostBack Then
                Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
                Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
                Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)
                Me.lblCerradoAl.Text = Request.QueryString("sFE_CIER")
                Me.lblDCRnoCerrado.Text = Request.QueryString("sMA_CONT")
                Me.lblFecVencMerc.Text = Request.QueryString("sFE_VENC")
                Me.lblModalidad.Text = Request.QueryString("sTI_MODA")
                Me.lblNroReferencia.Text = Request.QueryString("sNU_REFE")
                Me.lblReferencia.Text = Request.QueryString("sTI_REFE")
                LlenaGrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenaGrid()
        Dim dvTemporal As DataView
        dvTemporal = CType(Session("dtMovimiento"), DataView)
        Me.dgdResultado.DataSource = dvTemporal
        Me.dgdResultado.DataBind()
    End Sub
End Class
