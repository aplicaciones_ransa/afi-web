<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepRequeDocumentos.aspx.vb" Inherits="Infodepsa.RepRequeDocumentos" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepRequeDocumentos</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 16px; WIDTH: 832px; POSITION: absolute; TOP: 8px; HEIGHT: 605px"
				cellSpacing="0" cellPadding="0" width="832" border="0">
				<TR class="TextoRep">
					<TD width="210" style="WIDTH: 210px"></TD>
					<TD style="WIDTH: 280px" align="center" width="280"><asp:label id="Label1" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"> REQUERIMIENTO SOLICITADO</asp:label></TD>
					<TD width="190"></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="210" style="WIDTH: 210px; HEIGHT: 19px"><asp:label id="lbCO_USUA_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
					<TD style="WIDTH: 280px; HEIGHT: 19px" width="280"></TD>
					<TD width="190" style="HEIGHT: 19px"><asp:label id="Label2" runat="server" Font-Size="8pt" Font-Names="Tahoma">PAG. 001</asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="210" style="WIDTH: 210px; HEIGHT: 19px"></TD>
					<TD style="WIDTH: 280px; HEIGHT: 19px" width="280"></TD>
					<TD width="190" style="HEIGHT: 19px"><asp:label id="lbFE_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="210" style="WIDTH: 210px; HEIGHT: 20px"></TD>
					<TD style="WIDTH: 280px; HEIGHT: 20px" width="280"></TD>
					<TD width="150" style="HEIGHT: 20px"><asp:label id="lbHO_REPO_GENE" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD width="780" colSpan="3" style="HEIGHT: 93px">
						<TABLE id="Table2" style="WIDTH: 864px; HEIGHT: 86px" cellSpacing="2" cellPadding="0" width="864"
							border="0">
							<TR class="TextoRep">
								<TD style="WIDTH: 112px"><asp:label id="Label3" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">N� REQUERIMIENTO</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD style="WIDTH: 196px"><asp:label id="lbNO_NUMR_REQ" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD style="WIDTH: 122px"></TD>
								<TD style="WIDTH: 1px"></TD>
								<TD style="WIDTH: 7px"></TD>
								<TD style="WIDTH: 7px"></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 112px"><asp:label id="Label4" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">TIPO ENVIO</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label6" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD style="WIDTH: 196px"><asp:label id="lbCO_TIPO_ENVIO" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD style="WIDTH: 122px"><asp:label id="Label7" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma"
										Width="136px">PERSONA AUTORIZADA</asp:label></TD>
								<TD style="WIDTH: 1px"></TD>
								<TD style="WIDTH: 7px"><asp:label id="Label12" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD><asp:label id="lbPER_AUT" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 112px"><asp:label id="Label9" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">ESTADO</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label10" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD style="WIDTH: 196px"><asp:label id="lbDE_AREA" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD style="WIDTH: 122px"><asp:label id="Label8" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">AREA</asp:label></TD>
								<TD style="WIDTH: 1px"></TD>
								<TD style="WIDTH: 7px"><asp:label id="Label11" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label id="lbAREA" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 112px"><asp:label id="Label14" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">FECHA REG.</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label15" runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Tahoma">:</asp:label></TD>
								<TD style="WIDTH: 196px"><asp:label id="lbFE_REG" runat="server" Font-Size="8pt" Font-Names="Tahoma"></asp:label></TD>
								<TD style="WIDTH: 122px"></TD>
								<TD style="WIDTH: 1px"></TD>
								<TD style="WIDTH: 7px"></TD>
								<TD style="WIDTH: 7px"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD vAlign="top" align="center" width="780" colSpan="3">
						<HR style="WIDTH: 865px; HEIGHT: 2px" SIZE="2">
						<asp:datagrid id="dgdResultado" runat="server" Font-Size="8pt" Font-Names="Tahoma" AutoGenerateColumns="False"
							Width="864px" CellPadding="0" AllowSorting="True" PageSize="20" AllowPaging="True" BorderWidth="2px">
							<FooterStyle HorizontalAlign="Center"></FooterStyle>
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="NRO" HeaderText="Nro. Caja"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ID_UNID" HeaderText="ID_UNID"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_CRIT" HeaderText="Tipo Doc." DataFormatString="{0:d}">
									<HeaderStyle Width="30%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DSCR" HeaderText="Descripci&#243;n" DataFormatString="{0:d}">
									<HeaderStyle Width="30%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Detalle Adicional"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<BLOCKQUOTE dir="ltr" style="MARGIN-RIGHT: 0px">
							<P><asp:hyperlink id="hlRegresar" runat="server" Font-Size="8pt" Font-Names="Tahoma" NavigateUrl="../ConsultasxRequerimiento.aspx"><< Regresar</asp:hyperlink>
								<INPUT title="Imprimir" style="BACKGROUND-IMAGE: none; COLOR: #0000cc; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: transparent; TEXT-DECORATION: underline; BORDER-BOTTOM-STYLE: none"
									type="button"  onclick="window.print();" size="10" value="Imprimir" name="imp_jc"></P>
						</BLOCKQUOTE>
						<P><BR>
							&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
