<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepPersonasAutorizadas.aspx.vb" Inherits="Infodepsa.RepPersonasAutorizadas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepVencimientoCajas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" align="center" width="339"><asp:label id="Label1" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True"> REPORTE PERSONAS AUTORIZADAS</asp:label></TD>
					<TD width="190"></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"><asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="Label2" runat="server" Font-Names="Tahoma" Font-Size="8pt">PAG. 001</asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="600" border="0" align="center">
							<TR>
								<TD style="WIDTH: 102px"><asp:label id="Label14" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label3" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="LBLCLIENTE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 102px"><asp:label id="Label9" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">AREA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label10" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="lbDE_AREA" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD align="center" colSpan="3">
						<HR>
						<asp:datagrid id="dgdResultado" tabIndex="2" runat="server" Font-Names="Tahoma" Font-Size="8pt"
							AllowPaging="True" AllowSorting="True" CellPadding="0" Width="546px" AutoGenerateColumns="False"
							PageSize="20">
							<AlternatingItemStyle BorderColor="White" BackColor="White"></AlternatingItemStyle>
							<HeaderStyle Font-Bold="True"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="DE_AREA" HeaderText="&#193;rea">
									<HeaderStyle Width="40%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_PERS" HeaderText="Codigo">
									<HeaderStyle Width="20%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_NOMB_PERS" HeaderText="Persona">
									<HeaderStyle Width="40%"></HeaderStyle>
									<ItemStyle VerticalAlign="Top"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NI_AUTO" HeaderText="Nivel">
									<HeaderStyle Width="20%"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<P><BR>
							<asp:HyperLink id="hlRegresar" runat="server" Font-Names="Tahoma" Font-Size="8pt" NavigateUrl="../PersonasAutorizadas.aspx"><< Regresar</asp:HyperLink></P>
						<P>&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
