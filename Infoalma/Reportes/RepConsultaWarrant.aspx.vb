Public Class RepConsultaWarrant
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lblNroWarr As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoWar As System.Web.UI.WebControls.Label
    Protected WithEvents lblEntidad As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents lblEstado As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "CONSULTA_WARRANT") Then
            If Not Page.IsPostBack Then
                Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
                Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
                Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)
                Me.lblEntidad.Text = Request.QueryString("sNO_ENTI")
                Me.lblEstado.Text = Request.QueryString("sES_WARR")
                Me.lblNroWarr.Text = Request.QueryString("sNU_WARR")
                Me.lblTipoWar.Text = Request.QueryString("sTI_WARR")
                LlenaGrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenaGrid()
        Dim dvTemporal As DataView
        dvTemporal = CType(Session("dvWarrant"), DataView)
        Me.dgdResultado.DataSource = dvTemporal
        Me.dgdResultado.DataBind()
    End Sub
End Class
