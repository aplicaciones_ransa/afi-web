Public Class RepCajasVaciasPoderCliente
    Inherits System.Web.UI.Page
    Private obj As CajasVaciasPoderCliente
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lbTO_CAJA As System.Web.UI.WebControls.Label
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents lbFE_EXIS As System.Web.UI.WebControls.Label
    Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents lbDE_AREA As System.Web.UI.WebControls.Label
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents lbNO_CLIE_ADIC As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents hlRegresar As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If ValidarPagina("PAG19") = False Then Exit Sub
        Me.lbCO_USUA_GENE.Text = Session("IdUsuario").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

        If Not Page.IsPostBack Then
            obj = CType(context.Handler, CajasVaciasPoderCliente)
            pr_MUES_DATO()
        End If
    End Sub

    Private Sub pr_MUES_DATO()
        Me.lbNO_CLIE_ADIC.Text = CStr(Session("NomClieAdic"))
        Me.lbFE_EXIS.Text = obj.pFE_EXIS
        Me.lbDE_AREA.Text = obj.pDE_AREA
        Me.lbTO_CAJA.Text = obj.pTO_CAJA
        LlenaGrid()
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgdResultado.PageIndexChanged
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Private Sub LlenaGrid()
        Dim dvTCSOLI As DataView
        dvTCSOLI = CType(Session("dvReporte"), DataView)
        Me.dgdResultado.DataSource = dvTCSOLI
        Me.dgdResultado.DataBind()
    End Sub
End Class
