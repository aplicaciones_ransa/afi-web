<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepInventario.aspx.vb" Inherits="Infodepsa.RepInventario"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepInventario</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" border="0" width="100%">
				<TR class="TextoRep">
					<TD></TD>
					<TD align="center"><asp:label id="lblTitulo" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">REPORTE DE CONSULTA DE WARRANTS</asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="TextoRep">
					<TD>&nbsp;
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
					<TD></TD>
					<TD><asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD></TD>
					<TD></TD>
					<TD><asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD align="center" colSpan="3">
						<TABLE id="Table2" style="FONT-SIZE: 8pt; FONT-FAMILY: Tahoma" cellSpacing="4" cellPadding="0"
							width="700" border="0">
							<TR class="TextoRep">
								<TD width="13%">Referencia</TD>
								<TD width="1%">:</TD>
								<TD width="30%"><asp:label id="lblReferencia" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
								<TD width="15%">Nro Referencia</TD>
								<TD width="1%">:</TD>
								<TD width="40%"><asp:label id="lblNroReferencia" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD>Modalidad</TD>
								<TD>:</TD>
								<TD><asp:label id="lblModalidad" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
								<TD>Fec.Venc.Merc</TD>
								<TD>:</TD>
								<TD><asp:label id="lblFecVencMerc" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR>
								<TD>Cerrado Al</TD>
								<TD>:</TD>
								<TD><asp:label id="lblCerradoAl" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
								<TD>+ DCR no cerrado</TD>
								<TD>:</TD>
								<TD><asp:label id="lblDCRnoCerrado" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center" colSpan="3"></TD>
				</TR>
				<TR class="TextoRep">
					<TD align="center" colSpan="3"><asp:datagrid id="dgdResultado" tabIndex="2" runat="server" Width="90%" PageSize="20" AutoGenerateColumns="False"
							BorderColor="Gainsboro" CssClass="gv">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_SECU" HeaderText="&#205;tem">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_REGI_INGR" HeaderText="Fch.Ingreso" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_BODE_UBIC" HeaderText="Cod.Bodega">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_TIPO_BULT" HeaderText="Unid">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_SALD" HeaderText="Saldo" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_SALD" HeaderText="Cnt.Bulto" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc. Mercader&#237;a"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_TIPO_TARI" HeaderText="Tipo Tarifa"></asp:BoundColumn>
								<asp:BoundColumn DataField="IM_UNIT" HeaderText="Valor Unit" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="IM_REMO_SALD" HeaderText="Importe Saldo" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_UNID" HeaderText="Unidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Producto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="IM_TOTA_DOLA" HeaderText="Importe $" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_DUAS" HeaderText="Nro.Duas">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_ABAN_LEGA" HeaderText="Fec.Aban.Legal" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_VCTO_MERC" HeaderText="Fch.Vencimiento" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_ESTA_MERC" HeaderText="Est.Merc"></asp:BoundColumn>
								<asp:BoundColumn DataField="TI_TITU" HeaderText="Tip.Titulo">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro.Titulo">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
							name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:window.close();" type="button" value="Cerrar"
							name="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
