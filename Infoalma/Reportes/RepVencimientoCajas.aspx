<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RepVencimientoCajas.aspx.vb" Inherits="Infodepsa.RepVencimientoCajas" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>RepVencimientoCajas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" align="center" width="339"><asp:label id="Label1" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True"> REPORTE VENCIMIENTO DE CAJAS</asp:label></TD>
					<TD width="190"></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"><asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="Label2" runat="server" Font-Names="Tahoma" Font-Size="8pt">PAG. 001</asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep">
					<TD width="190"></TD>
					<TD style="WIDTH: 339px" width="339"></TD>
					<TD width="190"><asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="TextoRep" vAlign="top">
					<TD colSpan="3">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="600" border="0" align="center">
							<TR class="TextoRep">
								<TD style="WIDTH: 119px"><asp:label id="Label3" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label5" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="lbNO_CLIE_ADIC" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 119px"><asp:label id="Label4" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">TIPO UNIDAD</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label6" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="lbCO_TIPO_ITEM" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 119px"><asp:label id="Label9" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">AREA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label10" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="lbDE_AREA" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
							<TR class="TextoRep">
								<TD style="WIDTH: 119px"><asp:label id="Label14" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">FEC. VENCIMIENTO AL</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label id="Label15" runat="server" Font-Names="Tahoma" Font-Size="8pt" Font-Bold="True">:</asp:label></TD>
								<TD><asp:label id="lbFE_VENC" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR class="TextoRep">
					<TD vAlign="top" align="center" colSpan="3">
						<HR>
						<asp:datagrid id="dgdResultado" runat="server" Font-Names="Tahoma" Font-Size="8pt" BorderWidth="2px"
							AllowPaging="True" PageSize="20" AllowSorting="True" CellPadding="0" Width="534px" AutoGenerateColumns="False">
							<FooterStyle HorizontalAlign="Center"></FooterStyle>
							<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" BackColor="White"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ID_UNID" HeaderText="Nro. Caja"></asp:BoundColumn>
								<asp:BoundColumn DataField="FE_INGR" HeaderText="Fecha Ingreso" DataFormatString="{0:d}">
									<HeaderStyle Width="30%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_VENC" HeaderText="Fecha Vencimiento" DataFormatString="{0:d}">
									<HeaderStyle Width="30%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ST_UBIC" HeaderText="Ubicaci&#243;n">
									<HeaderStyle Width="120px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<P>
							<asp:HyperLink id="hlRegresar" runat="server" Font-Names="Tahoma" Font-Size="8pt" NavigateUrl="../VencimientoCajas.aspx"><< Regresar</asp:HyperLink></P>
						<P><BR>
							&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
