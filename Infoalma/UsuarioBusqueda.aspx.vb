Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Infodepsa
Imports System.Data

Public Class UsuarioBusqueda
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNombres As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_USUARIO") Then
            Me.lblError.Text = ""
            Try
                If Not IsPostBack Then
                    Me.txtNombres.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_USUARIO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    loadTipoEntidad()
                    If Session.Item("IdTipoEntidad") <> "01" Then
                        Me.cboTipoEntidad.SelectedValue = Session.Item("IdTipoEntidad")
                        Me.cboTipoEntidad.Enabled = False
                    End If
                    loadEntidad()
                    If Session.Item("IdTipoEntidad") <> "01" Then
                        Me.cboEntidad.SelectedValue = Session.Item("IdSico")
                        Me.cboEntidad.Enabled = False
                    End If
                    'If Session.Item("ubPag") = Nothing Then
                    '    Session.Add("ubTipoEntidad", Me.cboTipoEntidad.SelectedValue)
                    '    Session.Add("ubEntidad", Me.cboEntidad.SelectedValue)
                    '    Session.Add("ubNombres", Me.txtNombres.Text.Replace("'", ""))
                    'Else
                    '    Me.cboTipoEntidad.SelectedValue = Session.Item("ubTipoEntidad")
                    '    loadEntidad()
                    '    Me.cboEntidad.SelectedValue = Session.Item("ubEntidad")
                    '    Me.txtNombres.Text = Session.Item("ubNombres")
                    'End If
                    InicializaBusqueda()
                    BindDatagrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaUsuario") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaUsuario").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.cboTipoEntidad.SelectedValue = strFiltros(0)
            loadEntidad()
            Me.cboEntidad.SelectedValue = strFiltros(1)
            Me.txtNombres.Text = strFiltros(2)
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaUsuario", Me.cboTipoEntidad.SelectedValue & "-" & Me.cboEntidad.SelectedValue & "-" & Me.txtNombres.Text)
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            'dt = objUsuario.gGetBusquedaUsuario(Session.Item("ubTipoEntidad"), Session.Item("ubEntidad"), Session.Item("ubNombres"))
            dt = objUsuario.gGetBusquedaUsuario(Me.cboTipoEntidad.SelectedValue, Me.cboEntidad.SelectedValue, Me.txtNombres.Text)
            Session.Add("dtUsuario", dt)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt = Nothing
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Response.Redirect("UsuarioNuevo.aspx?Cod=" & dgi.Cells(0).Text(), False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 2)
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As DataTable
        Dim i As Integer
        Try
            Me.cboEntidad.Items.Clear()
            If Me.cboTipoEntidad.SelectedValue = "0" Then
                Me.cboEntidad.Items.Add(New ListItem("---------------Todos---------------", "0"))
            Else
                dtEntidad = objEntidad.gGetEntidades(Me.cboTipoEntidad.SelectedValue, "")
                Me.cboEntidad.Items.Add(New ListItem("---------------Todos---------------", "0"))
                For i = 0 To dtEntidad.Rows.Count - 1
                    Me.cboEntidad.Items.Add(New ListItem(dtEntidad.Rows(i)("NOM_ENTI"), dtEntidad.Rows(i)("COD_SICO")))
                Next
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        'Session.Add("ubTipoEntidad", Me.cboTipoEntidad.SelectedValue)
        'Session.Add("ubEntidad", Me.cboEntidad.SelectedValue)
        'Session.Add("ubNombres", Me.txtNombres.Text.Replace("'", ""))
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Private Sub cboTipoEntidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoEntidad.SelectedIndexChanged
        loadEntidad()
    End Sub

    Public Sub imgEstado_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_USUARIO", "CAMBIAR ESTADO DE USUARIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim imgEstado As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim strIdUsuario As String
            dgi = CType(imgEstado.Parent.Parent, DataGridItem)
            strIdUsuario = dgi.Cells(0).Text()
            Me.objUsuario.gCambiarEstadoUsuario(strIdUsuario, dgi.Cells(8).Text(), dgi.Cells(7).Text)
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnEstado As ImageButton
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            btnEstado = CType(e.Item.FindControl("imgEstado"), ImageButton)
            If e.Item.DataItem("EST_USER") = False Then
                btnEstado.ToolTip = "Anulado"
                btnEstado.ImageUrl = "Images/anulado.gif"
            Else
                btnEstado.ToolTip = "Activo"
                btnEstado.ImageUrl = "Images/Activar.gif"
            End If
            If Session.Item("IdTipoUsuario") = "04" Then 'Solo el Administrador de DEPSA podra anular usuarios
                btnEstado.Enabled = True
                If e.Item.DataItem("COD_SICO") = "0" Then
                    btnEstado.ImageUrl = "Images/anulado1.gif"
                    btnEstado.Enabled = False
                End If
            Else
                btnEstado.Enabled = False
                If e.Item.DataItem("EST_USER") = False Then
                    btnEstado.ImageUrl = "Images/anulado1.gif"
                Else
                    btnEstado.ImageUrl = "Images/Activar1.gif"
                End If
            End If
        End If
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        Try
            If e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemType = ListItemType.AlternatingItem Then
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                Else
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgExport.DataSource = Session("dtUsuario")
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'Me.dgdResultado.RenderControl(htmlWriter)
        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub
End Class
