<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmTicketBalanza.aspx.vb" Inherits="AlmTicketBalanza" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Tickets de Balanza</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">


        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function AbrirDetalleTicket(sCO_UNID, sCO_ALMA, sTI_DOCU, sNU_DOCU, sTI_TIK) {
            var ventana = 'Popup/AlmImpresionTicketBalanza.aspx?sCO_UNID=' + sCO_UNID + '&sCO_ALMA=' + sCO_ALMA + '&sTI_DOCU=' + sTI_DOCU + '&sNU_DOCU=' + sNU_DOCU + '&sTI_TIK=' + sTI_TIK;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }

        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);
            if (fchIni == "" && fchFin == "") {
                alert("Debe Ingresar fecha Inicial y Final");
                return false;
            }

            if (fchIni > fchFin && fchIni != "" && fchFin != "") {
                alert("La fecha inicial no puede ser mayor a la fecha final");
                return false;
            }
            return true;
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
			<table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <uc1:Header ID="Header1" runat="server"></uc1:Header>
                        <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                            border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                            <tr>
                                <td valign="top">
                                    <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                        <tr>
                                            <td></td>
                                            <td width="100%">
                                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" rowspan="3">
                                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                            </td>
                                            <td valign="top" width="100%" align="center">
                                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                    <tr>
                                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                                        <td>
                                                            <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="Text" width="17%">Tipo Movimiento</td>
                                                                    <td width="1%">:</td>
                                                                    <td width="35%">
                                                                        <asp:DropDownList ID="cboTipoTicket" runat="server" CssClass="Text" AutoPostBack="True">
                                                                            <asp:ListItem Value="3">Todos</asp:ListItem>
                                                                            <asp:ListItem Value="1">Ingreso</asp:ListItem>
                                                                            <asp:ListItem Value="2">Salida</asp:ListItem>
                                                                            <asp:ListItem Value="4">Otros</asp:ListItem>
                                                                        </asp:DropDownList></td>
                                                                    <td class="Text" width="13%">Referencia</td>
                                                                    <td width="1%">:</td>
                                                                    <td width="15%">
                                                                        <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                    <td width="20%">
                                                                        <asp:TextBox ID="txtNroReferencia" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text">Fecha Ingreso/Retiro</td>
                                                                    <td></td>
                                                                    <td>
                                                                        <table id="Table5" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td class="Text">Del:</td>
                                                                                <td>
                                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                        maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="text" value="..."
                                                                                            type="button" name="btnFecha"></td>
                                                                                <td class="Text">Al:</td>
                                                                                <td class="Text">
                                                                                    <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                        maxlength="10" size="6" name="txtFechaFinal" runat="server"><input id="btnFechaFinal" class="text" value="..."
                                                                                            type="button" name="btnFecha"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="Text" align="left">Modalidad</td>
                                                                    <td width="1%">:</td>
                                                                    <td class="Text" colspan="2">
                                                                        <asp:DropDownList ID="cboModalidad" runat="server" CssClass="text"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text">Almac�n</td>
                                                                    <td>:</td>
                                                                    <td colspan="7">
                                                                        <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" Width="300px"></asp:DropDownList></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                                <asp:Label Style="z-index: 0" ID="lblMensaje" runat="server" CssClass="Error"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center">
                                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                    <tr>
                                                        <td width="80">
                                                            <asp:Button Style="z-index: 0" ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                        <td width="80">
                                                            <asp:Button ID="btnExportarExcel" runat="server" CssClass="btn" Width="100px" Text="Exportar a Excel"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center">
                                                <asp:DataGrid Style="z-index: 0" ID="dgResultado" runat="server" CssClass="gv" OnPageIndexChanged="Change_Page"
                                                    BorderColor="Gainsboro" PageSize="30" AllowPaging="True" AutoGenerateColumns="False">
                                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="Cod.Unid" HeaderText="Cod.Unid"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="Cod.Alma" HeaderText="Cod.Alma"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NroTicket" HeaderText="Nro. Ticket">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="TipoTicket" HeaderText="TipoTicket">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DeTipoTicket" HeaderText="Tipo Movimiento">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Descripcion" HeaderText="Descripci&#243;n">
                                                            <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Placa" HeaderText="Placa">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="FechaTran" HeaderText="Fecha Pesada">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="HoraTran" HeaderText="Hora Pesada">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NetoKg" HeaderText="Neto Kg" DataFormatString="{0:N2}">
                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Ver">
                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgVer" runat="server" ImageUrl="Images/Ver.jpg" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                        Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
