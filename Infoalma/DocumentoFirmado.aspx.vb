Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports Infodepsa
Imports System.Data

Public Class DocumentoFirmado
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objEntidad As Entidad = New Entidad
    Private objReportes As Reportes = New Reportes
    Private objDocumento As Documento = New Documento
    Private strEndosoPath = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    Private strFin As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadFinanciera")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private objLiberacion As Liberacion = New Liberacion
    Dim strNroDoc As String
    Dim strIdTipoDocumento As String
    Dim strCod_TipOtro As String
    Dim strIdSico As String
    Dim strNroLib As String
    Dim strPeriodoAnual As String
    Dim strCodDoc As String
    Dim strCod_Unidad As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "1"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
            End If

            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "1", "FIRMAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            '---nuevocambio
            '---Recorriendo Datatable em session y firmando los documentos
            Dim dtIngreso001 As DataTable = New DataTable
            Dim i As Integer = 0
            Dim strMensaje As String
            strNroDoc = "" : strNroLib = "" : strIdTipoDocumento = "" : strCod_TipOtro = "" : strPeriodoAnual = "" : strCodDoc = "" : strIdSico = ""
            '===== Recorrer Datatable =====
            If Not dtIngreso001.Rows.Count = 0 Then
                dtIngreso001.Rows.Clear()
            Else
                dtIngreso001 = Session.Item("dtIngreso001")
            End If

            Session.Add("sstrContrasenaClave", Request.Form("txtContrasena"))
            Session.Add("ssNuevaContrasenaClave", Request.Form("txtNuevaContrasena"))

            For Each dr1 As DataRow In dtIngreso001.Rows
                'Valores de ssession enviados por la pagina :
                strNroDoc = dtIngreso001.Rows(i).Item(1)
                strNroLib = dtIngreso001.Rows(i).Item(3)
                strIdTipoDocumento = dtIngreso001.Rows(i).Item(4)
                strPeriodoAnual = dtIngreso001.Rows(i).Item(5)
                strCod_TipOtro = dtIngreso001.Rows(i).Item(6)
                strCodDoc = dtIngreso001.Rows(i).Item(7)
                strCod_Unidad = dtIngreso001.Rows(i).Item(9)
                strIdSico = Session.Item("IdSico")

                strMensaje = Inicializa(strNroDoc, strNroLib, strIdTipoDocumento, strPeriodoAnual, strCodDoc, strCod_TipOtro, strIdSico, strCod_Unidad)

                If strMensaje = "X" Then
                    Exit For
                End If

                i = i + 1
            Next

        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub


    Private Function Inicializa(strNroDoc As String, strNroLib As String, strIdTipoDocumento As String, strPeriodoAnual As String, strCod_Doc As String, strCod_TipOtro As String, strIdSico As String, strCod_Unidad As String) As String

        Dim strVar As String
        Dim strPagina As String
        strVar = Request.QueryString("var")
        strPagina = Request.QueryString("Pagina")
        Dim strEntidadFinanciera As String
        Dim strArchivoFirmado As String
        Dim strContrasena As String
        Dim strNuevaContrasena As String
        Dim contenido As Byte()

        '--------------------------------------------
        ' Dim SignedData As CAPICOM.SignedData
        '--------------------------------------------
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strMSG As String
            If Session("CertDigital") = True Then
                '-----------COMENTAR TODO ESTE BLOQUE SI SE QUIERE LA VERSION SIN FIRMAS----------------------
                'strArchivoFirmado = Request.Form("txtArchivo_pdf_firmado")
                'If Len(strArchivoFirmado) = 0 Then
                '    Session.Add("Mensaje", "Fallo al firmar el documento, verifique la configuraci�n de su PC")
                '    If strPagina = "1" Then
                '        Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                '    Else
                '        Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                '    End If
                '    Return strMSG
                'End If
                'contenido = Convert.FromBase64String(strArchivoFirmado)
                '-----------------------------------------------------------------------------------------------------------
            Else
                strArchivoFirmado = Request.Form("txtArchivo_pdf")
                contenido = Convert.FromBase64String(strArchivoFirmado)
                strContrasena = Funciones.EncryptString(Session.Item("sstrContrasenaClave").Trim, "���_[]0")
                strNuevaContrasena = Funciones.EncryptString(Session.Item("ssNuevaContrasenaClave").Trim, "���_[]0")
                Dim strIp As String()
                Dim strIp1 As String
                Dim strIp2 As String
                Dim strIp3 As String
                Dim strIp4 As String

                strIp = pr_OBTI_IP_PETI().Split(".")

                If strIp.Length < 3 Then
                    strIp1 = "111111111111"
                Else
                    strIp1 = Right("000" + strIp(0), 3)
                    strIp2 = Right("000" + strIp(1), 3)
                    strIp3 = Right("000" + strIp(2), 3)
                    strIp4 = Right("000" + strIp(3), 3)
                    strIp1 = strIp1 & strIp2 & strIp3 & strIp4
                End If

                strMSG = objDocumento.gGetValidaContrasena(Session.Item("IdUsuario"), strContrasena, strNuevaContrasena, strIp1, objTrans)
                Select Case strMSG
                    Case "I"
                        If Session.Item("ssNuevaContrasenaClave").Trim <> "" Then
                            Session.Add("sstrContrasenaClave", Session.Item("ssNuevaContrasenaClave").Trim)
                        End If
                        Session.Add("strfecha", Now)
                        Exit Select
                    Case "C"
                        Session.Add("Mensaje", "La contrase�a anterior no es correcta")
                        objTrans.Rollback()
                        objTrans.Dispose()
                        If strPagina = "1" Then
                            Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        Else
                            Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        End If
                        Return "X"
                    Case "X"
                        Session.Add("Mensaje", "La contrase�a ingresada no es correcta")
                        objTrans.Rollback()
                        objTrans.Dispose()
                        If strPagina = "1" Then
                            Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        Else
                            Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        End If
                        Return "X"
                    Case "P"
                        Session.Add("Mensaje", "No puede firmar desde la maquina, no es la IP correcta")
                        objTrans.Rollback()
                        objTrans.Dispose()
                        If strPagina = "1" Then
                            Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                        Else
                            Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                        End If
                        Return "X"
                End Select
                '----------------------------------------------------------------------------------------------------------
            End If

            'strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strCod_Doc, strCod_TipOtro, strVar, strIdSico, contenido, objTrans)
            'strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), Session.Item("UsuarioLogin"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, , strIdSico, contenido, objTrans)
            strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), Session.Item("UsuarioLogin"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, Session("CertDigital"),
                                                   strCod_Doc, strCod_TipOtro, strIdSico, Session.Item("IdTipoEntidad"), strCod_Unidad, contenido, strVar, objTrans)
            If strMSG = "X" Then
                Session.Add("Mensaje", "Error al firmar documento")
                objTrans.Rollback()
                objTrans.Dispose()

                If strPagina = "1" Then
                    Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
                Else
                    Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
                End If
                Return "X"
            End If
            Session.Add("Mensaje", strMSG)
            Dim strEstado As String = objDocumento.gContabilizarLiberacion(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, Session.Item("IdUsuario"), objTrans)
            objTrans.Commit()
            objTrans.Dispose()
            '---Descomentar para producci�n, es el envio al filemaster----------------------------------
            If strEstado = "04" Then
                Dim dtProrroga As DataTable
                Dim NombreReporte As String
                Dim strIds As String
                Dim dtDetalle As DataTable
                If strIdTipoDocumento = "02" Then
                    If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                        Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                        objWSFileMaster.CopiaDocumentosInfodepsaXIDDocumento(strCod_Doc)
                    End If
                End If
            End If
            '------------------------------------------------------------------------------------------
            If strPagina = "1" Then
                Response.Redirect("DocumentoPDF.aspx?var=" & strVar, False)
            Else
                Response.Redirect("DocumentoResumen.aspx?var=" & strVar, False)
            End If

            'Registrar Documento 07/12/2018
            If strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "05" Or strIdTipoDocumento = "10" Then 'warrant , wip y prorroga
                Dim dtSecu As DataTable
                dtSecu = objDocumento.gBuscarSecuencia(strIdTipoDocumento, strNroDoc, strNroLib, Session.Item("IdUsuario"))
                If dtSecu.Rows.Count <> 0 Then
                    If Session.Item("IdTipoEntidad") = strFin And dtSecu.Rows(0)("NRO_SECU") = dtSecu.Rows(0)("NRO_SECU_MAX") Then
                        RegistrarDocumento()
                    End If
                End If
            End If

            Return strMSG
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Session.Add("Mensaje", ex.Message)
            Response.Write(ex.Message)
            Return ex.Message
        Finally
            objConexion = Nothing
        End Try
    End Function

    'no se usa
    'Private Sub GrabaDocumento(ByVal strNombArch As String, ByVal ObjTrans As SqlTransaction)
    '    Dim FilePath As String
    '    Dim fs As System.IO.FileStream
    '    Dim Contenido As Byte()
    '    Dim strNombre As String
    '    strNombre = System.IO.Path.GetFileName(strNombArch)
    '    FilePath = strNombArch
    '    fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
    '    ReDim Contenido(fs.Length)
    '    fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
    '    objDocumento.gRegEndosoFolio(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strNombre, Contenido, ObjTrans)
    '    fs.Close()
    'End Sub

    'Nuevo: Movido de la pagina DocumentoPagUno.aspx , boton "Registrar" 07/12/2018
    '[ok] Current.session: 
    Private Sub RegistrarDocumento()

        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "REGISTRAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim drEndoso As DataRow
        Dim drSICO As DataRow
        Dim NombreReporte As String
        Dim strIds As String
        Dim dtProrroga As DataTable
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try

            If strIdTipoDocumento = "05" Then
                dtProrroga = objDocumento.gGetDataProrroga(strNroDoc, strNroLib, strCod_TipOtro)
                If dtProrroga.Rows.Count = 0 Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Session.Add("Mensaje", "No se encontr� la Prorroga")
                    Exit Sub
                Else
                    NombreReporte = strEndosoPath & Session.Item("NombrePDF")
                    drSICO = objEntidad.gUpdProrrogaSICO(strEmpresa, dtProrroga.Rows(0)("COD_TIPDOC"), dtProrroga.Rows(0)("NRO_DOCU"),
                    dtProrroga.Rows(0)("MON_ADEC"), dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("SAL_ACTU"),
                    dtProrroga.Rows(0)("FCH_OBLIGACION"), Session.Item("UsuarioLogin"))
                    If drSICO.IsNull("UL_NUME_FOLI") Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Session.Add("Mensaje", "No se pudo actualizar en SICO")
                        Exit Sub
                    End If
                    objDocumento.gUpdFolioProrroga(drSICO("UL_NUME_FOLI"), drSICO("FE_NUME_FOLI"), drSICO("NU_SECU"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, objTrans)
                    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado la Prorroga<br><br>", "", "S", "N", objTrans)
                    objTrans.Commit()
                    objTrans.Dispose()
                End If
                '   Warrants de Custodia
            ElseIf strIdTipoDocumento = "20" Then
                objDocumento.gUpdRegistrar_Custodia_SICO(strNroDoc, strIdTipoDocumento, strNroLib, strPeriodoAnual, objTrans)
                objReportes.GeneraPDFWarrantFirmas(strCodDoc, strCodDoc, strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), strIdTipoDocumento, objTrans)

                objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado Warrants Custodia <br><br>", "", "S", "N", objTrans)

                objTrans.Commit()
                objTrans.Dispose()
                Session.Add("Mensaje", "Se registr� correctamente")
            ElseIf strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "10" Then
                objDocumento.gRegistraEndosoSICO(strCodDoc, Session.Item("IdUsuario"), objTrans)
                If objDocumento.strResultado = "0" Then
                    objDocumento.gRegistraDobleEndosoSICO(strCodDoc, objTrans)
                    If objDocumento.strResultado <> "0" Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Exit Try
                    End If
                    'dtProrroga = objDocumento.GetIdsDocumentos(strCodDoc, objTrans)
                    'For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    '    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                    '        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, objTrans)
                    '    Else
                    '        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    '    End If
                    '    'strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    'Next
                    'If strIds = "" Then
                    '    objTrans.Rollback()
                    '    objTrans.Dispose()
                    '    Session.Add("Mensaje", "No se encontr� el warrant relacionado a este documento")
                    '    Exit Sub
                    'End If
                    objReportes.GeneraPDFWarrantCopia(strCodDoc, "", strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), strIdTipoDocumento, objTrans)

                    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado el Documento<br><br>", strEndosoPath, "S", "N", objTrans)
                    objTrans.Commit()
                    objTrans.Dispose()
                    Session.Add("Mensaje", "Se registr� correctamente")
                ElseIf objDocumento.strResultado = "3" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Session.Add("Mensaje", "El documento ya fue registrado")
                    Exit Sub
                ElseIf objDocumento.strResultado = "4" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Session.Add("Mensaje", "Debe de realizar el doble endoso en el SICO antes de registrarlo")
                    Exit Sub
                Else
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Session.Add("Mensaje", "Fallo al registrar")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Session.Add("Mensaje", ex.Message.ToString)
        Finally
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
