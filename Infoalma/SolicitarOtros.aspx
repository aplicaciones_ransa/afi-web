<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SolicitarOtros.aspx.vb" Inherits="SolicitarOtros" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Solicitar Caja Completa</title>
    <meta content="False" name="vs_showGrid">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaIngresoDesde").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaIngresoDesde").datepicker('show');
            });

            $("#txtFechaIngresoHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaIngresoHasta").datepicker('show');
            });
        });


        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            Estado.style.display = 'none';
        }
        function chkSelect_OnMouseMove(tableRowId, checkBox) {
            if (checkBox.checked == false)
                tableRowId.style.backgroundColor = "#FFFFC0";
        }

        function chkSelect_OnMouseOut(tableRowId, checkBox, rowIndex) {
            if (checkBox.checked == false) {
                var bgColor;
                if (rowIndex % 2 == 0)
                    bgColor = "WhiteSmoke";
                else
                    bgColor = "Transparent";
                tableRowId.style.backgroundColor = bgColor;
            }
        }

        function chkSelect_OnClick(tableRowId, checkBox, rowIndex) {
            var bgColor;
            if (rowIndex % 2 == 0) {
                bgColor = 'WhiteSmoke';
            }
            else {
                bgColor = 'Transparent';
            }
            if (document.getElementById(checkBox).checked == true) {
                document.getElementById(tableRowId).style.backgroundColor = '#FFFFC0';
            }
            else {
                document.getElementById(tableRowId).style.backgroundColor = bgColor;
            }
        }
        function chkSelectAll_OnClick(checkBox) {
            re = new RegExp('_chkSel$')
            for (i = 0; i <
                document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.id)) {
                        if (!elm.disabled) {
                            elm.checked = checkBox.checked;
                            elm.onclick();
                        }
                    }
                }
            }
        }
        function ValidaCheckBox() {
            var retorno = false;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        retorno = true;
                    }
                }
            }
            if (retorno == false) {
                alert("Seleccione por lo menos un registro para continuar.");
            }
            return retorno;
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="125">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" valign="middle" align="left" height="20">&nbsp;SOLICITAR OTROS
                            </td>
                        </tr>
                        <tr>
                            <td class="td" style="height: 77px">
                                <table id="Table16" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="text" style="width: 68px">�rea</td>
                                                    <td class="text" style="width: 14px" width="14">:</td>
                                                    <td width="140">
                                                        <asp:DropDownList ID="cboArea" runat="server" Width="140px" DataValueField="COD" DataTextField="DES"
                                                            CssClass="Text">
                                                            <asp:ListItem Value="Todas">Todas</asp:ListItem>
                                                            <asp:ListItem Value="Contabilidad" Selected="True">Contabilidad</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 63px"></td>
                                                    <td class="TEXT" style="width: 150px">Fecha Ingreso:</td>
                                                    <td style="width: 137px"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" style="width: 68px">Tipo Medio</td>
                                                    <td class="text" style="width: 14px">:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoItem" runat="server" Width="140px" DataValueField="COD" DataTextField="DES"
                                                            CssClass="Text">
                                                            <asp:ListItem Value="Todas">Todas</asp:ListItem>
                                                            <asp:ListItem Value="Contabilidad" Selected="True">BACKUP</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="Text" style="width: 63px"></td>
                                                    <td class="text" style="width: 150px">&nbsp;Desde&nbsp;
                                                        <input class="text" onkeypress="validarcharfecha()" id="txtFechaIngresoDesde" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaIngresoDesde" runat="server"><input class="Text" id="btnFechaInicial" type="button"
                                                                value="..." name="btnFecha"></td>
                                                    <td class="text" style="width: 137px">Hasta
                                                        <input class="text" onkeypress="validarcharfecha()" id="txtFechaIngresoHasta" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaIngresoHasta" runat="server"><input class="Text" id="btnFechaFinal" type="button"
                                                                value="..." name="btnFecha" size="8"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" style="width: 68px">Nro.&nbsp;</td>
                                                    <td class="text" style="width: 14px">:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtNroCaja" Style="text-transform: uppercase" runat="server" Width="140px" MaxLength="80"
                                                            CssClass="Text"></asp:TextBox></td>
                                                    <td class="Text" style="width: 63px"></td>
                                                    <td class="text" style="width: 150px">&nbsp;</td>
                                                    <td class="text" style="width: 137px"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="CampoObliga" colspan="3"></td>
                                                    <td style="width: 63px"></td>
                                                    <td style="width: 150px" align="center"></td>
                                                    <td style="width: 137px" align="right">
                                                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn"></asp:Button></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" height="20">
                                <asp:Label ID="lblNota" runat="server" Width="100%" CssClass="TEXT" Visible="False">NOTA: las unidades que figuran por defecto como seleccionados, ya se encuentran solicitados y su atenci�n est� en proceso.</asp:Label></td>
                        </tr>
                        <tr>
                            <td class="text" height="20">Resultado:
									<asp:Label ID="lblRegistros" runat="server" CssClass="text">0 registros</asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgdResultado" runat="server" Width="100%" AutoGenerateColumns="False" OnPageIndexChanged="Change_Page"
                                    AllowPaging="True" PageSize="20" AllowSorting="True" CssClass="gv" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" CssClass="Text" runat="server" AutoPostBack="False"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table id="Table12" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:CheckBox ID="chkSel" runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_SEL") %>'></asp:CheckBox></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ID_UNID" SortExpression="ID_UNID" HeaderText="Nro Medio">
                                            <HeaderStyle Width="12%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DES" HeaderText="Descripci&#243;n">
                                            <HeaderStyle Width="25%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_INGR" SortExpression="FE_INGR" HeaderText="Fecha Ingreso">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_PREC" HeaderText="Nro. Precinto">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="ST_UBIC" HeaderText="Ubicaci&#243;n">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="CO_AREA" HeaderText="CO_AREA"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="FLG_SEL" HeaderText="ST_FILA"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td align="right" width="50%">
                                            <asp:Button ID="btnVerSolicitud" runat="server" Width="100px" Text="Ver Solicitud" CssClass="Text"></asp:Button></td>
                                        <td style="width: 3px" align="center"></td>
                                        <td width="50%">
                                            <asp:Button ID="btnIngresarDoc" runat="server" Width="140px" Text="Ingresar Seleccionados" CssClass="btn"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
