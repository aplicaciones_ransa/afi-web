Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports Library.AccesoDB
Public Class EntidadBusqueda
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNombreEntidad As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_ENTIDAD") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_ENTIDAD"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                loadTipoEntidad()
                If Session.Item("IdTipoEntidad") <> "01" Then
                    Me.cboTipoEntidad.SelectedValue = Session.Item("IdTipoEntidad")
                    Me.txtNombreEntidad.Text = objEntidad.gGetDataEntidad(Session.Item("IdSico"), Session.Item("IdTipoEntidad"))("NOM_ENTI")
                    Me.btnBuscar.Enabled = False
                    Me.txtNombreEntidad.Enabled = False
                    Me.cboTipoEntidad.Enabled = False
                End If
                InicializaBusqueda()
                BindDatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaEntidad") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaEntidad").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.cboTipoEntidad.SelectedValue = strFiltros(0)
            Me.txtNombreEntidad.Text = strFiltros(1)
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaEntidad", Me.cboTipoEntidad.SelectedValue & "-" & Me.txtNombreEntidad.Text)
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 1)
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objEntidad.gGetBusquedaEntidad(Me.cboTipoEntidad.SelectedValue, Me.txtNombreEntidad.Text.Replace("'", "").ToUpper)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt = Nothing
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("IdSicoEntidad", dgi.Cells(0).Text())
            Session.Add("IdTipEnti", dgi.Cells(5).Text())
            Response.Redirect("EntidadDetalle.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
        RegistrarCadenaBusqueda()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "WARRANTE", "CONSULTA DE ENTIDAD", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub
    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        Try
            If e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemType = ListItemType.AlternatingItem Then
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                Else
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
