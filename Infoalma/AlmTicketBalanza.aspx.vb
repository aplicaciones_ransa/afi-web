Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class AlmTicketBalanza
    Inherits System.Web.UI.Page
    Private objTicket As clsCNTicket
    Private objParametro As clsCNParametro
    Private objRetiro As clsCNRetiro
    Private objWarrant As clsCNWarrant
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportarExcel As System.Web.UI.WebControls.Button
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    Private objAccesoWeb As clsCNAccesosWeb
    Dim objCNTicket As New clsCNTicket
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipoTicket As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "TICKETS_BALANZA") Then
            Me.lblOpcion.Text = "Tickets de Balanza"
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "TICKETS_BALANZA"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                GetCargarTipoMercaderia()
                LlenarAlmacenes()
                ListaReferencia()
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BuscarDocumentos()
    End Sub

    Private Sub BuscarDocumentos()
        Me.dgResultado.CurrentPageIndex = 0
        pr_REPO_0001()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "TICKETS_BALANZA", "CONSULTA DE TICKETS", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

    End Sub

    Private Sub ListaReferencia()
        objParametro = New clsCNParametro
        objParametro.gCNListarReferencia(Me.cboReferencia, Me.cboTipoTicket.SelectedValue)
    End Sub

    Private Sub pr_REPO_0001()
        Try
            objTicket = New clsCNTicket
            Dim trabajador As String
            Dim entidad As String
            Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"),
                                              Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboTipoTicket.SelectedValue, Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboAlmacen.SelectedValue, Me.cboModalidad.SelectedValue)

            Me.dgResultado.DataSource = dtTCALMA_MERC
            Me.dgResultado.DataBind()
            Session.Add("ssdtTickBala", dtTCALMA_MERC)
        Catch e1 As Exception
            pr_IMPR_MENS(e1.Message)
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pr_REPO_0002()
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        pr_REPO_0001()
    End Sub

    Private Sub GetCargarTipoMercaderia()
        Me.cboModalidad.Items.Clear()
        Try
            objWarrant = New clsCNWarrant
            Dim dt As DataTable
            With objWarrant
                Me.cboModalidad.Items.Add(New ListItem("---- Todos -----", "0"))
                dt = .gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "N")
            End With
            For Each dr As DataRow In dt.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI").trim, dr("CO_MODA_MOVI").trim))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                CType(e.Item.FindControl("imgVer"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirDetalleTicket('" & CStr(e.Item.DataItem("Cod.Unid")).Trim & "','" & CStr(e.Item.DataItem("Cod.Alma")).Trim & "','" & "TIK" & "','" & CStr(e.Item.DataItem("NroTicket")).Trim & "','" & CStr(e.Item.DataItem("TipoTicket")).Trim & "')")
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub pr_REPO_0002()
        Try
            objTicket = New clsCNTicket
            Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"),
                                    Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboTipoTicket.SelectedValue, Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboAlmacen.SelectedValue, Me.cboModalidad.SelectedValue)
            Dim sb As New System.Text.StringBuilder
            For i As Integer = 0 To dtTCALMA_MERC.Rows.Count - 1
                Dim dr As DataRow = dtTCALMA_MERC.Rows(i)
                For y As Integer = 0 To dtTCALMA_MERC.Columns.Count - 1
                    sb.Append(dr.ItemArray(y).ToString)
                    If y <> dtTCALMA_MERC.Columns.Count - 1 Then
                        sb.Append("|")
                    End If
                Next
                sb.Append(ControlChars.CrLf)
            Next
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ldetpt.txt")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.Private)
            Response.ContentType = "application/vnd.text"
            Dim stringWrite As System.IO.StreamWriter
            Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
            Response.Write(sb)
            Response.End()
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objTicket Is Nothing) Then
            objTicket = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboTipoTicket_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoTicket.SelectedIndexChanged
        If Me.cboTipoTicket.SelectedValue = 4 Then
            Me.cboReferencia.Items.Clear()
            Me.cboReferencia.Enabled = False
            Me.txtNroReferencia.Visible = False
        Else
            Me.cboReferencia.Enabled = True
            Me.txtNroReferencia.Visible = True
            ListaReferencia()
        End If
    End Sub

    Private Sub btnExportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        BuscarDocumentos()
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Cancelaciones.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("ssdtTickBala"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
        'objTicket = New clsCNTicket
        ''Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"), _
        ''                        Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboTipoTicket.SelectedValue, Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboAlmacen.SelectedValue, Me.cboModalidad.SelectedValue)

        'Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"), _
        '                        Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboTipoTicket.SelectedValue, Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboAlmacen.SelectedValue, Me.cboModalidad.SelectedValue)
        'Dim strFileName As String = "Reporte.xls"
        'Dim dgExport As DataGrid = New DataGrid

        ''se limpia el buffer del response
        'Response.Clear()
        'Response.Buffer = True

        ''se establece el tipo de accion a ejecutar
        'Response.ContentType = "application/download"
        'Response.Charset = ""
        'Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        'Response.Charset = "UTF-8"
        'Response.ContentEncoding = Encoding.Default

        ''se inhabilita el view state para no ejecutar ninguna accion
        'Me.EnableViewState = False

        ''se rederiza el control datagrid
        'Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        'Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        'dgExport.DataSource = dtTCALMA_MERC
        'dgExport.DataBind()
        'dgExport.RenderControl(htmlWriter)

        ''se escribe el contenido en el listado
        'Response.Write(sWriter.ToString())
        'Response.End()
    End Sub
End Class
