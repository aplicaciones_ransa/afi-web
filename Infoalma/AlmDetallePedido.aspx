<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmDetallePedido.aspx.vb" Inherits="AlmDetallePedido"%>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>�rdenes de Retiro</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script src="JScripts/jquery.min.cache.js"></script>
		<script src="JScripts/jquery-ui.min.cache.js"></script>
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<!--<script language="VBScript">
			Sub SubmitForm(intVar)
					mensaje = msgbox ( "Desea rechazar el retiro" ,vbyesno,"Confirmaci�n")
					if mensaje = vbyes then
						frmArchivo.action = "DocumentoFirmadoPedido.aspx?var=" & intVar & "&Pagina=1"
						frmArchivo.submit
						else
						exit Sub
					end if
			End Sub
			Sub SubmitForm2(intVar)
			    frmArchivo.action = "DocumentoFirmadoPedido.aspx?var=" & intVar & "&Pagina=1"
				frmArchivo.submit
		    End Sub
		</script>-->
		<script language="javascript">
		var $dialog = null;
            jQuery.showModalDialog = function(options) {

            var defaultOptns = {
                url: null,
                dialogArguments: null,
                height: 'auto',
                width: 'auto',
                position: 'top',
                resizable: false,
                scrollable: false,
                onClose: function() { },
                returnValue: null,
                doPostBackAfterCloseCallback: false,
                postBackElementId: null
            };

            var fns = {
                close: function() {
                    opts.returnValue = $dialog.returnValue;
                    $dialog = null;
                    opts.onClose();
                    if (opts.doPostBackAfterCloseCallback) {
                        postBackForm(opts.postBackElementId);
                    }
                },
                adjustWidth: function() { $frame.css("width", "100%"); }
            };

            // build main options before element iteration
            var opts = $.extend({}, defaultOptns, options);
            var $frame = $('<iframe id="iframeDialog" />');

            if (opts.scrollable)
                $frame.css('overflow', 'auto');
                
            $frame.css({
                'padding': 0,
                'margin': 0,
                'padding-bottom': 2
            });

            var $dialogWindow = $frame.dialog({
                autoOpen: true,
                modal: true,
                width: opts.width,
                height: opts.height,
                resizable: opts.resizable,
                position: opts.position,
                overlay: {
                    opacity: 0.5,
                    background: "black"
                },
                close: fns.close,
                resizeStop: fns.adjustWidth
            });

            $frame.attr('src', opts.url);
            fns.adjustWidth();

            $frame.load(function() {
                if ($dialogWindow) {
                    var maxTitleLength = 50;
                    var title = $(this).contents().find("title").html();

                    if (title.length > maxTitleLength) {
                        title = title.substring(0, maxTitleLength) + '...';
                    }
                    $dialogWindow.dialog('option', 'title', title);
                }
            });

            $dialog = new Object();
            $dialog.dialogArguments = opts.dialogArguments;
            $dialog.dialogWindow = $dialogWindow;
            $dialog.returnValue = null;
        }

        function postBackForm(targetElementId) {
            var theform;
            theform = document.forms[0];
            theform.__EVENTTARGET.value = targetElementId;
            theform.__EVENTARGUMENT.value = "";
            theform.submit();
        }
        
		function openWindow(strfirma)
		{			
			//if (document.getElementById('txtTipoUsuario').value != '03')
			//{
			//	SubmitForm2(1);
			//	return;
			//}
			var Argumentos = new Object();			
			if (strfirma == '1')		 
			{
	   			SubmitForm2(2);
			}			
			if (strfirma == '0')
			{
			 var Argumentos = new Object();
				var url = 'Popup/LgnSolicitaFirma.aspx';
				$.showModalDialog({
					url: url,
					dialogArguments: null,
					heigh: 110,
					width: 360,
					scrollable: false,
					onClose: function(){ Argumentos = this.returnValue; 
					if(Argumentos == null)
					{
						window.alert('Usted a cancelado el proceso de firma!');
					}
					else
					{ //quiere decir que se ha devuelto una lista de contactos
						document.getElementById('txtContrasena').value = Argumentos[0];
						document.getElementById('txtNuevaContrasena').value = Argumentos[1];
						SubmitForm2(2);
					}		
					}
				});
			}	  
  	   }
  	   function noboton(ev)
		{
			if (document.all) ev= event;
			if (ev.button & 2)
			alert("Bot�n inactivo.");
		}
  	   function bloquear()
		{
			document.onmousedown = noboton;		
		}
		function SubmitForm(intVar)
			{
				if(confirm("Desea rechazar el retiro"))
				{
					frmArchivo.action ='DocumentoFirmadoPedido.aspx?var='+ intVar + '&Pagina=1' ; 
					frmArchivo.submit();
				}
			}

		function SubmitForm2(intVar)
			{
				frmArchivo.action ='DocumentoFirmadoPedido.aspx?var='+ intVar + '&Pagina=1' ; 
				frmArchivo.submit();
			}
		</script>
		</SCRIPT>
	</HEAD>

	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="frmArchivo" method="post" runat="server">
			<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
							border="0" cellSpacing="0" cellPadding="0" width="100%">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table3" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="5"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="670" align="center">
													<TR>
														<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r1_c2.gif"></TD>
														<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD background="Images/table_r2_c1.gif" width="6"></TD>
														<TD>
															<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0" width="100%">
																<TR>
																	<TD class="td">Nro. pedido:</TD>
																	<TD><asp:label id="lblNroPedido" runat="server" CssClass="Text"></asp:label></TD>
																	<TD class="td">Fecha Emisi�n:</TD>
																	<TD><asp:label id="lblFechaEmision" runat="server" CssClass="Text"></asp:label></TD>
																</TR>
																<TR>
																	<TD class="td">Destino:</TD>
																	<TD><asp:label id="lblDestino" runat="server" CssClass="Text"></asp:label></TD>
																	<TD class="td">Retirado por:</TD>
																	<TD><asp:label id="lblRetirado" runat="server" CssClass="Text"></asp:label></TD>
																</TR>
															</TABLE>
														</TD>
														<TD background="Images/table_r2_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r3_c2.gif"></TD>
														<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="right">
												<DIV style="DISPLAY: none"><INPUT id="txtNuevaContrasena" name="txtNuevaContrasena" runat="server"><INPUT id="txtContrasena" name="txtContrasena" runat="server"></DIV>
												<asp:hyperlink id="hlkNuevo" runat="server" CssClass="Text" NavigateUrl="DocumentoAprobacionRetiro.aspx"
													Target="_parent">Regresar Lista Pedidos</asp:hyperlink></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
													<TR>
														<TD width="80"><asp:literal id="ltrFirmar1" runat="server"></asp:literal></TD>
														<TD width="80"><asp:literal id="ltrFirmar2" runat="server"></asp:literal></TD>
														<TD width="80"><asp:button id="btnExportar" runat="server" Text="Exportar" CssClass="Text"></asp:button></TD>
													</TR>
												</TABLE>
												<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:datagrid style="Z-INDEX: 0" id="dgResultado" runat="server" CssClass="gv" AutoGenerateColumns="False"
													HorizontalAlign="Center" BorderColor="Gainsboro" DESIGNTIMEDRAGDROP="308" Width="100%">
													<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
													<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="NRO_PEDI" HeaderText="Nro Pedido"></asp:BoundColumn>
														<asp:BoundColumn DataField="COD_PROD" HeaderText="Cod. Producto"></asp:BoundColumn>
														<asp:BoundColumn DataField="DSC_MERC" HeaderText="Desc. Producto"></asp:BoundColumn>
														<asp:BoundColumn DataField="LOTE" HeaderText="Lote"></asp:BoundColumn>
														<asp:BoundColumn DataField="CNT_SOLI" HeaderText="Cant. Solicitada" DataFormatString="{0:N2}"></asp:BoundColumn>
														<asp:BoundColumn DataField="UNI_MEDI" HeaderText="Unidad Medida"></asp:BoundColumn>
														<asp:BoundColumn DataField="CNT_PREP" HeaderText="Cant. preparada" DataFormatString="{0:N2}"></asp:BoundColumn>
														<asp:BoundColumn DataField="ESTATUS" HeaderText="Estatus"></asp:BoundColumn>
														<asp:BoundColumn DataField="DSC_RETE" HeaderText="Estado"></asp:BoundColumn>
													</Columns>
													<PagerStyle CssClass="gvPager"></PagerStyle>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="630">
													<TR>
														<TD class="td" colSpan="8">Leyenda</TD>
													</TR>
													<TR>
														<TD bgColor="#f5dc8c" width="8%"></TD>
														<TD class="Leyenda" width="25%">Pedidos&nbsp;pendientes.</TD>
														<TD bgColor="#32cd32" width="8%"></TD>
														<TD class="Leyenda" width="25%">Pedidos en Proceso.</TD>
														<TD bgColor="#add8e6" width="8%"></TD>
														<TD class="Leyenda" width="25%">Pedidos Terminados.</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			<asp:panel id="PANEL1" runat="server" Visible="False">Panel 
<asp:table style="Z-INDEX: 0; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #003466; FONT-SIZE: 11px; FONT-WEIGHT: normal"
					id="Table5" runat="server" Width="968px" BorderColor="Transparent" BorderWidth="0px">
					<asp:TableRow ID="ROW0">
						<asp:TableCell ID="R0C0"></asp:TableCell>
						<asp:TableCell ID="R0C1"></asp:TableCell>
						<asp:TableCell ID="R0C2"></asp:TableCell>
						<asp:TableCell ID="R0C3"></asp:TableCell>
						<asp:TableCell ID="R0C4"></asp:TableCell>
						<asp:TableCell ID="R0C5"></asp:TableCell>
						<asp:TableCell ID="R0C6"></asp:TableCell>
						<asp:TableCell ID="R0C7"></asp:TableCell>
						<asp:TableCell ID="R0C8"></asp:TableCell>
						<asp:TableCell ID="R0C9"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="ROW1">
						<asp:TableCell ID="R1C0"></asp:TableCell>
						<asp:TableCell ID="R1C1" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R1C2" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell Text="PEDIDO Nro." ID="R1C3" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid; font-weight: bold; text-align:right"></asp:TableCell>
						<asp:TableCell ID="R1C4" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid; text-align:left"></asp:TableCell>
						<asp:TableCell ID="R1C5" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R1C6" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R1C7" style="BORDER-BOTTOM: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R1C8" style="BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R1C9"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="ROW2">
						<asp:TableCell ID="R2C0"></asp:TableCell>
						<asp:TableCell Text="Cliente :" ID="R2C1" style="BORDER-LEFT: black 1px solid; font-weight: bold"></asp:TableCell>
						<asp:TableCell ID="R2C2"></asp:TableCell>
						<asp:TableCell ID="R2C3"></asp:TableCell>
						<asp:TableCell ID="R2C4"></asp:TableCell>
						<asp:TableCell Text="Enviado Por:" ID="R2C5" style="font-weight: bold"></asp:TableCell>
						<asp:TableCell ID="R2C6"></asp:TableCell>
						<asp:TableCell ID="R2C7"></asp:TableCell>
						<asp:TableCell ID="R2C8" style="BORDER-RIGHT: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R2C9"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="ROW3">
						<asp:TableCell ID="R3C0"></asp:TableCell>
						<asp:TableCell Text="Destino :" ID="R3C1" style="BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; font-weight: bold"></asp:TableCell>
						<asp:TableCell ID="R3C2" style="BORDER-BOTTOM: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R3C3" style="BORDER-BOTTOM: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R3C4" style="BORDER-BOTTOM: black 1px solid"></asp:TableCell>
						<asp:TableCell Text="Transportista :" ID="R3C5" style="BORDER-BOTTOM: black 1px solid; font-weight: bold"></asp:TableCell>
						<asp:TableCell ID="R3C6" style="BORDER-BOTTOM: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R3C7" style="BORDER-BOTTOM: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R3C8" style="BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid"></asp:TableCell>
						<asp:TableCell ID="R3C9"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="ROW4">
						<asp:TableCell ID="R4C0"></asp:TableCell>
						<asp:TableCell ID="R4C1"></asp:TableCell>
						<asp:TableCell ID="R4C2"></asp:TableCell>
						<asp:TableCell ID="R4C3"></asp:TableCell>
						<asp:TableCell ID="R4C4"></asp:TableCell>
						<asp:TableCell ID="R4C5"></asp:TableCell>
						<asp:TableCell ID="R4C6"></asp:TableCell>
						<asp:TableCell ID="R4C7"></asp:TableCell>
						<asp:TableCell ID="R4C8"></asp:TableCell>
						<asp:TableCell ID="R4C9"></asp:TableCell>
					</asp:TableRow>
					<asp:TableRow ID="ROW5">
						<asp:TableCell ID="R6C0"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Cod. Producto" ID="R5C1"
							style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Desc. Producto"
							ID="R5C2" style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Lote" ID="R5C3"
							style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Cant. Solicitada"
							ID="R5C4" style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Unidad Medida" ID="R5C5"
							style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Cant. preparada"
							ID="R5C6" style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Estatus" ID="R5C7"
							style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell BorderStyle="Inset" BorderWidth="1px" BorderColor="Black" Text="Estado" ID="R5C8"
							style="font-weight: bold; text-align:center"></asp:TableCell>
						<asp:TableCell ID="R5C9"></asp:TableCell>
					</asp:TableRow>
				</asp:table></asp:panel>
		</form>
	</body>
</HTML>
