<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EntidadDetalle.aspx.vb" Inherits="EntidadDetalle" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle Entidad</title>
		<meta content="False" name="vs_snapToGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="francisco_uni@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
			function solonumeros()
			{
			var tecla = window.event.keyCode;
				if (tecla < 43 || tecla > 57)
				{
				window.event.keyCode=0;
				}
				
			}
		function Formato(Campo, teclapres){
	var tecla = teclapres.keyCode;
	var vr = new String(Campo.value);  
	vr = vr.replace(".", ""); 
	vr = vr.replace(".", ""); 
	vr = vr.replace(",", "");
	vr = vr.replace(",", "");
	vr = vr.replace(",", "");
	vr = vr.replace(",", "");
	vr = vr.replace(",", "");
	vr = vr.replace("/", "");
	vr = vr.replace("-", "");
	vr = vr.replace(" ", "");
	var tam = 0;
	tam = vr.length; 
	if (tam > 2 && tam < 6){
		Campo.value = vr.substr(0,tam-2) + '.' + vr.substr(tam-2,2);
	}
	if (tam >= 6 && tam < 9){
		Campo.value = vr.substr(0,tam-5) + ',' + vr.substr(tam-5,3) + '.' + vr.substr(tam-2,2);
	}
	if (tam >= 9 && tam < 12){
		Campo.value = vr.substr(0,tam-8) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3) + '.' + vr.substr(tam-2,2);
	}
	if (tam >= 12 && tam < 15){
		Campo.value = vr.substr(0,tam-11) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2); 
	}
	if (tam >= 15 && tam < 18){
		Campo.value = vr.substr(0,tam-14) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2);
	}
	if (tam >= 18 && tam < 21){
		Campo.value = vr.substr(0,tam-17) + ',' + vr.substr(tam-17,3) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2);
	}
	if (tam >= 21){
		Campo.value = vr.substr(0,tam-20) + ',' + vr.substr(tam-20,3) + ',' + vr.substr(tam-17,3) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3)+ '.' + vr.substr(tam-5,2);
	}					 
}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table10" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD class="Titulo1" width="100%">Detalle de Entidad</TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="11">
									<uc1:menuinfo id="MenuInfo2" runat="server"></uc1:menuinfo></TD>
								<TD class="Subtitulo" vAlign="top" width="100%">Datos Generales</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" style="HEIGHT: 20px" width="18%">Tipo de Entidad:</TD>
														<TD style="HEIGHT: 20px" width="32%">
															<asp:dropdownlist id="cboTipoEntidad" runat="server" Width="150px" CssClass="Text"></asp:dropdownlist></TD>
														<TD class="Text" style="HEIGHT: 20px" width="15%">N�mero RUC:</TD>
														<TD style="HEIGHT: 20px" width="35%">
															<asp:textbox id="txtCodigoSICO" runat="server" Width="120px" CssClass="Text"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text">Nombre Entidad:</TD>
														<TD colSpan="3">
															<asp:textbox id="txtNombreEntidad" runat="server" Width="430px" CssClass="Text"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:panel id="pnlEntAsociadas" runat="server">
										<TABLE id="Table26" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
											<TR>
												<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
												<TD height="6" background="Images/table_r1_c2.gif"></TD>
												<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
											</TR>
											<TR>
												<TD background="Images/table_r2_c1.gif" width="6"></TD>
												<TD>
													<TABLE id="Table5" border="0" cellSpacing="4" cellPadding="0" width="100%">
														<TR>
															<TD class="Subtitulo">Entidades&nbsp;Depositantes:</TD>
															<TD></TD>
															<TD class="Subtitulo">Entidades&nbsp;Asociadas:</TD>
														</TR>
														<TR>
															<TD>
																<asp:DropDownList id="cboDepositantes" runat="server" CssClass="Text" Width="280px" Enabled="False"></asp:DropDownList></TD>
															<TD>
																<asp:ImageButton id="btnAgregar" runat="server" ImageUrl="Images/rt.gif"></asp:ImageButton></TD>
															<TD rowSpan="6" width="51%">
																<asp:ListBox id="lbxAsociadas" runat="server" CssClass="Text" Width="300px" Enabled="False" Height="100px"></asp:ListBox></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD>
																<asp:ImageButton id="btnEliminar" runat="server" ImageUrl="Images/delete.gif"></asp:ImageButton></TD>
														</TR>
														<TR>
															<TD>
																<asp:Label id="lblMensaje" runat="server" CssClass="Error"></asp:Label></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD width="47%"></TD>
															<TD width="2%"></TD>
														</TR>
													</TABLE>
												</TD>
												<TD background="Images/table_r2_c3.gif" width="6"></TD>
											</TR>
											<TR>
												<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
												<TD height="6" background="Images/table_r3_c2.gif"></TD>
												<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">Datos de configuraci�n</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">
									<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="630" border="0" align="center">
										<TR>
											<TD class="Text" width="25%">Seleccione Tipo Documento:</TD>
											<TD width="75%">
												<asp:dropdownlist id="cboTipoDocumento" runat="server" CssClass="Text" AutoPostBack="True"></asp:dropdownlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">Cantidad de Firmas por 
									Aprobaciones:</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">
									<asp:datagrid id="dgdResultado" runat="server" Width="100%" CssClass="gv" BorderColor="Gainsboro"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NRO_SECU" HeaderText="Nro Secuencial">
												<HeaderStyle Width="90px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DSC_SECU" HeaderText="Descripci&#243;n de la Secuencia">
												<HeaderStyle Width="500px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Nro Eventos">
												<HeaderStyle Width="70px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="70" border="0">
														<TR>
															<TD align="center"><INPUT class=Text onkeypress=validarnumerico() id=txtNroFirmas style="WIDTH: 60px; POSITION: static" value='<%# DataBinder.Eval(Container, "DataItem.CNT_EVEN") %>' name=txtNroFirmas runat="server"></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="NRO_RUTA" HeaderText="Nro Ruta"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC" HeaderText="Tipo Documento"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">Datos del Endoso:</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">
									<TABLE id="Table9" cellSpacing="4" cellPadding="0" width="630" border="0" align="center">
										<TR>
											<TD class="Text" width="30%">
												Tasa</TD>
											<TD width="1%">:</TD>
											<TD width="70%">
												<asp:TextBox id="txtTasa" runat="server" Width="400px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="Text" width="30%">
												Tiene clave firma?</TD>
											<TD>:</TD>
											<TD width="70%">
												<asp:checkbox id="chkFirma" runat="server" CssClass="Text"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" width="30%">Sin&nbsp;Protesto</TD>
											<TD>:</TD>
											<TD width="70%">
												<asp:checkbox id="chkProtesto" runat="server" CssClass="Text"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" width="30%">
												N�mero de D�as para calcular la fecha de vencimiento</TD>
											<TD>:</TD>
											<TD width="70%">
												<asp:textbox id="txtNroDias" runat="server" Width="120px" CssClass="Text"></asp:textbox>
												<asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="error" ControlToValidate="txtNroDias"
													Display="Dynamic" ValidationExpression="\d*" ErrorMessage="RegularExpressionValidator" ForeColor=" ">Ingrese solo n�meros</asp:regularexpressionvalidator></TD>
										</TR>
										<TR>
											<TD class="Text" width="30%" vAlign="top">Para garantizar el cumplimiento de 
												obligaci�n(es) derivada(s) de</TD>
											<TD vAlign="top">:</TD>
											<TD width="70%">
												<asp:textbox id="txtOperaci�n" runat="server" Width="400px" CssClass="Text" Height="70px" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" width="30%">Liberaci�n M�ltiple para Warrant en 
												custodia</TD>
											<TD>:</TD>
											<TD width="70%">
												<asp:checkbox id="chkLibMultCustodia" runat="server" CssClass="Text"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" width="30%">Tiene Token para firma digital</TD>
											<TD>:</TD>
											<TD width="70%">
												<asp:checkbox id="chkFirmaDigital" runat="server" CssClass="Text"></asp:checkbox></TD>
										</TR>
										<TR>
											<td class="Text" vAlign="top" width="30%">Cliente Aprueba Liberaci�n</td>
											<td>:</td>
											<td width="70%"><asp:checkbox id="chkAprCli" CssClass="Text" Runat="server"></asp:checkbox></td>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" width="30%">Rango de&nbsp; IP</TD>
											<TD></TD>
											<TD class="Text" width="70%"><asp:textbox id="txtIpIni1" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpIni2" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpIni3" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpIni4" runat="server" Width="32px" MaxLength="3"></asp:textbox>&nbsp; 
												AL&nbsp;&nbsp;
												<asp:textbox id="txtIpFin1" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpFin2" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpFin3" runat="server" Width="32px" MaxLength="3"></asp:textbox>.
												<asp:textbox id="txtIpFin4" runat="server" Width="32px" MaxLength="3"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top" align="center">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0" align="center">
										<TR>
											<TD>
												<asp:button id="btnGrabar" runat="server" Width="80px" CssClass="btn" Text="Grabar"></asp:button></TD>
											<TD>
												<asp:button id="btnRegresar" runat="server" Width="80px" CssClass="btn" Text="Regresar" CausesValidation="False"></asp:button></TD>
											<TD></TD>
											<TD></TD>
											<TD width="550"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="td" vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
