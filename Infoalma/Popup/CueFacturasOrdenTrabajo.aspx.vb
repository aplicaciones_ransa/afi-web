Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class CueFacturasOrdenTrabajo
    Inherits System.Web.UI.Page
    Dim objFactura As clsCNFactura
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroOrden As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCodOperacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtOperacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSecuencial As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodBase As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtBase As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodComplem As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtComplem As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodServicio As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtServicio As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodDetallado As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDetallado As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaInicial As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtHoraInicial As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaFin As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtHoraFin As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtRecursos As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTipoDoc As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroDoc As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "DETALLE_FACTURA") Then
            Session.LCID = 1033
            Try
                If Page.IsPostBack = False Then
                    Me.lblNroOrden.Text = Request.QueryString("sNU_DOC")
                    LLenaOrden()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = "No se pudo cargar la factura"
            End Try
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Sub LLenaOrden()
        objFactura = New clsCNFactura
        Dim dr As DataRow
        dr = objFactura.fn_GetOrdenTrabajo(Session("CoEmpresa"), Request.QueryString("sCO_UNID"), _
             "DOT", Request.QueryString("sNU_DOC"), Session("IdSico")).Rows(0)

        Me.txtBase.Text = dr("DE_TIPO_LINE")
        Me.txtCodBase.Text = dr("TI_LINE")
        Me.txtCodComplem.Text = dr("TI_SLIN")
        Me.txtCodDetallado.Text = dr("CO_SSER")
        Me.txtCodOperacion.Text = dr("CO_OPER")
        Me.txtCodServicio.Text = dr("CO_SERV")
        Me.txtComplem.Text = dr("DE_TIPO_SLIN")
        Me.txtDetallado.Text = dr("DE_SSER")
        Me.txtFechaFin.Text = dr("FE_FINA_SERV")
        Me.txtFechaInicial.Text = dr("FE_INIC_SERV")
        Me.txtHoraFin.Text = CStr(dr("NU_HORA_FINA")).Substring(0, 2) & ":" & CStr(dr("NU_HORA_FINA")).Substring(2, 2)
        Me.txtHoraInicial.Text = CStr(dr("NU_HORA_INIC")).Substring(0, 2) & ":" & CStr(dr("NU_HORA_INIC")).Substring(2, 2)
        Me.txtNroDoc.Text = dr("NU_DOCU_RECE")
        Me.txtOperacion.Text = dr("DE_OPER")
        Me.txtRecursos.Text = dr("NU_CANT_RECU")
        Me.txtSecuencial.Text = dr("NU_SECU")
        Me.txtServicio.Text = dr("DE_SERV")
        Me.txtTipoDoc.Text = dr("TI_DOCU_RECE")
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objFactura Is Nothing) Then
            objFactura = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
