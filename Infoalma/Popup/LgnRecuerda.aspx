<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LgnRecuerda.aspx.vb" Inherits="LgnRecuerda"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Recuperar Contrase�a</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" topMargin="10" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="4" cellPadding="0" width="280" border="0">
				<TR>
					<TD class="td" vAlign="middle" colSpan="2">Olvid� su Clave del AFI?</TD>
				</TR>
				<TR>
					<TD class="Text" align="center" colSpan="2">Ingrese su usuario y el correo 
						electr�nico con el que esta registrado en el Sistema AFI y&nbsp;un 
						personal de Alma Per� se comunicar� con usted.</TD>
				</TR>
				<TR>
					<TD class="Text" width="20%">Usuario:</TD>
					<TD width="80%">
						<asp:TextBox id="txtUsuario" runat="server" Width="180px" CssClass="Text"></asp:TextBox>
						<asp:RequiredFieldValidator style="Z-INDEX: 0" id="RequiredFieldValidator1" runat="server" ErrorMessage="*"
							Display="Dynamic" ControlToValidate="txtUsuario"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="Text" width="20%">E-mail :</TD>
					<TD width="80%">
						<asp:TextBox id="txtEmail" runat="server" Width="180px" CssClass="Text"></asp:TextBox>
						<asp:RequiredFieldValidator style="Z-INDEX: 0" id="RequiredFieldValidator2" runat="server" ErrorMessage="*"
							Display="Dynamic" ControlToValidate="txtEmail"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="Text" width="20%" height="10"></TD>
					<TD width="80%" height="10"></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2">
						<asp:Button id="btnEnviar" runat="server" Text="Enviar" Width="80px" CssClass="Text"></asp:Button>&nbsp;
						<INPUT class="Text" style="WIDTH: 80px" onclick="Javascript:window.close();" type="button"
							value="Cerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
