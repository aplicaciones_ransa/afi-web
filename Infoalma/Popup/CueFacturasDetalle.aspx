<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueFacturasDetalle.aspx.vb" Inherits="CueFacturasDetalle"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle de Factura</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
			function Cerrar()
				{			
				window.close();
		}	
		</script>
	</HEAD>
	<body leftMargin="0" topMargin="20" rightMargin="0" bottomMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center">
						<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="760" border="0">
							<TR>
								<TD width="6" background="../Images/table_r1_c1.gif" height="6"></TD>
								<TD background="../Images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r2_c1.gif"></TD>
								<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
									<TABLE class="Text" id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD vAlign="top" width="12%"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top" width="38%"></TD>
											<TD vAlign="top" width="12%"></TD>
											<TD vAlign="top"></TD>
											<TD class="Text2" vAlign="top" align="right" width="38%"><asp:label id="lblNroFactura" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top">RUC</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblRUC" runat="server"></asp:label></TD>
											<TD class="Text2" vAlign="top">MONEDA</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblMoneda" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top">NOMBRE</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblNombre" runat="server"></asp:label></TD>
											<TD class="Text2" vAlign="top">PROMOTOR</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top">DIRECCION</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblDireccion" runat="server"></asp:label></TD>
											<TD class="Text2" vAlign="top">COBRADOR</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblCobrador" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top">COBRAR EN</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblCobrar" runat="server"></asp:label></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top">REFERENCIA</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top"><asp:label id="lblReferencia" runat="server"></asp:label></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"><asp:label id="lblFecha" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="6">
												<P>
													<TABLE class="Text" id="Table3" style="BORDER-TOP: #3366cc 1px solid" cellSpacing="4" cellPadding="0"
														width="100%" border="0">
														<TR>
															<TD class="Text2">CONSTANCIA</TD>
															<TD class="Text2">WARRANT/PED.DEPOS.</TD>
															<TD class="Text2" colSpan="2">PEDIDO COBRANZA</TD>
															<TD></TD>
															<TD class="Text2">VALOR</TD>
															<TD></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD class="Text2">RECEPCION</TD>
															<TD class="Text2">NRO.MANIFIESTO</TD>
															<TD class="Text2">DESDE</TD>
															<TD class="Text2">HASTA</TD>
															<TD class="Text2">DESCRIPCION DE LA MERCADERIA</TD>
															<TD class="Text2">CANTIDAD</TD>
															<TD class="Text2">TARIFA</TD>
															<TD class="Text2">IMPORTE</TD>
														</TR>
														<TR>
															<TD style="BORDER-TOP: #3366cc 1px solid" colSpan="8"><asp:literal id="ltrDetalle" runat="server"></asp:literal></TD>
														</TR>
														<TR>
															<TD colSpan="8"></TD>
														</TR>
													</TABLE>
												</P>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="56">
												<TABLE class="Text" id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text2" align="center" width="65%"><asp:label id="lblMonto" runat="server"></asp:label></TD>
														<TD class="Text2">VALOR VENTA</TD>
														<TD class="Text2">I.G.V (<asp:label id="lblPorcentaje" runat="server"></asp:label></TD>
														<TD class="Text2">PRECIO VENTA</TD>
													</TR>
													<TR>
														<TD class="Text2" align="center">S.E.�.O</TD>
														<TD><asp:label id="lblVenta" runat="server"></asp:label></TD>
														<TD><asp:label id="lblIGV" runat="server"></asp:label></TD>
														<TD><asp:label id="lblTotal" runat="server"></asp:label></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center" colSpan="6" height="30"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD align="center" colSpan="6"><INPUT class="btn" id="btnImprimir" type="button" value="Imprimir" onclick="window.print();"><INPUT class="btn" id="btnCerrar" onclick="Javascript:Cerrar();" type="button" value="Cerrar"
													name="btnCerrar"></TD>
										</TR>
									</TABLE> <!--</div>--></TD>
								<TD width="6" background="../Images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r3_c1.gif" height="6"></TD>
								<TD background="../Images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
