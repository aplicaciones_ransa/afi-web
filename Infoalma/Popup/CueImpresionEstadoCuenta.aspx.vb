Imports System.Data

Public Class CueImpresionEstadoCuenta
    Inherits System.Web.UI.Page
    ' Protected oCueEstadoCuenta As CueEstadoCuenta
    Protected dvTCDOCU_CLIE As DataView
    Dim dsTCDOCU_CLIE As New DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgTYENE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_YENE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgYENE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbYENE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTSOLE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_SOLE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgSOLE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbSOLE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTLIBR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_LIBR As System.Web.UI.WebControls.Label
    'Protected WithEvents dgLIBR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbLIBR As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTEURO As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_EURO As System.Web.UI.WebControls.Label
    'Protected WithEvents dgEURO As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbEURO As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTCANA As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_CANA As System.Web.UI.WebControls.Label
    'Protected WithEvents dgCANA As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbCANA As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTAMER As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbTO_AMER As System.Web.UI.WebControls.Label
    'Protected WithEvents dgAMER As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbAMER As System.Web.UI.WebControls.Label
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_MONE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_LARG_ENFI As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "ESTADO_CUENTA") Then
            Session.LCID = 10250
            Me.lbAMER.Visible = False
            Me.lbCANA.Visible = False
            Me.lbEURO.Visible = False
            Me.lbLIBR.Visible = False
            Me.lbSOLE.Visible = False
            Me.lbYENE.Visible = False
            Me.lbTO_AMER.Visible = False
            Me.lbTO_CANA.Visible = False
            Me.lbTO_EURO.Visible = False
            Me.lbTO_LIBR.Visible = False
            Me.lbTO_SOLE.Visible = False
            Me.lbTO_YENE.Visible = False
            Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
            Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
            Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

            If Not Page.IsPostBack Then
                pr_MUES_DATO()
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_MUES_DATO()
        Me.lbDE_MONE.Text = Request.QueryString("sMONE")
        Me.lbDE_LARG_ENFI.Text = Session("sNO_ENTI_TRAB")
        '***Datagrid solicitudes de aprobacion pendientes***'
        Try
            pr_ESTA_CUEN()
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub

    Private Sub pr_ESTA_CUEN()
        Try
            '''''Creando tabla de totales''
            Dim myDataColumn As DataColumn
            Dim tbTO_CUEN As New DataTable
            myDataColumn = New DataColumn
            myDataColumn.DataType = Type.GetType("System.Double")
            myDataColumn.ColumnName = "DEBE"
            tbTO_CUEN.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = Type.GetType("System.Double")
            myDataColumn.ColumnName = "HABER"
            tbTO_CUEN.Columns.Add(myDataColumn)

            myDataColumn = New DataColumn
            myDataColumn.DataType = Type.GetType("System.Double")
            myDataColumn.ColumnName = "SALDO"
            tbTO_CUEN.Columns.Add(myDataColumn)
            '''''''''''''''''''''''''''''''
            Dim dTO_DEBE As Double
            Dim dTO_HABE As Double
            Dim dTO_SALD As Double
            Dim dvTCDETA As DataView
            Dim i As Integer
            Dim item As DataGridItem
            Dim drTO_CUEN As DataRow
            If Not Session("dvRE_GENE_AMER") Is Nothing Then
                Me.lbAMER.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_AMER"), DataView)
                Me.dgAMER.DataSource = dvTCDETA
                Me.dgAMER.DataBind()
                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next
                For Each item In Me.dgAMER.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_AMER.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = FormatNumber(dTO_DEBE - dTO_HABE, 2)
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTAMER.DataSource = tbTO_CUEN
                dgTAMER.DataBind()
            End If
            dTO_DEBE = 0
            dTO_HABE = 0
            dTO_SALD = 0
            tbTO_CUEN.Clear()
            If Not Session("dvRE_GENE_CANA") Is Nothing Then
                Me.lbCANA.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_CANA"), DataView)
                Me.dgCANA.DataSource = dvTCDETA
                Me.dgCANA.DataBind()
                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next
                For Each item In Me.dgCANA.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_CANA.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = dTO_DEBE - dTO_HABE
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTCANA.DataSource = tbTO_CUEN
                dgTCANA.DataBind()
            End If
            dTO_DEBE = 0
            dTO_HABE = 0
            dTO_SALD = 0
            tbTO_CUEN.Clear()
            If Not Session("dvRE_GENE_EURO") Is Nothing Then
                Me.lbEURO.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_EURO"), DataView)
                Me.dgEURO.DataSource = dvTCDETA
                Me.dgEURO.DataBind()
                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next
                For Each item In Me.dgEURO.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_EURO.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = dTO_DEBE - dTO_HABE
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTEURO.DataSource = tbTO_CUEN
                dgTEURO.DataBind()
            End If
            dTO_DEBE = 0
            dTO_HABE = 0
            dTO_SALD = 0
            tbTO_CUEN.Clear()
            If Not Session("dvRE_GENE_LIBR") Is Nothing Then
                Me.lbLIBR.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_LIBR"), DataView)
                Me.dgLIBR.DataSource = dvTCDETA
                Me.dgLIBR.DataBind()

                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next

                For Each item In Me.dgLIBR.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_LIBR.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = dTO_DEBE - dTO_HABE
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTLIBR.DataSource = tbTO_CUEN
                dgTLIBR.DataBind()
            End If
            dTO_DEBE = 0
            dTO_HABE = 0
            dTO_SALD = 0
            tbTO_CUEN.Clear()
            If Not Session("dvRE_GENE_SOLE") Is Nothing Then
                Me.lbSOLE.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_SOLE"), DataView)
                Me.dgSOLE.DataSource = dvTCDETA
                Me.dgSOLE.DataBind()

                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next
                For Each item In Me.dgSOLE.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_SOLE.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = dTO_DEBE - dTO_HABE
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTSOLE.DataSource = tbTO_CUEN
                dgTSOLE.DataBind()
            End If
            dTO_DEBE = 0
            dTO_HABE = 0
            dTO_SALD = 0
            tbTO_CUEN.Clear()
            If Not Session("dvRE_GENE_YENE") Is Nothing Then
                Me.lbYENE.Visible = True
                dvTCDETA = CType(Session("dvRE_GENE_YENE"), DataView)
                Me.dgYENE.DataSource = dvTCDETA
                Me.dgYENE.DataBind()
                For i = 0 To dvTCDETA.Count - 1
                    dTO_DEBE = dTO_DEBE + CDbl(dvTCDETA.Item(i).Item(11))
                    dTO_HABE = dTO_HABE + CDbl(dvTCDETA.Item(i).Item(12))
                Next
                For Each item In Me.dgYENE.Items
                    item.Cells(7).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                Next
                '''Totales'''
                Me.lbTO_YENE.Visible = True
                drTO_CUEN = tbTO_CUEN.NewRow()
                drTO_CUEN("DEBE") = dTO_DEBE
                drTO_CUEN("HABER") = dTO_HABE
                drTO_CUEN("SALDO") = dTO_DEBE - dTO_HABE
                tbTO_CUEN.Rows.Add(drTO_CUEN)
                dgTYENE.DataSource = tbTO_CUEN
                dgTYENE.DataBind()
            End If
        Catch ex As Exception
            Dim str As String = ex.Message
        End Try
    End Sub
End Class
