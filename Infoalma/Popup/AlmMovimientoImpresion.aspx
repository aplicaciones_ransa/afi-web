<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmMovimientoImpresion.aspx.vb" Inherits="AlmMovimientoImpresion"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlmMovimientoImpresion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="texto7pt" vAlign="top">
					<TD align="center" colSpan="2">
						<asp:label id="lblTitulo" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD>
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD>
						<asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD>
						<asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD align="center" colSpan="2">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0">
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px"></TD>
								<TD style="WIDTH: 7px"></TD>
								<TD colSpan="4"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 97px">
									<asp:label id="Label11" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">TIP. MOVIMIENTO</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label12" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbTI_MOVI" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px">
									<asp:label id="Label4" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">REFERENCIA</asp:label></TD>
								<TD style="WIDTH: 6px">
									<asp:label id="Label5" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD>
									<asp:label id="lbNO_REFE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label>&nbsp;-
									<asp:label id="lbNU_REFE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label8" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">TIPO REPORTE</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label13" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbTI_REPO" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px">
									<asp:label id="Label9" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">MODALIDAD</asp:label></TD>
								<TD style="WIDTH: 6px">
									<asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD>
									<asp:label id="lbNO_MODA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label6" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt"> ESTADO</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label2" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbEST_DCR" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px">
									<asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt"> FEC. DOCUMENTO</asp:label></TD>
								<TD style="WIDTH: 6px">
									<asp:label id="Label7" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD>
									<asp:label id="lbFE_EMIS_INIC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label>-
									<asp:label id="lbFE_EMIS_FINA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="lbMS_REGI" runat="server" ForeColor="Red" CssClass="text"></asp:label>
					</TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2">
						<asp:datagrid id="dgIngresos" runat="server" CssClass="gv" Visible="False" BorderColor="Gainsboro"
							HorizontalAlign="Center" PageSize="50" Width="90%" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_REGI_INGR" HeaderText="Fec.Ingreso" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_ACTU" HeaderText="Fec.Cierre" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_SECU" HeaderText="Sec">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MERC_GENE" HeaderText="Sub.TipoMercader&#237;a"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc.Mercader&#237;a"></asp:BoundColumn>
								<asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unid">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_RECI" HeaderText="Cnt.Unidad" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_RECI" HeaderText="Cnt.Bulto" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="IM_TOTA_NACI" HeaderText="Importe" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="CodProd">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_PROD_CLIE" HeaderText="Producto"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="Guia.Ingr">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_TARI" HeaderText="Tarifa"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CO_UNID" HeaderText="CO_UNID"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:datagrid id="dgRetiros" runat="server" CssClass="gv" Visible="False" BorderColor="Gainsboro"
							HorizontalAlign="Center" PageSize="50" Width="90%" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="NU_DOCU_RETI" HeaderText="DOR">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_DOCU_RETI" HeaderText="Fec.Retiro" DataFormatString="{0:d}">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_SECU" HeaderText="Item">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Prod">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc.Mercaderia"></asp:BoundColumn>
								<asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unid">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Cnt.Unidad" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Cnt.Bulto" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="IM_TOTA_NACI" HeaderText="Importe" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
								<asp:BoundColumn HeaderText="Guia.Salida">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Observaciones"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
							name="btnImprimir"> <INPUT class="btn" id="btnCerrar" style="WIDTH: 80px" onclick="window.close();" type="button"
							value="Cerrar" name="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
