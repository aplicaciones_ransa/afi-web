Imports System.Data

Public Class CueImpresionCancela
    Inherits System.Web.UI.Page
    'Protected oCueCancelaciones As CueCancelaciones
    Dim sNO_DOCU, sFE_INIC, sFE_FINA As String
    Protected dtTCCOBR As DataTable
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgTCCOBR_0001 As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTCCOBR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbFE_EMIS_FINA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_EMIS_INIC As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNO_DOCU As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNO_CLIE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "CANCELACIONES") Then
            Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
            Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
            Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)
            If Not Page.IsPostBack Then
                pr_MUES_DATO()
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_MUES_DATO()
        Me.lbNO_CLIE.Text = Session("sNO_ENTI_TRAB")
        Me.lbNO_DOCU.Text = Request.QueryString("sNO_DOCU")
        Me.lbFE_EMIS_INIC.Text = Request.QueryString("sFE_INIC")
        Me.lbFE_EMIS_FINA.Text = Request.QueryString("sFE_FINA")

        dtTCCOBR = CType(Session.Item("dtCueCanc"), DataTable)
        Me.dgTCCOBR.DataSource = dtTCCOBR
        Me.dgTCCOBR.DataBind()

        Dim dtTCCOBR_0001 As DataTable = CType(Session.Item("dtCueCanTot"), DataTable)
        Me.dgTCCOBR_0001.DataSource = dtTCCOBR_0001
        Me.dgTCCOBR_0001.DataBind()
    End Sub
End Class
