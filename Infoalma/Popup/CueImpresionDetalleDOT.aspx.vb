Imports System.Data
Imports Depsa.LibCapaNegocio
'Imports Root.Reports
'Imports System.IO.Stream
'Imports System.IO

Public Class CueImpresionDetalleDOT
    Inherits System.Web.UI.Page
    Dim sCO_EMPR, sCO_UNID, sNU_DOC, sCO_CLIE As String
    Dim objCCorrientes As clsCNCCorrientes = New clsCNCCorrientes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim dtRepo As DataTable
            'Dim drPDF As DataRow
            'Dim objStream As Stream
            'Dim dd As String = ""
            dtRepo = objCCorrientes.gCADGetDocumentoDetalleDOT(Request.QueryString("sCO_EMPR"), Request.QueryString("sCO_UNID"), Request.QueryString("sNU_DOC"), Request.QueryString("sCO_CLIE"), Request.QueryString("sCO_ALMA"))
            'dtRepo = objCCorrientes.gCADGetDocumentoDetalleDOT("01", "001", "00000705602", "20109714039", "0090")

            If dtRepo.Rows.Count <> 0 Then
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(CType(dtRepo.Rows(0).Item("NO_REPO"), Byte()))
            Else
                Response.Write("No se encontr� el archivo")
            End If
            Response.Flush()
            Response.Close()
        Catch ex As Exception
            Dim MSG As String = ex.ToString()
            Response.Write("No se encontr� el archivo")
        End Try
        'If Not Page.IsPostBack Then
        '    GenerarPDF()
        'End If
    End Sub

    'Private Sub GenerarPDF()
    '    Dim dt As New DataTable
    '    Try
    '        objCCorrientes = New clsCNCCorrientes
    '        dt = objCCorrientes.gCADGetDocumentoDetalleDOT(Request.QueryString("sCO_EMPR"), Request.QueryString("sCO_UNID"), Request.QueryString("sNU_DOC"), Request.QueryString("sCO_CLIE"))
    '        GenerarFactura(dt)
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '        Response.Write("No se encontr� el archivo PDF")
    '    End Try
    'End Sub

    'Private Sub GenerarFactura(ByVal dtTemporal As DataTable)
    '    Try
    '        Dim report As Report = New Report(New PdfFormatter)
    '        Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
    '        '===========  Logo DEPSA =============
    '        Dim fd_logo As FontProp = New FontProp(fd, 25.0, Color.White)
    '        Dim fd_logo1 As FontProp = New FontProp(fd, 22.0, Color.Black)
    '        Dim pp_logo As PenProp = New PenProp(report, 5.0)
    '        Dim bp_logo As BrushProp = New BrushProp(report, Color.Black)

    '        '===========  Logo DEPSA =============
    '        Dim fp As FontProp = New FontPropMM(fd, 2.5)
    '        Dim fp_table As FontProp = New FontPropMM(fd, 2.5)
    '        Dim fp_Negrita As FontProp = New FontPropMM(fd, 2.5)
    '        Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.5)
    '        Dim ppBold As PenProp = New PenProp(report, 1.0)
    '        Dim page As Page
    '        Dim intAltura As Integer
    '        Dim intLine As Int16 = 0
    '        Dim intPaginas As Decimal
    '        Dim intNroPag As Int16 = 0
    '        Dim intRegistro As Integer
    '        Dim sSubTitulo As String
    '        Dim sNomSeguro As String
    '        Dim sValorSeguro As String
    '        Dim sValorSeguroPlus As String
    '        Dim sComplementario As String = "A"
    '        Dim oFunciones As New Library.AccesoBL.Funciones
    '        fp_Titulo.bBold = True
    '        fp_Negrita.bBold = True

    '        If dtTemporal.Rows.Count > 0 Then
    '            page = New Page(report)
    '            page.rWidthMM = 297
    '            page.rHeightMM = 210

    '            page.SetLandscape()
    '            intAltura = 100

    '            '===========  Logo DEPSA =============
    '            page.Add(100, 56, New RepCircleMM(pp_logo, bp_logo, 4))
    '            page.Add(102, 54, New RepString(fd_logo, "d"))
    '            page.Add(42, 80, New RepString(fd_logo1, "depsa"))
    '            page.Add(43, 80, New RepString(fd_logo1, "depsa"))
    '            '===========  Logo DEPSA =============

    '            page.Add(450, 80, New RepString(fp_Titulo, "N� " & dtTemporal.Rows(0)("NU_ORDE_TRAB")))

    '            page.Add(200, intAltura, New RepString(fp_Titulo, "AUTORIZACION DE TRABAJO"))

    '            page.Add(50, intAltura + 50, New RepString(fp, "Sr. (es)"))
    '            page.Add(90, intAltura + 50, New RepString(fp_table, ": " & dtTemporal.Rows(0)("NO_CLIE_REPO")))

    '            page.Add(50, intAltura + 70, New RepString(fp, "Fecha"))
    '            page.Add(90, intAltura + 70, New RepString(fp_table, ": " & dtTemporal.Rows(0)("FECHA")))

    '            '================ SERVICIO =====================================
    '            page.Add(50, intAltura + 90, New RepString(fp_Negrita, "SERVICIO"))

    '            page.Add(50, intAltura + 125, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_MONTACARGA") = "S" Then
    '                page.Add(54, intAltura + 121, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 120, New RepString(fp, "Montacargas"))


    '            page.Add(210, intAltura + 120, New RepString(fp, "Hora(s) : "))
    '            If (dtTemporal.Rows(0)("CO_UNME_MONT")).Replace(" ", "") = "HORA" Then

    '                page.Add(250, intAltura + 120, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("HORAS"))))
    '            End If

    '            page.Add(210, intAltura + 140, New RepString(fp, "Peso     : "))
    '            If (dtTemporal.Rows(0)("CO_UNME_MONT")).Replace(" ", "") = "TM" Then
    '                page.Add(250, intAltura + 140, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("HORAS")) & " " & dtTemporal.Rows(0)("CO_UNME_MONT")))
    '            End If

    '            page.Add(50, intAltura + 165, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_SOBRETIEMPO") = "S" Then
    '                page.Add(54, intAltura + 161, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 160, New RepString(fp, "Sobre Tiempo"))

    '            page.Add(120, intAltura + 180, New RepString(fp, "Simple"))

    '            Dim strIniSimple As String = (dtTemporal.Rows(0)("INICIO_SIMPLE")).Replace(":", "")
    '            Dim strFinSimple As String = (dtTemporal.Rows(0)("FINAL_SIMPLE")).Replace(":", "")
    '            Dim strIniDoble As String = (dtTemporal.Rows(0)("INICIO_DOBLE")).Replace(":", "")
    '            Dim strFinDoble As String = (dtTemporal.Rows(0)("FINAL_DOBLE")).Replace(":", "")

    '            If strIniSimple.Length = 4 Then
    '                strIniSimple = (strIniSimple.Substring(0, 2) & ":" & strIniSimple.Substring(2, 2))
    '            End If
    '            If strFinSimple.Length = 4 Then
    '                strFinSimple = (strFinSimple.Substring(0, 2) & ":" & strFinSimple.Substring(2, 2))
    '            End If
    '            If strIniDoble.Length = 4 Then
    '                strIniDoble = (strIniDoble.Substring(0, 2) & ":" & strIniDoble.Substring(2, 2))
    '            End If
    '            If strFinDoble.Length = 4 Then
    '                strFinDoble = (strFinDoble.Substring(0, 2) & ":" & strFinDoble.Substring(2, 2))
    '            End If

    '            page.Add(210, intAltura + 180, New RepString(fp, "de" & " : " & strIniSimple & " horas"))
    '            page.Add(370, intAltura + 180, New RepString(fp, "a" & " : " & strFinSimple & " horas"))
    '            page.Add(120, intAltura + 200, New RepString(fp, "Doble"))
    '            page.Add(210, intAltura + 200, New RepString(fp, "de" & " : " & strIniDoble & " horas"))
    '            page.Add(370, intAltura + 200, New RepString(fp, "a" & " : " & strFinDoble & " horas"))


    '            page.Add(50, intAltura + 225, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_ESTI_DESE") = "S" Then
    '                page.Add(54, intAltura + 221, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 220, New RepString(fp, "Estiba / Desestiba" & ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_ESTI_DESE")) & " " & dtTemporal.Rows(0)("CO_UNME_ESTI")))


    '            page.Add(50, intAltura + 245, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_OTROS") = "S" Then
    '                page.Add(54, intAltura + 241, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 240, New RepString(fp, "Otros" & ": " & dtTemporal.Rows(0)("DE_OTROS")))

    '            '================ TRANSPORTE =====================================
    '            page.Add(50, intAltura + 260, New RepString(fp_Negrita, "TRANSPORTE"))
    '            page.Add(120, intAltura + 280, New RepString(fp_Negrita, "cantidad"))
    '            page.Add(260, intAltura + 280, New RepString(fp_Negrita, "cantidad"))

    '            page.Add(50, intAltura + 305, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_20PIES") = "S" Then
    '                page.Add(54, intAltura + 301, New RepString(fp_Titulo, "X"))
    '            End If

    '            page.Add(90, intAltura + 300, New RepString(fp, "20 pies"))
    '            page.Add(125, intAltura + 300, New RepString(fp, ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_20PIES"))))

    '            page.Add(210, intAltura + 305, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_40PIES") = "S" Then
    '                page.Add(214, intAltura + 301, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(240, intAltura + 300, New RepString(fp, "40 pies"))
    '            page.Add(275, intAltura + 300, New RepString(fp, ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_40PIES"))))

    '            page.Add(50, intAltura + 325, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_TRAILER") = "S" Then
    '                page.Add(54, intAltura + 321, New RepString(fp_Titulo, "X"))
    '            End If


    '            page.Add(90, intAltura + 320, New RepString(fp, "Trailer"))
    '            page.Add(125, intAltura + 320, New RepString(fp, ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_TRAILER"))))

    '            page.Add(210, intAltura + 325, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_OTRO_TRANSP") = "S" Then
    '                page.Add(214, intAltura + 321, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(240, intAltura + 320, New RepString(fp, "Otros"))
    '            page.Add(275, intAltura + 320, New RepString(fp, ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_OTRO_TRANSP"))))

    '            '================ CONDICI�N =====================================
    '            page.Add(50, intAltura + 340, New RepString(fp_Negrita, "CONDICI�N"))


    '            page.Add(50, intAltura + 365, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_COND_SIMPLE") = "S" Then
    '                page.Add(54, intAltura + 361, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 360, New RepString(fp, "Simple"))

    '            page.Add(210, intAltura + 365, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_COND_RECEPC") = "S" Then
    '                page.Add(214, intAltura + 361, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(240, intAltura + 360, New RepString(fp, "Recepci�n"))

    '            page.Add(370, intAltura + 360, New RepString(fp, "DCR"))
    '            page.Add(395, intAltura + 360, New RepString(fp, ": " & dtTemporal.Rows(0)("NU_DOCU_DCR")))


    '            page.Add(210, intAltura + 385, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_COND_RETIRO") = "S" Then
    '                page.Add(214, intAltura + 381, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(240, intAltura + 380, New RepString(fp, "Retiro"))

    '            page.Add(370, intAltura + 380, New RepString(fp, "DOR"))
    '            page.Add(395, intAltura + 380, New RepString(fp, ": " & dtTemporal.Rows(0)("NU_DOCU_DOR")))


    '            page.Add(50, intAltura + 405, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_COND_ADUANE") = "S" Then
    '                page.Add(54, intAltura + 401, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(90, intAltura + 400, New RepString(fp, "Aduanero"))

    '            page.Add(210, intAltura + 405, New RepRectMM(ppBold, 5, 5))
    '            If dtTemporal.Rows(0)("ST_COND_AFORO") = "S" Then
    '                page.Add(214, intAltura + 401, New RepString(fp_Titulo, "X"))
    '            End If
    '            page.Add(240, intAltura + 400, New RepString(fp, "Aforo"))

    '            page.Add(370, intAltura + 400, New RepString(fp, "PD"))
    '            page.Add(395, intAltura + 400, New RepString(fp, ": " & dtTemporal.Rows(0)("NU_DOCU_PD")))
    '            page.Add(370, intAltura + 420, New RepString(fp, "GR"))
    '            page.Add(395, intAltura + 420, New RepString(fp, ": " & dtTemporal.Rows(0)("NU_DOCU_GR")))

    '            page.Add(50, intAltura + 440, New RepString(fp, "Autoriza (Cliente)" & ": " & dtTemporal.Rows(0)("AUTORIZA")))

    '            Dim i As Integer = 0
    '            Dim cadena As String = ""
    '            cadena = dtTemporal.Rows(0)("REFE_MERCAD")
    '            i = (cadena).Length

    '            If i > 0 Then

    '                If i > 70 Then
    '                    page.Add(50, intAltura + 460, New RepString(fp, "Referencia mercader�a" & ": " & (cadena.Substring(0, 70))))
    '                    cadena = cadena.Remove(0, 70)
    '                    i = (cadena).Length
    '                    If i > 90 Then
    '                        page.Add(50, intAltura + 470, New RepString(fp, (cadena.Substring(0, 90))))
    '                        cadena = cadena.Remove(0, 90)
    '                        i = (cadena).Length
    '                        If i > 90 Then
    '                            page.Add(50, intAltura + 480, New RepString(fp, (cadena.Substring(0, 90))))
    '                        Else
    '                            page.Add(50, intAltura + 480, New RepString(fp, (cadena)))
    '                        End If
    '                    Else
    '                        page.Add(50, intAltura + 470, New RepString(fp, (cadena)))
    '                    End If
    '                Else
    '                    page.Add(50, intAltura + 460, New RepString(fp, "Referencia mercader�a" & ": " & (cadena)))
    '                End If
    '            Else
    '                page.Add(50, intAltura + 460, New RepString(fp, "Referencia mercader�a" & ":"))
    '            End If

    '            page.Add(50, intAltura + 505, New RepRectMM(ppBold, 175, 7))
    '            page.Add(60, intAltura + 500, New RepString(fp, "TOTAL A PAGAR"))

    '            page.Add(145, intAltura + 500, New RepString(fp, ": " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("CANT_PAGAR"))))

    '            page.Add(350, intAltura + 500, New RepString(fp, "HORAS  /  UNIDADES  /  TM  /  KG"))

    '            If dtTemporal.Rows(0)("ST_HORAS") = "S" Then
    '                page.Add(345, intAltura + 502, New RepRectMM(ppBold, 15, 5))
    '            End If
    '            If dtTemporal.Rows(0)("ST_VEHICULO") = "S" Then
    '                page.Add(396, intAltura + 502, New RepRectMM(ppBold, 20, 5))
    '            End If
    '            If dtTemporal.Rows(0)("ST_TM") = "S" Then
    '                page.Add(461, intAltura + 502, New RepRectMM(ppBold, 7, 5))
    '            End If
    '            If dtTemporal.Rows(0)("ST_KG") = "S" Then
    '                page.Add(487, intAltura + 502, New RepRectMM(ppBold, 7, 5))
    '            End If


    '            page.Add(420, intAltura + 600, New RepString(fp, "..............................................."))
    '            page.Add(470, intAltura + 620, New RepString(fp, "FIRMA"))

    '            page.Add(50, intAltura + 700, New RepString(fp, "DE-R-024-GO"))
    '            page.Add(400, intAltura + 700, New RepString(fp, "ADMINISTRACI�N DE ALMAC�N"))

    '        End If
    '        RT.ResponsePDF(report, Me)

    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '        Response.Write("No se encontr� el archivo PDF")
    '    End Try
    'End Sub

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
