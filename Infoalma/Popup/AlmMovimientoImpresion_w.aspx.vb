Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Library.AccesoDB
Imports System.Data

Public Class AlmMovimientoImpresion_w
    Inherits System.Web.UI.Page
    Private strdocu_aux As String
    Private objMovimiento As clsCNMovimiento
    'Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTI_MOVI As System.Web.UI.WebControls.Label
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTI_REPO As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents dgRetDet As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgRetRes As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgIngDet As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgIngRes As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_MERC As System.Web.UI.WebControls.Label
    'Protected WithEvents lbLOTE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNU_REFE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_ALMA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_INI_FIN As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_PROD As System.Web.UI.WebControls.Label
    Dim i As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

        If Not Page.IsPostBack Then
            prMUEST_DATO()
        End If
    End Sub

    Private Sub prMUEST_DATO()

        Me.lbTI_MOVI.Text = Request.QueryString("sDE_MOVI")
        Me.lbTI_REPO.Text = Request.QueryString("sDE_REPO")
        Dim strFe_Ini As String = "Del " & Request.QueryString("sFE_INIC")
        Dim strFe_Fin As String = " Al " & Request.QueryString("sFE_FINA")
        If strFe_Ini <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini
        End If
        If strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Fin
        End If
        If strFe_Ini <> "" And strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini & strFe_Fin
        End If
        Me.lbDE_ALMA.Text = Request.QueryString("sDE_ALMA")
        Me.lbCO_PROD.Text = Request.QueryString("sCO_PROD")
        Me.lbDE_MERC.Text = Request.QueryString("sDE_MERC")
        Me.lbLOTE.Text = Request.QueryString("sLOTE")
        Me.lbNU_REFE.Text = Request.QueryString("sCODIGO")

        Me.dgIngRes.CurrentPageIndex = 0
        Me.dgIngDet.CurrentPageIndex = 0
        Me.dgIngRes.CurrentPageIndex = 0
        Me.dgIngDet.CurrentPageIndex = 0


        If Request.QueryString("sCO_MOVI") = "1" And Request.QueryString("sCO_REPO") = 2 Then
            BindatagridIngreso()

        ElseIf Request.QueryString("sCO_MOVI") = "1" And Request.QueryString("sCO_REPO") = 1 Then
            BindatagridIngresoDetallado()

        ElseIf Request.QueryString("sCO_MOVI") = "2" And Request.QueryString("sCO_REPO") = 2 Then
            BindatagridRetiro()

        ElseIf Request.QueryString("sCO_MOVI") = "2" And Request.QueryString("sCO_REPO") = 1 Then
            BindatagridRetiroDetallado()

        End If
    End Sub


    Private Sub BindatagridIngreso()

        Me.dgIngRes.Visible = True
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = False

        Me.lblTitulo.Text = "REPORTE DE INGRESO DE MERCADER�A WMS"
        Dim dtIngRes As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtIngRes = .gCNListarMoviMercWMS(Request.QueryString("sCO_MOVI"), Request.QueryString("sCO_REPO"), _
                   Request.QueryString("sCODIGO"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"), Request.QueryString("sFE_INIC"), _
                    Request.QueryString("sFE_FINA"), Request.QueryString("sCO_ALMA"), Request.QueryString("sCO_PROD"), Session.Item("IdSico"))

                Me.dgIngRes.Visible = True
                Me.dgIngRes.DataSource = dtIngRes
                Me.dgIngRes.DataBind()
                Session.Item("dtMovimientoWMS") = dtIngRes

            End With
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub
    Private Sub BindatagridIngresoDetallado()

        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = True
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = False
        Me.lblTitulo.Text = "REPORTE DE INGRESO DE MERCADER�A WMS"

        Dim dtIngDet As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtIngDet = .gCNListarMoviMercWMS(Request.QueryString("sCO_MOVI"), Request.QueryString("sCO_REPO"), _
                                Request.QueryString("sCODIGO"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"), Request.QueryString("sFE_INIC"), _
                                 Request.QueryString("sFE_FINA"), Request.QueryString("sCO_ALMA"), Request.QueryString("sCO_PROD"), Session.Item("IdSico"))

                Me.dgIngDet.DataSource = dtIngDet
                Me.dgIngDet.DataBind()
                Session.Item("dtMovimientoWMS") = dtIngDet
            End With
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridRetiro()
        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = True
        Me.dgRetDet.Visible = False

        Me.lblTitulo.Text = "REPORTE DE �RDENES DE RETIRO  WMS"
        Dim dtRetRes As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtRetRes = .gCNListarMoviMercWMS(Request.QueryString("sCO_MOVI"), Request.QueryString("sCO_REPO"), _
                                Request.QueryString("sCODIGO"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"), Request.QueryString("sFE_INIC"), _
                                 Request.QueryString("sFE_FINA"), Request.QueryString("sCO_ALMA"), Request.QueryString("sCO_PROD"), Session.Item("IdSico"))

                Me.dgRetRes.Visible = True
                Me.dgRetRes.DataSource = dtRetRes
                Me.dgRetRes.DataBind()
                Session.Item("dtMovimientoWMS") = dtRetRes
            End With
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridRetiroDetallado()

        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = True

        Me.lblTitulo.Text = "REPORTE DE �RDENES DE RETIRO  WMS"
        Dim dtRetDet As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtRetDet = .gCNListarMoviMercWMS(Request.QueryString("sCO_MOVI"), Request.QueryString("sCO_REPO"), _
                                Request.QueryString("sCODIGO"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"), Request.QueryString("sFE_INIC"), _
                                 Request.QueryString("sFE_FINA"), Request.QueryString("sCO_ALMA"), Request.QueryString("sCO_PROD"), Session.Item("IdSico"))

                Me.dgRetDet.Visible = True
                Me.dgRetDet.DataSource = dtRetDet
                Me.dgRetDet.DataBind()
                Session.Item("dtMovimientoWMS") = dtRetDet
            End With
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub

    Sub Change_dgIngDet(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgIngDet.CurrentPageIndex = Args.NewPageIndex
        Me.dgIngDet.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgIngDet.DataBind()
    End Sub

    Sub Change_dgIngRes(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgIngRes.CurrentPageIndex = Args.NewPageIndex
        Me.dgIngRes.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgIngRes.DataBind()
    End Sub

    Sub Change_dgRetRes(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgRetRes.CurrentPageIndex = Args.NewPageIndex
        Me.dgRetRes.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgRetRes.DataBind()
    End Sub

    Sub Change_dgRetDet(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgRetDet.CurrentPageIndex = Args.NewPageIndex
        Me.dgRetDet.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgRetDet.DataBind()
    End Sub
    Private Sub dgIngRes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngRes.ItemDataBound
        Try
            'If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
            '    Dim sCA_CELL As String = e.Item.Cells(0).Text
            '    Dim sCA_LINK As String = "<A onclick=Javascript:AbrirDetaMoviWMS('" & Me.cboMovimiento.SelectedValue & "','" & e.Item.Cells(0).Text & "','" & Me.cboAlmacen.SelectedValue & "'); href='#'>" & sCA_CELL & "</a>"
            '    e.Item.Cells(0).Text = sCA_LINK
            'End If
        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub

    Private Sub dgIngDet_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngDet.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                If e.Item.Cells(10).Text = 1 Then
                    e.Item.Cells(0).Text = e.Item.Cells(0).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(8).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(11).Text
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                ElseIf e.Item.Cells(10).Text = 2 Then
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    'e.Item.Cells.RemoveAt(10)
                    'e.Item.Cells.RemoveAt(9)
                    'e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    'e.Item.Cells.RemoveAt(6)
                    'e.Item.Cells.RemoveAt(5)
                    'e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    'e.Item.Cells.RemoveAt(2)
                    'e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).Text = ""
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(10).Text = 3 Then
                    e.Item.Cells(0).Text = "TOTAL :&nbsp;&nbsp;&nbsp;" & e.Item.Cells(12).Text
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(10).Text = 4 Then
                    e.Item.Cells(0).Text = "&nbsp;"
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                End If

            End If
        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub

    Private Sub dgRetRes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetRes.ItemDataBound
        Try
            'If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
            '    Dim sCA_CELL As String = e.Item.Cells(0).Text
            '    Dim sCA_LINK As String = "<A onclick=Javascript:AbrirDetaMoviWMS('" & Me.cboMovimiento.SelectedValue & "','" & e.Item.Cells(0).Text & "','" & Me.cboAlmacen.SelectedValue & "'); href='#'>" & sCA_CELL & "</a>"
            '    e.Item.Cells(0).Text = sCA_LINK
            'End If
        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub


    Private Sub dgRetDet_ItemDataBound1(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetDet.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                If e.Item.Cells(11).Text = 1 Then
                    e.Item.Cells(0).Text = e.Item.Cells(0).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(10).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(12).Text
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                ElseIf e.Item.Cells(11).Text = 2 Then
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    'e.Item.Cells.RemoveAt(10)
                    'e.Item.Cells.RemoveAt(9)
                    'e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    'e.Item.Cells.RemoveAt(6)
                    'e.Item.Cells.RemoveAt(5)
                    'e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    'e.Item.Cells.RemoveAt(2)
                    'e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).Text = ""
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(11).Text = 3 Then
                    e.Item.Cells(0).Text = "TOTAL CANTIDAD SOLICITADA :&nbsp;&nbsp;&nbsp;" & (e.Item.Cells(13).Text).ToString & "&nbsp;&nbsp;&nbsp;" _
                                         & "TOTAL CANTIDAD PREPARADA :&nbsp;&nbsp;&nbsp;" & (e.Item.Cells(14).Text).ToString

                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(11).Text = 4 Then
                    e.Item.Cells(0).Text = "&nbsp;"
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                End If

            End If
        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub
End Class
