Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Root.Reports
'Imports System.IO

Public Class CueImpresionDetalleDoc
    Inherits System.Web.UI.Page
    Dim sCO_UNID, sTI_DOCU, sNU_DOCU As String
    Dim objCCorrientes As clsCNCCorrientes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            GenerarPDF()
        End If
    End Sub

    Private Sub GenerarPDF()
        Dim dt As New DataTable
        Try
            objCCorrientes = New clsCNCCorrientes
            dt = objCCorrientes.gCADGetDocumentoDetalle("01", Request.QueryString("sCO_UNID"), Request.QueryString("sTI_DOCU_COBR"), Request.QueryString("sNU_DOCU_COBR"))
            If Left(Request.QueryString("sNU_DOCU_COBR"), 1) = "F" Or Left(Request.QueryString("sNU_DOCU_COBR"), 1) = "B" Then
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(CType(dt.Rows(0)("NO_PDF_CSEL"), Byte()))
                Response.Flush()
                Response.Close()
            Else
                GenerarFactura(dt)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Private Sub GenerarFactura(ByVal dtTemporal As DataTable)
        Try
            Dim report As Report = New Report(New PdfFormatter)
            Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
            Dim fp As FontProp = New FontPropMM(fd, 2.0)
            Dim fp_table As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_Negrita As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_Titulo As FontProp = New FontPropMM(fd, 3.5)
            Dim ppBold As PenProp = New PenProp(report, 1.0)
            Dim page As Page
            Dim intAltura As Integer
            Dim intLine As Int16 = 0
            Dim intPaginas As Decimal
            Dim intNroPag As Int16 = 0
            Dim intRegistro As Integer
            Dim sSubTitulo As String
            Dim sNomSeguro As String
            Dim sValorSeguro As String
            Dim sValorSeguroPlus As String
            Dim sComplementario As String = "A"
            Dim oFunciones As New Library.AccesoBL.Funciones
            fp_Titulo.bBold = True
            fp_Negrita.bBold = True

            If dtTemporal.Rows.Count > 0 Then
                page = New Page(report)
                page.rWidthMM = 210
                page.rHeightMM = 297
                page.SetLandscape()
                intAltura = 100
                page.Add(650, intAltura, New RepString(fp_Titulo, dtTemporal.Rows(0)("NU_DOCU")))
                page.Add(50, intAltura + 50, New RepString(fp_Negrita, "RUC"))

                page.Add(110, intAltura + 50, New RepString(fp_table, ": " & dtTemporal.Rows(0)("CO_CLIE")))
                page.Add(400, intAltura + 50, New RepString(fp_Negrita, "MONEDA"))
                page.Add(450, intAltura + 50, New RepString(fp_table, ": " & dtTemporal.Rows(0)("DE_MONE")))

                page.Add(50, intAltura + 60, New RepString(fp_Negrita, "NOMBRE"))
                page.Add(110, intAltura + 60, New RepString(fp_table, ": " & dtTemporal.Rows(0)("NO_CLIE_REPO")))

                page.Add(50, intAltura + 70, New RepString(fp_Negrita, "DIRECCION"))
                page.Add(110, intAltura + 70, New RepString(fp_table, ": " & dtTemporal.Rows(0)("DE_DIRE_FACT")))

                page.Add(400, intAltura + 70, New RepString(fp_Negrita, "PROMOTOR"))
                page.Add(450, intAltura + 70, New RepString(fp_table, ": " & dtTemporal.Rows(0)("NO_QUIE_AUTO")))

                page.Add(50, intAltura + 80, New RepString(fp_Negrita, "COBRAR EN"))
                page.Add(110, intAltura + 80, New RepString(fp_table, ": " & dtTemporal.Rows(0)("DE_DIRE_COBR")))

                page.Add(400, intAltura + 80, New RepString(fp_Negrita, "COBRADOR"))
                page.Add(450, intAltura + 80, New RepString(fp_table, ": " & dtTemporal.Rows(0)("NO_LARG_ENTI")))

                page.Add(600, intAltura + 80, New RepString(fp_table, dtTemporal.Rows(0)("FE_EMIS")))

                page.Add(50, intAltura + 90, New RepString(fp_Negrita, "REFERENCIA"))
                page.Add(110, intAltura + 90, New RepString(fp_table, ": " & dtTemporal.Rows(0)("FE_EMIS_AVIS") & " DEL " & dtTemporal.Rows(0)("DE_REFE_COBR")))

                page.AddMM(17, 73, New RepLineMM(ppBold, 255, 0))
                page.Add(50, intAltura + 115, New RepString(fp_Negrita, "CONSTANCIA"))
                page.Add(50, intAltura + 122, New RepString(fp_Negrita, "RECEPCION"))
                page.Add(120, intAltura + 115, New RepString(fp_Negrita, "WARRANT/PED. DEPOS."))
                page.Add(120, intAltura + 122, New RepString(fp_Negrita, "NRO. MANIFIESTO"))
                page.Add(220, intAltura + 115, New RepString(fp_Negrita, "PEDIDO COBRANZA"))
                page.Add(220, intAltura + 122, New RepString(fp_Negrita, "DESDE"))
                page.Add(260, intAltura + 122, New RepString(fp_Negrita, "HASTA"))
                page.Add(350, intAltura + 115, New RepString(fp_Negrita, "DESCRIPCION DE LA MERCADERIA"))
                page.Add(600, intAltura + 115, New RepString(fp_Negrita, "VALOR"))
                page.Add(600, intAltura + 122, New RepString(fp_Negrita, "CANTIDAD"))
                page.Add(660, intAltura + 115, New RepString(fp_Negrita, "TARIFA"))
                page.Add(720, intAltura + 115, New RepString(fp_Negrita, "IMPORTE"))
                page.AddMM(17, 79, New RepLineMM(ppBold, 255, 0))

                For i As Int16 = 0 To dtTemporal.Rows.Count - 1
                    intAltura += 10
                    If dtTemporal.Rows(i)("DE_TIPO_SLIN") <> sComplementario Then
                        intAltura += 5
                        page.Add(50, intAltura + 120, New RepString(fp_table, dtTemporal.Rows(i)("DE_TIPO_LINE") & " - " & dtTemporal.Rows(i)("DE_TIPO_SLIN")))
                        sComplementario = dtTemporal.Rows(i)("DE_TIPO_SLIN")
                        intAltura += 10
                    End If
                    'StrIFF( NU_GLOS , '' , StrIFF( StrCompare( ST_ALMA , 'S' ) , DE_SSER , TI_DOCU_REFE||' '||NU_DOCU_REFE , TI_DOCU_REFE||' '||NU_DOCU_REFE ), '')
                    page.Add(50, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", _
                     IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, _
                      IIf(dtTemporal.Rows(i)("ST_ALMA") < "S", dtTemporal.Rows(i)("DE_SSER"), _
                         IIf(dtTemporal.Rows(i)("ST_ALMA") = "S", dtTemporal.Rows(i)("TI_DOCU_REFE") & " " & dtTemporal.Rows(i)("NU_DOCU_REFE"), dtTemporal.Rows(i)("TI_DOCU_REFE") & " " & dtTemporal.Rows(i)("NU_DOCU_REFE"))), _
                                dtTemporal.Rows(i)("DE_GLOS")))))
                    'StrIFF( NU_GLOS , '' , StrIFF( StrCompare( ST_ALMA , 'S') , '', CA_WARR , CA_WARR ), '')
                    page.Add(120, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, IIf(dtTemporal.Rows(i)("ST_ALMA") < "S", "", IIf(dtTemporal.Rows(i)("ST_ALMA") = "S", dtTemporal.Rows(i)("CA_WARR"), dtTemporal.Rows(i)("CA_WARR"))), ""))))
                    'StrIFF( NU_GLOS , '' , StrIFF( StrCompare( ST_ALMA , 'S' ) , '' , FE_INIC_PERI , FE_INIC_PERI ), '')
                    page.Add(220, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, IIf(dtTemporal.Rows(i)("ST_ALMA") < "S", "", IIf(dtTemporal.Rows(i)("ST_ALMA") = "S", dtTemporal.Rows(i)("FE_INIC_PERI"), dtTemporal.Rows(i)("FE_INIC_PERI"))), ""))))
                    'StrIFF( NU_GLOS , '' , StrIFF( StrCompare( ST_ALMA , 'S' ) , '' , FE_FINA_PERI , FE_FINA_PERI ), '')
                    page.Add(260, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, IIf(dtTemporal.Rows(i)("ST_ALMA") < "S", "", IIf(dtTemporal.Rows(i)("ST_ALMA") = "S", dtTemporal.Rows(i)("FE_FINA_PERI"), dtTemporal.Rows(i)("FE_FINA_PERI"))), ""))))

                    'StrIFF( NU_GLOS , '' , StrIFF( NU_BASE , '' , DE_MERC_GENE , StrIFF( StrCompare( ST_ALMA , 'S' ) , '' , DE_MERC_GENE , DE_MERC_GENE ) ), '')
                    page.Add(300, intAltura + 120, New RepString(fp_table, _
                    IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", _
                        IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, _
                            IIf(dtTemporal.Rows(i)("NU_BASE") < 0, "", _
                                IIf(dtTemporal.Rows(i)("NU_BASE") = 0, dtTemporal.Rows(i)("DE_MERC_GENE"), _
                                    IIf(dtTemporal.Rows(i)("ST_ALMA") < "S", "", _
                                        IIf(dtTemporal.Rows(i)("ST_ALMA") = "S", dtTemporal.Rows(i)("DE_MERC_GENE"), dtTemporal.Rows(i)("DE_MERC_GENE"))))), ""))))

                    'StrIFF( NU_GLOS , '' , StrIFF( NU_IMPT , '' , '' , CO_UNME_CONS||' '||Nu0mberToStrPicture(NU_PARA,'#,##0.00') ), '')
                    page.AddRight(640, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, IIf(dtTemporal.Rows(i)("NU_IMPT") < 0, "", IIf(dtTemporal.Rows(i)("NU_IMPT") = 0, "", dtTemporal.Rows(i)("CO_UNME_CONS") & " " & String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("NU_PARA")))), ""))))
                    'StrIFF( NU_GLOS , '' , StrIFF( NU_IMPT , '' , '' , NumberToStr( VA_CONS,4)||' '||CO_UNME_PARA ), '')
                    page.AddRight(690, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("NU_GLOS") < 0, "", IIf(dtTemporal.Rows(i)("NU_GLOS") = 0, IIf(dtTemporal.Rows(i)("NU_IMPT") < 0, "", IIf(dtTemporal.Rows(i)("NU_IMPT") = 0, "", dtTemporal.Rows(i)("VA_CONS") & " " & dtTemporal.Rows(i)("CO_UNME_PARA"))), ""))))
                    'StrIFF( IM_BRUT , '' , '' , NumberToStrPicture(IM_BRUT,'#,##0.00'))
                    page.AddRight(760, intAltura + 120, New RepString(fp_table, IIf(dtTemporal.Rows(i)("IM_BRUT") < 0, "", IIf(dtTemporal.Rows(i)("IM_BRUT") = 0, "", String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_BRUT"))))))
                Next
                intAltura = 270

                page.Add(160, intAltura + 250, New RepString(fp_Negrita, "SON: " & oFunciones.NumPalabra(dtTemporal.Rows(0)("IM_TOTA"), dtTemporal.Rows(0)("CO_MONE"))))
                page.Add(280, intAltura + 260, New RepString(fp_Negrita, "S.E.�.O."))
                'NumberToStr( PC_IMP1 , 0 ) || '%'
                page.Add(580, intAltura + 240, New RepString(fp_table, CStr(CInt(dtTemporal.Rows(0)("PC_IMP1"))) & "%"))
                page.Add(500, intAltura + 250, New RepString(fp_Negrita, "VALOR VENTA"))
                page.Add(580, intAltura + 250, New RepString(fp_Negrita, "I.G.V."))
                page.Add(660, intAltura + 250, New RepString(fp_Negrita, "PRECIO VENTA"))
                page.AddRight(550, intAltura + 260, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("IM_BRUT_CABE"))))
                page.AddRight(610, intAltura + 260, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("IM_IMP1"))))
                page.AddRight(710, intAltura + 260, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("IM_TOTA"))))
            End If
            RT.ResponsePDF(report, Me)

        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
