<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueFacturasOrdenTrabajo.aspx.vb" Inherits="CueFacturasOrdenTrabajo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Orden de Trabajo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
			function Cerrar()
				{			
				window.close();
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="20" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center">
						<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="650" border="0">
							<TR>
								<TD width="6" background="../Images/table_r1_c1.gif" height="6"></TD>
								<TD background="../Images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r2_c1.gif"></TD>
								<TD>
									<TABLE class="Text" id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="Text2" vAlign="top" colSpan="6">ORDEN DE TRABAJO:
												<asp:label id="lblNroOrden" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top" colSpan="6" align="center">
												<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top">OPERACION</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtCodOperacion" runat="server" CssClass="Text" Width="30px"></asp:TextBox>
												<asp:TextBox id="txtOperacion" runat="server" CssClass="Text" Width="150px" BackColor="#E0E0E0"></asp:TextBox></TD>
											<TD vAlign="top">SECUENCIAL</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtSecuencial" runat="server" CssClass="Text" Width="50px" BackColor="#E0E0E0"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top">S.BASE</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtCodBase" runat="server" CssClass="Text" Width="30px"></asp:TextBox>
												<asp:TextBox id="txtBase" runat="server" CssClass="Text" Width="150px" BackColor="#E0E0E0"></asp:TextBox></TD>
											<TD vAlign="top">S.COMPLEM</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtCodComplem" runat="server" CssClass="Text" Width="30px"></asp:TextBox>
												<asp:TextBox id="txtComplem" runat="server" CssClass="Text" Width="150px" BackColor="#E0E0E0"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top">SUB SERVICIO</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtCodServicio" runat="server" CssClass="Text" Width="50px"></asp:TextBox>
												<asp:TextBox id="txtServicio" runat="server" CssClass="Text" Width="130px" BackColor="#E0E0E0"></asp:TextBox></TD>
											<TD vAlign="top">DETALLADO</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtCodDetallado" runat="server" CssClass="Text" Width="80px"></asp:TextBox>
												<asp:TextBox id="txtDetallado" runat="server" CssClass="Text" Width="100px" BackColor="#E0E0E0"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top"></TD>
										</TR>
										<TR>
											<TD class="Text2" vAlign="top" colSpan="3">INICIO DEL SERVICIO</TD>
											<TD class="Text2" vAlign="top" colSpan="3">FIN DEL SERVICIO</TD>
										</TR>
										<TR>
											<TD vAlign="top">FECHA</TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top">
												<asp:TextBox id="txtFechaInicial" runat="server" CssClass="Text" Width="70px"></asp:TextBox>&nbsp; 
												HORA
												<asp:TextBox id="txtHoraInicial" runat="server" CssClass="Text" Width="50px"></asp:TextBox></TD>
											<TD vAlign="top">FECHA</TD>
											<TD vAlign="top"></TD>
											<TD vAlign="top">
												<asp:TextBox id="txtFechaFin" runat="server" CssClass="Text" Width="70px"></asp:TextBox>&nbsp; 
												HORA
												<asp:TextBox id="txtHoraFin" runat="server" CssClass="Text" Width="50px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD vAlign="top">CANTIDAD RECURSOS UTIL</TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtRecursos" runat="server" CssClass="Text" Width="100px"></asp:TextBox></TD>
											<TD vAlign="top" align="right">
												<asp:TextBox id="txtTipoDoc" runat="server" CssClass="Text" Width="30px"></asp:TextBox></TD>
											<TD vAlign="top">:</TD>
											<TD vAlign="top">
												<asp:TextBox id="txtNroDoc" runat="server" CssClass="Text" Width="150px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD align="center" colSpan="6"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
													name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:Cerrar();" type="button" value="Cerrar"
													name="btnCerrar"></TD>
										</TR>
									</TABLE>
								</TD>
								<TD width="6" background="../Images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r3_c1.gif" height="6"></TD>
								<TD background="../Images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
