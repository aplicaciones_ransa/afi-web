Imports System.Data

Public Class AlmMovimientoImpresion
    Inherits System.Web.UI.Page
    Private strdocu_aux As String
    'Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTI_MOVI As System.Web.UI.WebControls.Label
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNO_REFE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNU_REFE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTI_REPO As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNO_MODA As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbEST_DCR As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_EMIS_INIC As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_EMIS_FINA As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Dim i As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgIngresos As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgRetiros As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

        If Not Page.IsPostBack Then
            prMUEST_DATO()
        End If
    End Sub

    Private Sub prMUEST_DATO()
        Me.lbTI_MOVI.Text = Request.QueryString("sNO_MOVI")
        Me.lbNO_REFE.Text = Request.QueryString("sTI_REFE")
        Me.lbNU_REFE.Text = Request.QueryString("sNU_REFE")
        Me.lbTI_REPO.Text = Request.QueryString("sTI_REPO")
        Me.lbNO_MODA.Text = Request.QueryString("sTI_MODA")
        Me.lbEST_DCR.Text = Request.QueryString("sNO_CERR")
        Me.lbFE_EMIS_INIC.Text = Request.QueryString("sFE_INIC")
        Me.lbFE_EMIS_FINA.Text = Request.QueryString("sFE_FINA")
        If Me.lbTI_MOVI.Text.IndexOf("Ingreso") = 0 Then
            Me.lblTitulo.Text = "REPORTE DE INGRESO DE MERCADER�A"
            Me.dgIngresos.Visible = True
            Me.dgIngresos.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
            Me.dgIngresos.DataBind()
        End If
        If Me.lbTI_MOVI.Text.IndexOf("Retiro") = 0 Then
            Me.lblTitulo.Text = "REPORTE DE �RDENES DE RETIRO"
            Me.dgRetiros.Visible = True
            Me.dgRetiros.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
            Me.dgRetiros.DataBind()
        End If
    End Sub

    Private Sub dgRetiros_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetiros.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(11).Text <> "&nbsp;" Then
                    'e.Item.Cells(10).Font.Bold = True
                    e.Item.Cells(11).Font.Bold = True
                    e.Item.Cells(12).Font.Bold = True

                    e.Item.ForeColor = System.Drawing.Color.FromName("#112627")
                    e.Item.BackColor = System.Drawing.Color.FromName("#EAEAEA")

                ElseIf e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(2).Text <> "&nbsp;" Then
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(3)

                    e.Item.Cells(2).ColumnSpan = 2
                    e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(2).Font.Bold = True
                    e.Item.Cells(3).ColumnSpan = 3
                    e.Item.Cells(3).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(3).Font.Bold = True
                    e.Item.Cells(4).ColumnSpan = 5
                    e.Item.Cells(4).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(4).Font.Bold = True
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                End If
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgIngresos_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngresos.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(12).Text <> "&nbsp;" Then
                    e.Item.Cells(11).Font.Bold = True
                    e.Item.Cells(12).Font.Bold = True
                    e.Item.ForeColor = System.Drawing.Color.FromName("#112627")
                    e.Item.BackColor = System.Drawing.Color.FromName("#EAEAEA")
                ElseIf e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(4).Text <> "&nbsp;" Then
                    e.Item.Cells.RemoveAt(18)
                    e.Item.Cells.RemoveAt(17)
                    e.Item.Cells.RemoveAt(16)
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)

                    e.Item.Cells(4).ColumnSpan = 3
                    e.Item.Cells(4).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(4).Font.Bold = True
                    e.Item.Cells(5).ColumnSpan = 7
                    e.Item.Cells(5).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(5).Font.Bold = True
                    e.Item.Cells(6).ColumnSpan = 5
                    e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(6).Font.Bold = True
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                    'Else
                    '    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    '    Dim sCA_CELL As String = e.Item.Cells(0).Text
                    '    Dim sCA_LINK As String = "<A onclick=Javascript:Abrir('" & CStr(e.Item.DataItem("TI_DOCU_RECE")) & "','" & CStr(e.Item.DataItem("NU_DOCU_RECE")) & "','" & CStr(e.Item.DataItem("CO_UNID")) & "'); href='#'> " & _
                    '                             "" & sCA_CELL & "</a>"
                    '    e.Item.Cells(0).Text = sCA_LINK
                End If
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub
End Class
