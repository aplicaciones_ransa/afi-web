<%@ Page Language="vb" AutoEventWireup="false" CodeFile="PoputAbonos.aspx.vb" Inherits="PoputAbonos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle de Ingreso</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<script language="javascript" src="../JScripts/Validaciones.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="10" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="600" align="center">
				<TR>
					<TD class="td" onkeyup="this.value = this.value.toUpperCase();" align="center">Detalle 
						de Abonos</TD>
				</TR>
				<TR>
					<TD align="center"><asp:datagrid id="dgResultado" runat="server" OnPageIndexChanged="Change_Page" AllowPaging="True"
							AutoGenerateColumns="False" BorderColor="Gainsboro" Width="100%" CssClass="gv">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="NU_ABON" HeaderText="Nro.Operaci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_CHAB" HeaderText="Fech.Abono">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_COBR" HeaderText="Fech.Aplicaci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_MONE_PAGO" HeaderText="Moneda">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="IM_COBR" HeaderText="Importe" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center"><input style="WIDTH: 80px" class="Text" onclick="Javascript:window.close();" value="Cerrar"
							type="button"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
