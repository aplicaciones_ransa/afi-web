Public Class SolicitarInformacion
    Inherits System.Web.UI.Page
    Private objFuncion As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtDetalle As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAsunto As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Request.IsAuthenticated Then
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Try
            Dim strDe As String = System.Configuration.ConfigurationManager.AppSettings("EmailDepsa")
            Dim strPara As String = System.Configuration.ConfigurationManager.AppSettings("CorreoComercial")
            objFuncion.SendMail(strDe, strPara, "Solicitud de Informaci�n hist�rica en la web AFI: " & Me.txtAsunto.Text & " / " & "Usuario: " & Session.Item("nom_user") & " / " & "Empresa: " & Session.Item("NombreEntidad"), Me.txtDetalle.Text, "")
            Response.Write("<script language='javascript'>alert('Se env�o correctamente'); window.close();</script>")
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
End Class
