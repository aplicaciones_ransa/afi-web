Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Library.AccesoDB
Imports System.Data

Public Class AlmSaldoImpresion_w
    Inherits System.Web.UI.Page
    Private strdocu_aux As String
    Private objMercaderia As clsCNMercaderia
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_MERC As System.Web.UI.WebControls.Label
    'Protected WithEvents lbLOTE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_ALMA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_INI_FIN As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbFE_VENC As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCODIGO As System.Web.UI.WebControls.Label
    Dim i As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)

        If Not Page.IsPostBack Then
            prMUEST_DATO()
        End If
    End Sub

    Private Sub prMUEST_DATO()

        Dim strFe_Ini As String = "Del " & Request.QueryString("sFE_INIC")
        Dim strFe_Fin As String = " Al " & Request.QueryString("sFE_FINA")
        If strFe_Ini <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini
        End If
        If strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Fin
        End If
        If strFe_Ini <> "" And strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini & strFe_Fin
        End If
        Me.lbDE_ALMA.Text = Request.QueryString("sDE_ALMA")
        Me.lbFE_VENC.Text = Request.QueryString("sFE_VENC")
        Me.lbCODIGO.Text = Request.QueryString("sCODIGO")
        Me.lbDE_MERC.Text = Request.QueryString("sDE_MERC")
        Me.lbLOTE.Text = Request.QueryString("sLOTE")
        Me.dgResultado.CurrentPageIndex = 0

        BindatagridIngreso()

    End Sub

    Private Sub BindatagridIngreso()
        Try
            Dim dtMercWMS As DataTable
            objMercaderia = New clsCNMercaderia

            dtMercWMS = objMercaderia.gCNGetListarInventarioWMS(Request.QueryString("sCODIGO"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"), Request.QueryString("sFE_INIC"),
                                Request.QueryString("sFE_FINA"), Request.QueryString("sFE_VENC"), Request.QueryString("sCO_ALMA"), Session.Item("IdSico"))
            Session.Add("dtMovimientoWMS", dtMercWMS)
            Me.dgResultado.Visible = True
            Me.dgResultado.DataSource = dtMercWMS
            Me.dgResultado.DataBind()

        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub


End Class
