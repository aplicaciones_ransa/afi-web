Imports System.Data

Public Class AlmImpresionKardex
    Inherits System.Web.UI.Page
    Protected dvKardex As DataView
    Private strdocu_aux As String
    Dim i As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lbFE_EMIS_FINA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_EMIS_INIC As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbEST_DCR As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNO_MODA As System.Web.UI.WebControls.Label
    'Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label12 As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbNO_REF As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_PRODU As System.Web.UI.WebControls.Label
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)
        If Not Page.IsPostBack Then
            prMUEST_DATO()
        End If
    End Sub

    Private Sub prMUEST_DATO()
        Me.lbNO_MODA.Text = Request.QueryString("sNO_MODA")
        Me.lbEST_DCR.Text = Request.QueryString("sEST_DCR")
        Me.lbFE_EMIS_INIC.Text = Request.QueryString("sFE_INIC")
        Me.lbFE_EMIS_FINA.Text = Request.QueryString("sFE_FINA")
        Me.lbNO_REF.Text = Request.QueryString("sREFE")
        Me.lbCO_PRODU.Text = Request.QueryString("sCO_PRODU")

        dvKardex = CType(Session.Item("dtMovimiento"), DataView)
        Me.dgResultado.DataSource = dvKardex
        Me.dgResultado.DataBind()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim strflag As String
        Dim k As Integer
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                strflag = e.Item.Cells(6).Text
                If i = 0 Then
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells(0).ColumnSpan = 5
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(0).Font.Bold = True
                    e.Item.Cells(1).ColumnSpan = 5
                    e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(1).Font.Bold = True
                    e.Item.Cells(2).ColumnSpan = 5
                    e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(2).Font.Bold = True
                    e.Item.BackColor = System.Drawing.Color.Lavender
                    i = 1
                Else
                    If strflag = "0" Then
                        e.Item.Cells.RemoveAt(15)
                        e.Item.Cells.RemoveAt(14)
                        e.Item.Cells.RemoveAt(13)
                        e.Item.Cells.RemoveAt(12)
                        e.Item.Cells.RemoveAt(11)
                        e.Item.Cells.RemoveAt(10)
                        e.Item.Cells.RemoveAt(9)
                        e.Item.Cells.RemoveAt(8)
                        e.Item.Cells.RemoveAt(7)
                        e.Item.Cells.RemoveAt(6)
                        e.Item.Cells.RemoveAt(5)
                        e.Item.Cells.RemoveAt(4)
                        e.Item.Cells.RemoveAt(3)
                        e.Item.Cells(0).ColumnSpan = 5
                        e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(0).Font.Bold = True
                        e.Item.Cells(1).ColumnSpan = 5
                        e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(1).Font.Bold = True
                        e.Item.Cells(2).ColumnSpan = 5
                        e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(2).Font.Bold = True
                        e.Item.BackColor = System.Drawing.Color.Lavender
                    Else
                        If e.Item.Cells(15).Text = "N" Then
                            For k = 0 To 15
                                e.Item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                            Next
                        End If
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub
End Class
