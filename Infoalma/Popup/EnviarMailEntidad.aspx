<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EnviarMailEntidad.aspx.vb" Inherits="EnviarMailEntidad" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Configuración</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="francisco_uni@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="350" border="0">
				<TR>
					<TD class="td" height="30">Configurar Ruta
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="6" background="../Images/table_r1_c1.gif" height="6"></TD>
								<TD background="../Images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r2_c1.gif"></TD>
								<TD>
									<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="Text" width="18%">Secuencia:</TD>
											<TD class="Text" width="82%">
												<asp:label id="lblDescripcion" runat="server"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
								<TD width="6" background="../Images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r3_c1.gif" height="6"></TD>
								<TD background="../Images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="Text">Envio de Mail:
					</TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="dgdTipoEntidad" runat="server" Width="100%" PageSize="1" AutoGenerateColumns="False"
							CssClass="gv" BorderColor="Gainsboro">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="COD_TIPENTI" HeaderText="Cod Tipo Entidad"></asp:BoundColumn>
								<asp:BoundColumn DataField="NOM_TIPENTI" HeaderText="Tipo de Entidad">
									<HeaderStyle Width="200px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Recepciona">
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="60" border="0">
											<TR>
												<TD align="center">
													<asp:CheckBox id=chkMail runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.REC_MAIL") %>'>
													</asp:CheckBox></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="Text">Activación de controles</TD>
				</TR>
				<TR>
					<TD class="etiqueta">
						<TABLE id="Table3" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Text" width="50%">Seleccione Tipo Entidad:</TD>
								<TD width="50%">
									<asp:DropDownList id="cboTipoEntidad" runat="server" Width="150px" AutoPostBack="True" CssClass="Text"></asp:DropDownList></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="dgdControles" runat="server" Width="100%" PageSize="1" AutoGenerateColumns="False"
							CssClass="gv" BorderColor="Gainsboro">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="COD_CTRL" HeaderText="Cod Control"></asp:BoundColumn>
								<asp:BoundColumn DataField="NOM_CTRL" HeaderText="Control">
									<HeaderStyle Width="140px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Usr. Comun">
									<HeaderStyle Width="70px"></HeaderStyle>
									<ItemTemplate>
										<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="50" border="0">
											<TR>
												<TD align="center">
													<asp:CheckBox id=chkComun runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.USR_COMU") %>'>
													</asp:CheckBox></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Usr. Superv.">
									<HeaderStyle Width="70px"></HeaderStyle>
									<ItemTemplate>
										<TABLE id="Table11" cellSpacing="0" cellPadding="0" width="50" border="0">
											<TR>
												<TD align="center">
													<asp:CheckBox id=chkConsultor runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.USR_CONS") %>'>
													</asp:CheckBox></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Usr. Aprob.">
									<HeaderStyle Width="70px"></HeaderStyle>
									<ItemTemplate>
										<TABLE id="Table12" cellSpacing="0" cellPadding="0" width="50" border="0">
											<TR>
												<TD align="center">
													<asp:CheckBox id=chkAprobador runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.USR_APRO") %>'>
													</asp:CheckBox></TD>
											</TR>
										</TABLE>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="120"></TD>
								<TD><asp:button id="btnGrabar" runat="server" Width="80px" Text="Grabar" CssClass="btn"></asp:button></TD>
								<TD><INPUT id="btnCerrar" style="WIDTH: 80px; POSITION: static" onclick="window.close()" type="button"
										value="Cerrar" class="btn"></TD>
								<TD width="120"></TD>
							</TR>
						</TABLE>
						<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
