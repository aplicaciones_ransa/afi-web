<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmSaldoImpresion_w.aspx.vb" Inherits="AlmSaldoImpresion_w" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlmMovimientoImpresion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="4" cellPadding="0" width="100%">
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="center"><asp:label id="lblTitulo" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD><asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD><asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD><asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="center">
						<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="683">
							<TR>
								<TD style="WIDTH: 97px"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">ALMACEN</asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label style="Z-INDEX: 0" id="Label14" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px" colSpan="7"><asp:label style="Z-INDEX: 0" id="lbDE_ALMA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD width="16%"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">FEC. VENC MERC <                                     =</asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label id="Label13" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px"><asp:label style="Z-INDEX: 0" id="lbFE_VENC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"><asp:label style="Z-INDEX: 0" id="Label15" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> CODIGO </asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="lbCODIGO" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD width="17%"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> DESCRIPCIÓN MERC. </asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="Label17" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="lbDE_MERC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 97px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> LOTE </asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px"><asp:label style="Z-INDEX: 0" id="lbLOTE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"><asp:label style="Z-INDEX: 0" id="Label6" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">FECHA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD colSpan="4"><asp:label style="Z-INDEX: 0" id="lbFE_INI_FIN" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="lbMS_REGI" runat="server" ForeColor="Red" CssClass="text"></asp:label><asp:datagrid style="Z-INDEX: 0" id="dgResultado" runat="server" CssClass="gv" Visible="False"
							BorderColor="Gainsboro" Width="100%" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="PRODUCTO" HeaderText="Producto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD" HeaderText="Unidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="LOTE" HeaderText="Lote">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ESTADO" HeaderText="Estado"></asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_INGRESO" HeaderText="Fecha Ingreso">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_VENCIMIENTO" HeaderText="Fecha Vencimiento">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center"><INPUT id="btnImprimir" class="btn" onclick="window.print();" value="Imprimir" type="button"
							name="btnImprimir"> <INPUT style="WIDTH: 80px" id="btnCerrar" class="btn" onclick="window.close();" value="Cerrar"
							type="button" name="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
