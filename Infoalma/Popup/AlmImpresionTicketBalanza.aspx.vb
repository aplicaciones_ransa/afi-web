Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Root.Reports

'Imports System.IO

Public Class AlmImpresionTicketBalanza
    Inherits System.Web.UI.Page
    Dim sCO_UNID, sTI_DOCU, sNU_DOCU As String
    Dim objCNTicket As New clsCNTicket

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            GenerarPDF()
        End If
    End Sub

    Private Sub GenerarPDF()
        Dim dt As New DataTable
        Try
            ' Dim objTicket As New DEPSA.LibCapaNegocio.clsCNTicket
            dt = objCNTicket.fn_TCBALA_Sel_ReportePesadasPDF(Session("CoEmpresa"), Request.QueryString("sCO_UNID"), Request.QueryString("sCO_ALMA"), Request.QueryString("sTI_DOCU"), Request.QueryString("sNU_DOCU"), Request.QueryString("sTI_TIK"))

            'dt = objCNTicket.fn_TCBALA_Sel_ReportePesadasPDF("01", "001", "00010", "TIK", "388516", "I")
            'dt = objCNTicket.fn_TCBALA_Sel_ReportePesadasPDF("01", "001", "00010", "TIK", "388417", "S")
            GenerarSustento(dt)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Private Sub GenerarSustento(ByVal dtTemporal As DataTable)
        Try
            Dim report As Report = New Report(New PdfFormatter)
            Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
            Dim fp As FontProp = New FontPropMM(fd, 2.0)
            Dim fp_table As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_Negrita As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_peque�o As FontProp = New FontPropMM(fd, 1.5)
            Dim fp_peque�o2 As FontProp = New FontPropMM(fd, 1.2)
            Dim fp_Titulo As FontProp = New FontPropMM(fd, 3.5)
            Dim ppBold As PenProp = New PenProp(report, 1.0)
            Dim ppBold2 As PenProp = New PenProp(report, 0.6)
            Dim page As Page
            Dim intAltura As Integer
            Dim intLine As Int16 = 0
            Dim intPaginas As Decimal
            Dim sTipoMoneda As String
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            Dim dttFecha As DateTime
            dttFecha = Date.Today
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            dttFecha = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
            Dim sTotalCant As Decimal
            Dim sTotalMerc As Decimal
            Dim sTotalImpo As Decimal
            Dim sTotalComp As Decimal
            Dim sTTotalCant As Decimal
            Dim sTTotalMerc As Decimal
            Dim sTTotalImpo As Decimal
            Dim sTTotalComp As Decimal
            Dim sCodAlma As String
            Dim sNuTitu As String
            Dim count As Integer

            'CREAR DATATABLA GRUPO 1
            Dim dtGrupo1 As DataTable
            dtGrupo1 = GrupoDistinct(dtTemporal, "NU_PLAC")
            page = New Page(report)
            page.rWidthMM = 297
            page.rHeightMM = 210
            page.SetLandscape()
            intAltura = 50

            '=========== Print Regla ==========
            'For i As Integer = 10 To 800 - 1
            '    page.Add(i, intAltura + 10, New RepString(fp_Negrita, "|"))
            '    page.Add(i + 5, intAltura + 10, New RepString(fp_peque�o, "."))
            '    page.Add(i, intAltura + 20, New RepString(fp_peque�o2, i))
            '    i = i + 10 - 1
            'Next

            page.Add(30, intAltura + 40, New RepString(fp_Negrita, "ALMACENERA DEL PER� S.A."))
            page.Add(120, intAltura + 40, New RepString(fp_Negrita, "  RUC 20100000688"))
            page.Add(470, intAltura + 40, New RepString(fp_Negrita, "FECHA"))
            page.Add(510, intAltura + 40, New RepString(fp_Negrita, ": " & dttFecha))
            page.Add(30, intAltura + 50, New RepString(fp_Negrita, "DIRECCION"))
            page.Add(120, intAltura + 50, New RepString(fp_Negrita, ": " & dtTemporal.Rows(0)("DE_DIRE_ALMA")))
            page.Add(30, intAltura + 60, New RepString(fp_Negrita, "CIUDAD"))
            page.Add(120, intAltura + 60, New RepString(fp_Negrita, ": " & dtTemporal.Rows(0)("NO_UBIC_GEOG")))
            page.Add(470, intAltura + 60, New RepString(fp_Negrita, "TEL�FONO"))
            page.Add(510, intAltura + 60, New RepString(fp_Negrita, ": " & dtTemporal.Rows(0)("NU_TELE_0001")))

            page.Add(30, intAltura + 65, New RepString(fp_Negrita, "_______________________________________________________________________________________________________________________________________"))
            page.Add(240, intAltura + 80, New RepString(fp_Negrita, "TICKET DE PESAJE N� " & dtTemporal.Rows(0)("NU_DOCU")))
            page.Add(30, intAltura + 90, New RepString(fp_Negrita, IIf(dtTemporal.Rows(0)("TI_MOVI") = "I", "GUIA DE REMISI�N", "GUIA DE RETIRO")))

            If dtTemporal.Rows(0)("TI_MOVI") = "I" Then
                page.Add(120, intAltura + 90, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("NU_GUIA_REMI")))
                page.Add(470, intAltura + 90, New RepString(fp_Negrita, "DCR - " & dtTemporal.Rows(0)("NU_DOCU_RECE")))
            Else
                page.Add(120, intAltura + 90, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("NU_DOCU_RETI")))
            End If

            page.Add(30, intAltura + 100, New RepString(fp_Negrita, "MERCADER�A")) : page.Add(120, intAltura + 100, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("DE_MERC")))
            page.Add(30, intAltura + 110, New RepString(fp_Negrita, "COD. CLIENTE")) : page.Add(120, intAltura + 110, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("CO_CLIE") & " - " & dtTemporal.Rows(0)("NO_CLIE")))
            page.Add(30, intAltura + 120, New RepString(fp_Negrita, "EMPRESA TRANSPORTE")) : page.Add(120, intAltura + 120, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("NU_RUCC_TRAN") & " - " & dtTemporal.Rows(0)("EMPR_TRAN")))
            page.Add(30, intAltura + 130, New RepString(fp_Negrita, "NOM. CHOFER")) : page.Add(120, intAltura + 130, New RepString(fp_Negrita, "---> " & dtTemporal.Rows(0)("NO_CHOF")))
            page.Add(470, intAltura + 130, New RepString(fp_Negrita, "BREVETE"))
            page.Add(510, intAltura + 130, New RepString(fp_Negrita, ": " & dtTemporal.Rows(0)("CO_CHOF")))
            page.Add(30, intAltura + 140, New RepString(fp_Negrita, "TIPO MOVIMIENTO"))

            If dtTemporal.Rows(0)("TI_MOVI") = "I" Then
                page.Add(120, intAltura + 140, New RepString(fp_Negrita, "---> " & "INGRESO DE MERCADER�AS"))
            Else
                page.Add(120, intAltura + 140, New RepString(fp_Negrita, "---> " & "SALIDA DE MERCADER�AS"))
            End If

            page.Add(470, intAltura + 140, New RepString(fp_Negrita, "USUARIO"))
            page.Add(510, intAltura + 140, New RepString(fp_Negrita, ": " & dtTemporal.Rows(0)("CO_USUA_CREA")))
            page.Add(30, intAltura + 145, New RepString(fp_Negrita, "_______________________________________________________________________________________________________________________________________"))
            page.Add(50, intAltura + 160, New RepString(fp_Negrita, "TIPO DE CONTROL EFECTUADO : BALANZA      (Seg�n Reglamento Nacional de Veh�culos - Dec. Supremo 058-2003-MTC)"))
            page.Add(40, intAltura + 180, New RepString(fp_Negrita, "PLACAS ( cami�n, tracto, "))
            page.Add(40, intAltura + 190, New RepString(fp_Negrita, "Remolque, semiremolque, "))
            page.Add(40 + 20, intAltura + 200, New RepString(fp_Negrita, "Carretas ) "))
            page.Add(130, intAltura + 180, New RepString(fp_Negrita, "Dimensiones del Vehiculo "))
            page.Add(130, intAltura + 190, New RepString(fp_Negrita, "( Incluida Mercaderia Mt. ) "))
            page.Add(130, intAltura + 200, New RepString(fp_Negrita, "LARGO   ANCHO   ALTO"))
            page.Add(230, intAltura + 180, New RepString(fp_Negrita, ""))
            page.Add(230, intAltura + 190, New RepString(fp_Negrita, "CONFIGURACION "))
            page.Add(230 + 10, intAltura + 200, New RepString(fp_Negrita, "VEHICULAR "))
            page.Add(230 + 10, intAltura + 220, New RepString(fp_Negrita, dtTemporal.Rows(0)("CO_CONF_VEHI")))
            page.Add(320, intAltura + 180, New RepString(fp_Negrita, "Peso  Bruto"))
            page.Add(320, intAltura + 190, New RepString(fp_Negrita, "Maximo Permitido"))
            page.Add(320 + 10, intAltura + 200, New RepString(fp_Negrita, " (  KG. ) "))
            page.Add(320 + 10, intAltura + 220, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_MAXI"))))
            page.Add(400, intAltura + 180, New RepString(fp_Negrita, "Peso Bruto Total"))
            page.Add(400, intAltura + 190, New RepString(fp_Negrita, "Transportado"))
            page.Add(400 + 10, intAltura + 200, New RepString(fp_Negrita, " (  KG. ) "))
            page.Add(400 + 10, intAltura + 220, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_BRUT"))))
            page.Add(490, intAltura + 180, New RepString(fp_Negrita, "PBMx/ No"))
            page.Add(490, intAltura + 190, New RepString(fp_Negrita, "Control Ejes"))
            page.Add(490 + 5, intAltura + 200, New RepString(fp_Negrita, "95% (Kg.)"))
            page.Add(490 + 10, intAltura + 220, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_NCON"))))
            page.Add(30, intAltura + 205, New RepRectMM(ppBold2, 190, 14))
            'detalle de placas
            For g1 As Int16 = 0 To dtGrupo1.Rows.Count - 1
                If dtGrupo1.Rows(g1)("ST_PRINT") = "" Then
                    intAltura += 10
                    page.Add(40, intAltura + 230, New RepString(fp_Negrita, dtGrupo1.Rows(g1)("NU_PLAC")))
                    page.Add(130, intAltura + 230, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtGrupo1.Rows(g1)("NU_LARG"))))
                    page.Add(160, intAltura + 230, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtGrupo1.Rows(g1)("NU_ANCH"))))
                    page.Add(190, intAltura + 230, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtGrupo1.Rows(g1)("NU_ALTO"))))
                End If
            Next
            page.Add(30, intAltura + 240, New RepString(fp_Negrita, "_______________________________________________________________________________________________________________________________________"))
            page.Add(70, intAltura + 250, New RepString(fp_Negrita, "PRIMERA PESADA"))
            page.Add(220, intAltura + 250, New RepString(fp_Negrita, "SEGUNDA PESADA"))
            page.Add(370, intAltura + 250, New RepString(fp_Negrita, "PESO FINAL"))
            'primera pesada
            page.Add(40, intAltura + 260, New RepString(fp_Negrita, "FECHA")) : page.Add(70, intAltura + 260, New RepString(fp_Negrita, "--->"))
            page.Add(90, intAltura + 260, New RepString(fp_Negrita, dtTemporal.Rows(0)("FE_INGR_VEHI")))
            page.Add(40, intAltura + 270, New RepString(fp_Negrita, "HORA")) : page.Add(70, intAltura + 270, New RepString(fp_Negrita, "--->"))
            page.Add(90, intAltura + 270, New RepString(fp_Negrita, dtTemporal.Rows(0)("HO_INGR_VEHI")))
            page.Add(40, intAltura + 280, New RepString(fp_Negrita, "PESO")) : page.Add(70, intAltura + 280, New RepString(fp_Negrita, "--->"))
            page.Add(90, intAltura + 280, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_INGR")) & " Kg."))
            String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_DIFE"))
            'segunda pesada
            page.Add(190, intAltura + 260, New RepString(fp_Negrita, "FECHA")) : page.Add(220, intAltura + 260, New RepString(fp_Negrita, "--->"))
            page.Add(240, intAltura + 260, New RepString(fp_Negrita, dtTemporal.Rows(0)("FE_SALI_VEHI")))
            page.Add(190, intAltura + 270, New RepString(fp_Negrita, "HORA")) : page.Add(220, intAltura + 270, New RepString(fp_Negrita, "--->"))
            page.Add(240, intAltura + 270, New RepString(fp_Negrita, dtTemporal.Rows(0)("HO_SALI_VEHI")))
            page.Add(190, intAltura + 280, New RepString(fp_Negrita, "PESO")) : page.Add(220, intAltura + 280, New RepString(fp_Negrita, "--->"))
            page.Add(240, intAltura + 280, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_SALI")) & " Kg."))
            'peso bruto
            page.Add(340, intAltura + 260, New RepString(fp_Negrita, "BRUTO")) : page.Add(370, intAltura + 260, New RepString(fp_Negrita, "--->"))
            page.Add(390, intAltura + 260, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_BRUT")) & " Kg."))
            page.Add(340, intAltura + 270, New RepString(fp_Negrita, "TARA")) : page.Add(370, intAltura + 270, New RepString(fp_Negrita, "--->"))
            page.Add(390, intAltura + 270, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_TARA")) & " Kg."))
            page.Add(340, intAltura + 280, New RepString(fp_Negrita, "NETO")) : page.Add(370, intAltura + 280, New RepString(fp_Negrita, "--->"))
            page.Add(390, intAltura + 280, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_PESO_DIFE")) & " Kg."))
            page.Add(40, intAltura + 300, New RepString(fp_Negrita, dtTemporal.Rows(0)("DE_OBSE")))
            page.Add(450, intAltura + 300, New RepString(fp_Negrita, "CANT.BULTOS"))
            page.Add(510, intAltura + 300, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("NU_BULT"))))
            page.Add(40, intAltura + 310, New RepString(fp_Negrita, "DEP�SITO ADUANERO AUTORIZADO"))
            page.Add(490, intAltura + 310, New RepString(fp_Negrita, "DE-R-015-GAP"))

            RT.ResponsePDF(report, Me)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Function GrupoDistinct(ByVal dtTemporal As DataTable, ByVal sColumna1 As String)
        Dim dr As DataRow
        Dim dt1 As New DataTable
        Dim sExiste As Boolean
        dt1.Columns.Add(sColumna1, GetType(System.String))
        dt1.Columns.Add("ST_PRINT", GetType(System.String))
        dt1.Columns.Add("NU_LARG", GetType(System.Decimal))
        dt1.Columns.Add("NU_ANCH", GetType(System.Decimal))
        dt1.Columns.Add("NU_ALTO", GetType(System.Decimal))


        For i As Integer = 0 To dtTemporal.Rows.Count - 1
            sExiste = False
            For ii As Integer = 0 To dt1.Rows.Count - 1
                If dt1.Rows(ii)(sColumna1) = dtTemporal.Rows(i)(sColumna1) Then
                    sExiste = True
                    Exit For
                End If
            Next

            If sExiste = False Then
                dr = dt1.NewRow()
                dr(sColumna1) = dtTemporal.Rows(i)(sColumna1)
                dr("ST_PRINT") = "" ' Columna adicionales 
                dr("NU_LARG") = dtTemporal.Rows(i)("NU_LARG")
                dr("NU_ANCH") = dtTemporal.Rows(i)("NU_ANCH")
                dr("NU_ALTO") = dtTemporal.Rows(i)("NU_ALTO")
                dt1.Rows.Add(dr)
            End If
        Next
        Return dt1
    End Function

    Protected Overrides Sub Finalize()
        If Not (objCNTicket Is Nothing) Then
            objCNTicket = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
