<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueImpresionCancela.aspx.vb" Inherits="CueImpresionCancela"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Impresión de Cancelaciones</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/CDIDIXSTYL_GENE.css" type="text/css" rel="stylesheet">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body onload="window.print();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="texto7pt" vAlign="top">
					<TD align="center" colSpan="2">
						<asp:label id="Label10" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="8pt"> REPORTE DE LA RELACION DE CANCELACIONES</asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500">
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
					<TD width="200"></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500"></TD>
					<TD width="200">
						<asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500"></TD>
					<TD width="200">
						<asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="left">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0" align="center">
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label9" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD colSpan="4">
									<asp:label id="lbNO_CLIE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label6" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt"> TIPO DOCUMENTO</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label2" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbNO_DOCU" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"></TD>
								<TD style="WIDTH: 6px"></TD>
								<TD></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt"> FEC. DOCUMENTO</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label7" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbFE_EMIS_INIC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label>-
									<asp:label id="lbFE_EMIS_FINA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"></TD>
								<TD style="WIDTH: 6px"></TD>
								<TD>
									-</TD>
							</TR>
						</TABLE>
						<HR SIZE="2">
						<asp:datagrid id="dgTCCOBR" tabIndex="2" runat="server" AutoGenerateColumns="False" Width="700px"
							PageSize="20" CssClass="gv" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="TI_DOCU_COBR" HeaderText="Tipo Doc">
									<HeaderStyle Width="60px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_DOCU_COBR" SortExpression="NU_DOCU_COBR" HeaderText="Nro.Doc.">
									<HeaderStyle Width="270px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FEC. DOCUMENTO" SortExpression="FEC. DOCUMENTO" HeaderText="Fec.Doc."
									DataFormatString="{0:d}">
									<HeaderStyle Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FEC. CANCELACION" SortExpression="FEC. CANCELACION" HeaderText="Fec.Cancelac."
									DataFormatString="{0:d}">
									<HeaderStyle Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SOLES" SortExpression="SOLES" HeaderText="Importe S/." DataFormatString="{0:#,##0.00}">
									<HeaderStyle Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DOLARES" SortExpression="DOLARES" HeaderText="Importe $/." DataFormatString="{0:#,##0.00}">
									<HeaderStyle Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="5" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid><BR>
						<BR>
						<asp:label id="Label8" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="7pt">TOTAL GENERAL</asp:label><BR>
						<asp:datagrid id="dgTCCOBR_0001" runat="server" AutoGenerateColumns="False" Width="320px" CssClass="gv"
							BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter" VerticalAlign="Top"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="TOT_SOL" HeaderText="Total Soles" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="TOT_DOL" HeaderText="Total Dolares" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid><BR>
						<INPUT class="btn" id="btnCerrar" onclick="window.close();" type="button" value="Cerrar"
							style="WIDTH: 80px"> <INPUT class="btn" id="btnImprimir" style="WIDTH: 80px" onclick="window.print();" type="button"
							size="80" value="Imprimir" name="btnImprimir">
						<BR>
						<BR>
						<BR>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
