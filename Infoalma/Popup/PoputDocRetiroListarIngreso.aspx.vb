Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Library.AccesoBL
Imports System.Data

Public Class PoputDocRetiroListarIngreso
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objAcceso As Library.AccesoDB.Acceso = New Library.AccesoDB.Acceso
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objRetiro As New clsCNRetiro
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            If Not IsPostBack Then
                Bindatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dt As DataTable
            dt = objRetiro.gCNGetBuscarDetaRetirosIngresadosWMS(Request.QueryString("sDE_ALMA"), Request.QueryString("sNU_GUIA"))
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Session.Add("dtDetaRetiIngWMS", dt)
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Bindatagrid()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(8).Text = 1 Then
                    e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    'e.Item.Cells.RemoveAt(6)
                    'e.Item.Cells.RemoveAt(5)
                    'e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    'e.Item.Cells.RemoveAt(2)
                    'e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    'e.Item.Cells(0).Text = ""
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(8).Text = 2 Then
                    e.Item.Cells(0).Text = "TOTAL :&nbsp;&nbsp;&nbsp;" & (e.Item.Cells(4).Text).ToString
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 8
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgExport.DataSource = Session.Item("dtDetaRetiIngWMS")
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub
End Class

