<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueImpresionEstadoCuenta.aspx.vb" Inherits="CueImpresionEstadoCuenta"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Impresión Estado de Cuenta</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/CDIDIXSTYL_GENE.css" type="text/css" rel="stylesheet">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body onload="window.print();">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="texto7pt" vAlign="top">
					<TD align="center" colSpan="2">
						<asp:label id="Label10" runat="server" Font-Size="8pt" Font-Names="verdana,arial" Font-Bold="True"> REPORTE ESTADO DE CUENTA</asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500">
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Size="7pt" Font-Names="verdana,arial"></asp:label></TD>
					<TD width="200"></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500"></TD>
					<TD width="200">
						<asp:label id="lbFE_REPO_GENE" runat="server" Font-Size="7pt" Font-Names="verdana,arial"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD width="500"></TD>
					<TD width="200">
						<asp:label id="lbHO_REPO_GENE" runat="server" Font-Size="7pt" Font-Names="verdana,arial"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="600" border="0" align="center">
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 49px">
									<asp:label id="Label9" runat="server" Font-Size="7pt" Font-Names="verdana,arial" Font-Bold="True">CLIENTE</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label1" runat="server" Font-Size="7pt" Font-Names="verdana,arial" Font-Bold="True">:</asp:label></TD>
								<TD>
									<asp:label id="lbDE_LARG_ENFI" runat="server" Font-Size="7pt" Font-Names="verdana,arial"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 49px">
									<asp:label id="Label6" runat="server" Font-Size="7pt" Font-Names="verdana,arial" Font-Bold="True"> MONEDA</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label2" runat="server" Font-Size="7pt" Font-Names="verdana,arial" Font-Bold="True">:</asp:label></TD>
								<TD>
									<asp:label id="lbDE_MONE" runat="server" Font-Size="7pt" Font-Names="verdana,arial"></asp:label></TD>
							</TR>
						</TABLE>
						<HR SIZE="2">
						<asp:label id="lbMS_REGI" runat="server" ForeColor="Red" CssClass="Texto9pt"></asp:label><BR>
						<asp:label id="lbAMER" runat="server" CssClass="texto9pt">DOLARES AMERICANOS</asp:label>
						<asp:datagrid id="dgAMER" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" Width="720px"
							AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_AMER" runat="server" CssClass="texto9pt">TOTAL DOLARES AMERICANOS</asp:label>
						<asp:datagrid id="dgTAMER" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbCANA" runat="server" CssClass="texto9pt">DOLARES CANADIENSES</asp:label>
						<asp:datagrid id="dgCANA" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" BorderStyle="None"
							Width="720px" AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_CANA" runat="server" CssClass="texto9pt">TOTAL DOLARES CANADIENSES</asp:label>
						<asp:datagrid id="dgTCANA" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:N}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbEURO" runat="server" CssClass="texto9pt">EURO</asp:label>
						<asp:datagrid id="dgEURO" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" BorderStyle="None"
							Width="720px" AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_EURO" runat="server" CssClass="texto9pt">TOTAL EUROS</asp:label><BR>
						<asp:datagrid id="dgTEURO" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbLIBR" runat="server" CssClass="texto9pt">LIBRAS ESTERLINAS</asp:label>
						<asp:datagrid id="dgLIBR" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" BorderStyle="None"
							Width="720px" AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter" VerticalAlign="Top"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_LIBR" runat="server" CssClass="texto9pt">TOTAL LIBRAS ESTERLINAS</asp:label>
						<asp:datagrid id="dgTLIBR" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbSOLE" runat="server" CssClass="texto9pt">NUEVOS SOLES</asp:label>
						<asp:datagrid id="dgSOLE" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" BorderStyle="None"
							GridLines="None" Width="720px" AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_SOLE" runat="server" CssClass="texto9pt">TOTAL NUEVOS SOLES</asp:label>
						<asp:datagrid id="dgTSOLE" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbYENE" runat="server" CssClass="texto9pt">YENES</asp:label>
						<asp:datagrid id="dgYENE" tabIndex="2" runat="server" CssClass="gv" AllowSorting="True" BorderStyle="None"
							Width="720px" AutoGenerateColumns="False" PageSize="20" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Tip.Doc.">
									<HeaderStyle HorizontalAlign="Center" Width="55px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
									<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
									<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<asp:label id="lbTO_YENE" runat="server" CssClass="texto9pt">TOTAL YENES</asp:label>
						<asp:datagrid id="dgTYENE" runat="server" CssClass="gv" Width="300px" AutoGenerateColumns="False"
							Height="30px" BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
									<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
									<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid><BR>
						<BR>
						<BR>
						<INPUT class="btn" id="btnCerrar" onclick="window.close();" type="button" value="Cerrar"
							name="btnCerrar" style="WIDTH: 80px">&nbsp;<INPUT class="btn" id="btnImprimir" style="WIDTH: 80px" onclick="window.print();" type="button"
							value="Imprimir" name="btnImprimir">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
