Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class PoputDocRetiroIngreso
    Inherits System.Web.UI.Page
    Private objWMS As New clsCNWMS
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objAcceso As Library.AccesoDB.Acceso = New Library.AccesoDB.Acceso
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCodProd As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroLote As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCantidad As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnActualizar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblNroItem As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDescripcion As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            If Page.IsPostBack = False Then
                Me.lblNroItem.Text = Request.QueryString("sNU_ITEM")
                dtIngreso001 = Session.Item("dtIngreso001")
                If Me.lblNroItem.Text <> "" Then
                    For j As Integer = 0 To dtIngreso001.Rows.Count - 1
                        If dtIngreso001.Rows(j)("NU_ITEM") = Me.lblNroItem.Text Then
                            Me.txtCodProd.Text = dtIngreso001.Rows(j)("CO_PROD")
                            Me.txtDescripcion.Text = dtIngreso001.Rows(j)("DE_PROD")
                            Me.txtCantidad.Text = dtIngreso001.Rows(j)("NU_CANT")
                            Me.txtNroLote.Text = dtIngreso001.Rows(j)("NU_LOTE")
                        End If
                    Next
                    Me.btnActualizar.Visible = True
                Else
                    Me.btnGrabar.Visible = True
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Property dtIngreso001() As DataTable
        Get
            Return (ViewState("dtIngreso001"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtIngreso001") = Value
        End Set
    End Property

    'Private Sub crearSessionTabla()
    '    Dim dtIngreso As New DataTable("Ingreso")
    '    dtIngreso.Rows.Clear()
    '    Dim Column0 As New DataColumn("NU_ITEM")
    '    Column0.DataType = GetType(String)
    '    Dim Column1 As New DataColumn("NU_REFE")
    '    Column1.DataType = GetType(String)
    '    Dim Column2 As New DataColumn("CO_PROD")
    '    Column2.DataType = GetType(String)
    '    Dim Column3 As New DataColumn("DE_PROD")
    '    Column3.DataType = GetType(String)
    '    Dim Column4 As New DataColumn("NU_CANT")
    '    Column4.DataType = GetType(String)
    '    Dim Column5 As New DataColumn("FE_LLEG")
    '    Column5.DataType = GetType(String)
    '    Dim Column6 As New DataColumn("NU_LOTE")
    '    Column6.DataType = GetType(String)

    '    dtIngreso.Columns.Add(Column0)
    '    dtIngreso.Columns.Add(Column1)
    '    dtIngreso.Columns.Add(Column2)
    '    dtIngreso.Columns.Add(Column3)
    '    dtIngreso.Columns.Add(Column4)
    '    dtIngreso.Columns.Add(Column5)
    '    dtIngreso.Columns.Add(Column6)

    '    Dim dr As DataRow
    '    dtIngreso.Rows.Clear()
    '    Session.Add("dtIngreso001", dtIngreso)
    'End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        'Dim dtIngreso As DataTable ' New DataTable("Ingreso")
        Try
            Dim strDescripcion As String
            strDescripcion = objWMS.gCNValidaProducto(Session.Item("IdSico"), Me.txtCodProd.Text)
            If strDescripcion = "" Then
                Me.lblMensaje.Text = "El c�digo ingresado no existe favor enviar la informaci�n a Alma Per�"
                Return
            End If
            If Me.txtCantidad.Text = "" Then
                Me.lblMensaje.Text = "Debe ingresar una cantidad"
                Return
            End If
            dtIngreso001 = Session.Item("dtIngreso001")
            'Dim iItemMax As Integer = 0
            Dim dr As DataRow
            dr = dtIngreso001.NewRow
            dr(0) = dtIngreso001.Rows.Count + 1
            dr(1) = (Me.txtCodProd.Text).ToUpper
            dr(2) = strDescripcion
            dr(3) = Me.txtCantidad.Text
            dr(4) = (Me.txtNroLote.Text).ToUpper
            dtIngreso001.Rows.Add(dr)
            Session.Add("dtIngreso001", dtIngreso001)
            Me.lblNroItem.Text = dtIngreso001.Rows.Count + 1
            Me.lblMensaje.Text = "Se grab� correctamente"
            Me.btnGrabar.Visible = False
            Me.btnActualizar.Visible = True
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        'Dim dtIngreso As DataTable 'New DataTable("Ingreso")
        dtIngreso001 = Session.Item("dtIngreso001")
        For j As Integer = 0 To dtIngreso001.Rows.Count - 1
            If dtIngreso001.Rows(j)("NU_ITEM") = Me.lblNroItem.Text Then
                dtIngreso001.Rows(j)(1) = (Me.txtCodProd.Text).ToUpper
                dtIngreso001.Rows(j)(2) = (Me.txtDescripcion.Text).ToUpper
                dtIngreso001.Rows(j)(3) = Me.txtCantidad.Text
                dtIngreso001.Rows(j)(4) = (Me.txtNroLote.Text).ToUpper
            End If
        Next
        Session.Add("dtIngreso001", dtIngreso001)
        Me.lblMensaje.Text = "Se actualiz� correctamente"
    End Sub
End Class
