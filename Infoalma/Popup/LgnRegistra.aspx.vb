Imports System.Data

Public Class LgnRegistra
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtEmpresa As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTelefono As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtRUC As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAnexo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkAlmacenes As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkWarrants As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkFiles As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator3 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Page.IsPostBack = False Then
            Bindatagrid()
        End If
    End Sub

    Private Sub Bindatagrid()
        Dim dt As New DataTable
        Dim drComprometidos As DataRow
        Dim i As Integer = 1
        dt.Columns.Add(New DataColumn("Nro", GetType(Integer)))
        While i < 6
            drComprometidos = dt.NewRow
            drComprometidos("Nro") = i
            dt.Rows.Add(drComprometidos)
            i += 1
        End While
        Me.dgResultado.DataSource = dt
        Me.dgResultado.DataBind()
    End Sub

    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Try
            Me.lblMensaje.Text = ""
            If Me.txtEmpresa.Text = "" Then
                Me.lblMensaje.Text = "Ingrese empresa"
            ElseIf Me.txtRUC.Text = "" Then
                Me.lblMensaje.Text = "Ingrese RUC"
            ElseIf Me.txtTelefono.Text = "" Then
                Me.lblMensaje.Text = "Ingrese tel�fono"
            Else
                Dim objFuncion As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
                Dim strDe As String = System.Configuration.ConfigurationManager.AppSettings("EmailDepsa")
                Dim strPara As String = System.Configuration.ConfigurationManager.AppSettings("CorreoComercial")
                objFuncion.SendMail(strDe, strPara, "Solicitud de acceso al sistema AFI: " & Me.txtEmpresa.Text & " - " & Me.txtRUC.Text, Detalle, "")
                Response.Write("<script language='javascript'>alert('Su solicitud de acceso ser� atendida en los pr�ximos minutos');</script>")
                Me.txtAnexo.Text = ""
                Me.txtEmpresa.Text = ""
                Me.txtRUC.Text = ""
                Me.txtTelefono.Text = ""
                Me.chkAlmacenes.Checked = False
                Me.chkWarrants.Checked = False
                Bindatagrid()
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Function Detalle() As String
        Dim strBody As String
        'Dim strCampo As String
        'Dim dtContactos1 As DataTable
        'Dim dtDirecciones1 As DataTable
        Dim dgItem As DataGridItem
        Try
            strBody = "<FONT size='2' face='Arial, Tahoma' color='#003399'>Empresa - RUC : " & Me.txtEmpresa.Text & " - " & Me.txtRUC.Text & "</FONT><br>"
            strBody += "<FONT size='2' face='Arial, Tahoma' color='#003399'>Tel�fono - Anexo : " & Me.txtTelefono.Text & " - " & Me.txtAnexo.Text & "</FONT><br><br>"
            strBody += "<TABLE id='Table4' cellPadding='0' width='800px' border='1' style='FONT-SIZE: 11px; COLOR: #003399; FONT-FAMILY: Arial, Tahoma'>"
            strBody += "<TR bgColor='gainsboro' style='FONT-WEIGHT: bold'><TD>Nombres</TD><TD>Apellidos</TD><TD>DNI</TD><TD>Cargo</TD><TD>E-mail</TD></TR>"
            For j As Integer = 0 To Me.dgResultado.Items.Count - 1
                dgItem = Me.dgResultado.Items(j)
                strBody += "<TR><TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("txtNombre"), TextBox).Text & "</TD>"
                strBody += "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("txtApellidos"), TextBox).Text & "</TD>"
                strBody += "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("txtDNI"), TextBox).Text & "</TD>"
                strBody += "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("txtCargo"), TextBox).Text & "</TD>"
                strBody += "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("txtEmail"), TextBox).Text & "</TD></TR>"
            Next
            strBody += "</TABLE><br>"
            strBody += "<FONT size='2' face='Arial, Tahoma' color='#003399'>�reas de inter�s : </FONT><br>"

            If Me.chkAlmacenes.Checked = True Then
                strBody += "<FONT size='2' face='Arial, Tahoma' color='#003399'>- M�dulo de Almacenes</FONT><br>"
            End If
            If Me.chkWarrants.Checked = True Then
                strBody += "<FONT size='2' face='Arial, Tahoma' color='#003399'>- M�dulo de Warrants</FONT><br>"
            End If
            'If Me.chkFiles.Checked = True Then
            '    strBody += "<FONT size='2' face='Arial, Tahoma' color='#003399'>- M�dulo de Files</FONT><br>"
            'End If
            Return strBody
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Function
End Class
