Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class AlmFacturaDetalle
    Inherits System.Web.UI.Page
    Dim objCL_CLIE As clsCNFactura
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblNroDCR As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "DETALLE_FACTURA") Then
            Session.LCID = 1033
            If Page.IsPostBack = False Then
                pr_CARG_DETA()
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_CARG_DETA()
        Try
            Me.lblNroDCR.Text = Request.QueryString("sNU_COMP")
            pr_MOVI_INVE()
        Catch e1 As Exception
            Dim scA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub pr_MOVI_INVE()
        Try
            objCL_CLIE = New clsCNFactura
            Dim dsTMSALD_ALMA As DataSet
            dsTMSALD_ALMA = objCL_CLIE.fn_GetDetalleDCR(Session("CoEmpresa"), Me.lblNroDCR.Text)
            Dim dvTMSALD_ALMA As DataView = dsTMSALD_ALMA.Tables(0).DefaultView

            dgResultado.DataSource = dvTMSALD_ALMA
            Me.dgResultado.DataBind()
            If Me.dgResultado.Items.Count = 0 Then
                Me.lblError.Text = "No se encontr� Registros"
            End If
        Catch e1 As Exception
            Dim scA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(12).Text = String.Format("{0:##,##0.00}", Math.Round(CDec(e.Item.DataItem("ENTRADA (UNIDADES)") * e.Item.DataItem("IM_UNIT")), 2))
                e.Item.Cells(13).Text = String.Format("{0:##,##0.00}", Math.Round(CDec(e.Item.DataItem("SALIDA (UNIDADES)") * e.Item.DataItem("IM_UNIT")), 2))
                e.Item.Cells(14).Text = String.Format("{0:##,##0.00}", Math.Round(CDec(e.Item.DataItem("SALDO (UNIDADES)") * e.Item.DataItem("IM_UNIT")), 2))
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objCL_CLIE Is Nothing) Then
            objCL_CLIE = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
