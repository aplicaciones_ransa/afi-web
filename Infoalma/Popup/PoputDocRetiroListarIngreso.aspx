<%@ Page Language="vb" AutoEventWireup="false" CodeFile="PoputDocRetiroListarIngreso.aspx.vb" Inherits="PoputDocRetiroListarIngreso" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle de Ingreso</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<script language="javascript" src="../JScripts/Validaciones.js"></script>
		<script language="javascript">

										
			function ValidaFecha(sFecha)
			{
				var fecha1 = sFecha
				var fecha2 = document.getElementById('txtFechaInicial').value
				var miFecha1 = new Date(fecha1.substr(6,4), fecha1.substr(3,2), fecha1.substr(0,2))
				var miFecha2 = new Date(fecha2.substr(6,4), fecha2.substr(3,2), fecha2.substr(0,2)) 
				var diferencia = (miFecha1.getTime() - miFecha2.getTime())/(1000*60*60*24)
				//alert (diferencia);

				if (diferencia > 1540){
					window.open("Popup/SolicitarInformacion.aspx","Vencimientos","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
					return false;
				}
				else
				{
					return true;
				}
			}
			
			function ValidarFechas()
			{
			var fchIni;
			var fchFin;
			
			fchIni = document.getElementById("txtFechaInicial").value;
			fchFin = document.getElementById("txtFechaFinal").value;
			
			fchIni = fchIni.substr(6,4) + fchIni.substr(3,2) + fchIni.substr(0,2);
			fchFin = fchFin.substr(6,4) + fchFin.substr(3,2) + fchFin.substr(0,2);
			if (fchIni == "" && fchFin == "")
			{
				alert("Debe Ingresar fecha Inicial y Final");
				return false;
			}
				
			if(fchIni > fchFin && fchIni != "" && fchFin != "")
			{
				alert("La fecha inicial no puede ser mayor a la fecha final");
				return false;
			}
				return true;
			}
			
			function dois_pontos(tempo){
			if(event.keyCode<48 || event.keyCode>57){
				event.returnValue=false;}
			if(tempo.value.length==2){
				tempo.value+=":";}
			}
			
			 function ValidNum(e) { var tecla= document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46);}
					
					
		function isNumberOrLetter(evt) 
		{
		   var charCode = (evt.which) ? evt.which : event.keyCode;
		   if ((charCode > 65 && charCode < 91) || (charCode > 97 && charCode < 123) || (charCode > 47 && charCode < 58) )
		   return true;  
		   return false;
		}
		
		function isNumberOrLetterYGuion(evt) 
		{
		   var charCode = (evt.which) ? evt.which : event.keyCode;
		   if ((charCode > 65 && charCode < 91) || (charCode > 97 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode == 45 ))
		   return true;  
		   return false;
		}
							
		</script>
		<style type="text/css">
		.txtMayuscula { TEXT-ALIGN: left; TEXT-TRANSFORM: uppercase; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #003366; FONT-SIZE: 11px; FONT-WEIGHT: normal }
		</style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="10" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="687">
				<TR>
					<TD class="td" onkeyup="this.value = this.value.toUpperCase();" align="center">Detalle 
						de Ingreso</TD>
				</TR>
				<TR>
					<TD align="center">
						<asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="600px" BorderColor="Gainsboro"
							AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnPageIndexChanged="Change_Page">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ORDEN" HeaderText="Nro.Gu&#237;a">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CO_PROD" HeaderText="Cod.Producto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_PROD" HeaderText="Descripci&#243;n"></asp:BoundColumn>
								<asp:BoundColumn DataField="LOTE" HeaderText="Lote"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_CANT" HeaderText="Cantidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="PESO" HeaderText="Unidad Medida">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ALMACEN" HeaderText="Almac&#233;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FE_LLEG" HeaderText="Fecha LLegada">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FLG_ORDER" HeaderText="Order" Visible="False">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center">
						<asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:button><input class="Text" onclick="Javascript:window.close();" value="Cerrar" type="button" style="WIDTH: 80px"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
