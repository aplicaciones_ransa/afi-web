<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SolicitarInformacion.aspx.vb" Inherits="SolicitarInformacion"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SolicitarInformacion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="400" border="0">
				<TR>
					<TD class="td">Solicitud de Información</TD>
				</TR>
				<TR>
					<TD class="Text">
						Para solicitar Información histórica llene y envie este formulario.</TD>
				</TR>
				<TR>
					<TD align="center">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Text" width="13%">Asunto:</TD>
								<TD width="87%">
									<asp:TextBox id="txtAsunto" runat="server" Width="250px" CssClass="Text"></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtAsunto"></asp:RequiredFieldValidator></TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">Detalle:</TD>
								<TD>
									<asp:TextBox id="txtDetalle" runat="server" CssClass="Text" Width="320px" TextMode="MultiLine"
										Height="100px"></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtDetalle" ErrorMessage="*"></asp:RequiredFieldValidator></TD>
							</TR>
						</TABLE>
						<asp:Label id="lblError" runat="server" CssClass="error"></asp:Label>
					</TD>
				</TR>
				<TR>
					<TD align="center">
						<asp:Button id="btnEnviar" runat="server" CssClass="btn" Width="80px" Text="Enviar"></asp:Button><INPUT class="btn" id="btnCerrar" style="WIDTH: 80px" onclick="javascript:window.close()"
							type="button" value="Cerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
