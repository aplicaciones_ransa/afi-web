<%@ Page Language="vb" AutoEventWireup="false" CodeFile="poputRecordarClave.aspx.vb" Inherits="poputRecordarClave" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Recordar Contrase�a</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
	</HEAD>
	<body bottomMargin="0" leftMargin="15" rightMargin="0" topMargin="20" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 776px; HEIGHT: 368px" id="Table20" border="0" cellSpacing="0" cellPadding="0"
				width="776">
				<TR>
					<TD height="6" background="../Images/table_r1_c1.gif" width="6"></TD>
					<TD height="6" background="../Images/table_r1_c2.gif"></TD>
					<TD height="6" background="../Images/table_r1_c3.gif" width="6"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 338px" background="../Images/table_r2_c1.gif" width="6"></TD>
					<TD align="center"><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->  <!--</div>-->
						<TABLE id="Table3" border="0" cellSpacing="4" cellPadding="0">
							<TR>
								<TD class="td" colSpan="3">
									<P><asp:label id="Label2" runat="server" CssClass="Titulo">�Olvid� su contrase�a?</asp:label></P>
								</TD>
							</TR>
							<TR>
								<TD class="td" colSpan="3">
									<P align="center"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" Width="576px" CssClass="subtitulo">Para recuperar tu contrase�a de forma f�cil y segura, completa correctamente los datos que se  piden a continuaci�n. En caso no recuerdes tu contrase�a ni tu pregunta secreta, solicita una nueva contrase�a o comunicate con nosotros.</asp:label></P>
								</TD>
							</TR>
							<TR>
								<TD style="Z-INDEX: 0" vAlign="top"><asp:panel style="Z-INDEX: 0" id="Panel1" runat="server" Width="244px" BorderColor="Gainsboro"
										Height="168px" BorderStyle="Outset">
										<TABLE id="Table2" border="0" cellPadding="0" width="235">
											<TR>
												<TD style="Z-INDEX: 0">
													<P class="td" align="center">&nbsp;1) Ingresar usuario</P>
												</TD>
											</TR>
											<TR>
												<TD style="Z-INDEX: 0" class="Text" vAlign="top" align="left">Tu&nbsp;usuario&nbsp;:</TD>
											</TR>
											<TR>
												<TD class="td" vAlign="top">
													<asp:textbox style="Z-INDEX: 0" id="txtUsuario" onkeyup="this.value = this.value.toUpperCase();"
														runat="server" Width="220px" CssClass="Text" MaxLength="15"></asp:textbox></TD>
											</TR>
											<TR>
												<TD height="134" align="center">
													<asp:label style="Z-INDEX: 0" id="lblMensaje1" runat="server" CssClass="error"></asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:button style="Z-INDEX: 0" id="btnContinuar" runat="server" CssClass="Text" Text="Continuar"></asp:button>
													<asp:Button style="Z-INDEX: 0" id="btnCancelar" runat="server" CssClass="Text" Text="Cancelar"></asp:Button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
								<TD vAlign="top"><asp:panel style="Z-INDEX: 0" id="Panel2" runat="server" BorderColor="Gainsboro">
										<TABLE id="Table5" border="0" cellPadding="0" width="234">
											<TR>
												<TD style="Z-INDEX: 0">
													<P class="td" align="center">&nbsp; 2) Pregunta y respuesta secreta</P>
												</TD>
											</TR>
											<TR>
												<TD style="Z-INDEX: 0" class="Text" align="left">Tu pregunta&nbsp;de 
													seguridad&nbsp;:</TD>
											</TR>
											<TR>
												<TD class="td">
													<asp:textbox style="Z-INDEX: 0" id="txtPregSegu" runat="server" Width="220px" CssClass="Text"></asp:textbox></TD>
											</TR>
											<TR>
												<TD style="Z-INDEX: 0" class="Text">Tu respuesta secreta :</TD>
											</TR>
											<TR>
												<TD class="td">
													<asp:textbox style="Z-INDEX: 0" id="txtResSecre" onkeyup="this.value = this.value.toUpperCase();"
														runat="server" Width="220px" CssClass="Text" MaxLength="200" TextMode="Password"></asp:textbox></TD>
											</TR>
											<TR>
												<TD align="left">
													<asp:CheckBox id="CheckClaveAcceso" runat="server" CssClass="Text" Text="Enviar clave de acceso"></asp:CheckBox></TD>
											</TR>
											<TR>
												<TD align="left">
													<asp:CheckBox id="CheckClaveFirma" runat="server" CssClass="Text" Text="Enviar clave de firma"
														Enabled="False"></asp:CheckBox></TD>
											</TR>
											<TR>
												<TD height="50" align="center">
													<asp:label style="Z-INDEX: 0" id="lblMensaje2" runat="server" CssClass="error"></asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:button style="Z-INDEX: 0" id="btnContinuar1" runat="server" CssClass="Text" Text="Continuar"></asp:button>
													<asp:button style="Z-INDEX: 0" id="btnRegresar" runat="server" CssClass="Text" Text="Regresar"></asp:button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
								<TD vAlign="top"><asp:panel style="Z-INDEX: 0" id="Panel3" runat="server" BorderColor="Gainsboro">
										<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0" width="234">
											<TR>
												<TD style="Z-INDEX: 0; HEIGHT: 17px">
													<P class="td" align="center">3 )&nbsp;Listo</P>
												</TD>
											</TR>
											<TR>
												<TD height="164" align="center">
													<asp:label style="Z-INDEX: 0" id="lblMensaje3" runat="server" CssClass="error"></asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:button style="Z-INDEX: 0" id="btnCerrar" runat="server" CssClass="Text" Text="Cerrar"></asp:button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD background="../Images/table_r2_c3.gif" width="6"></TD>
				</TR>
				<TR>
					<TD height="6" background="../Images/table_r3_c1.gif" width="6"></TD>
					<TD height="6" background="../Images/table_r3_c2.gif"></TD>
					<TD height="6" background="../Images/table_r3_c3.gif" width="6"></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
