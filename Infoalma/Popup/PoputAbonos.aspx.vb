Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Root.Reports
'Imports System.IO

Public Class PoputAbonos
    Inherits System.Web.UI.Page
    Dim sCO_UNID, sTI_DOCU, sNU_DOCU As String
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    Dim objCCorrientes As clsCNCCorrientes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Bindatagrid()
        End If
    End Sub

    Private Sub Bindatagrid()
        Dim dt As New DataTable
        Try
            Me.lblMensaje.Text = ""
            objCCorrientes = New clsCNCCorrientes
            dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), Request.QueryString("sCO_UNID"), Request.QueryString("sTI_DOCU_COBR"), Request.QueryString("sNU_DOCU_COBR"))

            If dt.Rows.Count <> 0 Then
                Me.dgResultado.DataSource = dt
                Me.dgResultado.DataBind()
            Else
                Me.lblMensaje.Text = "Datos no encontrados"
            End If

        Catch ex As Exception
            lblMensaje.Text = ex.Message.ToString
        End Try
    End Sub
    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Bindatagrid()
    End Sub

    'Protected Overrides Sub Finalize()
    '    If Not (objCCorrientes Is Nothing) Then
    '        objCCorrientes = Nothing
    '    End If
    '    MyBase.Finalize()
    'End Sub
End Class
