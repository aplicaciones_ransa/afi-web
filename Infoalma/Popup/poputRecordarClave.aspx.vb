Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports Library.AccesoBL

Public Class poputRecordarClave
    Inherits System.Web.UI.Page
    Private objAcceso As Acceso = New Acceso
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMensaje1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    'Protected WithEvents txtPregSegu As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtResSecre As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje2 As System.Web.UI.WebControls.Label
    'Protected WithEvents btnContinuar1 As System.Web.UI.WebControls.Button
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnCancelar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtUsuario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnContinuar As System.Web.UI.WebControls.Button
    'Protected WithEvents Panel3 As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMensaje3 As System.Web.UI.WebControls.Label
    'Protected WithEvents btnCerrar As System.Web.UI.WebControls.Button
    'Protected WithEvents CheckClaveFirma As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents CheckClaveAcceso As System.Web.UI.WebControls.CheckBox
    Dim dt As New DataTable
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Me.btnCancelar.Attributes.Add("onclick", "Javascript:window.close();")
            Me.btnCerrar.Attributes.Add("onclick", "Javascript:window.close();")
            DesHabilitarControles()
            Me.txtUsuario.Enabled = True
            Me.btnContinuar.Enabled = True
            Me.btnCancelar.Enabled = True
            Panel1.Enabled = True
            Panel1.BorderStyle = BorderStyle.Outset
        End If
    End Sub
    Private Sub DesHabilitarControles()
        '========= panel1 ===========
        Me.txtUsuario.Enabled = False
        Me.btnContinuar.Enabled = False
        Me.btnCancelar.Enabled = False
        Panel1.Enabled = False
        Panel1.BorderStyle = BorderStyle.NotSet
        '========= panel2 ===========
        Me.txtPregSegu.Enabled = False
        Me.txtResSecre.Enabled = False
        Me.btnContinuar1.Enabled = False
        Me.btnRegresar.Enabled = False
        Me.CheckClaveFirma.Enabled = False
        Me.btnRegresar.Enabled = False
        Panel2.Enabled = False
        Panel2.BorderStyle = BorderStyle.NotSet
        '========= panel3 ===========
        Me.btnCerrar.Enabled = False
        Panel3.Enabled = False
        Panel3.BorderStyle = BorderStyle.NotSet
    End Sub
    Private Sub LimpiarControles()
        '========= panel1 ===========
        Me.txtUsuario.Text = ""
        Me.lblMensaje1.Text = ""
        '========= panel2 ===========
        Me.txtPregSegu.Text = ""
        Me.txtResSecre.Text = ""
        Me.lblMensaje2.Text = ""
        Me.CheckClaveAcceso.Checked = False
        Me.CheckClaveFirma.Checked = False
        '========= panel3 ===========
        Me.lblMensaje3.Text = ""
    End Sub
    Private Sub LimpiarMensajes()
        Me.lblMensaje1.Text = ""
        Me.lblMensaje2.Text = ""
        Me.lblMensaje3.Text = ""
    End Sub
    Private Sub btnContinuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar.Click
        Try
            Dim dt1 As New DataTable
            Dim dt2 As New DataTable
            If Me.txtUsuario.Text <> "" Then
                dt1 = objAcceso.gCADValidarUsuario(Me.txtUsuario.Text)
                If dt1.Rows.Count <> 0 Then
                    DesHabilitarControles()
                    LimpiarMensajes()
                    '======= habilitando controles del Panel2 ==============
                    Me.txtResSecre.Enabled = True
                    Me.btnContinuar1.Enabled = True
                    Me.btnRegresar.Enabled = True
                    Panel2.Enabled = True
                    Panel2.BorderStyle = BorderStyle.Outset
                    '======= Mandamos Respuesta secreta Encriptada Por Session y ==============
                    dt2 = objAcceso.gCADListarRespuestaSecreta(Me.txtUsuario.Text)
                    If dt2.Rows.Count <> 0 Then
                        Me.txtPregSegu.Text = CType(dt2.Rows(0)("DSC_PGTSECR"), String).ToUpper
                        Session.Add("ssRPT_SECR01", CType(dt2.Rows(0)("RPT_SECR"), String).Trim)
                        Session.Add("ssCOD_USER01", CType(dt2.Rows(0)("COD_USER"), String).Trim)
                        Session.Add("ssDIR_EMAI01", CType(dt2.Rows(0)("DIR_EMAI"), String).Trim)
                        If dt2.Rows(0)("EXISTE_USR_PASFIRM") = "TRUE" Then
                            Me.CheckClaveFirma.Enabled = True
                        Else
                            Me.CheckClaveFirma.Enabled = False
                        End If
                        Me.CheckClaveAcceso.Checked = True
                    Else
                        Me.lblMensaje2.Text = "Usted no tiene registrada una pregunta y respuesta secreta." _
                                            & vbCrLf & "por favor ll�manos al 518 6060" _
                                            & vbCrLf & " para actualizar tus datos."
                        Me.txtResSecre.Enabled = False
                        Me.btnContinuar1.Enabled = False
                    End If
                Else
                    Me.lblMensaje1.Text = "Por favor ingrese un  usuario  v�lido. " _
                    & vbCrLf & "Si el problema persiste, por favor ll�manos" _
                    & vbCrLf & "al 518 6060 para actualizar tus datos."
                End If
            Else
                Me.lblMensaje1.Text = "Ingrese usuario"
            End If
        Catch ex As Exception
            Me.lblMensaje1.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Private Sub btnContinuar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinuar1.Click
        Try
            If Me.txtResSecre.Text <> "" Then
                LimpiarMensajes()
                '======= Encriptar Respuesta Ingresada ==============
                Dim ResSecreIng As String
                ResSecreIng = Library.AccesoBL.Funciones.EncryptString(Me.txtResSecre.Text.Trim, "���_[]0")
                '======= Comparar ==============
                If ResSecreIng = Session.Item("ssRPT_SECR01") Then
                    '======= deshabilitando controles  ==============
                    DesHabilitarControles()
                    '======= Enviar Contrase�a ==============
                    Dim dt As DataTable = New DataTable
                    Dim strContrase�a As String
                    Dim strUsuario As String
                    Dim correoEnmascarado As String
                    Try
                        dt = objUsuario.gGetUsuario(Session.Item("ssCOD_USER01"))
                        strContrase�a = dt.Rows(0)("USR_PASW")
                        strUsuario = dt.Rows(0)("USR_ACCE")
                        strContrase�a = Funciones.DecryptString(strContrase�a, "���_[]0")
                        '======= Enviar email ==============			
                        If Me.CheckClaveAcceso.Checked = True Then
                            EnvioMail(strUsuario, strContrase�a)
                        End If
                        If Me.CheckClaveFirma.Checked = True Then
                            ResetearClaveFirma()
                        End If
                        '======= Mostrar Mensaje de confirmacion  ==============
                        correoEnmascarado = enmascararCorreo(Session.Item("ssDIR_EMAI01"))
                        Me.lblMensaje3.Text = "<span style='color: #003466; font-family: Arial, Helvetica, sans-serif;font-size: 11; font-weight: normal;'>" _
                                  & vbCrLf & "�Tu solicitud ha sido procesada  con �xito!" _
                                  & vbCrLf & "te  hemos  enviado  tu  contrase�a al" _
                                  & vbCrLf & "correo: " & correoEnmascarado & ". Te" _
                                  & vbCrLf & "sugerimos  revisar  tambi�n,  tu  bandeja  de" _
                                  & vbCrLf & "correos  no  deseados.  En  caso  no  lo" _
                                  & vbCrLf & "encuentres  o  consideres  que  el  correo" _
                                  & vbCrLf & "mostrado no es el actual, por favor ll�manos" _
                                  & vbCrLf & "al 518 6060" & " para actualizar " _
                                  & vbCrLf & "tus datos.</span>"
                        Panel3.Enabled = True
                        Panel3.BorderStyle = BorderStyle.Outset
                        Me.btnCerrar.Enabled = True
                    Catch ex As Exception
                        Me.lblMensaje3.Text = ex.Message
                    Finally
                        dt.Dispose()
                        dt = Nothing
                    End Try
                Else
                    Me.lblMensaje2.Text = "La respuesta ingresada no es correcta. Por favor," _
                                  & vbCrLf & "intenta de nuevo. Si no recuerdas la repuesta," _
                                  & vbCrLf & "por favor ll�manos al 518 6060 para actualizar" _
                                  & vbCrLf & "tus datos."
                End If
            Else
                Me.lblMensaje2.Text = "Ingrese respuesta secreta"
            End If
        Catch ex As Exception
            Me.lblMensaje2.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Sub ResetearClaveFirma()
        Dim strResult As String
        Dim strNombre As String
        Dim strTipo As String
        Dim dr As DataRow
        Dim strContrase�a As String
        Dim strEncriptado As String
        Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))
        'generando contrase�a Aleatoria desde el 100000 al 999999
        strContrase�a = (objRandom.Next(100000, 999999 + 1)).ToString
        strEncriptado = Funciones.EncryptString(strContrase�a, "���_[]0")
        strResult = objUsuario.gUpdClaveFirma(strEncriptado, Session.Item("ssCOD_USER01"), Session.Item("ssCOD_USER01"))
        lblMensaje2.Text = strResult
        '======= Enviar email ==============
        EnvioMailReseteo(Me.txtUsuario.Text, strContrase�a)
    End Sub
    Private Sub EnvioMail(ByVal strLogin As String, ByVal strContrase�a As String)
        objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", Session.Item("ssDIR_EMAI01"), "Contrase�a de Acceso", "Datos de acceso al Sistema AFI " &
                         ":<br><br>Usuario: " & strLogin & "<br>Contrase�a: " & strContrase�a & "<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Sistema AFI</A>" &
                         "<br>Cualquier consulta sobre la aplicaci�n escribir a soporte@almaperu.com.pe", "")
    End Sub
    Private Sub EnvioMailReseteo(ByVal strLogin As String, ByVal strContrase�a As String)
        objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", Session.Item("ssDIR_EMAI01"), "Clave para firmar", "Estimado(a) sr(a).  " &
                           strLogin.ToLower & "  su clave para firmar los documentos es:  " & strContrase�a & "<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Sistema AFI</A>" &
                         "<br>Cualquier consulta sobre la aplicaci�n escribir a soporte@almaperu.com.pe", "")
    End Sub
    Private Function enmascararCorreo(ByVal correo As String) As String
        Dim correoEnmascarado As String
        Dim varCortar As String
        Dim varRemplazar As String
        Dim varxxx As String = ""
        Dim varNroRemplazar As Integer
        Dim i As Integer = 0
        Dim split As String() = correo.Split(New [Char]() {"@"c})
        varCortar = split.GetValue(0)
        varNroRemplazar = (Len(varCortar) - 1)
        While i < varNroRemplazar
            varxxx = varxxx + "x"
            i += 1
        End While
        varRemplazar = Right(varCortar, varNroRemplazar)
        correoEnmascarado = correo.Replace(varRemplazar, varxxx)
        Return correoEnmascarado
    End Function
    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        DesHabilitarControles()
        LimpiarControles()
        '======= habilitando controles del Panel1 ==============
        Me.txtUsuario.Enabled = True
        Me.lblMensaje1.Text = ""
        Me.lblMensaje2.Text = ""
        Me.btnContinuar.Enabled = True
        Me.btnCancelar.Enabled = True
        Panel1.Enabled = True
        Panel1.BorderStyle = BorderStyle.Outset
    End Sub
End Class
