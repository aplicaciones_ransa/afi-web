Imports Library.AccesoDB
Public Class LgnSolicitaFirma
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents txtContrase�a As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblContrase�a As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNuevaContrase�a As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNuevaContrase�a As System.Web.UI.WebControls.TextBox
    'Protected WithEvents trNueva As System.Web.UI.HtmlControls.HtmlTableRow

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objAcceso As Acceso = New Acceso
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And (InStr(Session("PagId"), "1") Or InStr(Session("PagId"), "RETIROS_EMITIDO")) Then
            If Page.IsPostBack = False Then
                If objAcceso.gGetCaducaContrasena(Session.Item("IdUsuario")) = "C" Then
                    Me.lblNuevaContrase�a.Visible = True
                    Me.txtNuevaContrase�a.Visible = True
                    Me.lblContrase�a.Text = "Clave anterior"
                    Me.lblNuevaContrase�a.Text = "Clave nueva"
                Else
                    Me.lblNuevaContrase�a.Visible = False
                    Me.txtNuevaContrase�a.Visible = False
                    Me.lblContrase�a.Text = "Ingrese clave"
                End If
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub
End Class
