Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class CueFacturasDetalle
    Inherits System.Web.UI.Page
    Dim objFactura As clsCNFactura
    Dim objReporte As Library.AccesoBL.Reportes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMoneda As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCobrador As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRUC As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNombre As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDireccion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCobrar As System.Web.UI.WebControls.Label
    'Protected WithEvents lblReferencia As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrDetalle As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblMonto As System.Web.UI.WebControls.Label
    'Protected WithEvents lblVenta As System.Web.UI.WebControls.Label
    'Protected WithEvents lblIGV As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPorcentaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroFactura As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "DETALLE_FACTURA") Then
            Session.LCID = 1033
            Try
                If Page.IsPostBack = False Then
                    LLenaFactura()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = "No se pudo cargar la factura " & ex.Message
            End Try
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Sub LLenaFactura()
        objFactura = New clsCNFactura
        objReporte = New Library.AccesoBL.Reportes
        Dim dt As DataTable
        Dim dr As DataRow
        Dim strDetalle As String = ""
        Dim strQuiebre As String = ""
        dt = objFactura.fn_GetFactura(Session("CoEmpresa"), Request.QueryString("sCO_UNID"), _
        Session("CoGrup"), Request.QueryString("sTI_DOC"), Request.QueryString("sNU_FACT"))

        Me.lblCobrador.Text = IIf(IsDBNull(dt.Rows(0)("COBRADOR")), "", dt.Rows(0)("COBRADOR"))
        Me.lblCobrar.Text = IIf(IsDBNull(dt.Rows(0)("DIRECCION COBR.")), "", dt.Rows(0)("DIRECCION COBR."))
        Me.lblDireccion.Text = IIf(IsDBNull(dt.Rows(0)("DIRECCION FACT.")), "", dt.Rows(0)("DIRECCION FACT."))
        Me.lblFecha.Text = IIf(IsDBNull(dt.Rows(0)("FECHA")), "", dt.Rows(0)("FECHA"))
        Me.lblIGV.Text = String.Format("{0:##,##0.00}", IIf(IsDBNull(dt.Rows(0)("IMPUESTO")), "", dt.Rows(0)("IMPUESTO")))
        Me.lblMoneda.Text = IIf(IsDBNull(dt.Rows(0)("MONEDA")), "", dt.Rows(0)("MONEDA"))
        Me.lblMonto.Text = "SON: " & objReporte.NumPalabra(Val(CStr(IIf(IsDBNull(dt.Rows(0)("PRECIO VENTA")), "", dt.Rows(0)("PRECIO VENTA"))))).ToUpper & " " & IIf(IsDBNull(dt.Rows(0)("MONEDA")), "", dt.Rows(0)("MONEDA"))
        Me.lblNombre.Text = IIf(IsDBNull(dt.Rows(0)("CLIENTE")), "", dt.Rows(0)("CLIENTE"))
        Me.lblNroFactura.Text = IIf(IsDBNull(dt.Rows(0)("N� DOCUMENTO")), "", dt.Rows(0)("N� DOCUMENTO"))
        Me.lblPorcentaje.Text = CInt(IIf(IsDBNull(dt.Rows(0)("PORCENTAJE")), "", dt.Rows(0)("PORCENTAJE"))) & " %)"
        Me.lblReferencia.Text = IIf(IsDBNull(dt.Rows(0)("REFERENCIA AVISO")), "", dt.Rows(0)("REFERENCIA AVISO")) & " DEL " & IIf(IsDBNull(dt.Rows(0)("FECHA EMISION")), "", dt.Rows(0)("FECHA EMISION"))
        Me.lblRUC.Text = IIf(IsDBNull(dt.Rows(0)("COD. CLIENTE")), "", dt.Rows(0)("COD. CLIENTE"))
        Me.lblTotal.Text = String.Format("{0:##,##0.00}", IIf(IsDBNull(dt.Rows(0)("PRECIO VENTA")), "", dt.Rows(0)("PRECIO VENTA")))
        Me.lblVenta.Text = String.Format("{0:##,##0.00}", IIf(IsDBNull(dt.Rows(0)("VALOR VENTA")), "", dt.Rows(0)("VALOR VENTA")))
        strDetalle = "<table width='100%'>"

        For Each dr In dt.Rows
            If strQuiebre <> dr("QUIEBRE") Then
                strDetalle = strDetalle & "<tr><td class='Text' colspan=8>" & dr("SERVICIO BASE") & " - " & dr("SERVICIO COMPLEMENTARIO") & "</td></tr>"
                strQuiebre = dr("QUIEBRE")
            End If
            If strQuiebre = "WAR-OTI" Then
                strDetalle = strDetalle & "<tr><td class='Text' colspan=7>" & dr("GLOSA") & "</td>" & _
                                      "<td class='Text'>" & String.Format("{0:##,##0.00}", dr("IMPORTE DETALLE")) & "</td></tr>"
            Else
                strDetalle = strDetalle & "<tr><td class='Text'>" & dr("TIPO DOC. RECEPCION") & " " & dr("N� DOC. RECEPCION") & "</td>" & _
                                                      "<td class='Text'>" & dr("N� TITULO") & "</td>" & _
                                                      "<td class='Text'>" & dr("PERIODO COBR. DESDE") & "</td>" & _
                                                      "<td class='Text'>" & dr("PERIODO COBR. HASTA") & "</td>" & _
                                                      "<td class='Text'>" & dr("DESCRIPCION MERCADERIA") & "</td>" & _
                                                      "<td class='Text'>" & dr("UNID. MEDIDA TARIFA") & " " & String.Format("{0:##,##0.00}", dr("VALOR/CANTIDAD")) & "</td>" & _
                                                      "<td class='Text'>" & dr("TARIFA") & " %</td>" & _
                                                      "<td class='Text'>" & String.Format("{0:##,##0.00}", dr("IMPORTE DETALLE")) & "</td></tr>"
            End If

        Next
        Me.ltrDetalle.Text = strDetalle & "</table>"
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objFactura Is Nothing) Then
            objFactura = Nothing
        End If
        If Not (objReporte Is Nothing) Then
            objReporte = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
