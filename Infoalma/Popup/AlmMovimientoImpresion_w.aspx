<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmMovimientoImpresion_w.aspx.vb" Inherits="AlmMovimientoImpresion_w" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlmMovimientoImpresion</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="4" cellPadding="0" width="100%">
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="center"><asp:label id="lblTitulo" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="8pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD><asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD><asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD><asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="center">
						<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="683">
							<TR>
								<TD style="WIDTH: 97px"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">ALMACEN</asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label style="Z-INDEX: 0" id="Label14" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px" colSpan="7"><asp:label style="Z-INDEX: 0" id="lbDE_ALMA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 97px; HEIGHT: 10px"><asp:label id="Label11" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">TIP. MOVIMIENTO</asp:label></TD>
								<TD style="WIDTH: 7px; HEIGHT: 10px"><asp:label id="Label12" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px; HEIGHT: 10px"><asp:label id="lbTI_MOVI" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px; HEIGHT: 10px"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">TIPO REPORTE</asp:label></TD>
								<TD style="WIDTH: 6px; HEIGHT: 10px"><asp:label id="Label5" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="HEIGHT: 10px"><asp:label style="Z-INDEX: 0" id="lbTI_REPO" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="HEIGHT: 10px"></TD>
								<TD style="HEIGHT: 10px" width="1%"></TD>
								<TD style="HEIGHT: 10px"></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD width="16%"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">NRO. REFERENCIA</asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label id="Label13" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px"><asp:label style="Z-INDEX: 0" id="lbNU_REFE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"><asp:label style="Z-INDEX: 0" id="Label15" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> C�D. PRODUCTO </asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="lbCO_PROD" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD width="17%"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> DESCRIPCI�N MERC. </asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="Label17" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD><asp:label style="Z-INDEX: 0" id="lbDE_MERC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 97px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt"> LOTE </asp:label></TD>
								<TD style="WIDTH: 7px"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 182px"><asp:label style="Z-INDEX: 0" id="lbLOTE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px"><asp:label style="Z-INDEX: 0" id="Label6" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">FECHA</asp:label></TD>
								<TD style="WIDTH: 6px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" Font-Bold="True" Font-Names="verdana,arial"
										Font-Size="7pt">:</asp:label></TD>
								<TD colSpan="4"><asp:label style="Z-INDEX: 0" id="lbFE_INI_FIN" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="lbMS_REGI" runat="server" CssClass="text" ForeColor="Red"></asp:label><asp:datagrid style="Z-INDEX: 0" id="dgIngRes" runat="server" CssClass="gv" AutoGenerateColumns="False"
							Width="100%" HorizontalAlign="Center" BorderColor="Gainsboro" Visible="False" OnPageIndexChanged="Change_dgIngRes" Height="88px">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ALMACEN" HeaderText="Almac&#233;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ESTADO" HeaderText="Estado">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="dgIngDet" runat="server" CssClass="gv" AutoGenerateColumns="False"
							Width="100%" HorizontalAlign="Center" BorderColor="Gainsboro" Visible="False" OnPageIndexChanged="Change_dgIngDet"
							Height="88px">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ITEM" HeaderText="Item">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="COD_PRODUCTO" HeaderText="Cod.Producto">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="LOTE" HeaderText="Lote">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="MEDIDA" HeaderText="Medida">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CODIGO_RETENCION" HeaderText="Cod.Retencion">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_INGRESO" HeaderText="Fecha Ingreso">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_VENCIMIENTO" HeaderText="Fecha Vencimiento">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="FLG_ORDER" HeaderText="Order">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ALMACEN" HeaderText="Almac&#233;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CANTIDAD_TOTA" HeaderText="Cant.Total">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="dgRetRes" runat="server" CssClass="gv" AutoGenerateColumns="False"
							Width="100%" HorizontalAlign="Center" BorderColor="Gainsboro" Visible="False" OnPageIndexChanged="Change_dgRetRes">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD_SOLICITADA" HeaderText="Cant.Solicitada">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD_PREPARADA" HeaderText="Cant.Preparada">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="RETIRADO_POR" HeaderText="Retirado por">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CLIENTE" HeaderText="Cliente">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="dgRetDet" runat="server" CssClass="gv" AutoGenerateColumns="False"
							Width="100%" HorizontalAlign="Center" BorderColor="Gainsboro" Visible="False" OnPageIndexChanged="Change_dgRetDet">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CODIGO" HeaderText="Codigo">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="ITEM" HeaderText="Item">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD_SOLICITADA" HeaderText="Cant.Solicitada">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CANTIDAD_PREPARADA" HeaderText="Cant.Preparada">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="LOTE" HeaderText="Lote">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="RETIRADO_POR" HeaderText="Retirado por">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CLIENTE" HeaderText="Cliente">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="FLG_ORDER" HeaderText="Order">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ALMACEN" HeaderText="Almac&#233;n">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CANTIDAD_SOLICITADA_TOTAL">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="CANTIDAD_PREPARADA_TOTAL">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
								Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center"><INPUT id="btnImprimir" class="btn" onclick="window.print();" value="Imprimir" type="button"
							name="btnImprimir"> <INPUT style="WIDTH: 80px" id="btnCerrar" class="btn" onclick="window.close();" value="Cerrar"
							type="button" name="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
