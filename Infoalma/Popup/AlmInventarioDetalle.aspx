<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmInventarioDetalle.aspx.vb" Inherits="AlmInventarioDetalle"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>�rdenes de Retiro</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
			function Cerrar()
				{			
				window.close();
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="15" topMargin="20" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="500" border="0">
				<TR>
					<TD width="6" background="../Images/table_r1_c1.gif" height="6"></TD>
					<TD background="../Images/table_r1_c2.gif" height="6"></TD>
					<TD width="6" background="../Images/table_r1_c3.gif" height="6"></TD>
				</TR>
				<TR>
					<TD width="6" background="../Images/table_r2_c1.gif"></TD>
					<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="td" vAlign="top"></TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="td">N�mero DCR:
											</TD>
											<TD><asp:label id="lblNroDCR" runat="server" CssClass="Text"></asp:label></TD>
											<TD class="td">Item:</TD>
											<TD><asp:label id="lblNroItem" runat="server" CssClass="Text"></asp:label></TD>
										</TR>
										<TR>
											<TD width="50%" colSpan="2"><asp:radiobutton id="rbtCerrados" runat="server" GroupName="c" CssClass="Text" Text="DOR Cerrados"
													AutoPostBack="True" Checked="True"></asp:radiobutton></TD>
											<TD width="50%" colSpan="2"><asp:radiobutton id="rbtPendientes" runat="server" GroupName="c" CssClass="Text" Text="DOR Pendientes de Despacho"
													AutoPostBack="True"></asp:radiobutton></TD>
										</TR>
									</TABLE>
									<INPUT id="hiCO_UNID" type="hidden" runat="server">
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:datagrid id="dgResultado" runat="server" CssClass="gv" BorderColor="Gainsboro" HorizontalAlign="Center"
										Width="100%" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NU_DOCU_RETI" HeaderText="DOR"></asp:BoundColumn>
											<asp:BoundColumn DataField="NU_DOCU_AFRE" HeaderText="DOR Madre"></asp:BoundColumn>
											<asp:BoundColumn DataField="FE_DOCU_RETI" HeaderText="Fec.Retiro" DataFormatString="{0:d}"></asp:BoundColumn>
											<asp:BoundColumn DataField="FE_SALI_ALMA" HeaderText="Fec.Cont" DataFormatString="{0:d}"></asp:BoundColumn>
											<asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Cant." DataFormatString="{0:N2}"></asp:BoundColumn>
											<asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Peso" DataFormatString="{0:N2}"></asp:BoundColumn>
											<asp:BoundColumn DataField="CO_MONE" HeaderText="Mon."></asp:BoundColumn>
											<asp:BoundColumn DataField="IM_ITEM_RETI" HeaderText="Importe" DataFormatString="{0:N2}"></asp:BoundColumn>
										</Columns>
										<PagerStyle CssClass="gvPager"></PagerStyle>
									</asp:datagrid>
									<asp:Label id="lblMensaje" runat="server" CssClass="error"></asp:Label></TD>
							</TR>
							<TR>
								<TD align="center"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
										name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:Cerrar();" type="button" value="Cerrar"
										name="btnCerrar"></TD>
							</TR>
						</TABLE> <!--</div>--></TD>
					<TD width="6" background="../Images/table_r2_c3.gif"></TD>
				</TR>
				<TR>
					<TD width="6" background="../Images/table_r3_c1.gif" height="6"></TD>
					<TD background="../Images/table_r3_c2.gif" height="6"></TD>
					<TD width="6" background="../Images/table_r3_c3.gif" height="6"></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
