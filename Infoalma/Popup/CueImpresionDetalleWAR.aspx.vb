Imports System.Data
Imports Depsa.LibCapaNegocio
'Imports Root.Reports
'Imports System.IO.Stream
'Imports System.IO

Public Class CueImpresionDetalleWAR
    Inherits System.Web.UI.Page
    Dim sCO_EMPR, sCO_UNID, sNU_DOC, sCO_CLIE As String
    Dim objCCorrientes As clsCNCCorrientes = New clsCNCCorrientes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim dtRepo As DataTable
            'Dim drPDF As DataRow
            'Dim objStream As Stream
            'Dim dd As String = ""
            dtRepo = objCCorrientes.gCADGetDocumentoDetalleWAR(Request.QueryString("sCO_EMPR"), Request.QueryString("sCO_UNID"), Request.QueryString("sNU_DOC"), Request.QueryString("sTI_DOCU"))
            'dtRepo = objCCorrientes.gCADGetDocumentoDetalleDOT("01", "001", "00000705602", "20109714039", "0090")

            If dtRepo.Rows.Count <> 0 Then
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(CType(dtRepo.Rows(0).Item("NO_REPO"), Byte()))
            Else
                Response.Write("No se encontr� el archivo")
            End If
            Response.Flush()
            Response.Close()
        Catch ex As Exception
            Dim MSG As String = ex.ToString()
            Response.Write("No se encontr� el archivo")
        End Try
        'If Not Page.IsPostBack Then
        '    GenerarPDF()
        'End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
