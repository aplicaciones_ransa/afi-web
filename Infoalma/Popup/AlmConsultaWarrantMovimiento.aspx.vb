Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class AlmConsultaWarrantMovimiento
    Inherits System.Web.UI.Page
    Private objWarrant As clsCNWarrant
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblValor As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "CONSULTA_WARRANT") Then
            If Page.IsPostBack = False Then
                BindDataGrid()
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub BindDataGrid()
        objWarrant = New clsCNWarrant
        Dim dtRetiros As DataTable
        Dim dvTMSALD_ALMA As DataView
        dtRetiros = objWarrant.gCNGetListarRetiros(Session.Item("CoEmpresa"), Request.QueryString("sTI_TITU"), Request.QueryString("sNU_TITU"))
        dvTMSALD_ALMA = dtRetiros.DefaultView
        dvTMSALD_ALMA.RowFilter = "[DOCUMENTO] IS NOT NULL "
        Me.dgResultado.DataSource = dvTMSALD_ALMA
        Me.dgResultado.DataBind()

        Me.lblNroWarrant.Text = IIf(Not IsDBNull(dtRetiros.Rows(0)("N�MERO WARRANT")), dtRetiros.Rows(0)("N�MERO WARRANT"), "")
        Me.lblValor.Text = IIf(IsDBNull(dtRetiros.Rows(0)("MONEDA")), "", dtRetiros.Rows(0)("MONEDA")) & " " & IIf(IsDBNull(dtRetiros.Rows(0)("VALOR ORIGEN")), "", FormatNumber(dtRetiros.Rows(0)("VALOR ORIGEN"), 2))
    End Sub
End Class
