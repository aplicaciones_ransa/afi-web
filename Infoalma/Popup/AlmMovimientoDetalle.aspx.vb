Imports Depsa.LibCapaNegocio
Imports System.IO
Imports Root.Reports
Imports System.Data

Public Class AlmMovimientoDetalle
    Inherits System.Web.UI.Page
    Dim objMovimiento As New clsCNMovimiento
    Private objAccesoWeb As clsCNAccesosWeb
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If (Request.IsAuthenticated) Then
                If Page.IsPostBack = False Then

                    Dim strModalidad As String
                    Dim dtTemporal As DataTable = objMovimiento.gCNReporteDCR(Session.Item("CoEmpresa"), Request.QueryString("sCO_UNID"), Request.QueryString("sTI_DOCU_RECE"), Request.QueryString("sNU_DOCU_RECE"))

                    'nuevo
                    Dim dt1 As DataTable
                    dt1 = objMovimiento.gCADGetDesModalidad(Session.Item("CoEmpresa"), Request.QueryString("sCO_UNID"), Request.QueryString("sTI_DOCU_RECE"), Request.QueryString("sNU_DOCU_RECE"))
                    If dt1.Rows.Count <> 0 Then
                        If dt1.Rows(0)("DE_MODA_MOVI") <> "" Then
                            strModalidad = dt1.Rows(0)("DE_MODA_MOVI")
                            strModalidad = strModalidad.Replace(" ", "")
                        End If
                    End If

                    Dim report As Report = New Report(New PdfFormatter)
                    Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
                    Dim fp As FontProp = New FontPropMM(fd, 2.0)
                    Dim fp_table As FontProp = New FontPropMM(fd, 1.6)
                    Dim fp_Titulo As FontProp = New FontPropMM(fd, 2.7)
                    Dim fp_doc As FontProp = New FontPropMM(fd, 2.5)
                    Dim ppBold As PenProp = New PenProp(report, 1.0)
                    Dim page As Page
                    Dim stream_Firma As Stream
                    Dim intAltura As Integer
                    Dim intLine As Int16 = 0
                    Dim intPaginas As Decimal
                    Dim intNroPag As Int16 = 0
                    Dim intRegistro As Integer
                    Dim sSubTitulo As String
                    Dim sNomSeguro As String
                    Dim sValorSeguro As String
                    Dim sValorSeguroPlus As String
                    Dim sObservacion As String
                    Dim strFirmaDepsa1 As String
                    Dim strFirmaDepsa2 As String
                    fp_Titulo.bBold = True

                    If dtTemporal.Rows.Count > 0 Then
                        If dtTemporal.Rows.Count > 30 Then
                            intPaginas = ((dtTemporal.Rows.Count / 30) + 1)
                            intPaginas = Fix(intPaginas)
                        Else
                            intPaginas = 1
                        End If

                        For p As Int16 = 1 To intPaginas
                            page = New Page(report)
                            page.rWidthMM = 210
                            page.rHeightMM = 297

                            stream_Firma = New FileStream(strPathFirmas & "Logo_Alma.JPG", FileMode.Open, FileAccess.Read, FileShare.Read)
                            page.AddMM(3, 35, New RepImageMM(stream_Firma, Double.NaN, 35))
                            'stream_Firma = New FileStream(strPathFirmas & "Logo_ISO.JPG", FileMode.Open, FileAccess.Read, FileShare.Read)
                            'page.AddMM(173, 25, New RepImageMM(stream_Firma, Double.NaN, 25))

                            intAltura = 80
                            sSubTitulo = IIf(dtTemporal.Rows(0)("ST_MERC_REPR") = "S", "DEPOSITO SIMPLE CON RETENCION PRENDARIA", "DEPOSITO SIMPLE")
                            sSubTitulo = IIf(strModalidad = "ADUANERO", "DEPOSITO ADUANERO AUTORIZADO", "DEPOSITO SIMPLE")
                            page.AddCB(intAltura, New RepString(fp_Titulo, "COMPROBANTE DE RECEPCION(DCR)"))
                            page.AddCB(intAltura + 12, New RepString(fp_Titulo, sSubTitulo))
                            page.Add(450, intAltura, New RepString(fp, dtTemporal.Rows(0)("DE_UNID")))
                            page.Add(450, intAltura + 12, New RepString(fp_Titulo, dtTemporal.Rows(0)("NU_DOCU_RECE")))

                            page.Add(170, intAltura + 50, New RepString(fp, dtTemporal.Rows(0)("NO_CLIE_REPO")))
                            page.Add(170, intAltura + 60, New RepString(fp, dtTemporal.Rows(0)("DE_DIRE_CLIE")))
                            page.Add(280, intAltura + 70, New RepString(fp, "TLF. " & dtTemporal.Rows(0)("NU_TELE_0001")))
                            page.Add(170, intAltura + 70, New RepString(fp, "R.U.C. " & dtTemporal.Rows(0)("CO_CLIE_ACTU")))
                            page.Add(450, intAltura + 50, New RepString(fp_Titulo, dtTemporal.Rows(0)("FE_DOCU")))

                            page.Add(170, intAltura + 90, New RepString(fp, "R.U.C. 20100000688"))
                            page.Add(170, intAltura + 100, New RepString(fp, "En la fecha indicada se han recibido las mercad. que se detallan"))
                            page.Add(170, intAltura + 110, New RepString(fp, "m�s adelante, cuyas caracter�sticas gnrles, son las sgtes :"))
                            If dtTemporal.Rows(0)("VA_CONS_SEGU") = 0 Then
                                sValorSeguro = ""
                            Else
                                sValorSeguro = CStr(dtTemporal.Rows(0)("VA_CONS_SEGU"))
                            End If
                            If dtTemporal.Rows(0)("VA_CONS_PLUS") = 0 Then
                                sValorSeguroPlus = ""
                            Else
                                sValorSeguroPlus = CStr(dtTemporal.Rows(0)("VA_CONS_SEGU"))
                            End If
                            page.Add(450, intAltura + 80, New RepString(fp, dtTemporal.Rows(0)("IM_TOTA") & " " & sValorSeguro))
                            page.Add(450, intAltura + 90, New RepString(fp, sValorSeguroPlus))
                            page.Add(450, intAltura + 110, New RepString(fp_Titulo, "Pag. " & p.ToString))

                            If p = 1 Then
                                page.Add(50, intAltura + 120, New RepString(fp, "Fecha Ingreso"))
                                page.Add(50, intAltura + 130, New RepString(fp, "Criterio Valorizaci�n"))
                                page.Add(50, intAltura + 140, New RepString(fp, "Direcci�n Almac�n"))
                                page.Add(50, intAltura + 150, New RepString(fp, "Fecha Facturaci�n"))
                                page.Add(125, intAltura + 120, New RepString(fp, ": " & dtTemporal.Rows(0)("FE_REGI_INGR")))
                                page.Add(125, intAltura + 130, New RepString(fp, ": " & dtTemporal.Rows(0)("DE_CRIT_VALO")))
                                page.Add(125, intAltura + 140, New RepString(fp, ": " & dtTemporal.Rows(0)("DE_DIRE_ALMA")))
                                page.Add(125, intAltura + 150, New RepString(fp, ": " & dtTemporal.Rows(0)("FE_FACT")))
                                page.Add(300, intAltura + 120, New RepString(fp, "Vcmto L�mite"))
                                page.Add(300, intAltura + 130, New RepString(fp, "Cia. de Seguros"))
                                page.Add(300, intAltura + 150, New RepString(fp, "Fec. proc."))
                                page.Add(360, intAltura + 120, New RepString(fp, ": " & dtTemporal.Rows(0)("FE_VENC_MERC")))
                                If CStr(dtTemporal.Rows(0)("NO_CIAS_SEGU")).Length > 34 Then
                                    sNomSeguro = CStr(dtTemporal.Rows(0)("NO_CIAS_SEGU")).Substring(0, 34)
                                Else
                                    sNomSeguro = CStr(dtTemporal.Rows(0)("NO_CIAS_SEGU"))
                                End If
                                page.Add(360, intAltura + 130, New RepString(fp, ": " & sNomSeguro))
                                page.Add(360, intAltura + 150, New RepString(fp, ": " & Now.ToShortDateString & " " & Now.ToShortTimeString & "   " & CStr(Session.Item("UsuarioLogin")).ToUpper))
                                intLine = (intAltura + 157) / 2.84
                            Else
                                intLine = (intAltura + 117) / 2.84
                                intAltura = 40
                            End If

                            page.AddMM(15, intLine, New RepLineMM(ppBold, 172, 0))
                            page.Add(50, intAltura + 165, New RepString(fp, "Item"))
                            page.Add(80, intAltura + 165, New RepString(fp, "Bodega"))
                            page.Add(120, intAltura + 165, New RepString(fp, "Cantidad"))
                            page.Add(160, intAltura + 165, New RepString(fp, "Unidad"))
                            page.Add(200, intAltura + 165, New RepString(fp, "Cantidad"))
                            page.Add(260, intAltura + 165, New RepString(fp, "Bulto"))
                            page.Add(300, intAltura + 165, New RepString(fp, "Descripci�n de la Mercader�a"))
                            page.Add(440, intAltura + 165, New RepString(fp, "Prec. Unit."))
                            page.Add(495, intAltura + 165, New RepString(fp, "Valor"))
                            page.AddMM(15, intLine + 4, New RepLineMM(ppBold, 172, 0))
                            Dim decTotal As Decimal

                            If (p - 1) * (30) + 30 < dtTemporal.Rows.Count Then
                                intNroPag = (p - 1) * (30) + 30
                            Else
                                intNroPag = dtTemporal.Rows.Count
                            End If

                            For i As Int16 = (p - 1) * (30) To intNroPag - 1
                                intAltura += 10
                                If CInt(dtTemporal.Rows(i)("NU_SECU")) <> intRegistro Then
                                    page.Add(50, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("NU_SECU")))
                                    page.AddRight(150, intAltura + 170, New RepString(fp_table, String.Format("{0:##,##0.000}", dtTemporal.Rows(i)("NU_UNID_PESO"))))
                                    page.AddRight(250, intAltura + 170, New RepString(fp_table, String.Format("{0:##,##0.000}", dtTemporal.Rows(i)("NU_PESO_RECI"))))
                                    page.AddRight(460, intAltura + 170, New RepString(fp_table, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_UNIT"))))
                                    page.AddRight(525, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("SB_MONE") & " " & String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_ITEM"))))
                                    intRegistro = CInt(dtTemporal.Rows(i)("NU_SECU"))
                                End If
                                page.Add(80, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("CO_BODE_ACTU") & " " & dtTemporal.Rows(i)("CO_UBIC_ACTU")))
                                page.Add(160, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("DE_UNID_PESO")))
                                page.Add(260, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("DE_UNME_PESO")))
                                page.Add(300, intAltura + 170, New RepString(fp_table, dtTemporal.Rows(i)("DE_MERC")))
                            Next
                            intAltura = 370

                            If CStr(dtTemporal.Rows(0)("DE_OBSE")).Length > 70 Then
                                sObservacion = CStr(dtTemporal.Rows(0)("DE_OBSE")).Substring(0, 70)
                            Else
                                sObservacion = CStr(dtTemporal.Rows(0)("DE_OBSE"))
                            End If
                            page.Add(50, intAltura + 205, New RepString(fp, "OBSERVACION : " & sObservacion))

                            If p = intPaginas Then
                                page.AddRight(525, intAltura + 205, New RepString(fp, dtTemporal.Rows(0)("SB_MONE") & "  " & String.Format("{0:##,##0.00}", dtTemporal.Rows(0)("IM_VALO_MERC"))))
                            Else
                                page.Add(490, intAltura + 205, New RepString(fp, "Contin�a..."))
                            End If

                            'strFirmaDepsa1 = strPathFirmas & "20100044626D001.JPG"
                            'strFirmaDepsa2 = strPathFirmas & "20100044626D005.JPG"

                            'stream_Firma = New FileStream(strFirmaDepsa1, FileMode.Open, FileAccess.Read, FileShare.Read)
                            'page.AddMM(20, 250, New RepImageMM(stream_Firma, Double.NaN, 27))
                            'stream_Firma = New FileStream(strFirmaDepsa2, FileMode.Open, FileAccess.Read, FileShare.Read)
                            'page.AddMM(70, 250, New RepImageMM(stream_Firma, Double.NaN, 27))

                            'page.Add(360, intAltura + 250, New RepString(fp, "El Cliente declara conocer los almacenes y acepta"))
                            'page.Add(360, intAltura + 260, New RepString(fp, "las condiciones de almacenaje de su mercader�a."))
                            'page.Add(360, intAltura + 300, New RepString(fp, "Por  lo  establecido en el  Art.  224 de la Ley"))
                            'page.Add(360, intAltura + 310, New RepString(fp, "de T�tulos Valores 27287, DEPOSITOS S.A."))
                            'page.Add(360, intAltura + 320, New RepString(fp, "expedir� un  Certificado de Dep�sito,  dando"))
                            'page.Add(360, intAltura + 330, New RepString(fp, "aviso al Depositante."))
                            'page.Add(50, intAltura + 360, New RepString(fp, "El depositante conviene en se�alar de manera irrevocable que en caso de incumplimiento en los pagos por los servicios prestados ser� de"))
                            'page.Add(50, intAltura + 370, New RepString(fp, "aplicaci�n lo establecido en el Art. 1852 del C�digo Civil. "))
                            'page.Add(50, intAltura + 400, New RepString(fp, "DE-R-017-GAP"))
                            page.Add(50, intAltura + 350, New RepString(fp, "El almacenamiento de la mercader�a se realizar� de acuerdo a las condiciones ofrecidas en la Cotizaci�n del Servicio. El cliente declara conocer"))
                            page.Add(50, intAltura + 360, New RepString(fp, "el almac�n y aceptar las condiciones ofrecidas."))
                            page.Add(50, intAltura + 370, New RepString(fp, "De acuerdo al Art. 1852 del C�digo Civil, Alma Per� podr� retener la mercader�a en caso de incumplimiento de los pagos por los servicios prestados."))
                            page.Add(50, intAltura + 380, New RepString(fp, "El cliente autoriza a Alma Per� la emisi�n de un Certificado de Dep�sito conforme a lo establecido en la Ley N�27287."))
                            'page.Add(50, intAltura + 400, New RepString(fp_doc, "DE-R-017-GAP"))
                        Next
                    End If
                    RT.ResponsePDF(report, Me)
                End If
            Else
                Response.Redirect("../Salir.aspx?caduco=1", False)
            End If
        Catch ex As Exception
            Dim str As String = ex.ToString
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub
End Class
