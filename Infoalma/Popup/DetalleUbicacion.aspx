<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DetalleUbicacion.aspx.vb" Inherits="DetalleUbicacion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DetalleUbicacion</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="420" border="0">
				<TR>
					<TD class="Subtitulo"><asp:label id="lblTitulo" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="6" background="../images/table_r1_c1.gif" height="6"></TD>
								<TD background="../images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../images/table_r2_c1.gif"></TD>
								<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">--> &nbsp;
									<asp:datagrid id="dgUbicacion" runat="server" AutoGenerateColumns="False" CssClass="gv" Width="100%"
										BorderColor="Gainsboro">
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="nu_docu_rece" HeaderText="DCR">
												<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NU_SECU" HeaderText="Item">
												<HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="de_merc" HeaderText="Descripci&#243;n">
												<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TI_SITU" HeaderText="Situaci&#243;n">
												<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Detalle">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table11" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" OnClick="imgEditar_Click" runat="server" CausesValidation="False"
																	ImageUrl="../images/Ver.JPG" ToolTip="Editar Registro"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="ti_docu_rece" HeaderText="ti_docu_rece"></asp:BoundColumn>
										</Columns>
									</asp:datagrid><!--</div>--></TD>
								<TD width="6" background="../images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../images/table_r3_c1.gif" height="6"></TD>
								<TD background="../images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="6" background="../images/table_r1_c1.gif" height="6"></TD>
								<TD background="../images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../images/table_r2_c1.gif"></TD>
								<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">--> &nbsp;
									<asp:datagrid id="dgDetalle" runat="server" AutoGenerateColumns="False" CssClass="gv" Width="100%"
										BorderColor="Gainsboro">
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="nu_docu_rece" HeaderText="Bodega">
												<HeaderStyle HorizontalAlign="Center" Width="90px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="de_merc" HeaderText="Ubicaci&#243;n">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="fe_regi_merc" HeaderText="Fecha Ingreso.">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TI_SITU" HeaderText="Fecha Salida">
												<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid><!--</div>--></TD>
								<TD width="6" background="../images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../images/table_r3_c1.gif" height="6"></TD>
								<TD background="../images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center">
						<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center"><INPUT class="btn" id="btnImprimir" style="WIDTH: 80px" onclick="window.print();" type="button"
							size="80" value="Imprimir" name="btnImprimir"><INPUT style="WIDTH: 80px" onclick="javascript:window.close()" type="button" value="Cerrar"
							class="btn"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
