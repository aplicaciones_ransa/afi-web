<%@ Page Language="vb" AutoEventWireup="false" CodeFile="PoputDocRetiroIngreso.aspx.vb" Inherits="PoputDocRetiroIngreso" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Solicitar Acceso</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<script language="javascript" src="../JScripts/Validaciones.js"></script>
		<script language="javascript">			

								
			function ValidaFecha(sFecha)
				{
				var fecha1 = sFecha
				var fecha2 = document.getElementById('txtFechaInicial').value
				var miFecha1 = new Date(fecha1.substr(6,4), fecha1.substr(3,2), fecha1.substr(0,2))
				var miFecha2 = new Date(fecha2.substr(6,4), fecha2.substr(3,2), fecha2.substr(0,2)) 
				var diferencia = (miFecha1.getTime()�- miFecha2.getTime())/(1000*60*60*24)
				//alert (diferencia);

					if (diferencia > 1540){
					window.open("Popup/SolicitarInformacion.aspx","Vencimientos","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
					return false;
					}
					else
					{
					return true;
					}
				}
			function ValidarFechas()
				{
			var fchIni;
			var fchFin;
			
			fchIni = document.getElementById("txtFechaInicial").value;
			fchFin = document.getElementById("txtFechaFinal").value;
			
			fchIni = fchIni.substr(6,4) + fchIni.substr(3,2) + fchIni.substr(0,2);
			fchFin = fchFin.substr(6,4) + fchFin.substr(3,2) + fchFin.substr(0,2);
			if (fchIni == "" && fchFin == "")
				{
					alert("Debe Ingresar fecha Inicial y Final");
					return false;
				}
				
			if(fchIni > fchFin && fchIni != "" && fchFin != "")
				{
					alert("La fecha inicial no puede ser mayor a la fecha final");
					return false;
				}
				return true;
			}
			
			function dois_pontos(tempo){
			if(event.keyCode<48 || event.keyCode>57){
				event.returnValue=false;}
			if(tempo.value.length==2){
				tempo.value+=":";}
			}
			
		function ValidNum(e) { var tecla= document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46);}
							
		function isNumberOrLetter(evt) 
		{
		   var charCode = (evt.which) ? evt.which : event.keyCode;
		   if ((charCode > 65 && charCode < 91) || (charCode > 97 && charCode < 123) || (charCode > 47 && charCode < 58) )
		   return true;  
		   return false;
		}
		
		function isNumberOrLetterYGuion(evt) 
		{
		   var charCode = (evt.which) ? evt.which : event.keyCode;
		   if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || (charCode == 45 ))
		   return true;  
		   return false;
		}							
		</script>
		<style type="text/css">.txtMayuscula { TEXT-ALIGN: left; TEXT-TRANSFORM: uppercase; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #003366; FONT-SIZE: 11px; FONT-WEIGHT: normal }
		</style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="10" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="700">
				<TR>
					<TD class="td" onkeyup="this.value = this.value.toUpperCase();" align="center">Detalle 
						de Ingreso</TD>
				</TR>
				<TR>
					<TD align="center">
						<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0">
							<TR>
								<TD height="6" background="../Images/table_r1_c1.gif" width="6"></TD>
								<TD height="6" background="../Images/table_r1_c2.gif"></TD>
								<TD height="6" background="../Images/table_r1_c3.gif" width="6"></TD>
							</TR>
							<TR>
								<TD background="../Images/table_r2_c1.gif" width="6"></TD>
								<TD vAlign="top" align="center"><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
									<P>
										<TABLE id="Table3" border="0" cellSpacing="1" cellPadding="0" width="650" align="center">
											<TR>
												<TD class="Text">�tem</TD>
												<TD class="Text">:</TD>
												<TD class="Text"><asp:label id="lblNroItem" runat="server" CssClass="text"></asp:label></TD>
												<TD class="Text">Cod. Producto</TD>
												<TD class="Text">:</TD>
												<TD class="text"><asp:textbox id="txtCodProd" onkeypress="return isNumberOrLetter(event)" runat="server" CssClass="txtMayuscula"
														Width="80px"></asp:textbox></TD>
											</TR>
											<TR>
												<TD class="Text">Desripci�n</TD>
												<TD class="Text">:</TD>
												<TD class="Text" colSpan="4"><asp:textbox id="txtDescripcion" onkeypress="return isNumberOrLetterYGuion(event)" runat="server"
														CssClass="txtMayuscula" Width="500px" Enabled="False"></asp:textbox></TD>
											</TR>
											<TR>
												<TD class="Text">Cantidad</TD>
												<TD class="Text">:</TD>
												<TD><asp:textbox id="txtCantidad" onkeypress="javascript:return ValidNum(event);" runat="server"
														CssClass="Text" Width="80px"></asp:textbox></TD>
												<TD class="Text">Nro. Lote</TD>
												<TD class="Text">:</TD>
												<TD class="Text"><asp:textbox id="txtNroLote" onkeypress="return isNumberOrLetterYGuion(event)" runat="server"
														CssClass="txtMayuscula" Width="100px"></asp:textbox></TD>
											</TR>
										</TABLE>
									</P>
								</TD>
								<TD background="../Images/table_r2_c3.gif" width="6"></TD>
							</TR>
							<TR>
								<TD height="6" background="../Images/table_r3_c1.gif" width="6"></TD>
								<TD height="6" background="../Images/table_r3_c2.gif"></TD>
								<TD height="6" background="../Images/table_r3_c3.gif" width="6"></TD>
							</TR>
						</TABLE>
						<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center"><asp:button id="btnActualizar" runat="server" CssClass="Text" Visible="False" Width="80px" Text="Actualizar"></asp:button><asp:button id="btnGrabar" runat="server" CssClass="Text" Visible="False" Width="80px" Text="Grabar"></asp:button><input style="WIDTH: 80px" class="Text" onclick="Javascript:window.close(),window.opener.ActualizaWFormPadre();"
							value="Cerrar" type="button"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
