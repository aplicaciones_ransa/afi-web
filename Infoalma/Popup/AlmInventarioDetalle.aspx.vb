Imports Depsa.LibCapaNegocio
Public Class AlmInventarioDetalle
    Inherits System.Web.UI.Page
    Dim objMercaderia As clsCNMercaderia
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents rbtPendientes As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rbtCerrados As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents lblNroDCR As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroItem As System.Web.UI.WebControls.Label
    'Protected WithEvents hiCO_UNID As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) Then
            If Page.IsPostBack = False Then
                Me.lblNroDCR.Text = Request.QueryString("sNU_RECE")
                Me.lblNroItem.Text = Request.QueryString("sNU_ITEM")
                Me.hiCO_UNID.Value = Request.QueryString("sCO_UNID")
                Bindatagrid("S")
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid(ByVal strEstado As String)
        Try
            objMercaderia = New clsCNMercaderia
            Me.dgResultado.DataSource = objMercaderia.gCNGetListarRetiros(Session.Item("CoEmpresa"), Me.hiCO_UNID.Value, Me.lblNroDCR.Text, Me.lblNroItem.Text, strEstado)
            Me.dgResultado.DataBind()
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub rbtPendientes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtPendientes.CheckedChanged
        Bindatagrid("N")
    End Sub

    Private Sub rbtCerrados_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtCerrados.CheckedChanged
        Bindatagrid("S")
    End Sub
End Class
