Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Root.Reports

'Imports System.IO

Public Class CueImpresionSustentoDCR
    Inherits System.Web.UI.Page
    Dim sCO_UNID, sTI_DOCU, sNU_DOCU As String
    Dim objCCorrientes As clsCNCCorrientes

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected dvTCDOCU_CLIE As DataView

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            GenerarPDF()
        End If
    End Sub

    Private Sub GenerarPDF()
        Dim dt As New DataTable
        Try
            objCCorrientes = New clsCNCCorrientes
            dt = objCCorrientes.gCADGetSustentoDCR("01", Request.QueryString("sCO_UNID"), Request.QueryString("sTI_DOCU_COBR"), Request.QueryString("sNU_DOCU_COBR"))
            '  dt = objCCorrientes.gCADGetSustentoDCR("01", "001", "FAC", "F31-0000638")
            GenerarSustento(dt)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Private Sub GenerarSustento(ByVal dtTemporal As DataTable)
        Try
            Dim report As Report = New Report(New PdfFormatter)
            Dim fd As FontDef = New FontDef(report, FontDef.StandardFont.Helvetica)
            Dim fp As FontProp = New FontPropMM(fd, 2.0)
            Dim fp_table As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_Negrita As FontProp = New FontPropMM(fd, 1.8)
            Dim fp_peque�o As FontProp = New FontPropMM(fd, 1.5)
            Dim fp_Titulo As FontProp = New FontPropMM(fd, 3.5)
            Dim ppBold As PenProp = New PenProp(report, 1.0)
            Dim page As Page
            Dim intAltura As Integer
            Dim intLine As Int16 = 0
            Dim intPaginas As Decimal
            Dim sTipoMoneda As String
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            Dim dttFecha As DateTime
            dttFecha = Date.Today
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            dttFecha = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
            Dim sTotalCant As Decimal
            Dim sTotalMerc As Decimal
            Dim sTotalImpo As Decimal
            Dim sTotalComp As Decimal
            Dim sTTotalCant As Decimal
            Dim sTTotalMerc As Decimal
            Dim sTTotalImpo As Decimal
            Dim sTTotalComp As Decimal
            Dim sCodAlma As String
            Dim sNuTitu As String
            Dim count As Integer

            'CREAR DATATABLA GRUPO 1
            Dim dtGrupo1 As DataTable
            dtGrupo1 = GrupoDistinct(dtTemporal, "NU_DOCU_RECE")
            If dtTemporal.Rows.Count > 0 Then
                If dtTemporal.Rows.Count > 10 Then
                    intPaginas = ((dtTemporal.Rows.Count / 10) + 1)
                    intPaginas = Fix(intPaginas)
                Else
                    intPaginas = 1
                End If
                sTTotalCant = 0
                sTTotalMerc = 0
                sTTotalImpo = 0
                sTTotalComp = 0
                For p As Int16 = 1 To intPaginas
                    page = New Page(report)
                    page.rWidthMM = 210
                    page.rHeightMM = 297
                    page.SetLandscape()
                    intAltura = 50

                    page.Add(200, intAltura, New RepString(fp_Titulo, "REPORTE DE COMPROBANTE DE RECEPCI�N CON FACTURA"))
                    page.Add(50, intAltura + 50, New RepString(fp_Negrita, "ALMA PER�")) : page.Add(700, intAltura + 50, New RepString(fp_table, "P�g. " & "000" + CStr(p.ToString)))
                    page.Add(50, intAltura + 60, New RepString(fp_Negrita, (Session.Item("UsuarioLogin")).ToUpper())) : page.Add(700, intAltura + 60, New RepString(fp_Negrita, dttFecha))
                    page.Add(50, intAltura + 70, New RepString(fp_Negrita, "Sres. " & dtTemporal.Rows(0)("NO_CLIE_REPO"))) : page.Add(700, intAltura + 70, New RepString(fp_Negrita, Now.ToString("HH:mm:ss")))
                    page.Add(50, intAltura + 75, New RepString(fp_Negrita, "______________________________________________________________________________________________________________________________________________________________________________________________"))
                    page.Add(50, intAltura + 100, New RepString(fp_Negrita, "ITEM"))
                    page.Add(75, intAltura + 100, New RepString(fp_Negrita, "FECHA"))
                    page.Add(120, intAltura + 100, New RepString(fp_Negrita, "BULTO")) : page.Add(125, intAltura + 90, New RepString(fp_Negrita, "TIPO"))
                    page.Add(160, intAltura + 100, New RepString(fp_Negrita, "CANTIDAD"))
                    page.Add(210, intAltura + 100, New RepString(fp_Negrita, "MERCADERIA")) : page.Add(215, intAltura + 90, New RepString(fp_Negrita, "VALOR"))
                    page.Add(350, intAltura + 100, New RepString(fp_Negrita, "DESCRIPCION DE MERCADERIA"))
                    page.Add(650, intAltura + 100, New RepString(fp_Negrita, "TARIFA"))
                    page.Add(710, intAltura + 100, New RepString(fp_Negrita, "IMPORTE"))
                    page.Add(760, intAltura + 100, New RepString(fp_Negrita, "COMPROB.")) : page.Add(765, intAltura + 90, New RepString(fp_Negrita, "IMPORTE"))
                    page.Add(50, intAltura + 110, New RepString(fp_Negrita, "______________________________________________________________________________________________________________________________________________________________________________________________"))
                    page.Add(45, intAltura + 120, New RepString(fp_Negrita, "DOCUMENTO: " & dtTemporal.Rows(0)("TI_DOCU") & " - " & dtTemporal.Rows(0)("NU_DOCU")))

                    For g1 As Int16 = 0 To dtGrupo1.Rows.Count - 1
                        If dtGrupo1.Rows(g1)("ST_PRINT") = "" Then
                            intAltura += 10
                            page.Add(50, intAltura + 130, New RepString(fp_Negrita, "COMPROBANTE: " & dtTemporal.Rows(0)("TI_DOCU_RECE") & " - " & dtGrupo1.Rows(g1)("NU_DOCU_RECE")))
                            sCodAlma = ""
                            sNuTitu = ""
                            count = 0
                            'FELIX : grabando estado de impresion e el pdf
                            dtGrupo1.Rows(g1)("ST_PRINT") = "G"
                            sTotalCant = 0
                            sTotalMerc = 0
                            sTotalImpo = 0
                            sTotalComp = 0
                            For i As Int16 = 0 To dtTemporal.Rows.Count - 1

                                If dtTemporal.Rows(i)("CO_ALMA_ORIG") <> "ALMA" Then
                                    sCodAlma = dtTemporal.Rows(i)("CO_ALMA_ORIG")
                                End If
                                If dtTemporal.Rows(i)("NU_TITU") <> "" Then
                                    sNuTitu = dtTemporal.Rows(i)("NU_TITU")
                                End If

                                If dtTemporal.Rows(i)("NU_DOCU_RECE") = dtGrupo1.Rows(g1)("NU_DOCU_RECE") Then

                                    If dtTemporal.Rows(i)("CO_UNME_PARA") = "S/." Then
                                        sTipoMoneda = "SOL"
                                    Else
                                        sTipoMoneda = ""
                                    End If

                                    intAltura += 5
                                    page.Add(55, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("NU_SECU_MERC"))) 'ITEM
                                    page.Add(70, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("FE_MOVI"))) 'FECHA
                                    page.Add(120, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("CO_TIPO_BULT"))) 'BULTO
                                    page.Add(160, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("NU_UNID_SALD") & " " & dtTemporal.Rows(i)("SB_MONE")))   'CANTIDAD Y TIPO DE MONEDA
                                    page.Add(210, intAltura + 140, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_REMO_SALD"))))

                                    If Len(dtTemporal.Rows(i)("DE_MERC")) < "100" Then
                                        page.Add(260, intAltura + 140, New RepString(fp_peque�o, dtTemporal.Rows(i)("DE_MERC"))) 'DESCRIPCION DE MERCADERIA"

                                        page.Add(650, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("VA_CONS") & " " & dtTemporal.Rows(i)("CO_UNME_PARA") & " " & sTipoMoneda))     'TARIFA
                                        page.Add(720, intAltura + 140, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_TOTA")))) 'IMPORTE
                                        page.Add(770, intAltura + 140, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_REAL")))) 'COMPROB.
                                    Else
                                        page.Add(260, intAltura + 137, New RepString(fp_peque�o, Left(dtTemporal.Rows(i)("DE_MERC"), 100))) 'DESCRIPCION DE MERCADERIA"
                                        page.Add(260, intAltura + 143, New RepString(fp_peque�o, Mid(dtTemporal.Rows(i)("DE_MERC"), 101, Len(dtTemporal.Rows(i)("DE_MERC")) - 100))) 'DESCRIPCION DE MERCADERIA"

                                        page.Add(650, intAltura + 140, New RepString(fp_Negrita, dtTemporal.Rows(i)("VA_CONS") & " " & dtTemporal.Rows(i)("CO_UNME_PARA") & " " & sTipoMoneda))     'TARIFA
                                        page.Add(720, intAltura + 140, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_TOTA")))) 'IMPORTE
                                        page.Add(770, intAltura + 140, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", dtTemporal.Rows(i)("IM_REAL")))) 'COMPROB.

                                        count += 10
                                        intAltura += 10
                                    End If

                                    intAltura += 5
                                    sTotalCant = sTotalCant + dtTemporal.Rows(i)("NU_UNID_SALD")
                                    sTotalMerc = sTotalMerc + dtTemporal.Rows(i)("IM_REMO_SALD")
                                    sTotalImpo = sTotalImpo + dtTemporal.Rows(i)("IM_TOTA")
                                    sTotalComp = sTotalComp + dtTemporal.Rows(i)("IM_REAL")
                                    sTTotalCant = sTTotalCant + dtTemporal.Rows(i)("NU_UNID_SALD")
                                    sTTotalMerc = sTTotalMerc + dtTemporal.Rows(i)("IM_REMO_SALD")
                                    sTTotalImpo = sTTotalImpo + dtTemporal.Rows(i)("IM_TOTA")
                                    sTTotalComp = sTTotalComp + dtTemporal.Rows(i)("IM_REAL")
                                    count += 10
                                End If
                            Next

                            If sCodAlma <> "" Then
                                page.Add(210, intAltura - count + 130, New RepString(fp_Negrita, "ALMACEN :  " & sCodAlma))
                            End If
                            If sNuTitu <> "" Then
                                page.Add(300, intAltura - count + 130, New RepString(fp_Negrita, "TITULO :  " & sNuTitu))
                            End If

                            page.Add(160, intAltura + 140, New RepString(fp_Negrita, "__________"))
                            page.Add(210, intAltura + 140, New RepString(fp_Negrita, "__________"))
                            page.Add(710, intAltura + 140, New RepString(fp_Negrita, "__________"))
                            page.Add(760, intAltura + 140, New RepString(fp_Negrita, "__________"))
                            page.Add(160, intAltura + 150, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTotalCant)))
                            page.Add(210, intAltura + 150, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTotalMerc)))
                            page.Add(720, intAltura + 150, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTotalImpo)))
                            page.Add(770, intAltura + 150, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTotalComp)))

                            intAltura += 30

                            If intAltura > 400 Then
                                Exit For
                            End If
                        End If
                    Next
                Next

                page.Add(45, intAltura + 160, New RepString(fp_Negrita, "TOTAL DOCUMENTO :"))
                page.Add(160, intAltura + 150, New RepString(fp_Negrita, "__________"))
                page.Add(210, intAltura + 150, New RepString(fp_Negrita, "__________"))
                page.Add(710, intAltura + 150, New RepString(fp_Negrita, "__________"))
                page.Add(760, intAltura + 150, New RepString(fp_Negrita, "__________"))
                page.Add(160, intAltura + 160, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTTotalCant)))
                page.Add(210, intAltura + 160, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTTotalMerc)))
                page.Add(720, intAltura + 160, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTTotalImpo)))
                page.Add(770, intAltura + 160, New RepString(fp_Negrita, String.Format("{0:##,##0.00}", sTTotalComp)))
                page.Add(50, intAltura + 170, New RepString(fp_Negrita, "______________________________________________________________________________________________________________________________________________________________________________________________"))
                page.Add(400, intAltura + 180, New RepString(fp_Negrita, "*** Fin de Reporte ***"))
            End If
            RT.ResponsePDF(report, Me)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("No se encontr� el archivo PDF")
        End Try
    End Sub

    Function GrupoDistinct(ByVal dtTemporal As DataTable, ByVal sColumna1 As String) As DataTable
        Dim dr As DataRow
        Dim dt1 As New DataTable
        Dim sExiste As Boolean
        dt1.Columns.Add(sColumna1, GetType(System.String))
        dt1.Columns.Add("ST_PRINT", GetType(System.String))
        'dt1.Columns.Add("C01", GetType(System.String))
        'dt1.Columns.Add("C02", GetType(System.String))

        For i As Integer = 0 To dtTemporal.Rows.Count - 1
            sExiste = False
            For ii As Integer = 0 To dt1.Rows.Count - 1
                If dt1.Rows(ii)(sColumna1) = dtTemporal.Rows(i)(sColumna1) Then
                    sExiste = True
                    Exit For
                End If
            Next

            If sExiste = False Then
                dr = dt1.NewRow()
                dr(sColumna1) = dtTemporal.Rows(i)(sColumna1)
                dr("ST_PRINT") = "" ' Columna adicionales 
                'dr("C01") = ""  ' Columna adicionales 
                'dr("C02") = ""  ' Columna adicionales 
                dt1.Rows.Add(dr)
            End If
        Next
        Return dt1
    End Function

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
