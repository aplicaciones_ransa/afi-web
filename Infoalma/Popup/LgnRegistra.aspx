<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LgnRegistra.aspx.vb" Inherits="LgnRegistra"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Solicitar Acceso</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" topMargin="10" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="4" cellPadding="0" width="280" border="0">
				<TR>
					<TD class="td" vAlign="middle" colSpan="2">Desea tener acceso a la web AFI?</TD>
				</TR>
				<TR>
					<TD class="Text" align="center" colSpan="2">Ingrese sus datos y un ejecutivo de 
						Alma Per� atender� su solicitud.</TD>
				</TR>
				<TR>
					<TD class="Text" width="20%">Empresa - RUC :</TD>
					<TD width="80%"><asp:textbox id="txtEmpresa" runat="server" CssClass="Text" Width="250px"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" CssClass="Error" ErrorMessage="*" Display="Dynamic"
							ForeColor=" " ControlToValidate="txtEmpresa"></asp:RequiredFieldValidator>&nbsp;-
						<asp:textbox id="txtRUC" runat="server" CssClass="Text" Width="80px"></asp:textbox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" CssClass="Error" ErrorMessage="*" Display="Dynamic"
							ForeColor=" " ControlToValidate="txtRUC"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="Text">Tel�fono - Anexo :</TD>
					<TD class="Text"><asp:textbox id="txtTelefono" runat="server" CssClass="Text" Width="80px"></asp:textbox>&nbsp;-&nbsp;
						<asp:textbox id="txtAnexo" runat="server" CssClass="Text" Width="50px"></asp:textbox>
						<asp:RequiredFieldValidator style="Z-INDEX: 0" id="RequiredFieldValidator3" runat="server" ControlToValidate="txtTelefono"
							Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="Text" colSpan="2">Datos del (los) usuario(s):</TD>
				</TR>
				<TR>
					<TD class="Text" colSpan="2"><asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
							BorderColor="Gainsboro" HorizontalAlign="Center">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Nro" HeaderText="Nro"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Nombre">
									<ItemTemplate>
										<asp:TextBox id="txtNombre" runat="server" CssClass="Text" Width="100px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Apellidos">
									<ItemTemplate>
										<asp:TextBox id="txtApellidos" runat="server" CssClass="Text" Width="100px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="DNI">
									<ItemTemplate>
										<asp:TextBox id="txtDNI" runat="server" CssClass="Text" Width="50px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Cargo">
									<ItemTemplate>
										<asp:TextBox id="txtCargo" runat="server" CssClass="Text" Width="80px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="E-mail">
									<ItemTemplate>
										<asp:TextBox id="txtEmail" runat="server" CssClass="Text" Width="100px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD class="Text" vAlign="top">�reas de inter�s :
					</TD>
					<TD vAlign="top" height="40">
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Text"><asp:checkbox id="chkAlmacenes" runat="server" Text="Almacenes"></asp:checkbox>
								</TD>
								<TD class="Text">
									<asp:checkbox id="chkWarrants" runat="server" Text="Warrants"></asp:checkbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2" height="20"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2"><asp:button id="btnEnviar" runat="server" CssClass="Text" Width="80px" Text="Enviar"></asp:button>&nbsp;
						<INPUT class="Text" style="WIDTH: 80px" onclick="Javascript:window.close();" type="button"
							value="Cerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
