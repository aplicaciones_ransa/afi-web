Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class Vencimientos
    Inherits System.Web.UI.Page
    Private objTicket As clsCNTicket
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgDCR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgWarrant As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgAduanero As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblDCR As System.Web.UI.WebControls.Label
    'Protected WithEvents lblWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblAduanero As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Request.IsAuthenticated Then
            Session.LCID = 1033
            Try
                If Page.IsPostBack = False Then
                    Bindatagrid()
                    'BindatagridWarrant()
                    'BindatagridAduanero()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        objTicket = New clsCNTicket
        Dim ds As DataSet
        With objTicket
            ds = .gCNListar_Vencimientos(Session.Item("CoEmpresa"), Session.Item("IdSico"))
            Me.dgDCR.DataSource = ds.Tables(1)
            Me.dgDCR.DataBind()
            If Me.dgDCR.Items.Count = 0 Then
                Me.lblDCR.Text = "No se encontr� Mercader�a vencida"
            End If
            Me.dgWarrant.DataSource = ds.Tables(0)
            Me.dgWarrant.DataBind()
            If Me.dgWarrant.Items.Count = 0 Then
                Me.lblWarrant.Text = "No se encontr� Warrant vencidos"
            End If
            Me.dgAduanero.DataSource = ds.Tables(2)
            Me.dgAduanero.DataBind()
            If Me.dgAduanero.Items.Count = 0 Then
                Me.lblAduanero.Text = "No se encontr� Mercader�a aduanera con Abandono Legal"
            End If
        End With
    End Sub

    Private Sub dgDCR_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDCR.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(6).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Imp.Unit."))
                e.Item.Cells(8).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Unidades"))
                e.Item.Cells(9).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Valor"))
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgWarrant_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgWarrant.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(7).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Saldo"))
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgAduanero_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAduanero.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(4).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Saldo"))
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub
End Class
