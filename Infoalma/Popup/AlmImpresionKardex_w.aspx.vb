Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.Data

Public Class AlmImpresionKardex_w
    Inherits System.Web.UI.Page
    Protected dvKardex As DataView
    Private strdocu_aux As String
    'Protected WithEvents lbFE_INI_FIN As System.Web.UI.WebControls.Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label6 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbLOTE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_MERC As System.Web.UI.WebControls.Label
    'Protected WithEvents Label17 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_PROD As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNU_REFE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label13 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents lbDE_ALMA As System.Web.UI.WebControls.Label
    'Protected WithEvents Label14 As System.Web.UI.WebControls.Label
    'Protected WithEvents Label16 As System.Web.UI.WebControls.Label
    Private objKardex As New clsCNKardex
    Dim i As Integer = 0
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lbHO_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbFE_REPO_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCO_USUA_GENE As System.Web.UI.WebControls.Label
    'Protected WithEvents Label10 As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lbCO_USUA_GENE.Text = Session("nom_user").ToString.ToUpper
        Me.lbFE_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortDate)
        Me.lbHO_REPO_GENE.Text = FormatDateTime(Now(), DateFormat.ShortTime)
        If Not Page.IsPostBack Then
            prMUEST_DATO()
        End If
    End Sub

    Private Sub prMUEST_DATO()
        Dim strFe_Ini As String = "Del " & Request.QueryString("sFE_INIC")
        Dim strFe_Fin As String = " Al " & Request.QueryString("sFE_FINA")
        If strFe_Ini <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini
        End If
        If strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Fin
        End If
        If strFe_Ini <> "" And strFe_Fin <> "" Then
            Me.lbFE_INI_FIN.Text = strFe_Ini & strFe_Fin
        End If
        Me.lbDE_ALMA.Text = Request.QueryString("sDE_ALMA")
        Me.lbCO_PROD.Text = Request.QueryString("sCO_PROD")
        Me.lbDE_MERC.Text = Request.QueryString("sDE_MERC")
        Me.lbLOTE.Text = Request.QueryString("sLOTE")
        Me.lbNU_REFE.Text = Request.QueryString("sCODIGO")

        Bindatagrid()
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dtKardex As New DataTable
            With objKardex
                Dim strFe_Ini As String = "Del " & Request.QueryString("sFE_INIC")
                Dim strFe_Fin As String = " Al " & Request.QueryString("sFE_FINA")
                dtKardex = .gCNListarKardexWMS(Request.QueryString("sCODIGO"), Request.QueryString("sCO_PROD"), Request.QueryString("sDE_MERC"), Request.QueryString("sLOTE"),
                Request.QueryString("sFE_INIC"), Request.QueryString("sFE_FINA"), Request.QueryString("sCO_ALMA"), Session.Item("IdSico"))

                Me.dgResultado.Visible = True
                Me.dgResultado.DataSource = dtKardex
                Me.dgResultado.DataBind()
                Session.Item("dtMovimientoKardexWMS") = dtKardex

            End With
        Catch ex As Exception
            Me.lbMS_REGI.Text = ex.Message
        End Try
    End Sub


    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try


            If e.Item.Cells(9).Text = "0" Then 'Ingresos
                e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")

            ElseIf e.Item.Cells(9).Text = "2" Then 'Retiros
                e.Item.BackColor = System.Drawing.Color.Lavender

            ElseIf e.Item.Cells(9).Text = "3" Then  ' Ajustes
                e.Item.BackColor = System.Drawing.Color.FromName("#add8e6")

            ElseIf e.Item.Cells(9).Text = "4" Then ' Saldo
                e.Item.Cells(0).Text = " SALDO : " & (e.Item.Cells(4).Text).ToString
                e.Item.Cells.RemoveAt(10)
                e.Item.Cells.RemoveAt(9)
                e.Item.Cells.RemoveAt(8)
                e.Item.Cells.RemoveAt(7)
                e.Item.Cells.RemoveAt(6)
                e.Item.Cells.RemoveAt(5)
                e.Item.Cells.RemoveAt(4)
                e.Item.Cells.RemoveAt(3)
                e.Item.Cells.RemoveAt(2)
                e.Item.Cells.RemoveAt(1)
                'e.Item.Cells.RemoveAt(0)
                e.Item.Cells(0).ColumnSpan = 10
                e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                e.Item.BackColor = System.Drawing.Color.FromName("#FFFFFF")
            End If

        Catch e1 As Exception
            Me.lbMS_REGI.Text = e1.Message
        End Try
    End Sub
End Class
