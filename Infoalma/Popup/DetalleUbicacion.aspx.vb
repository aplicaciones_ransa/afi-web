Imports System.Data
Imports Depsa.LibCapaNegocio
Public Class DetalleUbicacion
    Inherits System.Web.UI.Page
    Private objBD As clsCNMapa
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents dgUbicacion As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgDetalle As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Page.IsPostBack = False Then
            Dim sCodigoAlmacen As String
            Dim sCodigoBodega As String
            Dim sCodigoUbicacion As String
            Dim sModelo As String
            Dim sColor As String
            Dim sModalidad As String
            Dim sChasis As String

            sCodigoAlmacen = Request.QueryString("co_alma")
            sCodigoBodega = Request.QueryString("co_bode")
            sCodigoUbicacion = Request.QueryString("co_ubic")
            sModalidad = Request.QueryString("moda")
            sModelo = Request.QueryString("mode")
            sColor = Request.QueryString("color")
            sChasis = Request.QueryString("chasis")

            If sCodigoUbicacion.Length = 3 Then
                Me.lblTitulo.Text = "ZONA " & sCodigoUbicacion.Substring(0, 1) & " UBICACI�N " & sCodigoUbicacion.Substring(1, 2)
            Else
                Me.lblTitulo.Text = "El c�digo de ubicaci�n no es correcto"
            End If
            loadData(sCodigoAlmacen, sCodigoBodega, sCodigoUbicacion, sModalidad, sModelo.Replace("/", " "), sColor.Replace("/", " "), sChasis)
        End If
    End Sub

    Private Sub loadData(ByVal sCodigoAlmacen As String, ByVal sCodigoBodega As String, _
                         ByVal sCodigoUbicacion As String, ByVal sModalidad As String, _
                         ByVal sModelo As String, ByVal sColor As String, _
                         ByVal sChasis As String)
        Dim dt As DataTable
        objBD = New clsCNMapa
        Try
            dt = objBD.gGetVehiculosxUbicacion(Session("CoEmpresa"), "001", Session("IdSico"), sCodigoAlmacen, sCodigoBodega, sCodigoUbicacion, _
                                                sModalidad, sModelo, sColor, sChasis)
            Me.dgUbicacion.DataSource = dt
            Me.dgUbicacion.DataBind()
            If dt.Rows.Count <= 1 Then
                Me.lblError.Text = "Se encontr� " & dt.Rows.Count.ToString & " veh�culo"
            Else
                Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " veh�culos"
            End If

        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim dt As DataTable
            objBD = New clsCNMapa
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            dt = objBD.gGetUbicacionesVehiculo(Session("CoEmpresa"), "001", dgi.Cells(5).Text(), _
                                            dgi.Cells(0).Text(), dgi.Cells(1).Text())
            Me.dgDetalle.DataSource = dt
            Me.dgDetalle.DataBind()
            If dt.Rows.Count = 0 Then
                Me.lblMensaje.Text = "No se encontraron registros de ubicaci�n"
            ElseIf dt.Rows.Count = 1 Then
                Me.lblMensaje.Text = "Se encontr� " & dt.Rows.Count.ToString & " registro de ubicaci�n"
            Else
                Me.lblMensaje.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros de ubicaci�n"
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objBD Is Nothing) Then
            objBD = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
