<%@ Page Language="vb" AutoEventWireup="false" CodeFile="poputFueraOficina.aspx.vb" Inherits="poputFueraOficina" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Solicitar Acceso</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="../Styles/Styles.css">
    <script language="javascript" src="../JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

       
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 1540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);
            if (fchIni == "" && fchFin == "") {
                alert("Debe Ingresar fecha Inicial y Final");
                return false;
            }

            if (fchIni > fchFin && fchIni != "" && fchFin != "") {
                alert("La fecha inicial no puede ser mayor a la fecha final");
                return false;
            }
            return true;
        }

        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 24) { //para rel�gio de 12 horas altere o valor aqui
                alert("Horas inv�lidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("MINUTOS inv�lidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inv�lidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="10" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table1" border="0" cellspacing="1" cellpadding="1" width="100%">
            <tr>
                <td valign="top" align="center">
                    <table id="Table20" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="6" background="../Images/table_r1_c1.gif" width="6"></td>
                            <td height="6" background="../Images/table_r1_c2.gif"></td>
                            <td height="6" background="../Images/table_r1_c3.gif" width="6"></td>
                        </tr>
                        <tr>
                            <td background="../Images/table_r2_c1.gif" width="6"></td>
                            <td style="z-index: 0" valign="top" align="center">
                                <!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
                                <p>
                                    <table style="z-index: 0" id="Table2" border="0" cellspacing="1" cellpadding="0">
                                        <tr>
                                            <td style="z-index: 0" class="td" valign="middle" colspan="6">Fuera de oficina</td>
                                        </tr>
                                        <tr>
                                            <td class="Text" colspan="6" align="center">Use esta opci�n para notificar que esta 
													fuera de oficina, de vacaciones o no disponible.</td>
                                        </tr>
                                        <tr>
                                            <td class="Text" colspan="6" align="left">
                                                <asp:CheckBox Style="z-index: 0" ID="chkActivar" runat="server" Text="Activar Fuera de Oficina"
                                                    AutoPostBack="True"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="Text" colspan="6" align="left">
                                                <asp:Panel Style="z-index: 0" ID="PANEL1" runat="server">
                                                    <table style="z-index: 0" border="0" cellspacing="1" cellpadding="0">
                                                        <tr>
                                                            <td class="Text">Fecha inicial</td>
                                                            <td class="Text">:</td>
                                                            <td>
                                                                <input style="z-index: 0" id="txtFechaInicial" class="text" disabled onkeypress="validarcharfecha()"
                                                                    onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="8" name="txtFechaInicial"
                                                                    runat="server"><input style="z-index: 0" id="btnFechaInicial" class="text" 
                                                                        value="..." type="button" name="btnFechaInicial" runat="server"></td>
                                                            <td style="height: 25px" class="Text">Hora</td>
                                                            <td style="height: 25px" class="Text">:</td>
                                                            <td style="height: 25px" class="text" width="80%">
                                                                <input onblur="valida_horas(this)" id="txtHoraInicial" class="Text" disabled onkeypress="dois_pontos(this)"
                                                                    maxlength="5" size="4" name="txtHoraInicial" runat="server">&nbsp;&nbsp;de 
																	0 a 24 horas</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="Text" width="20%">Fecha final</td>
                                                            <td class="Text">:</td>
                                                            <td>
                                                                <input style="z-index: 0" id="txtFechaFinal" class="text" disabled onkeypress="validarcharfecha()"
                                                                    onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="8" name="txtFechaFinal"
                                                                    runat="server"><input style="z-index: 0" id="btnFechaFinal" class="text" 
                                                                        value="..." type="button" name="btnFechaFinal" runat="server">&nbsp;</td>
                                                            <td class="Text">Hora</td>
                                                            <td class="Text">:</td>
                                                            <td width="80%">
                                                                <input onblur="valida_horas(this)" id="txtHoraFinal" class="Text" disabled onkeypress="dois_pontos(this)"
                                                                    maxlength="5" size="4" name="txtHoraFinal" runat="server">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="z-index: 0" class="Text" width="20%">Comentario</td>
                                                            <td class="Text">:</td>
                                                            <td colspan="4"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="Text" colspan="6">
                                                                <asp:TextBox ID="txtComentario" runat="server" Width="100%" CssClass="Text" TextMode="MultiLine"
                                                                    Height="60px" Enabled="False"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="center">
                                                <asp:Label Style="z-index: 0" ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="center">
                                                <asp:Button Style="z-index: 0" ID="btnGrabar" runat="server" Text="Grabar" CssClass="Text" Width="80px"></asp:Button><input style="z-index: 0; width: 80px" class="Text" onclick="Javascript: window.close();"
                                                    value="Cerrar" type="button"></td>
                                        </tr>
                                    </table>
                                </p>
                                <!--</div>-->
                            </td>
                            <td background="../Images/table_r2_c3.gif" width="6"></td>
                        </tr>
                        <tr>
                            <td height="6" background="../Images/table_r3_c1.gif" width="6"></td>
                            <td height="6" background="../Images/table_r3_c2.gif"></td>
                            <td height="6" background="../Images/table_r3_c3.gif" width="6"></td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
