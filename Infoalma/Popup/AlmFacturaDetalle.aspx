<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmFacturaDetalle.aspx.vb" Inherits="AlmFacturaDetalle"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle Comprobante Recepción</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="20" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="middle" align="center">
						<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="700" border="0">
							<TR>
								<TD width="6" background="../Images/table_r1_c1.gif" height="6"></TD>
								<TD background="../Images/table_r1_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r1_c3.gif" height="6"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r2_c1.gif"></TD>
								<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
									<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" width="15%">Nro DCR:</TD>
														<TD class="Text" width="35%"><asp:label id="lblNroDCR" runat="server"></asp:label></TD>
														<TD class="Text" width="15%"></TD>
														<TD class="Text" width="35%"></TD>
													</TR>
												</TABLE>
												<asp:label id="lblError" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><INPUT class="btn" id="Button1" onclick="window.print();" type="button" value="Imprimir"
													name="btnImprimir"><INPUT class="btn" id="Button2" onclick="Javascript:window.close();" type="button" value="Cerrar"
													name="btnCerrar"></TD>
										</TR>
										<TR>
											<TD vAlign="top"><asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="100%" HorizontalAlign="Center"
													BorderColor="Gainsboro" AutoGenerateColumns="False">
													<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
													<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="NU_SECU_MERC" HeaderText="Item">
															<HeaderStyle Width="150px"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="PESO BALANZA" HeaderText="Peso" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Nro.DCR/DOR"></asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA" HeaderText="FecMov" DataFormatString="{0:d}"></asp:BoundColumn>
														<asp:BoundColumn DataField="ENTRADA (UNIDADES)" HeaderText="Uni.Ingr" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn DataField="SALIDA (UNIDADES)" HeaderText="Uni.Ret" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO (UNIDADES)" HeaderText="Uni.Saldo" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="ENTRADA (PESO)" HeaderText="Bul.Ingr" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="SALIDA (PESO)" HeaderText="Bul.Ret" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="SALDO (PESO)" HeaderText="Bul.Saldo" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn DataField="IM_UNIT" HeaderText="P.Unitario" DataFormatString="{0:N2}"></asp:BoundColumn>
														<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda"></asp:BoundColumn>
														<asp:BoundColumn HeaderText="Importe Recibido" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn HeaderText="Importe Retirado" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn HeaderText="Importe Saldo" DataFormatString="{0:N}"></asp:BoundColumn>
														<asp:BoundColumn DataField="OBSERVACION" HeaderText="Observaci&#243;n">
															<HeaderStyle Width="200px"></HeaderStyle>
														</asp:BoundColumn>
													</Columns>
													<PagerStyle CssClass="gvPager"></PagerStyle>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD align="center"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
													name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:window.close();" type="button" value="Cerrar"
													name="btnCerrar"></TD>
										</TR>
									</TABLE> <!--</div>--></TD>
								<TD width="6" background="../Images/table_r2_c3.gif"></TD>
							</TR>
							<TR>
								<TD width="6" background="../Images/table_r3_c1.gif" height="6"></TD>
								<TD background="../Images/table_r3_c2.gif" height="6"></TD>
								<TD width="6" background="../Images/table_r3_c3.gif" height="6"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
