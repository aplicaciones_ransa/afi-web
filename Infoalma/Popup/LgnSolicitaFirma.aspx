<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LgnSolicitaFirma.aspx.vb" Inherits="LgnSolicitaFirma"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Clave</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../Styles/Styles.css">
		<link rel="stylesheet" href="../Styles/jquery-ui.cache.css" type="text/css">
		<script src="../JScripts/jquery.min.cache.js"></script>
		<script src="../JScripts/jquery-ui.min.cache.js"></script>
		<base target="_self">
		<meta content="no-cache" http-equiv="cache-control">
		<meta content="Tue, 01 Jan 1980 1:00:00 GMT" http-equiv="expires">
		<script type="text/javascript">
        // DIALOG Required Code
	        var prntWindow = getParentWindowWithDialog(); //$(top)[0];
	        var $dlg = prntWindow && prntWindow.$dialog;

	        function getParentWindowWithDialog() {
		        var p = window.parent;
		        var previousParent = p;
		        while (p != null) {
			        if ($(p.document).find('#iframeDialog').length) return p;
			        p = p.parent;
			        if (previousParent == p) return null;
			        // save previous parent
			        previousParent = p;
		        }
		        return null;
	        }

	        function setWindowReturnValue(value) {
		        if ($dlg) $dlg.returnValue = value;
		        window.returnValue = value; // in case popup is called using showModalDialog
	        }

	        function getWindowReturnValue() {
		        // in case popup is called using showModalDialog
		        if (!$dlg && window.returnValue != null)
			        return window.returnValue;
		        return $dlg && $dlg.returnValue;
	        }

	        if ($dlg) window.dialogArguments = 'prueba';  //$dlg.dialogArguments;
	        if ($dlg) window.close = function() { if ($dlg) $dlg.dialogWindow.dialog('close'); };
		</script>
		<script language="javascript">
			document.onkeypress=function(e){
				var esIE=(document.all);
				var esNS=(document.layers);
				tecla=(esIE) ? event.keyCode : e.which;
				if(tecla==13){
					enviarInformacion();
				}
				}
			function enviarInformacion()
			{
			var sContrasenaNueva;
			if (! document.forms['Form1'].elements['txtNuevaContrase�a']) { 
				sContrasenaNueva = ""; 
			}  
			else { 
				sContrasenaNueva = document.getElementById('txtNuevaContrase�a').value; 
				if (sContrasenaNueva == "")
				{
					document.getElementById('lblMensaje').innerHTML = "Ingrese una clave nueva";
					return;
				}
			
				if (sContrasenaNueva.length < 6)
				{
					document.getElementById('lblMensaje').innerHTML = "La clave nueva debe tener como m�nimo 6 caracteres";
					return;
				}
									
				var log=sContrasenaNueva.length; 
				var sw=0; 
				for (x=0; x<log; x++) 
				{
					 v1=sContrasenaNueva.substr(x,1); 
					if  (((v1 <= "Z") && (v1 >= "A") )|| ((v1 <= "z") && (v1 >= "a")) || ((v1 >= "0") && (v1 <= "9")))
					{
					sw=sw+0;
					}
				else
					{
					sw=sw+1;
					}
				} 
				
				if (sw==0)
				{
				document.getElementById('lblMensaje').innerHTML = "La clave nueva debe tener al menos un car�cter especial por ejemplo *, $, #, @";
				return;
				} 
				//else
				//{
				//alert('si tiene caracter especial')
				//}
				//return;
			} 
			
			var sContrasena = document.getElementById('txtContrase�a').value;
			//var sContrasenaNueva = document.getElementById('txtNuevaContrase�a').value;
			if (sContrasenaNueva == sContrasena && sContrasenaNueva != "")
			{
				document.getElementById('lblMensaje').innerHTML = "La clave nueva debe ser diferente a la clave anterior";
				return;
			}
				
			if (sContrasena == "")
			{
				document.getElementById('lblMensaje').innerHTML = "Ingrese una clave";
				return;
			}
			
			if (sContrasena.length < 6)
			{
				document.getElementById('lblMensaje').innerHTML = "La clave tiene como m�nimo 6 caracteres";
				return;
			}
						
			//var str1 = document.getElementById('txtContrase�a').value;
			//var str2 = document.getElementById('txtNuevaContrase�a').value;
			var ArgumentosAEnviar = new Array(sContrasena, sContrasenaNueva);
			setWindowReturnValue(ArgumentosAEnviar);
			// pasamos los argumentos de vuelta 
			$dlg.dialogWindow.dialog('close');
			//window.returnValue = ArgumentosAEnviar;
			//window.close();
			}
			
			function Login()
			{
				if(document.layers)
				{
					document.layers["txtContrase�a"].focus();
				}
				if(document.all)
				{
					document.all["txtContrase�a"].focus();
				}
			}
		</script>
	</HEAD>
	<body onload="Login();" bgColor="scrollbar" style="background-color:lightblue;">
		<!--<iframe height="0%" src="LgnSolicitaFirma.aspx" width="0%"></iframe>-->
		<FORM id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="4" cellPadding="0" width="310" height="0%">
				<TR>
					<TD class="Text" width="35%"><asp:label id="lblContrase�a" runat="server"></asp:label></TD>
					<TD width="64%" align="right"><asp:textbox id="txtContrase�a" runat="server" Width="200px" CssClass="Text" TextMode="Password"
							MaxLength="20"></asp:textbox></TD>
					<TD width="1%" align="right"></TD>
				</TR>
				<TR id="trNueva" runat="server">
					<TD class="Text" width="35%"><asp:label id="lblNuevaContrase�a" runat="server"></asp:label></TD>
					<TD width="64%" align="right"><asp:textbox id="txtNuevaContrase�a" runat="server" Width="200px" CssClass="Text" TextMode="Password"
							MaxLength="20"></asp:textbox></TD>
					<TD width="1%" align="right"></TD>
				</TR>
				<TR>
					<TD colSpan="3" align="right"><INPUT style="WIDTH: 80px" class="Text" onclick="Javascript:enviarInformacion();" value="OK"
							type="button" id="btnOK" name="btnOK">&nbsp;<INPUT style="WIDTH: 80px" class="Text" onclick="Javascript:window.close();" value="Cancelar"
							type="button"></TD>
				</TR>
				<TR>
					<TD colSpan="3" align="center">
						<DIV style="DISPLAY: inline" id="lblMensaje" class="Error" ms_positioning="FlowLayout"></DIV>
					</TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
