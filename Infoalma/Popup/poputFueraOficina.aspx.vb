Imports System.Data
Imports Infodepsa

Public Class poputFueraOficina
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim objAcceso As Library.AccesoDB.Acceso = New Library.AccesoDB.Acceso
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnFechaInicial As System.Web.UI.HtmlControls.HtmlInputButton
    'Protected WithEvents btnFechaFinal As System.Web.UI.HtmlControls.HtmlInputButton


    'Protected WithEvents txtComentario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtHoraInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtHoraFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkActivar As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents PANEL1 As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  If (Request.IsAuthenticated) And InStr(Session("PagId"), "") Then
        If Page.IsPostBack = False Then
            Dim strResult As String()
            Dim dttFecha As DateTime
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "DETALLE_FACTURA"), "-")
            Session.Item("Page") = strResult(0)
            Session.Item("Opcion") = strResult(1)
            dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
            Dia = "00" + CStr(dttFecha.Day)
            Mes = "00" + CStr(dttFecha.Month)
            Anio = "0000" + CStr(dttFecha.Year)
            txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
            Bindatagrid()
        End If
        ' Else
        ' Response.Redirect("Salir.aspx?caduco=1")
        ' End If
    End Sub

    Private Sub Bindatagrid()
        Dim dt As DataTable
        Try

            dt = objAcceso.gCADSelFueraOficina(Session.Item("IdUsuario"))
            If dt.Rows.Count <> 0 Then
                Me.txtFechaInicial.Value = dt.Rows(0)("FCH_FOFI_INI")
                Me.txtFechaFinal.Value = dt.Rows(0)("FCH_FOFI_FIN")
                Me.txtHoraInicial.Value = dt.Rows(0)("HOR_FOFI_INI")
                Me.txtHoraFinal.Value = dt.Rows(0)("HOR_FOFI_FIN")
                Me.txtComentario.Text = dt.Rows(0)("DES_FOFI")

                If dt.Rows(0)("FLG_FOFI_EST") = "1" Then
                    Me.chkActivar.Checked = True
                    Habilitar()
                End If
                If dt.Rows(0)("FLG_FOFI_EST") = "0" Then
                    Me.chkActivar.Checked = False
                    DesHabilitar()
                End If

                If Me.chkActivar.Checked = True Then
                    Habilitar()
                Else
                    DesHabilitar()
                End If

                Me.lblMensaje.Text = ""
            End If

        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim dtAct As DataTable
        Try
            Dim strFechIni = txtFechaInicial.Value
            Dim strFechFin = txtFechaFinal.Value
            Dim strHoraIni = txtHoraInicial.Value
            Dim strHoraFin = txtHoraFinal.Value
            Dim strCome = Me.txtComentario.Text

            If strFechIni = "" Then
                Me.lblMensaje.Text = "Ingrese fecha inicial"
                Exit Sub
            ElseIf strFechFin = "" Then
                Me.lblMensaje.Text = "Ingrese fecha final"
                Exit Sub
            ElseIf strHoraIni = "" Then
                Me.lblMensaje.Text = "Ingrese hora inicial"
                Exit Sub
            ElseIf strHoraIni.Length < 5 Then
                Me.lblMensaje.Text = "Ingrese hora inicial valida"
                Exit Sub
            ElseIf strHoraFin = "" Then
                Me.lblMensaje.Text = "Ingrese hora final"
                Exit Sub
            ElseIf strHoraFin.Length < 5 Then
                Me.lblMensaje.Text = "Ingrese hora final valida"
                Exit Sub
            ElseIf strCome = "" Then
                Me.lblMensaje.Text = "Ingrese un comentario"
                Exit Sub
            ElseIf Convert.ToDateTime(strFechIni) > Convert.ToDateTime(strFechFin) Then
                Me.lblMensaje.Text = "Corregir fecha final, debe ser mayor o igual a la fecha inicial"
                Exit Sub
            End If

            If strHoraIni.Length < 3 Then
                strHoraIni = Left(strHoraIni & "00", 2) & ":00"
            End If
            If strHoraIni.Length < 5 And strHoraIni.Length > 2 Then
                strHoraIni = Left(strHoraIni & "00", 5)
            End If

            If strHoraFin.Length < 3 Then
                strHoraFin = Left(strHoraFin & "00", 2) & ":00"
            End If
            If strHoraFin.Length < 5 And strHoraFin.Length > 2 Then
                strHoraFin = Left(strHoraFin & "00", 5)
            End If

            dtAct = objAcceso.gCADUpdFueraOficina(Session.Item("IdUsuario"), strFechIni, strFechFin, strHoraIni, strHoraFin, strCome, Me.chkActivar.Checked)
            If dtAct.Rows.Count <> 0 Then
                Me.lblMensaje.Text = dtAct.Rows(0)("MENSAJE")
            End If

        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try

    End Sub
    Private Sub Habilitar()
        txtFechaInicial.Disabled = False
        txtFechaFinal.Disabled = False
        btnFechaInicial.Disabled = False
        btnFechaFinal.Disabled = False
        txtHoraInicial.Disabled = False
        txtHoraFinal.Disabled = False
        txtComentario.Enabled = True
        Me.PANEL1.Enabled = True
    End Sub
    Private Sub DesHabilitar()
        txtFechaInicial.Disabled = True
        txtFechaFinal.Disabled = True
        btnFechaInicial.Disabled = True
        btnFechaFinal.Disabled = True
        txtHoraInicial.Disabled = True
        txtHoraFinal.Disabled = True
        txtComentario.Enabled = False
        Me.PANEL1.Enabled = False
    End Sub
    Private Sub chkActivar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkActivar.CheckedChanged
        Dim dtActEst As DataTable
        Try
            If Me.chkActivar.Checked = True Then
                Habilitar()
            Else
                DesHabilitar()
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub
End Class
