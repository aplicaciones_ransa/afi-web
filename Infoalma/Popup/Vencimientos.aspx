<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Vencimientos.aspx.vb" Inherits="Vencimientos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Vencimientos</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<!--<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}				
		document.oncontextmenu = function(){return false}
		</script> -->
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" background="../Images/fondo2.jpg">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="top" align="center">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0" bgColor="#f0f0f0">
							<TR>
								<TD class="td">Vencimientos</TD>
							</TR>
							<TR>
								<TD class="Text">A continuación se lista la mercadería o documentos&nbsp;vencidos.</TD>
							</TR>
							<TR>
								<TD align="center"><INPUT class="btn" id="Button1" onclick="window.print();" type="button" value="Imprimir"
										name="btnImprimir"><INPUT class="btn" id="Button2" onclick="Javascript:window.close();" type="button" value="Cerrar"
										name="btnCerrar"></TD>
							</TR>
							<TR>
								<TD class="Subtitulo">DCR con Mercaderia Vencida:</TD>
							</TR>
							<TR>
								<TD>
									<asp:datagrid id="dgDCR" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="Gainsboro"
										HorizontalAlign="Center" CssClass="gv">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Nro.DCR" HeaderText="Nro.DCR">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Modalidad" HeaderText="Modalidad">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaMov." HeaderText="FechaMov.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Dsc.Merc." HeaderText="Dsc.Merc."></asp:BoundColumn>
											<asp:BoundColumn DataField="Sec.Merc." HeaderText="Sec.Merc.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Moneda" HeaderText="Moneda">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Imp.Unit." HeaderText="Imp.Unit.">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Unid" HeaderText="Unid">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Unidades" HeaderText="Unidades">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Valor" HeaderText="Valor">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaVenc" HeaderText="FechaVenc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Almacen" HeaderText="Almacen"></asp:BoundColumn>
										</Columns>
										<PagerStyle CssClass="gvPager"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:label id="lblDCR" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="Subtitulo">Warrants Vencidos:</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 112px">
									<asp:datagrid id="dgWarrant" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="Gainsboro"
										HorizontalAlign="Center" CssClass="gv">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Nro.DCR" HeaderText="Nro.DCR">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Dsc.Merc" HeaderText="Dsc.Merc"></asp:BoundColumn>
											<asp:BoundColumn DataField="TipoDoc" HeaderText="TipoDoc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Nro.Doc" HeaderText="Nro.Warr">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Financiador" HeaderText="Financiador"></asp:BoundColumn>
											<asp:BoundColumn DataField="Fec.Venc" HeaderText="Fec.1er.Venc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Fec.Venc.Limi" HeaderText="Fec.Venc.Limi">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Moneda" HeaderText="Moneda">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Saldo" HeaderText="Saldo">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Almacen" HeaderText="Almacen"></asp:BoundColumn>
										</Columns>
										<PagerStyle CssClass="gvPager"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:label id="lblWarrant" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="Subtitulo">Mercadería Aduanera Vencida:</TD>
							</TR>
							<TR>
								<TD>
									<asp:datagrid id="dgAduanero" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="Gainsboro"
										HorizontalAlign="Center" CssClass="gv">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Nro.DCR" HeaderText="Nro.DCR">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DscMercader&#237;a" HeaderText="DscMercader&#237;a"></asp:BoundColumn>
											<asp:BoundColumn DataField="Fec.Venc" HeaderText="Fec.Venc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Moneda" HeaderText="Moneda">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Saldo" HeaderText="Saldo">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Almacen" HeaderText="Almacen"></asp:BoundColumn>
										</Columns>
										<PagerStyle CssClass="gvPager"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:label id="lblAduanero" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD align="center"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
										name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="window.close();" type="button" value="Cerrar"
										name="btnCerrar"></TD>
							</TR>
							<TR>
								<TD class="text2" align="left">Nota: Sírvase programar el retiro de su mercadería 
									que a la fecha se encuentra vencida. Mucho agradeceremos programar el retiro 
									con nuestro departamento de Administración Documentaria.</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
