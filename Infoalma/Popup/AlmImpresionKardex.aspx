<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmImpresionKardex.aspx.vb" Inherits="AlmImpresionKardex" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlmImpresionKardex</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}	
		</script>
	</HEAD>
	<body onload="window.print();" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
				<TR class="texto7pt" vAlign="top">
					<TD align="center" colSpan="2">
						<asp:label id="Label10" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="8pt"> REPORTE DE K�RDEX</asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD>
						<asp:label id="lbCO_USUA_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD>
						<asp:label id="lbFE_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD></TD>
					<TD>
						<asp:label id="lbHO_REPO_GENE" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
				</TR>
				<TR class="texto7pt" vAlign="top">
					<TD colSpan="2" align="center">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0">
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label9" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">MODALIDAD</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label1" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD colSpan="4">
									<asp:label id="lbNO_MODA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label6" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">ESTADO DCR</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label2" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbEST_DCR" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px">
									<asp:label id="Label4" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">REFERENCIA</asp:label></TD>
								<TD style="WIDTH: 6px">
									<asp:label id="Label5" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD>
									<asp:label id="lbNO_REF" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
							<TR class="texto7pt" vAlign="top">
								<TD style="WIDTH: 97px">
									<asp:label id="Label3" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt"> FEC. DOCUMENTO</asp:label></TD>
								<TD style="WIDTH: 7px">
									<asp:label id="Label7" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD style="WIDTH: 209px">
									<asp:label id="lbFE_EMIS_INIC" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label>-
									<asp:label id="lbFE_EMIS_FINA" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
								<TD style="WIDTH: 103px">
									<asp:label id="Label11" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">COD. PRODUCTO</asp:label></TD>
								<TD style="WIDTH: 6px">
									<asp:label id="Label12" runat="server" Font-Bold="True" Font-Names="verdana,arial" Font-Size="7pt">:</asp:label></TD>
								<TD>-
									<asp:label id="lbCO_PRODU" runat="server" Font-Names="verdana,arial" Font-Size="7pt"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="lbMS_REGI" runat="server" CssClass="text" ForeColor="Red"></asp:label>
					</TD>
				</TR>
				<TR>
					<TD align="center" colSpan="2">
						<asp:datagrid id="dgResultado" runat="server" Width="90%" AutoGenerateColumns="False" CssClass="gv"
							BorderColor="Gainsboro">
							<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle CssClass="gvRow"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="FE_MOVI" HeaderText="Fecha">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DOR">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="DE_TIPO_BULT" HeaderText="Unidad">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_RECI" HeaderText="Unid.Entrada" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Uni.Salida" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_UNID_SALD" HeaderText="Uni.Saldo" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_UNME_PESO" HeaderText="Bulto" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_RECI" HeaderText="Bul.Entrada" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Bul.Salida" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_PESO_SALD" HeaderText="Bul.Saldo" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="IM_REMO_INGR" HeaderText="Imp.Entrada" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="IM_REMO_RETI" HeaderText="Imp.Salida" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="IM_REMO_SALD" HeaderText="Imp.Saldo" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:BoundColumn DataField="NU_GUIA" HeaderText="Nro.Guia"></asp:BoundColumn>
								<asp:BoundColumn DataField="DE_OBSE" HeaderText="Observacion"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="ST_SALI_ALMA" HeaderText="Estado"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
							name="btnImprimir"> <INPUT class="btn" id="btnCerrar" style="WIDTH: 80px" onclick="window.close();" type="button"
							value="Cerrar" name="btnCerrar"></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
