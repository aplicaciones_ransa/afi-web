Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Infodepsa
Imports System.Data

Public Class EnviarMailEntidad
    Inherits System.Web.UI.Page
    Private objRuta As Ruta = New Ruta
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgdTipoEntidad As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDescripcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdControles As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_RUTA") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_RUTA"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblDescripcion.Text = Request.Item("Desc")
                loadTipoEntidad()
                BindDatagrid(Request.Item("Secu"), Request.Item("Ruta"), Request.Item("TipDoc"))
                BindDatagridControles(Request.Item("Secu"), Request.Item("Ruta"), Request.Item("TipDoc"))
                If Session.Item("IdTipoUsuario") = "04" Then 'Solo el Administrador de Alma Per� podra configurar 
                    Me.btnGrabar.Enabled = True               'las entidades que recibiran notificaci�n por secuencia
                Else
                    Me.btnGrabar.Enabled = False
                End If
            End If
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 0)
    End Sub

    Private Sub BindDatagrid(ByVal strSecuencia As String, ByVal strRuta As String, ByVal strTipDoc As String)
        Dim dt As DataTable
        Try
            dt = objRuta.gGetEnvioMailxTipoEntidad(strSecuencia, strRuta, strTipDoc)
            Me.dgdTipoEntidad.DataSource = dt
            Me.dgdTipoEntidad.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub

    Private Sub BindDatagridControles(ByVal strSecuencia As String, ByVal strRuta As String, ByVal strTipDoc As String)
        Dim dt As DataTable
        Try
            dt = objRuta.gGetControlesxTipoUsuario(strSecuencia, strRuta, strTipDoc, Me.cboTipoEntidad.SelectedValue)
            Me.dgdControles.DataSource = dt
            Me.dgdControles.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub

    Private Sub InsertarEnvioMail()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strCodTipoEntidad As String
            Dim blnEnvio As Boolean
            Dim dgItem As DataGridItem
            For i As Integer = 0 To Me.dgdTipoEntidad.Items.Count - 1
                dgItem = Me.dgdTipoEntidad.Items(i)
                strCodTipoEntidad = dgItem.Cells(0).Text()
                blnEnvio = CType(dgItem.FindControl("chkMail"), CheckBox).Checked
                objRuta.gInsEnvioMail(strCodTipoEntidad, Request.Item("Secu"), Request.Item("Ruta"), Request.Item("TipDoc"), Session.Item("IdUsuario"), blnEnvio, objTrans)
            Next i
            objTrans.Commit()
            objTrans.Dispose()
            Me.lblError.Text = "Se actualiz� correctamente"
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Ocurrio el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub InsertarAccesoControles()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strIdControl As String
            Dim blnUsrComun As Boolean
            Dim blnUsrConsultor As Boolean
            Dim blnUsrAprobador As Boolean
            Dim dgItem As DataGridItem
            For i As Integer = 0 To Me.dgdControles.Items.Count - 1
                dgItem = Me.dgdControles.Items(i)
                strIdControl = dgItem.Cells(0).Text()
                blnUsrComun = CType(dgItem.FindControl("chkComun"), CheckBox).Checked
                blnUsrConsultor = CType(dgItem.FindControl("chkConsultor"), CheckBox).Checked
                blnUsrAprobador = CType(dgItem.FindControl("chkAprobador"), CheckBox).Checked
                objRuta.gInsAccesoControles(strIdControl, Request.Item("Secu"), Request.Item("Ruta"), _
                Request.Item("TipDoc"), Me.cboTipoEntidad.SelectedValue, Session.Item("IdUsuario"), _
                blnUsrComun, blnUsrConsultor, blnUsrAprobador, objTrans)
            Next i
            objTrans.Commit()
            objTrans.Dispose()
            Me.lblError.Text = "Se actualiz� correctamente"
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Ocurrio el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        InsertarEnvioMail()
        InsertarAccesoControles()
    End Sub

    Private Sub cboTipoEntidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoEntidad.SelectedIndexChanged
        BindDatagridControles(Request.Item("Secu"), Request.Item("Ruta"), Request.Item("TipDoc"))
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRuta Is Nothing) Then
            objRuta = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
