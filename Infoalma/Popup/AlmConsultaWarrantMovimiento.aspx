<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmConsultaWarrantMovimiento.aspx.vb" Inherits="AlmConsultaWarrantMovimiento"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AlmConsultaWarrantMovimiento</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
			function Cerrar()
				{			
				window.close();
		}	
		</script>
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="420" border="0">
				<TR>
					<TD vAlign="top">
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="td" width="20%">Warrant Nro:</TD>
								<TD class="Text" width="30%">
									<asp:Label id="lblNroWarrant" runat="server"></asp:Label></TD>
								<TD class="td" width="20%">Valor Origen:</TD>
								<TD class="Text" width="30%">
									<asp:Label id="lblValor" runat="server"></asp:Label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top">
						<asp:datagrid id="dgResultado" runat="server" AutoGenerateColumns="False" BorderColor="Gainsboro"
							HorizontalAlign="Center" Width="100%" CssClass="gv">
							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="DOCUMENTO" HeaderText="Nro.DOR"></asp:BoundColumn>
								<asp:BoundColumn DataField="FECHA" HeaderText="Fec.Retiro" DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="MONEDA" HeaderText="Mon."></asp:BoundColumn>
								<asp:BoundColumn DataField="VALOR RETIRO" HeaderText="Val.Retiro" DataFormatString="{0:N}"></asp:BoundColumn>
								<asp:BoundColumn DataField="MONEDA" HeaderText="Mon."></asp:BoundColumn>
								<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:N}"></asp:BoundColumn>
							</Columns>
							<PagerStyle CssClass="gvPager"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="center"><INPUT class="btn" id="btnImprimir" onclick="window.print();" type="button" value="Imprimir"
							name="btnImprimir"><INPUT class="btn" id="btnCerrar" onclick="Javascript:Cerrar();" type="button" value="Cerrar"
							name="btnCerrar"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
