Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class EstadoRequerimiento
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents txtFechaDesde As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaHasta As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboPersonaAutorizada As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroRequerimiento As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG12") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargaFechas()
                    CargarCombos()
                    LlenaGrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                        Session.Item("NombreEntidad"), "C", "DEPSAFIL", "CONSULTAR ESTADO REQUERIMIENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgdResultado_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgdResultado.SortCommand
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    'Private Sub LlenaGrid()
    '    Try
    '        objRequ = New LibCapaNegocio.clsCNRequerimiento
    '        objFuncion = New LibCapaNegocio.clsFunciones
    '        dt = objRequ.gdtMostrarEstado(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
    '                cboArea.SelectedItem.Value, txtNroRequerimiento.Text, cboEstado.SelectedItem.Value, _
    '                cboPersonaAutorizada.SelectedItem.Value, txtFechaDesde.Value, txtFechaHasta.Value)
    '        objFuncion.gCargaGrid(dgdResultado, dt)
    '        lblRegistros.Text = objRequ.intFilasAfectadas
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub


    Private Sub LlenaGrid()
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            objFuncion = New LibCapaNegocio.clsFunciones
            dt = objRequ.gdtMostrarEstado(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, txtNroRequerimiento.Text, cboEstado.SelectedItem.Value,
cboPersonaAutorizada.SelectedItem.Value, txtFechaDesde.Value, txtFechaHasta.Value, Session.Item("UsuarioLogin"))
            objFuncion.gCargaGrid(dgdResultado, dt)
            lblRegistros.Text = objRequ.intFilasAfectadas
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("CodRequerimiento", dgi.Cells(0).Text())
            Session.Add("PaginaAnterior", "EstadoRequerimiento.aspx")
            Response.Redirect("ConsultasxRequerimiento.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        CargaPersonaAutorizada()
    End Sub

    Private Sub CargaPersonaAutorizada()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboPersonaAutorizada(cboPersonaAutorizada, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        objComun = Nothing
        objRequ = Nothing
    End Sub

    Private Sub CargaFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        Me.txtFechaDesde.Value = objFuncion.gstrFechaMes(0)
        Me.txtFechaHasta.Value = objFuncion.gstrFechaMes(1)
    End Sub

    'Private Sub CargarCombos()
    '    objComun = New LibCapaNegocio.clsCNComun
    '    objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))
    '    objComun.gCargarComboSituacion(cboEstado)
    '    objComun.gCargarComboPersonaAutorizada(cboPersonaAutorizada, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
    'End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        'objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr''(Session.Item("IdUsuario")), "*")

        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")


        objComun.gCargarComboSituacion(cboEstado)
        objComun.gCargarComboPersonaAutorizada(cboPersonaAutorizada, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
