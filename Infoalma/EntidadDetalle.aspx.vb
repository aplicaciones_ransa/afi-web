Imports System.Data
Imports System.Data.SqlClient
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports Library.AccesoDB
Public Class EntidadDetalle
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    'Protected WithEvents chkProtesto As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtTasa As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkLibMultCustodia As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkFirmaDigital As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkAprCli As System.Web.UI.WebControls.CheckBox
    Private objAccesoWeb As clsCNAccesosWeb

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCodigoSICO As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboTipoDocumento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents pnlEntAsociadas As System.Web.UI.WebControls.Panel
    'Protected WithEvents btnEliminar As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents lbxAsociadas As System.Web.UI.WebControls.ListBox
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents cboDepositantes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNombreEntidad As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroDias As System.Web.UI.WebControls.TextBox
    'Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents txtOperaci�n As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkFirma As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtIpIni1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpIni2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpIni3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpIni4 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpFin1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpFin2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpFin3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtIpFin4 As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        'Put user code to initialize the page here 
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_ENTIDAD") Then
            Me.lblError.Text = ""
            Me.lblMensaje.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_ENTIDAD"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.pnlEntAsociadas.Visible = False
                loadTipoDocumento()
                loadTipoEntidad()
                loadDataFirmaxEntidad()
                BindDatagrid()
                Me.cboTipoEntidad.Enabled = False
                Me.txtCodigoSICO.Enabled = False
                Me.txtNombreEntidad.Enabled = False
                If Session.Item("IdTipoUsuario") = "04" Then 'Solo el Administrador de Alma Per� podra grabar el detalle de la configuraci�n por entidad 
                    Me.btnGrabar.Enabled = True
                    Me.cboDepositantes.Enabled = True
                    Me.lbxAsociadas.Enabled = True
                    Me.txtTasa.Enabled = True
                    Me.txtOperaci�n.Enabled = True
                    Me.txtNroDias.Enabled = True
                    Me.chkFirma.Enabled = True
                    Me.chkAprCli.Enabled = True
                    Me.chkProtesto.Enabled = True
                    Me.chkFirmaDigital.Enabled = True
                    Me.chkLibMultCustodia.Enabled = True
                Else
                    Me.btnGrabar.Enabled = False
                    Me.btnAgregar.Enabled = False
                    Me.btnEliminar.Enabled = False
                    Me.txtTasa.Enabled = False
                    Me.txtOperaci�n.Enabled = False
                    Me.txtNroDias.Enabled = False
                    Me.chkFirma.Enabled = False
                    Me.chkAprCli.Enabled = False
                    Me.chkProtesto.Enabled = False
                    Me.chkFirmaDigital.Enabled = False
                    Me.chkLibMultCustodia.Enabled = False
                End If
                If Me.cboTipoEntidad.SelectedValue = "03" Then
                    Me.pnlEntAsociadas.Visible = True
                    loadEntidad()
                    loadAsociados()
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadTipoDocumento()
        objFunciones.GetTipoDocumento(Me.cboTipoDocumento, 0)
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As DataTable
        Try
            Me.cboDepositantes.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades(Me.cboTipoEntidad.SelectedValue, "")
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositantes.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadAsociados()
        Dim dtAsociados As DataTable
        Try
            Me.lbxAsociadas.Items.Clear()
            dtAsociados = objEntidad.gGetAsociados(Session.Item("IdSicoEntidad"))
            For Each dr As DataRow In dtAsociados.Rows
                Me.lbxAsociadas.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        Finally
            dtAsociados.Dispose()
            dtAsociados = Nothing
        End Try
    End Sub

    Private Sub loadDataFirmaxEntidad()
        Try
            Dim drEntidad As DataRow
            drEntidad = objEntidad.gGetDataEntidad(Session.Item("IdSicoEntidad"), Session.Item("IdTipEnti"))
            If Not IsDBNull(drEntidad("NOM_ENTI")) Then
                Me.txtNombreEntidad.Text = CType(drEntidad("NOM_ENTI"), String).Trim
            End If
            If Not IsDBNull(drEntidad("TAS_INTE")) Then
                Me.txtTasa.Text = drEntidad("TAS_INTE")
            End If
            If Not IsDBNull(drEntidad("FLG_PROT")) Then
                Me.chkProtesto.Checked = drEntidad("FLG_PROT")
            End If
            If Not IsDBNull(drEntidad("DIA_VENC")) Then
                Me.txtNroDias.Text = drEntidad("DIA_VENC")
            End If
            If Not IsDBNull(drEntidad("DSC_OPER")) Then
                Me.txtOperaci�n.Text = drEntidad("DSC_OPER")
            End If
            If Not IsDBNull(drEntidad("NRO_RUC")) Then
                Me.txtCodigoSICO.Text = CType(drEntidad("NRO_RUC"), String).Trim
            End If
            If Not IsDBNull(drEntidad("COD_TIPENTI")) Then
                Me.cboTipoEntidad.SelectedValue = CType(drEntidad("COD_TIPENTI"), String).Trim
            End If
            If Not IsDBNull(drEntidad("FLG_FIRM")) Then
                Me.chkFirma.Checked = drEntidad("FLG_FIRM")
            End If
            If Not IsDBNull(drEntidad("FLG_CERT")) Then
                Me.chkFirmaDigital.Checked = drEntidad("FLG_CERT")
            End If
            If Not IsDBNull(drEntidad("FLG_LIBMULT")) Then
                Me.chkLibMultCustodia.Checked = drEntidad("FLG_LIBMULT")
            End If
            If drEntidad("FLGFIRMDEP") <> 0 Then
                Me.chkAprCli.Checked = drEntidad("FLGFIRMDEP")
            End If

            If Not IsDBNull(drEntidad("RAN_IPINIC")) Then
                txtIpIni1.Text = Mid(drEntidad("RAN_IPINIC"), 1, 3)
                txtIpIni2.Text = Mid(drEntidad("RAN_IPINIC"), 4, 3)
                txtIpIni3.Text = Mid(drEntidad("RAN_IPINIC"), 7, 3)
                txtIpIni4.Text = Mid(drEntidad("RAN_IPINIC"), 10, 3)
            End If

            If Not IsDBNull(drEntidad("RAN_IPFINA")) Then
                txtIpFin1.Text = Mid(drEntidad("RAN_IPFINA"), 1, 3)
                txtIpFin2.Text = Mid(drEntidad("RAN_IPFINA"), 4, 3)
                txtIpFin3.Text = Mid(drEntidad("RAN_IPFINA"), 7, 3)
                txtIpFin4.Text = Mid(drEntidad("RAN_IPFINA"), 10, 3)
            End If

            Session.Add("TipoEntidadActualizar", drEntidad("COD_TIPENTI"))
            drEntidad = Nothing
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        End Try
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 1)
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objEntidad.gGetFirmaxEntidad(Session.Item("IdSicoEntidad"), Session.Item("TipoEntidadActualizar"), Me.cboTipoDocumento.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        UpdateEventosxEntidad()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                            Session.Item("NombreEntidad"), "M", "WARRANTE", "GRABAR DETALLE DE ENTIDAD", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub UpdateEventosxEntidad()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction

        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        If Me.txtNroDias.Text.Length > 8 Then
            Me.lblError.Text = "El n�mero de d�as para calcular la fecha de vencimiento es demasiado extenso"
            Exit Sub
        End If
        Try
            Dim strNroSecu As String
            Dim strNroRuta As String
            Dim strCntAprobadores As String
            Dim dgItem As DataGridItem

            Dim ipIni As String
            Dim ipFin As String

            ipIni = Right("000" + txtIpIni1.Text, 3) & Right("000" + txtIpIni2.Text, 3) & Right("000" + txtIpIni3.Text, 3) & Right("000" + txtIpIni4.Text, 3)
            ipFin = Right("000" + txtIpFin1.Text, 3) & Right("000" + txtIpFin2.Text, 3) & Right("000" + txtIpFin3.Text, 3) & Right("000" + txtIpFin4.Text, 3)

            'Dim strTasa As String
            'Dim strDecimal As String
            'If txtTasa.Value = "" Then
            '    strDecimal = "00"
            '    strTasa = "0"
            'ElseIf txtTasa.Value.Length = 1 Or txtTasa.Value.Length = 2 Then
            '    strDecimal = "00"
            '    strTasa = Me.txtTasa.Value
            'Else
            '    strDecimal = Me.txtTasa.Value.Substring(Len(txtTasa.Value) - 2, 2)
            '    strDecimal = strDecimal.Replace(".", "")
            '    strDecimal = strDecimal.Replace(",", "")
            '    strTasa = Me.txtTasa.Value.Substring(0, Len(txtTasa.Value) - 3)
            '    strTasa = strTasa.Replace(".", "")
            '    strTasa = strTasa.Replace(",", "")
            'End If
            For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(i)
                strNroSecu = dgItem.Cells(0).Text()
                strNroRuta = dgItem.Cells(3).Text()
                strCntAprobadores = CType(dgItem.FindControl("txtNroFirmas"), HtmlInputText).Value
                If strCntAprobadores = "" Then
                    strCntAprobadores = "0"
                End If
                objEntidad.gUpdEventosxEntidad(Session.Item("IdSicoEntidad"), strNroSecu, strNroRuta, _
                Me.cboTipoDocumento.SelectedValue, strCntAprobadores, Session.Item("IdUsuario"), _
                Session.Item("IdTipEnti"), objTrans)
            Next i
            objEntidad.gUpdEntidad(Session.Item("IdSicoEntidad"), Session.Item("IdUsuario"), _
                Session.Item("IdTipEnti"), Me.txtTasa.Text, Me.txtOperaci�n.Text, _
                Me.txtNroDias.Text, Me.chkFirma.Checked, Me.chkProtesto.Checked, _
                Me.chkFirmaDigital.Checked, Me.chkLibMultCustodia.Checked, ipIni, ipFin, Me.chkAprCli.Checked, objTrans)
            objTrans.Commit()
            objTrans.Dispose()
            Me.lblError.Text = "Se actualiz� correctamente"
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Se produjo el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged
        BindDatagrid()
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar.Click
        Dim strRespuesta As String
        If Me.cboDepositantes.SelectedValue <> Session.Item("IdSicoEntidad") Then
            strRespuesta = objEntidad.gInsAsociados(Session.Item("IdSicoEntidad"), Me.cboDepositantes.SelectedValue)
            If strRespuesta = "0" Then
                loadAsociados()
            Else
                Me.lblMensaje.Text = "La entidad depositante seleccionado ya esta incluido"
            End If
        Else
            Me.lblMensaje.Text = "Seleccione una entidad depositante distinta"
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEliminar.Click
        Dim strRespuesta As String
        If Me.lbxAsociadas.SelectedIndex > -1 Then
            strRespuesta = objEntidad.gDelAsociados(Session.Item("IdSicoEntidad"), Me.lbxAsociadas.SelectedValue)
            If strRespuesta = "0" Then
                loadAsociados()
            Else
                Me.lblMensaje.Text = "La entidad asociada que ha seleccionado no se encontr�"
            End If
        Else
            Me.lblMensaje.Text = "Seleccione una entidad asociada de la lista"
        End If
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("EntidadBusqueda.aspx")
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
