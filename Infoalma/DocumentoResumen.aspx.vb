Imports System.Data
Imports System.Data.SqlClient
Imports Infodepsa
Imports Library.AccesoDB
Public Class Visor
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    'Protected WithEvents txtTimeStamp As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents LtrFirma As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents ltrExpediente As System.Web.UI.WebControls.Literal
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")


    'Private oHashedData As New CAPICOM.HashedData
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents cntFirmas As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents txtArchivo_pdf As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtArchivo_pdf_firmado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrFirmar1 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar2 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrVisor As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtNumSerie As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblFirma As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroSerie As System.Web.UI.HtmlControls.HtmlInputText
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Dim dtControles As New DataTable
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "1"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
            End If
            Try
                Dim var As Integer
                var = Request.QueryString("var")
                Me.txtNroPagina.Value = var
                txtNumSerie.Value = Session.Item("NroCertificado")
                Me.lblFirma.Text = ""
                Me.txtTimeStamp.Value = Now.ToString("dd/MM/yyyy hh:mm:ss tt")
                If var = 1 Then
                    Dim listaAux As String
                    Dim drWarrant As DataRow
                    Dim strHTMLTable As String
                    Dim arrLista()
                    Dim arrDatosFila()
                    Dim i As Integer
                    '------------------------------
                    'oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
                    '-------------------------------
                    Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visor.aspx?var=1' runat='server'></iframe>"
                    drWarrant = objDocumento.gSelArchivoPDF(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), var)

                    If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                        txtArchivo_pdf.Value = Convert.ToBase64String(drWarrant("DOC_PDFFIRM"))
                    Else
                        ''----------------------------
                        'oHashedData.Hash(Convert.ToBase64String(drWarrant("DOC_PDFORIG")))
                        'Me.txtArchivo_pdf.Value = oHashedData.Value
                        '----------------------------
                    End If

                    'If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                    '    listaAux = listaFirmas(drWarrant("DOC_PDFFIRM"), drWarrant("DOC_PDFORIG"), drWarrant("FCH_MODI"))
                    '    strHTMLTable = "<TABLE WIDTH='100%' BORDER='0' CELLSPACING='4' CELLPADDING='0' bgColor='#f0f0f0'>" _
                    '        & "<THEAD><TR><TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>FIRMADO DIGITALMENTE POR:</TD>" _
                    '        & "<TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>TIMESTAMP LOCAL</TD></TR></THEAD><TBODY>"
                    '    If listaAux = "" Then
                    '        strHTMLTable = strHTMLTable & "<TR><TD colspan='2'>" _
                    '            & UCase("El documento no ha sido firmado o las firmas no son v�lidas</TD></TR>")
                    '    ElseIf listaAux = "X" Then
                    '        strHTMLTable = strHTMLTable & "<TR><TD colspan='2'>" _
                    '                    & UCase("El documento ha sido modificado</TD></TR>")
                    '    Else
                    '        arrLista = Split(listaAux, ";")
                    '        For i = 0 To UBound(arrLista)
                    '            arrDatosFila = Split(arrLista(i), "*")
                    '            strHTMLTable = strHTMLTable & "<TR><TD width='60%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & arrDatosFila(0) & "</TD>" _
                    '                        & "<TD width='40%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & Split(arrDatosFila(1), "|")(0) & "</TD></TR>"
                    '        Next
                    '    End If
                    '    strHTMLTable = strHTMLTable & "</TBODY></TABLE>"
                    '    cntFirmas.InnerHtml = strHTMLTable
                    'End If
                    '// listar todos los firmantes  nombres , fecha en la que firmo  wfuserx firmar

                    'If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                    strHTMLTable = "<TABLE WIDTH='100%' BORDER='0' CELLSPACING='4' CELLPADDING='0' bgColor='#f0f0f0'>" _
                    & "<THEAD><TR><TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>FIRMADO DIGITALMENTE POR:</TD>" _
                    & "<TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>TIMESTAMP LOCAL</TD></TR></THEAD><TBODY>"
                    Dim dtFirmantes As DataTable
                    dtFirmantes = objDocumento.BLGetDatosFirmantes(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
                    For Each dr As DataRow In dtFirmantes.Rows
                        strHTMLTable = strHTMLTable & "<TR><TD width='60%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("NOMBRES") & "</TD>" _
                                            & "<TD width='40%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("FECHA") & "</TD></TR>"
                    Next
                    strHTMLTable = strHTMLTable & "</TBODY></TABLE>"
                    LtrFirma.Text = strHTMLTable
                    'End If
                End If

                If var = 2 Then
                    Dim listaAux As String
                    Dim drWarrant As DataRow
                    Dim strHTMLTable As String
                    Dim arrLista()
                    Dim arrDatosFila()
                    Dim i As Integer
                    Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visor.aspx?var=1' runat='server'></iframe>"
                End If

                dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroDoc"), _
                                                            Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), var)

                Dim strfirma As String

                For j As Integer = 0 To dtControles.Rows.Count - 1
                    Select Case dtControles.Rows(j)("COD_CTRL")
                        Case "01"
                            If Session.Item("strcontrasenia") <> Nothing Then
                                If DateDiff(DateInterval.Minute, Session.Item("strfecha"), Now) > 5 Then
                                    strfirma = "X"
                                Else
                                    strfirma = "F"
                                    Me.txtContrasena.Value = Session.Item("strContrasenia")
                                End If
                            Else
                                strfirma = "X"
                            End If
                            If Request.QueryString("var") = 1 Then
                                If Session.Item("CertDigital") = True Then
                                    Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(1)' type='button' value='Firmar' name = 'cmdFirmar'>"
                                Else
                                    If strfirma = "X" Then
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                    Else
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                    End If
                                End If
                            End If
                        Case "07"
                            If Session.Item("strcontrasenia") <> Nothing Then
                                If DateDiff(DateInterval.Minute, Session.Item("strfecha"), Now) > 5 Then
                                    strfirma = "X"
                                Else
                                    strfirma = "F"
                                    Me.txtContrasena.Value = Session.Item("strContrasenia")
                                End If
                            Else
                                strfirma = "X"
                            End If
                            If Request.QueryString("var") = 2 Then
                                If Session.Item("CertDigital") = True Then
                                    Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(2)' type='button'	value='Firmar' name='cmdFirmar2'>"
                                Else
                                    If strfirma = "X" Then
                                        Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar2'>"
                                    Else
                                        Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar2'>"
                                    End If
                                End If
                            End If
                        Case "00"
                            Me.lblFirma.Text = "Otro usuario esta intentando firmar el documento, intente en otro momento"
                    End Select
                Next

                Dim drUsuario As DataRow
                drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
                If (IsDBNull(drUsuario("ENT_FINA")) Or drUsuario("ENT_FINA") = "" Or drUsuario("ENT_FINA") = "0") And Session.Item("IdTipoEntidad") = "03" And Session.Item("EstadoDoc") = "01" Then
                    Session.Add("Mensaje", "Grabe un endosatario para el warrant antes de verificar")
                    Me.ltrFirmar1.Text = ""
                End If
                If (drUsuario("COD_ENTASOC") = "" And Session.Item("IdTipoEntidad") = "03" And drUsuario("FLG_DBLENDO") = True) Then
                    Session.Add("Mensaje", "Seleccione una entidad asociada antes de verificar")
                    Me.ltrFirmar1.Text = ""
                End If
                Me.lblError.Text = Session.Item("Mensaje")
                Session.Add("Mensaje", "")
                If Session.Item("IdTipoEntidad") = "01" And (Session.Item("IdTipoDocumento") = "01" Or Session.Item("IdTipoDocumento") = "04" Or Session.Item("IdTipoDocumento") = "03" Or Session.Item("IdTipoDocumento") = "10") Then
                    Dim strWarrant As String()
                    Dim strLink As String
                    If (Session.Item("IdTipoDocumento") = "01" Or Session.Item("IdTipoDocumento") = "04") Then
                        strWarrant = Split(CStr(Session.Item("NroDoc")), "-")
                    End If
                    If (Session.Item("IdTipoDocumento") = "03" Or Session.Item("IdTipoDocumento") = "10") Then
                        strWarrant = Split(Session.Item("DscMerc"), "-")
                    End If
                    For Each strWar As String In strWarrant
                        strLink = strLink & "<A href='http://10.72.3.138/filemasterweb/warrant.aspx?IDServicioCliente=4&WAR=" & strWar & "&Grupo=FILEMAST'  Class='Ayuda' Target='_search'>VerExpediente:" & strWar & "</A><BR>"
                        'strLink = strLink & "<A href='http://181.65.210.150/filemasterweb/warrant.aspx?IDServicioCliente=4&WAR=" & strWar & "&Grupo=FILEMAST'  Class='Ayuda' Target='_search'>VerExpediente:" & strWar & "</A><BR>"
                    Next
                    Me.ltrExpediente.Text = strLink
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            Finally
                dtControles.Dispose()
                dtControles = Nothing
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    'Function listaFirmas(ByVal prmArchivo As Byte(), ByVal prmArchivoOriginal As Byte(), ByVal strFechaModi As DateTime) As String
    '    Try
    '        Dim SignedData As CAPICOM.SignedData
    '        Dim Signer As CAPICOM.Signer
    '        Dim strLista As String
    '        Dim strFila As String
    '        Dim Atrib As CAPICOM.Attribute
    '        Dim strNombreArchivo As String
    '        Dim strTimeStamp As String
    '        Dim Contenido As String
    '        Dim mstream As System.IO.MemoryStream
    '        '------------------------------
    '        oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
    '        '-----------------------------
    '        SignedData = New CAPICOM.SignedData
    '        Contenido = Convert.ToBase64String(prmArchivo)
    '        SignedData.Verify(Contenido, False, CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_ONLY)
    '        '-------------------------------
    '        Dim fechaAux As Date
    '        fechaAux = Format("MM/dd/yyyy", ConfigurationManager.AppSettings.Item("FechaCambioValidador"))
    '        If DateDiff(("d"), fechaAux, Date.Now) > 0 Then
    '            oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
    '            If oHashedData.Value <> SignedData.Content Then
    '                listaFirmas = "X"
    '                Exit Function
    '            End If
    '        End If
    '        'If strFechaModi > CType(ConfigurationSettings.AppSettings.Item("FechaCambioValidador"), DateTime) Then
    '        '    oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
    '        '    If oHashedData.Value <> SignedData.Content Then
    '        '        listaFirmas = "X"
    '        '        Exit Function
    '        '    End If
    '        'End If
    '        '--------------------------------
    '        listaFirmas = ""
    '        strLista = ""
    '        Signer = New CAPICOM.Signer
    '        For Each Signer In SignedData.Signers
    '            strFila = Signer.Certificate.GetInfo(0) & "*"
    '            For Each Atrib In Signer.AuthenticatedAttributes
    '                If Atrib.Name = 2 Thenhttp://localhost:61473/assets/
    '                    strTimeStamp = Atrib.Value
    '                    strFila = strFila & strTimeStamp
    '                End If
    '            Next
    '            strLista = strLista & strFila & ";"
    '        Next
    '        listaFirmas = Mid(UCase(strLista), 1, Len(UCase(strLista)) - 1)
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.ToString
    '    End Try
    'End Function

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
