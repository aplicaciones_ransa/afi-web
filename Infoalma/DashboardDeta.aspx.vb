Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports System.Text
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class DashboardDeta
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    Dim strAlmacen As String
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Protected WithEvents cboTipoTitulo As System.Web.UI.WebControls.DropDownList
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strCoEmpresa = ConfigurationManager.AppSettings.Item("CoEmpresa")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents FilePDF As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacenes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnStock As System.Web.UI.WebControls.Button
    'Protected WithEvents trAlmacen As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnGrabar As RoderoLib.BotonEnviar
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHoraIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkReemplazo As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put User code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "42") Then
            If Not IsPostBack Then
                'Me.ltrCodDoc.Text = Session.Item("Cod_Doc").trim
                'Me.ltrNroDocu.Text = Session.Item("NroDoc").trim
                'Me.ltrTipDoc.Text = Session.Item("TipDoc").trim

                '    Me.lblError.Text = ""
                '    If Not IsPostBack Then
                '        Dim strResult As String()
                '        strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "38"), "-")
                '        Session.Item("Page") = strResult(0)
                '        Session.Item("Opcion") = strResult(1)
                '   Me.lblOpcion.Text = "Solicitud de Warrants"

                ' Me.lblOpcion.Text = "Ver Detalle"
                'Me.lblTipoSol.Text = "Solicitud:0"

                'loadFinanciador()
                'loadAsociados()
                'loadTipoWarrant()
                'loadMoneda()

                ''=== carga por defecto ===
                'Me.cboTipoWarrant.SelectedValue = "013"
                'Me.cboTipoDocu.SelectedValue = "3"
                'Me.cboEndoso.SelectedValue = "0"
                'Me.cboTipoAlmacen.SelectedValue = "002"
                'validarEndoso()
                'loadAlmacen()
                ''        If Me.trAlmacen.Visible = False Then
                '            Me.cboAlmacen.Enabled = True
                '        End If
                '        'Me.tblAdjuntar.Visible = False
                '        loadAlmacen()

                'If Me.ltrCodDoc.Text <> "" Then
                '    Me.btnAgregar.Visible = True
                '    BindDatagrid()
                'Else
                '    Me.btnAgregar.Visible = False
                'End If
                'LisTarDetalle()
                'validarTipoWarrant()
                ''        Dim Dia As String
                '        Dim Mes As String
                '        Dim Anio As String
                '        Dim dttFecha As DateTime
                '        dttFecha = Date.Today
                '        Dia = "00" + CStr(dttFecha.Day)
                '        Mes = "00" + CStr(dttFecha.Month)
                '        Anio = "0000" + CStr(dttFecha.Year)
                '        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                '        Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de registrar la solicitud de warrants?')== false) return false;")


                'txtAnioDua.Attributes.Add("onClick", "Javascript:validardecimal()")

                Dim strCodDash As String
                strCodDash = Request.QueryString("CodDash")




                Dim dtIngreso001 As New DataTable
                Dim Column0 As New DataColumn("ITEM") : Column0.DataType = GetType(String)
                Dim Column1 As New DataColumn("NRO_OPER") : Column1.DataType = GetType(String)
                Dim Column2 As New DataColumn("NRO_WARR") : Column2.DataType = GetType(Decimal)
                Dim Column3 As New DataColumn("NU_VALO_INI") : Column3.DataType = GetType(Decimal)
                Dim Column4 As New DataColumn("NU_VALO_ACT") : Column4.DataType = GetType(Decimal)
                Dim Column5 As New DataColumn("CO_FINA") : Column5.DataType = GetType(String)
                Dim Column6 As New DataColumn("CO_CLIE") : Column6.DataType = GetType(String)
                Dim Column7 As New DataColumn("FE_EMIS") : Column7.DataType = GetType(String)
                Dim Column8 As New DataColumn("FE_VEN_MAX") : Column8.DataType = GetType(String)
                Dim Column9 As New DataColumn("FLG_WARR_XVEN") : Column9.DataType = GetType(String)

                dtIngreso001.Columns.Add(Column0)
                dtIngreso001.Columns.Add(Column1)
                dtIngreso001.Columns.Add(Column2)
                dtIngreso001.Columns.Add(Column3)
                dtIngreso001.Columns.Add(Column4)
                dtIngreso001.Columns.Add(Column5)
                dtIngreso001.Columns.Add(Column6)
                dtIngreso001.Columns.Add(Column7)
                dtIngreso001.Columns.Add(Column8)
                dtIngreso001.Columns.Add(Column9)
                Session.Add("dtprueba", dtIngreso001)




                Dim dr As DataRow
                dr = dtIngreso001.NewRow
                dr(0) = "1"
                dr(1) = "138664"
                dr(2) = "88540"
                dr(3) = "846600.00"
                dr(4) = "14040.20"
                dr(5) = "BBVA BCO. CONTINENTAL - SAN ISIDRO"
                dr(6) = "SEAFROST S.A.C."
                dr(7) = "21/08/2018"
                dr(8) = "15/03/2019"
                dr(9) = "S"
                dtIngreso001.Rows.Add(dr)


                dr = dtIngreso001.NewRow
                dr(0) = "1"
                dr(1) = "138664"
                dr(2) = "88540"
                dr(3) = "846600.00"
                dr(4) = "14040.20"
                dr(5) = "BBVA BCO. CONTINENTAL - SAN ISIDRO"
                dr(6) = "SEAFROST S.A.C."
                dr(7) = "21/08/2018"
                dr(8) = "15/03/2019"
                dr(9) = "S"
                dtIngreso001.Rows.Add(dr)


                dr = dtIngreso001.NewRow
                dr(0) = "1"
                dr(1) = "138664"
                dr(2) = "88540"
                dr(3) = "846600.00"
                dr(4) = "14040.20"
                dr(5) = "BBVA BCO. CONTINENTAL - SAN ISIDRO"
                dr(6) = "SEAFROST S.A.C."
                dr(7) = "21/08/2018"
                dr(8) = "15/03/2019"
                dr(9) = "S"
                dtIngreso001.Rows.Add(dr)

                Me.dgdResultado.DataSource = dtIngreso001
                Me.dgdResultado.DataBind()






            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    'Private Sub ValidarBarr()
    '    '=== Validar Cabecera ===
    '    If Me.ltrCodDoc.Text <> "" Then
    '        'Me.ImgBarr.ImageUrl = "Images/Barr2.png"
    '        Me.btnSol1.BackColor = System.Drawing.Color.FromName("#94B86E")
    '        Me.btnSol1.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
    '        Me.ImgCheck1.Visible = True
    '        Me.lblDeSol1.CssClass = "Titulo3"
    '    Else
    '        'Me.ImgBarr.ImageUrl = "Images/Barr1.png"
    '        Me.btnSol1.BackColor = System.Drawing.Color.FromName("#4D7496")
    '        Me.btnSol1.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
    '        Me.ImgCheck1.Visible = False
    '        Me.lblDeSol1.CssClass = "Titulo1"
    '    End If
    '    '=== Validar Detalle ===
    '    If dgdResultado.Items.Count <> "0" Then
    '        Me.btnSol2.BackColor = System.Drawing.Color.FromName("#94B86E")
    '        Me.btnSol2.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
    '        Me.ImgCheck2.Visible = True
    '        Me.lblDeSol2.CssClass = "Titulo3"
    '    Else
    '        Me.btnSol2.BackColor = System.Drawing.Color.FromName("#ECECEC")
    '        Me.btnSol2.ForeColor = System.Drawing.Color.FromName("#000000")
    '        Me.ImgCheck2.Visible = False
    '        Me.lblDeSol2.CssClass = "Titulo1"
    '    End If
    'End Sub

    'Private Sub loadFinanciador()
    '    Dim dtEntidad As New DataTable
    '    Try
    '        Me.cboFinanciador.Items.Clear()
    '        dtEntidad = objEntidad.gGetEntidades("02", "W")
    '        Me.cboFinanciador.Items.Add(New ListItem("--- SELECCIONE ---", 0))
    '        For Each dr As DataRow In dtEntidad.Rows
    '            Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
    '        Next
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.ToString
    '    Finally
    '        dtEntidad.Dispose()
    '        dtEntidad = Nothing
    '    End Try
    'End Sub

    'Private Sub loadAsociados()
    '    Dim dtAsociados As DataTable = New DataTable
    '    Try
    '        Me.cboAsociado.Items.Clear()
    '        dtAsociados = objEntidad.gGetAsociados(Session.Item("IdSico"))
    '        Me.cboAsociado.Items.Add(New ListItem("--- SELECCIONE ---", "0"))
    '        For Each dr As DataRow In dtAsociados.Rows
    '            Me.cboAsociado.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
    '        Next
    '    Catch ex As Exception
    '        'Me.lblMensaje.Text = "ERROR: " & ex.Message
    '    Finally
    '        dtAsociados.Dispose()
    '        dtAsociados = Nothing
    '    End Try
    'End Sub

    'Private Sub loadTipoWarrant()
    '    Dim dtEntidad As New DataTable
    '    Try
    '        Me.cboTipoWarrant.Items.Clear()
    '        dtEntidad = objDocumento.gGetBusquedaDocumentoTipoWarrant(strCoEmpresa)
    '        Me.cboTipoWarrant.Items.Add(New ListItem("--- SELECCIONE ---", 0))
    '        For Each dr As DataRow In dtEntidad.Rows
    '            Me.cboTipoWarrant.Items.Add(New ListItem(dr("DE_MODA_MOVI").ToString(), dr("CO_MODA_MOVI")))
    '        Next
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.ToString
    '    Finally
    '        dtEntidad.Dispose()
    '        dtEntidad = Nothing
    '    End Try
    'End Sub

    'Private Sub loadAlmacen()
    '    Dim dtAlma As DataTable
    '    Try
    '        Me.cboAlmacen.Items.Clear()
    '        dtAlma = objDocumento.gGetAlmacenCampoPropioXCliente(Session.Item("IdSico"), Me.cboTipoAlmacen.SelectedValue)
    '        Me.cboAlmacen.Items.Add(New ListItem("--- SELECCIONE ---", 0))
    '        For Each dr As DataRow In dtAlma.Rows
    '            Me.cboAlmacen.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
    '        Next
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR: " & ex.Message
    '    Finally
    '        dtAlma.Dispose()
    '        dtAlma = Nothing
    '    End Try
    'End Sub

    'Public Sub imgEnviar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        Dim imgEnviar As ImageButton = CType(sender, ImageButton)
    '        Dim dgi As DataGridItem
    '        Dim dtEnviar As DataTable
    '        Dim j As Integer = 0
    '        dgi = CType(imgEnviar.Parent.Parent, DataGridItem)
    '        dtEnviar = objDocumento.SetUpdateDocumentosSolicitadorWarrant(dgi.Cells(10).Text(), Session.Item("IdUsuario"), "ENV", "")
    '        If dtEnviar.Rows.Count <> 0 Then
    '            lblError.Text = dtEnviar.Rows(0).Item("MENSAJE")
    '        End If
    '        BindDatagrid()
    '        LisTarDetalle()
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub
    'Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        Dim imgEditar As ImageButton = CType(sender, ImageButton)
    '        Dim dgi As DataGridItem
    '        Dim j As Integer = 0
    '        dgi = CType(imgEditar.Parent.Parent, DataGridItem)
    '        Session.Add("NroDoc", Me.ltrNroDocu.Text)
    '        Session.Add("Cod_Doc", Me.ltrCodDoc.Text)
    '        Session.Add("TipDoc", Me.ltrTipDoc.Text)
    '        Session.Add("NroItem", dgi.Cells(1).Text())
    '        Session.Add("TipoWarrant", Me.cboTipoWarrant.SelectedValue)

    '        Response.Redirect("WarSolicitudWarrantRegistroDeta.aspx", False)
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub
    'Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        Dim imgEliminar As ImageButton = CType(sender, ImageButton)
    '        Dim dgi As DataGridItem
    '        Dim dtDesap As DataTable
    '        Dim j As Integer = 0
    '        dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
    '        dtDesap = objDocumento.SetDeleteDocumentosSolicitadorWarrantDeta(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, dgi.Cells(1).Text(), Session.Item("IdUsuario"))
    '        If dtDesap.Rows.Count <> 0 Then
    '            lblError.Text = dtDesap.Rows(0).Item("MENSAJE")
    '        End If
    '        BindDatagrid()
    '        LisTarDetalle()
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub

    'Private Sub loadMoneda()
    '    Dim dtEntidad As New DataTable
    '    Try
    '        Me.cboMoneda.Items.Clear()
    '        dtEntidad = objDocumento.gGetBusquedaDocumentoTraeMoneda(strCoEmpresa)
    '        Me.cboMoneda.Items.Add(New ListItem("--- SELECCIONE ---", 0))
    '        For Each dr As DataRow In dtEntidad.Rows
    '            Me.cboMoneda.Items.Add(New ListItem(dr("DE_MONE").ToString(), dr("CO_MONE")))
    '        Next
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.ToString
    '    Finally
    '        dtEntidad.Dispose()
    '        dtEntidad = Nothing
    '    End Try
    'End Sub
    'Private Property dt() As DataTable
    '    Get
    '        Return (ViewState("dt"))
    '    End Get
    '    Set(ByVal Value As DataTable)
    '        ViewState("dt") = Value
    '    End Set
    'End Property

    'Protected Overrides Sub Finalize()
    '    If Not (objFunciones Is Nothing) Then
    '        objFunciones = Nothing
    '    End If
    '    MyBase.Finalize()
    'End Sub

    'Private Sub cboTipoWarrant_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoWarrant.SelectedIndexChanged
    '    validarTipoWarrant()
    '    ValidarBarr()
    'End Sub

    'Private Sub validarTipoWarrant()

    '    If cboTipoWarrant.SelectedValue = "016" Then
    '        txtAnioDua.Enabled = True
    '        txtCodAduana.Enabled = True
    '        txtNroDua.Enabled = True
    '        'cboRegimen.Enabled = True
    '    Else
    '        txtAnioDua.Text = ""
    '        txtCodAduana.Text = ""
    '        txtNroDua.Text = ""
    '        'cboRegimen.SelectedValue = 0
    '        txtAnioDua.Enabled = False
    '        txtCodAduana.Enabled = False
    '        txtNroDua.Enabled = False
    '        'cboRegimen.Enabled = False
    '    End If

    'End Sub



    'Private Sub BindDatagrid()

    '    Try
    '        dt = Nothing
    '        dt = New DataTable

    '        dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrant(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, "0", Session.Item("IdSico"), Session.Item("CodFina"))
    '        If dt.Rows.Count <> 0 Then

    '            cboTipoWarrant.SelectedValue = dt.Rows(0).Item("COM_ANUL").ToString()

    '            If dt.Rows(0).Item("FLG_DBLENDO").ToString() = True Then
    '                cboEndoso.SelectedValue = "1"
    '            Else
    '                cboEndoso.SelectedValue = "0"
    '            End If

    '            lblTipoSol.Text = "Solicitud:" & Me.ltrNroDocu.Text
    '            txtAnioDua.Text = dt.Rows(0).Item("BAN_ENDO").ToString()
    '            'cboRegimen.SelectedValue = dt.Rows(0).Item("LUG_ENDO").ToString()
    '            cboTipoDocu.SelectedValue = dt.Rows(0).Item("OPE_ENDO").ToString()
    '            cboAsociado.SelectedValue = dt.Rows(0).Item("TAS_DOCU").ToString()
    '            cboTipoAlmacen.SelectedValue = dt.Rows(0).Item("FCH_VCTSICO").Trim
    '            loadAlmacen()
    '            cboAlmacen.SelectedValue = dt.Rows(0).Item("OFI_ENDO").ToString()
    '            txtCodAduana.Text = dt.Rows(0).Item("NRO_CTAENDO").ToString()
    '            cboMoneda.SelectedValue = dt.Rows(0).Item("NRO_GARA").ToString()
    '            cboFinanciador.SelectedValue = dt.Rows(0).Item("ENT_FINA").ToString()
    '            txtNroDua.Text = dt.Rows(0).Item("DC_ENDO").ToString()
    '            ValidarBarr()
    '        End If

    '    Catch ex As Exception
    '        Me.lblError.Text = ex.Message
    '    End Try
    'End Sub

    'Private Sub LisTarDetalle()
    '    Try

    '        dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrantDetalle(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, "0")
    '        If dt.Rows.Count <> 0 Then
    '            'Me.dgdResultado.Visible = True
    '            Me.dgdResultado.DataSource = dt
    '            Me.dgdResultado.DataBind()
    '        Else
    '            'Me.dgdResultado.Visible = False
    '            Me.dgdResultado.DataSource = Nothing
    '            Me.dgdResultado.DataBind()
    '        End If
    '        ValidarBarr()
    '    Catch ex As Exception
    '        Me.lblError.Text = ""
    '    End Try

    'End Sub
    'Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
    '    Response.Redirect("WarSolicitudWarrantDos.aspx", False)
    'End Sub
    'Protected Sub btnRegistro_Click(sender As Object, e As EventArgs) Handles btnRegistro.Click
    '    Try
    '        ValidarBarr()

    '        If cboTipoWarrant.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione Tipo Warrant"
    '            Exit Sub
    '            'ElseIf cboEndoso.SelectedValue = "0" Then
    '            '    lblError.Text = "Seleccione Tipo Endoso"
    '            '    Exit Sub
    '        ElseIf cboTipoAlmacen.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione Tipo Almac�n"
    '            Exit Sub
    '        ElseIf cboTipoDocu.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione Tipo Documento"
    '            Exit Sub
    '            'ElseIf cboAsociado.SelectedValue = "0" Then
    '            '    lblError.Text = "Seleccione Segundo Endosante "
    '            '    Exit Sub
    '        ElseIf cboAlmacen.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione un Almac�n"
    '            Exit Sub
    '        ElseIf cboMoneda.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione Moneda"
    '            Exit Sub
    '        ElseIf cboFinanciador.SelectedValue = "0" Then
    '            lblError.Text = "Seleccione un Financiador"
    '            Exit Sub
    '        End If

    '        '==Validar Aduanero===
    '        If cboTipoWarrant.SelectedValue = "016" Then
    '            If txtAnioDua.Text = "" Then
    '                lblError.Text = "Ingrese A�o DUA  "
    '                Exit Sub
    '            ElseIf txtCodAduana.Text = "" Then
    '                lblError.Text = "Ingrese Cod Aduana"
    '                Exit Sub
    '            ElseIf txtNroDua.Text = "" Then
    '                lblError.Text = "Ingrese Nro DUA"
    '                Exit Sub
    '            End If
    '        End If


    '        objAccesoWeb = New clsCNAccesosWeb
    '        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
    '                                    Session.Item("NombreEntidad"), "M", "38", "GRABA SOLICITUD WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

    '        Dim strResultado As String
    '        Dim dtGrabar As DataTable

    '        dtGrabar = objDocumento.SetGrabarSolicitudWarrantCabecera(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, cboTipoWarrant.SelectedValue, cboEndoso.SelectedValue, cboTipoAlmacen.SelectedValue, txtAnioDua.Text, "0",
    '                   cboTipoDocu.SelectedValue, cboAsociado.SelectedValue, cboAlmacen.SelectedValue, txtCodAduana.Text, cboMoneda.SelectedValue, Session.Item("IdSico"), cboFinanciador.SelectedValue, txtNroDua.Text, Session.Item("IdUsuario"))

    '        If dtGrabar.Rows.Count <> 0 Then
    '            'Select CaseCaseCase@SECUENCIA NRO_DOCU , @Resultado RESULTADO , @Mensaje MENSAJE
    '            If dtGrabar.Rows(0).Item("RESULTADO").ToString = "I" Then
    '                Session.Add("Cod_Doc", dtGrabar.Rows(0).Item("COD_DOC").ToString)
    '                Session.Add("NroDoc", dtGrabar.Rows(0).Item("NRO_DOCU").ToString)
    '                Session.Add("CodFina", dtGrabar.Rows(0).Item("COD_ENDO").ToString)
    '                Me.ltrCodDoc.Text = dtGrabar.Rows(0).Item("COD_DOC").ToString
    '                Me.ltrNroDocu.Text = dtGrabar.Rows(0).Item("NRO_DOCU").ToString
    '                Session.Add("NroItem", "0")
    '                lblTipoSol.Text = "Solicitud:" & Me.ltrNroDocu.Text
    '                Me.btnAgregar.Visible = True
    '                lblError.Text = dtGrabar.Rows(0).Item("MENSAJE").ToString
    '            ElseIf dtGrabar.Rows(0).Item("RESULTADO").ToString = "X" Then
    '            End If
    '        End If

    '        BindDatagrid()
    '        LisTarDetalle()
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub
    'Protected Sub cboEndoso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEndoso.SelectedIndexChanged
    '    validarEndoso()
    '    ValidarBarr()
    'End Sub

    'Private Sub validarEndoso()

    '    If cboEndoso.SelectedValue = "1" Then
    '        cboAsociado.Enabled = True
    '    Else
    '        cboAsociado.Enabled = False
    '    End If
    'End Sub

    'Protected Sub cboTipoAlmacen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoAlmacen.SelectedIndexChanged
    '    loadAlmacen()
    '    ValidarBarr()
    'End Sub

    'Protected Sub btnSol2_Click(sender As Object, e As EventArgs) Handles btnSol2.Click
    '    Session.Add("NroDoc", Me.ltrNroDocu.Text)
    '    Session.Add("Cod_Doc", Me.ltrCodDoc.Text)
    '    Session.Add("TipDoc", Me.ltrTipDoc.Text)
    '    Session.Add("TipoWarrant", Me.cboTipoWarrant.SelectedValue)
    '    Session.Add("NroItem", "0")
    '    Response.Redirect("WarSolicitudWarrantRegistroDeta.aspx", False)
    'End Sub

    'Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
    '    Session.Add("NroDoc", Me.ltrNroDocu.Text)
    '    Session.Add("Cod_Doc", Me.ltrCodDoc.Text)
    '    Session.Add("TipDoc", Me.ltrTipDoc.Text)
    '    Session.Add("TipoWarrant", Me.cboTipoWarrant.SelectedValue)
    '    Session.Add("NroItem", "0")
    '    Response.Redirect("WarSolicitudWarrantRegistroDeta.aspx", False)
    'End Sub

    'Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        'Aqui van tus acciones por cada fila de datos....
    '        CType(e.Item.FindControl("imgEliminar"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de eliminar el item seleccionado?')== false) return false;")

    '    End If
    'End Sub

    'Protected Sub btnSol1_Click(sender As Object, e As EventArgs) Handles btnSol1.Click
    '    BindDatagrid()
    '    LisTarDetalle()
    'End Sub


End Class
