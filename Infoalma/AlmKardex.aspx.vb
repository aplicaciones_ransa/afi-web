Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class AlmKardex
    Inherits System.Web.UI.Page
    Private strdor_pend As String
    Dim i As Integer = 0
    Dim dgExport As DataGrid = New DataGrid
    Private objAccesoWeb As clsCNAccesosWeb
    Private objparametro As clsCNParametro
    Private objKardex As clsCNKardex
    ' Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    Private objWarrant As clsCNWarrant
    Private objRetiro As clsCNRetiro
    Private objWMS As clsCNWMS
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents chkDOR_PEND As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEstadoDCR As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "KARDEX_DETALLADO") Then
            Me.lblOpcion.Text = "Kardex Detallado"
            If (Page.IsPostBack = False) Then
                Session.Add("dtKardex", Nothing)
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "KARDEX_DETALLADO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(365))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                GetCargarReferencias()
                GetCargarTipoMercaderia()
                LlenarEstadoDCR()
                LlenarAlmacenes()
                If Session("Co_AlmaW") = "" Then
                    Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                Else
                    Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                End If
                If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaW"))) > 0 Then
                    Response.Redirect("AlmKardex_W.aspx", False)
                    Exit Sub
                End If
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Dim dvKardex As DataView = New DataView
        Try
            objKardex = New clsCNKardex
            With objKardex
                dvKardex = .gCNListarKardex(Session("CoEmpresa"), Session("IdSico"), Me.cboModalidad.SelectedValue,
                                            Me.cboEstadoDCR.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value,
                                            Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.chkDOR_PEND.Checked, Me.cboAlmacen.SelectedValue)

                Me.dgResultado.DataSource = dvKardex
                Me.dgResultado.DataBind()
                'Session.Item("dtKardex") = dvKardex
                Session.Item("dtMovimiento") = dvKardex
            End With
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "KARDEX_DETALLADO", "CONSULTAR KARDEX", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim strflag As String
        Dim k As Integer
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                strflag = e.Item.Cells(6).Text
                If (i = 0 And Me.dgResultado.CurrentPageIndex = 0) = True Then
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)

                    e.Item.Cells(0).ColumnSpan = 5
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(0).Font.Bold = True

                    e.Item.Cells(1).ColumnSpan = 5
                    e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(1).Font.Bold = True

                    e.Item.Cells(2).ColumnSpan = 5
                    e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(2).Font.Bold = True

                    e.Item.BackColor = System.Drawing.Color.Lavender
                    i = 1
                Else
                    If strflag = "0" Then
                        e.Item.Cells.RemoveAt(15)
                        e.Item.Cells.RemoveAt(14)
                        e.Item.Cells.RemoveAt(13)
                        e.Item.Cells.RemoveAt(12)
                        e.Item.Cells.RemoveAt(11)
                        e.Item.Cells.RemoveAt(10)
                        e.Item.Cells.RemoveAt(9)
                        e.Item.Cells.RemoveAt(8)
                        e.Item.Cells.RemoveAt(7)
                        e.Item.Cells.RemoveAt(6)
                        e.Item.Cells.RemoveAt(5)
                        e.Item.Cells.RemoveAt(4)
                        e.Item.Cells.RemoveAt(3)

                        e.Item.Cells(0).ColumnSpan = 5
                        e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(0).Font.Bold = True

                        e.Item.Cells(1).ColumnSpan = 5
                        e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(1).Font.Bold = True

                        e.Item.Cells(2).ColumnSpan = 5
                        e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                        e.Item.Cells(2).Font.Bold = True

                        e.Item.BackColor = System.Drawing.Color.Lavender
                    Else
                        If e.Item.Cells(15).Text = "N" Then
                            e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")
                            'For k = 0 To 15
                            'e.Item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                            'Next
                            'Else
                            'For k = 0 To 15
                            'e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")
                            'Next
                        End If
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub GetCargarReferencias()
        Try
            objparametro = New clsCNParametro
            With objparametro
                .gCNListarReferencia(cboReferencia, "4")
            End With
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub GetCargarTipoMercaderia()
        Me.cboModalidad.Items.Clear()
        Try
            objWarrant = New clsCNWarrant
            Dim dt As DataTable
            With objWarrant
                dt = .gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "N")
            End With
            Me.cboModalidad.Items.Add(New ListItem("------ Todos -------", "0"))
            If dt Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dt.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI").trim, dr("CO_MODA_MOVI").trim))
            Next
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            'Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarEstadoDCR()
        Me.cboEstadoDCR.Items.Add(New ListItem("Todos", "0"))
        Me.cboEstadoDCR.Items.Add(New ListItem("Con Saldo", "1"))
        Me.cboEstadoDCR.Items.Add(New ListItem("Sin Saldo", "2"))
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"


        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Me.dgExport.DataSource = Session.Item("dsKardex1")
        Me.dgExport.DataBind()
        Me.dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Public Sub Change_Page(ByVal Src As System.Object, ByVal Args As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgResultado.PageIndexChanged
        dgResultado.CurrentPageIndex = Args.NewPageIndex
        'Me.dgResultado.DataSource = Session.Item("dtKardex")
        Me.dgResultado.DataSource = Session.Item("dtMovimiento")
        Me.dgResultado.DataBind()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objparametro Is Nothing) Then
            objparametro = Nothing
        End If
        If Not (objKardex Is Nothing) Then
            objKardex = Nothing
        End If
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) > 0 Then
            'Response.Redirect("AlmKardex.aspx?sCO_ALMA=" & Me.cboAlmacen.SelectedValue, False)
            Response.Redirect("AlmKardex_w.aspx", False)
        End If
    End Sub
End Class
