Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoFirmadoRetiro
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objDocumento As Documento = New Documento
    Private objRetiro As clsCNRetiro = New clsCNRetiro
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_EMITIDO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "1", "FIRMAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Inicializa()
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim strVar As String
        Dim strPagina As String
        strVar = Request.QueryString("var")
        strPagina = Request.QueryString("Pagina")
        Dim strEntidadFinanciera As String
        Dim strArchivoFirmado As String
        Dim strContrasena As String
        Dim strContrasenaClave As String
        Dim strNuevaContrasena As String
        Dim strNuevaContrasenaClave As String
        Dim contenido As Byte()
        If strVar = "3" Then
            Try
                Dim dtMail As New DataTable
                Dim strEmails As String
                Dim strTipoDocumento As String
                Dim objUsuario As Usuario = New Usuario

                Select Case Session.Item("IdTipoDocumento")
                    Case "22"
                        strTipoDocumento = "Simple"
                    Case "23"
                        strTipoDocumento = "Aduanero"
                End Select

                objDocumento.gUpdRechazarDocumentoRetiro(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
                If objDocumento.strResultado = "X" Then
                    Session.Add("Mensaje", "No se pudo rechazar el documento")
                    Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                    Exit Sub
                End If

                dtMail = objUsuario.gGetEmailAprobadores("00000001", Session.Item("CodAlmacen"), strTipoDocumento)

                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                End If

                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                        "Solicitud de rechazo para Retiro " & strTipoDocumento & " - " & Session.Item("NombreEntidad"),
                        "Estimados Se�ores:<br><br>" &
                        "El usuario " & Session.Item("UsuarioLogin") & " solicit� el rechazo del Retiro " & strTipoDocumento & "  N�mero: <STRONG><FONT color='#330099'> " & Session.Item("NroLib") &
                        "</FONT></STRONG><br><br>Cliente: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                        "<br><br> Atte.<br><br>S�rvanse ingresar a la web AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")

                Session.Add("Mensaje", "Se rechaz� correctamente")
                objAccesoWeb = New clsCNAccesosWeb
                objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                            Session.Item("NombreEntidad"), "C", "RETIROS_LINEA", "RECHAZAR RETIRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
                Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                Exit Sub
            Catch ex As Exception
                Session.Add("Mensaje", "ERROR " + ex.Message)
                Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                Exit Sub
            End Try
        End If
        '--------------------------------------------
        ' Dim SignedData As CAPICOM.SignedData
        '--------------------------------------------
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim strMSG As String
        Try
            If strVar <> "3" Then
                If Session("CertDigital") = False Then
                    strContrasenaClave = Request.Form("txtContrasena")
                    strNuevaContrasenaClave = Request.Form("txtNuevaContrasena")
                    strArchivoFirmado = Request.Form("txtArchivo_pdf")
                    contenido = Convert.FromBase64String(strArchivoFirmado)
                    strContrasena = Funciones.EncryptString(strContrasenaClave.Trim, "���_[]0")
                    strNuevaContrasena = Funciones.EncryptString(strNuevaContrasenaClave.Trim, "���_[]0")
                    strMSG = objDocumento.gGetValidaContrasena(Session.Item("IdUsuario"), strContrasena, strNuevaContrasena, "", objTrans)
                    Select Case strMSG
                        Case "I"
                            If strNuevaContrasena <> "" Then
                                Session.Add("strContrasenia", strNuevaContrasenaClave)
                            Else
                                Session.Add("strContrasenia", strContrasenaClave)
                            End If
                            Session.Add("strfecha", Now)
                            Exit Select
                        Case "C"
                            Session.Add("Mensaje", "La contrase�a anterior no es correcta")
                            objTrans.Rollback()
                            objTrans.Dispose()
                            Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                            Exit Sub
                        Case "X"
                            Session.Add("Mensaje", "La contrase�a ingresada no es correcta")
                            Session.Remove("strcontrasenia")
                            objTrans.Rollback()
                            objTrans.Dispose()
                            Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                            Exit Sub
                    End Select
                    '----------------------------------------------------------------------------------------------------------
                End If
            End If
            Dim dtRetiro As DataTable
            Dim strNombreReporte As String
            If Session.Item("IdTipoDocumento") = "23" Then
                dtRetiro = objRetiro.gCNCrearRetiroAprobado(Session.Item("CoEmpresa"), Session.Item("Cod_Unidad"), "DOR", Session.Item("NroLib"))
                strNombreReporte = strPath & Session.Item("Cod_Unidad") & Session.Item("NroLib") & ".PDF"
                objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", Session.Item("NroLib"), Session.Item("IdSico"), CType(dtRetiro, DataTable), strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), True, Session.Item("IdSico"), "", "S")
            End If
            'strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strVar, Session.Item("IdSico"), contenido, objTrans)
            strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), Session.Item("UsuarioLogin"), Session.Item("NroDoc"), Session.Item("NroLib"),
                                                   Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session("CertDigital"), Session("Cod_Doc"), Session("Cod_TipOtro"),
                                                   Session.Item("IdSico"), Session.Item("IdTipoEntidad"), Session.Item("Cod_Unidad"), contenido, strVar, objTrans)
            If strMSG = "X" Then
                Session.Add("Mensaje", "Error al firmar documento")
                'Session.Add("Mensaje", objFunciones.strMensaje)
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
                Exit Sub
            End If
            Session.Add("Mensaje", strMSG)
            objTrans.Commit()
            objTrans.Dispose()
            Response.Redirect("DocumentoRetiroPDF.aspx?var=" & strVar, False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Response.Write(ex.Message)
        Finally
            objConexion = Nothing
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
