<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SendCertificado.aspx.vb" Inherits="SendCertificado" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>SendCertificado</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="VBScript">
			Const CAPICOM_CURRENT_USER_STORE = 2
			Const CAPICOM_MY_STORE = "My"
			Const CAPICOM_STORE_OPEN_READ_ONLY = 0
			Const CAPICOM_STORE_OPEN_EXISTING_ONLY = 128
			Const CAPICOM_CERTIFICATE_FIND_KEY_USAGE = 12
			Const CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE = 128
			Const CAPICOM_CERT_INFO_SUBJECT_EMAIL_NAME = 2

			Sub SubmitForm
				On Error Resume Next

				Dim Certs
				Dim Store
				Dim SignerCert
				Dim strSN
				Dim strFecExp
				Dim	strCAKey 
				Dim intCar
				Dim strCod
				Dim intCom

			'	Set Certs = CreateObject("CAPICOM.Certificates")
				Set Store = CreateObject("CAPICOM.Store")
				Set SignerCert = CreateObject("CAPICOM.Certificate")

				Store.Open CAPICOM_CURRENT_USER_STORE, CAPICOM_MY_STORE, CAPICOM_STORE_OPEN_READ_ONLY Or CAPICOM_STORE_OPEN_EXISTING_ONLY
				Set Certs = Store.Certificates
			'	For Each Cert In Store.Certificates
			'		If Len (Cert.GetInfo(CAPICOM_CERT_INFO_SUBJECT_EMAIL_NAME)) > 0 Then
			'			Certs.Add Cert
			'		End If
			'	Next

				If Certs.Count = 0 Then
					Msgbox "No se encontr� ningun certificado V�lido." , vbInformation ," Certificado "
				Else
					'If Certs.Count = 1 Then 
					'	Set SignerCert = Certs(1)
					'Else
						Set SelectedCerts = Certs.Select()
						'make sure they didn't hit cancel
						if err.number = 0 then 
							Set SignerCert = SelectedCerts(1)
						Else
							msgbox "Usted debe de Seleccionar un certificado para continuar.", vbInformation , " Certificado "
							Exit Sub
						end if
					'End if

					For Each Extension In SignerCert.Extensions
						If Extension.OID.Value = "2.5.29.35" Then 
							'msgbox Extension.OID.FriendlyName & "(" & Extension.OID.Value & ")" 
							strCAKey = Extension.EncodedData.Format
							intCar = instr(strCAKey,"=")
							strCAKey = mid(strCAKey,intCar+1,len(strCAKey)-intCar+1)
							'msgbox strCAKey
							intCom = instr(strCAKey,",")
							'msgbox intCom 
							strCAKey = mid(strCAKey,1,intCom-1)
							'msgbox strCAKey
						End If
					Next

					strSN = SignerCert.SerialNumber
					'msgbox "Serie Cert ... " & LCase(strSN)
					strFecExp = mid(SignerCert.ValidToDate,1,10)
					'msgbox "Expiracion Cert ... " & strFecExp
					strCod = frmValida.txtCodUsuario.value 
					'msgbox "Codigo Usuario ... " & strCod

					If checkCertStatus(SignerCert) Then
					'	msgbox "Bien"
						Window.location = "ValidaCertificado.aspx?SN=" & strSN & "&CA=" & strCAKey & "&FE=" & strFecExp & "&COD=" & strCod
					'Else
					'	msgbox "Fallo"
					End if
				End If
				Set Certs = Nothing
				Set Store = Nothing
				Set SignerCert = Nothing
			End Sub
		</script>
		<script language="jscript">
		// CAPICOM constants
		var CAPICOM_INFO_SUBJECT_SIMPLE_NAME = 0;
		var CAPICOM_INFO_ISSUER_SIMPLE_NAME = 1;
		var CAPICOM_INFO_SUBJECT_EMAIL_NAME = 2;
		var CAPICOM_INFO_ISSUER_EMAIL_NAME  = 3;

		var CAPICOM_CHECK_NONE = 0;
		var CAPICOM_CHECK_TRUSTED_ROOT = 1;
		var CAPICOM_CHECK_TIME_VALIDITY = 2;
		var CAPICOM_CHECK_SIGNATURE_VALIDITY = 4;
		var CAPICOM_CHECK_ONLINE_REVOCATION_STATUS = 8;
		var CAPICOM_CHECK_OFFLINE_REVOCATION_STATUS = 16;
		var CAPICOM_CHECK_ONLINE_ALL = 495;

		var CAPICOM_TRUST_IS_NOT_TIME_VALID = 1;
		var CAPICOM_TRUST_IS_NOT_TIME_NESTED = 2;
		var CAPICOM_TRUST_IS_REVOKED = 4;
		var CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID = 8;
		var CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE = 16;
		var CAPICOM_TRUST_IS_UNTRUSTED_ROOT = 32;
		var CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN = 64;
		var CAPICOM_TRUST_IS_CYCLIC = 128;
		var CAPICOM_TRUST_IS_PARTIAL_CHAIN = 65536;
		var CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID = 131072;
		var CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID = 262144;
		var CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE = 524288;
		
		function checkCertStatus(pCert)
		{
			var Certificate = pCert;

			window.status="Comprobando el Estado del Certificado ...";
			// CAPICOM exposes Certificate status checking through IsValid, the CheckFlag parameter
			// allows you to specify the items you want to have checked for you.
			Certificate.IsValid().CheckFlag = (CAPICOM_CHECK_TRUSTED_ROOT | CAPICOM_CHECK_TIME_VALIDITY | CAPICOM_CHECK_SIGNATURE_VALIDITY | CAPICOM_CHECK_ONLINE_REVOCATION_STATUS);
			if (Certificate.IsValid().Result == true)
			{
				// clear the status window
				window.status="";
				//alert("El Certificado de \"" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "\" es Valido.");
				return true;
			}
			else
				{
					var Chain = new ActiveXObject("CAPICOM.Chain");
					Chain.Build(Certificate)

					// clear the status window
					window.status="";
					if (CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID & Chain.Status)
					{
						alert("Se encontro un problema con la firma del Certificado a nombre de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "'");
						return false;
					}
					if ((CAPICOM_TRUST_IS_UNTRUSTED_ROOT & Chain.Status) || (CAPICOM_TRUST_IS_PARTIAL_CHAIN & Chain.Status))
					{
						alert("No se ha podido establecer el Certificado de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "' a una Autoridad Certificadora (CA) de confianza.");
						return false;
					}
					if (CAPICOM_TRUST_IS_REVOKED & Chain.Status)
					{
						alert("El Certificado seleccionado a nombre de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "'\n se encuentra revocado por su Autoridad Certificadora (CA).");
						return false;
					}
					else
						{
							return true;
						}
				}
		}
		</script>
	</HEAD>
	<body onload="SubmitForm" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		bgColor="#f0f0f0">
		<form id="frmValida" method="get" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD class="td" align="center" colSpan="2" height="40">Seleccione su Certificado 
									para registrarlo en el sistema
									<HR style="BORDER-RIGHT: #051e46 1px solid; BORDER-TOP: #003374 1px solid; BORDER-LEFT: #003374 1px solid; BORDER-BOTTOM: #003374 1px solid; BACKGROUND-COLOR: #003374"
										width="100%" SIZE="1">
								</TD>
							</TR>
							<TR>
								<TD class="td" align="center" colSpan="2">
									<DIV style="DISPLAY: none"><INPUT id="txtCodUsuario" type="hidden" name="txtCodUsuario" runat="server"></DIV>
									<asp:Button id="btnAceptar" runat="server" CssClass="btn" Width="80" Text="Ir al Menu"></asp:Button>
									<asp:Button id="btnReintentar" runat="server" CssClass="btn" Width="80" Text="Reintentar"></asp:Button></TD>
							</TR>
							<TR>
								<TD class="Etiqueta" align="center" colSpan="2">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none">&nbsp;</DIV>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
