Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class ConsultasGeneradas
    Inherits System.Web.UI.Page
    Private lstrNumRequ As String
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblNroConsultas As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipoEnvio As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG5") Then
            Me.lblError.Text = ""
            PoliticaCache()
            Try
                If Not IsPostBack Then
                    CargarCombos()
                    LlenaGrid()
                    btnGrabar.Attributes.Add("onclick", "return MsgRegistrar(" & Session.Item("RequerimientoActivo").ToString & ");")
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        EliminarConsulta(sender)
        LlenaGrid()
    End Sub

    Private Sub dgdResultado_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim imgEliminar As ImageButton
            imgEliminar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
            imgEliminar.Attributes.Add("onClick", "return MsgEliminar(" & e.Item.DataItem("NU_CONS") & ");")
        End If
    End Sub

    'Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    '    If ActualizarRequerimiento() = True Then
    '        Dim dgItem As DataGridItem
    '        Dim strnCodArea As String
    '        Dim BooAprobado As Boolean
    '        Dim strAreas As String
    '        Dim strtemArea As String

    '        Session.Add("Seleccionados", "-")


    '        For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
    '            dgItem = Me.dgdResultado.Items(intI)
    '            If EsAprobador(dgItem.Cells(6).Text) = True Then
    '                BooAprobado = True
    '                If strtemArea <> dgItem.Cells(6).Text Then
    '                    strAreas = dgItem.Cells(2).Text + ",--" + strAreas
    '                    strtemArea = dgItem.Cells(6).Text
    '                End If
    '            Else
    '                BooAprobado = False
    '                Exit For
    '            End If
    '        Next

    '        If BooAprobado = True Then
    '            'For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
    '            'dgItem = Me.dgdResultado.Items(intI)

    '            'If dgItem.Cells(5).Text <> strnCodArea Then
    '            'If AprobarRequerimiento(CStr(dgItem.Cells(0).Text)) = True Then
    '            If AprobarRequerimiento(strAreas) = True Then
    '                Session.Add("RequerimientoActivo", "0")
    '                Session.Add("NroConsultas", "0")
    '                Response.Redirect("MensajeRegistro.aspx?CR=" & lstrNumRequ & "&CT=D&ER=REG")
    '            Else
    '                lblError.Text = "El requerimiento nro.: " & Session.Item("RequerimientoActivo") & " no se ha aprobado "
    '                Session.Add("RequerimientoActivo", "0")
    '                Session.Add("NroConsultas", "0")
    '                Response.Redirect("AprobarRequerimiento.aspx", False)
    '            End If
    '            'strnCodArea = dgItem.Cells(5).Text
    '            'End If
    '            'Next
    '            'Session.Add("RequerimientoActivo", "0")
    '            'Session.Add("NroConsultas", "0")
    '            'Response.Redirect("MensajeRegistro.aspx?CR=" & lstrNumRequ & "&CT=D&ER=REG")
    '        Else
    '            lblError.Text = "Solicitud de requerimiento nro.: " & Session.Item("RequerimientoActivo") & "ha sigo registrada y esta lista para ser aprobada"
    '            Session.Add("ProcesoDocumento", "LISTO")
    '            Response.Redirect("MensajeRegistro.aspx?CR=" & CStr(Session.Item("RequerimientoActivo")) & "&CT=D&ER=TEM")
    '            Session.Add("RequerimientoActivo", "0")
    '            Session.Add("NroConsultas", "0")

    '        End If
    '        Session.Add("RequerimientoActivo", "0")
    '        Session.Add("NroConsultas", "0")

    '    End If
    'End Sub
    Private Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If ActualizarRequerimiento() = True Then
            Dim dgItem As DataGridItem
            Dim strnCodArea As String
            Dim BooAprobado As Boolean
            Dim strAreas As String
            Dim strtemArea As String

            Session.Add("Seleccionados", "-")


            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If EsAprobador(dgItem.Cells(6).Text) = True Then
                    BooAprobado = True
                    If strtemArea <> dgItem.Cells(6).Text Then
                        strAreas = dgItem.Cells(2).Text + ",--" + strAreas
                        strtemArea = dgItem.Cells(6).Text
                    End If
                Else
                    BooAprobado = False
                    Exit For
                End If
            Next

            If BooAprobado = True Then
                'For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                'dgItem = Me.dgdResultado.Items(intI)

                'If dgItem.Cells(5).Text <> strnCodArea Then
                'If AprobarRequerimiento(CStr(dgItem.Cells(0).Text)) = True Then
                If AprobarRequerimiento(strAreas) = True Then
                    Session.Add("NroConsultas", "0")
                    Session.Add("RequerimientoActivo", "0")
                    'Session.Add("ActivoDocu", "1")
                    'Session("ActivoDocu") = "1"
                    Response.Redirect("MensajeRegistro.aspx?CR=" & lstrNumRequ & "&CT=D&ER=REG" & "&ActivoDocu=""1")
                    'Session("ActivoDocu") = "1"
                    'Session.Add("ActivoDocu", "0")
                Else
                    lblError.Text = "El requerimiento nro.: " & Session.Item("RequerimientoActivo") & " no se ha aprobado "
                    Session.Add("RequerimientoActivo", "0")
                    Session.Add("NroConsultas", "0")
                    Response.Redirect("AprobarRequerimiento.aspx", False)
                End If
                'strnCodArea = dgItem.Cells(5).Text
                'End If
                'Next
                'Session.Add("RequerimientoActivo", "0")
                'Session.Add("NroConsultas", "0")
                'Response.Redirect("MensajeRegistro.aspx?CR=" & lstrNumRequ & "&CT=D&ER=REG")
            Else
                lblError.Text = "Solicitud de requerimiento nro.: " & Session.Item("RequerimientoActivo") & "ha sigo registrada y esta lista para ser aprobada"
                Session.Add("ProcesoDocumento", "LISTO")
                Response.Redirect("MensajeRegistro.aspx?CR=" & CStr(Session.Item("RequerimientoActivo")) & "&CT=D&ER=TEM")
                Session.Add("RequerimientoActivo", "0")
                Session.Add("NroConsultas", "0")

            End If
            Session.Add("RequerimientoActivo", "0")
            Session.Add("NroConsultas", "0")

        End If
    End Sub

    'Private Function AprobarRequerimiento() As Boolean
    '    Try
    '        objRequ = New LibCapaNegocio.clsCNRequerimiento
    '        If objRequ.gbolAprobarRequerimiento(CStr(Session.Item("CodCliente")), CInt(Session.Item("RequerimientoActivo")), CStr(Session.Item("CodPersAuto")), _
    '            CStr(Session.Item("NomCliente")), CStr(Session.Item("UsuarioLogin"))) = True Then
    '            lblError.Text = lblError.Text & objRequ.strMensajeError
    '            lstrNumRequ = objRequ.strCodRequerimiento()
    '            Return True
    '        Else
    '            Return False
    '            lblError.Text = objRequ.strMensajeError
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "ERROR:" & ex.Message
    '        Return False
    '    End Try
    'End Function
    Private Function AprobarRequerimiento(ByVal strAreas As String) As Boolean
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            'If objRequ.gbolAprobarRequerimiento(CStr(Session.Item("CodCliente")), CInt(Session.Item("RequerimientoActivo")), 'CStr(Session.Item("CodPersAuto")), CStr(Session.Item("NomCliente")), CStr(Session.Item("IdUsuario")), strAreas, cboEstado.'SelectedItem.Text) = True Then 'cboArea.SelectedValue)


            If objRequ.gbolAprobarRequerimiento(CStr(Session.Item("CodCliente")), CInt(Session.Item("RequerimientoActivo")), CStr(Session.Item("CodPersAuto")), CStr(Session.Item("NomCliente")), CStr(Session.Item("UsuarioLogin")), strAreas, cboEstado.SelectedItem.Text) = True Then 'cboArea.SelectedValue)
                lblError.Text = lblError.Text & objRequ.strMensajeError
                lstrNumRequ = objRequ.strCodRequerimiento()
                Return True
            Else
                Return False
                lblError.Text = objRequ.strMensajeError
            End If
        Catch ex As Exception
            lblError.Text = "ERROR:" & ex.Message
            Return False
        End Try
    End Function
    Public Function EsAprobador(ByVal strCodArea As String) As Boolean
        Dim bolRetorno As Boolean = False
        objConsulta = New LibCapaNegocio.clsCNConsulta
        If strCodArea <> "" And strCodArea <> Nothing Then
            'If objConsulta.gstrUsuarioAprobador(CStr(Session.Item("CodCliente")), "", strCodArea, CStr(Session.Item("IdUsuario"))) = "S" Then

            If objConsulta.gstrUsuarioAprobador(CStr(Session.Item("CodCliente")), "", strCodArea, CStr(Session.Item("UsuarioLogin"))) = "S" Then
                bolRetorno = True
            Else
                bolRetorno = False
            End If
        End If
        Return bolRetorno
    End Function

    Private Function ActualizarRequerimiento() As Boolean
        If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
            If ActualizarCabeceraRequerimiento() = True Then
                If ActualizarConsultas() = True Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            lblError.Text = "No se ha generado numero de requerimiento para su registro."
            Return False
        End If
    End Function

    Private Function ActualizarCabeceraRequerimiento() As Boolean
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            If objRequ.gbolActualizarRequerimiento(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), "",
                cboTipoEnvio.SelectedValue, cboEstado.SelectedValue, "S", CInt(Session.Item("NroConsultas"))) = True Then
                lblError.Text = lblError.Text & " Requ: " & CStr(Session.Item("RequerimientoActivo")) & " actualizado!"
                Return True
            Else
                Return False
                lblError.Text = "ERROR EN EL PROCEDIMIENTO"
            End If
        Catch ex As Exception
            lblError.Text = "ERROR:" & ex.Message
            Return False
        End Try
    End Function

    Private Function ActualizarConsultas() As Boolean
        Dim dgItem As DataGridItem
        Dim bolEstado As Boolean = False
        Try
            For Each dgItem In dgdResultado.Items
                objConsulta = New LibCapaNegocio.clsCNConsulta
                If objConsulta.gbolActualizarConsulta(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), dgItem.Cells(0).Text,
                    cboTipoEnvio.SelectedValue) = True Then
                    lblError.Text = lblError.Text & " Cons: " & dgItem.Cells(0).Text & " actualizada!"
                End If
            Next
            Return True
        Catch ex As Exception
            lblError.Text = "ERROR:" & ex.Message
            Return False
        End Try
    End Function

    Private Sub LlenaGrid()
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            objFuncion = New LibCapaNegocio.clsFunciones
            objFuncion.gCargaGrid(dgdResultado, objRequ.gdtConseguirDetalleTemporal(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), "D"))
            lblNroConsultas.Text = Session.Item("NroConsultas").ToString
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboTipoEnvio(cboTipoEnvio)
    End Sub

    Public Function GetTipoEnvio() As DataTable
        objComun = New LibCapaNegocio.clsCNComun
        Return objComun.gdtGetTipoEnvio
    End Function

    'Private Sub EliminarConsulta(ByVal objSender As Object)
    '    Try
    '        Dim imgEliminar As ImageButton = CType(objSender, ImageButton)
    '        Dim dgItem As DataGridItem
    '        Dim j As Integer = 0
    '        Dim intNroConsultas As Integer = CInt(Session.Item("NroConsultas"))
    '        dgItem = CType(imgEliminar.Parent.Parent, DataGridItem)
    '        objConsulta = New LibCapaNegocio.clsCNConsulta
    '        If objConsulta.gbolEliminaConsulta(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), CInt(dgItem.Cells(0).Text), dgItem.Cells(6).Text) Then
    '            Session.Add("NroConsultas", intNroConsultas - 1)
    '            RemoverSeleccionado("SCAJ" & dgItem.Cells(2).Text)
    '            Me.lblError.Text = "La consulta nro: " + dgItem.Cells(0).Text + " fue eliminado exitosamente!"
    '        Else
    '            Me.lblError.Text = "ERROR en el procedimiento de eliminación."
    '        End If
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub
    Private Sub EliminarConsulta(ByVal objSender As Object)
        Try
            Dim imgEliminar As ImageButton = CType(objSender, ImageButton)
            Dim dgItem As DataGridItem
            Dim j As Integer = 0
            Dim intNroConsultas As Integer = CInt(Session.Item("NroConsultas"))
            dgItem = CType(imgEliminar.Parent.Parent, DataGridItem)
            objConsulta = New LibCapaNegocio.clsCNConsulta
            If objConsulta.gbolEliminaConsulta(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), CInt(dgItem.Cells(0).Text), CStr(dgItem.Cells(6).Text)) Then

                Session.Add("NroConsultas", intNroConsultas - 1)
                RemoverSeleccionado("SDOC" & dgItem.Cells(1).Text & dgItem.Cells(9).Text)
                Me.lblError.Text = "La consulta nro: " + dgItem.Cells(0).Text + " fue eliminado exitosamente!"
            Else
                Me.lblError.Text = "ERROR en el procedimiento de eliminación."
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect(Session.Item("PaginaAnterior"))
    End Sub

    Private Sub cboTipoEnvio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoEnvio.SelectedIndexChanged
        DireccionarComboTipoEnvio()
    End Sub

    Private Sub DireccionarComboTipoEnvio()
        Dim dgItem As DataGridItem
        For Each dgItem In dgdResultado.Items
            Dim objCombo As DropDownList
            objCombo = CType(dgItem.FindControl("cboEnvio"), DropDownList)
            objCombo.SelectedIndex = cboTipoEnvio.SelectedIndex
            objCombo.SelectedValue = cboTipoEnvio.SelectedValue
        Next
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Public Function EsAprobador() As Boolean
        Dim strCodArea As String = ""
        Dim bolRetorno As Boolean = False
        objConsulta = New LibCapaNegocio.clsCNConsulta
        strCodArea = Session.Item("CodArea")
        If strCodArea <> "" And strCodArea <> Nothing Then
            If objConsulta.gstrUsuarioAprobador(CStr(Session.Item("CodCliente")), "", strCodArea, CStr(Session.Item("UsuarioLogin"))) = "S" Then
                bolRetorno = True
            Else
                bolRetorno = False
            End If
        End If
        Return bolRetorno
    End Function

    Private Sub RemoverSeleccionado(ByVal pstrItem As String)
        Dim strSeleccionado As String = Session.Item("Seleccionados").ToString
        strSeleccionado = strSeleccionado.Replace(pstrItem, "")
        Session.Add("Seleccionados", strSeleccionado)
    End Sub

    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub dgdResultado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgdResultado.SelectedIndexChanged

    End Sub
End Class
