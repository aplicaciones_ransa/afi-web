<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UsuarioBusqueda.aspx.vb" Inherits="UsuarioBusqueda" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consultar Usuario</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="francisco_uni@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="document.all['btnBuscar'].focus();"
		bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD class="Titulo1" width="100%">Consulta&nbsp;de Usuarios</TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="690" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" width="45">Tipo Ent:</TD>
														<TD>
															<asp:dropdownlist id="cboTipoEntidad" runat="server" Width="80px" AutoPostBack="True" CssClass="Text"></asp:dropdownlist></TD>
														<TD class="Text">Ent:</TD>
														<TD>
															<asp:dropdownlist id="cboEntidad" runat="server" Width="220px" CssClass="Text"></asp:dropdownlist></TD>
														<TD class="Text">Ap. y Nom:</TD>
														<TD>
															<asp:textbox id="txtNombres" style="TEXT-TRANSFORM: uppercase" runat="server" Width="70px" CssClass="Text"></asp:textbox></TD>
														<TD>
															<asp:button id="btnBuscar" runat="server" CssClass="btn" Text="Buscar" Width="60px"></asp:button></TD>
														<TD>
															<asp:Button id="btnExportar" runat="server" CssClass="btn" Width="60px" Text="Exportar"></asp:Button></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="right">
									<asp:HyperLink id="hlkNuevo" runat="server" CssClass="Text" Target="_parent" NavigateUrl="UsuarioNuevo.aspx">Nuevo Usuario</asp:HyperLink></TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">Resultado:</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:datagrid id="dgdResultado" runat="server" Width="100%" CssClass="gv" PageSize="20" AllowPaging="True"
										OnPageIndexChanged="Change_Page" AutoGenerateColumns="False" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="COD_USER" HeaderText="C&#243;d">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Nombres" HeaderText="Nombres y Apellidos"></asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Nombre de Entidad"></asp:BoundColumn>
											<asp:BoundColumn DataField="DIR_EMAI" HeaderText="E-mail"></asp:BoundColumn>
											<asp:BoundColumn DataField="NO_GRUP" HeaderText="Grupo"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Ver.JPG" ToolTip="Ver detalles"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Estado">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEstado" onclick="imgEstado_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Activar.gif"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="EST_USER"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_SICO"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
