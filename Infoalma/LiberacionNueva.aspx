<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LiberacionNueva.aspx.vb" Inherits="LiberacionNueva" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LiberacionNueva</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text">Nro Warrant:</TD>
														<TD>
															<asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="80px"></asp:textbox></TD>
														<TD class="text">Endosatario:</TD>
														<TD>
															<asp:dropdownlist id="cboEndosatario" runat="server" CssClass="Text" Width="320px"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="text">Mercader�a:</TD>
														<TD>
															<asp:textbox id="txtDescripci�n" runat="server" CssClass="Text" Width="120px"></asp:textbox></TD>
														<TD class="text">Tipo Merc:</TD>
														<TD colSpan="3">
															<asp:dropdownlist id="cboTipoMercaderia" runat="server" CssClass="Text" Width="320px"></asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center">
									<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="250">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanged="Change_Page" AllowPaging="True"
										PageSize="15">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro Warr">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TI_TITU" HeaderText="Tipo Doc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FE_TITU" HeaderText="Fecha">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_DOCU_RECE" HeaderText="DCR">
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_CORT_ENFI" HeaderText="Endosatario">
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_MERC_GENE" HeaderText="Dsc Mercader&#237;a">
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="co_mone" HeaderText="Moneda">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="im_saldo" HeaderText="Saldo">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="ST_ENDO_EMBA" HeaderText="Emba">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ST_CUST_DEPS" HeaderText="Custodia?">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="PARCIAL" HeaderText="Parcial?">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Ver.JPG" ToolTip="Ver detalles"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="CO_ENTI_FINA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="TI_DOCU_RECE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UNID"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="virtual"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0">
										<TR>
											<TD class="td" colSpan="8">Leyenda</TD>
										</TR>
										<TR>
											<TD width="15%" bgColor="lightgoldenrodyellow"></TD>
											<TD class="Leyenda" width="35%">Warrant Electr�nico.</TD>
											<TD width="15%"></TD>
											<TD class="Leyenda" width="35%">Warrant Normal.</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
