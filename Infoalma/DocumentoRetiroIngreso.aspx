<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoRetiroIngreso.aspx.vb" Inherits="DocumentoRetiroIngreso" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Documento de retiro de aprobaci�n</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">
		function AbrirDetalle(sNU_ITEM)
		{
			var ventana = 'Popup/PoputDocRetiroIngreso.aspx?sNU_ITEM='+ sNU_ITEM;
			window.open(ventana,"Retiro","left=240,top=200,width=700,height=220,toolbar=0,resizable=1")
		}
		
		function isNumberOrLetter(evt)
		{
		   var charCode = (evt.which) ? evt.which : event.keyCode;
		   if ((charCode > 65 && charCode < 91) || (charCode > 97 && charCode < 123) || (charCode > 47 && charCode < 58) )
		   return true;  
		   return false;
		}
									
		function ActualizaWFormPadre()
		{
			document.getElementById('<%=btnRefrescar.ClientID%>').click();
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
							border="0" cellSpacing="0" cellPadding="0" width="100%">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table3" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%" align="left"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="5"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="760" align="center">
													<TR>
														<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r1_c2.gif"></TD>
														<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD background="Images/table_r2_c1.gif" width="6"></TD>
														<TD align="center">
															<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0">
																<TR>
																	<TD class="Text">Tipo de carga</TD>
																	<TD class="Text">:</TD>
																	<TD><asp:dropdownlist id="cboTipCarga" runat="server" CssClass="text" Width="150px" AutoPostBack="True">
																			<asp:ListItem Value="0">----Seleccione----</asp:ListItem>
																			<asp:ListItem Value="1">EXCEL</asp:ListItem>
																			<asp:ListItem Value="2">FORMULARIO</asp:ListItem>
																		</asp:dropdownlist></TD>
																	<TD class="Text">Almac�n</TD>
																	<TD class="Text">:</TD>
																	<TD><asp:dropdownlist id="cboAlmacen" runat="server" CssClass="text" Width="300px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD colSpan="6"><asp:panel id="Panel1" runat="server" Visible="False">
																			<TABLE id="Table7" border="0" cellSpacing="1" cellPadding="1">
																				<TR>
																					<TD class="text">Adjuntar archivo</TD>
																					<TD class="text">:</TD>
																					<TD><INPUT style="WIDTH: 350px" id="FileLoad" class="btn" type="file" name="FileLoad" runat="server">
																						<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="FileLoad"
																							ErrorMessage="Ingrese solo archivos Excel" Display="Dynamic" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls|.XLS|.xlsx|.XSLX)$"></asp:regularexpressionvalidator></TD>
																					<TD>
																						<asp:HyperLink id="lnkPlantilla" runat="server" CssClass="otros-links" NavigateUrl="Manuales/Ingresos.xls">Plantilla Excel</asp:HyperLink></TD>
																				</TR>
																				<TR>
																					<TD colSpan="4" align="center">
																						<asp:Button id="btnGrabar" runat="server" CssClass="Text" Width="80px" Text="Enviar"></asp:Button></TD>
																				</TR>
																			</TABLE>
																		</asp:panel><asp:panel id="Panel2" runat="server" Visible="False">
																			<TABLE id="Table9" border="0" cellSpacing="1" cellPadding="1">
																				<TR>
																					<TD class="text" vAlign="top" colSpan="3" align="left">Nro.&nbsp;Referencia :
																						<asp:TextBox id="txtNroGuia" runat="server" CssClass="Text" Width="180px"></asp:TextBox>&nbsp;(Orden 
																						de&nbsp;Compra, Gu�a de Remisi�n, Otros)</TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="3" align="center">
																						<asp:Button id="btnRefrescar" runat="server" CssClass="Text" Width="80px" Text="Refrescar"></asp:Button>
																						<asp:Button id="btnAgregar" runat="server" CssClass="Text" Width="80px" Text="Agregar"></asp:Button>
																						<asp:Button id="btnGrabar1" runat="server" CssClass="Text" Width="80px" Text="Enviar"></asp:Button></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="3" align="center">
																						<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="680px" Visible="True" BorderColor="Gainsboro"
																							AutoGenerateColumns="False" PageSize="20">
																							<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
																							<ItemStyle CssClass="gvRow"></ItemStyle>
																							<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
																							<Columns>
																								<asp:BoundColumn DataField="NU_ITEM" HeaderText="&#205;tem">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="CO_PROD" HeaderText="Cod.Producto">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="DE_PROD" HeaderText="Descripci&#243;n">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="NU_CANT" HeaderText="Cantidad">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="NU_LOTE" HeaderText="Nro. Lote">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:TemplateColumn HeaderText="Editar">
																									<HeaderStyle Width="80px"></HeaderStyle>
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																									<ItemTemplate>
																										<A style="FONT-FAMILY: Verdana; FONT-SIZE: 7pt; FONT-WEIGHT: bold" class=error onclick='AbrirDetalle("<%# DataBinder.Eval(Container.DataItem, "NU_ITEM") %>");' href="#">
																											Detalle</A>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn HeaderText="Eliminar">
																									<ItemStyle HorizontalAlign="Center"></ItemStyle>
																									<ItemTemplate>
																										<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" ToolTip="Eliminar registro"
																											ImageUrl="Images/Eliminar.JPG" CausesValidation="False"></asp:ImageButton>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																							</Columns>
																							<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
															</TABLE>
														</TD>
														<TD background="Images/table_r2_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r3_c2.gif"></TD>
														<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" colSpan="2" align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table2" border="0" cellPadding="0" width="630">
													<TR>
														<TD class="td" colSpan="8">Leyenda</TD>
													</TR>
													<TR>
														<TD colSpan="6">
															<UL>
																<LI class="Subtitulo">
																El c�digo de producto debe estar registrado en el WMS
																<LI class="Subtitulo">
																No debe existir caracteres especiales
																<LI class="Subtitulo">
																	El n�mero de ingreso debe estar en mayuscula y no exceder los 8 caracteres
																</LI>
															</UL>
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
