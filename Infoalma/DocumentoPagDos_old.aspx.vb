Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoPagDos_old
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    'Protected WithEvents chkProtesto As System.Web.UI.WebControls.CheckBox
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objEntidad As Entidad = New Entidad
    Private objReportes As Reportes = New Reportes
    'Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Private strEndosoPath = ConfigurationManager.AppSettings.Item("RutaEndoso")
    'Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents cboMoneda As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtOperacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtVencimiento As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents RequiredFieldValidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtValor As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtDC As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroCuenta As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtLugar As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaVenSICO As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTasa As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtBanco As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtOficina As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtFechaEmision As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents chkEmbarque As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblEntidadFinanciera As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroGarantia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            'Session.Remove("Mensaje")
            'If Not IsPostBack Then
            '    Me.lblOpcion.Text = "Datos del Endoso"
            '    Dim strResult As String()
            '    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
            '    Session.Item("Page") = strResult(0)
            '    Session.Item("Opcion") = strResult(1)
            '    'Me.txtNroPagina.Value = Request.QueryString("Pag")
            '    Inicializa()
            'End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
End Class
