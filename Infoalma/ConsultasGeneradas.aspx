<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConsultasGeneradas.aspx.vb" Inherits="ConsultasGeneradas" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Consultas Generadas</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  
		
		function Ocultar()
			{
			Estado.style.display='none';			
			}
		function MsgEliminar(v)
			{
			if (confirm('La consulta nro. '+v+' ser� eliminada. Presione Aceptar para confimar \no Cancel para cancelar la operaci�n.')==false) 
				return false;
			}
		function MsgRegistrar(v)
			{
		if (confirm('La solicitud nro. '+v+' ser� registrada. Presione Aceptar para confimar \no Cancel para cancelar la operaci�n.')==false) 
				return false;
			}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="125">
						<uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top" width="100%">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" border="0" align="center">
							<TR>
								<TD class="Titulo1" height="20">REQUERIMIENTO A SOLICITAR</TD>
							</TR>
							<TR>
								<TD class="td" style="HEIGHT: 39px">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" style="WIDTH: 68px; HEIGHT: 20px">Tipo Env�o</TD>
														<TD class="Text" style="WIDTH: 19px; HEIGHT: 20px" align="center">:</TD>
														<TD class="text" style="WIDTH: 225px; HEIGHT: 20px">
															<asp:dropdownlist id="cboTipoEnvio" runat="server" CssClass="Text" DataTextField="DES" DataValueField="COD"
																AutoPostBack="True" Width="140px">
																<asp:ListItem Value="ANEXADO">ANEXADO</asp:ListItem>
																<asp:ListItem Value="DOCUMENTO ORIGINAL">DOCUMENTO ORIGINAL</asp:ListItem>
																<asp:ListItem Value="DOF">DOF</asp:ListItem>
																<asp:ListItem Value="FAX">FAX</asp:ListItem>
																<asp:ListItem Value="FOTOCOPIA DOCUMENTO">FOTOCOPIA DOCUMENTO</asp:ListItem>
																<asp:ListItem Value="MAIL">MAIL</asp:ListItem>
																<asp:ListItem Value="UNIDAD COMPLETA">UNIDAD COMPLETA</asp:ListItem>
															</asp:dropdownlist>&nbsp;</TD>
														<TD class="Text" style="WIDTH: 116px; HEIGHT: 20px" align="right">Estado</TD>
														<TD class="Text" style="WIDTH: 47px; HEIGHT: 20px" align="center">:</TD>
														<TD class="Text" style="HEIGHT: 20px">
															<asp:dropdownlist id="cboEstado" runat="server" CssClass="Text" Width="140px">
																<asp:ListItem Value="S">URGENTE</asp:ListItem>
																<asp:ListItem Value="N">NORMAL</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Consultas :
									<asp:label id="lblNroConsultas" runat="server" CssClass="TEXT" Width="16px">0</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AutoGenerateColumns="False" OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="20"
										AllowSorting="True">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NU_CONS" HeaderText="Nro.">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ID_UNID" HeaderText="Nro. Caja">
												<HeaderStyle Width="7%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea"></asp:BoundColumn>
											<asp:BoundColumn DataField="DE_CRIT" HeaderText="Tipo Doc.">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="OBSE" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="38%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Detalle Adicional">
												<HeaderStyle Width="30%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_AREA" HeaderText="Cod. Area"></asp:BoundColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Env&#237;o">
												<ItemTemplate>
													<asp:DropDownList id=cboEnvio runat="server" DataTextField="DES" DataValueField="COD" Width="95px" CssClass="Text" DataSource="<%# GetTipoEnvio %>">
														<asp:ListItem Value="ANEXADO">ANEXADO</asp:ListItem>
														<asp:ListItem Value="DOCUMENTO ORIGINAL">DOCUMENTO ORIGINAL</asp:ListItem>
														<asp:ListItem Value="FAX">FAX</asp:ListItem>
														<asp:ListItem Value="DOF">DOF</asp:ListItem>
														<asp:ListItem Value="FOTOCOPIA DOCUMENTO">FOTOCOPIA DOCUMENTO</asp:ListItem>
														<asp:ListItem Value="MAIL">MAIL</asp:ListItem>
														<asp:ListItem Value="UNIDAD COMPLETA" Selected="True">UNIDAD COMPLETA</asp:ListItem>
													</asp:DropDownList>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Eliminar">
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" ToolTip="Eliminar registro"
														ImageUrl="Images/Eliminar.JPG" CausesValidation="False"></asp:ImageButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="CO_DIVI"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="td">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td">
									<TABLE id="Table5" cellSpacing="4" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD style="WIDTH: 498px"></TD>
											<TD style="WIDTH: 117px">
												<asp:button id="btnRegresar" runat="server" CssClass="btn" Text="Regresar"></asp:button></TD>
											<TD style="WIDTH: 135px" align="center">
												<asp:button id="btnGrabar" runat="server" CssClass="btn" Text="Registrar"></asp:button></TD>
											<TD width="500"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
