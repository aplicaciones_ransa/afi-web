Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class ReporteProrroga
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objDocumento As Documento = New Documento

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "32") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Me.lblOpcion.Text = "Reporte de Warrants vencidos y pr�ximos a vencer"
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "32"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objDocumento.gGetWarrantxVencer(Me.txtNroWarrant.Text.Replace("'", ""), Session.Item("IdSico"))
            Me.dgdResultado.DataSource = fn_Quiebre(dt)
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            pr_IMPR_MENS("Error al llenar grilla: " & ex.Message)
        End Try
    End Sub

    Private Function fn_Quiebre(ByVal dtMovimiento As DataTable) As DataTable
        Try
            Dim dr As DataRow
            Dim drNU_REGI As DataRow
            Dim strCO_CLIE_ACTU As String = "&nbsp;"
            Dim dtTCDOCU_CLIE As New DataTable
            dtTCDOCU_CLIE.Columns.Add("NU_TITU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("TI_TITU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("IM_SALD", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("FE_VENC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FE_VENC_LIMI", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FL_PROR", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FL_CONT", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_CLIE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NO_CLIE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_ENTI_FINA", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("TI_DOCU_RECE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("NU_DOCU_RECE", GetType(System.String))

            For i As Integer = 0 To dtMovimiento.Rows.Count - 1
                If strCO_CLIE_ACTU <> dtMovimiento.Rows(i)("CO_CLIE") Then
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("NU_TITU") = "Depositante : " & dtMovimiento.Rows(i)("NO_CLIE") & " - RUC : " & dtMovimiento.Rows(i)("CO_CLIE")
                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    strCO_CLIE_ACTU = dtMovimiento.Rows(i)("CO_CLIE")
                End If
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("NU_TITU") = dtMovimiento.Rows(i)("NU_TITU")
                drNU_REGI("TI_TITU") = dtMovimiento.Rows(i)("TI_TITU")
                drNU_REGI("DE_MERC") = dtMovimiento.Rows(i)("DE_MERC")
                drNU_REGI("CO_MONE") = dtMovimiento.Rows(i)("CO_MONE")
                drNU_REGI("IM_SALD") = CType(dtMovimiento.Rows(i)("IM_SALD"), Decimal)
                drNU_REGI("FE_VENC") = dtMovimiento.Rows(i)("FE_VENC")
                drNU_REGI("FE_VENC_LIMI") = dtMovimiento.Rows(i)("FE_VENC_LIMI")
                drNU_REGI("FL_PROR") = dtMovimiento.Rows(i)("FL_PROR")
                drNU_REGI("FL_CONT") = dtMovimiento.Rows(i)("FL_CONT")
                drNU_REGI("CO_CLIE") = dtMovimiento.Rows(i)("CO_CLIE")
                drNU_REGI("NO_CLIE") = dtMovimiento.Rows(i)("NO_CLIE")
                drNU_REGI("CO_ENTI_FINA") = dtMovimiento.Rows(i)("CO_ENTI_FINA")
                drNU_REGI("TI_DOCU_RECE") = dtMovimiento.Rows(i)("TI_DOCU_RECE")
                drNU_REGI("NU_DOCU_RECE") = dtMovimiento.Rows(i)("NU_DOCU_RECE")

                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            Next
            Return dtTCDOCU_CLIE
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Function

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    'Public Sub btnProrroga_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim dtMail As New DataTable
    '        Dim strEmails As String
    '        Dim imgEditar As Button = CType(sender, Button)
    '        Dim dgi As DataGridItem
    '        dgi = CType(imgEditar.Parent.Parent, DataGridItem)
    '        dtMail = objDocumento.gInsProrroga(dgi.Cells(0).Text(), dgi.Cells(1).Text(), _
    '        dgi.Cells(10).Text(), dgi.Cells(11).Text(), dgi.Cells(5).Text(), dgi.Cells(4).Text(), _
    '        dgi.Cells(6).Text(), dgi.Cells(2).Text(), dgi.Cells(8).Text(), dgi.Cells(12).Text(), dgi.Cells(13).Text(), dgi.Cells(7).Text(), Session.Item("IdUsuario"))

    '        If dtMail.Rows.Count > 0 Then
    '            If dtMail.Rows(0)("nro") = 0 Then
    '                For i As Integer = 0 To dtMail.Rows.Count - 1
    '                    If dtMail.Rows(i)("EMAI") <> "" Then
    '                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
    '                    End If
    '                Next
    '                objFunciones.SendMail("Depsa � Sistema de Warrants Electr�nicos <" & System.Configuration.ConfigurationSettings.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1), _
    '                "Ingresar informaci�n de prorroga - " & Session.Item("NombreEntidad"), _
    '                "Estimados Se�ores:<br><br>" & _
    '                "La entidad financiera notificada deber� ingresar la informaci�n necesaria para poder emitir la prorroga:" & _
    '                "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & dgi.Cells(0).Text() & "</FONT></STRONG><br>" & _
    '                "<br>Secuencial de prorroga N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("SECUENCIA") & "</FONT></STRONG><br>" & _
    '                "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" & _
    '                "<br><br>Saludos.<br><br>S�rvanse ingresar al SGWLE para aprobaciones: <A href='https://www.depsa.com.pe/SGWLE/Login.aspx'>Depsa - Sistema de Warrants Electr�nicos</A>", "")
    '            Else
    '                pr_IMPR_MENS(dtMail.Rows(0)("EMAI"))
    '                Exit Sub
    '            End If
    '        End If
    '        pr_IMPR_MENS("Se gener� correctamente ")
    '    Catch ex As Exception
    '        pr_IMPR_MENS("ERROR " + ex.Message)
    '    End Try
    'End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnProrroga As ImageButton
        Dim btnContable As ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If e.Item.Cells(1).Text = "&nbsp;" Then
                e.Item.Cells.RemoveAt(8)
                e.Item.Cells.RemoveAt(7)
                e.Item.Cells.RemoveAt(6)
                e.Item.Cells.RemoveAt(5)
                e.Item.Cells.RemoveAt(4)
                e.Item.Cells.RemoveAt(3)
                e.Item.Cells.RemoveAt(2)
                e.Item.Cells.RemoveAt(1)
                e.Item.Cells(0).ColumnSpan = 7
                e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                e.Item.Cells(0).Width = System.Web.UI.WebControls.Unit.Pixel(500)
                e.Item.Cells(0).Font.Bold = True
                e.Item.Cells(0).Text = e.Item.DataItem("NU_TITU")
                e.Item.ForeColor = System.Drawing.Color.FromName("#003466")
                e.Item.BackColor = System.Drawing.Color.FromName("#E5E5E5")
            Else
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
                btnProrroga = CType(e.Item.FindControl("btnProrroga"), ImageButton)
                btnContable = CType(e.Item.FindControl("btnContable"), ImageButton)
                btnProrroga.Enabled = False
                btnContable.Enabled = False
                If e.Item.DataItem("FL_PROR") = "S" Then
                    btnProrroga.ImageUrl = "Images/Activar.gif"
                Else
                    btnProrroga.ImageUrl = "Images/Transparente.JPG"
                End If
                If e.Item.DataItem("FL_CONT") = "S" Then
                    btnContable.ImageUrl = "Images/Activar.gif"
                Else
                    btnContable.ImageUrl = "Images/Transparente.JPG"
                End If
            End If
            'btnProrroga.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de generar prorroga al warrant " & e.Item.DataItem("NUMERO_TITULO") & "?')== false) return false;")
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
