Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AlmPedidoNuevo
    Inherits System.Web.UI.Page
    Private objParametro As clsCNParametro
    Private objRetiro As clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objWarrant As clsCNWarrant
    Private objWMS As clsCNWMS
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents ddlestado As System.Web.UI.WebControls.DropDownList
    Private bolApto As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaVenc As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkSolicitud As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        pr_VALI_SESI()
        PoliticaCache()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Nuevo Retiro"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_NUEVO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    'Dim dttFecha As DateTime
                    'Dim Dia As String
                    'Dim Mes As String
                    'Dim Anio As String
                    'dttFecha = Date.Today.Subtract(TimeSpan.FromDays(365))
                    'Dia = "00" + CStr(dttFecha.Day)
                    'Mes = "00" + CStr(dttFecha.Month)
                    'Anio = "0000" + CStr(dttFecha.Year)
                    'Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    'Dia = "00" + CStr(Now.Day)
                    'Mes = "00" + CStr(Now.Month)
                    'Anio = "0000" + CStr(Now.Year)
                    'Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    GetCargarTipoMercaderia()
                    LlenarAlmacenes()

                    If Session.Item("Co_AlmaS") = "" Then
                        Session.Item("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session.Item("Co_AlmaS")
                    End If
                    If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaS"))) > 0 Then
                        Response.Redirect("AlmPedidoNuevo_W.aspx", False)
                        Exit Sub
                    End If
                    ListaReferencia()
                    InicializaBusqueda()
                    Bindatagrid()
                    If Session.Item("strNroRetiro") <> Nothing Then
                        pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("strNroRetiro"))
                        Session.Remove("strNroRetiro")
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub GetCargarTipoMercaderia()
        Me.cboModalidad.Items.Clear()
        Try
            objWarrant = New clsCNWarrant
            Dim dt As DataTable
            With objWarrant
                dt = .gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "N")
            End With
            For Each dr As DataRow In dt.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI").trim, dr("CO_MODA_MOVI").trim))
                If dr("CO_MODA_MOVI") = "001" Then
                    Me.cboModalidad.SelectedValue = "001"
                End If
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaRetiroNuevo", Me.txtNroReferencia.Text.Replace("'", "") & "||" & txtFechaVenc.Value & "||" & Me.cboAlmacen.SelectedValue & "||" & Me.cboReferencia.SelectedValue & "||" & Me.ddlestado.SelectedValue)
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaRetiroNuevo") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaRetiroNuevo").ToString
            strFiltros = Split(strCadenaBusqueda, "||")
            Me.txtNroReferencia.Text = strFiltros(0)
            Me.txtFechaVenc.Value = strFiltros(1)
            Me.cboAlmacen.SelectedValue = strFiltros(2)
            Me.cboReferencia.SelectedValue = strFiltros(3)
            Me.ddlestado.SelectedValue = strFiltros(4)
        End If
    End Sub

    Private Sub ListaReferencia()
        objParametro = New clsCNParametro
        objParametro.gCNListarReferencia(Me.cboReferencia, "4")
    End Sub

    Private Sub Bindatagrid()
        Try
            objRetiro = New clsCNRetiro
            Dim dtRetiro As DataTable
            dtRetiro = objRetiro.gCNGetBuscarMercaderia(Session("CoEmpresa"), Session.Item("IdSico"), Me.cboAlmacen.SelectedValue, Me.cboReferencia.SelectedValue,
                                                        Me.txtNroReferencia.Text, Me.txtFechaVenc.Value, Session.Item("IdUsuario"), Me.cboModalidad.SelectedValue, Me.ddlestado.SelectedValue)

            Me.dgResultado.DataSource = dtRetiro
            Me.dgResultado.DataBind()
            Me.lblMensaje.Text = "Se encontraron " & dtRetiro.Rows.Count & " Registros"
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try
            Dim dgItem As DataGridItem
            Dim bST_VALI As Boolean = False

            For Each dgItem In dgResultado.Items
                If CType(dgItem.FindControl("chkItem"), CheckBox).Checked = True And CType(dgItem.FindControl("chkItem"), CheckBox).Enabled = True Then
                    bST_VALI = True
                    Exit For
                End If
            Next

            If bST_VALI = True Then
                If ValidarItemsSeleccionados() = True Then
                    Response.Redirect("AlmRetiroSolicitud.aspx", False)
                End If
            Else
                Me.lblMensaje.Text = "Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Function AgregarRegistros() As DataTable
        Dim dt1 As New DataTable
        Dim dgItem As DataGridItem
        Dim dr As DataRow

        If Session.Item("dt") Is Nothing Then
            dt1.Columns.Add(New DataColumn("NU_DOCU_RECE", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_SECU", GetType(Integer)))
            dt1.Columns.Add(New DataColumn("DE_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_PROD_CLIE", GetType(String)))
            dt1.Columns.Add(New DataColumn("LOTE", GetType(String)))
            dt1.Columns.Add(New DataColumn("FE_VCTO_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_TIPO_BULT", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_UNID_SALD", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("NU_UNID_RETI", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("ADUANERO", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_ALMA_ACTU", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_UNID", GetType(String)))
            dt1.Columns.Add(New DataColumn("FLG_SEL", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_BODE_ACTU", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_UBIC_ACTU", GetType(String)))
            dt1.Columns.Add(New DataColumn("DE_TIPO_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("DE_STIP_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("TI_DOCU_RECE", GetType(String)))
            dt1.Columns.Add(New DataColumn("FE_REGI_INGR", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_SECU_UBIC", GetType(Integer)))
            dt1.Columns.Add(New DataColumn("CO_TIPO_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_STIP_MERC", GetType(String)))
            dt1.Columns.Add(New DataColumn("DE_TIPO_BULT", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_UNME_PESO", GetType(String)))
            dt1.Columns.Add(New DataColumn("DE_UNME", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_PESO_RETI", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("NU_UNID_PESO", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("CO_MONE", GetType(String)))
            dt1.Columns.Add(New DataColumn("TI_TITU", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_TITU", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_UNME_AREA", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_AREA_USAD", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("CO_UNME_VOLU", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_VOLU_USAD", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("ST_VALI_RETI", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_UNID_RECI", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("NU_PESO_RECI", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("NU_PESO_UNID", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("TI_UNID_PESO", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_MANI", GetType(String)))
            dt1.Columns.Add(New DataColumn("ST_TEAL", GetType(String)))
            dt1.Columns.Add(New DataColumn("IM_UNIT", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("CO_MODA_MOVI", GetType(String)))
        Else
            dt1 = CType(Session.Item("dt"), DataTable).Copy()
        End If

        Dim nNU_UNID_RETI, nNU_PESO_UNID, nNU_PESO_RETI, nNU_UNID_RECI, nNU_PESO_RECI As Decimal
        Dim sTI_PESO_UNID As String

        For Each dgItem In dgResultado.Items
            If CType(dgItem.FindControl("chkItem"), CheckBox).Checked = True And CType(dgItem.FindControl("chkItem"), CheckBox).Enabled = True Then
                nNU_UNID_RETI = IIf(CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text.Trim = "", 0, CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text)
                nNU_PESO_RETI = IIf(CType(dgItem.FindControl("txtBultoRetirar"), TextBox).Text.Trim = "", 0, CType(dgItem.FindControl("txtBultoRetirar"), TextBox).Text)
                If nNU_UNID_RETI > 0 Then
                    nNU_UNID_RETI = FormatNumber(nNU_UNID_RETI, 3)
                    nNU_PESO_UNID = IIf(dgItem.Cells(38).Text.Trim = "&nbsp;", 0, dgItem.Cells(38).Text)
                    sTI_PESO_UNID = dgItem.Cells(39).Text
                    nNU_UNID_RECI = IIf(dgItem.Cells(9).Text = "&nbsp;", 0, dgItem.Cells(9).Text)
                    nNU_PESO_RECI = IIf(dgItem.Cells(26).Text = "&nbsp;", 0, dgItem.Cells(26).Text)

                    'si la columna peso por unidad , campo nu_peso_unid del carga es mayor a cero
                    'y la columna sTI_PESO_UNID es 'B' entonces calcula el peso 
                    If (nNU_PESO_UNID > 0 And sTI_PESO_UNID = "B") Then
                        If nNU_UNID_RETI <> nNU_UNID_RECI Then
                            nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI * nNU_PESO_UNID, 6)
                        Else
                            nNU_PESO_RETI = nNU_PESO_RECI
                        End If
                    End If
                    If nNU_PESO_UNID > 0 And sTI_PESO_UNID = "U" Then
                        If nNU_UNID_RETI <> nNU_UNID_RECI Then
                            nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI / nNU_PESO_UNID, 6)
                        Else
                            nNU_PESO_RETI = nNU_PESO_RECI
                        End If
                    End If
                Else
                    nNU_PESO_RETI = FormatNumber(nNU_PESO_RETI, 3)
                    nNU_PESO_UNID = IIf(dgItem.Cells(38).Text.Trim = "&nbsp;", 0, dgItem.Cells(38).Text)
                    sTI_PESO_UNID = dgItem.Cells(39).Text
                    nNU_UNID_RECI = IIf(dgItem.Cells(9).Text = "&nbsp;", 0, dgItem.Cells(9).Text)
                    nNU_PESO_RECI = IIf(dgItem.Cells(26).Text = "&nbsp;", 0, dgItem.Cells(26).Text)

                    'si la columna peso por unidad , campo nu_peso_unid del carga es mayor a cero
                    'y la columna sTI_PESO_UNID es 'B' entonces calcula el peso 
                    If (nNU_PESO_UNID > 0 And sTI_PESO_UNID = "B") Then
                        If nNU_PESO_RETI <> nNU_PESO_RECI Then
                            nNU_UNID_RETI = FormatNumber(nNU_PESO_RETI / nNU_PESO_UNID, 6)
                        Else
                            nNU_UNID_RETI = nNU_UNID_RECI
                        End If
                    End If
                    If nNU_PESO_UNID > 0 And sTI_PESO_UNID = "U" Then
                        If nNU_PESO_RETI <> nNU_PESO_RECI Then
                            nNU_UNID_RETI = FormatNumber(nNU_PESO_RETI * nNU_PESO_UNID, 6)
                        Else
                            nNU_UNID_RETI = nNU_UNID_RECI
                        End If
                    End If
                End If

                dr = dt1.NewRow
                dr("NU_DOCU_RECE") = dgItem.Cells(1).Text
                dr("NU_SECU") = dgItem.Cells(2).Text
                dr("DE_MERC") = dgItem.Cells(3).Text & " " & IIf(dgItem.Cells(4).Text = "&nbsp;", "", dgItem.Cells(4).Text) & " " & IIf(dgItem.Cells(5).Text = "&nbsp;", "", dgItem.Cells(5).Text)
                dr("CO_PROD_CLIE") = dgItem.Cells(4).Text
                dr("LOTE") = dgItem.Cells(5).Text
                dr("FE_VCTO_MERC") = dgItem.Cells(6).Text
                dr("CO_TIPO_BULT") = dgItem.Cells(8).Text
                dr("NU_UNID_SALD") = IIf(dgItem.Cells(9).Text = "&nbsp;", 0, dgItem.Cells(9).Text)
                'dr("NU_UNID_RETI") = IIf(CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text.Trim = "", 0, IIf(CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text.Trim = "NaN", 0, CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text))
                dr("NU_UNID_RETI") = nNU_UNID_RETI
                dr("ADUANERO") = dgItem.Cells(11).Text
                dr("CO_ALMA_ACTU") = dgItem.Cells(12).Text
                dr("CO_UNID") = dgItem.Cells(13).Text
                dr("FLG_SEL") = dgItem.Cells(14).Text
                dr("CO_BODE_ACTU") = dgItem.Cells(7).Text
                dr("CO_UBIC_ACTU") = dgItem.Cells(15).Text
                dr("DE_TIPO_MERC") = dgItem.Cells(16).Text
                dr("DE_STIP_MERC") = dgItem.Cells(17).Text
                dr("TI_DOCU_RECE") = dgItem.Cells(18).Text
                dr("FE_REGI_INGR") = dgItem.Cells(19).Text
                dr("NU_SECU_UBIC") = IIf(dgItem.Cells(20).Text = "&nbsp;", 0, dgItem.Cells(20).Text)
                dr("CO_TIPO_MERC") = dgItem.Cells(21).Text
                dr("CO_STIP_MERC") = dgItem.Cells(22).Text
                dr("DE_TIPO_BULT") = dgItem.Cells(23).Text
                dr("CO_UNME_PESO") = dgItem.Cells(24).Text
                dr("DE_UNME") = dgItem.Cells(25).Text
                'dr("NU_PESO_RETI") = IIf(CType(dgItem.FindControl("txtBultoRetirar"), TextBox).Text.Trim = "", 0, IIf(CType(dgItem.FindControl("txtBultoRetirar"), TextBox).Text.Trim = "NaN", 0, CType(dgItem.FindControl("txtBultoRetirar"), TextBox).Text))
                dr("NU_PESO_RETI") = nNU_PESO_RETI
                dr("NU_UNID_PESO") = IIf(dgItem.Cells(26).Text = "&nbsp;", 0, dgItem.Cells(26).Text)
                dr("CO_MONE") = dgItem.Cells(28).Text
                dr("TI_TITU") = dgItem.Cells(29).Text
                dr("NU_TITU") = dgItem.Cells(30).Text
                dr("CO_UNME_AREA") = dgItem.Cells(31).Text
                dr("NU_AREA_USAD") = IIf(dgItem.Cells(32).Text = "&nbsp;", 0, dgItem.Cells(32).Text)
                dr("CO_UNME_VOLU") = dgItem.Cells(33).Text
                dr("NU_VOLU_USAD") = IIf(dgItem.Cells(34).Text = "&nbsp;", 0, dgItem.Cells(34).Text)
                dr("ST_VALI_RETI") = dgItem.Cells(35).Text
                dr("NU_UNID_RECI") = IIf(dgItem.Cells(36).Text = "&nbsp;", 0, dgItem.Cells(36).Text)
                dr("NU_PESO_RECI") = IIf(dgItem.Cells(37).Text = "&nbsp;", 0, dgItem.Cells(37).Text)
                dr("NU_PESO_UNID") = IIf(dgItem.Cells(38).Text = "&nbsp;", 0, dgItem.Cells(38).Text)
                dr("TI_UNID_PESO") = dgItem.Cells(39).Text
                dr("NU_MANI") = dgItem.Cells(40).Text
                dr("ST_TEAL") = dgItem.Cells(41).Text
                dr("IM_UNIT") = IIf(dgItem.Cells(42).Text = "&nbsp;", 0, dgItem.Cells(42).Text)
                dr("CO_MODA_MOVI") = dgItem.Cells(43).Text
                dt1.Rows.Add(dr)
            End If
        Next
        Return dt1
    End Function

    Private Function ValidarItemsSeleccionados() As Boolean
        Dim bolRetorno As Boolean = True
        Dim bolcantidad As Boolean = True
        Dim bST_SEL0 As Boolean = False
        Dim bST_SELE As Boolean = False
        Dim bST_SEL1 As Boolean = False
        Dim bST_SEL2 As Boolean = False
        Dim bST_SEL3 As Boolean = False
        Dim bST_SEL4 As Boolean = False
        Dim bST_SEL5 As Boolean = False

        Dim sCO_ALMA_EVAL As String
        Dim sNU_DOCU_REC1 As String = ""
        Dim sNU_SECU As Integer = 0
        Dim sNU_TITU As String
        Dim sTI_DOCU_REC1 As String
        Dim nNU_FILA As Integer
        Dim sNU_MANI_EVAL As String
        Dim sVA_EVAL_FLAG As String
        Dim sCO_MONE As String
        Dim dt As DataTable
        Dim dr As DataRow
        Try
            dt = AgregarRegistros()
            sCO_MONE = dt.Rows(0)("CO_MONE")
            For Each dr In dt.Rows
                bST_SEL2 = True

                If sCO_MONE <> dr("CO_MONE") Then
                    bST_SEL5 = True
                End If

                If dr("NU_UNID_RETI") <= 0 And dr("NU_PESO_RETI") <= 0 Then
                    bolRetorno = False
                End If

                If dr("NU_UNID_RETI") > dr("NU_UNID_SALD") Or dr("NU_PESO_RETI") > dr("NU_UNID_PESO") Then
                    bolcantidad = False
                End If

                If bST_SEL4 = False Then
                    sCO_ALMA_EVAL = dr("CO_ALMA_ACTU")
                    bST_SEL4 = True
                End If

                If bST_SEL4 = True And dr("CO_ALMA_ACTU") <> sCO_ALMA_EVAL Then
                    bST_SEL3 = True
                End If

                If bST_SEL1 = False Then
                    sNU_TITU = dr("NU_TITU")
                    sTI_DOCU_REC1 = dr("TI_DOCU_RECE")
                    sNU_DOCU_REC1 = dr("NU_DOCU_RECE")
                    bST_SEL1 = True
                End If

                If dr("NU_DOCU_RECE") < sNU_DOCU_REC1 Then
                    sTI_DOCU_REC1 = dr("TI_DOCU_RECE")
                    sNU_DOCU_REC1 = dr("NU_DOCU_RECE")
                End If
            Next
            If bST_SEL2 = False Then
                Me.lblMensaje.Text = "Debe seleccionar por lo menos un �tem"
                Return False
            End If
            If bST_SEL3 = True Then
                Me.lblMensaje.Text = "Comprobantes generados en diferentes Almacenes, Operaci�n No V�lida"
                Return False
            End If
            If bST_SEL5 = True Then
                Me.lblMensaje.Text = "No puede retirar productos con dos monedas distintas"
                Return False
            End If
            If bolRetorno = False Then
                Me.lblMensaje.Text = "No ha ingresado la cantidad a retirar"
                Return False
            End If
            If bolcantidad = False Then
                Me.lblMensaje.Text = "Esta tratando de retirar mas de lo que tiene disponible"
                Return False
            End If

            bST_SELE = False
            bST_SEL1 = False
            bST_SEL2 = False

            For Each dr In dt.Rows
                If bST_SEL1 = False Then
                    sTI_DOCU_REC1 = dr("TI_DOCU_RECE")
                    sNU_DOCU_REC1 = dr("NU_DOCU_RECE")
                    bST_SEL1 = True
                End If
                If dr("NU_DOCU_RECE") <> sNU_DOCU_REC1 Then
                    bST_SEL2 = True
                End If
                If dr("ST_VALI_RETI") = "N" Then
                    bST_SELE = True
                End If
            Next
            If bST_SELE = True And bST_SEL2 = True Then
                Me.lblMensaje.Text = "Modalidad no permite generar un retiro de diferentes comprobantes"
                Return False
            End If
            Dim filterRows As DataRow()
            For Each dr In dt.Rows
                filterRows = dt.Select("NU_DOCU_RECE = " & dr("NU_DOCU_RECE") & " and NU_SECU = " & dr("NU_SECU"), "")
                If filterRows.Length > 1 Then
                    bST_SEL0 = True
                    Exit For
                End If

                'If dr("NU_DOCU_RECE") = sNU_DOCU_REC1 And dr("NU_SECU") = sNU_SECU Then
                '    sTI_DOCU_REC1 = dr("TI_DOCU_RECE")
                '    sNU_DOCU_REC1 = dr("NU_DOCU_RECE")
                '    bST_SEL0 = True
                'Else
                '    sTI_DOCU_REC1 = dr("TI_DOCU_RECE")
                '    sNU_DOCU_REC1 = dr("NU_DOCU_RECE")
                '    sNU_SECU = dr("NU_SECU")
                'End If
            Next
            If bST_SEL0 = True Then
                Bindatagrid()
                Me.lblMensaje.Text = "No puede agregar varias veces el mismo �tem, verifique!!"
                Return False
            End If
            Session.Add("dt", dt)
            Return True
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
            Return False
        End Try
    End Function

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim dt As DataTable
        Dim dr As DataRow()
        Dim txtPeso As New TextBox
        Dim txtUnidad As New TextBox
        dt = CType(Session("dt"), DataTable)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(11).Text = "1" Then
                    'CType(e.Item.FindControl("chkItem"), CheckBox).Enabled = False
                    'CType(e.Item.FindControl("txtUnidRetirar"), TextBox).Enabled = False
                    e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                End If
                If e.Item.DataItem("ESTADO").ToString.Trim <> "" Then
                    e.Item.BackColor = System.Drawing.Color.Thistle
                End If

                If e.Item.DataItem("st_merc_repr") = "S" Then
                    CType(e.Item.FindControl("chkItem"), CheckBox).Enabled = False
                    CType(e.Item.FindControl("txtUnidRetirar"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtBultoRetirar"), TextBox).Enabled = False
                    e.Item.BackColor = System.Drawing.Color.DarkSeaGreen 'FromArgb(245, 220, 140)
                End If
                txtPeso = CType(e.Item.FindControl("txtBultoRetirar"), TextBox)
                txtUnidad = CType(e.Item.FindControl("txtUnidRetirar"), TextBox)
                txtPeso.Attributes.Add("onkeypress", "Javascript:validardecimal();")
                txtUnidad.Attributes.Add("onkeypress", "Javascript:validardecimal();")
                txtUnidad.Attributes.Add("onkeyup", "Javascript:CalcularPeso(this.value, '" & txtPeso.ClientID & "', '" & e.Item.DataItem("NU_UNID_SALD") & "', '" & e.Item.DataItem("NU_UNID_PESO") & "','" & txtUnidad.ClientID & "');")
                txtPeso.Attributes.Add("onkeyup", "Javascript:CalcularCantidad(this.value, '" & txtUnidad.ClientID & "', '" & e.Item.DataItem("NU_UNID_SALD") & "', '" & e.Item.DataItem("NU_UNID_PESO") & "', '" & txtPeso.ClientID & "');")

                If Session.Item("dt") Is Nothing Then
                Else
                    If dt.Rows.Count <> 0 Then
                        dr = dt.Select("NU_DOCU_RECE=" & e.Item.DataItem("NU_DOCU_RECE") & " and  NU_SECU=" & e.Item.DataItem("NU_SECU"))
                        If dr.Length > 0 Then
                            CType(e.Item.FindControl("chkItem"), CheckBox).Checked = True
                            CType(e.Item.FindControl("chkItem"), CheckBox).Enabled = False
                            CType(e.Item.FindControl("txtUnidRetirar"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtBultoRetirar"), TextBox).Enabled = False
                        End If
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Bindatagrid()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaS"))) > 0 Then
            Response.Redirect("AlmPedidoNuevo_W.aspx", False)
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
