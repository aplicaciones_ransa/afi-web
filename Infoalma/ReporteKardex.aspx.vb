Imports Library.AccesoBL
Imports Library.AccesoDB
Imports System.Text
Imports System.IO
Imports System.Data
Imports Infodepsa

Public Class ReporteKardex
    Inherits System.Web.UI.Page
    Private objDocumento As clsBLDocumento = New clsBLDocumento
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents hlkDetalle As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipoMercaderia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "28") Then
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "28"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Movimiento de Warrants"
                loadDepositante()
                loadTipoMerc()
                loadEstado()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadTipoMerc()
        Dim dtTipoMerc As New DataTable
        Me.cboTipoMercaderia.Items.Clear()
        dtTipoMerc = objEntidad.gGetTipoMerc()
        Me.cboTipoMercaderia.Items.Add(New ListItem("-------------------- Todos -------------------", "0"))
        For Each dr As DataRow In dtTipoMerc.Rows
            Me.cboTipoMercaderia.Items.Add(New ListItem(dr("DE_TIPO_MERC").ToString(), CType(dr("CO_TIPO_MERC"), String).Trim))
        Next
        dtTipoMerc.Dispose()
        dtTipoMerc = Nothing
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable = New DataTable("Items")
        Try
            dt = objDocumento.BLGetKardex(Me.cboDepositante.SelectedValue, Me.cboEstado.SelectedValue,
                                              Session.Item("IdSico"), Me.cboTipoMercaderia.SelectedValue,
                                              Me.txtNroWarrant.Text, Session.Item("IdTipoEntidad"))
            Session.Add("dtKardex", dt)

            Me.dgdResultado.DataSource = fn_Quiebre(dt)
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Function fn_Quiebre(ByVal dtMovimiento As DataTable) As DataTable
        Try
            Dim dr As DataRow
            Dim drNU_REGI As DataRow
            Dim strCO_CLIE_ACTU As String = "&nbsp;"
            Dim dtTCDOCU_CLIE As New DataTable
            dtTCDOCU_CLIE.Columns.Add("CO_EMPR", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("CO_UNID", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("TI_DOCU_RECE", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Operaci�n", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("TI_TITU", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Warrant", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Certificado", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("WarrantSiguiente", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("WarrantAnterior", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Producto", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Unid.Medida", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Cant/PesoInicial", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Moneda", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("PrecioUnitario", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("SaldoInicialWarrant", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("SaldoProducto", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("SaldoActualWarrant", GetType(System.Decimal))
            dtTCDOCU_CLIE.Columns.Add("FechaIngreso", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FechaVencimiento", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("FechaVencimientoLimite", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Nro.Garant�a", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Prorroga", GetType(System.Boolean))
            dtTCDOCU_CLIE.Columns.Add("Contable", GetType(System.Boolean))
            dtTCDOCU_CLIE.Columns.Add("TipoWarrant", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Electr�nico", GetType(System.Boolean))
            dtTCDOCU_CLIE.Columns.Add("RUC", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Cliente", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Cnt.Items", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Unid.MedidaConvertible", GetType(System.String))
            dtTCDOCU_CLIE.Columns.Add("Cant.Convertible", GetType(System.Decimal))

            For i As Integer = 0 To dtMovimiento.Rows.Count - 1
                If strCO_CLIE_ACTU <> dtMovimiento.Rows(i)("RUC") Then
                    drNU_REGI = dtTCDOCU_CLIE.NewRow()
                    drNU_REGI("CO_EMPR") = ""
                    drNU_REGI("CO_UNID") = ""
                    drNU_REGI("TI_DOCU_RECE") = ""
                    drNU_REGI("Operaci�n") = "Depositante : " & dtMovimiento.Rows(i)("Cliente") & " - RUC : " & dtMovimiento.Rows(i)("RUC")
                    drNU_REGI("Electr�nico") = CType(0, Boolean)
                    drNU_REGI("Prorroga") = CType(0, Boolean)
                    drNU_REGI("Contable") = CType(0, Boolean)
                    dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                    strCO_CLIE_ACTU = dtMovimiento.Rows(i)("RUC")
                End If
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("CO_EMPR") = dtMovimiento.Rows(i)("CO_EMPR")
                drNU_REGI("CO_UNID") = dtMovimiento.Rows(i)("CO_UNID")
                drNU_REGI("TI_DOCU_RECE") = dtMovimiento.Rows(i)("TI_DOCU_RECE")
                drNU_REGI("Operaci�n") = dtMovimiento.Rows(i)("Operaci�n")
                drNU_REGI("TI_TITU") = dtMovimiento.Rows(i)("TI_TITU")
                drNU_REGI("Warrant") = dtMovimiento.Rows(i)("Warrant")
                drNU_REGI("Certificado") = dtMovimiento.Rows(i)("Certificado")
                drNU_REGI("Moneda") = dtMovimiento.Rows(i)("Moneda")
                drNU_REGI("SaldoInicialWarrant") = CType(dtMovimiento.Rows(i)("SaldoInicialWarrant"), Decimal)
                drNU_REGI("FechaVencimiento") = dtMovimiento.Rows(i)("FechaVencimiento")
                drNU_REGI("FechaVencimientoLimite") = dtMovimiento.Rows(i)("FechaVencimientoLimite")
                drNU_REGI("FechaIngreso") = dtMovimiento.Rows(i)("FechaIngreso")
                drNU_REGI("Electr�nico") = CType(dtMovimiento.Rows(i)("Electr�nico"), Boolean)
                drNU_REGI("RUC") = dtMovimiento.Rows(i)("RUC")
                drNU_REGI("Cliente") = dtMovimiento.Rows(i)("Cliente")
                drNU_REGI("Nro.Garant�a") = dtMovimiento.Rows(i)("Nro.Garant�a")
                drNU_REGI("TipoWarrant") = dtMovimiento.Rows(i)("TipoWarrant")
                drNU_REGI("Prorroga") = CType(dtMovimiento.Rows(i)("Prorroga"), Boolean)
                drNU_REGI("Contable") = CType(dtMovimiento.Rows(i)("Contable"), Boolean)
                drNU_REGI("SaldoActualWarrant") = CType(dtMovimiento.Rows(i)("SaldoActualWarrant"), Decimal)
                drNU_REGI("Producto") = dtMovimiento.Rows(i)("Producto")
                drNU_REGI("Unid.Medida") = dtMovimiento.Rows(i)("Unid.Medida")
                drNU_REGI("Cant/PesoInicial") = dtMovimiento.Rows(i)("Cant/PesoInicial")
                drNU_REGI("PrecioUnitario") = dtMovimiento.Rows(i)("PrecioUnitario")
                drNU_REGI("SaldoProducto") = CType(dtMovimiento.Rows(i)("SaldoProducto"), Decimal)
                drNU_REGI("WarrantAnterior") = dtMovimiento.Rows(i)("WarrantAnterior")
                drNU_REGI("WarrantSiguiente") = dtMovimiento.Rows(i)("WarrantSiguiente")
                drNU_REGI("Cnt.Items") = dtMovimiento.Rows(i)("Cnt.Items")
                drNU_REGI("Unid.MedidaConvertible") = dtMovimiento.Rows(i)("Unid.MedidaConvertible")
                drNU_REGI("Cant.Convertible") = dtMovimiento.Rows(i)("Cant.Convertible")
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
            Next
            Return dtTCDOCU_CLIE
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Function

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim page As Page = New Page
        Dim form As HtmlForm = New HtmlForm
        Dim dg As DataGrid = New DataGrid

        dg.DataSource = CType(Session.Item("dtKardex"), DataTable)
        dg.DataBind()

        dg.EnableViewState = False
        page.DesignerInitialize()
        page.Controls.Add(form)
        form.Controls.Add(dg)
        page.RenderControl(htw)

        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=Kardex.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
        dg = Nothing
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Private Sub loadDepositante()
        Try
            Dim dtFinanciador As DataTable
            cboDepositante.Items.Clear()
            dtFinanciador = objEntidad.gCADGetListarFinanciadores("01", Session("IdSico"), Session("IdTipoEntidad"))
            cboDepositante.Items.Add(New ListItem("-------------------- Todos -------------------", 0))
            If dtFinanciador Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dtFinanciador.Rows
                cboDepositante.Items.Add(New ListItem(dr("DE_LARG_ENFI"), dr("CO_ENTI_FINA")))
            Next
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub loadEstado()
        objfunciones.GetLlenaEstado(Me.cboEstado, "W")
        Me.cboEstado.SelectedIndex = 1
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(1).Text = "&nbsp;" And e.Item.Cells(2).Text = "&nbsp;" And e.Item.Cells(3).Text <> "&nbsp;" Then
                    e.Item.Cells.RemoveAt(25)
                    e.Item.Cells.RemoveAt(24)
                    e.Item.Cells.RemoveAt(23)
                    e.Item.Cells.RemoveAt(22)
                    e.Item.Cells.RemoveAt(21)
                    e.Item.Cells.RemoveAt(20)
                    e.Item.Cells.RemoveAt(19)
                    e.Item.Cells.RemoveAt(18)
                    e.Item.Cells.RemoveAt(17)
                    e.Item.Cells.RemoveAt(16)
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells(3).ColumnSpan = 23
                    e.Item.Cells(3).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(3).Width = System.Web.UI.WebControls.Unit.Pixel(500)
                    e.Item.Cells(3).Font.Bold = True
                    e.Item.Cells(3).Text = e.Item.DataItem("Operaci�n")
                    e.Item.ForeColor = System.Drawing.Color.FromName("#003466")
                    e.Item.BackColor = System.Drawing.Color.FromName("#E5E5E5")
                Else
                    e.Item.Cells(3).Font.Bold = True
                    e.Item.Cells(10).Text = IIf(IsDBNull(e.Item.DataItem("Cant/PesoInicial")), "", String.Format("{0:##,##0.00}", CType(e.Item.DataItem("Cant/PesoInicial"), Decimal)))
                    e.Item.Cells(14).Text = IIf(IsDBNull(e.Item.DataItem("PrecioUnitario")), "", String.Format("{0:##,##0.00}", CType(e.Item.DataItem("PrecioUnitario"), Decimal)))
                End If
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        If e.Item.ItemType = ListItemType.Header Then
            e.Item.Cells(16).ToolTip = "* U: �nico �tem  " & Chr(13) & " * M: M�ltiples �tems"
            e.Item.Cells(22).ToolTip = "Warrant con al menos una Prorroga"
            e.Item.Cells(23).ToolTip = "Warrant con Liberaci�n Contable"
            e.Item.Cells(24).ToolTip = "Es Warrant Electr�nico"
            e.Item.Cells(25).ToolTip = "* Simple: Son todos los warrants de mercader�a de origen nacional y nacionalizadas " & Chr(13) & " * Aduanero: Son todos los warrants de mercader�a no nacionalizada"
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objfunciones Is Nothing) Then
            objfunciones = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
