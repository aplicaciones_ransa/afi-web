<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Encuesta.aspx.vb" Inherits="Encuesta" %>
<%@ Register TagPrefix="uc1" TagName="HeaderLogin" Src="UserControls/HeaderLogin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Aalma Per� >> AFI >> Ingreso al Sistema</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="author" content="francisco_uni@hotmail.com">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
	</HEAD>
	<BODY bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD>
						<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%" background="Images/Login.jpg">
							<TBODY>
								<TR>
								</TR>
								<TR>
									<TD vAlign="middle" align="left">
										<uc1:HeaderLogin id="HeaderLogin1" runat="server"></uc1:HeaderLogin>
										<TABLE id="Table3" border="0" cellSpacing="4" cellPadding="0" width="100%">
											<TR>
												<TD align="center"><asp:label id="lblTitulo" runat="server" CssClass="Titulo1"></asp:label></TD>
											</TR>
											<TR>
												<TD class="Subtitulo1" align="center">
													<asp:label style="Z-INDEX: 0" id="lblCuerpo" runat="server"></asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<DIV id="UserPassword">
														<TABLE id="Ingreso" border="0" cellSpacing="4" align="center">
															<TR>
																<TD class="Subtitulo2" colSpan="2" align="center">Encuesta</TD>
															</TR>
															<TR>
																<TD class="text" colSpan="2"><asp:label id="lblPregunta1" runat="server" CssClass="Text" Font-Bold="True"></asp:label></TD>
															</TR>
															<TR>
																<TD class="text"><asp:radiobutton id="rb1a" runat="server" CssClass="Text" GroupName="PREG1"></asp:radiobutton></TD>
																<TD align="left"><asp:radiobutton id="rb1b" runat="server" CssClass="Text" GroupName="PREG1"></asp:radiobutton></TD>
															</TR>
															<TR>
																<TD class="text"><asp:radiobutton id="rb1c" runat="server" GroupName="PREG1"></asp:radiobutton></TD>
																<TD align="left"><asp:radiobutton id="rb1d" runat="server" CssClass="Text" GroupName="PREG1"></asp:radiobutton></TD>
															</TR>
															<TR>
																<TD class="text"></TD>
																<TD align="left"></TD>
															</TR>
															<TR>
																<TD class="Text" width="426" colSpan="2"><asp:label style="Z-INDEX: 0" id="lblPregunta2" runat="server" Font-Bold="True"></asp:label></TD>
															</TR>
															<TR>
																<TD><asp:radiobutton id="rb2a" runat="server" CssClass="Text" GroupName="PREG2"></asp:radiobutton></TD>
																<TD width="50%" align="left"><asp:radiobutton id="rb2b" runat="server" CssClass="Text" GroupName="PREG2"></asp:radiobutton>&nbsp;</TD>
															</TR>
															<TR>
																<TD class="Text" width="430"><asp:radiobutton id="rb2c" runat="server" GroupName="PREG2"></asp:radiobutton></TD>
																<TD class="Text" width="50%" align="left"><asp:radiobutton id="rb2d" runat="server" GroupName="PREG2"></asp:radiobutton></TD>
															</TR>
															<TR>
																<TD></TD>
																<TD width="50%" align="center"></TD>
															</TR>
															<TR>
																<TD class="Text" colSpan="2"><asp:label id="lblPregunta3" runat="server" CssClass="Text" Font-Bold="True"></asp:label></TD>
															</TR>
															<TR>
																<TD><asp:radiobutton id="rb3a" runat="server" CssClass="Text" GroupName="PREG3"></asp:radiobutton></TD>
																<TD class="Text" width="50%" align="left"><asp:radiobutton id="rb3b" runat="server" GroupName="PREG3"></asp:radiobutton></TD>
															</TR>
															<TR>
																<TD class="Text"><asp:radiobutton id="rb3c" runat="server" GroupName="PREG3"></asp:radiobutton></TD>
																<TD class="Text" width="50%" align="left"><asp:radiobutton id="rb3d" runat="server" GroupName="PREG3"></asp:radiobutton></TD>
															</TR>
															<TR>
																<TD class="Text"><asp:radiobutton id="rb3e" runat="server" GroupName="PREG3"></asp:radiobutton></TD>
																<TD class="Text" width="50%" align="left"></TD>
															</TR>
															<TR>
																<TD></TD>
																<TD width="50%" align="center"></TD>
															</TR>
															<TR>
																<TD class="Text" colSpan="2"><asp:label id="lblPregunta4" runat="server" CssClass="Text" Font-Bold="True"></asp:label></TD>
															</TR>
															<TR>
																<TD colSpan="2"><asp:textbox id="txt4a" runat="server" TextMode="MultiLine" Width="848px" CssClass="Text" MaxLength="500"></asp:textbox></TD>
															</TR>
															<TR>
																<TD colSpan="3" align="center"><asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar"></asp:button>
																	<asp:button id="btnCerrar" runat="server" CssClass="btn" Width="80px" Text="Cerrar"></asp:button></TD>
															</TR>
														</TABLE>
													</DIV>
													<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label>
												</TD>
											</TR>
											<TR>
												<TD align="center"></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</BODY>
</HTML>
