<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueEstadoCuenta.aspx.vb" Inherits="CueEstadoCuenta" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Estado de Cuenta</title>
		<META content="text/html; charset=windows-1252" http-equiv="Content-Type">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script>
		   function fn_DetDoc(sCO_UNID,sTI_DOCU_COBR,sNU_DOCU_COBR)
		   {		    
		    var ventana = 'Popup/CueImpresionDetalleDoc.aspx?sCO_UNID='+ sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOCU_COBR + '&sNU_DOCU_COBR=' + sNU_DOCU_COBR;
		    //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
		    window.open(ventana,"Retiro","left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
		   }
		   function fn_Impresion(sMONE)
		   {
		    var ventana = 'Popup/CueImpresionEstadoCuenta.aspx?sMONE=' + sMONE;
		    //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
		    window.open(ventana,"Retiro","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400")
		   }	
		   
		   function AbrirAbono(sCO_UNID,sTI_DOCU_COBR,sNU_DOCU_COBR)
		   {		    
		    var ventana = 'Popup/PoputAbonos.aspx?sCO_UNID='+ sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOCU_COBR + '&sNU_DOCU_COBR=' + sNU_DOCU_COBR;
		    //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
		    window.open(ventana,"Retiro","left=240,top=200,width=600,height=300,toolbar=0,resizable=1")
		   }
		   		
		   		
		    function muestra()
		    {
			if (document.getElementById("Select1").value=="b")
				{
		          document.getElementById("div2").style.visibility="visible"
		          document.getElementById("div1").style.visibility="hidden"
		          document.getElementById("div1").style.height=0
				}
				else
				{
					document.getElementById("div2").style.visibility="hidden"
					document.getElementById("div1").style.visibility="visible"
					document.getElementById("div2").style.height=0
				}
		    }
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0"
		text="#e00de0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table3"
							border="0" cellSpacing="0" cellPadding="0" width="100%" height="400">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label><asp:dropdownlist id="ddTTMONE" runat="server" CssClass="text" Visible="False" Width="200px" DataTextField="descripcion"
													DataValueField="codigo"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="4"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD class="TEXT" vAlign="top"><asp:label id="lbMS_REGI" runat="server" CssClass="text" ForeColor="Red"></asp:label><BR>
												<asp:label id="lbAMER" runat="server" CssClass="texto9pt">DOLARES AMERICANOS</asp:label><BR>
												<asp:datagrid id="dgAMER" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC" HeaderText="FLAG"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_AMER" runat="server" CssClass="texto9pt">TOTAL DOLARES AMERICANOS</asp:label><BR>
												<asp:datagrid id="dgTAMER" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:N2}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:N2}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:N2}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid><asp:label id="lbES_AMER" runat="server" CssClass="texto9pt"></asp:label><asp:label id="lbCANA" runat="server" CssClass="texto9pt">DOLARES CANADIENSES</asp:label><asp:datagrid id="dgCANA" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer1" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_CANA" runat="server" CssClass="texto9pt">TOTAL DOLARES CANADIENSES</asp:label><asp:datagrid id="dgTCANA" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid><asp:label id="lbES_CANA" runat="server" CssClass="texto9pt"></asp:label><asp:label id="lbEURO" runat="server" CssClass="texto9pt">EURO</asp:label><asp:datagrid id="dgEURO" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer2" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_EURO" runat="server" CssClass="texto9pt">TOTAL EUROS</asp:label><asp:datagrid id="dgTEURO" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid><asp:label id="lbES_EURO" runat="server" CssClass="texto9pt"></asp:label><asp:label id="lbLIBR" runat="server" CssClass="texto9pt">LIBRAS ESTERLINAS</asp:label><asp:datagrid id="dgLIBR" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer3" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_LIBR" runat="server" CssClass="texto9pt">TOTAL LIBRAS ESTERLINAS</asp:label><asp:datagrid id="dgTLIBR" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid><asp:label id="lbES_LIBR" runat="server" CssClass="texto9pt"></asp:label><asp:label id="lbSOLE" runat="server" CssClass="texto9pt">NUEVOS SOLES</asp:label><asp:datagrid id="dgSOLE" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC" HeaderText="FLAG"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer4" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_SOLE" runat="server" CssClass="texto9pt">TOTAL NUEVOS SOLES</asp:label><asp:datagrid id="dgTSOLE" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid><asp:label id="lbES_SOLE" runat="server" CssClass="texto9pt"></asp:label><asp:label id="lbYENE" runat="server" CssClass="texto9pt">YENES</asp:label><asp:datagrid id="dgYENE" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" AllowPaging="True">
													<AlternatingItemStyle BorderColor="White" VerticalAlign="Top" BackColor="Gainsboro"></AlternatingItemStyle>
													<ItemStyle VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn HeaderText="Tip.Doc."></asp:BoundColumn>
														<asp:BoundColumn DataField="NUMERO" HeaderText="Documento">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="FECHA EMISION" HeaderText="Fec.Doc." DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="NUMERO AVISO" HeaderText="Aviso" DataFormatString="{0:d}">
															<HeaderStyle HorizontalAlign="Center" Width="155px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn visible="False" DataField="FECHA DE AVISO" HeaderText="Fecha Aviso" DataFormatString="{0:N}">
															<HeaderStyle HorizontalAlign="Center" Width="75px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_VENC"></asp:BoundColumn>
														<asp:BoundColumn DataField="Saldos" HeaderText="Saldos" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Abono">
															<ItemTemplate>
																<asp:ImageButton ID="imgVer5" runat="server" ImageUrl="Images/Ver.jpg" />
															</ItemTemplate>
															<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></ItemStyle>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lbTO_YENE" runat="server" CssClass="texto9pt">TOTAL YENES</asp:label><asp:datagrid id="dgTYENE" runat="server" CssClass="text" BorderStyle="None" BorderColor="Gainsboro"
													AutoGenerateColumns="False" Height="30px">
													<AlternatingItemStyle BorderColor="White" BackColor="Gainsboro"></AlternatingItemStyle>
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" CssClass="Texto8pt" BackColor="#003366"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="DEBE" HeaderText="Debe" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="HABER" HeaderText="Haber" DataFormatString="{0:#,##0.00}">
															<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="SALDO" HeaderText="Saldo" DataFormatString="{0:#,##0.00}">
															<ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD class="TEXT" vAlign="top" align="center">
												<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
													<TR>
														<TD width="80"><INPUT id=btnImprimir class=btn onclick="fn_Impresion('<%=ddTTMONE.SelectedItem.Text%>');" value=Imprimir type=button></TD>
													</TR>
												</TABLE>
												<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
										</TR>
										<TR>
											<TD class="TEXT" vAlign="top" align="center">
												<TABLE id="tblLeyenda" border="0" cellSpacing="4" cellPadding="0" width="40%">
													<TR>
														<TD class="td" colSpan="2">Leyenda</TD>
													</TR>
													<TR>
														<TD bgColor="#ffcc66" width="30%"></TD>
														<TD class="Leyenda" width="70%" align="center">Facturas Vencidas</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
