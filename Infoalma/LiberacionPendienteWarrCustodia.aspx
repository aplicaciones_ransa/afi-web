<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LiberacionPendienteWarrCustodia.aspx.vb" Inherits="LiberacionPendienteWarrCustodia" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Liberacion Pendiente Warrants Custodia</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript">	
			function ActivaBoton()
				{			
				frm=document.forms[0];				
				frm.btnEliminar.disabled=false;		
				}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
				<TR>
					<TD colSpan="2"><uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table9"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5"><uc1:menuinfo id="MenuInfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table4" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="text">Nro Warrant:</TD>
														<TD><asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
														<TD class="text">Nro Liberación:</TD>
														<TD><asp:textbox id="txtNroLiberacion" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center">
									<P><asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></P>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top"><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
										OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="15" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Sel">
												<ItemStyle HorizontalAlign="Center" Width="0px"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id="chST_SELE" runat="server" CssClass="Text"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_EMPR"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_UNID"></asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro.Warr">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_TIPDOC" HeaderText="Tip.Doc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_LIBE" HeaderText="Nro Retiro">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FCH_CREA" HeaderText="Fch.Creaci&#243;n">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n Mercader&#237;a">
												<ItemStyle HorizontalAlign="Left" Width="0px"></ItemStyle>
												<ItemTemplate>
													<asp:HyperLink id=hplDescipcion runat="server" CssClass="Text" ToolTip="Click para abrir PDF" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.NOM_PDF") %>' Target="_blank">
														<%# DataBinder.Eval(Container, "DataItem.DSC_MERC") %>
													</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ENTIFINA" HeaderText="Endosatario">
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ESTA" HeaderText="Estado">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_ENTFINA" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="co_alma"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="EMB_LIBE" HeaderText="EMB_LIBE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_CUST" HeaderText="FLG_CUST"></asp:BoundColumn>
											<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="IM_RETI" HeaderText="Monto" DataFormatString="{0:N2}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Anular">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:ImageButton style="Z-INDEX: 0" id="imgCancelar" onclick="imgCancelar_Click" runat="server" ToolTip="Anular Liberación Electrónica"
														ImageUrl="Images/anulado.gif"></asp:ImageButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><cc1:botonenviar id="btnEmitir" runat="server" CssClass="btn" Width="135px" Text="Emitir Ord.Retiro"
										TextoEnviando="Emitiendo..."></cc1:botonenviar></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
