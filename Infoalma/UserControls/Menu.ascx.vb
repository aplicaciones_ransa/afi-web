Imports System.Data
Public Class Menu
    Inherits System.Web.UI.UserControl

    Private objMenu As LibCapaNegocio.clsCNMenu

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents mnuPrincipal As skmMenu.Menu

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        'Put user code to initialize the page here
        If (Page.IsPostBack = False) Then
            If Request.IsAuthenticated Then
                ConstruirMenu()
            End If
        End If
    End Sub
    Public Sub ConstruirMenu()
        Dim i As Integer = 0
        Dim dtMenu As New DataTable
        Dim dr() As DataRow
        Dim submenu As skmMenu.MenuItem
        objMenu = New LibCapaNegocio.clsCNMenu
        objMenu.strCodGrupo = CStr(Session.Item("CoGrup"))
        objMenu.intCodModulo = CInt(Session.Item("CodModulo"))

        dtMenu = objMenu.gdtConseguirMenuxGrupo()

        While i < dtMenu.Rows.Count
            If dtMenu.Rows(i)("NI_MENU") = 1 Then
                submenu = New skmMenu.MenuItem(dtMenu.Rows(i)("NO_MENU"), dtMenu.Rows(i)("RU_MENU").ToString, dtMenu.Rows(i)("DE_MENU").ToString)
                If dtMenu.Rows(i)("PA_MENU") = 0 Then
                    Dim k As Integer = i + 1
                    Dim intPadre As Integer = dtMenu.Rows(i)("CO_MENU")
                    While (dtMenu.Rows(k)("NI_MENU") = 2) And (dtMenu.Rows(k)("PA_MENU") = intPadre)
                        submenu.SubItems.Add(New skmMenu.MenuItem(dtMenu.Rows(k)("NO_MENU"), dtMenu.Rows(k)("RU_MENU").ToString, dtMenu.Rows(k)("DE_MENU").ToString))
                        i = k
                        k += 1
                        If k >= dtMenu.Rows.Count Then Exit While
                    End While
                End If
                Me.mnuPrincipal.Items.Add(submenu)
            End If
            i += 1
        End While
        Me.mnuPrincipal.DataBind()
    End Sub



End Class
