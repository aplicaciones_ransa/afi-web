<%@ Control Language="vb" AutoEventWireup="false" CodeFile="Header.ascx.vb" Inherits="Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
<LINK rel="shortcut icon" href="Images/favicon.ico">
<meta name="vs_showGrid" content="True">
<script>
function Abrir(url, Height, Width)
			{			
				window.open(url,"Lgn","top=250,left=350,toolbar=0,status=0,directories=0,menubar=0,scrollbars=0,resize=0,width=" + Width + ",height=" + Height);
			}
			
		function ActualizaWFormPadre(){
		    document.getElementById('btnRefrescar').click();
	    }
			
		//	function IraURL(url,Target)
		//	{	
			//	if(Target == "_blank")
			//	{
			//	window.open(url,Target);
			//	}
				//if(Target == "_parent")
				//{
				//window.location="http://www.tutorialspoint.com";
				//}		
								
		//	}
			
				
</script>
<meta name="vs_defaultClientScript" content="JavaScript">
<table style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
	border="0" cellSpacing="0" cellPadding="0" width="100%">
	<TBODY>
		<tr>
			<td vAlign="top" background="Images/Header.jpg">
				<table id="Table2" border="0" cellSpacing="0" cellPadding="0" width="100%" height="70">
					<TBODY>
						<tr>
							<td vAlign="bottom" width="3%" align="left">&nbsp;</td>
							<TD vAlign="bottom" width="35%">
                                
                                <asp:datalist id="tabs" RepeatDirection="horizontal" CellPadding="0" CellSpacing="0" ItemStyle-CssClass="tab-inactive"
									SelectedItemStyle-CssClass="tab-active" runat="server">
									<SelectedItemStyle CssClass="tab-active"></SelectedItemStyle>
									<SelectedItemTemplate>
										<a class="MenuSelected" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
											<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
										</a>
									</SelectedItemTemplate>
									<ItemStyle CssClass="tab-inactive"></ItemStyle>
									<ItemTemplate>
										<a class="Menu" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
											<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
										</a>
									</ItemTemplate>
								</asp:datalist></TD>
							<TD vAlign="top" width="40%" align="right">
								<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="50%" height="80%">
									<TBODY>
										<TR>
											<TD align="center">
												<asp:hyperlink id="ImgHlkRegUsuarios" runat="server" Target="_blank" NavigateUrl="#" onclick="Javascript:Abrir('Manuales/REGISTRO DE USUARIOS.pdf', '550','800'); return false;"
													ToolTip="Registro de Usuarios">
												<img src="Images/RegUsuarios_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkRegUsuarios" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Manuales/REGISTRO DE USUARIOS.pdf', '550','800')"
														href="#">&nbsp;Reg.Usuarios&nbsp;</a>
												</asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkCondiServicio" runat="server" Target="_blank" CssClass="Header" NavigateUrl="Manuales/condiciones-generales-del-servicio_13012016.pdf"
													onclick="Javascript:Abrir('Manuales/condiciones-generales-del-servicio_13012016.pdf', '550','800'); return false;"
													ToolTip="Condiciones del servicio">
													<img src="Images/Condiciones_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkCondiServicio" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Manuales/condiciones-generales-del-servicio_13012016.pdf', '550','800')"
														href="#">&nbsp;Condiciones&nbsp;</a></asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkGlosario" runat="server" Target="_blank" NavigateUrl="#" onclick="Javascript:Abrir('Manuales/glosario.pdf', '550','800'); return false;"
													ToolTip="Glosario">
													<img src="Images/Glosario_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkGlosrio" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Manuales/glosario.pdf', '550','800')"
														href="#">&nbsp;Glosario&nbsp;</a></asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkFueraOficina" runat="server" Target="_blank" CssClass="Header" NavigateUrl="#"
													onclick="Javascript:Abrir('Popup/poputFueraOficina.aspx', '250','450'); return false;" ToolTip="Fuera de oficina">
													<img src="Images/FueraOficina_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkFueraOficina" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Popup/poputFueraOficina.aspx', '250','450')"
														href="#">&nbsp;Fuera&nbsp;de&nbsp;oficina&nbsp;</a></asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkVencimiento" runat="server" Target="_blank" NavigateUrl="#" onclick="Javascript:Abrir('Popup/Vencimientos.aspx', '550','800'); return false;"
													ToolTip="Vencimientos">
													<img src="Images/Vencimiento_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkVencimientos" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Popup/Vencimientos.aspx', '500','900')"
														href="#">&nbsp;Vencimientos&nbsp;</a>
												</asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkAyudaOnLine" runat="server" Target="_blank" NavigateUrl="#" onclick="Javascript:Abrir('Manuales/PREGUNTAS FRECUENTES INFODEPSA.pdf', '550','800'); return false;"
													ToolTip="Preguntas Frecuentes">
													<img src="Images/Ayuda_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkAyuda" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('Manuales/PREGUNTAS FRECUENTES INFODEPSA.pdf', '550','800')"
														href="#">&nbsp;Preg.Frecuentes&nbsp;</a>
												</asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="ImgHlkRegistrar" runat="server" Target="_blank" NavigateUrl="#" onclick="Javascript:Abrir('SendCertificado.aspx', '550','800'); return false;"
													ToolTip="Registrar">
													<img src="Images/Registrar_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkRegistrar" runat="server" Target="_blank" CssClass="Header">
													<a class="Header" onclick="Javascript:Abrir('SendCertificado.aspx', '500','900')" href="#">
														&nbsp;Registrar&nbsp;</a>
												</asp:hyperlink>
											</TD>
											<TD align="center">
												<asp:hyperlink id="hlkCerrarSesion" runat="server" Target="_parent" NavigateUrl="../Salir.aspx?caduco=2"
													ToolTip="Cerrar Sesi�n">
													<img src="Images/Cerrar_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
												<asp:hyperlink id="hlkLogOff" runat="server" Target="_parent" CssClass="Header" NavigateUrl="../Salir.aspx?caduco=2"></asp:hyperlink>
											</TD>
										</TR>
									</TBODY>
									<!--TD vAlign="top" align="right" height="0%">&nbsp;</TD><TD vAlign="bottom" align="right" height="100%"> --></TABLE>
							</TD>
						</tr>
					</TBODY>
				</table>
			</td>
		</tr>
		<TR>
			<TD class="tab-footer" height="25" vAlign="middle" align="center">
				<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
					<TR>
						<TD width="30%"><asp:label id="lblFecha" runat="server" CssClass="Text2"></asp:label></TD>
						<TD width="70%" align="right"><asp:label id="lblUsuario" runat="server" CssClass="Text2"></asp:label><asp:dropdownlist id="cboCliente" runat="server" CssClass="Text" Visible="False" AutoPostBack="True">
								<asp:ListItem Value="Basf Peruana">Basf Peruana s.a.</asp:ListItem>
								<asp:ListItem Value="Depsa">Alma Per�</asp:ListItem>
							</asp:dropdownlist></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TBODY>
</table>
</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE>
