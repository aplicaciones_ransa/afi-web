Imports System.Data
Imports AjaxControlToolkit

Public Class MenuInfo
    Inherits System.Web.UI.UserControl
    Dim objAcceso As Library.AccesoDB.Acceso = New Library.AccesoDB.Acceso
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents tabs As System.Web.UI.WebControls.DataList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Page.IsPostBack = False) Then
            CargarMenu()
        End If
    End Sub

    Private Sub CargarMenu()
        Dim tabItems As New ArrayList
        Dim intIndex As Integer
        Dim i As Integer = 0
        Dim j As Integer = -1
        Dim dtMenu As DataTable
        Dim dtNewMenu As New DataTable
        Dim strCodMenu As String

        If Session.Item("IdSico") <> Nothing Then
            dtMenu = Me.objAcceso.gGetMenuPrincipal(Session.Item("CoGrup"), Session.Item("CoSist"))
            dtNewMenu = dtMenu.Clone()
            strCodMenu = ""
            While i < dtMenu.Rows.Count
                If strCodMenu <> dtMenu.Rows(i)("CO_MODU") Then
                    j = j + 1
                    strCodMenu = dtMenu.Rows(i)("CO_MODU")
                End If
                If j = Session.Item("Page") Then
                    tabItems.Add(New Infodepsa.Infodepsa.TabItem(dtMenu.Rows(i)("NO_MENU"), dtMenu.Rows(i)("NO_URL")))
                    dtNewMenu.ImportRow(dtMenu.Rows(i))
                End If
                i += 1
            End While
        End If

        Me.dgMenu.DataSource = dtNewMenu
        Me.dgMenu.DataBind()

        If Not (Session.Item("Opcion") Is Nothing) Then
            intIndex = Convert.ToInt32(Session.Item("Opcion"))
        Else
            intIndex = 0
        End If
        tabs.DataSource = tabItems
        tabs.SelectedIndex = intIndex
        tabs.DataBind()
        If Not (Session.Item("Opcion") Is Nothing) Then
            intIndex = Convert.ToInt32(Session.Item("Opcion"))
        Else
            intIndex = 0
        End If
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    End Sub

    Private Sub dgMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgMenu.ItemDataBound
        Dim btnMenu As System.Web.UI.WebControls.Button
        'Dim hlkMenu As System.Web.UI.WebControls.LinkButton
        Dim strCodMenuSel, strCodSubMenuSel As String
        '=== Ocultando Header ===
        If e.Item.ItemType = ListItemType.Header Then
            e.Item.Visible = False
        End If

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            '=== Sessiones de seleccion ===
            strCodMenuSel = Session.Item("ssCodMenuSel")
            strCodSubMenuSel = Session.Item("ssCodSubMenuSel")
            btnMenu = CType(e.Item.FindControl("btnMenu"), System.Web.UI.WebControls.Button)
            'hlkMenu = CType(e.Item.FindControl("hlkMenu"), System.Web.UI.WebControls.LinkButton)
            '=== Pintando el primer tab de el primer ingreso ===
            If Session.Item("ssSecMenu") = Nothing And Session.Item("ssCodMenuSel") = Nothing Then
                e.Item.BackColor = System.Drawing.Color.FromArgb(254, 255, 177)
                btnMenu.BackColor = System.Drawing.Color.FromArgb(254, 255, 177)
                Session.Add("ssCodmenuDesple", ">")
            End If
            '=== Ocultando hijos no seleccioinados ===
            If InStr(e.Item.Cells(3).Text, "---") <> 0 Then
                e.Item.Cells(1).Text = Session.Item("ssSecMenu")
                e.Item.Visible = False
            Else
                Session.Add("ssSecMenu", e.Item.Cells(1).Text)
                e.Item.Cells(2).Text = "0"
            End If
            '=== Habilitando tabs padre e hijos seleccioinados ===
            If e.Item.Cells(3).Text = "RETIRO" Then
                Dim pausa As String = ""
            End If

            If e.Item.Cells(1).Text = strCodMenuSel Then
                e.Item.Visible = True
            End If

            '=== Pintando el tab seleccionado ===
            If e.Item.Cells(1).Text = strCodMenuSel And e.Item.Cells(2).Text = strCodSubMenuSel Then
                e.Item.BackColor = System.Drawing.Color.FromArgb(254, 255, 177)
                btnMenu.BackColor = System.Drawing.Color.FromArgb(254, 255, 177)
                e.Item.Visible = True
            End If
            Dim strvar As String = ""
            strvar = CType(e.Item.FindControl("btnMenu"), Button).OnClientClick

            '===== quitando los espacios en blanco del nombre del menu
            e.Item.Cells(3).Text = ((e.Item.Cells(3).Text).Replace(" &nbsp;", " ")).Trim
        End If
    End Sub

    Private Sub dgMenu_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgMenu.ItemCreated
    End Sub
End Class
