<%@ Control Language="vb" AutoEventWireup="false" CodeFile="HeaderLogin.ascx.vb" Inherits="HeaderLogin" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
<LINK rel="shortcut icon" href="Images/favicon.ico">
<TABLE id="Table1" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
	cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD vAlign="top" background="Images/Header.jpg">
			<TABLE id="Table2" height="70" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD vAlign="bottom" align="left" width="3%">&nbsp;</TD>
					<TD vAlign="bottom" width="45%"></TD>
					<TD vAlign="top" align="right" width="30%">
						<TABLE id="Table3" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top" align="right" height="40%">&nbsp;</TD>
							</TR>
							<TR>
								<TD height="20%">
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom" align="right" height="40%">
									<asp:HyperLink id="hlkLogOff" CssClass="Header" runat="server" NavigateUrl="../Salir.aspx?caduco=2"
										Target="_parent" BorderWidth="0px" Font-Size="9px"></asp:HyperLink></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<TR>
		<TD class="tab-footer" vAlign="middle" align="center" height="25">
			<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="30%">
						<asp:label id="lblFecha" runat="server" CssClass="Text2"></asp:label></TD>
					<TD align="right" width="70%">
						<asp:label id="lblUsuario" runat="server" CssClass="Text2"></asp:label></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
