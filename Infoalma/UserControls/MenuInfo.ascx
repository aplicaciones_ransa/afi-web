<%@ Control Language="vb" AutoEventWireup="false" CodeFile="MenuInfo.ascx.vb"  Inherits="MenuInfo" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script runat="server">
    Sub btnMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Protected Sub btnMenu_Click(sender As Object, e As EventArgs)
        Dim btnMenu As Button = CType(sender, Button)
        Dim strUrl As String
        Dim strCodMenuSel, strCodSubMenuSel, strCodmenuDesple As String
        Dim dgi As DataGridItem
        Dim j As Integer = 0
        dgi = CType(btnMenu.Parent.Parent, DataGridItem)
        strCodMenuSel = Session.Item("ssCodMenuSel")
        strCodSubMenuSel = Session.Item("ssCodSubMenuSel")
        strCodmenuDesple = Session.Item("ssCodSubMenuSel")

        If dgi.Cells(1).Text() <> strCodMenuSel And Session.Item("ssCodmenuDesple") = "V" Then
            Session.Add("ssCodmenuDesple", ">")
        End If
        Session.Add("ssCodMenuSel", dgi.Cells(1).Text())
        Session.Add("ssCodSubMenuSel", dgi.Cells(2).Text())
        If Session.Item("ssCodmenuDesple") = "V" Then
            Session.Add("ssCodmenuDesple", ">")
        Else
            Session.Add("ssCodmenuDesple", "V")
        End If

        strUrl = dgi.Cells(4).Text()
        Response.Redirect(strUrl)
    End Sub
    </script>
<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <TABLE ID="Table1" border="0" cellPadding="0" cellSpacing="0" width="100">
            <tr>
                <td valign="top">            
            <!-- otra lista -->		            
                    <asp:DataGrid ID="dgMenu" runat="server" AutoGenerateColumns="False" BorderColor="Gainsboro" BorderWidth="1px" CssClass="gv" PageSize="30">
                        <AlternatingItemStyle CssClass="gvRowAlter" />
                        <ItemStyle CssClass="gvRow" />
                        <Columns>
                            <asp:TemplateColumn visible="False">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgIcoMenu" runat="server" CausesValidation="False" ImageUrl="Images/Editar.png" OnClick="imgEditar_Click" />
                                    <%--<asp:Button ID="btnMenu" runat="server" BackColor="White" BorderStyle="None" class="btn" Font-Bold="True" ForeColor="#666666" Text='<%#  DataBinder.Eval(Container, "DataItem.NO_MENU") %>' Width="180px" OnClick="btnMenu_Click" style="TEXT-ALIGN: left" />--%><%--<asp:hyperlink id="hlkMenu" runat="server"  CssClass="Header" NavigateUrl='<%# "../" & DataBinder.Eval(Container, "DataItem.NO_URL") %>'>'<%# "../" & DataBinder.Eval(Container, "DataItem.NO_MENU") %>'</asp:hyperlink>--%><%--	<asp:hyperlink id="hlkMenu" runat="server" Target="_parent" NavigateUrl="../Salir.aspx?caduco=2"
													ToolTip="Cerrar Sesi�n">
													<asp:ima src="Images/Cerrar_Blanco.png" width="24" height="24" border="0"><BR>
												</asp:hyperlink>
                                                --%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CO_MENU" HeaderText="CO_MENU" visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CO_MENU" HeaderText="CO_SUBMENU" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="NO_MENU" HeaderText="" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="NO_URL" HeaderText="" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Button ID="btnMenu" runat="server" BackColor="White" BorderStyle="None" CausesValidation="False" class="btn" Font-Bold="True" ForeColor="#666666" OnClick="btnMenu_Click" style="TEXT-ALIGN: left" Text='<%#  DataBinder.Eval(Container, "DataItem.NO_MENU") %>' Width="180px" />
                                    <%--<asp:hyperlink id="hlkMenu" runat="server"  class="menuAyuda" Target="_parent" NavigateUrl='<%# "../" & DataBinder.Eval(Container, "DataItem.NO_URL") %>'> <%# DataBinder.Eval(Container, "DataItem.NO_MENU") %> </asp:hyperlink>--%><%--<asp:hyperlink id="hlkMenu" runat="server" Target="_parent" NavigateUrl="../Salir.aspx?caduco=2" ToolTip="Cerrar Sesi�n">											
										</asp:hyperlink>--%><%--<asp:ImageButton ID="imgIcoMenu" OnClick="imgEditar_Click" runat="server" CausesValidation="False"
                                                    ImageUrl="Images/Editar.png"></asp:ImageButton>--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle CssClass="gvPager" HorizontalAlign="Center" Mode="NumericPages" PageButtonCount="30" Position="TopAndBottom" />
                    </asp:DataGrid>
                </td>
            </tr>
        </TABLE>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="dgMenu" EventName="Unload" />
    </Triggers>
</asp:UpdatePanel>

	<asp:datalist id="tabs" runat="server" CellPadding="0" CellSpacing="0" ItemStyle-CssClass="tabAyuda-inactive"	SelectedItemStyle-CssClass="tabAyuda-active" Width="1%" Visible="False">
				<SelectedItemStyle CssClass="tabAyuda-active"></SelectedItemStyle>
				<SelectedItemTemplate>
					<a class="Ayuda" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
						<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
					</a>
				</SelectedItemTemplate>
				<ItemStyle CssClass="tabAyuda-inactive"></ItemStyle>
				<ItemTemplate    >	  
	<a class="menuAyuda" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
						<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
					</a>
                 <!-- sun menu -->
           <asp:datalist id="tabsub" runat="server" CellPadding="0" CellSpacing="0" ItemStyle-CssClass="tabAyuda-inactive"	SelectedItemStyle-CssClass="tabAyuda-active" Width="100%">
				<SelectedItemStyle CssClass="tabAyuda-active"></SelectedItemStyle>
				<SelectedItemTemplate>
					<a class="Ayuda" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
						<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
					</a>
				</SelectedItemTemplate>
				<ItemStyle CssClass="tabAyuda-inactive"></ItemStyle>
				<ItemTemplate>
					<a class="menuAyuda" href='<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Path %>'>
						<%# CType(Container.DataItem, Infodepsa.Infodepsa.TabItem).Name %>
					</a>   
     </ItemTemplate>
     </asp:DataList>
     </ItemTemplate>
     </asp:DataList>