<%@ Register TagPrefix="cc1" Namespace="skmMenu" Assembly="skmMenu" %>
<%@ Control Language="vb" AutoEventWireup="false" CodeFile="Menu.ascx.vb" Inherits="Menu" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table id="Table1" cellspacing="0" cellpadding="0" width="125" border="0">
    <tr>
        <td valign="top">
            <cc1:Menu ID="mnuPrincipal" runat="server" GridLines="Horizontal" HighlightTopMenu="False"
                ItemPadding="5" Cursor="Pointer" ItemSpacing="0" Width="100%">
                <UnselectedMenuItemStyle CssClass="tabAyuda-inactive"></UnselectedMenuItemStyle>
                <SelectedMenuItemStyle CssClass="tabAyuda-active"></SelectedMenuItemStyle>
            </cc1:Menu>
        </td>
    </tr>
</table>
