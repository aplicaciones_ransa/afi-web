Imports System.Web.Security
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class Header
    Inherits System.Web.UI.UserControl
    Dim objAcceso As Library.AccesoDB.Acceso = New Library.AccesoDB.Acceso
    'Protected WithEvents hlkAyuda As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkLogOff As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkRegistrar As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    'Protected WithEvents cboCliente As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents hlkGlosrio As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkFueraOficina As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkCondiServicio As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkFueraOficina As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkVencimiento As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkAyudaOnLine As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkGlosario As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkRegistrar As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkVencimientos As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkCerrarSesion As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkCondiServicio As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ImgHlkRegUsuarios As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents hlkRegUsuarios As System.Web.UI.WebControls.HyperLink
    Private objFunciones As Library.AccesoBL.Funciones

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents tabs As System.Web.UI.WebControls.DataList

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        If (Page.IsPostBack = False) Then
            Dim tabItems As New ArrayList
            Dim intIndex As Integer
            Dim i As Integer = 0
            Dim dtMenu As DataTable
            Dim strCodModulo As String
            If Request.IsAuthenticated Then
                Me.hlkLogOff.Visible = True
                Me.lblUsuario.Text = Session.Item("nom_user").ToString.ToUpper & " - "
                Me.hlkLogOff.Text = "&nbsp;Cerrar&nbsp;Sesi�n&nbsp;"
                Me.hlkAyuda.Visible = True
                Me.hlkCondiServicio.Visible = False
                Me.ImgHlkCondiServicio.Visible = False
                'If Session.Item("IdTipoUsuario") = "03" And objAcceso.gCertificadoRevocado(Session.Item("IdUsuario")) Then
                'Me.ImgHlkRegistrar.Visible = True
                'Me.hlkRegistrar.Visible = True
                'Me.hlkRegistrar.Text = "&nbsp;Reg&nbsp;Certificado&nbsp;"
                'Else
                Me.ImgHlkRegistrar.Visible = False
                Me.hlkRegistrar.Visible = False
                'End If

                If Session.Item("IdTipoEntidad") = "03" Then
                    Me.ImgHlkVencimiento.Visible = True
                    Me.hlkVencimientos.Visible = True
                Else
                    Me.ImgHlkVencimiento.Visible = False
                    Me.hlkVencimientos.Visible = False
                End If

                '' ======= Usamos el valida encuesta , para ver si es usuario depsa  ===========
                ''Dim dtFueraOfic As DataTable
                ''dtFueraOfic = objAcceso.gCADValidaEncuesta(Session.Item("IdUsuario"))
                ''If (dtFueraOfic.Rows(0)("ES_DEPSA")) > 0 Then ' si es de depsa, muestra fuera de oficina
                ''    Me.hlkFueraOficina.Visible = True
                ''Else
                ''    Me.hlkFueraOficina.Visible = False
                ''End If

                If Session.Item("IdTipoEntidad") = "01" And Session.Item("IdTipoUsuario") = "03" Then
                    Me.ImgHlkFueraOficina.Visible = True
                    Me.hlkFueraOficina.Visible = True
                Else
                    Me.ImgHlkFueraOficina.Visible = False
                    Me.hlkFueraOficina.Visible = False
                End If
                ''======================

                If Session.Item("IdTipoEntidad") = "03" Then
                    Me.ImgHlkVencimiento.Visible = True
                    Me.hlkVencimientos.Visible = True
                Else
                    Me.ImgHlkVencimiento.Visible = False
                    Me.hlkVencimientos.Visible = False
                End If

                If Session.Item("IdUsuario") <> Nothing Then
                    Me.cboCliente.Visible = True
                    objFunciones = New Library.AccesoBL.Funciones
                    objFunciones.GetloadEntidadUsr(Me.cboCliente, Session.Item("IdUsuario"))
                    Me.cboCliente.SelectedValue = Session.Item("Cliente")
                End If
                If Session.Item("IdSico") <> Nothing Then
                    dtMenu = Me.objAcceso.gGetMenuPrincipal(Session.Item("CoGrup"), Session.Item("CoSist"))
                    strCodModulo = ""
                    While i < dtMenu.Rows.Count
                        If strCodModulo <> dtMenu.Rows(i)("CO_MODU") Then
                            tabItems.Add(New Infodepsa.Infodepsa.TabItem(dtMenu.Rows(i)("NO_MODU"), dtMenu.Rows(i)("NO_URL")))
                            strCodModulo = dtMenu.Rows(i)("CO_MODU")
                        End If
                        i += 1
                    End While
                End If
            Else

                Me.hlkLogOff.Visible = False
                Me.hlkRegistrar.Visible = False
                Me.hlkAyuda.Visible = False
                Me.lblUsuario.Visible = False
                Me.hlkCondiServicio.Visible = False

                Me.hlkCerrarSesion.Visible = False
                Me.ImgHlkRegistrar.Visible = False
                Me.ImgHlkAyudaOnLine.Visible = False
                Me.ImgHlkFueraOficina.Visible = False
                Me.ImgHlkGlosario.Visible = False
                Me.ImgHlkVencimiento.Visible = False
                Me.ImgHlkCondiServicio.Visible = False
            End If

            tabs.DataSource = tabItems
            If Not (Session.Item("Page") Is Nothing) Then
                intIndex = Convert.ToInt32(Session.Item("Page"))
            Else
                intIndex = 0
            End If

            tabs.SelectedIndex = intIndex
            tabs.DataBind()

            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            Dim strNombreDia As String
            Dim count As Integer
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            strNombreDia = Split(Date.Now.ToLongDateString, ",").GetValue(0)
            Select Case strNombreDia.ToUpper
                Case "MONDAY"
                    strNombreDia = "Lunes"
                Case "LUNES"
                    strNombreDia = "Lunes"
                Case "TUESDAY"
                    strNombreDia = "Martes"
                Case "MARTES"
                    strNombreDia = "Martes"
                Case "WEDNESDAY"
                    strNombreDia = "Miercoles"
                Case "MIERCOLES"
                    strNombreDia = "Miercoles"
                Case "THURSDAY"
                    strNombreDia = "Jueves"
                Case "JUEVES"
                    strNombreDia = "Jueves"
                Case "FRIDAY"
                    strNombreDia = "Viernes"
                Case "VIERNES"
                    strNombreDia = "Viernes"
                Case "SATURDAY"
                    strNombreDia = "Sabado"
                Case "SABADO"
                    strNombreDia = "Sabado"
                Case "SUNDAY"
                    strNombreDia = "Domingo"
                Case "DOMINGO"
                    strNombreDia = "Domingo"
            End Select
            Select Case Mes.Substring(Mes.Length - 2)
                Case "01"
                    Mes = "Enero"
                Case "02"
                    Mes = "Febrero"
                Case "03"
                    Mes = "Marzo"
                Case "04"
                    Mes = "Abril"
                Case "05"
                    Mes = "Mayo"
                Case "06"
                    Mes = "Junio"
                Case "07"
                    Mes = "Julio"
                Case "08"
                    Mes = "Agosto"
                Case "09"
                    Mes = "Setiembre"
                Case "10"
                    Mes = "Octubre"
                Case "11"
                    Mes = "Noviembre"
                Case "12"
                    Mes = "Diciembre"
            End Select
            Me.lblFecha.Text = "&nbsp;" & strNombreDia & ", " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)
        End If
    End Sub

    Private Sub cboCliente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCliente.SelectedIndexChanged
        objAcceso = New Library.AccesoDB.Acceso
        Session.Item("Co_AlmaS") = ""
        Session.Item("CadenaBusquedaRetiroNuevo") = Nothing
        Dim dt As DataTable = New DataTable
        Try
            objAcceso.gInicioSesionEmpresa(Session.Item("IdUsuario"), Me.cboCliente.SelectedItem.Value)
            Session.Add("Cliente", Me.cboCliente.SelectedItem.Value)
            If objAcceso.blnEstado Then
                Session.Remove("CadenaBusquedaRetiro")
                Session.Remove("CadenaBusquedaTraders")
                Session.Remove("Co_AlmaW")
                Session.Remove("dtWMS")
                Session.Remove("dt")
                Session.Add("IdTipoUsuario", objAcceso.strIdTipoUsuario)
                Session.Add("IdSico", objAcceso.strIdSico)
                Session.Add("Pendiente", objAcceso.strPendiente)
                Session.Add("IdTipoEntidad", objAcceso.strIdTipoEntidad)
                Session.Add("NombreEntidad", objAcceso.strNomEntidad)
                Session.Add("CoSist", objAcceso.strCoSist)
                Session.Add("CoGrup", objAcceso.strCoGrup)
                Session.Add("CertDigital", objAcceso.blnCert)
                Session.Add("LibMultiple", objAcceso.blnLibMult)
                Session.Add("CodSAP", objAcceso.strCodSAP)
                Dim strIdPag As String = ""
                Dim strRutaPag As String = ""
                dt = objAcceso.gGetMenuxGrupo(objAcceso.strCoGrup, objAcceso.strCoSist)
                For i As Integer = 0 To dt.Rows.Count - 1
                    strIdPag = strIdPag & dt.Rows(i)("CO_MENU") & ","
                    strRutaPag = strRutaPag & dt.Rows(i)("NO_URL") & ","
                Next
                If strIdPag.Length < 1 Then
                    pr_IMPR_MENS("Su usuario no tiene asignado opciones en el sistema")
                    Exit Sub
                Else
                    strIdPag = strIdPag.Remove(strIdPag.Length - 1, 1)
                    Session.Add("PagId", strIdPag)
                    PoliticaCache()
                    'Response.Redirect(dt.Rows(0)("NO_URL"))
                    If Request.QueryString("sPagina") = "" Then
                        Dim strNoUrl As String = "Salir.aspx?caduco=1"
                        For j As Integer = 0 To dt.Rows.Count - 1
                            If dt.Rows(j)("NO_URL") <> "#" Then
                                strNoUrl = dt.Rows(j)("NO_URL")
                                Exit For
                            End If
                        Next
                        Response.Redirect(strNoUrl, False)
                        '======= fin  validar si es cliente  ===========
                    ElseIf InStr(strRutaPag, Request.QueryString("sPagina")) Then
                        Response.Redirect(Request.QueryString("sPagina"), False)
                    Else
                        Session.Add("strMensaje", "Usted no tiene acceso a la opci�n seleccionada")
                        Response.Redirect("Login.aspx", False)
                    End If
                    Exit Sub
                End If

            Else
                pr_IMPR_MENS("Su usuario no esta activo para la entidad seleccionada, consulte con el administrador")
            End If
        Catch ex As Exception
            pr_IMPR_MENS(ex.Message)
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub
End Class
