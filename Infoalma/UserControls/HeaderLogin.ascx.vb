Imports System.Data
Imports System.Web.Security
Public Class HeaderLogin
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkLogOff As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If (Page.IsPostBack = False) Then
            Dim tabItems As New ArrayList
            Dim intIndex As Integer
            Dim i As Integer = 0
            Dim dtMenu As DataTable
            Dim strCodModulo As String
            If Request.IsAuthenticated Then
                Me.hlkLogOff.Visible = True
                Me.lblUsuario.Text = Session.Item("nom_user").ToString.ToUpper
                Me.hlkLogOff.Text = " Cerrar Sesi�n "
            Else
                Me.hlkLogOff.Visible = False
                Me.lblUsuario.Visible = False
            End If
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            Dim strNombreDia As String
            Dim count As Integer
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            strNombreDia = Split(Date.Now.ToLongDateString, ",").GetValue(0)
            Select Case strNombreDia.ToUpper
                Case "MONDAY"
                    strNombreDia = "Lunes"
                Case "LUNES"
                    strNombreDia = "Lunes"
                Case "TUESDAY"
                    strNombreDia = "Martes"
                Case "MARTES"
                    strNombreDia = "Martes"
                Case "WEDNESDAY"
                    strNombreDia = "Miercoles"
                Case "MIERCOLES"
                    strNombreDia = "Miercoles"
                Case "THURSDAY"
                    strNombreDia = "Jueves"
                Case "JUEVES"
                    strNombreDia = "Jueves"
                Case "FRIDAY"
                    strNombreDia = "Viernes"
                Case "VIERNES"
                    strNombreDia = "Viernes"
                Case "SATURDAY"
                    strNombreDia = "Sabado"
                Case "SABADO"
                    strNombreDia = "Sabado"
                Case "SUNDAY"
                    strNombreDia = "Domingo"
                Case "DOMINGO"
                    strNombreDia = "Domingo"
            End Select
            Select Case Mes.Substring(Mes.Length - 2)
                Case "01"
                    Mes = "Enero"
                Case "02"
                    Mes = "Febrero"
                Case "03"
                    Mes = "Marzo"
                Case "04"
                    Mes = "Abril"
                Case "05"
                    Mes = "Mayo"
                Case "06"
                    Mes = "Junio"
                Case "07"
                    Mes = "Julio"
                Case "08"
                    Mes = "Agosto"
                Case "09"
                    Mes = "Setiembre"
                Case "10"
                    Mes = "Octubre"
                Case "11"
                    Mes = "Noviembre"
                Case "12"
                    Mes = "Diciembre"
            End Select
            Me.lblFecha.Text = "&nbsp;" & strNombreDia & ", " & Dia.Substring(Dia.Length - 2) & " de " & Mes & " del " & Anio.Substring(Anio.Length - 4)
        End If
    End Sub
End Class
