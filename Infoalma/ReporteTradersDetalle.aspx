<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ReporteTradersDetalle.aspx.vb" Inherits="ReporteTradersDetalle" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ReporteTradersDetalle</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="JavaScript">

        $(function () {
            $("#txtFechaIniRecep").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIniRecep").click(function () {
                $("#txtFechaIniRecep").datepicker('show');
            });

            $("#txtFechaFinRecep").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinRecep").click(function () {
                $("#txtFechaFinRecep").datepicker('show');
            });
        });

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }

        function Todos(ckall, citem) {
            var actVar = ckall.checked;
            for (i = 0; i < Form1.length; i++) {
                if (Form1.elements[i].type == "checkbox") {
                    //si los check estan habilitados
                    if (Form1.elements[i].disabled == false) {
                        //Checker all 
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
        }

    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table9"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="6">
                                <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%" align="center">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px" background="Images/table_r2_c1.gif" width="6"></td>
                                        <td style="height: 19px">
                                            <table style="z-index: 0" id="Table2" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="Text" align="left">N�mero Operaci�n</td>
                                                    <td style="width: 1%" class="Text">:</td>
                                                    <td colspan="4" align="left">
                                                        <asp:Label ID="lblTrader" runat="server" CssClass="Text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Cliente</td>
                                                    <td style="width: 1%" class="Text">:</td>
                                                    <td colspan="4" align="left">
                                                        <asp:DropDownList ID="cboCliente" runat="server" CssClass="Text" Enabled="False" AutoPostBack="True"
                                                            Width="500px">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Almac�n</td>
                                                    <td style="width: 1%" class="Text">:</td>
                                                    <td colspan="4" align="left">
                                                        <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" Enabled="False" AutoPostBack="True"
                                                            Width="500px">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Conductor</td>
                                                    <td class="Text">:</td>
                                                    <td colspan="4" align="left">
                                                        <asp:Label ID="lblConductor" runat="server" CssClass="Text" Width="500px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Empresa Transporte</td>
                                                    <td class="Text">:</td>
                                                    <td colspan="4" align="left">
                                                        <asp:TextBox ID="txtEmprTransporte" runat="server" CssClass="Text" Enabled="False" Width="500px"
                                                            MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" ControlToValidate="txtEmprTransporte"
                                                                ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Transporte por</td>
                                                    <td class="Text">:</td>
                                                    <td align="left">
                                                        <asp:DropDownList Style="z-index: 0" ID="cboTransportePor" runat="server" CssClass="Text" Enabled="False" Width="150px">
                                                            <asp:ListItem Value="0">--- SELECCIONE ---</asp:ListItem>
                                                            <asp:ListItem Value="CLI">CLIENTE</asp:ListItem>
                                                            <asp:ListItem Value="CON">CONDUCTOR</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" Width="11px" ControlToValidate="cboTransportePor"
                                                            ErrorMessage="*" Font-Size="Large"></asp:RequiredFieldValidator>
                                                      
                                                        </td>
                                                    <td class="Text" align="left">Usuario de Cierre</td>
                                                    <td style="width: 1%" align="left">:</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblUsuariCierre" runat="server" CssClass="Text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Mercader�a</td>
                                                    <td class="Text">:</td>
                                                    <td align="left">
                                                        &nbsp;<asp:TextBox ID="txtMercaderia" runat="server" CssClass="Text" Enabled="False" Width="250px"
                                                            MaxLength="200"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtMercaderia" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                                    <td class="Text" align="left">Vapor</td>
                                                    <td align="left">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtVapor" runat="server" CssClass="Text" Enabled="False" Width="100px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="txtVapor" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Cantidad Solicitada</td>
                                                    <td class="Text">:</td>
                                                    <td align="left">
                                                        <input id="txtCantSolicitada" class="Text" disabled onkeypress="javascript:return ValidNum(event);"
                                                            onkeyup="Formato(this,event);" size="15" name="txtCantSolicitada" runat="server"><asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtCantSolicitada"
                                                            ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                                    <td class="Text" align="left">Dua</td>
                                                    <td align="left">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtDua" runat="server" CssClass="Text" Enabled="False" Width="100px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtDua" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Fecha&nbsp;Inicio&nbsp;Ingreso</td>
                                                    <td class="Text">:</td>
                                                    <td class="Text" align="left">&nbsp;<input id="txtFechaIniRecep" class="Text" disabled onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                        maxlength="10" size="8" name="txtFechaIniRecep" runat="server">
                                                        <input id="btnFechaIniRecep" class="Text" 
                                                            value="..." type="button" name="btnFechaIniRecep" runat="server">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" Width="1px" ControlToValidate="txtFechaIniRecep"
                                                            ErrorMessage="*" Font-Size="Large"></asp:RequiredFieldValidator></td>
                                                    <td class="Text" align="left">Fecha&nbsp;Termino&nbsp;Ingreso</td>
                                                    <td class="Text" align="left">:</td>
                                                    <td class="Text" align="left">
                                                        <input id="txtFechaFinRecep" class="Text" disabled onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="8" name="txtFechaFinRecep" runat="server">
                                                        <input id="btnFechaFinRecep" class="Text" 
                                                            value="..." type="button" name="btnFechaFinRecep" runat="server">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" Width="1px" ControlToValidate="txtFechaFinRecep"
                                                            ErrorMessage="*" Font-Size="Large"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Unidad de Medida</td>
                                                    <td class="Text">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:DropDownList ID="cboUnidadMedida" runat="server" CssClass="Text" Enabled="False" Width="150px"></asp:DropDownList><asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" Width="11px" ControlToValidate="cboUnidadMedida"
                                                            ErrorMessage="*" Font-Size="Large"></asp:RequiredFieldValidator></td>
                                                    <td class="Text" align="left">Estado</td>
                                                    <td class="Text" align="left">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:Label ID="lblEstado" runat="server" CssClass="Text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">N�mero DCR</td>
                                                    <td class="Text">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:TextBox ID="txtDCR" runat="server" CssClass="Text" Enabled="False" Width="100px" MaxLength="100"></asp:TextBox></td>
                                                    <td class="Text" align="left">N�mero Warrant</td>
                                                    <td class="Text" align="left">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:TextBox ID="txtWarrant" runat="server" CssClass="Text" Enabled="False" Width="100px" MaxLength="100"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" align="left">Acumulado Cliente</td>
                                                    <td class="Text">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:Label ID="lblAcuCli" runat="server" CssClass="Text"></asp:Label></td>
                                                    <td class="Text" align="left">Acumulado Conductor</td>
                                                    <td class="Text" align="left">:</td>
                                                    <td class="Text" align="left">
                                                        <asp:Label ID="lblAcuCon" runat="server" CssClass="Text"></asp:Label></td>
                                                </tr>
                                                </table>
                                            &nbsp;
                                        </td>
                                        <td style="height: 19px" background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center">
                                <asp:Button ID="btnExportar" runat="server" CssClass="Text" Width="80px" Text="Exportar"></asp:Button><asp:Button ID="btnRegresar" runat="server" CssClass="Text" Width="80px" Text="Regresar" CausesValidation="False"></asp:Button>
                                <asp:Button Style="z-index: 0" ID="btnCerrarMarcados" runat="server" CssClass="Text" Width="91px"
                                    Text="Cerrar Marcados" CausesValidation="False"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" valign="top" align="left">Resultado:</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="gvTraders" runat="server" CssClass="gv" Width="100%" BorderWidth="1px" PageSize="30"
                                    AutoGenerateColumns="False" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="NU_SECU" HeaderText="Sec">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SECU" HeaderText="Sec para Anular">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_ING_MERC" HeaderText="Fecha Ingreso">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_SALI_MERC" HeaderText="Fecha Salida">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TEMP" HeaderText="Temperatura" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_HUME" HeaderText="Humedad" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_PLAC" HeaderText="Placa">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_DOCU_CLIE" HeaderText="Numero Gu&#237;a">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TICK" HeaderText="Ticket Conductor">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_INGR_CLIE" HeaderText="Ingreso Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SALI_CLIE" HeaderText="Salida Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_INGR_COND" HeaderText="Ingreso Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_SALI_COND" HeaderText="Salida Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_ACU_CLIE" HeaderText="Acumulado Cliente" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_ACU_COND" HeaderText="Acumulado Conductor" DataFormatString="{0:#,##0.000}">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_DOCU_RETI" HeaderText="DOR">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_USUA_MODI" HeaderText="Fecha Modif.">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_USUA_CREA" HeaderText="Ult. Usuario Modif.">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_ESTA_ANUL" HeaderText="Anulado">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_ESTA_FINA" HeaderText="Finalizado">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR Generado">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Cerrado">
                                            <HeaderTemplate>
                                                Cerrado
													<asp:CheckBox ID="chkHCerrar" runat="server" CssClass="Text"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table id="Table18" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:CheckBox ID="chkCerrar" runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.CO_ESTA_FINA") %>'></asp:CheckBox></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table style="width: 376px; height: 37px" id="Table4" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td style="height: 16px" class="td" colspan="8">Leyenda</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="gainsboro" width="30%"></td>
                                        <td class="Leyenda" width="20%">Anulado.</td>
                                        <td bgcolor="#c8dcf0" width="30%"></td>
                                        <td class="Leyenda" width="30%">Finalizado.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
