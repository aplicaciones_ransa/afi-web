<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AdmParametro.aspx.vb" Inherits="AdmParametro" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AdmParametro</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:header id="Header2" runat="server"></uc1:header>
						<TABLE id="Table3" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%">
												<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="3">
												<uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table7" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="text" width="10%">Dominio :</TD>
																	<TD width="15%">
																		<asp:dropdownlist id="cboDominio" runat="server" CssClass="text" AutoPostBack="True"></asp:dropdownlist></TD>
																	<TD width="5%">
																		<asp:button id="btnNuevoDom" runat="server" CssClass="btn" Text="Nuevo Dom."></asp:button></TD>
																	<TD width="5%">
																		<asp:button id="btnEditar" runat="server" CssClass="btn" Text="Editar Dom."></asp:button></TD>
																	<TD width="5%">
																		<asp:button id="btnEliminar" runat="server" CssClass="btn" Text="Eliminar Dom."></asp:button></TD>
																	<TD width="10%">
																		<asp:button id="btnNuevoPar" runat="server" CssClass="btn" Text="Nuevo"></asp:button></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top"></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
													AutoGenerateColumns="False">
													<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
													<ItemStyle CssClass="gvRow"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="CO_DOMIN" HeaderText="CO_DOMIN"></asp:BoundColumn>
														<asp:BoundColumn DataField="CO_PAR" HeaderText="Nro">
															<ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="NO_PAR" HeaderText="Descripci&#243;n Corta">
															<ItemStyle HorizontalAlign="Left" VerticalAlign="Bottom"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DE_LARG_PAR" HeaderText="Descripci&#243;n Larga">
															<ItemStyle HorizontalAlign="Left" VerticalAlign="Bottom"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Editar">
															<HeaderStyle Width="40px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<TABLE id="Table11" cellSpacing="0" cellPadding="0" width="30" border="0">
																	<TR>
																		<TD align="center">
																			<asp:ImageButton id="imgEditar" onclick="imgeditar_click" runat="server" CausesValidation="False"
																				ImageUrl="Images/Editar.gif" ToolTip="Editar Registro"></asp:ImageButton></TD>
																	</TR>
																</TABLE>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Anular">
															<HeaderStyle Width="40px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<TABLE id="Table21" cellSpacing="0" cellPadding="0" width="30" border="0">
																	<TR>
																		<TD align="center">
																			<asp:ImageButton id="imgEliminar" onclick="imgEliminar_click" runat="server" CausesValidation="False"
																				ImageUrl="Images/anulado.gif" ToolTip="Eliminar Registro"></asp:ImageButton></TD>
																	</TR>
																</TABLE>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle CssClass="gvPager"></PagerStyle>
												</asp:datagrid>
												<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
