Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class ActualizarDatos
    Inherits System.Web.UI.Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents rbtNatural As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rbtJuridico As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rbtExtranjero As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents txtRazon As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSigla As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSBS As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtPaterno As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtMaterno As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNombres As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtRuc2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtRuc1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtWeb As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgdDirecciones As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgdContactos As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnAddDirec As System.Web.UI.WebControls.Button
    'Protected WithEvents btnAddContac As System.Web.UI.WebControls.Button
    'Protected WithEvents btnActualizar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblLeyenda As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private objEntidad As clsCNEntidad
    Private objAccesoWeb As clsCNAccesosWeb

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "ACTUALIZAR_DATOS") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "ACTUALIZAR_DATOS"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                CargarDatos()
            End If
            Me.lblOpcion.Text = "Datos de Empresa"
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarDatos()
        Try
            objEntidad = New clsCNEntidad
            dtEmpresa = New DataTable
            dtContactos = New DataTable
            dtDirecciones = New DataTable
            With objEntidad
                dtEmpresa = .gCNGetDataEmpresa(Session.Item("IdSico"))
                If dtEmpresa.Rows.Count = 0 Then
                    Me.lblMensaje.Text = "No se pudo cargar la informaci�n de la empresa"
                    Exit Sub
                End If
                Select Case dtEmpresa.Rows(0)("TI_CLIE")
                    Case "NAT"
                        Me.rbtNatural.Checked = True
                    Case "JUR"
                        Me.rbtJuridico.Checked = True
                    Case "EXT"
                        Me.rbtExtranjero.Checked = True
                End Select
                Me.txtMaterno.Text = dtEmpresa.Rows(0)("AP_MATE_CLIE")
                Me.txtNombres.Text = dtEmpresa.Rows(0)("NO_CLIE")
                Me.txtPaterno.Text = dtEmpresa.Rows(0)("AP_PATE_CLIE")
                Me.txtRazon.Text = dtEmpresa.Rows(0)("NO_RAZO_SOCI")
                Me.txtRuc1.Text = dtEmpresa.Rows(0)("NU_RUCS_0001")
                Me.txtRuc2.Text = dtEmpresa.Rows(0)("NU_RUCS_0002")
                Me.txtSBS.Text = dtEmpresa.Rows(0)("CO_SBSS")
                Me.txtSigla.Text = dtEmpresa.Rows(0)("DE_SIGL")
                Me.txtWeb.Text = dtEmpresa.Rows(0)("DI_PWEB")
                dtDirecciones = .gCNGetDataDirecciones(Session.Item("IdSico"))
                Me.dgdDirecciones.DataSource = dtDirecciones
                Me.dgdDirecciones.DataBind()
                dtContactos = .gCNGetDataContactos(Session.Item("IdSico"))
                Me.dgdContactos.DataSource = dtContactos
                Me.dgdContactos.DataBind()
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Function GetTipoDireccion() As DataTable
        objEntidad = New clsCNEntidad
        Return objEntidad.gCNGetTipoDireccion
    End Function

    Public Function GetPais() As DataTable
        objEntidad = New clsCNEntidad
        Return objEntidad.gCNGetPais
    End Function

    Public Function GetTipoContacto() As DataTable
        objEntidad = New clsCNEntidad
        Return objEntidad.gCNGetTipoContacto
    End Function

    Public Function GetTipoDoc() As DataTable
        objEntidad = New clsCNEntidad
        Return objEntidad.gCNGetTipoDoc
    End Function

    Private Property dtEmpresa() As DataTable
        Get
            Return (ViewState("dtEmpresa"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtEmpresa") = Value
        End Set
    End Property

    Private Property dtContactos() As DataTable
        Get
            Return (ViewState("dtContactos"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtContactos") = Value
        End Set
    End Property

    Private Property dtDirecciones() As DataTable
        Get
            Return (ViewState("dtDirecciones"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtDirecciones") = Value
        End Set
    End Property

    Private Sub btnAddDirec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDirec.Click
        Dim dr As DataRow
        CargarDirec()
        dr = dtDirecciones.NewRow
        dr(0) = Me.dgdDirecciones.Items.Count + 1
        dr(1) = "COB"
        dr(2) = ""
        dr(3) = "051"
        dr(4) = ""
        dr(5) = ""
        dr(6) = ""
        dtDirecciones.Rows.Add(dr)
        Me.dgdDirecciones.DataSource = dtDirecciones
        Me.dgdDirecciones.DataBind()
    End Sub

    Private Sub btnAddContac_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddContac.Click
        Dim dr As DataRow
        CargarContac()
        dr = dtContactos.NewRow
        dr(0) = Me.dgdContactos.Items.Count + 1
        dr(1) = "000"
        dr(2) = ""
        dr(3) = ""
        dr(4) = ""
        dr(5) = ""
        dr(6) = ""
        dr(7) = ""
        dr(8) = ""
        dr(9) = "0"
        dtContactos.Rows.Add(dr)
        Me.dgdContactos.DataSource = dtContactos
        Me.dgdContactos.DataBind()
    End Sub

    Private Sub CargarContac()
        Dim dr As DataRow
        Dim dgItem As DataGridItem
        dtContactos.Rows.Clear()
        For i As Integer = 0 To dgdContactos.Items.Count - 1
            dgItem = dgdContactos.Items(i)
            dr = dtContactos.NewRow
            dr(0) = dgItem.Cells(0).Text()
            dr(1) = CType(dgItem.FindControl("cboTipoContacto"), DropDownList).SelectedValue
            dr(2) = CType(dgItem.FindControl("txtNombre"), TextBox).Text
            dr(3) = CType(dgItem.FindControl("txtTelefono"), TextBox).Text
            dr(4) = CType(dgItem.FindControl("txtAnexo"), TextBox).Text
            dr(5) = CType(dgItem.FindControl("txtFax"), TextBox).Text
            dr(6) = CType(dgItem.FindControl("txtEmail"), TextBox).Text
            dr(7) = CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedValue
            dr(8) = CType(dgItem.FindControl("txtNroDoc"), TextBox).Text
            dr(9) = CType(dgItem.FindControl("checFaEl"), CheckBox).Checked
            dtContactos.Rows.Add(dr)
        Next i
    End Sub

    Private Sub CargarDirec()
        Dim dr As DataRow
        Dim dgItem As DataGridItem
        dtDirecciones.Rows.Clear()
        For i As Integer = 0 To dgdDirecciones.Items.Count - 1
            dgItem = dgdDirecciones.Items(i)
            dr = dtDirecciones.NewRow
            dr(0) = dgItem.Cells(0).Text()
            dr(1) = CType(dgItem.FindControl("cboTipoDireccion"), DropDownList).SelectedValue
            dr(2) = CType(dgItem.FindControl("txtDireccion"), TextBox).Text
            dr(3) = CType(dgItem.FindControl("cboPais"), DropDownList).SelectedValue
            dr(4) = CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text
            dr(5) = CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text
            dr(6) = CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text
            dtDirecciones.Rows.Add(dr)
        Next i
    End Sub

    Public Sub imgEliminarContac_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            CargarContac()
            dtContactos.Rows.RemoveAt(dgi.ItemIndex)
            For Each dr1 As DataRow In dtContactos.Rows
                dtContactos.Rows(j).AcceptChanges()
                dtContactos.Rows(j).BeginEdit()
                dtContactos.Rows(j).Item(0) = j + 1
                dtContactos.Rows(j).EndEdit()
                j += 1
            Next
            Me.dgdContactos.DataSource = dtContactos
            Me.dgdContactos.DataBind()
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgEliminarDirec_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            CargarDirec()
            dtDirecciones.Rows.RemoveAt(dgi.ItemIndex)
            For Each dr1 As DataRow In dtDirecciones.Rows
                dtDirecciones.Rows(j).AcceptChanges()
                dtDirecciones.Rows(j).BeginEdit()
                dtDirecciones.Rows(j).Item(0) = j + 1
                dtDirecciones.Rows(j).EndEdit()
                j += 1
            Next
            Me.dgdDirecciones.DataSource = dtDirecciones
            Me.dgdDirecciones.DataBind()
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Dim objFuncion As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
            Dim strDe As String = System.Configuration.ConfigurationManager.AppSettings("EmailDepsa")
            Dim strPara As String = System.Configuration.ConfigurationManager.AppSettings("CorreoComercial")
            objAccesoWeb = New clsCNAccesosWeb
            Me.lblMensaje.Text = objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1),
                Session.Item("IdSico"), Session.Item("NombreEntidad"), "P", "ACTUALIZAR_DATOS", "ACTUALIZAR DATOS EMPRESA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            objFuncion.SendMail(strDe, strPara, "Actualizaci�n de datos: " & Session.Item("NombreEntidad") & " / " & "Usuario: " & Session.Item("nom_user"), Detalle, "")
            'objFuncion.SendMail(strDe, "fmalaver@depsa.com.pe", "Actualizaci�n de datos: " & Session.Item("NombreEntidad") & " / " & "Usuario: " & Session.Item("nom_user"), Detalle, "")
            Response.Write("<script language='javascript'>alert('Su solicitud de actualizaci�n de datos sera atendida en los proximos minutos');</script>")
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Function Detalle() As String
        Dim strBody As String
        Dim strCampo As String
        objEntidad = New clsCNEntidad
        Dim dtContactos1 As DataTable
        Dim dtDirecciones1 As DataTable
        Dim dgItem As DataGridItem
        Try
            strBody = "<TABLE id='Table4' cellPadding='0' width='800px' border='1' style='FONT-SIZE: 11px; COLOR: #003399; FONT-FAMILY: Arial, Tahoma'><TR>"

            With objEntidad
                dtDirecciones1 = .gCNGetDataDirecciones(Session.Item("IdSico"))
                dtContactos1 = .gCNGetDataContactos(Session.Item("IdSico"))
            End With
            If Me.rbtExtranjero.Checked = True Then
                strCampo = "EXT"
            ElseIf Me.rbtJuridico.Checked = True Then
                strCampo = "JUR"
            ElseIf Me.rbtNatural.Checked = True Then
                strCampo = "NAT"
            End If
            strBody += IIf(dtEmpresa.Rows(0)("TI_CLIE") <> strCampo, "<TD bgColor='#ffcc66' colSpan=8>Tipo Persona: " & strCampo & "</TD></TR>", "<TD colSpan=8>Tipo Persona: " & strCampo & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("AP_PATE_CLIE") <> Me.txtPaterno.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Ap. Paterno: " & Me.txtPaterno.Text & "</TD></TR>", "<TR><TD colSpan=8>Ap. Paterno: " & Me.txtPaterno.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("AP_MATE_CLIE") <> Me.txtMaterno.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Ap. Materno: " & Me.txtMaterno.Text & "</TD></TR>", "<TR><TD colSpan=8>Ap. Materno: " & Me.txtMaterno.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("NO_CLIE") <> Me.txtNombres.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Nombres: " & Me.txtNombres.Text & "</TD></TR>", "<TR><TD colSpan=8>Nombres: " & Me.txtNombres.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("NO_RAZO_SOCI") <> Me.txtRazon.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Raz�n Social: " & Me.txtRazon.Text & "</TD></TR>", "<TR><TD colSpan=8>Raz�n Social: " & Me.txtRazon.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("NU_RUCS_0001") <> Me.txtRuc1.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>RUC 1: " & Me.txtRuc1.Text & "</TD></TR>", "<TR><TD colSpan=8>RUC 1: " & Me.txtRuc1.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("NU_RUCS_0002") <> Me.txtRuc2.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>RUC 2: " & Me.txtRuc2.Text & "</TD></TR>", "<TR><TD colSpan=8>RUC 2: " & Me.txtRuc2.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("DE_SIGL") <> Me.txtSigla.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Sigla Comercial: " & Me.txtSigla.Text & "</TD></TR>", "<TR><TD colSpan=8>Sigla Comercial: " & Me.txtSigla.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("CO_SBSS") <> Me.txtSBS.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>C�digo SBS: " & Me.txtSBS.Text & "</TD></TR>", "<TR><TD colSpan=8>C�digo SBS: " & Me.txtSBS.Text & "</TD></TR>")
            strBody += IIf(dtEmpresa.Rows(0)("DI_PWEB") <> Me.txtWeb.Text, "<TR><TD bgColor='#ffcc66' colSpan=8>Web site: " & Me.txtWeb.Text & "</TD></TR>", "<TR><TD colSpan=8>Web site: " & Me.txtWeb.Text & "</TD></TR></TABLE>")

            strBody += "<TABLE id='Table4' cellPadding='0' width='800px' border='1' style='FONT-SIZE: 11px; COLOR: #003399; FONT-FAMILY: Arial, Tahoma'><TR><TD colSpan=8>Direcciones</TD></TR>"
            strBody += "<TR bgColor='gainsboro' style='FONT-WEIGHT: bold'><TD>TipoDirec</TD><TD colSpan=3>Direcci�n</TD><TD>Pa�s</TD><TD>Tel�fono</TD><TD>Anexo</TD><TD>Fax</TD></TR>"
            For j As Integer = 0 To Me.dgdDirecciones.Items.Count - 1
                dgItem = Me.dgdDirecciones.Items(j)
                If dtDirecciones1.Rows.Count >= j + 1 Then
                    strBody += IIf(CType(dgItem.FindControl("cboTipoDireccion"), DropDownList).SelectedValue <> dtDirecciones1.Rows(j)("TI_DIRE"), "<TR><TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboTipoDireccion"), DropDownList).SelectedItem.Text & "</TD>", "<TR><TD>" & CType(dgItem.FindControl("cboTipoDireccion"), DropDownList).SelectedItem.Text & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtDireccion"), TextBox).Text <> dtDirecciones1.Rows(j)("DE_DIRE"), "<TD bgColor='#ffcc66' colSpan=3>" & IIf(CType(dgItem.FindControl("txtDireccion"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtDireccion"), TextBox).Text) & "</TD>", "<TD colSpan=3>" & IIf(CType(dgItem.FindControl("txtDireccion"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtDireccion"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("cboPais"), DropDownList).SelectedValue <> dtDirecciones1.Rows(j)("CO_PAIS"), "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboPais"), DropDownList).SelectedItem.Text & "</TD>", "<TD>" & CType(dgItem.FindControl("cboPais"), DropDownList).SelectedItem.Text & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text <> dtDirecciones1.Rows(j)("NU_TELE_0001"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text <> dtDirecciones1.Rows(j)("NU_ANEX"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text <> dtDirecciones1.Rows(j)("NU_FAXE"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text) & "</TD></TR>", "<TD>" & IIf(CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text) & "</TD></TR>")
                Else
                    strBody += "<TR><TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboTipoDireccion"), DropDownList).SelectedItem.Text & "</TD>"
                    strBody += "<TD bgColor='#ffcc66' colSpan=3>" & IIf(CType(dgItem.FindControl("txtDireccion"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtDireccion"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboPais"), DropDownList).SelectedItem.Text & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefonoDirec"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexoDirec"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFaxDirec"), TextBox).Text) & "</TD></TR>"
                End If
            Next
            strBody += "</TABLE>"
            strBody += "<TABLE id='Table4' cellPadding='0' width='800px' border='1' style='FONT-SIZE: 11px; COLOR: #003399; FONT-FAMILY: Arial, Tahoma'><TR><TD colSpan=9>Contactos</TD></TR>"
            strBody += "<TR bgColor='gainsboro' style='FONT-WEIGHT: bold'><TD>TipoContac</TD><TD>Nombre</TD><TD>Tel�fono</TD><TD>Anexo</TD><TD>Fax</TD><TD>E-mail</TD><TD>TipoDoc</TD><TD>NroDoc</TD><TD>F.E.</TD></TR>"

            For j As Integer = 0 To Me.dgdContactos.Items.Count - 1
                dgItem = Me.dgdContactos.Items(j)
                If dtContactos1.Rows.Count >= j + 1 Then
                    strBody += IIf(CType(dgItem.FindControl("cboTipoContacto"), DropDownList).SelectedValue <> dtContactos1.Rows(j)("CO_TIPO_CONT"), "<TR><TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboTipoContacto"), DropDownList).SelectedItem.Text & "</TD>", "<TR><TD>" & CType(dgItem.FindControl("cboTipoContacto"), DropDownList).SelectedItem.Text & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtNombre"), TextBox).Text <> dtContactos1.Rows(j)("NO_CONT"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtNombre"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNombre"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtNombre"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNombre"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtTelefono"), TextBox).Text <> dtContactos1.Rows(j)("NU_TELE_0001"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtTelefono"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefono"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtTelefono"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefono"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtAnexo"), TextBox).Text <> dtContactos1.Rows(j)("NU_ANEX"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtAnexo"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexo"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtAnexo"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexo"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtFax"), TextBox).Text <> dtContactos1.Rows(j)("NU_FAXE"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtFax"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFax"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtFax"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFax"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtEmail"), TextBox).Text <> dtContactos1.Rows(j)("DI_EMAI"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtEmail"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtEmail"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtEmail"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtEmail"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedValue <> dtContactos1.Rows(j)("TI_DOCU_IDEN"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text = "", "&nbsp;", CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text = "", "&nbsp;", CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("txtNroDoc"), TextBox).Text <> dtContactos1.Rows(j)("NU_DOCU_IDEN"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtNroDoc"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNroDoc"), TextBox).Text) & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("txtNroDoc"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNroDoc"), TextBox).Text) & "</TD>")
                    strBody += IIf(CType(dgItem.FindControl("checFaEl"), CheckBox).Checked <> dtContactos1.Rows(j)("ST_ENVI_FAEL"), "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("checFaEl"), CheckBox).Checked = "0", "N", "S") & "</TD>", "<TD>" & IIf(CType(dgItem.FindControl("checFaEl"), CheckBox).Checked = "True", "S", "N") & "</TD></TR>")
                Else
                    strBody += "<TR><TD bgColor='#ffcc66'>" & CType(dgItem.FindControl("cboTipoContacto"), DropDownList).SelectedItem.Text & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtNombre"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNombre"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtTelefono"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtTelefono"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtAnexo"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtAnexo"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtFax"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtFax"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtEmail"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtEmail"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text = "", "&nbsp;", CType(dgItem.FindControl("cboTipoDoc"), DropDownList).SelectedItem.Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("txtNroDoc"), TextBox).Text = "", "&nbsp;", CType(dgItem.FindControl("txtNroDoc"), TextBox).Text) & "</TD>"
                    strBody += "<TD bgColor='#ffcc66'>" & IIf(CType(dgItem.FindControl("checFaEl"), CheckBox).Checked = "0", "N", "S") & "</TD></TR>"
                End If
            Next
            strBody += "</TABLE>"
            Return strBody
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Function

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
