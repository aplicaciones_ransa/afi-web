Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports System.Data

Public Class VisorDocElectronico
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private objFactura As clsCNFactura = New clsCNFactura
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Dim dtBoleta As DataTable
            Dim drPDF As DataRow
            Dim objStream As Stream

            Dim sTipoCon As String = Session.Item("ssComEleSerPes_sTipoCon")
            Dim sTipoDoc As String = Session.Item("ssComEleSerPes_sTipoDoc")
            Dim sSerie As String = Session.Item("ssComEleSerPes_sSerie")
            Dim sCorre As String = Session.Item("ssComEleSerPes_sCorre")
            Dim sFcha As String = Session.Item("ssComEleSerPes_sFcha")
            Dim sMonto As String = Session.Item("ssComEleSerPes_sMonto")


            dtBoleta = objFactura.fn_GetBoleta(sTipoCon, sTipoDoc, sSerie, sCorre, sFcha, sMonto)

            If dtBoleta.Rows.Count <> 0 Then
                If sTipoCon = "1" Then
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "application/pdf"
                    Response.BinaryWrite(CType(dtBoleta.Rows(0).Item("NO_PDF_CSEL"), Byte()))
                End If
            Else
                Response.Write("No se encontr� el archivo")
            End If
            Response.Flush()
            Response.Close()
        Catch ex As Exception
            Response.Write("No se encontr� el archivo")
        End Try
    End Sub


    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
