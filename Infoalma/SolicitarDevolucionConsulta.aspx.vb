Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class SolicitarDevolucionConsulta
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtDevolucionConsulta As System.Web.UI.WebControls.TextBox
    'Protected WithEvents RequiredFieldValidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtSolicitante As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroCajas As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDirEntrega As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAutorizador As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG9") Then
            Try
                CargarCombos()
                txtAutorizador.Text = Session.Item("UsuarioLogin")
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))
    End Sub

    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        EnviarCorreo()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "P", "DEPSAFIL", "SOLICITAR DEVOLUCION DE CONSULTA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Sub EnviarCorreo()
        objFuncion = New LibCapaNegocio.clsFunciones
        Try
            objFuncion.SendMail("Depsa Files � Sistema de Gesti�n de Archivos <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                                        System.Configuration.ConfigurationManager.AppSettings.Item("Destinatario"),
                                        CStr(Session.Item("NomCliente")) & " - Solicitud Devoluci�n de Consultas",
                                        "Solicitado por     :   " & Me.txtSolicitante.Text & " <br>" &
                                        "�rea       :   " & Me.cboArea.SelectedItem.Text & "<br>" &
                                        "Cantidad Cajas Solicitadas       :   " & Me.txtNroCajas.Text & "<br>" &
                                        "Direcci�n de Entrega       :   " & Me.txtDirEntrega.Text & "<br>" &
                                        "Devoluci�n Consulta       :   " & Me.txtDevolucionConsulta.Text & "<br>" &
                                        "Autorizado por       :   " & Me.txtAutorizador.Text & "<br><br><br>" &
                                        "Fecha Solicitud    :   " & Now() & "<BR> Dep�sitos S.A. - Sistema de Gesti�n de Archivos Depsa Files")
            Me.lblError.Text = "Su solicitud fue enviada con �xito!"
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
