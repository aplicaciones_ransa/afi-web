Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.IO
Imports System.Data
Imports Infodepsa

Public Class ReporteTradersDetalle
    Inherits System.Web.UI.Page
    Private objTraders As clsCNTraders = New clsCNTraders
    'Protected WithEvents txtWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDCR As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    'Protected WithEvents Requiredfieldvalidator11 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents cboUnidadMedida As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents Requiredfieldvalidator10 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator9 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator6 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtDua As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator8 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtMercaderia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator7 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtEmprTransporte As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblConductor As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTrader As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHumedadIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtCantSolicitada As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTempIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnFechaIniRecep As System.Web.UI.HtmlControls.HtmlInputButton
    'Protected WithEvents btnFechaFinRecep As System.Web.UI.HtmlControls.HtmlInputButton
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents gvTraders As System.Web.UI.WebControls.DataGrid
    Public Event ItemCreated As DataGridItemEventHandler
    Dim dt As DataTable
    Dim dtAlmacen As DataTable
    Dim dtDetalle As DataTable
    'Protected WithEvents cboCliente As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtVapor As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnCerrarMarcados As System.Web.UI.WebControls.Button
    Dim sValidaVacios As Boolean
    'Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaIniRecep As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinRecep As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents hlkDetalle As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "40") Then
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "40"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Detalle del documento"
                Me.btnExportar.Attributes.Add("onclick", "javascript:if(confirm('�Est� seguro de exportar el documento?')== false) return false;")

                'Dim var1 As String = Session.Item("IdUsuario") & Session.Item("IdTipoUsuarioFiles") & _
                ' Session.Item("UsuarioLogin") & Session.Item("PeriodoAnual") & _
                'Session.Item("nom_user") & Session.Item("dir_emai") & _
                'Session.Item("cod_sist") & Session.Item("NroCertificado")

                'If Session.Item("IdTipoEntidad") = "04" Then
                '    Me.btnCerrarMarcados.Visible = True
                'Else
                Me.btnCerrarMarcados.Visible = False
                'End If

                ListaClientes()
                ListaUnidadMedida()

                Bindatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    Private Sub ListaClientes()
        Try
            Me.cboCliente.Items.Clear()
            dt = objTraders.gCNGetListarClienteTraders
            Me.cboCliente.Items.Add(New ListItem("TODOS", "0"))
            For Each dr As DataRow In dt.Rows
                Me.cboCliente.Items.Add(New ListItem(dr("Nombre").ToUpper(), dr("Codigo")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Private Sub ListaUnidadMedida()
        Try
            Me.cboUnidadMedida.Items.Clear()
            dt = objTraders.gCNListarUnidadMedida()
            For Each dr As DataRow In dt.Rows
                Me.cboUnidadMedida.Items.Add(New ListItem(dr("DE_UNME").ToUpper(), dr("CO_UNME")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub Bindatagrid()
        Try
            If Session.Item("TIP_USUA") = "SUPER" Then
                Me.txtDCR.Enabled = True
                Me.txtWarrant.Enabled = True
                gvTraders.Columns(19).Visible = False
                Me.cboCliente.Enabled = False
                Me.cboAlmacen.Enabled = False
                Me.cboUnidadMedida.Enabled = False
                Me.txtEmprTransporte.Enabled = False
                Me.txtMercaderia.Enabled = False
                Me.txtCantSolicitada.Disabled = True
                'Me.txtTempIngreso.Disabled = True
                Me.txtFechaIniRecep.Disabled = True
                Me.txtFechaFinRecep.Disabled = True
                'Me.txtHumedadIngreso.Disabled = True
                Me.txtVapor.Enabled = False
                Me.lblConductor.Enabled = False
                Me.txtDua.Enabled = False
                Me.cboTransportePor.Enabled = False
            End If
            If Session("NU_DOCU_TRADERS") Then
                dt = objTraders.gCNListarTradersXDocumento(Session.Item("NU_DOCU_TRADERS"))
                Me.lblTrader.Text = Session.Item("NU_DOCU_TRADERS")
                Me.cboCliente.SelectedValue = CType(dt.Rows(0)("CO_CLIE"), String).Trim
                ListarAlmacen()
                Me.cboAlmacen.SelectedValue = CType(dt.Rows(0)("CO_ALMA"), String).Trim
                Me.lblConductor.Text = CType(dt.Rows(0)("DE_COND_LOCA"), String).Trim
                Me.txtEmprTransporte.Text = CType(dt.Rows(0)("DE_TRAN"), String).ToUpper
                Me.txtMercaderia.Text = CType(dt.Rows(0)("DE_MERC"), String).ToUpper
                Me.txtCantSolicitada.Value = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_PESO_SOLI")))
                'Me.txtTempIngreso.Value = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_TEMP_INGR")))
                'Me.txtHumedadIngreso.Value = String.Format("{0:#,##0.00}", Convert.ToDouble(dt.Rows.Item(0).Item("NU_HUME_INGR")))
                Me.txtFechaIniRecep.Value = CType(dt.Rows(0)("FE_INI_RECE"), String).Trim
                Me.txtFechaFinRecep.Value = CType(dt.Rows(0)("FE_FINA_RECE"), String).Trim
                Me.txtVapor.Text = CType(dt.Rows(0)("DE_VAPO"), String).ToUpper
                Me.txtDua.Text = CType(dt.Rows(0)("NU_DUA"), String).Trim
                Me.txtDCR.Text = CType(dt.Rows(0)("NU_DOCU_RECE"), String).Trim
                Me.txtWarrant.Text = CType(dt.Rows(0)("NU_TITU"), String).Trim
                Me.cboUnidadMedida.SelectedValue = CType(dt.Rows(0)("CO_UNME"), String).Trim
                Me.lblEstado.Text = CType(dt.Rows(0)("DE_ESTA"), String).Trim
                Me.cboTransportePor.SelectedValue = CType(dt.Rows(0)("CO_TRAN_POR"), String).Trim




                Dim sCoUsuaco As String = ""
                Dim sFeUsuaco As String = ""
                sCoUsuaco = CType(dt.Rows(0)("CO_USUA_COPE"), String).Trim
                If CType(dt.Rows(0)("FE_USUA_COPE"), String).Trim = "01/01/1900" Then
                    sFeUsuaco = ""
                Else
                    sFeUsuaco = " - " & CType(dt.Rows(0)("FE_USUA_COPE"), String).Trim
                End If
                lblUsuariCierre.Text = sCoUsuaco & sFeUsuaco

                If CType(dt.Rows(0)("CO_ESTA"), String).Trim = "1" Or CType(dt.Rows(0)("CO_ESTA"), String).Trim = "2" Or CType(dt.Rows(0)("CO_ESTA"), String).Trim = "X" Then
                    Me.cboCliente.Enabled = False
                    Me.cboAlmacen.Enabled = False
                    Me.cboUnidadMedida.Enabled = False
                    Me.txtEmprTransporte.Enabled = False
                    Me.txtMercaderia.Enabled = False
                    Me.txtCantSolicitada.Disabled = True
                    'Me.txtTempIngreso.Disabled = True
                    Me.txtFechaIniRecep.Disabled = True
                    Me.txtFechaFinRecep.Disabled = True
                    'Me.txtHumedadIngreso.Disabled = True
                    Me.cboTransportePor.Enabled = False
                    Me.txtVapor.Enabled = False
                    Me.lblConductor.Enabled = False
                    Me.txtDua.Enabled = False
                    gvTraders.Columns(19).Visible = False
                End If
                dtDetalle = objTraders.gCNListarTradersDetalleCardex(Session.Item("NU_DOCU_TRADERS"))
                gvTraders.Columns(1).Visible = True
                Me.gvTraders.DataSource = dtDetalle
                Me.gvTraders.DataBind()
                gvTraders.Columns(1).Visible = False

                gvTraders.Columns(4).Visible = False
                gvTraders.Columns(5).Visible = False

                gvTraders.Columns(18).Visible = False
                gvTraders.Columns(19).Visible = False
                Me.lblError.Text = "Total de registros: " & ((dtDetalle.Rows.Count) - 1)
                Session.Add("dtTradersDeta", dtDetalle)


                'Mostrar Acumulado Cliente y Acumulado Conductor
                Dim i As Integer
                For i = 0 To dtDetalle.Rows.Count - 1
                    If i = (dtDetalle.Rows.Count - 2) Then
                        If IsDBNull(dtDetalle.Rows(i).Item("NU_ACU_CLIE")) Then
                            Me.lblAcuCli.Text = "0.000"
                        Else
                            Me.lblAcuCli.Text = Format(dtDetalle.Rows(i).Item("NU_ACU_CLIE"), "#,##0.000")
                        End If
                        If IsDBNull(dtDetalle.Rows(i).Item("NU_ACU_COND")) Then
                            Me.lblAcuCon.Text = "0.000"
                        Else
                            Me.lblAcuCon.Text = Format(dtDetalle.Rows(i).Item("NU_ACU_COND"), "#,##0.000")
                        End If
                    End If
                Next


            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub ListarAlmacen()
        Try
            Me.cboAlmacen.Items.Clear()
            dtAlmacen = objTraders.gCNgetlistaralmacen(Me.cboCliente.SelectedValue)
            Me.cboAlmacen.Items.Add(New ListItem("TODOS", "0"))
            For Each dr As DataRow In dtAlmacen.Rows
                Me.cboAlmacen.Items.Add(New ListItem(dr("DE_ALMA").ToUpper(), dr("CO_ALMA")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Private Sub ListarConductor()
        Try
            Me.lblConductor.Text = (objTraders.gCNGetListarConductor(Me.cboAlmacen.SelectedValue)).ToUpper
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("ReporteTraders.aspx")
    End Sub
    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        ListarConductor()
    End Sub
    Private Sub gvTraders_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvTraders.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim ck1 As CheckBox = CType(e.Item.FindControl("chkHCerrar"), CheckBox)
            If Not IsNothing(ck1) Then
                ck1.Attributes.Add("onclick", "Todos(this, 'chkCerrar');")
            End If
        End If
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            ' CType(e.Item.FindControl("imgAnular"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('�Est� seguro de anular el registro?')== false) return false;")
            '   CType(e.Row.FindControl("imgFinalizar"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('�Est� seguro de cerrar el registro?')== false) return false;")

            'documento finalizado
            If e.Item.DataItem("CO_ESTA_FINA").ToString.Trim = "0" Then
                If Session.Item("TIP_AUDI") = True Then
                    CType(e.Item.FindControl("chkCerrar"), CheckBox).Enabled = True
                Else
                    CType(e.Item.FindControl("chkCerrar"), CheckBox).Enabled = False
                End If
            Else
                e.Item.BackColor = System.Drawing.Color.FromName("#c8dcf0")
                CType(e.Item.FindControl("chkCerrar"), CheckBox).Enabled = False
                ' CType(e.Item.FindControl("imgAnular"), ImageButton).ImageUrl = "~/Images/transparente.png"
                'CType(e.Item.FindControl("imgAnular"), ImageButton).Enabled = False
            End If
            'anulados
            If e.Item.DataItem("CO_ESTA_ANUL").ToString = "1" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#dcdcdc")
                ' CType(e.Item.FindControl("imgAnular"), ImageButton).ImageUrl = "~/Images/transparente.png"
                ' CType(e.Item.FindControl("imgAnular"), ImageButton).Enabled = False
                CType(e.Item.FindControl("chkCerrar"), CheckBox).Enabled = False
            End If
            'ocultar icono finalizado
            If e.Item.DataItem("FE_ING_MERC").ToString = "" Then
                CType(e.Item.FindControl("chkCerrar"), CheckBox).Enabled = False
            End If
            'header inferior de la grilla
            If e.Item.DataItem("NU_DOCU_CLIE").ToString = "Total :" Then
                CType(e.Item.FindControl("chkCerrar"), CheckBox).Visible = False
                e.Item.BackColor = System.Drawing.Color.FromName("#5D7B9D")
                e.Item.ForeColor = System.Drawing.Color.White
                e.Item.Font.Bold = True
            End If

        End If
    End Sub
    Protected Sub cboCliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCliente.SelectedIndexChanged
        ListarAlmacen()
    End Sub
    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim sb As StringBuilder = New StringBuilder
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim page As Page = New Page
        Dim form As HtmlForm = New HtmlForm
        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtTradersDeta"), DataTable)
        dg.DataBind()
        dg.EnableViewState = False
        page.DesignerInitialize()
        page.Controls.Add(form)
        form.Controls.Add(dg)
        page.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=TradersDetalle.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
        dg = Nothing
    End Sub
    Private Function ValidaCheckMarcado() As Boolean
        Dim ValidaCheck As Boolean
        ValidaCheck = False
        Dim row As DataGridItem
        Dim intCount As Integer = 0
        If gvTraders.Items.Count <> 0 Then
            For Each row In gvTraders.Items
                If CType(row.FindControl("chkCerrar"), CheckBox).Checked = True And CType(row.FindControl("chkCerrar"), CheckBox).Enabled = True Then
                    ValidaCheck = True
                End If
                intCount += 1
            Next
        End If
        Return ValidaCheck
    End Function

    Private Sub btnCerrarMarcados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarMarcados.Click
        Try
            If ValidaCheckMarcado() = True Then
                Dim row As DataGridItem
                Dim intCount As Integer = 0
                If gvTraders.Items.Count <> 0 Then
                    For Each row In gvTraders.Items
                        If CType(row.FindControl("chkCerrar"), CheckBox).Checked = True And CType(row.FindControl("chkCerrar"), CheckBox).Enabled = True Then
                            objTraders.gCNFinalizarDetalleTraders(Session.Item("NU_DOCU_TRADERS"), row.Cells(1).Text(), Session.Item("IdUsuario"))
                        End If
                        intCount += 1
                    Next
                End If

                pr_IMPR_MENS("Se cerro correctamente")
            Else
                pr_IMPR_MENS("Ninguna fila seleccionada")
            End If
            Bindatagrid()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
End Class
