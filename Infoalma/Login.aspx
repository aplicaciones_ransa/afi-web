﻿<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HeaderLogin" Src="UserControls/HeaderLogin.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Alma Perú >> AFI >> Ingreso al Sistema</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="author" content="francisco_uni@hotmail.com">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
        <link href="font-awesome.css" rel="stylesheet">
        
		<script type="text/javascript" language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}				
		document.oncontextmenu = function(){return false}
		function Login()
			{
			var browserName=navigator.appName; 
			//window.open('Popup/AvisoFiestas.htm','Medios','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=456px,height=375px');
			if (browserName!="Microsoft Internet Explorer")
			{ 
				if(document.layers)
				{
				document.layers["UserPassword"].visibility="hide";				
				}
				if(document.all)
				{
				//btnEntrar.style.visibility = "hidden";
				document.all["UserPassword"].style.visibility="hidden";				
				}			
				//alert("Su explorador no esta soportado por nuestro sistema!");
			}
			else
			{
				if(document.layers)
				{				
				document.layers["txtUsuario"].focus();				
				}
				if(document.all)
				{				
				document.all["txtUsuario"].focus();				
				}	
			}
			if (top.location != window.location)
				{
				 top.location = window.location;							
				}
			}
			function Abrir(url, Height, Width)
			{			
				window.open(url,"Lgn","top=250,left=350,toolbar=0,status=0,directories=0,menubar=0,scrollbars=0,resize=0,width=" + Width + ",height=" + Height);
			}
			function AbrirPoputRecordarClave() {
				window.open("Popup/poputRecordarClave.aspx","",'left=180,top=140,width=800,height=400,toolbar=0,scrollbars=0,resizable=0');
			}
		</script>
 	    <style type="text/css">
            .auto-style1 {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 11px;
                color: #051e46;
                font-weight: normal;
                height: 204px;
                padding-left: 0px;
            }
            .auto-style2 {
                font-family: Arial, Helvetica, sans-serif;
                color: #454545;
                font-size: 11px;
                font-weight: normal;
            }
        </style>
 	</HEAD>
         	<BODY onload="Login();" bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0"
		bgColor="#f0f0f0">
          
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:headerlogin id="HeaderLogin1" runat="server"></uc1:headerlogin>
                        <%--<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%" background="Images/Login.jpg"--%>
                        <TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%" height="400">
							<TR>
                                <%--<TD class="Titulo" height="90" vAlign="middle" align="center">Bienvenidos a nuestra solución en línea--%>
								</TD>
							</TR>
							<TR>
								<TD align="center">
                                    <%--<asp:label id="lblIP" runat="server" CssClass="Text"></asp:label>--%>
								    <asp:label id="lblIP" runat="server" CssClass="Text"></asp:label>
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center">
									<TABLE id="Table3" border="0" cellSpacing="4" cellPadding="0" width="100%">
										<TR>
											<TD align="center"><asp:label id="lblTitulo" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD class="auto-style1" align="center">
        <img src="images/logo.png" style="height:200px;"/></TD>
										</TR>
										<TR>
											<TD align="center">
												<DIV id="UserPassword">
													<TABLE style="BORDER-BOTTOM: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-TOP: #66964D 8px solid; BORDER-RIGHT: #cccccc 1px solid"
														id="Ingreso" border="0" cellSpacing="4" width="300" align="center">
														<TR>
															<TD colspan="3" ></TD>
														</TR>
														<TR>
															<TD align="center" colspan="3"><asp:label id="lblCuerpo" runat="server" CssClass="Text"></asp:label></TD>
														</TR>
														<TR>
															<TD class="Text" width="15%">Usuario&nbsp;:</TD>
															<TD align="left" width="60%"><asp:textbox id="txtUsuario" runat="server" CssClass="auto-style2" Width="70%"></asp:textbox></TD>
															<TD width="5%"><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtUsuario"
																	ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
														</TR>
														<TR>
															<TD class="Text" width="15%">Contraseña&nbsp;:</TD>
															<TD align="left" width="60%"><asp:textbox id="txtContraseña" runat="server" CssClass="auto-style2" Width="70%" TextMode="Password"></asp:textbox></TD>
															<TD width="5%"><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtContraseña"
																	ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
														</TR>
														<TR>
															<TD colSpan="3" align="right">
                                    <asp:Button class="btn" ID="btnEntrar" runat="server"  Text="Validar Acceso &gt;" Width="109px" BorderStyle="None" BackColor="#3968C6" Font-Bold="True" ForeColor="White" />
                                                            </TD>
														</TR>
													</TABLE>
												</DIV>
											</TD>
										</TR>
										<TR>
											<TD align="center">
												<TABLE style="BORDER-BOTTOM: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-RIGHT: #cccccc 1px solid"
													id="Table2" border="0" cellSpacing="2" width="350" align="center">
													<TR>
														<TD><A class="Text" onclick="Javascript:AbrirPoputRecordarClave()"
																href="#">Recuperar contraseñas</A>
															<BR>
															<A class="Text" onclick="Javascript:Abrir('Popup/LgnRecuerda.aspx', '180', '295')"
																href="#">Si olvidó su clave y su respuesta secreta</A>
															<!--<BR>
															<A class="Ayuda" onclick="Javascript:Abrir('Popup/LgnRegistra.aspx', '380', '490')"
																href="#">Si aún no se encuentra registrado</A>-->
														</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
									<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD height="70"></TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</BODY>
</HTML>
