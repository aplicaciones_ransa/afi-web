<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codefile="AdmAsignarMenu.aspx.vb" Inherits="AdmAsignarMenu" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AdmAsignarMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:header id="Header2" runat="server"></uc1:header>
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="3">
												<uc1:MenuInfo id="MenuInfo1" runat="server"></uc1:MenuInfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="630" border="0" align="center">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table7" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="text" width="5%">Sistema:</TD>
																	<TD width="35%"><asp:dropdownlist id="cboSistema" runat="server" CssClass="text" AutoPostBack="True"></asp:dropdownlist></TD>
																	<TD class="text" width="7%">Grupo:</TD>
																	<TD width="40%"><asp:dropdownlist id="cboGrupo" runat="server" CssClass="text" AutoPostBack="True"></asp:dropdownlist></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
													AutoGenerateColumns="False">
													<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
													<ItemStyle CssClass="gvRow"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn DataField="NO_MODU" HeaderText="Nom. Modulo">
															<HeaderStyle Width="80px"></HeaderStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DE_MENU" HeaderText="Descripcion">
															<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Sel">
															<HeaderStyle Width="60px"></HeaderStyle>
															<ItemTemplate>
																<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="60" border="0">
																	<TR>
																		<TD align="center">
																			<asp:CheckBox id="chkActivo" runat="server" CssClass="Text"></asp:CheckBox></TD>
																	</TR>
																</TABLE>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn Visible="False" DataField="CO_MODU" HeaderText="CO_MODU"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="FLG_ACTI" HeaderText="FLG_ACTI"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="CO_MENU" HeaderText="CO_MENU"></asp:BoundColumn>
													</Columns>
													<PagerStyle CssClass="gvPager" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:button id="btnGrabar" runat="server" CssClass="btn" Text="Grabar" Width="80px"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
