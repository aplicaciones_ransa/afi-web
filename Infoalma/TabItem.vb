Imports System
Namespace Infodepsa
    Public Class TabItem
        Private _name As String
        Private _path As String

        Public Sub New(ByVal newName As String, ByVal newPath As String)
            _name = newName
            _path = newPath
        End Sub 'New

        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal Value As String)
                _name = Value
            End Set
        End Property

        Public Property Path() As String
            Get
                Return _path
            End Get
            Set(ByVal Value As String)
                _path = Value
            End Set
        End Property

    End Class
End Namespace