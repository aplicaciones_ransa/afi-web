Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports DEPSA.LibCapaNegocio
Imports System.Data
Imports Infodepsa

Public Class LiberacionDetalleContable
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objLiberacion As Liberacion = New Liberacion
    Private objSICO As SICO = New SICO
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCoEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    'Protected WithEvents chkWarrCustodia As System.Web.UI.WebControls.CheckBox
    Protected sCO_ALMA_SELE As String
    Structure strDATA
        Dim sCO_ALMA As String
        Dim sNU_DCRS As String
        Dim sNU_SECU As String
        Dim sUN_RETI As String
        Dim sPE_RETI As String
        Dim sUN_RECI As String
        Dim sPE_RECI As String
        Dim sNU_PESO_UNID As String
        Dim sTI_PESO_UNID As String
        Dim sIM_UNIT As String
        Dim sNU_SECU_UBIC As String
        Dim sCO_TIPO_BULT As String
        Dim sCO_UNME_PESO As String
        Dim sCO_UNME_AREA As String
        Dim sNU_AREA_USAD As String
        Dim sCO_UNME_VOLU As String
        Dim sNU_VOLU_USAD As String
        Dim sCO_TIPO_MERC As String
        Dim sDE_TIPO_MERC As String
        Dim sCO_STIP_MERC As String
        Dim sDE_STIP_MERC As String
        Dim sDE_MERC As String
        Dim sCO_MONE As String
    End Structure
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtEndosatario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFecha As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgTCALMA_MERC As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents hiOperacion As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents hiST_ALMA_SUBI As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents hiCO_ALMA As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbNU_DOCU_GRAB As System.Web.UI.WebControls.Label
    'Protected WithEvents cboUnidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboMoneda As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoRetiro As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoTitulo As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoComprobante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroRetiro As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroComprobante As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtAlmacen As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDeudaSoles As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDeudaDolares As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtObservacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hiNU_CANT_LINE As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtMercaderia As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents chkEmbarque As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnEmitir As RoderoLib.BotonEnviar
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And (InStr(Session("PagId"), "19") Or InStr(Session("PagId"), "22") Or InStr(Session("PagId"), "20")) Then

            Try
                Me.lblError.Text = ""
                Session.Remove("Mensaje")
                If Not IsPostBack Then
                    Dim strResult As String()
                    Me.lblOpcion.Text = "Datos del Documento"
                    If Request.QueryString("Pagina") = "N" Then
                        strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "19"), "-")
                    End If
                    If Request.QueryString("Pagina") = "E" Then
                        strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "22"), "-")
                    End If
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Me.txtEndosatario.Enabled = False
                    Me.txtFecha.Enabled = False
                    Me.cboTipoComprobante.Enabled = False
                    Me.cboTipoTitulo.Enabled = False
                    Me.cboTipoRetiro.Enabled = False
                    Me.cboModalidad.Enabled = False
                    Me.cboMoneda.Enabled = False
                    Me.cboUnidad.Enabled = False
                    Me.txtAlmacen.Enabled = False
                    Me.txtNroComprobante.Enabled = False
                    Me.txtNroRetiro.Enabled = False
                    Me.txtNroWarrant.Enabled = False
                    Me.txtNroWarrant.Text = Session.Item("NroDoc")
                    Me.txtMercaderia.Value = Request.QueryString("Merc")
                    Me.txtEndosatario.Text = Request.QueryString("Banco")
                    Me.txtFecha.Text = Request.QueryString("Fecha")
                    If Session.Item("Embarque") = "SI" Then
                        Me.chkEmbarque.Checked = True
                    Else
                        Me.chkEmbarque.Checked = False
                    End If
                    If (Session.Item("IdTipoEntidad") = "03" Or Session.Item("IdTipoEntidad") = "01") And Session.Item("IdTipoUsuario") <> "02" Then
                        Me.btnSolicitar.Enabled = True
                    Else
                        Me.btnSolicitar.Enabled = False
                    End If
                    If Session.Item("IdTipoEntidad") = "01" Then
                        Me.chkEmbarque.Enabled = True
                    Else
                        Me.chkEmbarque.Enabled = False
                    End If
                    'If Session("WarCustodia") = "SI" Then
                    '    Me.chkWarrCustodia.Checked = True
                    '    'Me.btnEmitir.Text = "Liberar Warrant en custodia"
                    'Else
                    '    Me.chkWarrCustodia.Checked = False
                    'End If

                    If Session.Item("IdTipoEntidad") = "03" And Session.Item("FlgContable") = "SI" And (Session.Item("IdTipoUsuario") = "03" Or Session.Item("IdTipoUsuario") = "01") Then
                        'If Session.Item("IdTipoEntidad") = "03" And Session.Item("FlgContable") = "SI" And Session.Item("IdTipoUsuario") = "03" Then
                        Me.chkTraslado.Enabled = True
                        'Me.btnAprobar.Enabled = True
                        Me.txtObservacion.Enabled = True
                        Me.cboDestino.Visible = True

                        'If Session.Item("FlgVisto") = 0 Then
                        'Me.btnSolicitar.Enabled = False
                        '    Me.lblError.Text = "El documento no tiene visto bueno"
                        'Else
                        Me.btnSolicitar.Enabled = True
                        'End If

                    Else
                        Me.chkTraslado.Enabled = False
                        Me.btnSolicitar.Enabled = False
                        Me.txtObservacion.Enabled = False
                        Me.cboDestino.Visible = False
                    End If


                    'Tipo de unidades
                    pr_AYUD_UNID()
                    'Tipo de documento que identifica a retiros
                    pr_SELE_RETI()
                    'Tipo de warrants
                    pr_AYUD_TIPO()
                    'Tipo de Comprobantes de Recepci�n
                    pr_AYUD_COMP()
                    'Tipo de monedas
                    pr_AYUD_MONE()
                    'Tipo de modalidades
                    pr_AYUD_MODA()
                    Me.txtNroWarrant.Text = Session("NroDoc")
                    Me.cboTipoTitulo.SelectedValue = Session("IdTipoDocumento")
                    Me.cboTipoComprobante.SelectedValue = Session("TipoComprobante")
                    Me.txtNroComprobante.Text = Session("NroComprobante")
                    Me.cboUnidad.SelectedValue = Session("CodUnidad")
                    'visualiza los datos que no se editan
                    pr_SELE_NOED()
                    pr_CARG_GRIL_0001()
                    loadAlmacen()
                    Me.cboDestino.SelectedValue = Session("CodAlmaDest")
                    ' Habilitar campo importe
                    If objDocumento.gCADGetRetiroImporte(Session.Item("IdSicoDepositante"), Me.cboModalidad.SelectedValue) = "S" Then
                        dgTCALMA_MERC.Columns(31).Visible = True
                        dgTCALMA_MERC.Columns(32).Visible = True
                    Else
                        dgTCALMA_MERC.Columns(31).Visible = False
                        dgTCALMA_MERC.Columns(32).Visible = False
                    End If
                Else
                    If Me.hiOperacion.Value = "Calcular" Then
                        'pr_CALC_DETA()
                    End If
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    'Private Sub pr_AYUD_UNID()
    '    Dim dsTTTIPO_INF2 As DataSet = objSICO.fn_TTTIPO_INF2_Q01(Session("CoEmpresa"))
    '    Me.cboUnidad.Items.Clear()
    '    For i As Integer = 0 To dsTTTIPO_INF2.Tables(0).Rows.Count - 1
    '        Me.cboUnidad.Items.Add(New ListItem(dsTTTIPO_INF2.Tables(0).Rows(i)("Descripcion"), dsTTTIPO_INF2.Tables(0).Rows(i)("Codigo")))
    '    Next
    '    dsTTTIPO_INF2.Dispose()
    'End Sub
    Private Sub pr_SELE_NOED()
        Try
            Dim drTCALMA_MERC As SqlDataReader = objSICO.fn_TCALMA_MERC_Q05(Session("CoEmpresa"),
                                                Me.cboUnidad.SelectedItem.Value, Me.cboTipoComprobante.SelectedItem.Value, Me.txtNroComprobante.Text,
                                                Me.cboTipoTitulo.SelectedItem.Value, Me.txtNroWarrant.Text)
            If drTCALMA_MERC.Read Then
                'almacen
                Me.txtAlmacen.Text = IIf(IsDBNull(drTCALMA_MERC("DE_ALMA")), "", drTCALMA_MERC("DE_ALMA"))
                Me.hiCO_ALMA.Value = IIf(IsDBNull(drTCALMA_MERC("CO_ALMA")), "", drTCALMA_MERC("CO_ALMA"))
                'modalidad
                If Not IsDBNull(drTCALMA_MERC("CO_MODA_MOVI")) Then
                    Me.cboModalidad.SelectedIndex = Me.cboModalidad.Items.IndexOf(Me.cboModalidad.Items.FindByValue(drTCALMA_MERC("CO_MODA_MOVI")))
                End If
                'moneda
                If Not IsDBNull(drTCALMA_MERC("CO_MONE")) Then
                    Me.cboMoneda.SelectedIndex = Me.cboMoneda.Items.IndexOf(Me.cboMoneda.Items.FindByValue(drTCALMA_MERC("CO_MONE")))
                End If
                'status almacen
                Me.hiST_ALMA_SUBI.Value = IIf(IsDBNull(drTCALMA_MERC("ST_ALMA_SUBI")), "", drTCALMA_MERC("ST_ALMA_SUBI"))
                Me.hiNU_CANT_LINE.Value = IIf(IsDBNull(drTCALMA_MERC("NU_CANT_LINE")), 0, drTCALMA_MERC("NU_CANT_LINE"))

                'Traslado
                If IIf(IsDBNull(drTCALMA_MERC("CO_ALMA")), "", drTCALMA_MERC("CO_ALMA")) <> Session("CodAlmaDest") Then
                    Me.chkTraslado.Checked = True
                End If
            End If
            drTCALMA_MERC.Close()
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub pr_CARG_GRIL_0001()
        Try
            'Carga el detalle
            pr_CARG_TABL()
            Me.hiOperacion.Value = "Calcular"
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
            Me.lbMS_REGI.Text = "En este momento no se puede hacer la consulta, vuelva a intentar"
        End Try
    End Sub

    Private Sub pr_CARG_TABL()
        Try
            Dim drTCALMA_MERC As SqlDataReader
            Dim sCA_VALI As String
            Dim dsTCALMA_MERC As DataSet
            'Validaci�n previa de saldo 0 y retiros en los 3 meses anteriores
            'busqueda de todos los documentos ST_ALMA_SUBI
            If Me.hiCO_ALMA.Value.Trim <> "" Then
                If Me.txtNroWarrant.Text.Trim = "" And Me.txtNroComprobante.Text.Trim = "" Then
                    If objSICO.fn_TMALMA_Q01(Session("CoEmpresa"), Me.hiCO_ALMA.Value) = "S" Then
                        Me.hiST_ALMA_SUBI.Value = "N"
                    ElseIf objSICO.fn_TMALMA_Q01(Session("CoEmpresa"), Me.hiCO_ALMA.Value) = "N" Then
                        Me.hiST_ALMA_SUBI.Value = "S"
                    End If
                End If
            End If

            dsTCALMA_MERC = objSICO.fn_TCALMA_MERC_Q06(Session("CoEmpresa"),
                            Me.cboUnidad.SelectedItem.Value, Me.cboTipoComprobante.SelectedItem.Value,
                            Me.txtNroComprobante.Text, Session.Item("IdSicoDepositante"), Me.hiCO_ALMA.Value,
                            Me.hiST_ALMA_SUBI.Value, "", "", "", "", "",
                            "", Me.cboModalidad.SelectedItem.Value, Me.cboMoneda.SelectedItem.Value, "")

            If dsTCALMA_MERC.Tables(0).Rows.Count > 0 Then
                Dim nNU_FILA_MOST As Integer = Convert.ToInt32(ConfigurationManager.AppSettings.Item("SF"))
                Dim nNU_FILA As Integer = dsTCALMA_MERC.Tables(0).Rows.Count
                Dim dtTCALMA_MERC As New DataTable
                dtTCALMA_MERC = fg_FILT_REGI(dsTCALMA_MERC.Tables(0).DefaultView, nNU_FILA_MOST)
                Session("dvRE_GENE") = New DataView(dtTCALMA_MERC)
                Me.dgTCALMA_MERC.DataSource = New DataView(dtTCALMA_MERC)
                Me.dgTCALMA_MERC.DataBind()
                If nNU_FILA > nNU_FILA_MOST Then
                    Me.lbMS_REGI.Text = "Hay disponibles m�s de " & nNU_FILA_MOST.ToString() & " registros. Intente aplicando otros filtros si no encuentra el registro que busca."
                End If
            Else
                If Me.txtNroWarrant.Text.Trim = "" And Me.txtNroComprobante.Text.Trim = "" Then
                    Me.lbMS_REGI.Text = "No se encontraron registros"
                Else
                    Me.lbMS_REGI.Text = "Comprobante no tiene saldo"
                End If
                pg_DATA_LIMP(Me.dgTCALMA_MERC)
            End If



        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub
    Private Sub loadAlmacen()
        Try
            Dim dt As DataTable
            Me.cboDestino.Items.Clear()
            dt = objDocumento.gCADGetListarAlmacen(Session.Item("IdSicoDepositante"))
            Me.cboDestino.Items.Add(New ListItem("", "0"))
            For Each dr As DataRow In dt.Rows
                Me.cboDestino.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        End Try
    End Sub
    Public Sub pg_DATA_LIMP(ByVal pDG_GENE As WebControls.DataGrid)
        Dim dtTA_NUEV As New DataTable
        Dim dsCO_NUEV As New DataSet
        dsCO_NUEV.Tables.Add(dtTA_NUEV)
        pDG_GENE.DataSource = dsCO_NUEV
        pDG_GENE.DataBind()
    End Sub

    Public Function fg_FILT_REGI(ByVal pDV_GENE As DataView, ByVal nNU_MOST As Integer) As DataTable
        Dim nNU_FILA As Integer = pDV_GENE.Table.Rows.Count
        Dim nNU_COLU As Integer = pDV_GENE.Table.Columns.Count
        Dim dtTA_NUEV As New DataTable

        If nNU_FILA > nNU_MOST Then
            dtTA_NUEV = pDV_GENE.Table.Clone()

            For nFI_ACTU As Integer = 1 To nNU_MOST
                Dim dwTA_NUEV As System.Data.DataRow = dtTA_NUEV.NewRow()
                dwTA_NUEV.ItemArray = pDV_GENE.Table.Rows(nFI_ACTU - 1).ItemArray
                dtTA_NUEV.Rows.Add(dwTA_NUEV)
            Next nFI_ACTU
        Else
            dtTA_NUEV = pDV_GENE.Table
        End If
        Return dtTA_NUEV
    End Function

    Private Sub pr_CALC_DETA()
        Try
            Dim nNU_FILA As Integer
            Dim bOK_SELE As Boolean = False
            Dim bOK_DATO As Boolean
            Dim sDA_DCRS As strDATA
            For nNU_FILA = 0 To Me.dgTCALMA_MERC.Items.Count - 1
                Dim chST_MARC As CheckBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("chST_SELE"), CheckBox)
                If chST_MARC.Checked Then
                    'selecciono al menos un registro
                    bOK_SELE = True
                    Dim sTI_PESO_UNID As String
                    Dim nNU_UNID_RETI, nNU_UNID_RECI, nNU_PESO_RETI, nNU_PESO_RECI, nNU_PESO_UNID As Decimal
                    'almacen
                    sDA_DCRS.sCO_ALMA = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(1).Text

                    sDA_DCRS.sNU_DCRS = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(2).Text
                    'secuencial DCR
                    sDA_DCRS.sNU_SECU = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(3).Text
                    'unidades retiradas
                    Dim tbNU_UNID_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_UNID_RETI"), TextBox)
                    sDA_DCRS.sUN_RETI = IIf(tbNU_UNID_RETI.Text.Trim = "", 0, tbNU_UNID_RETI.Text)
                    'peso retirado
                    Dim tbNU_PESO_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_PESO_RETI"), TextBox)
                    sDA_DCRS.sPE_RETI = IIf(tbNU_PESO_RETI.Text.Trim = "", 0, tbNU_PESO_RETI.Text)
                    'unidades recibidas
                    sDA_DCRS.sUN_RECI = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text)
                    'peso recibido
                    sDA_DCRS.sPE_RECI = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text)
                    'numero peso unidades
                    sDA_DCRS.sNU_PESO_UNID = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(18).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(18).Text)
                    'tipo peso unidades
                    sDA_DCRS.sTI_PESO_UNID = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(19).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(19).Text)
                    '------asignacion de valores
                    'unidades retiradas
                    nNU_UNID_RETI = CType(sDA_DCRS.sUN_RETI, Decimal)
                    'peso retirado
                    nNU_PESO_RETI = CType(sDA_DCRS.sPE_RETI, Decimal)
                    'unidades recibidas
                    nNU_UNID_RECI = CType(sDA_DCRS.sUN_RECI, Decimal)
                    'peso recibido
                    nNU_PESO_RECI = CType(sDA_DCRS.sPE_RECI, Decimal)
                    'numero peso unidades
                    nNU_PESO_UNID = CType(sDA_DCRS.sNU_PESO_UNID, Decimal)
                    'tipo peso unidades
                    sTI_PESO_UNID = sDA_DCRS.sTI_PESO_UNID
                    bOK_DATO = True
                    ''-----------------------------------------------------''
                    ''--------- validacion de unidades retiradas ----------''
                    If nNU_UNID_RETI <= 0 Then
                        'si la columnma de unidades retiradas es menor a cero                    
                        If nNU_UNID_RECI <> 0 Then
                            nNU_UNID_RETI = FormatNumber(nNU_UNID_RECI, 6)
                        End If
                        pr_IMPR_MENS("Cantidad Unidades Retiradas No V�lido, debe ser Menor o Igual a " & CStr(nNU_UNID_RECI) & " en C/R " & sDA_DCRS.sNU_DCRS & ", secuencial " & sDA_DCRS.sNU_SECU)
                        bOK_DATO = False

                    ElseIf nNU_UNID_RETI > nNU_UNID_RECI Then
                        'si las unidades retiradas son mayores a las unidades de saldo 
                        If nNU_UNID_RECI <> 0 Then
                            nNU_UNID_RETI = FormatNumber(nNU_UNID_RECI, 6)
                        End If
                        pr_IMPR_MENS("Unidades Retiradas debe ser Menor o Igual a " & CStr(nNU_UNID_RECI) & " en C/R " & sDA_DCRS.sNU_DCRS & ", secuencial " & sDA_DCRS.sNU_SECU)
                        bOK_DATO = False
                    Else
                        nNU_UNID_RETI = FormatNumber(nNU_UNID_RETI, 6)

                        'si la columna peso por unidad , campo nu_peso_unid del carga es mayor a cero
                        'y la columna sTI_PESO_UNID es 'B' entonces calcula el peso 
                        If (nNU_PESO_UNID > 0 And sTI_PESO_UNID = "B") Then
                            If nNU_UNID_RETI <> nNU_UNID_RECI Then
                                nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI * nNU_PESO_UNID, 6)
                            Else
                                nNU_PESO_RETI = nNU_PESO_RECI
                            End If
                        End If
                        If nNU_PESO_UNID > 0 And sTI_PESO_UNID = "U" Then
                            If nNU_UNID_RETI <> nNU_UNID_RECI Then
                                nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI / nNU_PESO_UNID, 6)
                            Else
                                nNU_PESO_RETI = nNU_PESO_RECI
                            End If
                        End If
                    End If

                    tbNU_PESO_RETI.Text = FormatNumber(nNU_PESO_RETI, 6)
                    tbNU_UNID_RETI.Text = FormatNumber(nNU_UNID_RETI, 6)
                    If bOK_DATO = False Then
                        Exit For
                    End If
                End If
            Next
            'si no se selecciono ningun registro
            If bOK_SELE = False Then
                pr_IMPR_MENS("Debe seleccionar al menos un registro")
                Exit Sub
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub pr_AYUD_MODA()
        Dim dsTTMODA As DataSet = objSICO.fn_CONS_MODA_0002(Session("CoEmpresa"))
        Me.cboModalidad.Items.Clear()
        For i As Integer = 0 To dsTTMODA.Tables(0).Rows.Count - 1
            Me.cboModalidad.Items.Add(New ListItem(dsTTMODA.Tables(0).Rows(i)("Descripcion"), dsTTMODA.Tables(0).Rows(i)("Codigo")))
        Next
        dsTTMODA.Dispose()
    End Sub

    Private Sub pr_AYUD_MONE()
        Dim dsTTMONE As DataSet = objSICO.fn_TTMONE_Q01()
        Me.cboMoneda.Items.Clear()
        For i As Integer = 0 To dsTTMONE.Tables(0).Rows.Count - 1
            Me.cboMoneda.Items.Add(New ListItem(dsTTMONE.Tables(0).Rows(i)("Descripcion"), dsTTMONE.Tables(0).Rows(i)("Codigo")))
        Next
        dsTTMONE.Dispose()
    End Sub

    Private Sub pr_AYUD_COMP()
        Dim dsTCALMA_MERC As DataSet = objSICO.fn_TCALMA_MERC_Q02()
        Me.cboTipoComprobante.Items.Clear()
        For i As Integer = 0 To dsTCALMA_MERC.Tables(0).Rows.Count - 1
            Me.cboTipoComprobante.Items.Add(New ListItem(dsTCALMA_MERC.Tables(0).Rows(i)("Descripcion"), dsTCALMA_MERC.Tables(0).Rows(i)("Codigo")))
        Next
        dsTCALMA_MERC.Dispose()
    End Sub

    Private Sub pr_AYUD_TIPO()
        Dim dsTCALMA_MERC As DataSet
        dsTCALMA_MERC = objSICO.fn_TCALMA_MERC_Q01(Session("CoEmpresa"))
        Me.cboTipoTitulo.Items.Clear()
        For i As Integer = 0 To dsTCALMA_MERC.Tables(0).Rows.Count - 1
            Me.cboTipoTitulo.Items.Add(New ListItem(dsTCALMA_MERC.Tables(0).Rows(i)("Descripcion"), dsTCALMA_MERC.Tables(0).Rows(i)("Codigo")))
        Next
        dsTCALMA_MERC.Dispose()
    End Sub

    Private Sub pr_AYUD_UNID()
        Dim dsTTTIPO_INF2 As DataSet = objSICO.fn_TTTIPO_INF2_Q01(Session("CoEmpresa"))
        Me.cboUnidad.Items.Clear()
        For i As Integer = 0 To dsTTTIPO_INF2.Tables(0).Rows.Count - 1
            Me.cboUnidad.Items.Add(New ListItem(dsTTTIPO_INF2.Tables(0).Rows(i)("Descripcion"), dsTTTIPO_INF2.Tables(0).Rows(i)("Codigo")))
        Next
        dsTTTIPO_INF2.Dispose()
    End Sub

    Public Function fg_CREA_AYUD(ByVal dsTB_GENE As DataSet, Optional ByVal sTX_DESC As String = "") As DataTable
        Dim tbTB_GENE As DataTable = dsTB_GENE.Tables(0)
        Dim drTB_GENE As DataRow = tbTB_GENE.NewRow()

        Try
            drTB_GENE("CODIGO") = ""
        Catch
            drTB_GENE("CODIGO") = 0
        End Try
        drTB_GENE("DESCRIPCION") = sTX_DESC
        tbTB_GENE.Rows.InsertAt(drTB_GENE, 0)
        Return tbTB_GENE
    End Function


    Private Sub pr_SELE_RETI()
        Dim sTI_DOCU_RETI As String = objSICO.fn_TMPARA_OPER_Q01(Session("CoEmpresa"))
        Me.cboTipoRetiro.Items.Clear()
        Me.cboTipoRetiro.Items.Add(New ListItem(sTI_DOCU_RETI, sTI_DOCU_RETI))
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        'If Session.Item("IdTipoEntidad") = "01" Then
        '    Response.Redirect("LiberacionEmbarque.aspx")
        'Else
        Response.Redirect("LiberacionContable.aspx")
        'End If
    End Sub


    Protected Sub btnSolicitar_Click(sender As Object, e As EventArgs) Handles btnSolicitar.Click

        Emitir() 'liberacion Nueva
        'Contable() 'liberacion Contable
    End Sub


    Private Sub Emitir()

        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "19", "EMITIR LIBERACION", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        ''======== (I) GENERA SOLICITUD CONTABLE ==================
        Dim dtMail As New DataTable
        Dim dtcab As New DataTable
        Dim strEmails As String
        Dim blnEmbarque As Boolean
        Dim strNombreArchivoPDF As String
        Dim strCodAlmaDest As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        Dim sMsgSolicitud As String
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()

        Try
            'Obtener datos de la liberacion
            Dim dt As New DataTable
            dt = objDocumento.gGetWarrantLiberaciones(Session.Item("NroDoc"), "0", "0", "", Session.Item("IdSico"), "N", "0")
            strCodAlmaDest = Me.cboDestino.SelectedValue
            If dt.Rows(0).Item("ST_ENDO_EMBA").ToString() = "SI" Then
                blnEmbarque = True
            Else
                blnEmbarque = False
            End If
            If dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "&nbsp;" Or dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "" Then
                objTrans.Rollback()
                objTrans.Dispose()
                pr_IMPR_MENS("Warrant no negociado")
                Exit Sub
            End If

            'GENERA DOR EN SICO 

            '======== (II) GENERA SOLICITUD CONTABLE ==================

            'verificacion de limite por cliente - "muestra limite en alerta"
            Dim strResultado As String
            strResultado = objDocumento.InsSolicitudWarrants_LimiteNvoWarrant("01", Session.Item("SecContable"), Session.Item("IdSicoDepositante"))
            If strResultado <> "S" Then
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = "No se puede generar la liberaci�n para nuevo embarque, " & strResultado
                Exit Sub
            End If
            If Me.chkTraslado.Checked = True And Me.cboDestino.SelectedValue = 0 Then
                objTrans.Rollback()
                objTrans.Dispose()
                pr_IMPR_MENS("Si es con traslado seleccione un destino")
                Exit Sub
            End If
            If Me.chkEmbarque.Checked = True Then
                objTrans.Rollback()
                objTrans.Dispose()
                pr_IMPR_MENS("No puede ser para Embarque")
                Exit Sub

                'pr_GUAR_0001() :
                'Verifica si existe factor de cambio del d�a
                'procesa los datos del ultimo DCR
                'Generaci�n orden retiro (web)

            ElseIf pr_GUAR_0001() Then

                Dim strNombreCliente As String
                Dim strResult As String
                Dim strEmailsConAdjunto As String = ""
                Dim strEmailsSinAdjunto As String = ""

                If objDocumento.gInsLiberacion(Me.cboUnidad.SelectedValue, Me.txtNroWarrant.Text,
                    Me.cboTipoTitulo.SelectedValue, Session.Item("IdSicoDepositante"), Session.Item("CodEntiFina"),
                    Me.cboTipoRetiro.SelectedValue, Me.txtNroRetiro.Text, Me.txtFecha.Text, Session.Item("IdUsuario"),
                    Me.cboModalidad.SelectedValue, Me.cboMoneda.SelectedValue, Me.cboTipoComprobante.SelectedValue,
                    Me.txtNroComprobante.Text, Me.txtAlmacen.Text, Me.txtObservacion.Text, "", "", Me.txtMercaderia.Value,
                    Session.Item("Virtual"), Me.chkEmbarque.Checked, True, Me.chkTraslado.Checked, False, Me.cboDestino.SelectedItem.Text, objTrans) = 1 Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("El n�mero de retiro ya existe")
                    Exit Sub
                End If
                If InsertarItemLiberacion(Me.txtNroRetiro.Text, objTrans) = False Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("El retiro no pudo registrarse")
                    Exit Sub
                Else
                    objTrans.Commit()
                    objTrans.Dispose()

                    strNombreArchivoPDF = strPath & "02" & Me.txtNroWarrant.Text & Me.txtNroRetiro.Text & ".pdf"
                    Dim dtDetalle As New DataTable

                    'Lista el detalle de la liberaci�n 
                    dtDetalle = objDocumento.gGetDetalleLiberacion(strCoEmpresa, Me.cboUnidad.SelectedValue, Me.txtNroRetiro.Text, Session.Item("IdTipoDocumento"), Me.txtNroWarrant.Text)
                    strNombreCliente = objLiberacion.GetReporteLiberacion(strNombreArchivoPDF, strCoEmpresa, Me.cboUnidad.SelectedValue, "DOR",
                    Me.txtNroRetiro.Text, Session.Item("IdSicoDepositante"), dtDetalle, Me.cboTipoTitulo.SelectedValue, Me.chkEmbarque.Checked,
                    True, Me.chkTraslado.Checked, strPathFirmas)

                    'Insertar Liberacion en wfdocumento
                    strResult = objDocumento.gInsLiberacionFlujo(Me.txtNroWarrant.Text, Me.txtNroRetiro.Text, Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"), Me.cboUnidad.SelectedValue)
                    If strResult.Substring(0, 2) = "XX" Then
                        pr_IMPR_MENS("No se pudo generar el documento")
                        Exit Sub
                    End If

                    dtDetalle.Dispose()
                    dtDetalle = Nothing
                End If

            Else
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = "No se puede generar la liberaci�n "
                Exit Sub
            End If

            'FIN GENERA DOR SICO


            'Insertar liberacion contable 
            objConexion.Close()
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()

            If objDocumento.gInsLiberacionContable(dt.Rows(0).Item("CO_UNID").ToString(), dt.Rows(0).Item("NU_TITU").ToString(),
            dt.Rows(0).Item("TI_TITU").ToString(), Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(),
            "DOR", txtNroRetiro.Text, dt.Rows(0).Item("FE_VIGE_BANC").ToString(), Session.Item("IdUsuario"),
             dt.Rows(0).Item("CO_MODA_MOVI").ToString(), dt.Rows(0).Item("co_mone").ToString(), dt.Rows(0).Item("TI_DOCU_RECE").ToString(),
              dt.Rows(0).Item("NU_DOCU_RECE").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", "", "", dt.Rows(0).Item("DE_MERC_GENE").ToString(),
              dt.Rows(0).Item("virtual").ToString(), blnEmbarque, strCodAlmaDest, objTrans) = 1 Then
                pr_IMPR_MENS("El n�mero de warrant ya tiene una Solicitud pendiente")
                objTrans.Rollback()
                objTrans.Dispose()
                Exit Sub
            Else
                Dim strFlgDblEndoso As String
                Dim strTipoTitulo As String
                If dt.Rows(0).Item("FLG_DENDO").ToString() = "S" Then
                    strFlgDblEndoso = 1
                Else
                    strFlgDblEndoso = 0
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "WAR" Then
                    strTipoTitulo = 3
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "CER" Then
                    strTipoTitulo = 1
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "WIP" Then
                    strTipoTitulo = 3
                End If

                'Inserta Cabecera de la solicitud de warrant

                dtcab = objDocumento.SetGrabarSolicitudWarrantCabecera("", 0, "24", dt.Rows(0).Item("CO_MODA_MOVI").ToString(), strFlgDblEndoso, dt.Rows(0).Item("TI_ALMA").ToString(), "",
                       "0", strTipoTitulo, dt.Rows(0).Item("CO_SEGU_ENDO").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", dt.Rows(0).Item("co_mone").ToString(),
                       Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(), "", Session.Item("IdUsuario"), dt.Rows(0).Item("NU_TITU").ToString(), objTrans)

                If dtcab.Rows.Count() > 0 Then
                    Session.Add("NroSoliWarr", dtcab.Rows(0)("NRO_DOCU"))

                End If

                dtMail = objUsuario.gGetEmailAprobadores("00000001", dt.Rows(0).Item("CO_ALMA").ToString(), "01")

                If dtMail.Rows.Count > 0 Then
                        For i As Integer = 0 To dtMail.Rows.Count - 1
                            If dtMail.Rows(i)("EMAI") <> "" And dtMail.Rows(i)("FLG_ADJU") = "1" Then
                                strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                            End If
                        Next

                        If objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                    strEmails.Remove(strEmails.Length - 1, 1),
                    "Solicitud de Liberaci�n para emisi�n de nuevo Warrant - " & Session.Item("NombreEntidad"),
                    "Estimados Se�ores:<br><br>" &
                    "El cliente ha solicitado una Liberaci�n contable" &
                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("NU_TITU").ToString() & "</FONT></STRONG><br>" &
                    "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG><br>" &
                    "Direcci�n de Almac�n: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("DE_DIRE_ALMA").ToString() & "</FONT></STRONG><br>" &
                    "<STRONG><FONT color='#330099'>Agradecemos enviar Inventario F�sico para reemplazo de Warrant al �rea de Operaciones</FONT></STRONG>" &
                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "") = False Then
                            pr_IMPR_MENS("No se pudo enviar el correo de notificaci�n")
                        End If

                    End If
                    dtMail.Dispose()
                    dtMail = Nothing

                    objTrans.Commit()
                    objTrans.Dispose()
                End If

                Response.Redirect("LiberacionContable.aspx", False)

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

#Region "***************************** Emitir Orden *********************************"



    Private Function fn_PROC_DATO_DCRS(ByVal arRE_DCRS As ArrayList, ByVal sNU_RECE_GRAB As String) As Boolean
        Try
            'validamos que el arreglo contenga registros
            If Not arRE_DCRS.Count > 0 Then
                Return True
            End If
            Dim sDA_DCRS As strDATA
            Dim sNU_DCRS, sTI_PESO_UNID As String
            Dim i, nNU_SECU As Integer
            Dim nIM_TOTA As Decimal
            Dim sCO_MONE As String

            nIM_TOTA = 0
            'calculamos lo montos de los items del DCR
            For i = 0 To arRE_DCRS.Count - 1
                Dim nNU_UNID_RETI, nNU_UNID_RECI, nNU_PESO_RETI, nNU_PESO_RECI, nNU_PESO_UNID, nIM_UNIT As Decimal
                Dim bOK_VALI As Boolean = True
                nIM_UNIT = 0
                sDA_DCRS = arRE_DCRS.Item(i)
                sNU_DCRS = sDA_DCRS.sNU_DCRS
                nNU_SECU = CInt(sDA_DCRS.sNU_SECU)

                'almacen seleccionado
                If sCO_ALMA_SELE = "" Then
                    sCO_ALMA_SELE = sDA_DCRS.sCO_ALMA
                Else
                    If sDA_DCRS.sCO_ALMA <> sCO_ALMA_SELE Then
                        'si la columnma de unidades retiradas es menor a cero                    
                        pr_IMPR_MENS("No es posible hacer retiros en diferentes Almacenes")
                        Return False
                    End If
                End If
                'unidades retiradas
                nNU_UNID_RETI = CType(sDA_DCRS.sUN_RETI, Decimal)
                'peso retirado
                nNU_PESO_RETI = CType(sDA_DCRS.sPE_RETI, Decimal)
                'unidades recibidas
                nNU_UNID_RECI = CType(sDA_DCRS.sUN_RECI, Decimal)
                'peso recibido
                nNU_PESO_RECI = CType(sDA_DCRS.sPE_RECI, Decimal)
                'numero peso unidades
                nNU_PESO_UNID = CType(sDA_DCRS.sNU_PESO_UNID, Decimal)
                'tipo peso unidades
                sTI_PESO_UNID = sDA_DCRS.sTI_PESO_UNID
                'monto unitario
                nIM_UNIT = CType(sDA_DCRS.sIM_UNIT, Decimal)
                'MONEDA
                sCO_MONE = sDA_DCRS.sCO_MONE
                ''--------- validacion de unidades retiradas ----------''
                If nNU_UNID_RETI <= 0 Then
                    'si la columnma de unidades retiradas es menor a cero                    
                    pr_IMPR_MENS("Cantidad Unidades Retiradas No V�lido, debe ser Menor o Igual a " & CStr(nNU_UNID_RECI) & " en C/R " & sNU_DCRS & ", secuencial " & nNU_SECU.ToString)
                    Return False
                ElseIf nNU_UNID_RETI > nNU_UNID_RECI Then
                    'si las unidades retiradas son mayores a las unidades de saldo 
                    pr_IMPR_MENS("Unidades Retiradas debe ser Menor o Igual a " & CStr(nNU_UNID_RECI) & " en C/R " & sNU_DCRS & ", secuencial " & nNU_SECU.ToString)
                    Return False
                Else
                    nNU_UNID_RETI = FormatNumber(nNU_UNID_RETI, 6)
                    'si la columna peso por unidad , campo nu_peso_unid del carga es mayor a cero
                    'y la columna sTI_PESO_UNID es 'B' entonces calcula el peso 
                    If (nNU_PESO_UNID > 0 And sTI_PESO_UNID = "B") Then
                        If nNU_UNID_RETI <> nNU_UNID_RECI Then
                            nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI * nNU_PESO_UNID, 6)
                        Else
                            nNU_PESO_RETI = nNU_PESO_RECI
                        End If
                    End If
                    If nNU_PESO_UNID > 0 And sTI_PESO_UNID = "U" Then
                        If nNU_UNID_RETI <> nNU_UNID_RECI Then
                            nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI / nNU_PESO_UNID, 6)
                        Else
                            nNU_PESO_RETI = nNU_PESO_RECI
                        End If
                    End If
                End If
                'monto total
                nIM_TOTA += CType(nNU_UNID_RETI, Decimal) * nIM_UNIT
            Next

            '--------------------------------------------------------------------
            'Dim dsTMMENS_VALI As DataSet = objSICO.fn_TMMENS_VALI_Q01(Session("CoEmpresa"))
            Dim sDE_MEN1, sDE_MEN2 As String
            'If dsTMMENS_VALI.Tables(0).Rows.Count > 0 Then
            '    sDE_MEN1 = IIf(IsDBNull(dsTMMENS_VALI.Tables(0).Rows(0)("DE_MEN1")), "", dsTMMENS_VALI.Tables(0).Rows(0)("DE_MEN1"))
            '    sDE_MEN2 = IIf(IsDBNull(dsTMMENS_VALI.Tables(0).Rows(0)("DE_MEN2")), "", dsTMMENS_VALI.Tables(0).Rows(0)("DE_MEN2"))
            'End If
            'Se evalua que el resultado de (deuda/saldo por cliente) no exceda un valor ingresado
            '**********************************************************************************

            'VALIDA UIT
            Dim drUIT As DataRow = objSICO.fn_VALUIT(sCO_MONE, nIM_TOTA, sNU_DCRS, "C").Rows(0)
            Dim sEST_UIT As String = drUIT("EST_UIT")
            Dim sVAL_UIT As Decimal = drUIT("VAL_UIT")

            If sEST_UIT = "N" Then
                pr_IMPR_MENS("EL Valor del retiro es menor a los 5 UIT(S/ " & CStr(sVAL_UIT) & " ), debe solicitar un monto mayor a los 5 UIT ")
                Return False
            End If


            Dim objWSSAP As wsSAP.Service
            objWSSAP = New wsSAP.Service
            Dim objDeuda As wsSAP.CREDIT_ACCOUNT()

            Dim nPorcentajeDeuda As String
            Dim sDeudaTotal As Decimal = 0
            Dim sMoneda As String = "DOL"
            Dim strCodSAP As String = Session("CodSAP")
            If strCodSAP = "" Then
                strCodSAP = "0"
            End If
            objDeuda = objWSSAP.ZBAPI_VECTOR_CRED_AS400(strCodSAP)
            nPorcentajeDeuda = objDeuda(0).AGOTAMIENTO
            sDeudaTotal = objDeuda(0).RCVBL_VALS
            sMoneda = objDeuda(0).CURRENCY

            If Trim(sMoneda) = "USD" Or sMoneda Is Nothing Then
                sMoneda = "DOL"
            End If
            If Trim(sMoneda) = "PEN" Then
                sMoneda = "SOL"
            End If

            'Dim sST_EJEC_AGOT As String = objSICO.fn_TMCLIE_MERC_Q04(Session("IdSicoDepositante"), nIM_TOTA)
            Dim drRow As DataRow = objSICO.fn_TMCLIE_MERC_Q04(Session("IdSicoDepositante"), nIM_TOTA).Rows(0)
            Dim sST_EJEC_AGOT As String = drRow("ST_VALI_AGOT")
            Dim sST_ANTI_DEUD As String = drRow("ST_ESTA_DEUD")
            Dim sST_ESTA As String = drRow("ST_ESTA")

            If sST_ESTA = "2" Then
                pr_IMPR_MENS("Por el momento el servicio de Ordenes de Retiro v�a Web se encuentra suspendido, por favor comun�quese con el �rea de Warrant, Tel�fono 518 6060 o al correo electr�nico warrants@almaperu.com.pe")
                Return False
            End If
            '-----------------------------------------------------------------------------
            Dim dt As New DataTable
            Dim OBJ = New WZAPI.ServiceSoapClient()
            Dim dia, mes, anio As String
            Dim vfecha As String
            Dim v30 As Decimal = "0"
            Dim v60 As Decimal = "0"
            Dim v90 As Decimal = "0"
            Dim vdvencida As Decimal = "0"
            Dim vpvencer As Decimal = "0"
            Dim DeudaTotal As Decimal = "0"
            anio = DateAndTime.Year(Today)
            mes = DateAndTime.Month(Today)
            dia = DateAndTime.Day(Today)
            vfecha = anio + mes.PadLeft(2, "0"c) + dia.PadLeft(2, "0"c)

            'Limpiamos Lista
            Dim Str As List(Of WZAPI.LINEITEMS) '= Nothing
            'Traer data del WS
            strCodSAP = "0000000000" & strCodSAP
            strCodSAP = Right(strCodSAP, 10)
            Str = OBJ.ZBAPI_AR_ACC_GETOPENITEMS(strCodSAP, vfecha)

            For Each sItem In Str
                If sItem.BSCHL = "01" Then
                    If Convert.ToInt32(sItem.DSCRDT) <= "0" Then
                        vpvencer = vpvencer + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                    ElseIf Convert.ToInt32(sItem.DSCRDT) > "0" And Convert.ToInt32(sItem.DSCRDT) <= "30" Then
                        v30 = v30 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                    ElseIf Convert.ToInt32(sItem.DSCRDT) > "30" And Convert.ToInt32(sItem.DSCRDT) <= "60" Then
                        v60 = v60 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                    ElseIf Convert.ToInt32(sItem.DSCRDT) > "60" Then
                        v90 = v90 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                    End If
                    If Convert.ToInt32(sItem.DSCRDT) > "0" Then
                        vdvencida = vdvencida + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                    End If
                    DeudaTotal = DeudaTotal + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                End If
            Next

            '--------------------------------------------------------------------------------------
            If nPorcentajeDeuda > 100 And sST_EJEC_AGOT = "S" Then
                sDE_MEN2 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Me.cboUnidad.SelectedValue, Me.cboTipoComprobante.SelectedValue,
                                                       Me.txtNroComprobante.Text, "0", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "LS", "DOL", DeudaTotal, vpvencer, v30, v60, v90)
                pr_IMPR_MENS("Cliente supero el l�mite de deuda configurado, coordinar con el �rea de Finanzas")
                Return False
            Else
                Dim sST_EJEC_RETI As String = objSICO.fn_TMCLIE_MERC_Q01(Session("CoEmpresa"), Me.cboUnidad.SelectedValue, Session("IdSicoDepositante"), sCO_MONE, nIM_TOTA, "DOL", DeudaTotal)
                If sST_EJEC_RETI = "S" Then
                    '--validacion por antiguedad
                    'Dim sFE_ACTU As String = FormatDateTime(Now(), DateFormat.ShortDate)
                    'Dim dsTCDOCU_CLIE As DataSet = objSICO.fn_TCDOCU_CLIE_Q08(Session("CoEmpresa"), Me.cboUnidad.SelectedValue,
                    '                                        sFE_ACTU, Session("IdSicoDepositante"))
                    If vdvencida > 0 And sST_ANTI_DEUD = "S" Then
                        sDE_MEN2 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Me.cboUnidad.SelectedValue, Me.cboTipoComprobante.SelectedValue,
                                                       Me.txtNroComprobante.Text, "2", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "LS", "DOL", DeudaTotal, vpvencer, v30, v60, v90)
                        pr_IMPR_MENS(sDE_MEN2)
                        Return False
                        'ElseIf dsTCDOCU_CLIE.Tables(0).Rows(0)("ST_ESTA") = 2 Then
                        '    pr_IMPR_MENS("Por el momento el servicio de Ordenes de Retiro v�a Web se encuentra suspendido, por favor comun�quese con el �rea de Warrant, Tel�fono 518 6060 o al correo electr�nico warrants@almaperu.com.pe")
                        '    Return False
                    Else
                        'insertamos  cada item del DCR en TWMERC_SELE
                        For i = 0 To arRE_DCRS.Count - 1
                            Dim nNU_UNID_RETI, nNU_UNID_RECI, nNU_PESO_RETI, nNU_PESO_RECI, nNU_PESO_UNID, nIM_UNIT As Decimal
                            sDA_DCRS = arRE_DCRS.Item(i)
                            sNU_DCRS = sDA_DCRS.sNU_DCRS
                            nNU_SECU = CInt(sDA_DCRS.sNU_SECU)
                            'unidades retiradas
                            nNU_UNID_RETI = CType(sDA_DCRS.sUN_RETI, Decimal)
                            'peso retirado
                            nNU_PESO_RETI = CType(sDA_DCRS.sPE_RETI, Decimal)
                            'unidades recibidas
                            nNU_UNID_RECI = CType(sDA_DCRS.sUN_RECI, Decimal)
                            'peso recibido
                            nNU_PESO_RECI = CType(sDA_DCRS.sPE_RECI, Decimal)
                            'numero peso unidades
                            nNU_PESO_UNID = CType(sDA_DCRS.sNU_PESO_UNID, Decimal)
                            'tipo peso unidades
                            sTI_PESO_UNID = sDA_DCRS.sTI_PESO_UNID
                            'monto unitario
                            nIM_UNIT = CType(sDA_DCRS.sIM_UNIT, Decimal)
                            ''----------------------------------------------------''
                            ''--calculo de unidades y pesos sin mensajes de validacion
                            nNU_UNID_RETI = FormatNumber(nNU_UNID_RETI, 6)
                            'si la columna peso por unidad , campo nu_peso_unid del carga es mayor a cero
                            'y la columna sTI_PESO_UNID es 'B' entonces calcula el peso 

                            If (nNU_PESO_UNID > 0 And sTI_PESO_UNID = "B") Then
                                If nNU_UNID_RETI <> nNU_UNID_RECI Then
                                    nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI * nNU_PESO_UNID, 6)
                                Else
                                    nNU_PESO_RETI = nNU_PESO_RECI
                                End If
                            End If
                            If nNU_PESO_UNID > 0 And sTI_PESO_UNID = "U" Then
                                If nNU_UNID_RETI <> nNU_UNID_RECI Then
                                    nNU_PESO_RETI = FormatNumber(nNU_UNID_RETI / nNU_PESO_UNID, 6)
                                Else
                                    nNU_PESO_RETI = nNU_PESO_RECI
                                End If
                            End If
                            'Si las unidades retiradas (que digitas por pantalla columna editable unidad) es mayor a la 
                            'variable que retorna el procedure o el peso retirado (columna editable bulto)es mayor a la 
                            'variable de peso, entonces debe emitir el mensaje de alerta : �Se est� efectuando un retiro 
                            'sobre este C / R, debe volver a Cargar Datos� y se cancelar� la generaci�n del retiro. 

                            If objSICO.fn_TCALMA_MERC_Q07(Session("CoEmpresa"), Me.cboUnidad.SelectedItem.Value,
                                                Me.cboTipoComprobante.SelectedItem.Value, sNU_DCRS, nNU_SECU,
                                                CInt(sDA_DCRS.sNU_SECU_UBIC), Me.hiST_ALMA_SUBI.Value, nNU_UNID_RETI, nNU_PESO_RETI) = True Then
                                'inserta los registros seleccionados en tabla fija TWMERC_SELE_Q01_1 para procesar
                                Dim nOK_INSE As Integer
                                nOK_INSE = objSICO.fn_TWMERC_SELE_I01(sNU_RECE_GRAB, Me.cboTipoComprobante.SelectedItem.Value, sNU_DCRS,
                                           nNU_SECU, CInt(sDA_DCRS.sNU_SECU_UBIC), sDA_DCRS.sCO_TIPO_BULT,
                                           sDA_DCRS.sCO_UNME_PESO, nNU_UNID_RETI, nNU_PESO_RETI, sDA_DCRS.sCO_MONE, sDA_DCRS.sCO_UNME_AREA,
                                           CType(sDA_DCRS.sNU_AREA_USAD, Decimal), sDA_DCRS.sCO_UNME_VOLU,
                                           CType(sDA_DCRS.sNU_VOLU_USAD, Decimal), nNU_UNID_RECI, nNU_PESO_RECI,
                                           sDA_DCRS.sCO_TIPO_MERC, sDA_DCRS.sDE_TIPO_MERC, sDA_DCRS.sCO_STIP_MERC,
                                           sDA_DCRS.sDE_STIP_MERC, sDA_DCRS.sDE_MERC, Session("UsuarioLogin"))
                                If Not nOK_INSE > 0 Then
                                    Return False
                                End If
                            Else
                                pr_IMPR_MENS("Se esta efectuando un retiro sobre el C/R " & sNU_DCRS & ", debe volver a Cargar Datos y se cancelara la generacion del retiro")
                                Return False
                            End If
                        Next
                        Return True
                    End If
                ElseIf sST_EJEC_RETI = "N" Then
                    sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Me.cboUnidad.SelectedValue, Me.cboTipoComprobante.SelectedValue,
                                             Me.txtNroComprobante.Text, "1", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "LS", "DOL", DeudaTotal, vpvencer, v30, v60, v90)
                    pr_IMPR_MENS(sDE_MEN1)
                    Return False
                ElseIf sST_EJEC_RETI = "X" Then
                    sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Me.cboUnidad.SelectedValue, Me.cboTipoComprobante.SelectedValue,
                                             Me.txtNroComprobante.Text, "9", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "LS", "DOL", DeudaTotal, vpvencer, v30, v60, v90)
                    pr_IMPR_MENS(sDE_MEN1)
                    Return False
                End If
                Return True
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String
            Response.Write(e1.Message)
            sCA_ERRO = e1.Message
        End Try
    End Function

    Private Function pr_GUAR_0001() As Boolean
        Try
            Me.lbMS_REGI.Text = ""
            ' almacen seleccionado
            'validar que el tipo de cambio del dia este ingresado:
            If objSICO.fn_TCFACT_CAMB_Q01(Session("CoEmpresa")) <> "" Then
                pr_IMPR_MENS(objSICO.fn_TCFACT_CAMB_Q01(Session("CoEmpresa")))
                Return False
                Exit Function
            End If

            Dim nNU_FILA As Integer
            Dim sNU_DCRS, sNU_DCRS_GRAB As String
            Dim arRE_DCRS As New ArrayList
            Dim sDA_DCRS As strDATA
            Dim bOK_SELE As Boolean = False
            sNU_DCRS_GRAB = ""
            For nNU_FILA = 0 To Me.dgTCALMA_MERC.Items.Count - 1


                Dim chST_MARC As CheckBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("chST_SELE"), CheckBox)
                If chST_MARC.Checked Then
                    Dim strImporte As String
                    Dim txtImporte As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("txtImporte"), TextBox)
                    strImporte = IIf(txtImporte.Text.Trim = "", 0, txtImporte.Text)
                    If strImporte <> "0" Then
                        CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_UNID_RETI"), TextBox).Text = strImporte / IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text)
                    End If
                    'selecciono al menos un registro
                    bOK_SELE = True
                    Dim nNU_UNID_RETI, nNU_PESO_RETI As Decimal
                    'si es diferente --> es nuevo DCR
                    If Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(2).Text <> sNU_DCRS Then
                        'procesa los detalles del DCR
                        If fn_PROC_DATO_DCRS(arRE_DCRS, sNU_DCRS_GRAB) = False Then
                            'si fallo la validacion de un item seleccionado, concluimos el procedimiento
                            Return False
                            Exit Function
                        End If
                        arRE_DCRS = New ArrayList
                    End If
                    'actualiza numero DCR
                    sNU_DCRS = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(2).Text
                    'graba el primer DCR
                    If sNU_DCRS_GRAB = "" Then
                        sNU_DCRS_GRAB = sNU_DCRS
                    End If
                    'almacen
                    sDA_DCRS.sCO_ALMA = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(1).Text
                    sDA_DCRS.sNU_DCRS = sNU_DCRS
                    'secuencial DCR
                    sDA_DCRS.sNU_SECU = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(3).Text
                    '----------------------------------------------------------------------------------
                    'unidades retiradas
                    Dim tbNU_UNID_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_UNID_RETI"), TextBox)
                    sDA_DCRS.sUN_RETI = IIf(tbNU_UNID_RETI.Text.Trim = "", 0, tbNU_UNID_RETI.Text)
                    'peso retirado
                    Dim tbNU_PESO_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_PESO_RETI"), TextBox)
                    sDA_DCRS.sPE_RETI = IIf(tbNU_PESO_RETI.Text.Trim = "", 0, tbNU_PESO_RETI.Text)
                    'unidades recibidas(CANTIDAD_UNIDAD)
                    sDA_DCRS.sUN_RECI = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text)
                    'peso recibido(CANTIDAD_BULTO)
                    sDA_DCRS.sPE_RECI = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text)
                    '----------------------------------------------------------------------------------
                    'numero peso unidades
                    sDA_DCRS.sNU_PESO_UNID = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(18).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(18).Text)
                    'tipo peso unidades
                    sDA_DCRS.sTI_PESO_UNID = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(19).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(19).Text)
                    'monto unitario
                    sDA_DCRS.sIM_UNIT = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text)
                    'MONEDA
                    sDA_DCRS.sCO_MONE = IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(30).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(30).Text)

                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(4).Text = "&nbsp;" Then sDA_DCRS.sDE_MERC = "" Else _
                                sDA_DCRS.sDE_MERC = dgTCALMA_MERC.Items(nNU_FILA).Cells(4).Text
                    If Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(5).Text = "&nbsp;" Then sDA_DCRS.sCO_TIPO_BULT = "" Else _
                                 sDA_DCRS.sCO_TIPO_BULT = dgTCALMA_MERC.Items(nNU_FILA).Cells(5).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(7).Text = "&nbsp;" Then sDA_DCRS.sCO_UNME_PESO = "" Else _
                             sDA_DCRS.sCO_UNME_PESO = dgTCALMA_MERC.Items(nNU_FILA).Cells(7).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(21).Text = "&nbsp;" Then sDA_DCRS.sCO_UNME_AREA = "" Else _
                            sDA_DCRS.sCO_UNME_AREA = dgTCALMA_MERC.Items(nNU_FILA).Cells(21).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(22).Text = "&nbsp;" Then sDA_DCRS.sNU_AREA_USAD = 0 Else _
                           sDA_DCRS.sNU_AREA_USAD = dgTCALMA_MERC.Items(nNU_FILA).Cells(22).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(23).Text = "&nbsp;" Then sDA_DCRS.sCO_UNME_VOLU = "" Else _
                            sDA_DCRS.sCO_UNME_VOLU = dgTCALMA_MERC.Items(nNU_FILA).Cells(23).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(24).Text = "&nbsp;" Then sDA_DCRS.sNU_VOLU_USAD = 0 Else _
                            sDA_DCRS.sNU_VOLU_USAD = dgTCALMA_MERC.Items(nNU_FILA).Cells(24).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(25).Text = "&nbsp;" Then sDA_DCRS.sCO_TIPO_MERC = "" Else _
                            sDA_DCRS.sCO_TIPO_MERC = dgTCALMA_MERC.Items(nNU_FILA).Cells(25).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(26).Text = "&nbsp;" Then sDA_DCRS.sDE_TIPO_MERC = "" Else _
                            sDA_DCRS.sDE_TIPO_MERC = dgTCALMA_MERC.Items(nNU_FILA).Cells(26).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(27).Text = "&nbsp;" Then sDA_DCRS.sCO_STIP_MERC = "" Else _
                            sDA_DCRS.sCO_STIP_MERC = dgTCALMA_MERC.Items(nNU_FILA).Cells(27).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(28).Text = "&nbsp;" Then sDA_DCRS.sDE_STIP_MERC = "" Else _
                            sDA_DCRS.sDE_STIP_MERC = dgTCALMA_MERC.Items(nNU_FILA).Cells(28).Text
                    If dgTCALMA_MERC.Items(nNU_FILA).Cells(29).Text = "&nbsp;" Then sDA_DCRS.sNU_SECU_UBIC = 0 Else _
                            sDA_DCRS.sNU_SECU_UBIC = dgTCALMA_MERC.Items(nNU_FILA).Cells(29).Text
                    arRE_DCRS.Add(sDA_DCRS)
                End If
            Next

            'si no se selecciono ningun registro
            If bOK_SELE = False Then
                pr_IMPR_MENS("Debe seleccionar al menos un registro")
                Return False
                Exit Function
            End If
            'procesa los datos del ultimo DCR
            If fn_PROC_DATO_DCRS(arRE_DCRS, sNU_DCRS_GRAB) = False Then
                Return False
                Exit Function
            End If
            'si todos los detalles se insertaron correctamente, graba cabecera de retiro
            Dim sNU_DOCU_GRAB As String
            'deshabilitacion de fecha de recojo
            Dim sDE_OBSE_NUEV As String = Me.txtObservacion.Text.ToUpper
            If sDE_OBSE_NUEV.Length > 400 Then
                sDE_OBSE_NUEV = sDE_OBSE_NUEV.Substring(0, 399)
            End If
            Dim sCO_USUA_FIR1 As String = IIf(Session("sCO_USUA_FIR1") Is Nothing, "", Session("sCO_USUA_FIR1"))
            Dim sCO_USUA_FIR2 As String = IIf(Session("sCO_USUA_FIR2") Is Nothing, "", Session("sCO_USUA_FIR2"))
            Dim strEmbarque As String
            If Me.chkEmbarque.Checked = True Then
                strEmbarque = "S"
            Else
                strEmbarque = "N"
            End If
            'Generaci�n orden retiro (web)
            sNU_DOCU_GRAB = objSICO.fn_TCRETI_MERC_I01(Session("CoEmpresa"), Me.cboUnidad.SelectedItem.Value,
                            Me.cboTipoComprobante.SelectedItem.Value, sNU_DCRS_GRAB, Me.hiST_ALMA_SUBI.Value, Me.cboTipoRetiro.SelectedItem.Value,
                            Me.txtNroRetiro.Text, sDA_DCRS.sCO_ALMA, sDA_DCRS.sCO_MONE, Session("UsuarioLogin"),
                            "", "", "", sDE_OBSE_NUEV, sNU_DCRS_GRAB, sCO_USUA_FIR1, sCO_USUA_FIR2, strEmbarque)
            'asigna numero de documento grabado
            Me.lbNU_DOCU_GRAB.Text = sNU_DOCU_GRAB
            If sNU_DOCU_GRAB <> Me.txtNroRetiro.Text Then
                Me.txtNroRetiro.Text = sNU_DOCU_GRAB
                Session.Add("NroRetiro", sNU_DOCU_GRAB)
            Else
                pr_IMPR_MENS("Se Gener� la Orden de Retiro satisfactoriamente")
            End If
            Return True
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
            Return False
            Me.lbMS_REGI.Text = "En este momento no se puede realizar el proceso, vuelva a intentar"
        End Try
    End Function

    '    Private Sub Emitir()
    '        objAccesoWeb = New clsCNAccesosWeb
    '        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
    '                                Session.Item("NombreEntidad"), "C", "19", "EMITIR LIBERACION", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)


    '        'pr_GUAR_0001()  = inserta  en TC_RETIMERC , TD_RETI_MERC , TD_RETI_UBIC y TDRETI_PRTE
    '        'pr_GUAR_0001()  = actualiaza en  TMALMA , TCRETI_MERC , TCALMA_MERC
    '        If pr_GUAR_0001() Then
    '            Dim dtMail As New DataTable
    '            'Dim strEmails As String
    '            Dim strNombreArchivoPDF As String
    '            Dim strNombreCliente As String
    '            Dim strResult As String = ""
    '            Dim blnEmbarque As Boolean
    '            Dim strTipoDocumento As String
    '            Dim strEmailsConAdjunto As String = ""
    '            Dim strEmailsSinAdjunto As String = ""
    '            Dim objConexion As SqlConnection
    '            Dim objTrans As SqlTransaction
    '            objConexion = New SqlConnection(strConn)
    '            objConexion.Open()
    '            objTrans = objConexion.BeginTransaction()
    '            Try


    '                '' inserta la cabecera en WFLIBERACION y actualiza TCRETI_MERC
    '                'If objDocumento.gInsLiberacion(Me.cboUnidad.SelectedValue, Me.txtNroWarrant.Text,
    '                'Me.cboTipoTitulo.SelectedValue, Session.Item("IdSicoDepositante"), Session.Item("CodEntiFina"),
    '                'Me.cboTipoRetiro.SelectedValue, Me.txtNroRetiro.Text, Me.txtFecha.Text, Session.Item("IdUsuario"),
    '                'Me.cboModalidad.SelectedValue, Me.cboMoneda.SelectedValue, Me.cboTipoComprobante.SelectedValue,
    '                'Me.txtNroComprobante.Text, Me.txtAlmacen.Text, Me.txtObservacion.Text, "", "", Me.txtMercaderia.Value,
    '                'Session.Item("Virtual"), Me.chkEmbarque.Checked, False, False, Me.chkWarrCustodia.Checked, "", objTrans) = 1 Then
    '                '    pr_IMPR_MENS("El n�mero de retiro ya existe")
    '                '    objTrans.Rollback()
    '                '    objTrans.Dispose() 
    '                '    Exit Sub
    '                'End If

    '                Dim strCodAlmaDest As String
    '                strCodAlmaDest = Me.cboUnidad.SelectedValue

    '                Dim dt As New DataTable
    '                dt = objDocumento.gGetWarrantLiberaciones(Session.Item("NroDoc"), "0", "0", "", Session.Item("IdSico"), "N", "0")

    '                If dt.Rows(0).Item("ST_ENDO_EMBA").ToString() = "SI" Then
    '                    blnEmbarque = True
    '                Else
    '                    blnEmbarque = False
    '                End If
    '                If dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "&nbsp;" Or dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "" Then
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                    pr_IMPR_MENS("Warrant no negociado")
    '                    Exit Sub
    '                End If

    '                If objDocumento.gInsLiberacionContable(dt.Rows(0).Item("CO_UNID").ToString(), dt.Rows(0).Item("NU_TITU").ToString(),
    '                    dt.Rows(0).Item("TI_TITU").ToString(), Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(),
    '                    "DOR", "", dt.Rows(0).Item("FE_VIGE_BANC").ToString(), Session.Item("IdUsuario"),
    '                    dt.Rows(0).Item("CO_MODA_MOVI").ToString(), dt.Rows(0).Item("co_mone").ToString(), dt.Rows(0).Item("TI_DOCU_RECE").ToString(),
    '                    dt.Rows(0).Item("NU_DOCU_RECE").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", "", "", dt.Rows(0).Item("DE_MERC_GENE").ToString(),
    '                    dt.Rows(0).Item("virtual").ToString(), blnEmbarque, strCodAlmaDest, objTrans) = 1 Then
    '                    pr_IMPR_MENS("El n�mero de warrant ya tiene una Solicitud pendiente")
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                End If



    '                '============== ok ======================


    '                ' inserta en los items en WFITEMLIBERACION
    '                '------ cambiar ------------
    '                If InsertarItemLiberacion(Me.txtNroRetiro.Text, objTrans) = False Then
    '                    pr_IMPR_MENS("El retiro no pudo registrarse")
    '                    objTrans.Rollback()
    '                    objTrans.Dispose()
    '                    Exit Sub

    '                Else
    '                    'paso
    '                    objTrans.Commit()
    '                    objTrans.Dispose()
    '                    Dim dtDetalle As New DataTable
    '                    strNombreArchivoPDF = strPath & "02" & Me.txtNroWarrant.Text & Me.txtNroRetiro.Text & ".pdf"
    '                    dtDetalle = objDocumento.gGetDetalleLiberacion(strCoEmpresa, Me.cboUnidad.SelectedValue, Me.txtNroRetiro.Text,
    '                    Session.Item("IdTipoDocumento"), Me.txtNroWarrant.Text)

    '                    ' select en WFITEMLIBERACION , WFLOGDOCUMENTO ,etc
    '                    strNombreCliente = objLiberacion.GetReporteLiberacion(strNombreArchivoPDF, strCoEmpresa, Me.cboUnidad.SelectedValue,
    '                    "DOR", Me.txtNroRetiro.Text, Session.Item("IdSicoDepositante"), dtDetalle, Me.cboTipoTitulo.SelectedValue, Me.chkEmbarque.Checked, False, False, strPathFirmas)

    '                    If Session("LibMultiple") = True And Me.chkWarrCustodia.Checked = True Then
    '                        GoTo salirCustodia
    '                    Else
    '                        If Session("Pendiente") = False Then 'And Me.chkWarrCustodia.Checked = False Then
    '                            strResult = objDocumento.gInsLiberacionFlujo(Me.txtNroWarrant.Text, Me.txtNroRetiro.Text, Session.Item("IdTipoDocumento"),
    '                            Session.Item("IdUsuario"), Me.cboUnidad.SelectedValue)
    '                        End If
    '                    End If
    'salirCustodia:
    '                    If strResult = "" Then
    '                        dtMail = objUsuario.gGetEmailAprobadores("00000001", Me.hiCO_ALMA.Value, "02")
    '                    Else
    '                        dtMail = objDocumento.gEnviarMail(Me.txtNroWarrant.Text, Me.txtNroRetiro.Text, Now.Year.ToString, strResult.Substring(0, 2), "S")
    '                    End If






    '                    '----------------------------------------------



    '                    Dim strFlgDblEndoso As String
    '                        Dim strTipoTitulo As String
    '                        If dt.Rows(0).Item("FLG_DENDO").ToString() = "S" Then
    '                            strFlgDblEndoso = 1
    '                        Else
    '                            strFlgDblEndoso = 0
    '                        End If
    '                        If dt.Rows(0).Item("TI_TITU").ToString() = "WAR" Then
    '                            strTipoTitulo = 3
    '                        End If
    '                        If dt.Rows(0).Item("TI_TITU").ToString() = "CER" Then
    '                            strTipoTitulo = 1
    '                        End If
    '                        If dt.Rows(0).Item("TI_TITU").ToString() = "WIP" Then
    '                            strTipoTitulo = 3
    '                        End If

    '                        objDocumento.SetGrabarSolicitudWarrantCabecera("", 0, "24", dt.Rows(0).Item("CO_MODA_MOVI").ToString(), strFlgDblEndoso, dt.Rows(0).Item("TI_ALMA").ToString(), "",
    '                       "0", strTipoTitulo, dt.Rows(0).Item("CO_SEGU_ENDO").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", dt.Rows(0).Item("co_mone").ToString(),
    '                       Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(), "", Session.Item("IdUsuario"), dt.Rows(0).Item("NU_TITU").ToString(), objTrans)

    '                        objTrans.Commit()
    '                        objTrans.Dispose()

    '                        dtMail = objUsuario.gGetEmailAprobadores("00000001", dt.Rows(0).Item("CO_ALMA").ToString(), "01")
    '                        If dtMail.Rows.Count > 0 Then
    '                            For i As Integer = 0 To dtMail.Rows.Count - 1
    '                                If dtMail.Rows(i)("EMAI") <> "" And dtMail.Rows(i)("FLG_ADJU") = "1" Then
    '                                    strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
    '                                End If
    '                            Next

    '                            If objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
    '                    strEmails.Remove(strEmails.Length - 1, 1),
    '                    "Solicitud de Liberaci�n para emisi�n de nuevo Warrant - " & Session.Item("NombreEntidad"),
    '                    "Estimados Se�ores:<br><br>" &
    '                    "El cliente ha solicitado una Liberaci�n contable" &
    '                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("NU_TITU").ToString() & "</FONT></STRONG><br>" &
    '                    "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG><br>" &
    '                    "Direcci�n de Almac�n: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("DE_DIRE_ALMA").ToString() & "</FONT></STRONG><br>" &
    '                    "<STRONG><FONT color='#330099'>Agradecemos enviar Inventario F�sico para reemplazo de Warrant al �rea de Operaciones</FONT></STRONG>" &
    '                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "") = False Then
    '                                pr_IMPR_MENS("No se pudo enviar el correo de notificaci�n")
    '                            End If

    '                        End If
    '                        dtMail.Dispose()
    '                        dtMail = Nothing
    '                        pr_IMPR_MENS("Se ha solicitado la Liberaci�n para emisi�n de nuevo Warrant Nro: " & dt.Rows(0).Item("NU_TITU").ToString())


    '                '------------------------------------------------------------
    '            Catch ex As Exception
    '                Me.lblError.Text = "ERROR " + ex.Message
    '            End Try
    '        End If
    '    End Sub




    Function InsertarItemLiberacion(ByVal strNroRetiro As String, ByVal ObjTrans As SqlTransaction) As Boolean
        Dim nNU_FILA As Integer
        Dim strAlmacen As String
        Dim strItem As String
        Dim strCntRetirada As Decimal
        Dim strBultoRetirado As Decimal
        Dim strCntDisponible As Decimal
        Dim strCntBulto As Decimal
        Dim strMercaderia As String
        Dim strUnidad As String
        Dim strBulto As String
        Dim strFechaFact As String
        Dim strFechaIngr As String
        Dim strBodega As String
        Dim strUbicacion As String
        Dim strPrecioUnit As Decimal

        For nNU_FILA = 0 To Me.dgTCALMA_MERC.Items.Count - 1
            Dim chST_MARC As CheckBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("chST_SELE"), CheckBox)
            If chST_MARC.Checked Then
                'almacen
                strAlmacen = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(1).Text
                'secuencial Item
                strItem = Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(3).Text
                'unidades retiradas
                Dim tbNU_UNID_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_UNID_RETI"), TextBox)
                strCntRetirada = CType(IIf(tbNU_UNID_RETI.Text.Trim = "", 0, tbNU_UNID_RETI.Text), Decimal)
                'peso retirado
                Dim tbNU_PESO_RETI As TextBox = CType(Me.dgTCALMA_MERC.Items(nNU_FILA).FindControl("tbNU_PESO_RETI"), TextBox)
                strBultoRetirado = CType(IIf(tbNU_PESO_RETI.Text.Trim = "", 0, tbNU_PESO_RETI.Text), Decimal)
                'Cantidad unidad
                strCntDisponible = CType(IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(6).Text), Decimal)
                'Cantidad Bulto
                strCntBulto = CType(IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(8).Text), Decimal)
                'Mercaderia
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(4).Text = "&nbsp;" Then strMercaderia = "" Else _
                            strMercaderia = dgTCALMA_MERC.Items(nNU_FILA).Cells(4).Text
                'Unidad
                If Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(5).Text = "&nbsp;" Then strUnidad = "" Else _
                             strUnidad = dgTCALMA_MERC.Items(nNU_FILA).Cells(5).Text
                'Bulto
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(7).Text = "&nbsp;" Then strBulto = "" Else _
                         strBulto = dgTCALMA_MERC.Items(nNU_FILA).Cells(7).Text
                'Fecha Fact
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(13).Text = "&nbsp;" Then strFechaFact = "" Else _
                        strFechaFact = dgTCALMA_MERC.Items(nNU_FILA).Cells(13).Text
                'Fecha Ingr.
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(14).Text = "&nbsp;" Then strFechaIngr = "" Else _
                        strFechaIngr = dgTCALMA_MERC.Items(nNU_FILA).Cells(14).Text
                'Bodega.
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(16).Text = "&nbsp;" Then strBodega = "" Else _
                        strBodega = dgTCALMA_MERC.Items(nNU_FILA).Cells(16).Text

                'Ubicacion.
                If dgTCALMA_MERC.Items(nNU_FILA).Cells(17).Text = "&nbsp;" Then strUbicacion = "" Else _
                        strUbicacion = dgTCALMA_MERC.Items(nNU_FILA).Cells(17).Text
                'Precio Unit.
                strPrecioUnit = CType(IIf(Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text.Trim = "&nbsp;", 0, Me.dgTCALMA_MERC.Items(nNU_FILA).Cells(20).Text), Decimal)
                If objDocumento.gInsDetalleLiberacion("01", Me.cboUnidad.SelectedValue, Me.cboTipoTitulo.SelectedValue, Me.txtNroRetiro.Text, strItem,
                strAlmacen, strBulto, strCntBulto, strMercaderia, strUnidad, strCntDisponible, strCntRetirada, strBultoRetirado, strFechaFact,
                strFechaIngr, strBodega & "-" & strUbicacion, strPrecioUnit, Session.Item("IdUsuario"), ObjTrans) = 1 Then
                    Return False
                    Exit Function
                End If
            End If
        Next
        Return True
    End Function

#End Region

#Region "*****************************  Antiguo boton solicitar *********************************"

    Public Sub Contable()

        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "21", "SOLICITAR NUEVO WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim dtMail As New DataTable
        Dim strEmails As String
        Dim blnEmbarque As Boolean
        Dim strNombreArchivoPDF As String
        Dim NombreReporte As String
        Dim strCodAlmaDest As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try

            Dim dt As New DataTable
            dt = objDocumento.gGetWarrantLiberaciones(Session.Item("NroDoc"), "0", "0", "", Session.Item("IdSico"), "N", "0")

            strCodAlmaDest = Me.cboUnidad.SelectedValue

            If dt.Rows(0).Item("ST_ENDO_EMBA").ToString() = "SI" Then
                blnEmbarque = True
            Else
                blnEmbarque = False
            End If
            If dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "&nbsp;" Or dt.Rows(0).Item("DE_CORT_ENFI").ToString() = "" Then
                objTrans.Rollback()
                objTrans.Dispose()
                pr_IMPR_MENS("Warrant no negociado")
                Exit Sub
            End If
            '-- cambiar():
            If objDocumento.gInsLiberacionContable(dt.Rows(0).Item("CO_UNID").ToString(), dt.Rows(0).Item("NU_TITU").ToString(),
            dt.Rows(0).Item("TI_TITU").ToString(), Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(),
            "DOR", "", dt.Rows(0).Item("FE_VIGE_BANC").ToString(), Session.Item("IdUsuario"),
             dt.Rows(0).Item("CO_MODA_MOVI").ToString(), dt.Rows(0).Item("co_mone").ToString(), dt.Rows(0).Item("TI_DOCU_RECE").ToString(),
              dt.Rows(0).Item("NU_DOCU_RECE").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", "", "", dt.Rows(0).Item("DE_MERC_GENE").ToString(),
              dt.Rows(0).Item("virtual").ToString(), blnEmbarque, strCodAlmaDest, objTrans) = 1 Then

                pr_IMPR_MENS("El n�mero de warrant ya tiene una Solicitud pendiente")

                objTrans.Rollback()
                objTrans.Dispose()

            Else
                Dim strFlgDblEndoso As String
                Dim strTipoTitulo As String
                If dt.Rows(0).Item("FLG_DENDO").ToString() = "S" Then
                    strFlgDblEndoso = 1
                Else
                    strFlgDblEndoso = 0
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "WAR" Then
                    strTipoTitulo = 3
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "CER" Then
                    strTipoTitulo = 1
                End If
                If dt.Rows(0).Item("TI_TITU").ToString() = "WIP" Then
                    strTipoTitulo = 3
                End If

                objDocumento.SetGrabarSolicitudWarrantCabecera("", 0, "24", dt.Rows(0).Item("CO_MODA_MOVI").ToString(), strFlgDblEndoso, dt.Rows(0).Item("TI_ALMA").ToString(), "",
                       "0", strTipoTitulo, dt.Rows(0).Item("CO_SEGU_ENDO").ToString(), dt.Rows(0).Item("CO_ALMA").ToString(), "", dt.Rows(0).Item("co_mone").ToString(),
                       Session.Item("IdSico"), dt.Rows(0).Item("CO_ENTI_FINA").ToString(), "", Session.Item("IdUsuario"), dt.Rows(0).Item("NU_TITU").ToString(), objTrans)

                objTrans.Commit()
                objTrans.Dispose()

                dtMail = objUsuario.gGetEmailAprobadores("00000001", dt.Rows(0).Item("CO_ALMA").ToString(), "01")
                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" And dtMail.Rows(i)("FLG_ADJU") = "1" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next

                    If objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                    strEmails.Remove(strEmails.Length - 1, 1),
                    "Solicitud de Liberaci�n para emisi�n de nuevo Warrant - " & Session.Item("NombreEntidad"),
                    "Estimados Se�ores:<br><br>" &
                    "El cliente ha solicitado una Liberaci�n contable" &
                    ":<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("NU_TITU").ToString() & "</FONT></STRONG><br>" &
                    "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG><br>" &
                    "Direcci�n de Almac�n: <STRONG><FONT color='#330099'>" & dt.Rows(0).Item("DE_DIRE_ALMA").ToString() & "</FONT></STRONG><br>" &
                    "<STRONG><FONT color='#330099'>Agradecemos enviar Inventario F�sico para reemplazo de Warrant al �rea de Operaciones</FONT></STRONG>" &
                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "") = False Then
                        pr_IMPR_MENS("No se pudo enviar el correo de notificaci�n")
                    End If

                End If
                dtMail.Dispose()
                dtMail = Nothing
                pr_IMPR_MENS("Se ha solicitado la Liberaci�n para emisi�n de nuevo Warrant Nro: " & dt.Rows(0).Item("NU_TITU").ToString())
            End If

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objLiberacion Is Nothing) Then
            objLiberacion = Nothing
        End If
        If Not (objSICO Is Nothing) Then
            objSICO = Nothing
        End If
        MyBase.Finalize()
    End Sub
    Protected Sub chkTraslado_CheckedChanged(sender As Object, e As EventArgs) Handles chkTraslado.CheckedChanged
        If chkTraslado.Checked = True Then
            Me.pnlDestino.Visible = True
        Else
            Me.pnlDestino.Visible = False
        End If
    End Sub


End Class
