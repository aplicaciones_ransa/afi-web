Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class ConsultasxRequerimientoTemp
    Inherits System.Web.UI.Page
    Private lstrNumRequ As String
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents txtNroRequerimiento As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
            Me.lblError.Text = ""
            Try
                If Not IsPostBack Then
                    txtNroRequerimiento.Text = CStr(Session.Item("CodRequerimiento"))
                    LlenaGrid()
                    objAccesoWeb = New clsCNAccesosWeb
                    objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                            Session.Item("NombreEntidad"), "C", "DEPSAFIL", "CONSULTA DE REQUERIMIENTO TEMPORAL", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim itemType As ListItemType = CType(e.Item.ItemType, ListItemType)
        If itemType <> ListItemType.Footer And itemType <> ListItemType.Separator Then
            If itemType = ListItemType.Header Then
            Else
                Dim imgEliminar As ImageButton
                imgEliminar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
                imgEliminar.Attributes.Add("onClick", "return MsgEliminar(" & e.Item.DataItem("NU_CONS") & ");")
            End If
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        EliminarConsulta(sender)
        LlenaGrid()
    End Sub

    Private Sub dgdResultado_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub LlenaGrid()
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            objFuncion = New LibCapaNegocio.clsFunciones
            objFuncion.gCargaGrid(dgdResultado, objRequ.gdtConseguirDetalleTemp(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodRequerimiento"))))
            lblRegistros.Text = CStr(objRequ.intFilasAfectadas)
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    'Private Sub EliminarConsulta(ByVal objSender As Object)
    '    Try
    '        Dim imgEliminar As ImageButton = CType(objSender, ImageButton)
    '        Dim dgItem As DataGridItem
    '        Dim j As Integer = 0
    '        dgItem = CType(imgEliminar.Parent.Parent, DataGridItem)
    '        objConsulta = New LibCapaNegocio.clsCNConsulta
    '        If objConsulta.gbolEliminaConsulta(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodRequerimiento")), CInt(dgItem.Cells(0).Text)) Then
    '            Me.lblError.Text = "La consulta nro: " + dgItem.Cells(0).Text + " fue eliminado exitosamente!"
    '        Else
    '            Me.lblError.Text = "ERROR en el procedimiento de eliminación."
    '        End If
    '    Catch ex As Exception
    '        Me.lblError.Text = "ERROR " + ex.Message
    '    End Try
    'End Sub

    Private Sub EliminarConsulta(ByVal objSender As Object)
        Try
            Dim imgEliminar As ImageButton = CType(objSender, ImageButton)
            Dim dgItem As DataGridItem
            Dim j As Integer = 0
            Dim intNroConsultas As Integer = CInt(Session.Item("NroConsultas"))
            dgItem = CType(imgEliminar.Parent.Parent, DataGridItem)
            objConsulta = New LibCapaNegocio.clsCNConsulta

            'PENDIENTE MODIFICAR AURIS
            If objConsulta.gbolEliminaConsulta(CStr(Session.Item("CodCliente")), CStr(Session.Item("RequerimientoActivo")), CInt(dgItem.Cells(0).Text), dgItem.Cells(6).Text) Then
                Session.Add("NroConsultas", intNroConsultas - 1)
                RemoverSeleccionado("SCAJ" & dgItem.Cells(2).Text)
                Me.lblError.Text = "La consulta nro: " + dgItem.Cells(0).Text + " fue eliminado exitosamente!"
            Else
                Me.lblError.Text = "ERROR en el procedimiento de eliminación."
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect(Session.Item("PaginaAnterior"))
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub RemoverSeleccionado(ByVal pstrItem As String)
        Dim strSeleccionado As String = Session.Item("Seleccionados").ToString
        strSeleccionado = strSeleccionado.Replace(pstrItem, "")
        Session.Add("Seleccionados", strSeleccionado)
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub


End Class
