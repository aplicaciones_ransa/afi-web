<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Mapa.aspx.vb" Inherits="Mapa" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Mapa</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" rel="stylesheet">
		<script language="Javascript">
		function abrir(co_alma, co_bode, co_ubic, moda, mode, color, chasis){
			window.open('Popup/DetalleUbicacion.aspx?co_alma=' + co_alma + '&co_bode=' + co_bode + '&co_ubic=' + co_ubic + '&moda=' + moda+ '&mode=' + mode+ '&color=' + color+ '&chasis=' + chasis,'Ubicación','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=460,height=300');
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:Header id="Header1" runat="server"></uc1:Header>
						<TABLE id="Table11" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table12" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%">
												<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="5">
												<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="670" align="center" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="text">Almacen</TD>
																	<TD class="Text">:</TD>
																	<TD width="74%" colSpan="4">
																		<asp:dropdownlist id="cboAlmacenes" runat="server" CssClass="text" Width="540px" AutoPostBack="True"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD class="text" width="10%">Modalidad</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD width="39%">
																		<asp:dropdownlist id="cboModalidad" runat="server" CssClass="text" Width="220px"></asp:dropdownlist></TD>
																	<TD class="text" width="9%">Nro Chasis</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD width="40%">
																		<asp:textbox id="txtNroChasis" runat="server" CssClass="text" Width="100px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text">Modelo Auto</TD>
																	<TD class="Text">:</TD>
																	<TD>
																		<asp:dropdownlist id="cboModelo" runat="server" CssClass="text" Width="220px"></asp:dropdownlist></TD>
																	<TD class="text">Color Auto</TD>
																	<TD class="Text">:</TD>
																	<TD>
																		<asp:dropdownlist id="cboColor" runat="server" CssClass="text" Width="220px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD class="text">Bod. o Loza</TD>
																	<TD class="Text">:</TD>
																	<TD colSpan="4">
																		<asp:datalist id="dtlBodega" runat="server" CssClass="text" RepeatColumns="7" RepeatDirection="Horizontal"
																			BorderColor="#CCCCCC" BorderStyle="None" BackColor="#F0F0F0" CellPadding="3" GridLines="Both"
																			BorderWidth="1px">
																			<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
																			<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
																			<ItemStyle ForeColor="#000066"></ItemStyle>
																			<ItemTemplate>
																				<asp:Button id="btnBodega" CssClass="btn" runat="server" Width=40 Text='<%#Container.DataItem("co_bode")%>' ToolTip='<%#Container.DataItem("de_bode")%>' OnClick="btnBodega_Click">
																				</asp:Button>
																				<!--<INPUT type="hidden" value='<%#Container.DataItem("co_bode")%>' runat=server id="hdBodega">-->
																			</ItemTemplate>
																			<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
																		</asp:datalist></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<asp:label id="lblTitulo" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top">
												<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">--> &nbsp;
															<asp:literal id="ltrUbicaciones" runat="server"></asp:literal><!--</div>--></TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top">
												<asp:label id="lblTotal" runat="server" CssClass="Subtitulo"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top">
												<asp:label id="lblError" runat="server" CssClass="error"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:Footer id="Footer1" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
