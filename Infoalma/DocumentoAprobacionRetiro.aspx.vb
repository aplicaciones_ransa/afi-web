Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class DocumentoAprobacionRetiro
    Inherits System.Web.UI.Page
    Private objRetiro As New clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objWMS As clsCNWMS
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipRetiro As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroPedido As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        pr_VALI_SESI()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            Try
                Me.lblOpcion.Text = "Retiros en L�nea"
                If Page.IsPostBack = False Then
                    LlenarAlmacenes()
                    inicializa()
                    If Session.Item("Co_AlmaS") = "" Then
                        Session.Item("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session.Item("Co_AlmaS")
                    End If
                    If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaS"))) > 0 Then
                        Session.Item("Co_AlmaW") = Session.Item("Co_AlmaS")
                        Response.Redirect("DocumentoAprobacionRetiro_W.aspx?Estado=" + Session.Item("EstadoDoc"), False)
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub inicializa()
        If Request.Item("Estado") <> "" Then
            Session.Add("EstadoDoc", Request.Item("Estado"))
        End If
        Dim strResult As String()
        Select Case Session.Item("EstadoDoc") 'Request.Item("Estado") 
            'Case "01"
            '    Me.lblOpcion.Text = "Consultar Documentos Nuevos"
            Case "02"
                Me.lblOpcion.Text = "Consultar Documentos Emitidos"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_EMITIDO"), "-")
            Case "04"
                Me.lblOpcion.Text = "Consultar Documentos Registrados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_REGISTRADO"), "-")
            Case "06"
                Me.lblOpcion.Text = "Consultar Documentos Rechazados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_RECHAZADO"), "-")
        End Select

        Session.Item("Page") = strResult(0)
        Session.Item("Opcion") = strResult(1)
        Dim dttFecha As DateTime
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        If Me.txtFechaInicial.Value = "" Then
            Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        End If
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        If Me.txtFechaFinal.Value = "" Then
            Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        End If

        Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
        If Session.Item("strNroRetiro") <> Nothing Then
            pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("strNroRetiro"))
            Session.Remove("strNroRetiro")
        End If
        InicializaBusqueda()
        Bindatagrid()
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dt As DataTable
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objRetiro.gCNGetBuscarRetiros(Session("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdTipoEntidad"), _
                                           Me.txtNroPedido.Text, FechaInicial, FechaFinal, Me.cboTipRetiro.SelectedValue, _
                                           Session.Item("EstadoDoc"), Me.cboAlmacen.SelectedValue)
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Session.Add("dtMovimiento", dt)
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaRetiro") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaRetiro").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.cboTipRetiro.SelectedValue = strFiltros(0)
            Me.txtNroPedido.Text = strFiltros(1)
            txtFechaInicial.Value = strFiltros(2)
            txtFechaFinal.Value = strFiltros(3)
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaRetiro", Me.cboTipRetiro.SelectedValue & "-" & _
          Me.txtNroPedido.Text & "-" & txtFechaInicial.Value & "-" & txtFechaFinal.Value)
    End Sub

    Public Sub btnRechazar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim imgEditar As Button = CType(sender, Button)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim objUsuario As Usuario = New Usuario

            dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(9).Text(), dgi.Cells(7).Text())
            If dtMail.Rows.Count > 0 Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("EMAI") <> "" Then
                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
            End If

            objFunciones.SendMail("Alma Per� � Sistema AFI <" & ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                    "Solicitud de rechazo para Retiro " & dgi.Cells(7).Text() & " - " & Session.Item("NombreEntidad"),
                    "Estimados Se�ores:<br><br>" &
                    "El usuario " & Session.Item("UsuarioLogin") & " solicit� el rechazo del Retiro " & dgi.Cells(7).Text() & "  N�mero: <STRONG><FONT color='#330099'> " & dgi.Cells(8).Text() &
                    "</FONT></STRONG><br><br>Fecha Emisi�n Retiro: <STRONG><FONT color='#330099'> " & dgi.Cells(4).Text() & "</FONT></STRONG><br>" &
                    "Cliente: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                    "<br><br> Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")

            pr_IMPR_MENS("Se solicit� correctamente ")
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                                        Session.Item("NombreEntidad"), "C", "RETIROS_LINEA", "RECHAZAR RETIRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.lblMensaje.Text = ""
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
        Me.dgResultado.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Factura.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Public Sub lnkDOR_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim imgEditar As LinkButton = CType(sender, LinkButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(16).Text())
            Session.Add("NroLib", dgi.Cells(9).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(13).Text())
            Session.Add("PeriodoAnual", dgi.Cells(14).Text())
            Session.Add("Cod_Doc", dgi.Cells(15).Text())
            Session.Add("Cod_Unidad", dgi.Cells(10).Text())
            Session.Add("Aprobado", dgi.Cells(17).Text())
            Session.Add("CodAlmacen", dgi.Cells(18).Text())
            Session.Add("FlgDepsa", dgi.Cells(19).Text())
            Session.Add("EstadoDoc", Session.Item("EstadoDoc"))
            Session.Add("Moneda", dgi.Cells(5).Text())
            Session.Add("Importe", dgi.Cells(6).Text())

            If Session.Item("EstadoDoc") = "04" Then
                Dim objDocumento As Documento = New Documento
                If objDocumento.gGetEstadoImpresion(Session.Item("Cod_Doc")) = True Then
                    Response.Redirect("DocumentoRetiroPDF.aspx?var=1", False)
                    Exit Try
                End If
                Dim objConexion As SqlConnection
                Dim objTrans As SqlTransaction
                Dim objReportes As Reportes = New Reportes
                objConexion = New SqlConnection(strConn)
                objConexion.Open()
                objTrans = objConexion.BeginTransaction()
                objReportes.GeneraRetiro(dgi.Cells(15).Text(), strPath, strPathFirmas, True, dgi.Cells(13).Text(), objTrans)
                objTrans.Commit()
                objTrans.Dispose()
            End If
            Response.Redirect("DocumentoRetiroPDF.aspx?var=1", False)
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.DataItem("FLG_ALMA").ToString.Trim = Session.Item("IdSico") And (Session.Item("IdTipoEntidad") = "01" Or Session.Item("IdTipoEntidad") = "03") Then
                    e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                ElseIf e.Item.DataItem("st_sali_alma").ToString.Trim = "S" Then
                    e.Item.BackColor = System.Drawing.Color.FromArgb(173, 213, 230)
                Else
                    e.Item.BackColor = System.Drawing.Color.White
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Public Sub btnAutorizarRetiro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim dtMail As New DataTable
            Dim dtRetiro As DataTable
            Dim dtRetiroUsu As DataTable
            Dim strEmails As String
            Dim imgEditar As Button = CType(sender, Button)
            Dim dgi As DataGridItem
            Dim strScript As String
            Dim UsuaModifcado As String
            Dim strCodUnidad As String
            Dim strNroRetiro As String
            Dim strNombreCliente As String
            Dim strNombreReporte As String
            Dim nIM_TOTA As Decimal
            Dim nIM_UNIT As Decimal
            Dim nNU_UNID_RETI As Decimal
            Dim sCO_MONE As String
            Dim sCO_UNID As String
            Dim CO_MODA_MOVI As String
            Dim CO_USUA_MODI As String
            Dim objUsuario As Usuario = New Usuario

            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            strCodUnidad = dgi.Cells(10).Text()
            strNroRetiro = dgi.Cells(8).Text()

            dtRetiro = objRetiro.gCNCrearRetiroAprobado(Session.Item("CoEmpresa"), strCodUnidad, "DOR", strNroRetiro)
            For i As Integer = 0 To dtRetiro.Rows.Count - 1
                CO_USUA_MODI = dtRetiro.Rows(i)("CO_USUA_MODI")
            Next
            Dim strCadenaConexion As String = ConfigurationManager.AppSettings("ConnectionStringWE")
            Dim objConexion As SqlConnection
            Dim objTransaccion As SqlTransaction
            objConexion = New SqlConnection(strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction

            If objRetiro.gCNUpdRetiro(Session.Item("CoEmpresa"), strCodUnidad, Left(Session.Item("UsuarioLogin"), 8), "DOR", dgi.Cells(8).Text(), objTransaccion).ToString = "UsuarioAutorizo" Then
                pr_IMPR_MENS("Ya ha sido aprobado por el usuario " & CO_USUA_MODI)
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                objConexion.Close()
                Bindatagrid()
                Exit Sub
            End If

            If objRetiro.gCNUpdRetiro(Session.Item("CoEmpresa"), strCodUnidad, Left(Session.Item("UsuarioLogin"), 8), "DOR", dgi.Cells(8).Text(), objTransaccion).ToString = "DatosIncompletos" Then
                pr_IMPR_MENS("Faltan Datos para la Aprobaci�n ")
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                objConexion.Close()
                Bindatagrid()
                Exit Sub
            Else
                objTransaccion.Commit()
                objTransaccion.Dispose()
                objConexion.Close()
                '**************** Reporte de Aprobacion***************************
                strNombreReporte = strPath & strCodUnidad & strNroRetiro & ".PDF"
                'strNombreCliente = objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", strNroRetiro, Session.Item("IdSico"), CType(dtRetiro, DataTable), strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), True, Session.Item("IdSico"), "")
                '**************** Email  ******************************
                dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(9).Text(), dgi.Cells(7).Text())
                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                End If
                objFunciones.SendMail("Alma Per� � Sistema AFI <" & ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                                  "Se Autoriz� el Retiro " & dgi.Cells(7).Text() & " Nro " & dgi.Cells(8).Text(),
                                  "Estimados Se�ores:<br><br>" &
                                  "El usuario " & Session.Item("UsuarioLogin") & "  autoriz� el Retiro " & dgi.Cells(7).Text() & " N�mero: <STRONG><FONT color='#330099'>" & dgi.Cells(8).Text() &
                                  "</FONT></STRONG><br><br>Fecha Emisi�n Retiro: <STRONG><FONT color='#330099'>" & dgi.Cells(4).Text() & "</FONT></STRONG><br>" &
                                  "Cliente:<STRONG><FONT color='#330099'> " & dgi.Cells(1).Text() & "</FONT></STRONG>" &
                                  "<br>Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreReporte)

                pr_IMPR_MENS("Se aprob� correctamente")
                Bindatagrid()
            End If
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaS"))) > 0 Then
            Session("Co_AlmaW") = Session("Co_AlmaS")
            Response.Redirect("DocumentoAprobacionRetiro_W.aspx?Estado=" + Session.Item("EstadoDoc"), False)
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
