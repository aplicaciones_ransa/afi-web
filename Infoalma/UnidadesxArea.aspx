<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UnidadesxArea.aspx.vb" Inherits="UnidadesxArea" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Unidades por �rea</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  

			//------------------------------------------ Inicio Validar Fecha
		var primerslap=false; 
		var segundoslap=false; 
		function IsNumeric(valor) 
		{ 
			var log=valor.length; var sw="S"; 
			for (x=0; x<log; x++) 
			{ v1=valor.substr(x,1); 
			v2 = parseInt(v1); 
			//Compruebo si es un valor num�rico 
			if (isNaN(v2)) { sw= "N";} 
			} 
				if (sw=="S") {return true;} else {return false; } 
		} 	
		function formateafecha(fecha) 
		{ 
			var long = fecha.length; 
			var dia; 
			var mes; 
			var ano; 

			if ((long>=2) && (primerslap==false)){dia=fecha.substr(0,2); 
			if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
			else { fecha=""; primerslap=false;} 
			} 
			else 
			{ dia=fecha.substr(0,1); 
			if (IsNumeric(dia)==false) 
			{fecha="";} 
			if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
			} 
			if ((long>=5) && (segundoslap==false)) 
			{ mes=fecha.substr(3,2); 
			if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
			else { fecha=fecha.substr(0,3);; segundoslap=false;} 
			} 
			else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
			if (long>=7) 
			{ ano=fecha.substr(6,4); 
			if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
			else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
			} 

			if (long>=10) 
			{ 
				fecha=fecha.substr(0,10); 
				dia=fecha.substr(0,2); 
				mes=fecha.substr(3,2); 
				ano=fecha.substr(6,4); 
				// A�o no viciesto y es febrero y el dia es mayor a 28 
				if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
			} 
			return (fecha); 
		} 
		//-----------------------------------Fin formatear fecha	
		function Ocultar()
			{
			Estado.style.display='none';			
			}		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="125"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Titulo1" height="20">UNIDADES POR �REA</TD>
							</TR>
							<TR>
								<TD class="TEXT">
									<TABLE id="Table26" cellSpacing="0" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text" style="WIDTH: 57px">�rea</TD>
														<TD class="text" style="WIDTH: 15px">:</TD>
														<TD class="text" style="WIDTH: 314px"><asp:textbox id="txtArea" runat="server" ReadOnly="True" Width="304px" CssClass="Text"></asp:textbox></TD>
														<TD class="text" style="WIDTH: 84px" align="center"></TD>
														<TD class="text" style="WIDTH: 1px"></TD>
														<TD class="text"></TD>
														<TD class="text"></TD>
													</TR>
													<TR>
														<TD class="text" style="WIDTH: 57px">Nro.&nbsp;Caja</TD>
														<TD class="text" style="WIDTH: 15px">:</TD>
														<TD class="text" style="WIDTH: 314px"><asp:textbox id="txtNroCaja" runat="server" Width="140px" MaxLength="80" CssClass="Text"></asp:textbox></TD>
														<TD class="text" style="WIDTH: 84px" align="center"><asp:button id="btnBuscar" runat="server" Text="Buscar" CssClass="btn"></asp:button></TD>
														<TD class="text" style="WIDTH: 1px"></TD>
														<TD class="text"></TD>
														<TD class="text"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Resultado :
									<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></TD>
							</TR>
							<TR>
								<TD>
									<asp:datagrid id="dgdResultado" runat="server" AllowSorting="True" AllowPaging="True" OnPageIndexChanged="Change_Page"
										AutoGenerateColumns="False" Width="100%" PageSize="20" CssClass="gv" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="ID_UNID" HeaderText="Nro.">
												<HeaderStyle Width="15%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_TIPO_ITEM" HeaderText="Tipo">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FE_INGR" HeaderText="Fecha Ingreso">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FE_VENC" HeaderText="Fecha Vencimiento">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NU_PREC" HeaderText="Nro. Precinto">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="ST_UBIC" HeaderText="Ubicaci&#243;n">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
																	CausesValidation="False"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
									<asp:DataGrid id="dgdResultadotemp" runat="server" Width="328px" AutoGenerateColumns="False" Height="44px"
										BorderWidth="1px">
										<HeaderStyle Font-Size="XX-Small" BackColor="Gainsboro"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="ID_UNID" HeaderText="CAJA"></asp:BoundColumn>
											<asp:BoundColumn DataField="DE_AREA" HeaderText="AREA"></asp:BoundColumn>
											<asp:BoundColumn DataField="OBSE" HeaderText="DETALLE"></asp:BoundColumn>
											<asp:BoundColumn DataField="DE_CRIT_DESD" HeaderText=" DESCRIPCION"></asp:BoundColumn>
											<asp:BoundColumn DataField="DIVI" HeaderText=" DIVISION"></asp:BoundColumn>
											<asp:BoundColumn DataField="FECHA" HeaderText="FECHA"></asp:BoundColumn>
										</Columns>
									</asp:DataGrid></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:button id="btnRegresar" runat="server" Text="Regresar" CssClass="btn"></asp:button>
									<asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:button></TD>
							</TR>
							<TR>
								<TD class="td"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td" id="Estado"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
