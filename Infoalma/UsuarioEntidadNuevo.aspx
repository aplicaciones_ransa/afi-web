<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UsuarioEntidadNuevo.aspx.vb" Inherits="UsuarioEntidadNuevo" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UsuarioEntidadNuevo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116)
			{ 
				window.event.keyCode = 505;  
				} 
				if(window.event && window.event.keyCode == 505)
				{  
				return false;     
				}  
			}  
		
			function Ocultar()
			{			
				if (document.all.chkAprobador.checked==true || document.all.chkComun.checked==true)
				{
				document.getElementById("Liberar").style.display="inline";
								}
				else
				{
				document.getElementById("Liberar").style.display="none";
								}			
			}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="Ocultar()" rightMargin="0"
		bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD class="Titulo1" width="100%">
									<asp:label id="lblTitulo" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6">
									<uc1:menuinfo id="MenuInfo2" runat="server"></uc1:menuinfo></TD>
								<TD class="Subtitulo" vAlign="top" width="100%">Datos Generales</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="690" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table9" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" width="17%">Tipo Entidad</TD>
														<TD width="1%">:</TD>
														<TD width="32%">
															<asp:dropdownlist id="cboTipoEntidad" runat="server" CssClass="Text" Enabled="False" AutoPostBack="True"
																Width="150px"></asp:dropdownlist></TD>
														<TD class="Text" width="19%"></TD>
														<TD width="1%"></TD>
														<TD width="30%"></TD>
													</TR>
													<TR>
														<TD class="Text">Nombre Entidad</TD>
														<TD>:</TD>
														<TD colSpan="4">
															<asp:dropdownlist id="cboEntidad" runat="server" CssClass="Text" Enabled="False" Width="470px"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="Text">Grupo</TD>
														<TD>:</TD>
														<TD colSpan="4">
															<asp:dropdownlist id="cboGrupo" runat="server" CssClass="Text"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="Text">Nro Tel�fono</TD>
														<TD>:</TD>
														<TD><INPUT class="Text" onkeypress="validartelefono()" id="txtTelefono" style="WIDTH: 150px"
																disabled name="txtTelefono" runat="server"></TD>
														<TD class="Text">Nro Fax</TD>
														<TD>:</TD>
														<TD><INPUT class="Text" onkeypress="validarnumerico()" id="txtFax" style="WIDTH: 150px" disabled
																name="txtFax" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Subtitulo" vAlign="top">Datos Warrant Electr�nicos</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table30" cellSpacing="0" cellPadding="0" width="690" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table5" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text">Usuario Activo</TD>
														<TD>:</TD>
														<TD>
															<asp:checkbox id="chkUsrActivo" runat="server" CssClass="Text" Enabled="False" Checked="True"></asp:checkbox></TD>
														<TD class="Text">Notificar por Email:</TD>
														<TD>
															<asp:checkbox id="chkNotificar" runat="server" CssClass="Text" Enabled="False"></asp:checkbox></TD>
													</TR>
													<TR>
														<TD class="Text">Recepcionar Alerta</TD>
														<TD>:</TD>
														<TD>
															<asp:checkbox id="chkAlerta" runat="server" CssClass="Text" Enabled="False"></asp:checkbox></TD>
														<TD class="Text">PDF&nbsp;Adjunto al correo:</TD>
														<TD>
															<asp:checkbox id="chkAdjunto" runat="server" CssClass="Text" Enabled="False"></asp:checkbox></TD>
													</TR>
													<TR>
														<TD class="Text">Tipo Usuario</TD>
														<TD>:</TD>
														<TD colSpan="3">
															<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD><INPUT class="Text" id="chkSupervisor" disabled onclick="Ocultar();" type="radio" value="chkSupervisor"
																			name="RadioGroup" runat="server"></TD>
																	<TD class="Text">1. Consultor</TD>
																	<TD><INPUT class="Text" id="chkComun" disabled onclick="Ocultar();" type="radio" CHECKED="true" value="chkComun"
																			name="RadioGroup" runat="server"></TD>
																	<TD class="Text">2. Solicitante</TD>
																	<TD><INPUT class="Text" id="chkAprobador" disabled onclick="Ocultar();" type="radio" value="chkAprobador"
																			name="RadioGroup" runat="server"></TD>
																	<TD class="Text">3. Aprobador</TD>
																	<TD><INPUT class="Text" id="chkAdministrador" disabled onclick="Ocultar();" type="radio" value="chkAdministrador"
																			name="RadioGroup" runat="server"></TD>
																	<TD class="Text">Administrador</TD>
																	<TD></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD class="Text">Ubicar Firma</TD>
														<TD>:</TD>
														<TD colSpan="3"><INPUT class="btn" id="FileFirma" style="WIDTH: 300px" disabled type="file" name="FileFirma"
																runat="server">
															<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="FileFirma"
																ErrorMessage="Ingrese solo archivos jpg y gif" Display="Dynamic" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpg|.JPG|.gif|.GIF)$"></asp:regularexpressionvalidator></TD>
													</TR>
													<TR>
														<TD class="Text" height="20">Nombre Archivo</TD>
														<TD class="Text" height="20">:</TD>
														<TD class="Text" colSpan="3" height="20">
															<asp:label id="lblNombreImg" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Firma del Usuario</TD>
														<TD>:</TD>
														<TD colSpan="2">
															<asp:image id="imgFirma" runat="server" CssClass="Text" Width="200px" Height="100px" BorderColor="Gainsboro"
																BorderWidth="1px"></asp:image></TD>
														<TD class="Text" id="Liberar">
															<asp:checkbox id="chkLiberador" runat="server" CssClass="Text" Enabled="False" Text="Se visualiza firma en DOR?"></asp:checkbox>
															<br />
															<asp:checkbox id="chkVistoBueno" runat="server" CssClass="Text" Text="Visto Bueno Nuevo Warrant?" Enabled="False"></asp:checkbox></TD>
													</TR>
													<TR>
														<TD class="Text" width="17%">Todas las sucursales</TD>
														<TD width="1%">:</TD>
														<TD width="32%">
															<asp:checkbox id="chkSucursales" runat="server" Enabled="False"></asp:checkbox></TD>
														<TD class="Etiqueta" width="20%"></TD>
														<TD width="30%"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="690" border="0">
										<TR>
											<TD>
												<asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar"></asp:button></TD>
											<TD></TD>
											<TD>
												<asp:button id="btnRegresar" runat="server" CssClass="btn" Width="80px" Text="Regresar" CausesValidation="False"></asp:button></TD>
											<TD>
												<DIV style="DISPLAY: none"><INPUT id="txtCodUsuario" name="txtCodUsuario" runat="server"></DIV>
											</TD>
											<TD>
												<DIV style="DISPLAY: none"><INPUT id="txtCodEntidad" name="txtCodUsuario" runat="server"></DIV>
											</TD>
											<TD width="450"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
