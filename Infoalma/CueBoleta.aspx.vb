Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.Data

Public Class CueBoleta
    Inherits System.Web.UI.Page
    Private objFactura As clsCNFactura = New clsCNFactura

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipoDoc As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCorrelativo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSerie As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtMonto As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnVerDoc As System.Web.UI.HtmlControls.HtmlInputButton
    'Protected WithEvents imgCaptcha As System.Web.UI.WebControls.Image
    'Protected WithEvents txtCaptcha As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Image1 As System.Web.UI.WebControls.Image
    'Protected WithEvents imgrefresh As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents btnVerXML As System.Web.UI.HtmlControls.HtmlInputButton
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            imgCaptcha.ImageUrl = "~/CreateCaptcha.aspx?New=1"
        End If

        If Page.IsPostBack = False Then
            Dim dttFecha As DateTime
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
            Dia = "00" + CStr(dttFecha.Day)
            Mes = "00" + CStr(dttFecha.Month)
            Anio = "0000" + CStr(dttFecha.Year)
            Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        End If

    End Sub
    Public Sub imgrefresh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        imgCaptcha.ImageUrl = "~/CreateCaptcha.aspx?New=0"
    End Sub
    Private Sub btnVerDoc_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerDoc.ServerClick
        VerDocumento("1")
    End Sub
    Private Sub btnVerXML_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerXML.ServerClick
        VerDocumento("2")
    End Sub

    Private Sub VerDocumento(ByVal strTipoCon As String)
        Try
            If Me.cboTipoDoc.SelectedValue = "0" Then
                Me.lblMensaje.Text = "Seleccione tipo de documento"
                Exit Sub
            ElseIf Me.txtSerie.Text = "" Then
                Me.lblMensaje.Text = "Ingrese serie"
                Exit Sub
            ElseIf Me.txtCorrelativo.Text = "" Then
                Me.lblMensaje.Text = "Ingrese correlativo"
                Exit Sub
            ElseIf Me.txtFechaInicial.Value = "" Then
                Me.lblMensaje.Text = "Ingrese fecha"
                Exit Sub
            ElseIf Me.txtMonto.Text = "" Then
                Me.lblMensaje.Text = "Ingrese monto"
                Exit Sub
            End If
            Me.txtCorrelativo.Text = Format(CInt(Me.txtCorrelativo.Text), "0000000")
            If (Me.txtCaptcha.Text = Session("CaptchaCode").ToString()) Then
                imgCaptcha.ImageUrl = "~/CreateCaptcha.aspx?New=0"
            Else
                lblMensaje.Text = "C�digo Captcha erroneo, intentelo nuevamente!!"
                Exit Sub
            End If

            Dim dtBoleta As DataTable
            dtBoleta = objFactura.fn_GetBoleta(strTipoCon, Me.cboTipoDoc.SelectedValue, Me.txtSerie.Text, Me.txtCorrelativo.Text, Me.txtFechaInicial.Value, Me.txtMonto.Text)
            If dtBoleta.Rows.Count <> 0 Then
                If (dtBoleta.Rows(0).Item("NO_PDF_CSEL").ToString) <> "" Then
                    If strTipoCon = "1" Then
                        Session.Add("ssComEleSerPes_sTipoCon", strTipoCon)
                        Session.Add("ssComEleSerPes_sTipoDoc", Me.cboTipoDoc.SelectedValue)
                        Session.Add("ssComEleSerPes_sSerie", Me.txtSerie.Text)
                        Session.Add("ssComEleSerPes_sCorre", Me.txtCorrelativo.Text)
                        Session.Add("ssComEleSerPes_sFcha", Me.txtFechaInicial.Value)
                        Session.Add("ssComEleSerPes_sMonto", Me.txtMonto.Text)

                        Dim focusScript As String = "<script language='javascript'>" &
                          "window.open('VisorDocElectronico.aspx','','left=180,top=10,width=740,height=650,toolbar=0,scrollbars=1,resizable=0');</script>"
                        'Page.RegisterStartupScript("FocusScript", focusScript)
                        ClientScript.RegisterStartupScript(Me.GetType(), "FocusScript", focusScript)
                    End If

                    If strTipoCon = "2" Then
                        Dim response As HttpResponse = HttpContext.Current.Response
                        response.Clear()
                        response.Charset = ""
                        response.ContentType = "text"
                        response.AddHeader("Content-Disposition", "attachment;filename=" & "DocumentoXML" & ".xml")
                        response.ContentEncoding = Encoding.Default
                        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
                        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
                        response.Write(dtBoleta.Rows(0).Item("NO_PDF_CSEL"))
                        response.End()
                    End If
                    Me.lblMensaje.Text = ""
                Else
                    Me.lblMensaje.Text = "No se encontr� el archivo"
                End If
            Else
                Me.lblMensaje.Text = "No se encontr� el archivo"
            End If
        Catch ex As Exception
            lblMensaje.Text = ex.Message.ToString
        End Try
    End Sub
End Class
