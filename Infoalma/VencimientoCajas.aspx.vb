Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class VencimientoCajas
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objReporte As LibCapaNegocio.clsCNReporte
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private dvTemporal As DataView
    Private IntPase As Integer
    Private Gridexporta As New DataGrid
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents txtFechaHasta As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnVerReporte As System.Web.UI.WebControls.Button
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG17") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargarFechas()
                    CargarCombos()
                    LlenaGrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarCombos()
        'objComun = New LibCapaNegocio.clsCNComun
        'objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))

        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")
    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        txtFechaHasta.Value = objFuncion.gstrFecha()
    End Sub

    Private Sub LlenaGrid()
        'Try
        '    objReporte = New LibCapaNegocio.clsCNReporte
        '    objFuncion = New LibCapaNegocio.clsFunciones
        '    dvTemporal = New DataView(objReporte.gdrRepVencimientoCaja(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
        '            cboArea.SelectedItem.Value, "CAJ", "V", "", txtFechaHasta.Value, Session.Item("UsuarioLogin")))
        '    Session.Add("dvReporte", dvTemporal)
        '    objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
        '    lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"
        'Catch ex As Exception
        '    lblError.Text = ex.Message
        'End Try
        Try
            objReporte = New LibCapaNegocio.clsCNReporte
            objFuncion = New LibCapaNegocio.clsFunciones
            dvTemporal = New DataView(objReporte.gdrRepVencimientoCaja(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, "CAJ", "V", "", txtFechaHasta.Value, Session.Item("UsuarioLogin")))

            Session.Add("dvReporte", dvTemporal)
            objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)

            If IntPase = 1 Then
                objFuncion.gCargaGrid(Gridexporta, dvTemporal.Table)
                IntPase = 0
            Else
                objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)

            End If
            lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgdResultado.PageIndexChanged
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "DEPSAFIL", "CONSULTA DE VENCIMIENTO DE CAJAS", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        'objFuncion = New LibCapaNegocio.clsFunciones
        'Me.EnableViewState = False
        'objFuncion.ExportarAExcel("VencimientoCajas.xls", Response, dgdResultado)
        'Me.EnableViewState = True

        objFuncion = New LibCapaNegocio.clsFunciones
        Me.EnableViewState = False
        IntPase = 1
        Dim dgGrid As DataGrid = Gridexporta
        LlenaGrid()
        'Gridexporta As New DataGrid
        'objFuncion.ExportarAExcel("VencimientoCajas.xls", Response, dgdResultado)
        objFuncion.ExportarAExcel("VencimientoCajas.xls", Response, dgGrid)
        Gridexporta.Dispose()
        Gridexporta = Nothing
        Me.EnableViewState = True
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
        If Me.dgdResultado.Items.Count > 0 Then
            Response.Redirect("Reportes/RepVencimientoCajas.aspx")
        Else
            lblError.Text = "Falta informacion para imprimir"
        End If
    End Sub

    ReadOnly Property pCO_TIPO_ITEM() As String
        Get
            Return "CAJA"
        End Get
    End Property

    ReadOnly Property pDE_AREA() As String
        Get
            Return IIf(Me.cboArea.SelectedItem.Text = "", "TODOS", Me.cboArea.SelectedItem.Text)
        End Get
    End Property

    ReadOnly Property pFE_VENC() As String
        Get
            Return " hasta " & Me.txtFechaHasta.Value
        End Get
    End Property

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objReporte Is Nothing) Then
            objReporte = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
