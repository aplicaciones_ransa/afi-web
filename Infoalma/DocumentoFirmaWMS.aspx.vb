Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class DocumentoFirmaWMS
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objReportes As Reportes = New Reportes
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private oHashedData As New CAPICOM.HashedData
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents ltrVisor As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrFirmar2 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar1 As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblFirma As System.Web.UI.WebControls.Label
    'Protected WithEvents cntFirmas As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents txtArchivo_pdf As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtArchivo_pdf_firmado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNumSerie As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtEstado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTimeStamp As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents LtrFirma As System.Web.UI.WebControls.Literal
    ' Protected WithEvents hlkFilemaster As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ltrExpediente As System.Web.UI.WebControls.Literal

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Dim dtControles As New DataTable
            Try
                Dim var As Integer
                var = Request.QueryString("var")
                Me.txtNroPagina.Value = var
                Me.txtEstado.Value = Session.Item("EstadoDoc")
                'loadDataDocumento()
                txtNumSerie.Value = Session.Item("NroCertificado")
                Me.lblFirma.Text = ""
                Me.txtTimeStamp.Value = Now.ToString("dd/MM/yyyy hh:mm:ss tt")
                'If var = 1 Then
                '    Dim listaAux As String
                '    Dim drWarrant As DataRow
                '    Dim strHTMLTable As String
                '    Dim arrLista()
                '    Dim arrDatosFila()
                '    Dim i As Integer
                '    '----------------------------
                '    oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
                '    '------------------------------
                '    Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visor.aspx?var=1' runat='server'></iframe>"
                '    'drWarrant = objDocumento.gSelArchivoPDF(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), var)

                '    'If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                '    '    txtArchivo_pdf.Value = Convert.ToBase64String(drWarrant("DOC_PDFFIRM"))
                '    'Else
                '    '    oHashedData.Hash(Convert.ToBase64String(drWarrant("DOC_PDFORIG")))
                '    '    Me.txtArchivo_pdf.Value = oHashedData.Value
                '    'End If

                '    strHTMLTable = "<TABLE WIDTH='100%' BORDER='0' CELLSPACING='4' CELLPADDING='0' bgColor='#f0f0f0'>" _
                '    & "<THEAD><TR><TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>FIRMADO DIGITALMENTE POR:</TD>" _
                '    & "<TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>TIMESTAMP LOCAL</TD></TR></THEAD><TBODY>"
                '    Dim dtFirmantes As DataTable
                '    'dtFirmantes = objDocumento.BLGetDatosFirmantes(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
                '    'For Each dr As DataRow In dtFirmantes.Rows
                '    '    strHTMLTable = strHTMLTable & "<TR><TD width='60%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("NOMBRES") & "</TD>" _
                '    '                        & "<TD width='40%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("FECHA") & "</TD></TR>"
                '    'Next
                '    strHTMLTable = strHTMLTable & "</TBODY></TABLE>"
                '    LtrFirma.Text = strHTMLTable
                '    'End If
                'End If

                Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar2'>"
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            Finally
                dtControles.Dispose()
                dtControles = Nothing
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Function listaFirmas(ByVal prmArchivo As Byte(), ByVal prmArchivoOriginal As Byte(), ByVal strFechaModi As DateTime) As String
        Try
            Dim SignedData As CAPICOM.SignedData
            Dim Signer As CAPICOM.Signer
            Dim strLista As String
            Dim strFila As String
            Dim Atrib As CAPICOM.Attribute
            Dim strNombreArchivo As String
            Dim strTimeStamp As String
            Dim Contenido As String
            Dim mstream As System.IO.MemoryStream
            '------------------------------
            oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
            '-----------------------------
            SignedData = New CAPICOM.SignedData
            Contenido = Convert.ToBase64String(prmArchivo)
            SignedData.Verify(Contenido, False, CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_ONLY)
            '-------------------------------
            Dim fechaAux As Date
            fechaAux = Format("MM/dd/yyyy", ConfigurationManager.AppSettings.Item("FechaCambioValidador"))
            If DateDiff(("d"), fechaAux, Date.Now) > 0 Then
                oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
                If oHashedData.Value <> SignedData.Content Then
                    listaFirmas = "X"
                    Exit Function
                End If
            End If
            'If strFechaModi > CType(ConfigurationManager.AppSettings.Item("FechaCambioValidador"), DateTime) Then
            '    oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
            '    If oHashedData.Value <> SignedData.Content Then
            '        listaFirmas = "X"
            '        Exit Function
            '    End If
            'End If
            '--------------------------------
            listaFirmas = ""
            strLista = ""
            Signer = New CAPICOM.Signer
            For Each Signer In SignedData.Signers
                strFila = Signer.Certificate.GetInfo(0) & "*"
                For Each Atrib In Signer.AuthenticatedAttributes
                    If Atrib.Name = 2 Then
                        strTimeStamp = Atrib.Value
                        strFila = strFila & strTimeStamp
                    End If
                Next
                strLista = strLista & strFila & ";"
            Next
            listaFirmas = Mid(UCase(strLista), 1, Len(UCase(strLista)) - 1)
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Function

    Private Sub loadDataDocumento()
        Try
            Dim drUsuario As DataRow
            Dim strTipoDocumento As String
            Dim blnEndoso As Boolean

            drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
            If Session.Item("IdTipoDocumento") = "20" Then
                strTipoDocumento = Session("Cod_TipOtro")
                blnEndoso = True
            Else
                strTipoDocumento = Session("IdTipoDocumento")
                blnEndoso = False
            End If
            If Me.txtNroPagina.Value = "2" Then
                'If Me.txtNroPagina.Value = "1" Then
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, Session.Item("IdUsuario"), "0", "")
                    If objReportes.strMensaje <> "0" Then
                        Session.Add("Mensaje", objReportes.strMensaje)
                    End If
                End If
            End If

            If Me.txtNroPagina.Value = "1" Then
                If Session.Item("IdTipoDocumento") = "20" Or Session.Item("IdTipoDocumento") = "12" Then
                    If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                        objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, Session.Item("IdUsuario"), "0", "")
                        If objReportes.strMensaje <> "0" Then
                            Session.Add("Mensaje", objReportes.strMensaje)
                        End If
                    End If
                    Exit Sub
                End If


                drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim)
                End If
            End If
            drUsuario = Nothing
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub GrabaPDF(ByVal strNombArch As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Contenido, Session.Item("IdTipoDocumento"))
        fs.Close()
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
