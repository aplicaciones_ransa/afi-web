
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Text

Public Class CreateCaptcha
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private rand As New Random
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            CreateCaptchaImage()
        End If
    End Sub

    Private Sub CreateCaptchaImage()
        Dim code As String = GetRandomText().Trim
        Dim bitmap As New Bitmap(120, 40, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        Dim g As Graphics = Graphics.FromImage(bitmap)
        Dim pen As New Pen(Color.Yellow)
        Dim rect As New Rectangle(0, 0, 120, 40)
        Dim blue As New SolidBrush(Color.Silver)
        Dim black As New SolidBrush(Color.White)
        Dim counter As Integer = 0
        g.DrawRectangle(pen, rect)
        g.FillRectangle(blue, rect)

        g.DrawString(code.ToString(), New Font("Tahoma", 18, _
                       FontStyle.Italic), black, New PointF(9 + counter, 9))

        DrawRandomLines(g)
        bitmap.Save(Response.OutputStream, ImageFormat.Gif)
        g.Dispose()
        bitmap.Dispose()
    End Sub
   
    Private Sub DrawRandomLines(ByVal g As Graphics)
        Dim yellow As New SolidBrush(Color.Yellow)
        For i As Integer = 0 To 19
            g.DrawLines(New Pen(yellow, 1), GetRandomPoints())
        Next
    End Sub

    Private Function GetRandomPoints() As Point()
        Dim points As Point() = {New Point(rand.[Next](0, 150), rand.[Next](1, 150)), _
                                 New Point(rand.[Next](0, 200), rand.[Next](1, 190))}
        Return points
    End Function

    Private Function GetRandomText() As String
        Dim randomText As New StringBuilder
        Dim alphabets As String = "012345679ACEFGHKLMNPRSWXZ"
        Dim r As New Random
        For j As Integer = 0 To 5
            Dim valor As String
            valor = Left((alphabets.Substring(r.[Next](alphabets.Length))), 1)
            randomText.Append(valor)
        Next
        Session("CaptchaCode") = randomText.ToString()
        Return CType(Session("CaptchaCode"), String)
    End Function

End Class
