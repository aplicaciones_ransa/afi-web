Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class DocumentosxUnidad
    Inherits System.Web.UI.Page
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objUnidad As LibCapaNegocio.clsCNUnidad
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroMedio As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTipo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtArea As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    RecuperarVariables()
                    LlenaCabecera()
                    LlenaGrid()
                    objAccesoWeb = New clsCNAccesosWeb
                    objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                            Session.Item("NombreEntidad"), "C", "DEPSAFIL", "DOCUMENTOS POR UNIDAD", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenaGrid()
        Try
            objUnidad = New LibCapaNegocio.clsCNUnidad
            objFuncion = New LibCapaNegocio.clsFunciones
            'objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtConsultarDocumentosxUnidad(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
            'txtNroMedio.Text))

            objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtConsultarDocumentosxUnidad(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    txtNroMedio.Text))

            lblRegistros.Text = objUnidad.intFilasAfectadas.ToString & " registros"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Private Sub LlenaCabecera()
        objUnidad = New LibCapaNegocio.clsCNUnidad
        objUnidad.gConseguirCabeceraUnidad(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                        txtNroMedio.Text)
        txtTipo.Text = objUnidad.strDesTipoItem
        txtArea.Text = objUnidad.strDesArea
    End Sub



    Private Sub RecuperarVariables()
        If Request.QueryString("IU") <> "" Then
            Me.txtNroMedio.Text = Request.QueryString("IU")
        End If
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("UnidadesxArea.aspx")
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Protected Overrides Sub Finalize()
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objUnidad Is Nothing) Then
            objUnidad = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
