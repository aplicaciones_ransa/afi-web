<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarSolicitudWarrant.aspx.vb" Inherits="WarSolicitudWarrant" %>

<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarSolicitudWarrants</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {
            //if(parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value))
            //  {
            //   alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
            //   document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
            //   return;
            //  }				  				  
            document.getElementById(Monto).value = formatearDecimal(parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value), 3);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }

        function formatearDecimal(numero, cantDecimales) {
            if (cantDecimales > 0) {
                var i = 0;
                var aux = 1;
                for (i = 0; i < cantDecimales; i++) {
                    aux = aux * 10;
                }
                numero = numero * aux;
                numero = Math.round(numero);
                numero = numero / aux;
            }
            return numero;
        }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 23) { //para rel�gio de 12 horas altere o valor aqui
                alert("Horas inv�lidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("Minutos inv�lidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inv�lidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
    <script language="javascript">		
            function Todos(ckall, citem) {
                var actVar = ckall.checked;
                for (i = 0; i < Form1.length; i++) {
                    if (Form1.elements[i].type == "checkbox") {
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="660">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                        <td>
                                            <table id="Table1" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr id="trAlmacen" runat="server">
                                                    <td class="text">Almac�n con Stock</td>
                                                    <td class="text">:</td>
                                                    <td class="text">
                                                        <asp:DropDownList ID="cboAlmacenes" runat="server" CssClass="Text" AutoPostBack="True" Width="500px"></asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Otros almacenes</td>
                                                    <td class="text">:</td>
                                                    <td class="text" colspan="7">
                                                        <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" Width="500px" Enabled="False"></asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Financiador</td>
                                                    <td class="text">:</td>
                                                    <td class="text">
                                                        <asp:DropDownList ID="cboFinanciador" runat="server" CssClass="Text" Width="500px"></asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Fecha solicitud</td>
                                                    <td class="text">:</td>
                                                    <td class="text">
                                                        <table id="Table2" border="0" cellspacing="1" cellpadding="1" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="Text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                <td class="text">Hora&nbsp;solicitud</td>
                                                                <td style="width: 2px" class="text">:</td>
                                                                <td class="Text" align="left">
                                                                    <input onblur="valida_horas(this)" id="txtHoraIngreso" class="Text" onkeypress="dois_pontos(this)"
                                                                        maxlength="5" size="1" name="txtHoraIngreso" runat="server"></td>
                                                                <td class="Text" align="left">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="text">de 0 a 24 Horas</asp:Label></td>
                                                                <td class="Text" align="left">
                                                                    <asp:CheckBox ID="chkReemplazo" runat="server" Text="Reemplazo"></asp:CheckBox></td>
                                                                <td class="Text" align="left">Tipo T�tulo</td>
                                                                <td class="Text" align="left">:</td>
                                                                <td class="Text" align="left">
                                                                    <asp:DropDownList ID="cboTipoTitulo" runat="server" CssClass="Text">
                                                                        <asp:ListItem Value="Seleccione">Seleccione</asp:ListItem>
                                                                        <asp:ListItem Value="WAR">WAR</asp:ListItem>
                                                                        <asp:ListItem Value="WIP">WIP</asp:ListItem>
                                                                        <asp:ListItem Value="CER">CER</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Adjuntar archivo</td>
                                                    <td class="text">:</td>
                                                    <td class="text">
                                                        <input style="width: 400px" id="FilePDF" class="Text" type="file" name="FilePDF" runat="server">
                                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" CssClass="Error" EnableViewState="False"
                                                            Display="Dynamic" ControlToValidate="FilePDF" ErrorMessage="Solo archivos PDF" ForeColor=" " ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.PDF)$">Solo archivos PDF</asp:RegularExpressionValidator></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td width="80">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="Text" Width="80px" Text="Buscar" CausesValidation="False"></asp:Button></td>
                                        <td width="80">
                                            <asp:Button ID="btnAgregar" runat="server" CssClass="btn" Width="80px" Text="Agregar" CausesValidation="False"></asp:Button></td>
                                        <td width="80">
                                            <cc1:BotonEnviar ID="btnGrabar" runat="server" CssClass="Text" Width="80px" Text="Grabar" TextoEnviando="Grabando..."></cc1:BotonEnviar></td>
                                        <td width="80">
                                            <asp:Button ID="btnStock" runat="server" CssClass="btn" Width="120px" Text="Descargar Stock"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="200" valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AutoGenerateColumns="False"
                                    BorderWidth="1px" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Sel">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkTodos" runat="server"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSel" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Descripci&#243;n del &#237;tem">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtdescripcion" runat="server" CssClass="Text" Width="300px" Text='<%# DataBinder.Eval(Container, "DataItem.Item") %>' MaxLength="300">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Uni.Medida">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtUnidad" runat="server" CssClass="Text" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.UNI_MEDI") %>' MaxLength="10">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Stock Disponible">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtstock" runat="server" CssClass="Text" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.Disponible") %>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Cant. a Solicitar">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCantidad" runat="server" CssClass="Text" Width="100px" Text='<%# DataBinder.Eval(Container, "DataItem.Cantidad") %>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="P. Unitario">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtprecio" runat="server" CssClass="Text" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.precio") %>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Moneda">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMoneda" runat="server" CssClass="Text" AutoPostBack="True" Width="80px" OnSelectedIndexChanged="cboMoneda_select">
                                                    <asp:ListItem Value="SOL">SOLES</asp:ListItem>
                                                    <asp:ListItem Value="DOL">DOLARES</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Importe">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtImporte" runat="server" CssClass="Text" Width="120px" Text='<%# DataBinder.Eval(Container, "DataItem.Importe") %>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Eli.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.JPG"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
