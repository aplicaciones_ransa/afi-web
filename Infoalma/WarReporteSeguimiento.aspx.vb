Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class WarReporteSeguimiento
    Inherits System.Web.UI.Page
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
    Private strEndosoPath = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private objLiberacion As Liberacion = New Liberacion
    'Protected WithEvents chkPendientes As System.Web.UI.WebControls.CheckBox
    Private objReportes As Reportes = New Reportes

#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "39") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "39"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("strMensaje") <> Nothing Then
                    pr_IMPR_MENS(Session.Item("strMensaje"))
                    Session.Remove("strMensaje")
                End If
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Sub Inicializa()
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime
        Dim strResult As String()

        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(7))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        loadDepositante()
        loadFinanciador()

        If Session.Item("IdTipoEntidad") = "03" Then
            Me.cboDepositante.SelectedValue = Session.Item("IdSico")
            Me.cboDepositante.Enabled = False
        End If
        If Session.Item("IdTipoEntidad") = "02" Then
            Me.cboFinanciador.SelectedValue = Session.Item("IdSico")
            Me.cboFinanciador.Enabled = False
        End If
        BindDatagrid()
    End Sub

    Private Sub loadDepositante()
        Dim dtEntidad As DataTable
        Try
            Me.cboDepositante.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("03", "")
            Me.cboDepositante.Items.Add(New ListItem("---------------Todos---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositante.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadFinanciador()
        Dim dtEntidad As DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "")
            Me.cboFinanciador.Items.Add(New ListItem("---------------Todos---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objDocumento.gGetBusquedaDocumentosSolicitudWarrants(Me.txtNroWarrant.Text.Replace("'", ""), FechaInicial, FechaFinal,
            Me.cboDepositante.SelectedValue, Me.cboFinanciador.SelectedValue, Session.Item("IdUsuario"), Me.chkPendientes.Checked)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Session.Item("dtSeguimiento") = dt
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim intTiempo1 As Integer
        Dim intTiempo2 As Integer
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            intTiempo1 = e.Item.DataItem("Tiempo1")
            intTiempo2 = e.Item.DataItem("Tiempo2")
            e.Item.Cells(29).ForeColor = System.Drawing.Color.White
            e.Item.Cells(30).ForeColor = System.Drawing.Color.White
            If intTiempo1 > 28800 Then
                e.Item.Cells(29).BackColor = System.Drawing.Color.Red
            Else
                e.Item.Cells(29).BackColor = System.Drawing.Color.Blue
            End If
            If intTiempo2 > 43200 Then
                e.Item.Cells(30).BackColor = System.Drawing.Color.Red
            Else
                e.Item.Cells(30).BackColor = System.Drawing.Color.Blue
            End If
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgSeguimiento As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgSeguimiento.DataSource = Session.Item("dtSeguimiento")
        dgSeguimiento.DataBind()
        dgSeguimiento.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub
End Class
