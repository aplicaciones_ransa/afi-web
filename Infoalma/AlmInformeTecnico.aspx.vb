Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data.SqlClient
Imports System.IO
Imports Infodepsa
Imports System.Data

Public Class AlmInformeTecnico
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    'Protected WithEvents txtNroVapor As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "INFORME TECNICO") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "INFORME TECNICO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Reporte de Monitoreo de Granos"
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime
        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)

    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Try
        '    Dim imgEditar As ImageButton = CType(sender, ImageButton)
        '    Dim dgi As DataGridItem
        '    dgi = CType(imgEditar.Parent.Parent, DataGridItem)
        '    Session.Add("NroDoc", dgi.Cells(0).Text())
        '    Session.Add("NroLib", dgi.Cells(16).Text())
        '    Session.Add("IdTipoDocumento", dgi.Cells(2).Text())
        '    Session.Add("PeriodoAnual", dgi.Cells(13).Text())
        '    Session.Add("Cod_TipOtro", dgi.Cells(14).Text())
        '    Session.Add("Cod_Doc", dgi.Cells(17).Text())
        '    Session.Add("Cod_Unidad", dgi.Cells(18).Text())
        '    Response.Redirect("DocumentoPagUno.aspx", False)
        'Catch ex As Exception
        '    Me.lblError.Text = "ERROR " + ex.Message
        'End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
        ' RegistrarCadenaBusqueda()
    End Sub

    'Private Sub RegistrarCadenaBusqueda()
    '    Session.Add("CadenaBusqueda", txtFechaInicial.Value & "-" & txtFechaFinal.Value)
    'End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            'Dim FechaInicial As String
            'Dim FechaFinal As String
            ' dt = objDocumento.gGetBusquedaDocumentosInformesTecnicos(txtFechaInicial.Value, txtFechaFinal.Value, Session.Item("IdSico"), Me.txtNroVapor.Text)

            dt = objDocumento.gGetBusquedaDocumentosInformes(txtFechaInicial.Value, txtFechaFinal.Value, Session.Item("IdSico"), Me.txtNroVapor.Text, dboTipoInforme.SelectedValue)

            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                'Dim txtRuta As System.Web.UI.HtmlControls.HtmlInputHidden
                'txtRuta = CType(e.Item.FindControl("txtRuta"), System.Web.UI.HtmlControls.HtmlInputHidden)

                'Dim btnVistaPreliminar As ImageButton
                'btnVistaPreliminar = CType(e.Item.FindControl("imgEditar"), ImageButton)
                'btnVistaPreliminar.Attributes.Add("onclick", "return vistaPreliminar('" + txtRuta.Value + "');")

            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
End Class
