<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmMovimiento_W.aspx.vb" Inherits="AlmMovimiento_W" %>

<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Movimiento de Mercader�a</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIni").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFin").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });


        function AbrirDetaMoviWMS(sMOVI, sORDEN, sALMACEN) {
            var ventana = 'Popup/poputAlmMovimiento_w.aspx?sMOVI=' + sMOVI + '&sORDEN=' + sORDEN + '&sALMACEN=' + sALMACEN;
            window.open(ventana, 'Retiro', 'left=240,top=200,width=800,height=600,toolbar=0,resizable=1');
        }
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() - miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }


        function fn_Impresion(sCO_MOVI, sDE_MOVI, sCO_REPO, sDE_REPO, sFE_INIC, sFE_FINA, sCO_ALMA, sDE_ALMA, sCODIGO, sDE_MERC, sLOTE, sCO_PROD) {
            var ventana = 'Popup/AlmMovimientoImpresion_w.aspx?sCO_MOVI=' + sCO_MOVI + '&sDE_MOVI=' + sDE_MOVI + '&sCO_REPO=' + sCO_REPO + '&sDE_REPO=' + sDE_REPO + '&sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sCO_ALMA=' + sCO_ALMA + '&sDE_ALMA=' + sDE_ALMA + '&sCODIGO=' + sCODIGO + '&sDE_MERC=' + sDE_MERC + '&sLOTE=' + sLOTE + '&sCO_PROD=' + sCO_PROD;
            window.open(ventana, "Impresion", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
            //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
        }
    </script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="710" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text" width="18%">Almac�n</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="32%" colspan="9">
                                                                    <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" Width="400px" AutoPostBack="True"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" width="18%">Tipo Movimiento</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="139">
                                                                    <asp:DropDownList ID="cboMovimiento" runat="server" CssClass="Text" Width="140px">
                                                                        <asp:ListItem Value="1">Ingreso de Mercader&#237;a</asp:ListItem>
                                                                        <asp:ListItem Value="2">Retiro de Mercader&#237;a</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text" width="14%">Tipo Reporte</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text" width="119" colspan="2">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboReporte" runat="server" CssClass="Text" Width="100px">
                                                                        <asp:ListItem Value="2">Resumido</asp:ListItem>
                                                                        <asp:ListItem Value="1">Detallado</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text" width="1%"></td>
                                                                <td class="Text" width="19%" align="left"></td>
                                                                <td width="1%" align="left"></td>
                                                                <td width="25%" align="left"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" width="18%">Nro. Referencia</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="139">
                                                                    <asp:TextBox ID="txtCodigo" runat="server" CssClass="Text" Width="140px"></asp:TextBox></td>
                                                                <td class="Text" width="14%">C�d. producto
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td class="Text" width="119" colspan="2">
                                                                    <asp:TextBox ID="txtCodprod" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                                <td class="Text" width="1%"></td>
                                                                <td class="Text" width="19%" align="left">Descripci�n Merc.</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td width="25%" align="left">
                                                                    <asp:TextBox ID="txtDescMerc" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" width="18%">Lote</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="139">
                                                                    <asp:TextBox ID="txtLote" runat="server" CssClass="Text" Width="140px"></asp:TextBox></td>
                                                                <td class="Text" width="14%">Fecha Del</td>
                                                                <td width="1%">:</td>
                                                                <td class="Text">
                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="10" name="txtFechaInicial" runat="server"></td>
                                                                <td class="Text">
                                                                    <input id="btnFechaIni" class="text"
                                                                        value="..." type="button" name="btnFechaIni"></td>
                                                                <td class="Text" width="1%" align="right">&nbsp;</td>
                                                                <td class="Text" width="19%" align="left">Al</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td class="Text" width="25%" align="left">
                                                                    <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="10" name="txtFechaFinal" runat="server"><input style="z-index: 0" id="btnFechaFin" class="text"
                                                                        value="..." type="button" name="btnFechaFin"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                    <td width="80">
                                                        <input id="btnImprimir" class="btn" onclick="fn_Impresion('<%=cboMovimiento.SelectedItem.Value%>','<%=cboMovimiento.SelectedItem.Text%>','<%=cboReporte.SelectedItem.Value%>','<%=cboReporte.SelectedItem.Text%>','<%=txtFechaInicial.Value%>','<%=txtFechaFinal.Value%>','<%=cboAlmacen.SelectedItem.Value%>','<%=cboAlmacen.SelectedItem.Text%>','<%=txtCodigo.Text%>','<%=txtDescMerc.Text%>','<%=txtLote.Text%>','<%=txtCodprod.Text%>')" value="Imprimir" type="button" name="btnImprimir"></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgIngRes" runat="server" CssClass="gv" Width="987px" OnPageIndexChanged="Change_dgIngRes"
                                                BorderColor="Gainsboro" HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ALMACEN" HeaderText="Almac&#233;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ESTADO" HeaderText="Estado">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:DataGrid Style="z-index: 0" ID="dgIngDet" runat="server" CssClass="gv" Width="987px" OnPageIndexChanged="Change_dgIngDet"
                                                BorderColor="Gainsboro" HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ITEM" HeaderText="Item">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="COD_PRODUCTO" HeaderText="Cod.Producto">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LOTE" HeaderText="Lote">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MEDIDA" HeaderText="Medida">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CODIGO_RETENCION" HeaderText="Cod.Retencion">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_INGRESO" HeaderText="Fecha Ingreso">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_VENCIMIENTO" HeaderText="Fecha Vencimiento">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="FLG_ORDER" HeaderText="Order">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="ALMACEN" HeaderText="Almac&#233;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CANTIDAD_TOTA" HeaderText="Cant.Total">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:DataGrid Style="z-index: 0" ID="dgRetRes" runat="server" CssClass="gv" Width="987px" OnPageIndexChanged="Change_dgRetRes"
                                                BorderColor="Gainsboro" HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD_SOLICITADA" HeaderText="Cant.Solicitada">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD_PREPARADA" HeaderText="Cant.Preparada">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="RETIRADO_POR" HeaderText="Retirado por">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CLIENTE" HeaderText="Cliente">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><asp:DataGrid Style="z-index: 0" ID="dgRetDet" runat="server" CssClass="gv" Width="987px" OnPageIndexChanged="Change_dgRetDet"
                                                BorderColor="Gainsboro" HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CODIGO" HeaderText="C�digo">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ITEM" HeaderText="Item">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD_SOLICITADA" HeaderText="Cant.Solicitada">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD_PREPARADA" HeaderText="Cant.Preparada">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LOTE" HeaderText="Lote">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="RETIRADO_POR" HeaderText="Retirado por">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CLIENTE" HeaderText="Cliente">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fecha Confirmaci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="FLG_ORDER" HeaderText="Order">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="ALMACEN" HeaderText="Almac&#233;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CANTIDAD_SOLICITADA_TOTAL">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CANTIDAD_PREPARADA_TOTAL">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
