<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarAlmacenXCliente.aspx.vb" Inherits="WarAlmacenXCliente" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LISTA DE ALMACEN </title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="0" cellSpacing="1" cellPadding="1" width="400">
				<TR>
					<TD class="Text">Almacen:
						<asp:textbox id="txtAlmacen" runat="server" Width="100%" CssClass="Text" MaxLength="200"></asp:textbox></TD>
				</TR>
				<TR>
					<TD align="center"><asp:button id="btnAgregar" runat="server" Width="80px" Text="Agregar" CssClass="btn"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:listbox id="lstAlmacen" runat="server" Width="100%" Height="300px" CssClass="Text"></asp:listbox></TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
				</TR>
				<TR>
					<TD align="center"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
