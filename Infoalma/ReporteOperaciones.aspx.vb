Imports Library.AccesoDB
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class ReporteOperaciones
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "24") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "24"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime
        Me.lblOpcion.Text = "Reporte de Operaciones"
        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(360))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        loadDepositante()
        loadFinanciador()
    End Sub

    Private Sub loadDepositante()
        Dim dtEntidad As DataTable = New DataTable
        Try
            Me.cboDepositante.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("03", "")
            Me.cboDepositante.Items.Add(New ListItem("---------------Todos---------------", "0"))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositante.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadFinanciador()
        Dim dtEntidad As DataTable = New DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "")
            Me.cboFinanciador.Items.Add(New ListItem("---------------Todos---------------", "0"))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable = New DataTable("Items")
        Dim dtTotal As DataTable = New DataTable("Totales")
        Dim dr As DataRow
        Try
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objDocumento.gGetReporteOperaciones(FechaInicial, FechaFinal, Me.cboDepositante.SelectedValue, Me.cboFinanciador.SelectedValue)
            dtTotal = objDocumento.gGetReporteOperacionesTotal(FechaInicial, FechaFinal, Me.cboDepositante.SelectedValue, Me.cboFinanciador.SelectedValue)

            For Each dr In dtTotal.Rows
                dt.ImportRow(dr)
            Next
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BindDatagrid()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True
        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        'Response.Charset = "UTF-8"
        'Response.ContentEncoding = Encoding.Default
        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False
        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
        Me.dgdResultado.RenderControl(htmlWriter)
        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            For Each cel As TableCell In e.Item.Cells
                If IsNumeric(cel.Text) Then
                    cel.Text = String.Format("{0:##,###.##0}", CType(cel.Text, Decimal))
                End If
                If cel.Text = "Total" Then
                    e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                End If
            Next
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
