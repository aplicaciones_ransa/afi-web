Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class DocumentoRetiroIngreso
    Inherits System.Web.UI.Page
    Private objRetiro As New clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objWMS As clsCNWMS
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strPathIngreso = ConfigurationManager.AppSettings.Item("RutaTemporal")
    'Protected WithEvents txtNroGuia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnRefrescar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGrabar1 As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Private strPathDownload = ConfigurationManager.AppSettings.Item("RutaDownload")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboTipCarga As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents FileLoad As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents Panel1 As System.Web.UI.WebControls.Panel
    'Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    'Protected WithEvents lnkPlantilla As System.Web.UI.WebControls.HyperLink
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Env�o de Ingreso al WMS"
                If Page.IsPostBack = False Then
                    Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                    Me.btnAgregar.Attributes.Add("onclick", "Javascript:AbrirDetalle('','');")
                    ListarAlmacen()
                    Inicializa()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub BindDatagrid()
        'Dim dtIngreso As DataTable = Nothing
        dtIngreso001 = Session.Item("dtIngreso001")
        If Not dtIngreso001 Is Nothing Then
            Me.dgdResultado.DataSource = dtIngreso001
            Me.dgdResultado.DataBind()
        Else
            crearSessionTabla()
        End If
    End Sub

    Private Sub crearSessionTabla()
        Dim dtIngreso001 As New DataTable
        'dtIngreso.Rows.Clear()
        Dim Column0 As New DataColumn("NU_ITEM")
        Column0.DataType = GetType(String)
        Dim Column1 As New DataColumn("CO_PROD")
        Column1.DataType = GetType(String)
        Dim Column2 As New DataColumn("DE_PROD")
        Column2.DataType = GetType(String)
        Dim Column3 As New DataColumn("NU_CANT")
        Column3.DataType = GetType(String)
        Dim Column4 As New DataColumn("NU_LOTE")
        Column4.DataType = GetType(String)

        dtIngreso001.Columns.Add(Column0)
        dtIngreso001.Columns.Add(Column1)
        dtIngreso001.Columns.Add(Column2)
        dtIngreso001.Columns.Add(Column3)
        dtIngreso001.Columns.Add(Column4)

        Session.Add("dtIngreso001", dtIngreso001)
    End Sub

    Private Sub ListarAlmacen()
        Dim dsAlma As DataTable = objRetiro.gCNGetListarAlmacenesWMS(Session("IdSico"))
        Me.cboAlmacen.Items.Clear()
        For i As Integer = 0 To dsAlma.Rows.Count - 1
            Me.cboAlmacen.Items.Add(New ListItem(dsAlma.Rows(i)("DE_ALMA"), dsAlma.Rows(i)("WHSE_ID")))
        Next
        dsAlma.Dispose()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                'Dim imgEditar As ImageButton
                'imgEditar = CType(e.Item.FindControl("imgEditar"), ImageButton)
                'imgEditar.Attributes.Add("onClick", "javascript:AbrirDetalle('" & Me.txtNroGuia.Text & "','" & e.Item.DataItem("NU_ITEM").ToString.Trim & "');")
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub Inicializa()
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
    End Sub

    Private Sub cboTipCarga_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipCarga.SelectedIndexChanged
        Inicializa()
        If Me.cboTipCarga.SelectedValue = 1 Then
            Me.Panel1.Visible = True
        End If
        If Me.cboTipCarga.SelectedValue = 2 Then
            Me.Panel2.Visible = True
            BindDatagrid()
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            If Not FileLoad.PostedFile Is Nothing And FileLoad.PostedFile.ContentLength > 0 Then
                Dim strNomArchivo As String = System.IO.Path.GetFileName(FileLoad.PostedFile.FileName)
                Dim strRuta As String = strPathIngreso & strNomArchivo
                Dim dsIngreso As DataSet
                If strNomArchivo <> "" Then
                    FileLoad.PostedFile.SaveAs(strRuta)
                    Me.lblMensaje.Text = "El archivo ha sido cargado."
                End If
                '========= copiar datos de excel a tabla temporal y enviar correo , generar archivo texto ==============
                dsIngreso = objRetiro.gCNSetEnviaIngresoWMS(Session("CoEmpresa"), strRuta, "*", Session.Item("IdSico"), Me.cboAlmacen.SelectedValue)
                If objRetiro.strMensajeError = "2" Then 'dsIngreso.Tables(0) Is Nothing Or dsIngreso.Tables(0).Rows.Count = 1 Then
                    Dim strMensaje As String = "Los siguientes c�digos de producto no estan registrados en nuestro Sistema, favor de enviar la informaci�n de dichos codigos a Alma Per�</BR></BR>"
                    For i As Integer = 0 To dsIngreso.Tables(0).Rows.Count - 1
                        strMensaje = strMensaje & dsIngreso.Tables(0).Rows(i)("CO_PROD") & " / " & dsIngreso.Tables(0).Rows(i)("DE_PROD") & "</BR>"
                    Next
                    Me.lblMensaje.Text = strMensaje
                    'Session.Add("MensajePedido", "No se gener� el archivo de texto")
                    'Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                    Exit Sub
                End If
                If dsIngreso.Tables(0) Is Nothing Or dsIngreso.Tables(0).Rows.Count = 1 Then
                    Me.lblMensaje.Text = "No se gener� el archivo de texto"
                    'Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                    Exit Sub
                End If
                If dsIngreso.Tables(0).Rows.Count > 2 Then
                    If CreateTextDelimiterFile(strPathDownload & "I" & dsIngreso.Tables(1).Rows(0)(0) & ".txt", dsIngreso.Tables(0), "*", False, False, Me.txtNroGuia.Text) = False Then
                        Me.lblMensaje.Text = "No se pudo generar el ingreso"
                        'Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                        Exit Sub
                    End If
                End If
            Else
                Me.lblMensaje.Text = "Seleccione un archivo que cargar."
            End If
        Catch Exc As Exception
            Me.lblMensaje.Text = "Error: " & Exc.Message
        End Try
    End Sub

    Private Function CreateTextDelimiterFile(ByVal fileName As String, _
                                         ByVal dt As DataTable, _
                                         ByVal separatorChar As Char, _
                                         ByVal hdr As Boolean, _
                                         ByVal textDelimiter As Boolean, _
                                         ByVal strNroRetiro As String) As Boolean

        If (fileName = String.Empty) OrElse _
           (dt Is Nothing) Then Throw New System.ArgumentException("Argumentos no v�lidos.")

        If (IO.File.Exists(fileName)) Then
            'If (MessageBox.Show("Ya existe un archivo de texto con el mismo nombre." & Environment.NewLine & _
            '                   "�Desea sobrescribirlo?", _
            '                   "Crear archivo de texto delimitado", _
            '                   MessageBoxButtons.YesNo, _
            '                   MessageBoxIcon.Information) = DialogResult.No) Then 
            'Return False
        End If
        Dim sw As System.IO.StreamWriter

        Try
            Dim col As Integer = 0
            Dim value As String = String.Empty
            ' Creamos el archivo de texto con la codificaci�n por defecto.
            sw = New IO.StreamWriter(fileName, False, System.Text.Encoding.Default)

            If (hdr) Then
                ' La primera l�nea del archivo de texto contiene
                ' el nombre de los campos.
                For Each dc As DataColumn In dt.Columns
                    If (textDelimiter) Then
                        ' Incluimos el nombre del campo entre el caracter
                        ' delimitador de texto especificado.
                        value &= """" & dc.ColumnName & """" & separatorChar
                    Else
                        ' No se incluye caracter delimitador de texto alguno.
                        value &= dc.ColumnName & separatorChar
                    End If
                Next
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
            End If

            ' Recorremos todas las filas del objeto DataTable
            ' incluido en el conjunto de datos.
            For Each dr As DataRow In dt.Rows
                For Each dc As DataColumn In dt.Columns
                    If ((dc.DataType Is System.Type.GetType("System.String")) And _
                       (textDelimiter = True)) Then
                        ' Incluimos el dato alfanum�rico entre el caracter
                        ' delimitador de texto especificado.
                        value &= """" & dr.Item(col).ToString & """" & separatorChar
                    Else
                        ' No se incluye caracter delimitador de texto alguno
                        value &= dr.Item(col).ToString & separatorChar
                    End If
                    ' Siguiente columna
                    col += 1
                Next
                ' Al escribir los datos en el archivo, elimino el
                ' �ltimo car�cter delimitador de la fila.
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
                col = 0
            Next ' Siguiente fila
            sw.Close()
            ' Se ha creado con �xito el archivo de texto.
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        Finally
            sw = Nothing
        End Try
    End Function

    Private Property dtIngreso001() As DataTable
        Get
            Return (ViewState("dtIngreso001"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtIngreso001") = Value
        End Set
    End Property

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            CargarTabla()
            dtIngreso001.Rows.RemoveAt(dgi.ItemIndex)
            For Each dr1 As DataRow In dtIngreso001.Rows
                dtIngreso001.Rows(j).AcceptChanges()
                dtIngreso001.Rows(j).BeginEdit()
                dtIngreso001.Rows(j).Item(0) = j + 1
                dtIngreso001.Rows(j).EndEdit()
                j += 1
            Next
            Me.dgdResultado.DataSource = dtIngreso001
            Me.dgdResultado.DataBind()
            Session.Add("dtIngreso001", dtIngreso001)
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub CargarTabla()
        Dim dr As DataRow
        Dim dgItem As DataGridItem
        dtIngreso001.Rows.Clear()
        For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
            dgItem = Me.dgdResultado.Items(i)
            dr = dtIngreso001.NewRow
            dr(0) = dgItem.Cells(0).Text()
            dr(1) = dgItem.Cells(1).Text()
            dr(2) = dgItem.Cells(2).Text()
            dr(3) = dgItem.Cells(3).Text()
            dr(4) = dgItem.Cells(4).Text()
            dtIngreso001.Rows.Add(dr)
        Next i
    End Sub

    Private Sub btnGrabar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar1.Click
        If Me.txtNroGuia.Text = "" Then
            Me.lblMensaje.Text = "Debe ingresar el n�mero de gu�a"
            Return
        End If
        Dim dsIngreso As DataSet
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        objWMS = New clsCNWMS
        If objWMS.gCNAddItemIngresoWMS(Session.Item("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdUsuario"), _
                                          Me.cboAlmacen.SelectedValue, dtIngreso001, Me.txtNroGuia.Text, objTrans) = False Then
            Me.lblMensaje.Text = "No se pudo enviar el ingreso"
            objTrans.Rollback()
            objTrans.Dispose()
            Exit Sub
        Else
            objTrans.Commit()
            objTrans.Dispose()
        End If
        dsIngreso = objRetiro.gCNSetEnviaIngresoWMS(Session("CoEmpresa"), "", Me.txtNroGuia.Text, Session.Item("IdSico"), Me.cboAlmacen.SelectedValue)
        If objRetiro.strMensajeError = "1" Then 'dsIngreso.Tables(0) Is Nothing Or dsIngreso.Tables(0).Rows.Count = 1 Then
            Me.lblMensaje.Text = "No se gener� el archivo de texto"
            'objTrans.Rollback()
            'objTrans.Dispose()
            'Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
            Exit Sub
        End If
        If dsIngreso.Tables(0).Rows.Count > 2 Then
            If CreateTextDelimiterFile(strPathDownload & "I" & dsIngreso.Tables(1).Rows(0)(0) & ".txt", dsIngreso.Tables(0), "*", False, False, Me.txtNroGuia.Text) = False Then
                Me.lblMensaje.Text = "No se pudo generar el ingreso"
                'objTrans.Rollback()
                'objTrans.Dispose()
                'Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
        End If
        'objTrans.Commit()
        'objTrans.Dispose()
        dtIngreso001 = Nothing
        Session.Remove("dtIngreso001")
        Me.lblMensaje.Text = "Se gener� el ingreso n�mero " & "I" & dsIngreso.Tables(1).Rows(0)(0)
    End Sub

    Private Sub btnRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefrescar.Click
        BindDatagrid()
    End Sub
End Class
