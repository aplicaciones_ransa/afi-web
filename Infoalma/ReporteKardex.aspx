<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ReporteKardex.aspx.vb" Inherits="ReporteKardex" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ReporteKardex</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" align="center" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="670" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text" width="10%">Depositante</TD>
														<TD class="text" width="1%">:</TD>
														<TD width="60%">
															<asp:dropdownlist id="cboDepositante" runat="server" CssClass="Text" Width="350px"></asp:dropdownlist></TD>
														<TD class="text" width="8%">Estado</TD>
														<TD width="1%">:</TD>
														<TD width="20%">
															<asp:dropdownlist id="cboEstado" runat="server" CssClass="Text" Width="100px"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="text" width="10%">Producto</TD>
														<TD class="text" width="1%">:</TD>
														<TD width="60%">
															<asp:dropdownlist id="cboTipoMercaderia" runat="server" CssClass="Text" Width="350px"></asp:dropdownlist></TD>
														<TD class="text" width="8%">Nro.Warrant</TD>
														<TD width="1%">:</TD>
														<TD width="20%">
															<asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="80px"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="td" vAlign="top" align="center">
									<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button>
									<asp:button id="btnExportar" runat="server" CssClass="btn" Width="80px" Text="Exportar"></asp:button></TD>
							</TR>
							<TR>
								<TD class="text" vAlign="top" align="left">Resultado:</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AutoGenerateColumns="False" PageSize="30" BorderWidth="1px">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="CO_EMPR" HeaderText="CO_EMPR"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UNID" HeaderText="CO_UNID"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="TI_DOCU_RECE" HeaderText="TI_DOCU_RECE"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Nro Operaci&#243;n">
												<ItemTemplate>
													<asp:HyperLink id=hlkDetalle NavigateUrl='<%# "ReporteKardexDetalle.aspx?sCO_EMPR=" &amp; Container.DataItem("CO_EMPR")&amp; "&amp;sCO_UNID=" &amp; Container.DataItem("CO_UNID")&amp; "&amp;sTI_TITU=" &amp; Container.DataItem("TI_TITU")&amp; "&amp;sNU_TITU=" &amp; Container.DataItem("Warrant") %>' Runat="server">
														<%# DataBinder.Eval(Container, "DataItem.Operación") %>
													</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Warrant" HeaderText="Warrant">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Certificado" HeaderText="Certificado"></asp:BoundColumn>
											<asp:BoundColumn DataField="WarrantSiguiente" HeaderText="Warrant Siguiente"></asp:BoundColumn>
											<asp:BoundColumn DataField="WarrantAnterior" HeaderText="Warrant Anterior"></asp:BoundColumn>
											<asp:BoundColumn DataField="Producto" HeaderText="Un Producto(U)/Uno de los Productos &gt;(M)"></asp:BoundColumn>
											<asp:BoundColumn DataField="Unid.Medida" HeaderText="Unidad Medida"></asp:BoundColumn>
											<asp:BoundColumn DataField="Cant/PesoInicial" HeaderText="Cant/Peso. Inicial" DataFormatString="{0:N2}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Unid.MedidaConvertible" HeaderText="Unid.Medida Convertible" DataFormatString="{0:N2}"></asp:BoundColumn>
											<asp:BoundColumn DataField="Cant.Convertible" HeaderText="Cant.Convertible" DataFormatString="{0:N2}"></asp:BoundColumn>
											<asp:BoundColumn DataField="Moneda" HeaderText="Moneda"></asp:BoundColumn>
											<asp:BoundColumn DataField="PrecioUnitario" HeaderText="Precio Unitario" DataFormatString="{0:N2}"></asp:BoundColumn>
											<asp:BoundColumn DataField="SaldoInicialWarrant" HeaderText="Saldo Inicial Warrant" DataFormatString="{0:N2}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Cnt.Items" HeaderText="Cnt.Items"></asp:BoundColumn>
											<asp:BoundColumn DataField="SaldoActualWarrant" HeaderText="Saldo Actual Warrant" DataFormatString="{0:N2}">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaIngreso" HeaderText="Fecha Ingreso">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaVencimiento" HeaderText="Fecha Vencimiento">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FechaVencimientoLimite" HeaderText="Fecha L&#237;mite"></asp:BoundColumn>
											<asp:BoundColumn DataField="Nro.Garant&#237;a" HeaderText="Nro Garant&#237;a">
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Pror.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id=chkProrroga runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.Prorroga") %>' Enabled="False">
													</asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Lib.Cont.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id=chkContable runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.Contable") %>' Enabled="False">
													</asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Elect.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id=chkElectronico runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.Electrónico") %>' Enabled="False">
													</asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="TipoWarrant" HeaderText="Tipo Warrant"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
											Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
