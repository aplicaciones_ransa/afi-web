<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ProrrogaDetalle.aspx.vb" Inherits="ProrrogaDetalle" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ProrrogaDetalle</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	
        
        $(function () {
            $("#txtFechaObligacion").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaObligacion").click(function () {
                $("#txtFechaObligacion").datepicker('show');
            });

 
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha	

        function Validar() {
            var FechLimite;
            var FechAnterior;
            var FechProrroga;
            FechLimite = document.all["txtFechaLimite"].value;
            FechAnterior = document.all["txtFechaAnterior"].value;
            FechProrroga = document.all["txtFechaObligacion"].value;

            if (FechLimite.length < 10 || FechLimite.length > 10) {
                alert("La Fecha Limite no esta bien, comuniquese con el administrador");
                return false;
            }
            if (FechAnterior.length < 10 || FechAnterior.length > 10) {
                alert("La Fecha Anterior de vencimiento no esta bien, comuniquese con el administrador");
                return false;
            }

            FechLimite = FechLimite.substr(6, 4) + FechLimite.substr(3, 2) + FechLimite.substr(0, 2)
            FechAnterior = FechAnterior.substr(6, 4) + FechAnterior.substr(3, 2) + FechAnterior.substr(0, 2)
            FechProrroga = FechProrroga.substr(6, 4) + FechProrroga.substr(3, 2) + FechProrroga.substr(0, 2)
            if (FechProrroga < FechAnterior) {
                alert("La fecha de obligaci�n debe ser mayor a la fecha de la ultima prorroga");
                return false;
            }
            if (FechProrroga > FechLimite) {
                alert("La fecha de vencimiento debe ser menor a la fecha limite del documento");
                return false;
            }
        }
    </script>
    <script>
        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="Table9" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        cellspacing="6" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="6">
                                <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%">
                                <table id="Table20" cellspacing="0" cellpadding="0" width="630" align="center" border="0">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="Text" width="20%">Tipo Documento:</td>
                                                    <td width="28%">
                                                        <asp:TextBox ID="txtTipoDocumento" runat="server" CssClass="Text" Width="150px"></asp:TextBox></td>
                                                    <td class="Text" width="20%">Nro Warrant:</td>
                                                    <td width="36%">
                                                        <asp:TextBox ID="txtNroWarrant" runat="server" CssClass="Text" Width="180px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Cod. Depositante:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtCodCliente" runat="server" CssClass="Text" Width="150px" EnableViewState="False"></asp:TextBox></td>
                                                    <td class="Text">Nom. Depositante:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtCliente" runat="server" CssClass="Text" Width="180px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Cod. Endosatario:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtCodFina" runat="server" CssClass="Text" Width="150px" EnableViewState="False"></asp:TextBox></td>
                                                    <td class="Text">Nom. Endosatario:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtFina" runat="server" CssClass="Text" Width="180px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Estado:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboEstado" runat="server" CssClass="Text" Width="150px"></asp:DropDownList></td>
                                                    <td class="Text">Secuencia Prorroga:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtSecuencia" runat="server" CssClass="Text" Width="180px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Fecha Venc Anterior:</td>
                                                    <td>
                                                        <input class="Text" id="txtFechaAnterior" maxlength="10" size="15" name="txtFechaAnterior"
                                                            runat="server"></td>
                                                    <td class="Text">Fecha Venc L�mite:</td>
                                                    <td>
                                                        <input class="Text" id="txtFechaLimite" maxlength="10" size="15" name="txtFechaLimite"
                                                            runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Saldo:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtSaldoSICO" runat="server" CssClass="Text" Width="150px"></asp:TextBox></td>
                                                    <td class="Text"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Mercader�a:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtMercaderia" runat="server" CssClass="Text" Width="470px"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td" valign="top" align="center">Ingreso deInformaci�n</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table id="Table3" cellspacing="0" cellpadding="0" width="630" align="center" border="0">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="Text" width="23%">Moneda y Saldo Adeudado:</td>
                                                    <td width="29%">
                                                        <asp:DropDownList ID="cboMonedaAdecuado" runat="server" CssClass="Text" Width="50px"></asp:DropDownList><input class="Text" id="txtSaldoAdecuado" onkeyup="Formato(this,event);" size="15" name="txtSaldoAdecuado"
                                                            runat="server">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtSaldoAdecuado"></asp:RequiredFieldValidator></td>
                                                    <td class="Text" width="20%">Moneda y Saldo Actual:</td>
                                                    <td width="28%">
                                                        <asp:DropDownList ID="cboMonedaActual" runat="server" CssClass="Text" Width="50px"></asp:DropDownList><input class="Text" id="txtSaldoActual" onkeyup="Formato(this,event);" size="15" name="txtSaldoActual"
                                                            runat="server">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtSaldoActual"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Fecha L�mite Obl:</td>
                                                    <td>
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaObligacion" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="10" name="txtFechaObligacion" runat="server"><input class="Text" id="btnFechaObligacion" 
                                                                type="button" value="..." name="btnFechaObligacion">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtFechaObligacion"></asp:RequiredFieldValidator></td>
                                                    <td class="Text"></td>
                                                    <td>
                                                        <input class="Text" id="txtFechaRegistro" type="hidden" maxlength="10" size="10" name="txtFechaRegistro"
                                                            runat="server"></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table id="Table2" cellspacing="4" cellpadding="0" width="630" align="center" border="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnGenerarPDF" runat="server" CssClass="btn" Width="100px" Text="Generar PDF"
                                                BackColor="Red" Font-Bold="True" ForeColor="White"></asp:Button></td>
                                        <td>
                                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn" Width="80px" Text="Regresar" CausesValidation="False"></asp:Button></td>
                                        <td></td>
                                        <td></td>
                                        <td width="500"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblMensaje" runat="server" CssClass="Error" EnableViewState="False"></asp:Label><input id="txtConCertificado" type="hidden" name="txtConCertificado" runat="server"><input id="txtModalidad" type="hidden" name="txtModalidad" runat="server"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
