Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.Data
Imports Infodepsa

Public Class LiberacionPendiente
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroLiberacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "25") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "25"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("NroRetiro") <> Nothing Then
                    pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("NroRetiro"))
                    Session.Remove("NroRetiro")
                End If
                Me.lblOpcion.Text = "Consultar Liberaciones Pendientes"
                BindDatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objDocumento.gGetBusquedaLiberacionPendiente(Me.txtNroWarrant.Text.Replace("'", ""), Me.txtNroLiberacion.Text.Replace("'", ""), Session.Item("IdTipoEntidad"), Session.Item("IdSico"))
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Public Sub imgAprobar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "25", "APROBAR LIBERACION PENDIENTE", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim strResult As String
            Dim strEmailsConAdjunto As String = ""
            Dim strEmailsSinAdjunto As String = ""
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim strCOD_EMPR As String = dgi.Cells(0).Text()
            Dim strCOD_UNID As String = dgi.Cells(1).Text()
            Dim strNRO_DOC_WARR As String = dgi.Cells(2).Text()
            Dim strID_TIPO_DOC As String = dgi.Cells(13).Text()
            Dim strNRO_LIBE As String = dgi.Cells(4).Text()
            Dim strCOD_BANC As String = dgi.Cells(14).Text()

            strResult = objDocumento.gInsLiberacionFlujo(strNRO_DOC_WARR, strNRO_LIBE, strID_TIPO_DOC, Session.Item("IdUsuario"), strCOD_UNID)
            If strResult.Substring(0, 2) = "XX" Then
                pr_IMPR_MENS("No se pudo aprobar el documento")
                Exit Sub
            Else
                pr_IMPR_MENS("Se emiti� la Liberaci�n nro: " & dgi.Cells(4).Text())
            End If
            dtMail = objDocumento.gEnviarMail(strNRO_DOC_WARR, strNRO_LIBE, Now.Year.ToString, strResult.Substring(0, 2), "S")
            If dtMail.Rows.Count > 0 Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("FLG_ADJU") = True Then
                        strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("EMAI") & ","
                    Else
                        strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
                If strEmailsConAdjunto <> "" Then
                    objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                    strResult.Substring(2, strResult.Length - 2) & " - " & Session.Item("NombreEntidad"),
                    "Estimados Se�ores:<br><br>" &
                    "Alma Per� ha emitido " & dtMail.Rows(0)("NOM_TIPDOC") & " N�: " & strNRO_LIBE &
                    "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNRO_DOC_WARR & "</FONT></STRONG><br>" &
                    "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strPath & "02" & strNRO_DOC_WARR & strNRO_LIBE & ".pdf")
                End If
                If strEmailsSinAdjunto <> "" Then
                    objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                    strResult.Substring(2, strResult.Length - 2) & " - " & Session.Item("NombreEntidad"),
                        "Estimados Se�ores:<br><br>" &
                    "Alma Per� ha emitido " & dtMail.Rows(0)("NOM_TIPDOC") & " N�: " & strNRO_LIBE &
                    "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNRO_DOC_WARR & "</FONT></STRONG><br>" &
                    "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
                End If
            End If
            dtMail.Dispose()
            dtMail = Nothing
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "25", "ANULAR LIBERACION PENDIENTE", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim strCOD_EMPR As String = dgi.Cells(0).Text()
            Dim strCOD_UNID As String = dgi.Cells(1).Text()
            Dim strNRO_DOC_WARR As String = dgi.Cells(2).Text()
            Dim strID_TIPO_DOC As String = dgi.Cells(13).Text()
            Dim strNRO_LIBE As String = dgi.Cells(4).Text()

            'Obtenemos el n�mero de liberaci�n despues de actualizar la informaci�n en el SICO
            pr_IMPR_MENS(objDocumento.gEliminarLiberacionPendiente(strCOD_EMPR, strCOD_UNID, strNRO_DOC_WARR, strID_TIPO_DOC, strNRO_LIBE, Session.Item("IdUsuario")))
            dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(15).Text(), "02")
            If dtMail.Rows.Count > 0 Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("EMAI") <> "" Then
                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                "Liberaci�n/Retiro anulado por el cliente - " & Session.Item("NombreEntidad"),
                "Estimados Se�ores:<br><br>" &
                "Se ha anulado la Liberaci�n/Retiro N�: " & strNRO_LIBE &
                "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNRO_DOC_WARR & "</FONT></STRONG><br>" &
                "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                "Usuario que anul� el documento: <STRONG><FONT color='#330099'>" & Session.Item("UsuarioLogin") & "</FONT></STRONG>" &
                "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
            End If
            dtMail.Dispose()
            dtMail = Nothing
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnAprobar As New ImageButton
        Dim btnCancelar As New ImageButton
        Dim intCierre As Integer
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Cells(10).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("IM_RETI"))
            If e.Item.DataItem("flg_virtual") = "N" Then
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
            Else
                e.Item.BackColor = System.Drawing.Color.LightGoldenrodYellow
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='LightGoldenrodYellow'")
            End If
            btnAprobar = CType(e.Item.FindControl("imgAprobar"), ImageButton)
            btnAprobar.Attributes.Add("onclick", "javascript:if(confirm('Desea aprobar la Liberaci�n/Retiro Nro " & e.Item.DataItem("NRO_LIBE") & "?')== false) return false;")
            btnCancelar = CType(e.Item.FindControl("imgCancelar"), ImageButton)
            btnCancelar.Attributes.Add("onclick", "javascript:if(confirm('Desea anular la Liberaci�n/Retiro Nro " & e.Item.DataItem("NRO_LIBE") & "?')== false) return false;")
            If Session.Item("IdTipoEntidad") = "03" And Session.Item("IdTipoUsuario") = "03" Then
                btnCancelar.Enabled = True
                btnAprobar.Enabled = True
            Else
                btnCancelar.Enabled = False
                btnAprobar.Enabled = False
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
