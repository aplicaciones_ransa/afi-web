Imports System.Data.SqlClient
Imports System.IO
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class SolicitarDocumento
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objUnidad As LibCapaNegocio.clsCNUnidad
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private bolApto As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnIngresarDoc As System.Web.UI.WebControls.Button
    'Protected WithEvents btnVerSolicitud As System.Web.UI.WebControls.Button
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents lblIDInicio As System.Web.UI.WebControls.Label
    'Protected WithEvents lblIDFinal As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCurrentPage As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTotalPages As System.Web.UI.WebControls.Label
    'Protected WithEvents lbtnPrevious As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lbtnNext As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lblNota As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtDescripcion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroCaja As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboTipoDocumento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroDocumento As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    'Protected WithEvents txtFechaDocDesde As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaDocHasta As System.Web.UI.HtmlControls.HtmlInputText

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()

        If (Request.IsAuthenticated) And ValidarPagina("PAG5") Then
            Try
                Inicializar()
                Me.lblError.Text = ""
                PoliticaCache()
                If Not IsPostBack Then
                    CargarCombos()
                    InicializaBusqueda()
                    dgdResultado.CurrentPageIndex = 1
                    Me.dgdResultado.CurrentPageIndex = 1
                    If ValidarCriteriosBusqueda() = True Then
                        LlenaGrid(0)
                        CalcularPaginado()
                        RegistrarCadenaBusqueda()
                    End If
                    btnIngresarDoc.Attributes.Add("onclick", "return ValidaCheckBox();")
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub CalcularPaginado()
        Dim intTotalPaginas As Int16
        lbtnPrevious.Enabled = False
        intTotalPaginas = CInt(lblTotalPages.Text)
        If intTotalPaginas > 1 Then
            lbtnNext.Enabled = True
        Else
            lbtnNext.Enabled = False
        End If
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                CType(e.Item.FindControl("txtDetalle"), TextBox).Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Try
            Dim itemType As ListItemType = CType(e.Item.ItemType, ListItemType)
            Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
            Dim param1 As String
            Dim param2 As String
            Dim param3 As String
            Dim param4 As String
            Dim param5 As String
            Dim param6 As String
            If itemType <> ListItemType.Footer And itemType <> ListItemType.Separator Then
                If itemType = ListItemType.Header Then
                Else
                    Dim idFila As String = dgdResultado.ClientID & "_row" & e.Item.ItemIndex.ToString
                    e.Item.Attributes.Add("id", idFila)
                    Dim chkSel As CheckBox = CType(e.Item.FindControl("chkSel"), CheckBox)
                    Dim lnkBtn As HyperLink = CType(e.Item.FindControl("lnkUnidad"), HyperLink)
                    Dim idCheckBox As String = CType(chkSel, CheckBox).ClientID
                    e.Item.Attributes.Add("onMouseMove", "Javascript:chkSelect_OnMouseMove(" & idFila & "," & idCheckBox & ")")
                    e.Item.Attributes.Add("onMouseOut", "Javascript:chkSelect_OnMouseOut(" & idFila & "," & idCheckBox & "," & e.Item.ItemIndex & ")")
                    chkSel.Attributes.Add("onClick", "Javascript:chkSelect_OnClick('" & idFila & "','" & idCheckBox & "'," & e.Item.ItemIndex & ")")
                    If Convert.ToString(e.Item.DataItem("FLG_SEL")) = "1" Then
                        chkSel.Enabled = False
                    End If

                    If InStr(strSeleccionados, "SDOC" & Convert.ToString(e.Item.DataItem("ID_UNID")) & Convert.ToString(e.Item.DataItem("NU_CONT")) & "-" & Convert.ToString(e.Item.DataItem("NU_SECU_DIVI"))) <> 0 Or Convert.ToString(e.Item.DataItem("ST_UBIC")) = "TEM" Or Convert.ToString(e.Item.DataItem("ST_UBIC")) = "CON" Then
                        chkSel.Checked = True
                        chkSel.Enabled = False
                        param1 = IIf(IsDBNull(e.Item.DataItem("ID_UNID")), "", e.Item.DataItem("ID_UNID"))
                        param2 = IIf(IsDBNull(e.Item.DataItem("DE_CRIT_DESD")), "", e.Item.DataItem("DE_CRIT_DESD"))
                        param3 = IIf(IsDBNull(e.Item.DataItem("DIVI")), "", e.Item.DataItem("DIVI"))
                        param4 = IIf(IsDBNull(e.Item.DataItem("DE_NOMB_PERS")), "", e.Item.DataItem("DE_NOMB_PERS"))
                        'param5 = IIf(IsDBNull(e.Item.DataItem("FE_CIER")), "", e.Item.DataItem("FE_CIER"))
                        param5 = IIf(IsDBNull(e.Item.DataItem("FE_USUA_CREA")), "", e.Item.DataItem("FE_USUA_CREA"))
                        param6 = IIf(IsDBNull(e.Item.DataItem("NU_REQU")), "", e.Item.DataItem("NU_REQU"))
                        lnkBtn.Attributes.Add("onclick", "Javascript:AbrirDetalle('" & param1 & "', '" & param2 & "', '" & param3 & "', '" & param4 & "', '" & param5 & "', '" & param6 & "')")
                    End If
                    If Convert.ToString(e.Item.DataItem("ST_DEVO_GENE")) = "N" Then
                        chkSel.Checked = True
                        chkSel.Enabled = False
                        param1 = IIf(IsDBNull(e.Item.DataItem("ID_UNID")), "", e.Item.DataItem("ID_UNID"))
                        param2 = IIf(IsDBNull(e.Item.DataItem("DE_CRIT_DESD")), "", e.Item.DataItem("DE_CRIT_DESD"))
                        param3 = IIf(IsDBNull(e.Item.DataItem("DIVI")), "", e.Item.DataItem("DIVI"))
                        param4 = IIf(IsDBNull(e.Item.DataItem("DE_NOMB_PERS")), "", e.Item.DataItem("DE_NOMB_PERS"))
                        ' param5 = IIf(IsDBNull(e.Item.DataItem("FE_CIER")), "", e.Item.DataItem("FE_CIER")) 'Format(e.Item.DataItem("FE_CIER"), "dd/MM/yyyy"))
                        param5 = IIf(IsDBNull(e.Item.DataItem("FE_USUA_CREA")), "", e.Item.DataItem("FE_USUA_CREA")) 'Format(e.Item.DataItem("FE_CIER"), "dd/MM/yyyy"))
                        param6 = IIf(IsDBNull(e.Item.DataItem("NU_REQU")), "", e.Item.DataItem("NU_REQU"))
                        lnkBtn.Attributes.Add("onclick", "Javascript:AbrirDetalle('" & param1 & "', '" & param2 & "', '" & param3 & "', '" & param4 & "', '" & param5 & "', '" & param6 & "')")
                    End If
                    If chkSel.Checked = False Then
                        e.Item.Cells(4).Text = e.Item.DataItem("DES")
                    End If
                End If
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid(0)
        CalcularPaginado()
    End Sub

    Private Sub LlenaGrid(ByVal pintIDConsulta As Double, Optional ByVal pstrDireccionPagina As String = ">",
                Optional ByVal pstrEstTotalRegistros As String = "S")
        Dim intTotalRegistros As Integer = 0
        Dim intTotalPaginas As Decimal = 0
        Try
            objUnidad = New LibCapaNegocio.clsCNUnidad
            objFuncion = New LibCapaNegocio.clsFunciones

            objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtMostrarDocumentosHP(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                               cboArea.SelectedItem.Value, cboTipoDocumento.SelectedItem.Value, txtNroCaja.Text.Trim, txtNroDocumento.Text.Trim,
                               txtFechaDocDesde.Value, txtFechaDocHasta.Value, txtDescripcion.Text.Trim, pintIDConsulta, pstrDireccionPagina, pstrEstTotalRegistros, CType(Session.Item("UsuarioLogin"), String)))

            If pstrEstTotalRegistros = "S" Then
                intTotalRegistros = objUnidad.intFilasAfectadas
                lblRegistros.Text = intTotalRegistros.ToString & " registros"

                intTotalPaginas = intTotalRegistros / 50
                If intTotalPaginas > Fix(intTotalPaginas) Then
                    intTotalPaginas = Fix(intTotalPaginas) + 1
                Else
                    intTotalPaginas = Fix(intTotalPaginas)
                End If
                lblTotalPages.Text = intTotalPaginas.ToString
            End If
            lblCurrentPage.Text = dgdResultado.CurrentPageIndex.ToString
            lblIDInicio.Text = objUnidad.intIDConsultaInicio.ToString
            lblIDFinal.Text = objUnidad.intIDConsultaFinal.ToString
            If objUnidad.intFilasAfectadas > 0 Then
                lblNota.Visible = True
            Else
                lblNota.Visible = False
            End If
        Catch ex As Exception
            dgdResultado.DataSource = Nothing
            dgdResultado.DataBind()
            intTotalRegistros = 0
            lblRegistros.Text = intTotalRegistros.ToString & " registros"
            If intTotalRegistros Mod 50 = 0 Then
                intTotalPaginas = intTotalRegistros / 50
            Else
                intTotalPaginas = (intTotalRegistros / 50) + 1
            End If
            lblTotalPages.Text = intTotalPaginas.ToString
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 1
        If ValidarCriteriosBusqueda() = True Then
            LlenaGrid(0)
            CalcularPaginado()
            RegistrarCadenaBusqueda()
        End If
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "DEPSAFIL", "SOLICITAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusqueda", "?ts=D&?ca=" & cboArea.SelectedIndex & "&?td=" & cboTipoDocumento.SelectedIndex &
            "&?nd=" & txtNroDocumento.Text & "&?nc=" & txtNroCaja.Text & "&?de=" & txtDescripcion.Text & "&?fi=" & txtFechaDocDesde.Value &
            "&?ff=" & txtFechaDocHasta.Value & "&")
    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        Me.txtFechaDocDesde.Value = objFuncion.gstrFechaMes(0)
        Me.txtFechaDocHasta.Value = objFuncion.gstrFechaMes(1)
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")

        objComun.gCargarComboTipoDocumento(cboTipoDocumento, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboTipoDocumento(cboTipoDocumento, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
        btnBuscar_Click(0, e)
    End Sub

    Private Sub btnIngresarDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIngresarDoc.Click
        If ValidarFormulario() = True Then
            IngresarDocumento()
        End If
    End Sub


    'Private Sub IngresarDocumento()
    '    If CStr(Session.Item("ProcesoCaja")) = "PENDIENTE" Or CStr(Session.Item("ProcesoOtros")) = "PENDIENTE" Then
    '        Session.Add("RequerimientoActivo", "0")
    '        Session.Add("ProcesoCaja", "LISTO")
    '        Session.Add("ProcesoOtros", "LISTO")
    '    End If

    '    If CStr(Session.Item("RequerimientoActivo")) = "0" Then
    '        'Ingresa cabecera
    '        IngresarRequerimiento()
    '        Session.Add("ProcesoDocumentos", "PENDIENTE")
    '    End If
    '    If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
    '        'ingresa detalle
    '        IngresarConsultas()
    '    End If
    '    If bolApto = True Then
    '        Session.Add("PaginaAnterior", "SolicitarDocumento.aspx")
    '        Response.Redirect("ConsultasGeneradas.aspx", False)
    '    Else
    '        Me.dgdResultado.CurrentPageIndex = 1
    '        If ValidarCriteriosBusqueda() = True Then
    '            LlenaGrid(0)
    '            CalcularPaginado()
    '            RegistrarCadenaBusqueda()
    '        End If
    '    End If
    '    'Response.Write("<script language='JavaScript'> alert('PRUEBA') </script>")
    'End Sub

    Private Sub IngresarDocumento()
        If CStr(Session.Item("ProcesoCajaDOCUMENTO")) = "PENDIENTE" Or CStr(Session.Item("ProcesoOtros")) = "PENDIENTE" Then
            '    If CStr(Session.Item("ProcesoDocumentos")) = "PENDIENTE" Or CStr(Session.Item("ProcesoOtros")) = "PENDIENTE" Then
            Session.Add("RequerimientoActivo", "0")
            Session.Add("ProcesoCaja", "LISTO")
            Session.Add("ProcesoOtros", "LISTO")
        End If

        If CStr(Session.Item("RequerimientoActivo")) = "0" Then
            'Ingresa cabecera
            IngresarRequerimiento()
            Session.Add("ProcesoDocumentos", "PENDIENTE")
        End If
        If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
            'ingresa detalle
            IngresarConsultas()
        End If
        If bolApto = True Then
            Session.Add("PaginaAnterior", "SolicitarDocumento.aspx")
            Response.Redirect("ConsultasGeneradas.aspx", False)
        Else
            Me.dgdResultado.CurrentPageIndex = 1
            If ValidarCriteriosBusqueda() = True Then
                LlenaGrid(0)
                CalcularPaginado()
                RegistrarCadenaBusqueda()
            End If
        End If
        'Response.Write("<script language='JavaScript'> alert('PRUEBA') </script>")
    End Sub



    'borra jAURIS
    'Private Sub IngresarRequerimiento()
    '    Try
    '        objRequ = New LibCapaNegocio.clsCNRequerimiento
    '        Session.Add("CodArea", cboArea.SelectedValue)
    '        If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "", _
    '                    CStr(Session.Item("CodClieAdic")), "CAJ", CStr(Session.Item("CodPersAuto")), _
    '                    CStr(Session.Item("DesPersAuto")), cboArea.SelectedValue, "dir envio", "N", _
    '                   "obsevacion", 0, CStr(Session.Item("UsuarioLogin")), "DOC") = True Then
    '            lblError.Text = "El registro se grab� con �xito nroRequTemp: " & objRequ.intNumRequTemp
    '            Session.Add("RequerimientoActivo", objRequ.intNumRequTemp)
    '            Session.Add("Seleccionados", "-")
    '        Else
    '            lblError.Text = objRequ.strMensajeError
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub
    Private Sub IngresarRequerimiento()
        Try
            Dim strArea As String
            Dim dgItem As DataGridItem
            Dim strSeleccionados As String = Session.Item("Seleccionados").ToString

            objRequ = New LibCapaNegocio.clsCNRequerimiento
            Session.Add("CodArea", cboArea.SelectedValue)

            If cboArea.SelectedValue = "0" Then
                For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                    dgItem = Me.dgdResultado.Items(intI)
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                        If dgItem.Cells(16).Text = "" Or dgItem.Cells(16).Text = "&nbsp;" Then
                            If InStr(strSeleccionados, "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text) = 0 And dgItem.Cells(12).Text = "0" Then

                                strArea = dgItem.Cells(11).Text
                                Exit For
                            End If
                        End If
                    End If
                Next
            Else
                strArea = cboArea.SelectedValue

            End If

            'If InStr(strSeleccionados, "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text) Then




            'If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "", _
            '      CStr(Session.Item("CodClieAdic")), "CAJ", CStr(Session.Item("CodPersAuto")), _
            '      CStr(Session.Item("DesPersAuto")), strArea, "dir envio", "N", _
            '     "obsevacion", 0, CStr(Session.Item("UsuarioLogin")), "COK") = True Then
            '    lblError.Text = "El registro se grabo con �xito nroRequTemp: " & objRequ.intNumRequTemp
            '    Session.Add("RequerimientoActivo", objRequ.intNumRequTemp)
            '    Session.Add("Seleccionados", "-")

            If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "",
                  CStr(Session.Item("CodClieAdic")), "CAJ", CStr(Session.Item("CodPersAuto")),
                  CStr(Session.Item("DesPersAuto")), strArea, "dir envio", "N",
                 "obsevacion", 0, CStr(Session.Item("UsuarioLogin")), "COK") = True Then
                lblError.Text = "El registro se grabo con �xito nroRequTemp: " & objRequ.intNumRequTemp
                Session.Add("RequerimientoActivo", objRequ.intNumRequTemp)
                Session.Add("Seleccionados", "-")
            Else
                lblError.Text = objRequ.strMensajeError
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Sub IngresarConsultas()

        'Dim strDetalle As String
        'Dim dgItem As DataGridItem
        'Dim S As String = "a"
        'Dim strFalg As String
        'Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivo"))
        'Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultas"))
        'Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        'Try
        '    For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
        '        dgItem = Me.dgdResultado.Items(intI)
        '        If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True And CType(dgItem.FindControl("chkSel"), CheckBox).Enabled = True Then
        '            If dgItem.Cells(16).Text = "" Or dgItem.Cells(16).Text = "&nbsp;" Then
        '                If InStr(strSeleccionados, "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text) = 0 And dgItem.Cells(12).Text = "0" Then
        '                    strSeleccionados &= "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text & ","
        '                    objConsulta = New LibCapaNegocio.clsCNConsulta
        '                    objConsulta.intNumRequTemp = intNumRequTemp
        '                    strDetalle = CType(dgItem.FindControl("txtDetalle"), TextBox).Text

        '                    '*****************************************************************
        '                    'verificar si el item selccionado ha sido tomado por otro usuario

        '                    If objConsulta.gbolConsultaTempApropiados(CStr(Session.Item("CodCliente")), "CAJ", dgItem.Cells(1).Text, dgItem.Cells(10).Text, dgItem.Cells(8).Text, dgItem.Cells(9).Text) = False Then
        '                        ' lblError.Text = "El documento de la caja N� " & dgItem.Cells(4).Text & "\n" & " del   �rea: " & dgItem.Cells(3).Text & "\n" & " esta solicitado temporalmente por otro usuario: " + objConsulta.strUsuarioTemp
        '                        lblError.Text = "El documento: " & dgItem.Cells(4).Text & "\n\n" & "�rea: " & dgItem.Cells(3).Text & "\n\n" & "Solicitado temporalmente: " + objConsulta.strUsuarioTemp
        '                        pr_IMPR_MENS(lblError.Text)
        '                        lblError.Text = ""
        '                        ' Response.Write("<script language='JavaScript'> alert('cesar') </script>")
        '                        'Response.Write("<script language='JavaScript'>{document.getElementById('altaOK').style.visibility='visible';}</script>");) pero me da un error de JavaScript en la p�gina. 
        '                        bolApto = False
        '                        strFalg = 1
        '                        Exit For
        '                    End If
        '                    strFalg = 0
        '                    '*****************************************************************

        '                    If objConsulta.gbolInsConsultaTemp(CStr(Session.Item("CodCliente")), "CAJ", _
        '                 "", dgItem.Cells(1).Text, "", dgItem.Cells(10).Text, "", _
        '                 strDetalle, dgItem.Cells(11).Text, dgItem.Cells(10).Text, dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text, _
        '                  dgItem.Cells(8).Text, dgItem.Cells(9).Text, CStr(Session.Item("UsuarioLogin")), "COK") = True Then
        '                        lblError.Text = lblError.Text & " IDCons : " & objConsulta.intNumConsulta
        '                        intNumConsultas = intNumConsultas + 1
        '                    Else
        '                        lblError.Text = objRequ.strMensajeError
        '                        bolApto = False
        '                        Exit For
        '                    End If
        '                End If
        '            End If
        '        End If
        '    Next
        '    If strFalg = 0 Then
        '        Session.Add("NroConsultas", intNumConsultas)
        '        Session.Add("Seleccionados", strSeleccionados)
        '        bolApto = True
        '    End If
        'Catch ex As Exception
        '    bolApto = False
        '    lblError.Text &= " " & ex.Message
        'End Try

        Dim strDetalle As String
        Dim dgItem As DataGridItem
        Dim S As String = "a"
        Dim strFalg As String
        Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivo"))
        Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultas"))
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        Try
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True And CType(dgItem.FindControl("chkSel"), CheckBox).Enabled = True Then
                    If dgItem.Cells(16).Text = "" Or dgItem.Cells(16).Text = "&nbsp;" Then
                        If InStr(strSeleccionados, "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text) = 0 And dgItem.Cells(12).Text = "0" Then
                            strSeleccionados &= "SDOC" & dgItem.Cells(1).Text & dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text & ","
                            objConsulta = New LibCapaNegocio.clsCNConsulta
                            objConsulta.intNumRequTemp = intNumRequTemp
                            strDetalle = CType(dgItem.FindControl("txtDetalle"), TextBox).Text

                            '*****************************************************************
                            'verificar si el item selccionado ha sido tomado por otro usuario

                            If objConsulta.gbolConsultaTempApropiados(CStr(Session.Item("CodCliente")), "CAJ", dgItem.Cells(1).Text, dgItem.Cells(10).Text, dgItem.Cells(8).Text, dgItem.Cells(9).Text) = False Then
                                ' lblError.Text = "El documento de la caja N� " & dgItem.Cells(4).Text & "\n" & " del   �rea: " & dgItem.Cells(3).Text & "\n" & " esta solicitado temporalmente por otro usuario: " + objConsulta.strUsuarioTemp
                                lblError.Text = "El documento: " & dgItem.Cells(4).Text & "\n\n" & "�rea: " & dgItem.Cells(3).Text & "\n\n" & "Solicitado temporalmente: " + objConsulta.strUsuarioTemp
                                pr_IMPR_MENS(lblError.Text)
                                lblError.Text = ""
                                ' Response.Write("<script language='JavaScript'> alert('cesar') </script>")
                                'Response.Write("<script language='JavaScript'>{document.getElementById('altaOK').style.visibility='visible';}</script>");) pero me da un error de JavaScript en la p�gina. 
                                bolApto = False
                                strFalg = 1
                                Exit For
                            End If
                            strFalg = 0
                            '*****************************************************************

                            If objConsulta.gbolInsConsultaTemp(CStr(Session.Item("CodCliente")), "CAJ",
                         "", dgItem.Cells(1).Text, "", dgItem.Cells(10).Text, "",
                         strDetalle, dgItem.Cells(11).Text, dgItem.Cells(10).Text, dgItem.Cells(8).Text & "-" & dgItem.Cells(9).Text,
                          dgItem.Cells(8).Text, dgItem.Cells(9).Text, CStr(Session.Item("UsuarioLogin")), "COK") = True Then
                                lblError.Text = lblError.Text & " IDCons : " & objConsulta.intNumConsulta
                                intNumConsultas = intNumConsultas + 1
                            Else
                                lblError.Text = objRequ.strMensajeError
                                bolApto = False
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next
            If strFalg = 0 Then
                Session.Add("NroConsultas", intNumConsultas)
                Session.Add("Seleccionados", strSeleccionados)
                bolApto = True
            End If
        Catch ex As Exception
            bolApto = False
            lblError.Text &= " " & ex.Message
        End Try
    End Sub

    Private Sub btnVerSolicitud_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerSolicitud.Click
        VerSolicitud(e)
    End Sub
    Private Sub VerSolicitud(ByVal e As System.EventArgs)

        If CStr(Session.Item("RequerimientoActivo")) = "0" Then
            lblError.Text = "No se ha ingresado documentos para ver la solicitud. Para visualizar la solicitud seleccione e ingrese los documentos."
            btnBuscar_Click(0, e)
        Else
            If Session.Item("ProcesoDocumentos").ToString = "PENDIENTE" Then
                Session.Add("PaginaAnterior", "SolicitarDocumento.aspx")
                Response.Redirect("ConsultasGeneradas.aspx", False)
            End If
        End If
    End Sub



    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            If Session.Item("PaginasPermitidas") = Nothing Then
                Return False
                Exit Function
            End If
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function
    Public Sub Inicializar()
        Me.txtDescripcion.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtNroCaja.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtNroDocumento.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
    End Sub

    Private Function ValidarFormulario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            If dgdResultado.Items.Count = 0 Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado registro(s). Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            ElseIf dgdResultado.Items.Count > 0 Then
                Dim dgItem As DataGridItem
                For Each dgItem In dgdResultado.Items
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                        bolRetorno = True
                        Exit For
                    End If
                Next
                If bolRetorno = False Then
                    lblError.Text = "No se ha seleccionado registro(s). Seleccione por lo menos un registro para continuar."
                End If
            End If
            If cboArea.SelectedValue = "" Or cboArea.SelectedValue = Nothing Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado un �rea. Seleccione un �rea para poder identificar la solicitud y poder continuar el siguiente paso."
            End If
            Return bolRetorno
        Catch ex As Exception
            lblError.Text = ex.Message
            Return False
        End Try
    End Function

    Public Sub chkSel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim dgItem As DataGridItem
        Dim chkSel As CheckBox = CType(sender, CheckBox)
        dgItem = (CType(chkSel.Parent.Parent, DataGridItem))
        If chkSel.Checked = True Then
            dgItem.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
        End If
    End Sub

    Public Function EncontrarVariable(ByVal pstrCadenaBusqueda As String, ByVal pstrVar As String) As String
        Dim intPosicion As Integer = InStr(pstrCadenaBusqueda, pstrVar)
        Dim strValor As String = ""
        If intPosicion = 0 And pstrCadenaBusqueda = "" Then
            Return ""
        Else
            intPosicion += pstrVar.Length
            strValor = pstrCadenaBusqueda.Substring(intPosicion, pstrCadenaBusqueda.IndexOf("&", intPosicion) - intPosicion)
            Return strValor
        End If
    End Function

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        If Session.Item("CadenaBusqueda") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusqueda").ToString
            If EncontrarVariable(strCadenaBusqueda, "?ts") = "D" Then
                SeleccionaItemCombo(cboArea, CInt(EncontrarVariable(strCadenaBusqueda, "?ca")))
                objComun.gCargarComboTipoDocumento(cboTipoDocumento, CStr(Session.Item("CodCliente")), cboArea.SelectedValue)
                SeleccionaItemCombo(cboTipoDocumento, CInt(EncontrarVariable(strCadenaBusqueda, "?td")))
                txtNroDocumento.Text = EncontrarVariable(strCadenaBusqueda, "?nd")
                txtNroCaja.Text = EncontrarVariable(strCadenaBusqueda, "?nc")
                txtDescripcion.Text = EncontrarVariable(strCadenaBusqueda, "?de")
                txtFechaDocDesde.Value = EncontrarVariable(strCadenaBusqueda, "?fi")
                txtFechaDocHasta.Value = EncontrarVariable(strCadenaBusqueda, "?ff")
            End If
        End If
    End Sub

    Public Sub SeleccionaItemCombo(ByVal objcombo As DropDownList, ByVal intIndex As Integer)
        Dim cboItem As ListItem
        Dim intI As Integer = 0
        For intI = 0 To objcombo.Items.Count - 1
            If (intI = intIndex) Then
                objcombo.Items(intI).Selected = True
            Else
                objcombo.Items(intI).Selected = False
            End If
        Next
    End Sub

    Public Sub PoliticaCache()
        HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddYears(-10))
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.Cache.SetNoStore()
    End Sub

    Public Sub PreviousPage1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim intPaginaActual As Integer = CInt(lblCurrentPage.Text)
        Dim intTotalPaginas As Integer = CInt(lblTotalPages.Text)

        If intPaginaActual > 1 Then
            dgdResultado.CurrentPageIndex -= 1
            LlenaGrid(CDbl(lblIDInicio.Text), "<", "N")
        End If
        If dgdResultado.CurrentPageIndex = 1 Then
            lbtnPrevious.Enabled = False
        End If
        If dgdResultado.CurrentPageIndex < intTotalPaginas Then
            lbtnNext.Enabled = True
        End If
    End Sub

    Public Sub NextPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim intPaginaActual As Integer = CInt(lblCurrentPage.Text)
        Dim intTotalPaginas As Integer = CInt(lblTotalPages.Text)

        If intPaginaActual < intTotalPaginas Then
            dgdResultado.CurrentPageIndex += 1
            LlenaGrid(CDbl(lblIDFinal.Text), ">", "N")
        End If
        If dgdResultado.CurrentPageIndex = intTotalPaginas Then
            lbtnNext.Enabled = False
        End If
        If dgdResultado.CurrentPageIndex > 1 Then
            lbtnPrevious.Enabled = True
        End If
    End Sub

    Private Function ValidarCriteriosBusqueda() As Boolean
        Dim blnRetorno As Boolean = False
        If cboArea.SelectedItem.Value = "-" Or cboArea.SelectedItem.Value = "" Then
            lblError.Text = "No se ha seleccionado �rea. Seleccione un �rea para continuar."
            blnRetorno = False
        Else
            blnRetorno = True
        End If
        Return blnRetorno
    End Function
    Public Sub lnkUnidad_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim dgi As DataGridItem
        Dim imgEditar As ImageButton = CType(sender, ImageButton)
        'dgi = CType(lnkUnidad.Parent.Parent, DataGridItem)
        Session.Add("CodRequerimiento", dgi.Cells(0).Text())
    End Sub


    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objUnidad Is Nothing) Then
            objUnidad = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
