Imports System.Data
Imports Library.AccesoDB
Public Class ValidaCertificado
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCodUsuario As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnAceptar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnReintentar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Dim strCA As String
            Dim strFchExp As String
            Dim strCert As String
            Dim strCod As String
            Dim dtCert As New DataTable
            Dim strMensaje As String
            Try
                strCert = Request.QueryString("SN")
                strCA = Request.QueryString("CA")
                strFchExp = Request.QueryString("FE")
                strCod = Request.QueryString("COD")
                strFchExp = Mid(strFchExp, 7, 4) + Mid(strFchExp, 4, 2) + Mid(strFchExp, 1, 2)
                dtCert = objDocumento.gGetValidaCertificado(strCod, strCert, strCA, strFchExp)
                If dtCert.Rows(0)(0) = 0 Then
                    For i As Integer = 0 To dtCert.Rows.Count - 1
                        strMensaje += " - " & dtCert.Rows(i)(1) & "<br>"
                    Next
                    Session.Add("NroCertificado", strCert)
                    Me.lblError.Text = strMensaje
                    btnAceptar.Visible = True
                Else
                    For i As Integer = 0 To dtCert.Rows.Count - 1
                        strMensaje += " - " & dtCert.Rows(i)(1) & "<br>"
                    Next
                    Me.lblError.Text = strMensaje
                    btnReintentar.Visible = True
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            Finally
                dtCert.Dispose()
                dtCert = Nothing
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Response.Redirect("DocumentoConsultar.aspx?Estado=01", False)
    End Sub

    Private Sub btnReintentar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReintentar.Click
        Response.Redirect("SendCertificado.aspx")
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
