<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmRetiroSolicitud.aspx.vb" Inherits="AlmRetiroSolicitud"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Solicitud de Retiro</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript" type="text/javascript">
		function EventoEnter()
	  	{
			if(window.event.keyCode==13)
			{
    		 	var btn = document.getElementById('btnBuscar');
			 	if (btn) btn.click();
				window.focus();	
    		}
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" onkeypress="EventoEnter();" method="post" runat="server">
			<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
							border="0" cellSpacing="0" cellPadding="0" width="100%" height="400">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table3" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="10"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<P><asp:datagrid id="dgPedido" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
														AutoGenerateColumns="False">
														<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
														<ItemStyle CssClass="gvRow"></ItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR"></asp:BoundColumn>
															<asp:BoundColumn DataField="NU_SECU" HeaderText="Item"></asp:BoundColumn>
															<asp:BoundColumn DataField="DE_MERC" HeaderText="Mercaderia"></asp:BoundColumn>
															<asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Producto"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="LOTE" HeaderText="Lote"></asp:BoundColumn>
															<asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unidad"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="FE_VCTO_MERC"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="CO_TIPO_BULT"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="NU_UNID_SALD"></asp:BoundColumn>
															<asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Cnt a Retirar" DataFormatString="{0:N6}"></asp:BoundColumn>
															<asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Bulto a Retirar" DataFormatString="{0:N6}"></asp:BoundColumn>
															<asp:TemplateColumn HeaderText="Anular">
																<HeaderStyle Width="30px"></HeaderStyle>
																<ItemTemplate>
																	<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="30" border="0">
																		<TR>
																			<TD align="center">
																				<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" ToolTip="Eliminar Item"
																					ImageUrl="Images/anulado.gif"></asp:ImageButton></TD>
																		</TR>
																	</TABLE>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn Visible="False" DataField="CO_ALMA_ACTU"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="CO_UNID" HeaderText="CO_UNID"></asp:BoundColumn>
															<asp:BoundColumn Visible="False" DataField="CO_MODA_MOVI" HeaderText="Tipo de Retiro"></asp:BoundColumn>
														</Columns>
														<PagerStyle CssClass="gvPager"></PagerStyle>
													</asp:datagrid></P>
											</TD>
										</TR>
										<TR>
											<TD height="20" vAlign="top" align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
											<TD height="20" vAlign="top" width="100%"></TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top">Datos del Transportista:</TD>
											<TD vAlign="top" width="100%"></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="left">
												<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0" width="630" align="center">
													<TR>
														<TD class="Text">Brevete</TD>
														<TD></TD>
														<TD class="Text" colSpan="4"><asp:textbox id="txtBrevete" runat="server" CssClass="Text3" Width="104px"></asp:textbox><asp:button id="btnBuscar" runat="server" CssClass="btn" Width="60px" Text="Buscar"></asp:button></TD>
													</TR>
													<TR>
														<TD class="Text">Nombre del Chofer</TD>
														<TD class="Text">:</TD>
														<TD class="Text"><asp:textbox id="txtChofer" runat="server" CssClass="Text3" Width="250px"></asp:textbox></TD>
														<TD class="Text">DNI</TD>
														<TD class="Text">:</TD>
														<TD class="Text"><asp:textbox id="txtDNI" runat="server" CssClass="Text" Width="80px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text" width="17%">Placa del Veh�culo</TD>
														<TD class="Text" width="1%">:</TD>
														<TD class="Text" width="44%"><asp:textbox id="txtPlaca" runat="server" CssClass="Text3" Width="120px"></asp:textbox></TD>
														<TD class="Text" width="10%"></TD>
														<TD class="Text" width="1%"></TD>
														<TD class="Text" width="27%"></TD>
													</TR>
													<TR>
														<TD class="Text" width="17%">Destino</TD>
														<TD class="Text" width="1%">:</TD>
														<TD class="Text" colSpan="4"><asp:textbox id="txtDestino" runat="server" CssClass="Text3" Width="450px" MaxLength="100"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text" width="17%">Observaciones</TD>
														<TD class="Text" width="1%"></TD>
														<TD class="Text" colSpan="4"><asp:textbox id="txtCiaTransporte" runat="server" CssClass="Text3" Width="450px" MaxLength="85"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="left"><asp:checkbox id="chkPreDespacho" runat="server" Text="Con Pre-Despacho *"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="left"><asp:checkbox id="chkImprime" runat="server" Text="Imprime totales" Checked="True"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="left"><asp:checkbox id="chkPagoElec" runat="server" Text="Con Pago Electr�nico" Visible="False"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="right"><asp:hyperlink id="hlkRegresar" runat="server" CssClass="Text" Target="_parent" NavigateUrl="AlmPedidoNuevo.aspx">Regresar p�gina anterior</asp:hyperlink></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
													<TR>
														<TD width="80"><asp:button id="btnEnviar" runat="server" CssClass="btn" Text="Enviar"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Text" vAlign="top" align="center">* El servicio de Pre-Despacho tiene 
												costo de acuerdo a tarifa pactada, en caso de no tener tarifa para el servicio 
												de Pre-Despacho consultar con su ejecutivo de Ventas o llame al tel�fono: 
												611-6380</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
