<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoPDF.aspx.vb" Inherits="DocumentoPDF" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DocumentoPDF</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script src="JScripts/jquery.min.cache.js"></script>
		<script src="JScripts/jquery-ui.min.cache.js"></script>
		<LINK rel="stylesheet" type="text/css" href="Styles/jquery-ui.cache.css">		
		<script language="javascript">
		   var $dialog = null;
            jQuery.showModalDialog = function(options) {

            var defaultOptns = {
                url: null,
                dialogArguments: null,
                height: 'auto',
                width: 'auto',
                position: 'top',
                resizable: false,
                scrollable: false,
                onClose: function() { },
                returnValue: null,
                doPostBackAfterCloseCallback: false,
                postBackElementId: null
            };

            var fns = {
                close: function() {
                    opts.returnValue = $dialog.returnValue;
                    $dialog = null;
                    opts.onClose();
                    if (opts.doPostBackAfterCloseCallback) {
                        postBackForm(opts.postBackElementId);
                    }
                },
                adjustWidth: function() { $frame.css("width", "100%"); }
            };

            // build main options before element iteration
            var opts = $.extend({}, defaultOptns, options);
            var $frame = $('<iframe id="iframeDialog" />');

            if (opts.scrollable)
                $frame.css('overflow', 'auto');
                
            $frame.css({
                'padding': 0,
                'margin': 0,
                'padding-bottom': 2
            });

            var $dialogWindow = $frame.dialog({
                autoOpen: true,
                modal: true,
                width: opts.width,
                height: opts.height,
                resizable: opts.resizable,
                position: opts.position,
                overlay: {
                    opacity: 0.5,
                    background: "black"
                },
                close: fns.close,
                resizeStop: fns.adjustWidth
            });

            $frame.attr('src', opts.url);
            fns.adjustWidth();

            $frame.load(function() {
                if ($dialogWindow) {
                    var maxTitleLength = 50;
                    var title = $(this).contents().find("title").html();

                    if (title.length > maxTitleLength) {
                        title = title.substring(0, maxTitleLength) + '...';
                    }
                    $dialogWindow.dialog('option', 'title', title);
                }
            });

            $dialog = new Object();
            $dialog.dialogArguments = opts.dialogArguments;
            $dialog.dialogWindow = $dialogWindow;
            $dialog.returnValue = null;
        }

        function postBackForm(targetElementId) {
            var theform;
            theform = document.forms[0];
            theform.__EVENTTARGET.value = targetElementId;
            theform.__EVENTARGUMENT.value = "";
            theform.submit();
        }
        
        function SubmitForm2(strFirma)
		{
            document.getElementById("frmArchivo").action = "DocumentoFirmado.aspx?var=" + strFirma + "&Pagina=1";
            document.getElementById("frmArchivo").submit();             
		}
		function openWindow(strFirma)
		{				
			if (strFirma == 1)		 
			{
	   			SubmitForm2(1);
			}	
			else				
			{
			    var Argumentos = new Object();
				var url = 'Popup/LgnSolicitaFirma.aspx';
				$.showModalDialog({
					url: url,
					dialogArguments: null,
					heigh: 110,
					width: 360,
					scrollable: false,
					onClose: function(){ Argumentos = this.returnValue; 
					if(Argumentos == null)
					{
						window.alert('Usted a cancelado el proceso de firma!');
					}
					else
					{ //quiere decir que se ha devuelto una lista de contactos			    
						document.getElementById('txtContrasena').value = Argumentos[0];
						document.getElementById('txtNuevaContrasena').value = Argumentos[1];
						SubmitForm2(1);
					}		
					}
				});
					//var Argumentos = new Object();
					//Argumentos = window.showModalDialog("Popup/LgnSolicitaFirma.aspx", Argumentos, "dialogHeight:150px; dialogWidth:310px; edge:Raised; center:Yes; help:No; resizable:No; status:No; scroll=no"); 

					//if(Argumentos == null)
					//{
					//	window.alert('Usted a cancelado el proceso de firma!');
					//}
					//else
					//{
					//	document.getElementById('txtContrasena').value = Argumentos[0];
					//	document.getElementById('txtNuevaContrasena').value = Argumentos[1];
					//	SubmitForm2(1);
					//}
			}
		}
		</script>		
		<script language="javascript">		
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  
			function ObtenerP(nParametro) 
			{    
			var callingURL = document.location.href;    
			var cgiString = callingURL.substring(callingURL.indexOf('?')+1,callingURL.length); 
			nParametro -=1; 
			var col_array=cgiString.split("&"); 
			var part_num=0; 
				while (part_num < col_array.length) 
						{ 
								if ( part_num == nParametro ) 
								return col_array[part_num]; 
								else 
								part_num+=1; 
						} 
			return 0; 
			} 
			function Cerrar()
			{
			var NroPagina;
			var strEstado;
			document.frmArchivo.btnCerrar.disabled=true; 
			NroPagina= document.all["txtNroPagina"].value;
			strEstado= document.all["txtEstado"].value;
			//if (NroPagina==2)
			//	{
			//	window.location ='DocumentoConsultar.aspx?Estado=01';
			//	}
			//	else
			//	{
			//	window.location ='DocumentoAprobacion.aspx?Estado=' + strEstado + '&var2=1';
			//	}
			}
			function noboton(ev)
			{
			if (document.all) ev= event;
			if (ev.button & 2)
			alert("Bot�n inactivo.");
			}

			function bloquear()
			{
			document.onmousedown = noboton;		
			}
		</script>	 
	</HEAD>
	<body onload="bloquear()" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		bgColor="#f0f0f0">
		<form id="frmArchivo" name="frmArchivo" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<DIV id="cntFirmas" runat="server"></DIV>
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<td width="60%" align="center">
									<asp:literal id="LtrFirma" runat="server"></asp:literal></td>
								<TD width="30%">
                        <table cellpadding="0" cellspacing="2" width="100%" border="0" >
                        <tr>
                            <td align="left" valign="top" >
                                    <asp:Button class="btn" ID="btnAnterior" runat="server" style="background-color: #22A458; color: #FFFFFF;" Text="Anterior Documento" Width="32%" BorderStyle="None" />
                                    <asp:TextBox ID="txtDeItem" style="text-align:center" type="text" runat="server" Enabled="False" Width="32%" BorderStyle="None"></asp:TextBox>           
                                    <asp:Button class="btn" ID="btnSiguiente" runat="server" style="background-color: #22A458; color: #FFFFFF;" Text="Siguiente Documento" Width="32%" BorderStyle="None" />
                        </td>
                </tr>
                        </table>
                                </TD>								
								<TD align="center" class="Ayuda">
									<asp:literal id="ltrExpediente" runat="server"></asp:literal>
								</TD>
								<TD align="center">
									<asp:literal id="ltrFirmar1" runat="server"></asp:literal></TD>
								<TD align="center">
									<asp:literal id="ltrFirmar2" runat="server"></asp:literal></TD>
								<td><asp:button id="btnRechazar" runat="server" CssClass="btn" Width="100px" Visible="False" Text="Rechazar" CausesValidation="False" BackColor="Blue" ForeColor="White" Font-Bold="True"></asp:button></td>
								<TD align="center"><asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="100px" CssClass="btn" />
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="5">
									<asp:label id="lblError" runat="server" CssClass="error"></asp:label>
									<asp:label id="lblFirma" runat="server" CssClass="error"></asp:label>
									</TD>
                                <td align="right">
                                    <asp:Label ID="lblck" runat="server" Text="Impr."></asp:Label>
                                </td>
                                
                                <td>
                                    <asp:CheckBox ID="ckimpr" runat="server" AutoPostBack="True" />

                                </td>
							</TR>
							<!--<TR>
								<TD align="center" colSpan="4">&nbsp;</TD>
							</TR>-->
						</TABLE>
						<DIV style="DISPLAY: none">
                            <INPUT id="HiNavMax" name="HiNavMax" runat="server">
                            <INPUT id="HiNavSel" name="HiNavSel" runat="server">
                            <INPUT id="HiNavMin" name="HiNavMin" runat="server">
                            <INPUT id="txtArchivo_pdf" name="txtArchivo_pdf" runat="server">
							<INPUT id="txtArchivo_pdf_firmado" name="txtArchivo_pdf_firmado" runat="server">
							<INPUT id="txtNumSerie" name="txtNumSerie" runat="server"> <INPUT id="txtNroPagina" name="txtNroPagina" runat="server">
							<INPUT id="txtEstado" name="txtEstado" runat="server"> <INPUT id="txtTimeStamp" name="txtTimeStamp" runat="server">
							<INPUT id="txtContrasena" name="txtContrasena" runat="server"> <INPUT id="txtNuevaContrasena" name="txtNuevaContrasena" runat="server"></DIV>
					</TD>
				</TR>
				<TR>
					<TD>
						<asp:literal id="ltrVisor" runat="server"></asp:literal></TD>
				</TR>
			</TABLE>
		        </form>
	</body>
</HTML>
