Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class CajasExistentesxArea
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objReporte As LibCapaNegocio.clsCNReporte
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    'Protected WithEvents btnVerReporte As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button

    'Protected WithEvents txtFecha As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultadoTotal As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    Private Gridexporta As New DataGrid
    Private IntPase As Integer
    Private dvTemporal As DataView
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG16") Then
            Try
                'If ValidarPagina("PAG16") = False Then Exit Sub
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargarFechas()
                    CargarCombos()
                    LlenaGrid()
                    LlenaGridTotal()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarCombos()
        'objComun = New LibCapaNegocio.clsCNComun
        'objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")

    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        txtFecha.Value = objFuncion.gstrFecha()
    End Sub

    'Private Sub LlenaGrid()
    '    Try
    '        objReporte = New LibCapaNegocio.clsCNReporte
    '        objFuncion = New LibCapaNegocio.clsFunciones
    '        dvTemporal = New DataView(objReporte.gdrRepUnidadesExistentesxArea(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
    '                cboArea.SelectedItem.Value, txtFecha.Value))
    '        Session.Add("dvReporte", dvTemporal)
    '        objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
    '        lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub
    Private Sub LlenaGrid()
        Try
            objReporte = New LibCapaNegocio.clsCNReporte
            objFuncion = New LibCapaNegocio.clsFunciones
            dvTemporal = New DataView(objReporte.gdrRepUnidadesExistentesxArea(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, txtFecha.Value, Session.Item("UsuarioLogin")))
            Session.Add("dvReporte", dvTemporal)
            objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)

            If IntPase = 1 Then
                objFuncion.gCargaGrid(Gridexporta, dvTemporal.Table)
                IntPase = 0
            Else
                objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
            End If

            lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub LlenaGridTotal()
        Dim drFila As DataRow
        Dim dtTotal As New System.Data.DataTable
        Dim diItem As DataGridItem
        Dim intNroDepsa As Integer = 0
        Dim intNroClientes As Integer = 0
        Try
            For Each diItem In dgdResultado.Items
                intNroDepsa += CInt(diItem.Cells(1).Text)
                intNroClientes += CInt(diItem.Cells(2).Text)
            Next

            Dim dcDepsa As New DataColumn("Depsa", GetType(String))
            Dim dcClientes As New DataColumn("Clientes", GetType(String))
            Dim dcTotal As New DataColumn("Total", GetType(String))

            dtTotal.Columns.Add(dcDepsa)
            dtTotal.Columns.Add(dcClientes)
            dtTotal.Columns.Add(dcTotal)

            drFila = dtTotal.NewRow
            drFila("Depsa") = intNroDepsa.ToString
            drFila("Clientes") = intNroClientes.ToString
            drFila("Total") = (intNroDepsa + intNroClientes).ToString
            dtTotal.Rows.Add(drFila)

            Session.Add("dvReporteAuxiliar", New DataView(dtTotal))
            dgdResultadoTotal.DataSource = dtTotal
            dgdResultadoTotal.DataBind()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        objFuncion = New LibCapaNegocio.clsFunciones
        'se inhabilita el view state para no ejecutar ninguna accion
        'Dim dgGrid As DataGrid = dgdResultado
        'Dim dgCol As DataGridColumn = dgdResultado.Columns(5)
        'dgGrid.Columns.Remove(dgCol)
        'Me.EnableViewState = False
        IntPase = 1
        Dim dgGrid As DataGrid = Gridexporta
        LlenaGrid()
        Gridexporta.Dispose()
        Gridexporta = Nothing
        objFuncion.ExportarAExcel("CajasExistentesxArea.xls", Response, dgGrid)
        Me.EnableViewState = True
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgdResultado.PageIndexChanged
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
        LlenaGridTotal()
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
        Dim cadena As String
        If Me.dgdResultado.Items.Count > 0 Then
            cadena = "Reportes/RepCajasExistentesxArea.aspx?dato1=" & cboArea.SelectedItem.Text & "&dato2= " & txtFecha.Value
            Response.Redirect(cadena)
            'Server.Transfer(cadena)
        Else
            lblError.Text = "Falta informacion para imprimir"
        End If
    End Sub

    ReadOnly Property pFE_EXIS() As String
        Get
            Return Me.txtFecha.Value
        End Get
    End Property

    ReadOnly Property pDE_AREA() As String
        Get
            Return IIf(Me.cboArea.SelectedItem.Value = "", "TODOS", Me.cboArea.SelectedItem.Text)
        End Get
    End Property

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("CO_AREA", dgi.Cells(4).Text())
            Session.Add("CO_TIPO_ITEM", "CAJ")
            Session.Add("DE_AREA", dgi.Cells(0).Text())
            Session.Add("PaginaAnterior", "CajasExistentesxArea.aspx")
            Session.Add("FechaAl", txtFecha.Value)
            Response.Redirect("UnidadesxArea.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objReporte Is Nothing) Then
            objReporte = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub dgdResultado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgdResultado.SelectedIndexChanged

    End Sub
End Class
