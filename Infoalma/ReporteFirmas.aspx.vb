Imports Library.AccesoBL
Imports Library.AccesoDB
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class ReporteFirmas
    Inherits System.Web.UI.Page
    Private objReporte As Reportes = New Reportes
    Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If (Request.IsAuthenticated) And InStr(Session("PagId"), "26") Then
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "26"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Inicializa()
                End If
            Else
                Response.Redirect("Salir.aspx?caduco=1", False)
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub Inicializa()
        Me.lblOpcion.Text = "Consulta Firmas x Usuario"
        loadEntidad()
    End Sub

    Private Sub BindDatagrid()
        Me.dgdResultado.DataSource = objReporte.gCNGetListarFirmas(Me.cboEntidad.SelectedValue)
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnAprobador As CheckBox
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            btnAprobador = CType(e.Item.FindControl("chkAprueba"), CheckBox)
            If CStr(e.Item.DataItem("Aprobador")) = "03" Then
                btnAprobador.Checked = True
            Else
                btnAprobador.Checked = False
            End If
        End If
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True
        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        'Response.Charset = "UTF-8"
        'Response.ContentEncoding = Encoding.Default
        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False
        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
        Me.dgdResultado.RenderControl(htmlWriter)
        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As DataTable
        Try
            Me.cboEntidad.Items.Clear()
            dtEntidad = objEntidad.gGetListarEntidades()
            Me.cboEntidad.Items.Add(New ListItem("---------------Todos---------------", "0"))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboEntidad.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BindDatagrid()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objReporte Is Nothing) Then
            objReporte = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
