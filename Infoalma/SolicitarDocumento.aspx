<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SolicitarDocumento.aspx.vb" Inherits="SolicitarDocumento" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Solicitar Documentos</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaDocDesde").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaDocDesde").datepicker('show');
            });

            $("#txtFechaDocHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaDocHasta").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //------------------------------------------ Inicio Validar Fecha
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            var TipoDocumento;
            TipoDocumento = document.all["txtTipoDocumento"].value;
            document.all["pnlAnular"].style.display = 'none';

            if (TipoDocumento == 'Warrant Electr�nico') {
                NroLib.style.display = 'none';
                txtLib.style.display = 'none';
            }
            else {
                NroLib.style.display = 'inline';
                txtLib.style.display = 'inline';
            }

            if (document.all["txtAnulacion"].value == '') {
                Anulacion.style.display = 'none';
            }
            else {
                Anulacion.style.display = 'inline';
            }
        }
        function CerrarPanel() {
            document.all["pnlAnular"].style.display = 'none';
        }
        function AbrirPanel() {
            document.all["pnlAnular"].style.display = 'inline';
        }
        function Visor() {
            window.location = 'DocumentoResumen.aspx?var=1'//,'null','Width=800px,Height=680px,top=5,left=50,scrollbars=yes');
            //window.location ='DocumentoReporte.aspx?var=1'
        }
        function chkSelect_OnMouseMove(tableRowId, checkBox) {
            if (checkBox.checked == false)
                tableRowId.style.backgroundColor = "#FFFFC0";
        }

        function chkSelect_OnMouseOut(tableRowId, checkBox, rowIndex) {
            if (checkBox.checked == false) {
                var bgColor;
                if (rowIndex % 2 == 0)
                    bgColor = "WhiteSmoke";
                else
                    bgColor = "Transparent";
                tableRowId.style.backgroundColor = bgColor;
            }
        }

        function chkSelect_OnClick(tableRowId, checkBox, rowIndex) {
            var bgColor;
            if (rowIndex % 2 == 0) {
                bgColor = 'WhiteSmoke';
            }
            else {
                bgColor = 'Transparent';
            }
            if (document.getElementById(checkBox).checked == true) {
                document.getElementById(tableRowId).style.backgroundColor = '#FFFFC0';
            }
            else {
                document.getElementById(tableRowId).style.backgroundColor = bgColor;
            }
        }
        function ValidaCheckBox() {
            var retorno = false;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        retorno = true;
                    }
                }
            }
            if (retorno == false) {
                alert("Seleccione por lo menos un registro para continuar.");
            }
            return retorno;
        }
        function AbrirDetalle(param1, param2, param3, param4, param5, param6) {

            //window.open("DetalleRequ.aspx?param1="+param1+"&param2="+param2+"&param3="+param3+"&param4="+param4+"&param5="+param5+"&param6="+param6,"",'left=240,top=200,width=400,height=170,toolbar=0,resizable=0'); 

            window.showModalDialog("DetalleRequ.aspx?param1=" + param1 + "&param2=" + param2 + "&param3=" + param3 + "&param4=" + param4 + "&param5=" + param5 + "&param6=" + param6, "", 'status:no;dialogWidth:360px;dialogHeight:190px;dialogHide:true;help:no;scroll:no;resizable=no;toolbar=no;menubar=no;center=yes');
        }

    </script>
</head>
<body bgcolor="#f0f0f0" bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
        <uc1:Header ID="Header2" runat="server"></uc1:Header>
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td valign="top" width="112">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">&nbsp;SOLICITAR DOCUMENTOS</td>
                        </tr>
                        <tr>
                            <td class="text" style="height: 141px">
                                <table id="Table26" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="text" style="width: 220px; height: 11px">Ar�a</td>
                                                    <td class="text" style="width: 12px; height: 11px" align="center">:</td>
                                                    <td style="width: 145px; height: 11px">
                                                        <asp:DropDownList ID="cboArea" runat="server" CssClass="Text" Width="145px" DataValueField="COD" DataTextField="DES"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 23px; height: 11px"></td>
                                                    <td class="text" style="width: 139px; height: 11px">Tipo&nbsp;Documento</td>
                                                    <td class="text" style="width: 3px; height: 11px" align="center">:</td>
                                                    <td style="width: 163px; height: 11px">
                                                        <asp:DropDownList ID="cboTipoDocumento" runat="server" CssClass="Text" Width="192px" DataValueField="COD"
                                                            DataTextField="DES">
                                                        </asp:DropDownList></td>
                                                    <td style="height: 11px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" style="width: 220px; height: 12px">Nro. Documento</td>
                                                    <td class="text" style="width: 12px; height: 12px">:</td>
                                                    <td style="width: 145px; height: 12px">
                                                        <asp:TextBox ID="txtNroDocumento" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Width="145px"></asp:TextBox></td>
                                                    <td class="Text" style="width: 23px; height: 12px" width="23"></td>
                                                    <td class="text" style="width: 139px; height: 12px">Fecha Documento</td>
                                                    <td class="text" style="width: 3px; height: 12px" align="center" width="3"></td>
                                                    <td style="width: 163px; height: 12px"></td>
                                                    <td style="height: 12px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 220px; height: 9px">Nro. Caja</td>
                                                    <td class="text" style="width: 12px; height: 9px" align="center" width="12">:</td>
                                                    <td style="width: 145px; height: 9px">
                                                        <asp:TextBox ID="txtNroCaja" runat="server" CssClass="Text" Width="145px"></asp:TextBox></td>
                                                    <td class="Text" style="width: 23px; height: 9px" width="23"></td>
                                                    <td class="text" style="width: 139px; height: 9px">Desde&nbsp;
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaDocDesde" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaDocDesde" runat="server"><input class="Text" id="btnFechaInicial" type="button"
                                                                value="..." name="btnFecha">
                                                    </td>
                                                    <td style="width: 3px; height: 9px" width="3"></td>
                                                    <td class="text" style="width: 163px; height: 9px" align="right">Hasta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="Text" onkeypress="validarcharfecha()" id="txtFechaDocHasta" onkeyup="this.value=formateafecha(this.value);"
                                                        maxlength="10" size="6" name="txtFechaDocHasta" runat="server"><input class="Text" id="btnFechaFinal" type="button"
                                                            value="..." name="btnFecha">
                                                    </td>
                                                    <td style="height: 9px" width="15"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 220px; height: 12px">Descripci�n</td>
                                                    <td class="text" style="width: 12px; height: 12px" align="center">:</td>
                                                    <td style="width: 495px; height: 12px" colspan="5">
                                                        <asp:TextBox ID="txtDescripcion" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Width="513px"></asp:TextBox></td>
                                                    <td style="height: 12px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="CampoObliga" style="width: 250px" align="left" colspan="3">&nbsp;</td>
                                                    <td style="width: 23px"></td>
                                                    <td style="width: 139px" align="right"></td>
                                                    <td style="width: 3px"></td>
                                                    <td style="width: 163px" align="right">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" style="height: 18px" height="18">
                                <asp:Label ID="lblNota" runat="server" CssClass="TEXT" Width="100%" Visible="False">NOTA: los documentos que figuran por defecto como seleccionados, ya se encuentran solicitados y su atenci�n est� en proceso.</asp:Label></td>
                        </tr>
                        <tr>
                            <td class="text" style="height: 18px" height="18">Resultado :
									<asp:Label ID="lblRegistros" runat="server" CssClass="text" Width="424px">0 registros</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    AllowCustomPaging="True" AutoGenerateColumns="False" PageSize="50" OnPageIndexChanged="Change_Page" AllowSorting="True">
                                    <SelectedItemStyle Font-Names="Arial" ForeColor="Black" BackColor="#99CC66"></SelectedItemStyle>
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader" VerticalAlign="Middle"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Sel">
                                            <HeaderStyle Font-Bold="True" Width="5%"></HeaderStyle>
                                            <ItemTemplate>
                                                <table id="Table12" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:CheckBox ID="chkSel" runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_SEL") %>'></asp:CheckBox></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ID_UNID" SortExpression="ID_UNID" HeaderText="Nro. Caja">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_CRIT_DESD" SortExpression="DE_CRIT_DESD" HeaderText="Tipo Doc.">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Descripci&#243;n">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkUnidad" runat="server" NavigateUrl="#">
														<%# DataBinder.Eval(Container,"DataItem.DES") %>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="DIVI" HeaderText="Divisi&#243;n Desde/Hasta">
                                            <HeaderStyle Font-Bold="True" Width="10%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_INGR" SortExpression="FE_INGR" HeaderText="Fecha Doc.">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Detalle Adicional">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="15px" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDetalle" runat="server" Width="140px" CssClass="Text"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="NU_CONT"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="NU_SECU_DIVI"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="CO_CRIT_DESD"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="CO_AREA" HeaderText="CO_AREA"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="FLG_SEL" HeaderText="ST_FILA"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="ST_UBIC" HeaderText="ST_UBIC"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="DE_NOMB_PERS" HeaderText="DE_NOMB_PERS"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="FE_USUA_CREA" HeaderText="FE_USUA_CREA"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="NU_REQU" HeaderText="NU_REQU"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid>
                                <table class="tblstandard" cellspacing="2" cellpadding="2" width="100%">
                                    <tr>
                                        <td class="text">(P�gina
												<asp:Label ID="lblCurrentPage" CssClass="text" runat="server"></asp:Label>de
												<asp:Label ID="lblTotalPages" CssClass="text" runat="server">1</asp:Label>)
                                        </td>
                                        <td align="right">
                                            <asp:LinkButton ID="lbtnPrevious" OnClick="PreviousPage1" CssClass="text" runat="server" Enabled="False"><< Anterior</asp:LinkButton>&nbsp;
												<asp:LinkButton ID="lbtnNext" OnClick="NextPage" CssClass="text" runat="server" Enabled="False">  Siguiente >></asp:LinkButton></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblIDInicio" runat="server" CssClass="text" Visible="False"></asp:Label><asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label><asp:Label ID="lblIDFinal" runat="server" CssClass="text" Visible="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td align="right" width="50%">
                                            <asp:Button ID="btnVerSolicitud" runat="server" CssClass="btn" Width="100px" Text="Ver Solicitud"></asp:Button></td>
                                        <td style="width: 3px" align="center"></td>
                                        <td width="50%">
                                            <asp:Button ID="btnIngresarDoc" runat="server" CssClass="btn" Width="120px" Text="Ingresar Documentos"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
