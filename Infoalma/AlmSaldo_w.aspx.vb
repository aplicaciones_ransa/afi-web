Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.Data
Imports Infodepsa

Public Class AlmSaldo_w
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objParametro As clsCNParametro
    Private objWarrant As clsCNWarrant
    Private objMercaderia As clsCNMercaderia
    Private dvInventario As DataView
    Private objRetiro As New clsCNRetiro
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtLote As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDescMerc As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodigo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    Private objWMS As clsCNWMS

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboCodProducto As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaVenc As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "INVENTARIO_MERCADERI") Then
            Try
                Me.lblOpcion.Text = "Inventario Mercaderķa WMS"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "INVENTARIO_MERCADERI"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    'Dim dttFecha As DateTime
                    'Dim Dia As String
                    'Dim Mes As String
                    'Dim Anio As String
                    'dttFecha = Date.Today.Subtract(TimeSpan.FromDays(7))
                    'Dia = "00" + CStr(dttFecha.Day)
                    'Mes = "00" + CStr(dttFecha.Month)
                    'Anio = "0000" + CStr(dttFecha.Year)
                    'Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    'Dia = "00" + CStr(Now.Day)
                    'Mes = "00" + CStr(Now.Month)
                    'Anio = "0000" + CStr(Now.Year)
                    'Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    LlenarAlmacenes()
                    If Session("Co_AlmaW") = "" Then
                        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                    End If
                    If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaW"))) = 0 Then
                        Response.Redirect("AlmSaldo.aspx", False)
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            ' Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgResultado.DataBind()
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dtMercWMS As DataTable
            objMercaderia = New clsCNMercaderia
            dtMercWMS = objMercaderia.gCNGetListarInventarioWMS(Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value,
                                Me.txtFechaFinal.Value, Me.txtFechaVenc.Value, Me.cboAlmacen.SelectedValue, Session.Item("IdSico"))
            If dtMercWMS.Rows.Count Then
                Me.dgResultado.Visible = True
                Session.Add("dtMovimientoWMS", dtMercWMS)
                Me.dgResultado.DataSource = dtMercWMS
                Me.dgResultado.DataBind()
            End If


        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
        Session.Item("NombreEntidad"), "C", "INVENTARIO_MERCADERI", "CONSULTAR INVENTARIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound

    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgExport.DataSource = Session.Item("dtMovimientoWMS")
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    'Protected Overrides Sub Finalize()
    '    If Not (objParametro Is Nothing) Then
    '        objParametro = Nothing
    '    End If
    '    If Not (objAccesoWeb Is Nothing) Then
    '        objAccesoWeb = Nothing
    '    End If
    '    If Not (objWarrant Is Nothing) Then
    '        objWarrant = Nothing
    '    End If
    '    If Not (objMercaderia Is Nothing) Then
    '        objMercaderia = Nothing
    '    End If
    '    MyBase.Finalize()
    'End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) = 0 Then
            Response.Redirect("AlmSaldo.aspx", False)
        End If
    End Sub
End Class
