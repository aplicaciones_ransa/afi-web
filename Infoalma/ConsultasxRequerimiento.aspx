<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConsultasxRequerimiento.aspx.vb" Inherits="ConsultasxRequerimiento" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Consultas Generadas</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  
	
		function Ocultar()
			{
			Estado.style.display='none';			
			}		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="125"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Titulo1" height="20">CONSULTA DE REQUERIMIENTOS</TD>
							</TR>
							<TR>
								<TD class="td" style="HEIGHT: 88px">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text" style="WIDTH: 116px; HEIGHT: 25px">Nro. Requerimiento
														</TD>
														<TD class="text" style="HEIGHT: 25px">:</TD>
														<TD style="WIDTH: 126px; HEIGHT: 25px"><asp:textbox id="txtNroRequerimiento" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
														<TD class="text" style="WIDTH: 117px; HEIGHT: 25px"></TD>
														<TD class="text" style="HEIGHT: 25px"></TD>
														<TD class="text" style="HEIGHT: 25px"></TD>
													</TR>
													<TR>
														<TD class="text" style="WIDTH: 116px">Tipo Env�o</TD>
														<TD class="text">:</TD>
														<TD style="WIDTH: 126px"><asp:textbox id="txtTipoEnvio" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
														<TD class="text" style="WIDTH: 117px">Persona Autorizada</TD>
														<TD class="text">:</TD>
														<TD class="text"><asp:textbox id="txtPersonaAutorizada" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text" style="WIDTH: 116px" align="left">Estado
														</TD>
														<TD class="text" width="9">:</TD>
														<TD style="WIDTH: 126px" width="126"><asp:textbox id="txtEstado" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
														<TD class="Text" style="WIDTH: 117px" width="117">�rea</TD>
														<TD class="Text" width="7">:</TD>
														<TD class="Text" width="148"><asp:textbox id="txtArea" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text" style="WIDTH: 116px" align="left">Fecha Reg.</TD>
														<TD class="text" width="9">:</TD>
														<TD style="WIDTH: 126px" width="126"><asp:textbox id="txtFechaRegistro" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
														<TD class="Text" style="WIDTH: 117px" width="117"></TD>
														<TD class="Text" width="7"></TD>
														<TD class="Text" width="148"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Resultado :&nbsp;
									<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></TD>
							</TR>
							<TR>
								<TD><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AllowSorting="True" PageSize="20" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NRO" HeaderText="Nro. Consulta">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ID_UNID" HeaderText="Nro. Caja">
												<HeaderStyle Width="12%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_CRIT" HeaderText="Tipo Doc.">
												<HeaderStyle Width="25%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DSCR" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="35%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Detalle Adicional">
												<HeaderStyle Width="25%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="td"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td">
									<TABLE id="Table5" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD align="center"><INPUT class="btn" id="btnAtras" onclick="location='EstadoRequerimiento.aspx'" type="button"
													value="Regresar">
												<asp:button id="btnVerReporte" runat="server" CssClass="btn" Text="Ver Reporte "></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
