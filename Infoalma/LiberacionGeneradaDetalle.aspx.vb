Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Infodepsa
Imports Library.AccesoDB
Public Class LiberacionGeneradaDetalle
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtTipoTitulo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroTitulo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFecha As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroLibe As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDepositante As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEndosatario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "20") Then
            Try
                Me.lblError.Text = ""
                Session.Remove("Mensaje")
                If Not IsPostBack Then
                    Dim strResult As String()
                    Me.lblOpcion.Text = "Datos del Documento"
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "20"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Me.txtDepositante.Enabled = False
                    Me.txtNroTitulo.Enabled = False
                    Me.txtFecha.Enabled = False
                    Me.txtTipoTitulo.Enabled = False
                    Me.txtNroLibe.Enabled = False
                    Me.txtEndosatario.Enabled = False
                    Me.txtNroTitulo.Text = Session.Item("NroDoc")
                    Me.txtNroLibe.Text = Session.Item("NroLib")
                    Me.txtTipoTitulo.Text = Request.QueryString("TipDoc")
                    Me.txtDepositante.Text = Request.QueryString("Cliente")
                    Me.txtEndosatario.Text = Request.QueryString("Finan")
                    Me.txtFecha.Text = Request.QueryString("Fecha")
                    loadDataDocumento()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadDataDocumento()
        Dim dtDetalle As DataTable
        dtDetalle = objDocumento.gGetDetalleLiberacion("01", "", Session.Item("NroLib"), Session.Item("IdTipoDocumento"), Session.Item("NroDoc"))
        Session.Add("dv", dtDetalle)
        Me.dgdResultado.DataSource = dtDetalle
        Me.dgdResultado.DataBind()
        dtDetalle.Dispose()
        dtDetalle = Nothing
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("LiberacionConsultar.aspx")
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
