<%@ Page Language="vb" AutoEventWireup="false" CodeFile="SolicitarDevolucionConsulta.aspx.vb" Inherits="SolicitarDevolucionConsulta" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Solicitar Devoluci�n de Consulta</title>
		<meta name="vs_showGrid" content="False">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  

			//------------------------------------------ Inicio Validar Fecha
		var primerslap=false; 
		var segundoslap=false; 
		function IsNumeric(valor) 
		{ 
			var log=valor.length; var sw="S"; 
			for (x=0; x<log; x++) 
			{ v1=valor.substr(x,1); 
			v2 = parseInt(v1); 
			//Compruebo si es un valor num�rico 
			if (isNaN(v2)) { sw= "N";} 
			} 
				if (sw=="S") {return true;} else {return false; } 
		} 	
		function formateafecha(fecha) 
		{ 
			var long = fecha.length; 
			var dia; 
			var mes; 
			var ano; 

			if ((long>=2) && (primerslap==false)){dia=fecha.substr(0,2); 
			if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
			else { fecha=""; primerslap=false;} 
			} 
			else 
			{ dia=fecha.substr(0,1); 
			if (IsNumeric(dia)==false) 
			{fecha="";} 
			if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
			} 
			if ((long>=5) && (segundoslap==false)) 
			{ mes=fecha.substr(3,2); 
			if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
			else { fecha=fecha.substr(0,3);; segundoslap=false;} 
			} 
			else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
			if (long>=7) 
			{ ano=fecha.substr(6,4); 
			if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
			else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
			} 

			if (long>=10) 
			{ 
				fecha=fecha.substr(0,10); 
				dia=fecha.substr(0,2); 
				mes=fecha.substr(3,2); 
				ano=fecha.substr(6,4); 
				// A�o no viciesto y es febrero y el dia es mayor a 28 
				if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
			} 
			return (fecha); 
		} 
		//-----------------------------------Fin formatear fecha	
		function Ocultar()
			{
			Estado.style.display='none';			
			}		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
						<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
							<TR>
								<TD colSpan="2">
									<uc1:header id="Header2" runat="server"></uc1:header></TD>
							</TR>
							<TR>
								<TD vAlign="top" width="125">
									<uc1:Menu id="Menu1" runat="server"></uc1:Menu></TD>
								<TD vAlign="top">
									<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0" style="HEIGHT: 348px">
										<TR>
											<TD class="Titulo1" height="20">
												<P align="center">SOLICITAR DEVOLUCI�N DE CONSULTA</P>
											</TD>
										</TR>
										<TR>
											<TD class="td">
												<TABLE id="Table26" cellSpacing="0" cellPadding="0" width="630" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD align="center">
															<TABLE id="Table2" style="HEIGHT: 214px" cellSpacing="3" cellPadding="3" width="408" bgColor="whitesmoke"
																border="0" class="Text">
																<TR>
																	<TD class="text" style="WIDTH: 135px">Solicitado por
																	</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtSolicitante" style="TEXT-TRANSFORM :uppercase" runat="server" Width="180px"
																			CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtSolicitante" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">�rea</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:dropdownlist id="cboArea" runat="server" Width="180px" DataValueField="COD" DataTextField="DES"
																			CssClass="Text"></asp:dropdownlist>&nbsp;</TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Cantidad de Cajas</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtNroCajas" style="TEXT-TRANSFORM :uppercase" runat="server" Width="50px" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtNroCajas" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Direcci�n de Recojo</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtDirEntrega" style="TEXT-TRANSFORM :uppercase" runat="server" TextMode="MultiLine"
																			Width="180px" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtDirEntrega" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Devoluci�n de Consulta</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtDevolucionConsulta" style="TEXT-TRANSFORM :uppercase" runat="server" Width="180px"
																			CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ControlToValidate="txtDevolucionConsulta"
																			ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 135px">Autorizado por</TD>
																	<TD class="text" style="WIDTH: 16px" align="center">:</TD>
																	<TD>
																		<asp:textbox id="txtAutorizador" style="TEXT-TRANSFORM :uppercase" runat="server" Width="180px"
																			ReadOnly="True" CssClass="Text"></asp:textbox>&nbsp;
																		<asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ControlToValidate="txtAutorizador" ErrorMessage="Ingrese"></asp:RequiredFieldValidator></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 135px; HEIGHT: 9px"></TD>
																	<TD style="WIDTH: 16px; HEIGHT: 9px"></TD>
																	<TD style="HEIGHT: 9px"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 160px; HEIGHT: 9px" align="right" colSpan="2"><INPUT type="reset" value="Limpiar" class="btn"></TD>
																	<TD style="HEIGHT: 9px">
																		<asp:button id="btnEnviar" runat="server" Text="Enviar" CssClass="btn"></asp:button></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD>
											</TD>
										</TR>
										<TR>
											<TD></TD>
										</TR>
										<TR>
											<TD class="td">
												<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
										</TR>
										<TR>
											<TD class="td" id="Estado"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2">
									<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
							</TR>
						</TABLE>
		</form>
	</body>
</HTML>
