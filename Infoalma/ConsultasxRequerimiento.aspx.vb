Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class ConsultasxRequerimiento
    Inherits System.Web.UI.Page
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objAccesoWeb As clsCNAccesosWeb
    ' Protected WithEvents btnVerReporte As System.Web.UI.WebControls.Button
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private dvTemporal As DataView
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnAtras As System.Web.UI.HtmlControls.HtmlButton
    'Protected WithEvents txtPersonaAutorizada As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtArea As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaRegistro As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtEstado As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroRequerimiento As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTipoEnvio As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    LlenaCabecera()
                    LlenaDetalle()
                    objAccesoWeb = New clsCNAccesosWeb
                    objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                            Session.Item("NombreEntidad"), "C", "DEPSAFIL", "CONSULTA DE REQUERIMIENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaCabecera()
        LlenaDetalle()
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Private Sub dgdResultado_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgdResultado.SortCommand
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub LlenaDetalle()
        'objFuncion = New LibCapaNegocio.clsFunciones
        'objRequ = New LibCapaNegocio.clsCNRequerimiento
        'dt = objRequ.gdtConseguirDetalle(CStr(Session.Item("CodCliente")), _
        'CStr(Session.Item("CodRequerimiento")))
        'objFuncion.gCargaGrid(dgdResultado, dt)
        'lblRegistros.Text = objRequ.intFilasAfectadas.ToString & " registros"

        objFuncion = New LibCapaNegocio.clsFunciones
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        dvTemporal = New DataView(objRequ.gdtConseguirDetalle(CStr(Session.Item("CodCliente")),
        CStr(Session.Item("CodRequerimiento"))))
        Session.Add("dvReporte", dvTemporal)
        objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
        lblRegistros.Text = objRequ.intFilasAfectadas.ToString & " registros"


    End Sub

    Sub LlenaCabecera()
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        Dim dtCabecera As DataTable = objRequ.gdtConseguirCabecera(CStr(Session.Item("CodCliente")),
        CStr(Session.Item("CodRequerimiento")), CStr(Session.Item("CodClieAdic")))
        If dtCabecera.Rows.Count = 1 Then
            Dim drFila As DataRow
            For Each drFila In dtCabecera.Rows
                txtNroRequerimiento.Text = drFila("NU_REQU")
                txtTipoEnvio.Text = drFila("DE_TIPO_ENVI")
                txtEstado.Text = drFila("TI_ESTA_RECE")
                txtFechaRegistro.Text = drFila("FE_RECE") & " " & drFila("HO_RECE")
                txtPersonaAutorizada.Text = drFila("DE_NOMB_PERS")
                txtArea.Text = drFila("DE_AREA")
            Next
        End If
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Protected Overrides Sub Finalize()
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
        Dim cadena As String
        If Me.dgdResultado.Items.Count > 0 Then
            cadena = "Reportes/RepRequeDocumentos.aspx?dato1=" & txtNroRequerimiento.Text & "&dato2= " & txtTipoEnvio.Text & "&dato3= " & txtEstado.Text & "&dato4= " & txtFechaRegistro.Text & "&dato5= " & txtPersonaAutorizada.Text & "&dato6= " & txtArea.Text
            'Response.Redirect("Reportes/RepRequeDocumentos.aspx?dato1=" & txtNroRequerimiento.Text & "&dato2 = " & txtTipoEnvio.Text)
            Response.Redirect(cadena)
        Else
            lblError.Text = "Falta informacion para imprimir"
        End If
    End Sub
End Class
