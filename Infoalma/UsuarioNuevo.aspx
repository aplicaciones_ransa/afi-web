<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UsuarioNuevo.aspx.vb" Inherits="UsuarioNuevo" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <title>Nuevo Usuario</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="francisco_uni@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	
        
        $(function () {
            $("#txtFechaCadu").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFecha").click(function () {
                $("#txtFechaCadu").datepicker('show');
            });

        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        function Ocultar() {
            if (document.all.rdoSI.checked == true) {
                document.getElementById("Certificado").style.display = "inline";
            }
            else {
                document.getElementById("Certificado").style.display = "none";
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" onload="Ocultar()" rightmargin="0"
    bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="Table1" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        cellspacing="6" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td></td>
                            <td class="Titulo1" width="100%">
                                <asp:Label ID="lblTitulo" runat="server" EnableViewState="False" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%">
                                <table id="Table20" cellspacing="0" cellpadding="0" width="690" align="center" border="0">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table6" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="Text">Nombres</td>
                                                    <td>:</td>
                                                    <td width="83%" colspan="4">
                                                        <asp:TextBox ID="txtNombre" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Enabled="False" Width="400px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="txtNombre"
                                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="GrupoValida1">Ingrese</asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Apellidos</td>
                                                    <td>:</td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtApellido" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Enabled="False" Width="400px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="txtApellido"
                                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="GrupoValida1">Ingrese</asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Número DNI</td>
                                                    <td>:</td>
                                                    <td>
                                                        <input class="Text" onkeypress="validarnumerico()" id="txtDNI" style="width: 120px" disabled
                                                            name="txtDNI" runat="server">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" CssClass="Error" ErrorMessage="RequiredFieldValidator"
                                                            ControlToValidate="txtDNI" ForeColor=" " Display="Dynamic" ValidationGroup="GrupoValida1">Ingrese</asp:RequiredFieldValidator></td>
                                                    <td class="Text">Correo Electrónico</td>
                                                    <td>:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtCorreo" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Enabled="False" Width="150px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="txtCorreo"
                                                            ErrorMessage="RequiredFieldValidator" Display="Dynamic" ValidationGroup="GrupoValida1">Ingrese</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="Error" ForeColor=" " ControlToValidate="txtCorreo"
                                                            ErrorMessage="RegularExpressionValidator" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="GrupoValida1">Incorrecto</asp:RegularExpressionValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" width="16%">Login de Usuario</td>
                                                    <td width="1%">:</td>
                                                    <td width="30%">
                                                        <asp:TextBox ID="txtLogin" runat="server" CssClass="Text" Enabled="False" Width="120px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" CssClass="Error" ErrorMessage="RequiredFieldValidator"
                                                            ControlToValidate="txtLogin" Display="Dynamic" ValidationGroup="GrupoValida1">Ingrese</asp:RequiredFieldValidator></td>
                                                    <td class="Text" width="16%">Fch. caduca acceso</td>
                                                    <td width="1%">:</td>
                                                    <td width="36%">
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaCadu" onkeyup="this.value=formateafecha(this.value);"
                                                            disabled maxlength="10" size="8" name="txtFechaCadu" runat="server"><input class="Text" id="btnFecha" type="button" value="..."
                                                                name="btnFecha"></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Tiene certificado</td>
                                                    <td>:</td>
                                                    <td>
                                                        <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Text">1. Si</td>
                                                                <td width="40">
                                                                    <input class="Text" id="rdoSI" disabled onclick="Ocultar();" type="radio" value="Radio1"
                                                                        name="grup" runat="server"></td>
                                                                <td class="Text">2. No</td>
                                                                <td>
                                                                    <input class="Text" id="rdoNO" disabled onclick="Ocultar();" type="radio" value="Radio2"
                                                                        name="grup" runat="server"></td>
                                                                <td width="30"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="Text">Certificado Activo</td>
                                                    <td id="Certificado1">:</td>
                                                    <td id="Certificado">
                                                        <asp:CheckBox ID="chkActivo" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Text" valign="top">
                                <table id="Table2" cellspacing="4" cellpadding="0" width="690" align="center" border="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnGrabar" runat="server" EnableViewState="True" CssClass="btn" Width="80px"
                                                Text="Grabar" ValidationGroup="GrupoValida1"></asp:Button></td>
                                        <td>
                                            <asp:Button ID="btnAddEntidad" runat="server" CssClass="btn" Width="100px" Text="Agregar Entidad"></asp:Button></td>
                                        <td>
                                            <asp:Button ID="btnEnviarContraseña" runat="server" CssClass="btn" Width="100px" Text="Enviar Acceso"></asp:Button></td>
                                        <td>
                                            <asp:Button ID="btnRegresar" runat="server" CssClass="btn" Width="100px" Text="Ir a busqueda"
                                                CausesValidation="False"></asp:Button></td>
                                        <td width="500">
                                            <asp:Button ID="btnResetear" runat="server" CssClass="btn" Width="100px" Text="Resetear"></asp:Button></td>
                                        <td>
                                            <div style="display: none">
                                                <input id="txtCodUsuario" name="txtCodUsuario" runat="server"></div>
                                        </td>
                                        <td width="500"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="Text" valign="top">Entidades Asociadas:</td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:DataGrid ID="dgdEntidad" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    AutoGenerateColumns="False" PageSize="5">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="COD_SICO" HeaderText="C&#243;digo">
                                            <HeaderStyle Width="60px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Nom_enti" HeaderText="Entidad">
                                            <HeaderStyle Width="250px"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="nom_tipenti" HeaderText="Tipo Entidad">
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Eliminar">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <table id="Table5" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" CausesValidation="False"
                                                                ToolTip="Eliminar registro" ImageUrl="Images/delete.gif"></asp:ImageButton></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Edit">
                                            <HeaderStyle Width="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <table id="Table15" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Editar.gif"></asp:ImageButton></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="Cod_TipEnti"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
    </TR></TBODY></TABLE>
</body>
</html>
