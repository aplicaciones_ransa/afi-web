<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoPagUno.aspx.vb" Inherits="DocumentoPagUno" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="author" content="francisco_uni@hotmail.com">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript" src="JScripts/Funcionalidad.js"></script>
	</HEAD>
	<body onload="Ocultar()" bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0"
		bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colSpan="2"><uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table13"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6"><uc1:menuinfo id="Menuinfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table4" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="Text" width="18%">Tipo Documento:</TD>
														<TD width="36%"><asp:label id="lblTipoDocumento" runat="server" CssClass="Text"></asp:label></TD>
														<TD class="Text" width="18%">Fecha Creaci�n:</TD>
														<TD width="30%"><asp:label id="lblFechaCreacion" runat="server" CssClass="Text"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Nro Warrant:</TD>
														<TD><asp:label id="lblNroWarrant" runat="server" CssClass="Text"></asp:label></TD>
														<TD id="NroLib" class="Text">Nro Retiro:</TD>
														<TD id="txtLib"><asp:label id="lblNroLiberacion" runat="server" CssClass="Text"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Nom. Depositante:</TD>
														<TD><asp:label id="lblCliente" runat="server" CssClass="Text"></asp:label></TD>
														<TD class="Text">Estado:</TD>
														<TD><asp:label id="lblEstado" runat="server" CssClass="Text"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Mercader�a:</TD>
														<TD colSpan="3"><asp:label id="lblMercaderia" runat="server" CssClass="Text"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Dsc de Secuencia:</TD>
														<TD colSpan="3"><asp:label id="lblSecuencia" runat="server" CssClass="Text"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="4"><asp:panel id="pnlCompromiso" runat="server">
																<TABLE id="Table22" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD class="Text" height="20" vAlign="top" width="18%">Warrants relacionados:</TD>
																		<TD height="20" width="82%">
																			<asp:DataGrid id="dgWarrants" runat="server" CssClass="gv" Width="400px" BorderColor="Gainsboro"
																				AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
																				<ItemStyle CssClass="gvRow"></ItemStyle>
																				<HeaderStyle CssClass="gvHeader"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="TI_TITU" HeaderText="Tipo Warrant"></asp:BoundColumn>
																					<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro Warrant"></asp:BoundColumn>
																					<asp:BoundColumn DataField="FE_TITU" HeaderText="Fecha"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle CssClass="gvPager"></PagerStyle>
																			</asp:DataGrid></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="4"><asp:panel id="pnlEndosatario" runat="server">
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD class="Text" height="20" width="18%">Endosatario:</TD>
																		<TD height="20" width="82%">
																			<asp:dropdownlist id="cboEntidadFinanciera" runat="server" CssClass="Text" Width="350px"></asp:dropdownlist>
																			<asp:button id="btnGrabarEndo" runat="server" CssClass="btn" Width="80px" Text="Grabar" Visible="False"></asp:button>
																			<asp:label id="lblAlerta" runat="server" CssClass="error"></asp:label></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="4"><asp:panel id="pnlOriginal" runat="server">
																<TABLE id="Table9" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD class="Text" height="20" width="18%">Cedido por:</TD>
																		<TD height="20" width="82%">
																			<asp:label id="lblOriginal" runat="server" CssClass="Text"></asp:label></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="4"><asp:panel id="pnlDblEndoso" runat="server">
																<TABLE id="Table10" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD class="Text" height="20" width="18%">Con doble endoso?</TD>
																		<TD height="20" width="82%">
																			<asp:checkbox id="chkDobleEndoso" runat="server" CssClass="Text" AutoPostBack="True"></asp:checkbox></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="4"><asp:panel id="pnlAsociado" runat="server">
																<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD class="Text" height="20" width="18%">Seleccione&nbsp;Asociado:</TD>
																		<TD height="20" width="82%">
																			<asp:dropdownlist id="cboAsociado" runat="server" CssClass="Text" Width="350px"></asp:dropdownlist>
																			<asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar" Visible="False"></asp:button>
																			<asp:label id="lblEndoso" runat="server" CssClass="error"></asp:label></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD class="Etiqueta" colSpan="4">
															<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="100%">
																<TR>
																	<TD><!--<asp:literal id="ltrVisualizar" runat="server"></asp:literal>--></TD>
																	<TD><asp:button id="btnRechazar" runat="server" CssClass="btn" Width="80px" Visible="False" Text="Rechazar"
																			CausesValidation="False" BackColor="Red" ForeColor="White"></asp:button></TD>
																	<TD><cc1:botonenviar id="btnRegistrar" runat="server" CssClass="btn" Width="80px" Visible="False" Text="Registrar"
																			TextoEnviando="Registrando..."></cc1:botonenviar></TD>
																	<TD><!--<cc1:botonenviar id="btnReporte" runat="server" CssClass="btn" Width="120px" Visible="False" Text="Impresi�n con Firmas"
																			TextoEnviando="Generando..."></cc1:botonenviar>--></TD>
																	<TD>
																		<asp:Button style="Z-INDEX: 0" id="btnAnular" runat="server" CssClass="Text" Width="80px" Text="Anular"
																			Visible="False"></asp:Button></TD>
																	<TD><asp:button id="btnRegresar" runat="server" CssClass="btn" Width="80px" Text="Regresar" CausesValidation="False"
																			style="Z-INDEX: 0"></asp:button></TD>
																	<TD></TD>
																	<TD></TD>
																	<TD width="500"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
									<asp:label id="lblSICO" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table11" border="0" cellSpacing="4" cellPadding="0" width="100%">
										<TR>
											<TD class="Text" width="50%">Seguimiento:</TD>
											<TD width="50%" align="right"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top"><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" AutoGenerateColumns="False" BorderColor="Gainsboro"
										Width="100%" PageSize="1">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Nombres" HeaderText="Usuarios Firmantes">
												<HeaderStyle Width="200px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Empresa">
												<HeaderStyle Width="150px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="DSC_SECU" HeaderText="Descripci&#243;n Aprobaci&#243;n">
												<HeaderStyle Width="180px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FCH_FIRM" HeaderText="Fecha">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Firm&#243;">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table12" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id="chkFirmo" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_EVEN"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><INPUT id="txtAsociado" type="hidden" name="txtAsociado" runat="server"><INPUT id="txtTipoDocumento" type="hidden" name="txtTipoDocumento" runat="server"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
