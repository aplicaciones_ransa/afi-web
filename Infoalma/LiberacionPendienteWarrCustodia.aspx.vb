Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.Data
Imports Infodepsa

Public Class LiberacionPendienteWarrCustodia
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objLiberacion As Liberacion = New Liberacion
    Private objSICO As SICO = New SICO
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCoEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    'Protected WithEvents chkWarrCustodia As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnEmitir As RoderoLib.BotonEnviar
    Protected sCO_ALMA_SELE As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroLiberacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "37") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "37"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Consultar Liberaciones Pendientes de Warrants en Custodia"
                BindDatagrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objDocumento.gGetBusquedaLiberacionPendienteWarCustodia(Me.txtNroWarrant.Text.Replace("'", ""), Me.txtNroLiberacion.Text.Replace("'", ""), Session.Item("IdTipoEntidad"), Session.Item("IdSico"))
            Me.dgdResultado.DataSource = dt

            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Public Sub imgCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "37", "ANULAR LIBERACION DE WARRANT EN CUSTODIA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim strCOD_EMPR As String = dgi.Cells(1).Text()
            Dim strCOD_UNID As String = dgi.Cells(2).Text()
            Dim strNRO_DOC_WARR As String = dgi.Cells(3).Text()
            Dim strID_TIPO_DOC As String = dgi.Cells(10).Text()
            Dim strNRO_LIBE As String = dgi.Cells(5).Text()

            'Obtenemos el n�mero de liberaci�n despues de actualizar la informaci�n en el SICO
            pr_IMPR_MENS(objDocumento.gEliminarLiberacionPendiente(strCOD_EMPR, strCOD_UNID, strNRO_DOC_WARR, strID_TIPO_DOC, strNRO_LIBE, Session.Item("IdUsuario")))
            dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(12).Text(), "02")
            If dtMail.Rows.Count > 0 Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("EMAI") <> "" Then
                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                "Liberaci�n de Warrant en custodia anulado por el cliente - " & Session.Item("NombreEntidad"),
                "Estimados Se�ores:<br><br>" &
                "Se ha anulado la Liberaci�n de Warrant en custodia N�: " & strNRO_LIBE &
                "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNRO_DOC_WARR & "</FONT></STRONG><br>" &
                "Depositante: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                "Usuario que anul� el documento: <STRONG><FONT color='#330099'>" & Session.Item("UsuarioLogin") & "</FONT></STRONG>" &
                "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
            End If
            dtMail.Dispose()
            dtMail = Nothing
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnEmitir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmitir.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "37", "EMISI�N DE LIBERACION DE WARRANT EN CUSTODIA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        If pr_GUAR_0001() Then
            Dim dtMail As New DataTable
            Dim dtCustodia As New DataTable
            Dim strNombreArchivoPDF As String
            Dim strNombreCliente As String
            Dim strResult As String
            Dim strEmailsConAdjunto As String = ""
            Dim strEmailsSinAdjunto As String = ""
            Dim strNroLiberacion As String = ""
            Dim strNroWarratsLib As String = ""
            Dim objConexion As SqlConnection
            Dim objTrans As SqlTransaction

            objConexion = New SqlConnection(strConn)
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()

            Try
                Dim nCOD_UNID As String
                Dim nNRO_DOCU As String
                Dim nCOD_TIPDOC As String
                Dim nNRO_LIBE As String
                Dim nCO_ALMA As String
                Session.Remove("dtCustodia")
                If ValidarItemsSeleccionados() = True Then
                    dtCustodia = CType(Session.Item("dtCustodia"), DataTable)
                    For i As Integer = 0 To dtCustodia.Rows.Count - 1
                        nCOD_UNID = dtCustodia.Rows(i)("COD_UNID")
                        nNRO_DOCU = dtCustodia.Rows(i)("NRO_DOCU")
                        nCOD_TIPDOC = dtCustodia.Rows(i)("COD_TIPDOC")
                        nNRO_LIBE = dtCustodia.Rows(i)("NRO_LIBE")
                        nCO_ALMA = dtCustodia.Rows(i)("CO_ALMA")
                    Next
                End If

                For i As Integer = 0 To dtCustodia.Rows.Count - 1
                    strNroLiberacion = strNroLiberacion & dtCustodia.Rows(i)("NRO_LIBE") & "-"
                    strNroWarratsLib = strNroWarratsLib & dtCustodia.Rows(i)("NRO_DOCU") & "-"
                Next

                strNroLiberacion = strNroLiberacion.Substring(0, strNroLiberacion.Length - 1)
                strNroWarratsLib = strNroWarratsLib.Substring(0, strNroWarratsLib.Length - 1)

                strResult = objDocumento.gInsLiberacionFlujoCustodia(Session.Item("IdUsuario"), System.Configuration.ConfigurationManager.AppSettings("CoUnidad"), Session.Item("IdSico"), strNroLiberacion, objTrans)
                UpdateItemLiberacionCustodia(strResult, objTrans)

                strNombreArchivoPDF = strPath & "21" & strResult & "00000000000" & ".pdf"

                strNombreCliente = objLiberacion.GetReporteLiberacionCustodia(strNombreArchivoPDF, strCoEmpresa, "DOR", nNRO_LIBE, Session.Item("IdSico"),
                dtCustodia, False, False, False, strPathFirmas)

                If strResult.Substring(0, 2) = "XX" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("No se pudo generar el documento")
                    Exit Sub
                Else
                    objTrans.Commit()
                    objTrans.Dispose()
                End If

                dtMail = objUsuario.gGetEmailAprobadores("00000001", nCO_ALMA, "02")

                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("FLG_ADJU") = True Then
                            strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("EMAI") & ","
                        Else
                            strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next

                    If strEmailsConAdjunto <> "" Then
                        objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                             "Se ha emitido la liberaci�n m�ltiple de Warrant en custodia - " & strNombreCliente,
                             "Estimados Se�ores:<br><br>" &
                             "Se ha emitido la liberaci�n m�ltiple de Warrant en custodia N�: " & strNroLiberacion &
                             "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarratsLib & "</FONT></STRONG><br>" &
                             "Depositante: <STRONG><FONT color='#330099'>" & strNombreCliente & "</FONT></STRONG>" &
                             "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreArchivoPDF)
                    End If

                    If strEmailsSinAdjunto <> "" Then
                        objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                            "Se ha emitido la liberaci�n m�ltiple de warrant en custodia - " & strNombreCliente,
                            "Estimados Se�ores:<br><br>" &
                            "Se ha emitido la liberaci�n m�ltiple de Warrant en custodia N�: " & strNroLiberacion &
                            "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & strNroWarratsLib & "</FONT></STRONG><br>" &
                            "Depositante: <STRONG><FONT color='#330099'>" & strNombreCliente & "</FONT></STRONG>" &
                            "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
                    End If
                End If

                dtMail.Dispose()
                dtMail = Nothing
                Session.Remove("dtCustodia")
                pr_IMPR_MENS("Se ha emitido un liberaci�n m�ltiple de Warrant en custodia Nro: " & strResult)
                BindDatagrid()
            Catch ex As Exception
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = "ERROR " + ex.Message
            End Try
        End If
    End Sub

    Private Function ValidarItemsSeleccionados() As Boolean
        Dim dt As DataTable
        Try
            dt = AgregarRegistros()
            Session.Add("dtCustodia", dt)
            Return True
        Catch ex As Exception
            Me.lblError.Text = ex.Message
            Return False
        End Try
    End Function

    Function AgregarRegistros() As DataTable
        Dim dt1 As New DataTable
        Dim dgItem As DataGridItem
        Dim dr As DataRow
        If Session.Item("dtCustodia") Is Nothing Then
            dt1.Columns.Add(New DataColumn("COD_UNID", GetType(String)))
            dt1.Columns.Add(New DataColumn("NRO_DOCU", GetType(String)))
            dt1.Columns.Add(New DataColumn("COD_TIPDOC", GetType(String)))
            dt1.Columns.Add(New DataColumn("NRO_LIBE", GetType(String)))
            dt1.Columns.Add(New DataColumn("CO_ALMA", GetType(String)))
        Else
            dt1 = CType(Session.Item("dtCustodia"), DataTable).Copy()
        End If

        For Each dgItem In dgdResultado.Items
            If CType(dgItem.FindControl("chST_SELE"), CheckBox).Checked = True Then
                dr = dt1.NewRow
                dr("COD_UNID") = dgItem.Cells(2).Text
                dr("NRO_DOCU") = dgItem.Cells(3).Text
                dr("COD_TIPDOC") = dgItem.Cells(10).Text
                dr("NRO_LIBE") = dgItem.Cells(5).Text
                dr("CO_ALMA") = dgItem.Cells(12).Text
                dt1.Rows.Add(dr)
            End If
        Next
        Return dt1
    End Function

    Function UpdateItemLiberacionCustodia(ByVal strNroDocuDest As String, ByVal ObjTrans As SqlTransaction) As Boolean
        Dim nNU_FILA As Integer
        Dim nCOD_UNID As String
        Dim nNRO_DOCU As String
        Dim nNOM_TIPDOC As String
        Dim nNRO_LIBE As String

        For nNU_FILA = 0 To Me.dgdResultado.Items.Count - 1
            Dim chST_MARC As CheckBox = CType(Me.dgdResultado.Items(nNU_FILA).FindControl("chST_SELE"), CheckBox)
            If chST_MARC.Checked Then
                nCOD_UNID = dgdResultado.Items(nNU_FILA).Cells(2).Text
                nNRO_DOCU = dgdResultado.Items(nNU_FILA).Cells(3).Text
                nNOM_TIPDOC = dgdResultado.Items(nNU_FILA).Cells(10).Text
                nNRO_LIBE = dgdResultado.Items(nNU_FILA).Cells(5).Text
                objDocumento.gUpdDetalleLiberacionCustodia(nNRO_DOCU, nNRO_LIBE, nNOM_TIPDOC, Session.Item("IdUsuario"), nCOD_UNID, strNroDocuDest, ObjTrans)
            End If
        Next
        Return True
    End Function

    Private Function pr_GUAR_0001() As Boolean
        Try
            Dim nNU_FILA As Integer
            Dim bOK_SELE As Boolean = False

            For nNU_FILA = 0 To Me.dgdResultado.Items.Count - 1
                Dim chST_MARC As CheckBox = CType(Me.dgdResultado.Items(nNU_FILA).FindControl("chST_SELE"), CheckBox)
                If chST_MARC.Checked Then
                    'selecciono al menos un registro
                    bOK_SELE = True
                End If
            Next
            'si no se selecciono ningun registro
            If bOK_SELE = False Then
                pr_IMPR_MENS("Debe seleccionar al menos un registro")
                Return False
                Exit Function
            End If
            Return True
        Catch e1 As Exception
            Return False
        End Try
    End Function

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnCancelar As New ImageButton
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If Session.Item("IdTipoEntidad") = "03" And (Session.Item("IdTipoUsuario") = "03" Or Session.Item("IdTipoUsuario") = "01") Then
                    CType(e.Item.FindControl("chST_SELE"), CheckBox).Enabled = True
                Else
                    CType(e.Item.FindControl("chST_SELE"), CheckBox).Enabled = False
                End If
                btnCancelar = CType(e.Item.FindControl("imgCancelar"), ImageButton)
                btnCancelar.Attributes.Add("onclick", "javascript:if(confirm('Desea anular la liberaci�n de Warrant en custodia Nro " & e.Item.DataItem("NRO_LIBE") & "?')== false) return false;")
                If Session.Item("IdTipoEntidad") = "03" Then
                    btnCancelar.Enabled = True
                Else
                    btnCancelar.Enabled = False
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub
End Class
