Imports System.Data.SqlClient
Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class RutaConfigurar
    Inherits System.Web.UI.Page
    Private objRuta As Ruta = New Ruta
    Private objEntidad As Entidad = New Entidad
    Private objAccesoWeb As clsCNAccesosWeb
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Dim objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboTipoDocumento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_RUTA") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_RUTA"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Al grabar generara una nueva ruta y debera de configurar los controles nuevamente')== false) return false;")
                loadTipoDocumento()
                BindDatagrid()
                ValidarControles()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ValidarControles()
        If Session.Item("IdTipoUsuario") = "04" Then
            Me.btnGrabar.Enabled = True
            Me.btnAgregar.Enabled = True
        Else
            Me.btnGrabar.Enabled = False
            Me.btnAgregar.Enabled = False
        End If
    End Sub

    Private Sub BindDatagrid()
        Try
            dt = Nothing
            dt = New DataTable
            dt = objRuta.gGetRutaxTipoDocumento(Me.cboTipoDocumento.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Private Sub loadTipoDocumento()
        objfunciones.GetTipoDocumento(Me.cboTipoDocumento, 0)
    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged
        BindDatagrid()
    End Sub

    Public Function GetTipoEntidad() As DataTable
        Return objEntidad.gGetTipoEntidades
    End Function

    Public Function GetEstado() As DataTable
        Return objEntidad.gGetEstado("0")
    End Function

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            CargarTabla()
            dt.Rows.RemoveAt(dgi.ItemIndex)
            For Each dr1 As DataRow In dt.Rows
                dt.Rows(j).AcceptChanges()
                dt.Rows(j).BeginEdit()
                dt.Rows(j).Item(0) = j + 1
                dt.Rows(j).EndEdit()
                j += 1
            Next
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim dr As DataRow
        CargarTabla()
        dr = dt.NewRow
        dr(0) = Me.dgdResultado.Items.Count + 1
        dr(1) = ""
        dr(2) = ""
        dr(3) = ""
        dr(4) = "01"
        dr(5) = "01"
        dr(6) = 0
        dr(7) = 0
        dt.Rows.Add(dr)
        Me.dgdResultado.DataSource = dt
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        InsertarSecuencia()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "M", "MAESTRO_RUTA", "CONFIGURAR RUTA DE APROBACION", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub CargarTabla()
        Dim dr As DataRow
        Dim dgItem As DataGridItem
        dt.Rows.Clear()
        For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
            dgItem = Me.dgdResultado.Items(i)
            dr = dt.NewRow
            dr(0) = dgItem.Cells(0).Text()
            dr(1) = ""
            dr(2) = Me.cboTipoDocumento.SelectedValue
            dr(3) = CType(dgItem.FindControl("txtDescripcion"), TextBox).Text
            dr(4) = CType(dgItem.FindControl("cboTipoEntidad"), DropDownList).SelectedValue
            dr(5) = CType(dgItem.FindControl("cboEstado"), DropDownList).SelectedValue
            dr(6) = CType(dgItem.FindControl("chkFirma"), CheckBox).Checked
            dr(7) = CType(dgItem.FindControl("chkDblEndo"), CheckBox).Checked
            dt.Rows.Add(dr)
        Next i
    End Sub

    Private Sub InsertarSecuencia()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            Dim strNroSecuencia As String
            Dim strCodTipoEntidad As String
            Dim strDescripcion As String
            Dim strCodEstado As String
            Dim dgItem As DataGridItem
            For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(i)
                strNroSecuencia = dgItem.Cells(0).Text()
                strCodTipoEntidad = CType(dgItem.FindControl("cboTipoEntidad"), DropDownList).SelectedValue
                strDescripcion = CType(dgItem.FindControl("txtDescripcion"), TextBox).Text
                strCodEstado = CType(dgItem.FindControl("cboEstado"), DropDownList).SelectedValue
                objRuta.gInsSecuencia(strNroSecuencia, Me.cboTipoDocumento.SelectedValue, strDescripcion,
                strCodTipoEntidad, strCodEstado, Session.Item("IdUsuario"), objTrans)
            Next i
            objTrans.Commit()
            objTrans.Dispose()
            BindDatagrid()
            Me.lblError.Text = "Se actualiz� correctamente"
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Se produjo el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
        End If
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnEliminar As ImageButton
        Dim blnEstado As Boolean
        Dim chkFirma As CheckBox
        Dim chkDblEndo As CheckBox
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            btnEliminar = CType(e.Item.FindControl("imgEliminar"), ImageButton)

            If Session.Item("IdTipoUsuario") = "04" Then 'Solo el Administrador de Alma Per� podra eliminar ruta
                btnEliminar.Enabled = True
            Else
                btnEliminar.Enabled = False
                btnEliminar.ImageUrl = "Images/Eliminar1.jpg"
            End If
            chkFirma = CType(e.Item.FindControl("chkFirma"), CheckBox)
            chkDblEndo = CType(e.Item.FindControl("chkDblEndo"), CheckBox)
            If Convert.ToString(e.Item.DataItem("NRO_RUTA")) = "" Then
                chkFirma.Enabled = False
                chkDblEndo.Enabled = False
            Else
                chkFirma.Enabled = True
                chkDblEndo.Enabled = True
            End If
            chkFirma.Checked = e.Item.DataItem("FLG_FIRM")
            chkDblEndo.Checked = e.Item.DataItem("FLG_DBLENDO")
        End If
    End Sub

    Public Sub chkFirma_shange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkFirma As CheckBox = CType(sender, CheckBox)
            Dim blnFirma As Boolean
            Dim dgi As DataGridItem
            Dim strSecuencia As String
            dgi = CType(chkFirma.Parent.Parent, DataGridItem)
            blnFirma = CType(dgi.FindControl("chkFirma"), CheckBox).Checked()
            strSecuencia = dgi.Cells(0).Text()
            objRuta.gUpdFirma(strSecuencia, Me.cboTipoDocumento.SelectedValue, Session.Item("IdUsuario"), blnFirma)
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub chkDblEndo_shange(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkDblEndo As CheckBox = CType(sender, CheckBox)
            Dim blnDblEndo As Boolean
            Dim dgi As DataGridItem
            Dim strSecuencia As String
            dgi = CType(chkDblEndo.Parent.Parent, DataGridItem)
            blnDblEndo = CType(dgi.FindControl("chkDblEndo"), CheckBox).Checked()
            strSecuencia = dgi.Cells(0).Text()
            objRuta.gUpdDblEndo(strSecuencia, Me.cboTipoDocumento.SelectedValue, Session.Item("IdUsuario"), blnDblEndo)
            BindDatagrid()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRuta Is Nothing) Then
            objRuta = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objfunciones Is Nothing) Then
            objfunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
