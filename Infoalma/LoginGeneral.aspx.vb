Imports System.Data
Imports System.Web.Security
Imports Infodepsa

Public Class LoginGeneral
    Inherits System.Web.UI.Page
    Private objAcceso As Library.AccesoDB.Acceso
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objUsuario As Library.AccesoDB.Usuario = New Library.AccesoDB.Usuario
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            objAcceso = New Library.AccesoDB.Acceso
            'Dim strContrase�a As String
            Dim txtUsuario As String = Request.QueryString("id100")
            'txtUsuario = Library.AccesoBL.Funciones.DecryptString(txtUsuario, "_��_[]0")
            'txtUsuario = "MANDRADET" 'cliete
            '  txtUsuario = "JCAVALOS" 'banco

            txtUsuario = "JFRANCISCO"

            '(1)------------------------ FIRMAS DEPSA -------------------------------------------
            ' txtUsuario = "CALDANA"   ' depsa
            ' txtUsuario = "GMARIATEGUI"   ' depsa
            ' '---verificacion automatica

            '(2)------------------------ ENDOSO BANCO -------------------------------------------
            ' txtUsuario = "AORDONEZ" ' financiador (bbva continental)
            ' txtUsuario = "NGONZALEZ" ' financiador (bbva continental)
            ' txtUsuario = "JSCHROTH" ' financiador (LATIN AMERICA EXPORT FINANCE FUND II  LTD)
            ' txtUsuario = "OSANDOVAL" ' financiador (LATIN AMERICA EXPORT FINANCE FUND II  LTD)

            '(3)------------------------ FIRMAS CLIENTE -------------------------------------------
            'txtUsuario = "HSAMALVIDESM" ' Depositante CAMPOSUR INC S.A.C.
            'txtUsuario = "ELEONM" ' Depositante CAMPOSUR INC S.A.C.

            '(4)------------------------ FIRMA BANCO -------------------------------------------
            ' txtUsuario = "AORDONEZ" ' financiador (bbva continental)
            ' txtUsuario = "NGONZALEZ" ' financiador (bbva continental)
            ' txtUsuario = "JSCHROTH" ' financiador (LATIN AMERICA EXPORT FINANCE FUND II  LTD)
            ' txtUsuario = "OSANDOVAL" ' financiador (LATIN AMERICA EXPORT FINANCE FUND II  LTD)


            'strContrase�a = objFunciones.EncryptString(Me.txtContrase�a.Text.Trim, "���_[]0")
            With objAcceso
                If .gInicioSesionGeneral(txtUsuario, pr_OBTI_IP_PETI) Then
                    Session.Add("IdUsuario", .strIdUsuario)
                    Session.Add("IdTipoUsuarioFiles", .strTipoUsuario)
                    Session.Add("UsuarioLogin", txtUsuario)
                    Session.Add("PeriodoAnual", Date.Now.Year)
                    Session.Add("nom_user", .strNombre)
                    Session.Add("dir_emai", .strDirEmai)
                    Session.Add("cod_sist", "SMIF0002")
                    Session.Add("NroCertificado", .strNroCertificado)
                    PoliticaCache()
                    FormsAuthentication.Initialize()
                    FormsAuthentication.RedirectFromLoginPage(Session.Item("IdUsuario"), False)
                    Response.Redirect("Cliente.aspx?sPagina=", False) 'nuevo
                    Exit Sub
                Else
                    If .strResult = "N" Then
                        Me.Response.Write("El usuario " & txtUsuario & " que ingresastes no esta registrado en el sistema. Por favor, vuelve a intentarlo")
                        txtUsuario = ""
                        Exit Sub
                    End If
                    If .strResult = "C" Then
                        Session.Add("IdUsuario", .strIdUsuario)
                        Session.Add("UsuarioLogin", txtUsuario)
                        Response.Redirect("ActualizarContrase�a.aspx", False)
                        Exit Sub
                    End If
                    If .strResult = "X" Then
                        Me.Response.Write("La contrase�a ingresada no escorrecta, por favor, vuelve a intentarlo")
                        Dim dtIntentos As DataTable
                        Dim dr As DataRow
                        dtIntentos = Session.Item("dtIntentos")
                        dr = dtIntentos.NewRow
                        dr(0) = txtUsuario
                        dtIntentos.Rows.Add(dr)
                        Dim drTotal As DataRow()
                        Dim count As Integer
                        count = 0
                        drTotal = dtIntentos.Select("IdUsuario='" & txtUsuario & "'")
                        For Each df As DataRow In drTotal
                            count += 1
                        Next
                        If count >= 3 Then
                            objUsuario.gCambiarEstadoUsuario(.strIdUsuario, "0", "X")
                            Me.Response.Write("Te has equivocado 3 veces al ingresar tu clave. Tu usuario ha sido desactivado, comunicate con el administrador")
                        End If
                        Session.Add("dtIntentos", dtIntentos)
                        dtIntentos.Dispose()
                        dtIntentos = Nothing
                        Exit Sub
                    End If
                End If
            End With
        Catch ex As Exception
            Me.Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub
End Class
