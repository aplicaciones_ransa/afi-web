Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class Encuesta
    Inherits System.Web.UI.Page
    Private objFunciones As Library.AccesoBL.Funciones
    Private objAcceso As Acceso = New Acceso
    'Protected WithEvents rb1a As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb1b As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb1c As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb1d As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb2a As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb2c As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb2b As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb2d As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb3a As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb3b As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb3c As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents rb3d As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents txt4a As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblPregunta1 As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPregunta2 As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPregunta3 As System.Web.UI.WebControls.Label
    'Protected WithEvents lblPregunta4 As System.Web.UI.WebControls.Label
    'Protected WithEvents rb3e As System.Web.UI.WebControls.RadioButton
    'Protected WithEvents btnCerrar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblCuerpo As System.Web.UI.WebControls.Label
    Private objUsuario As Library.AccesoDB.Usuario = New Library.AccesoDB.Usuario
#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            listarPreguntas()
        End If
    End Sub

    Private Sub listarPreguntas()

        Me.lblTitulo.Text = "Se�ores(as), con el fin de mejorar el sistema AFI, agradeceremos responder la siguiente encuesta:"
        Me.lblCuerpo.Text = "En caso tenga una duda sobre el manejo del mismo comun�quese a servicio@almaperu.com.pe, en caso no desee dar la encuesta en estos momentos dar click en el bot�n cerrar"

        Me.lblPregunta1.Text = "1) C�mo califica la velocidad de respuesta del sistema?:"
        Me.rb1a.Text = "a) Muy R�pido"
        Me.rb1b.Text = "b) R�pido"
        Me.rb1c.Text = "c) Lento"
        Me.rb1d.Text = "d) Muy Lento"

        Me.lblPregunta2.Text = "2) Considera que el manejo del sistema es:"
        Me.rb2a.Text = "a) Sencillo F�cil y/o Predictivo."
        Me.rb2b.Text = "b) Complicado"
        Me.rb2c.Visible = False
        Me.rb2d.Visible = False

        Me.lblPregunta3.Text = "3) El sistema cubre sus expectativas de informaci�n?."
        Me.rb3a.Text = "a) Totalmente."
        Me.rb3b.Text = "b) Lo necesario."
        Me.rb3c.Text = "c) Muy poco."
        Me.rb3d.Text = "d) Ninguno."
        Me.rb3e.Text = "e) No lo uso."

        Me.lblPregunta4.Text = "4) Que Informaci�n adicional desear�a ver en el AFI?"

    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click

        Dim strRes1 As String
        Dim strRes2 As String
        Dim strRes3 As String
        Dim strRes4 As String
        Dim validar As String
        validar = True

        If Me.txt4a.Text = "" Then
            Me.lblMensaje.Text = "Ingrese la respuesta numero 4"
            validar = False
        End If

        If rb3a.Checked = True Then
            strRes3 = "a"
        ElseIf rb3b.Checked = True Then
            strRes3 = "b"
        ElseIf rb3c.Checked = True Then
            strRes3 = "c"
        ElseIf rb3d.Checked = True Then
            strRes3 = "d"
        ElseIf rb3e.Checked = True Then
            strRes3 = "e"
        Else
            Me.lblMensaje.Text = "Seleccione la pregunta numero 3"
            validar = False
        End If

        If rb2a.Checked = True Then
            strRes2 = "a"
        ElseIf rb2b.Checked = True Then
            strRes2 = "b"
        ElseIf rb2c.Checked = True Then
            strRes2 = "c"
        ElseIf rb2d.Checked = True Then
            strRes2 = "d"
        Else
            Me.lblMensaje.Text = "Seleccione la pregunta numero 2"
            validar = False
        End If

        If rb1a.Checked = True Then
            strRes1 = "a"
        ElseIf rb1b.Checked = True Then
            strRes1 = "b"
        ElseIf rb1c.Checked = True Then
            strRes1 = "c"
        ElseIf rb1d.Checked = True Then
            strRes1 = "d"
        Else
            Me.lblMensaje.Text = "Seleccione la pregunta numero 1"
            validar = False
        End If


        If validar = True Then

            Dim dtGrabar As DataTable
            dtGrabar = objAcceso.sCADGrabarEncuesta("1", Session.Item("IdUsuario"), Session.Item("Cliente"), Session.Item("IdTipoEntidad"), strRes1, strRes2, strRes3, Me.txt4a.Text)
            Me.lblMensaje.Text = dtGrabar.Rows(0)("MENSAJE")
            CerrarEncuesta()

        End If

    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        CerrarEncuesta()
    End Sub

    Private Sub CerrarEncuesta()
        Response.Redirect("Cliente.aspx?sPagina=" & Session.Item("cboOpcionCerrarEncuesta"), False)
        Session.Add("ssEncuestaCerrar", "true")
    End Sub
End Class
