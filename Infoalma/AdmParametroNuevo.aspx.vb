Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AdmParametroNuevo
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objParametro As clsCNParametro
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboDominio As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtValor1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtValor2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtValor3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDescLarg As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNombPar As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnNuevo As System.Web.UI.WebControls.Button
    'Protected WithEvents txtValor4 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtValor5 As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        Me.lblOpcion.Text = "Nuevo Par�metro"
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_PARAMETROS") Then
            If Page.IsPostBack = False Then
                llenarDominios()
                Me.cboDominio.SelectedIndex = 0
                Me.btnNuevo.Visible = False
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_PARAMETROS"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("CodDomin") <> "" And Session.Item("CodParm") <> "" Then
                    Me.lblOpcion.Text = "Modificar Par�metro"
                    Me.btnGrabar.Text = "Actualizar"
                    Me.cboDominio.Enabled = False
                    CargarDatosParametro(Val(Session.Item("CodParm")), Val(Session.Item("CodDomin")))
                    Me.btnNuevo.Visible = True
                ElseIf Session.Item("CodDomin") <> "" Then
                    CargarDatosDominio(Val(Session.Item("CodDomin")))
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Session.Item("CodParm") = ""
        Session.Item("CodDomin") = ""
        Response.Redirect("AdmParametro.aspx")
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        objParametro = New clsCNParametro
        If Me.txtNombPar.Text = "" Then
            Me.lblError.Text = "Debe ingresar el c�digo de par�metro"
            Exit Sub
        Else
            If Me.btnGrabar.Text = "Grabar" Then
                Try
                    With objParametro
                        If ValidarParametro() Then
                            .gCNInsertarParametros(Val(Me.cboDominio.SelectedItem.Value), Me.txtNombPar.Text, Me.txtDescLarg.Text, Me.txtValor1.Text,
                                            Me.txtValor2.Text, Me.txtValor3.Text, Me.txtValor4.Text, Me.txtValor5.Text, Session.Item("IdUsuario"))
                            If .fn_devuelveResultado = "X" Then
                                Me.lblError.Text = "Se insert� correctamente el par�metro"
                                Me.btnGrabar.Text = "Actualizar"
                                Me.btnNuevo.Visible = True
                                Me.cboDominio.Enabled = False
                                CapturarParametro()
                            End If
                        Else
                            Me.lblError.Text = "El c�digo ya existe,ingrese otro."
                        End If
                    End With
                Catch ex As Exception
                    Me.lblError.Text = "No se pudo insertar el par�metro"
                    Me.lblError.Text = ex.Message
                End Try
            ElseIf Me.btnGrabar.Text = "Actualizar" Then
                Try
                    With objParametro
                        .gCNActualizarParametro(Val(Session.Item("CodDomin")), Val(Session.Item("CodParm")), Me.txtNombPar.Text,
                                                Me.txtDescLarg.Text, Me.txtValor1.Text, Me.txtValor2.Text, Me.txtValor3.Text,
                                                Me.txtValor4.Text, Me.txtValor5.Text, Session.Item("IdUsuario"))
                        If .fn_devuelveResultado = "X" Then
                            Me.lblError.Text = "Se Modific� correctamente el par�metro"
                        End If
                    End With
                Catch ex As Exception
                    Me.lblError.Text = "No se pudo modificar el par�metro"
                    Me.lblError.Text = ex.Message
                End Try
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_PARAMETROS", "ACTUALIZAR PARAMETRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        End If
    End Sub

    Private Sub llenarDominios()
        objParametro = New clsCNParametro
        Dim dttemporal As DataTable

        Try
            With objParametro
                .gCNMostrarDominios("1", 0)
                dttemporal = .fn_devuelveDataTable
            End With
            For Each dr As DataRow In dttemporal.Rows
                Me.cboDominio.Items.Add(New ListItem(dr("DE_LARG_DOMIN"), dr("CO_DOMIN")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "No se pudieron leer los datos del dominio"
        End Try

    End Sub

    Private Sub LimpiarDatos()
        'Me.cboDominio.SelectedIndex = 0
        Me.txtNombPar.Text = ""
        Me.txtDescLarg.Text = ""
        Me.txtValor1.Text = ""
        Me.txtValor2.Text = ""
        Me.txtValor3.Text = ""
        Me.txtValor4.Text = ""
        Me.txtValor5.Text = ""
        Me.lblError.Text = ""
    End Sub

    Private Sub CargarDatosParametro(ByVal intco_par, ByVal intco_domin)
        objParametro = New clsCNParametro
        Dim dtTemporal As DataTable
        Dim intindice As Integer

        Try
            With objParametro
                .gCADMostrarParametros(intco_par, intco_domin, "2")
                dtTemporal = .fn_devuelveDataTable
            End With
            If dtTemporal.Rows.Count > 0 Then
                intindice = dtTemporal.Rows(0)("CO_DOMIN")
                Me.cboDominio.SelectedValue = intindice
                Me.txtNombPar.Text = dtTemporal.Rows(0)("NO_PAR").trim
                Me.txtDescLarg.Text = dtTemporal.Rows(0)("DE_LARG_PAR").trim
                Me.txtValor1.Text = dtTemporal.Rows(0)("VALOR1").trim
                Me.txtValor2.Text = dtTemporal.Rows(0)("VALOR2").trim
                Me.txtValor3.Text = dtTemporal.Rows(0)("VALOR3").trim
                Me.txtValor4.Text = dtTemporal.Rows(0)("VALOR4").trim
                Me.txtValor5.Text = dtTemporal.Rows(0)("VALOR5").trim
            End If
        Catch ex As Exception
            Me.lblError.Text = "No se pudieron mostrar los par�metros"
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.btnGrabar.Text = "Grabar"
        Me.cboDominio.Enabled = False
        LimpiarDatos()
    End Sub

    Private Sub CargarDatosDominio(ByVal intco_domin)
        objParametro = New clsCNParametro
        Dim dtTemporal As DataTable
        Dim intindice As Integer

        Try
            With objParametro
                .gCNMostrarDominios("2", intco_domin)
                dtTemporal = .fn_devuelveDataTable
            End With
            If dtTemporal.Rows.Count > 0 Then
                Me.cboDominio.SelectedValue = dtTemporal.Rows(0)("CO_DOMIN")
            End If
        Catch ex As Exception
            Me.lblError.Text = "No se pudieron mostrar los dominios"
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Function ValidarParametro() As Boolean
        objParametro = New clsCNParametro
        Dim dt As DataTable
        With objParametro
            .gCNValidarParametro(Me.txtNombPar.Text.Trim, Val(Session.Item("CodDomin")))
            dt = .fn_devuelveDataTable
            If dt.Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If
        End With
    End Function

    Private Sub CapturarParametro()
        objParametro = New clsCNParametro
        Dim dt As DataTable
        With objParametro
            .gCNValidarParametro(Me.txtNombPar.Text.Trim, Val(Session.Item("CodDomin")))
            dt = .fn_devuelveDataTable
            Session.Item("CodDomin") = dt.Rows(0)("CO_DOMIN")
            Session.Item("CodParm") = dt.Rows(0)("CO_PAR")
        End With
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
