<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoFirmaWMS.aspx.vb" Inherits="DocumentoFirmaWMS" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DocumentoPDF</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script src="JScripts/jquery.min.cache.js"></script>
		<script src="JScripts/jquery-ui.min.cache.js"></script>
		<LINK rel="stylesheet" type="text/css" href="Styles/jquery-ui.cache.css">
		<script language="VBScript">
			Const CAPICOM_CURRENT_USER_STORE = 2
			Const CAPICOM_MY_STORE = "My"
			Const CAPICOM_STORE_OPEN_READ_ONLY = 0
			Const CAPICOM_STORE_OPEN_EXISTING_ONLY = 128
			Const CAPICOM_CERTIFICATE_FIND_KEY_USAGE = 12
			Const CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE = 128
			Const CAPICOM_CERT_INFO_SUBJECT_EMAIL_NAME = 2
			
			Function SignFile(pSignerCert,pstrFecExp)
				Dim Signer
				Dim SignedData
				Dim strNCert
				Dim strNBD
				Dim strSrvExp
				Dim strFecExp
				
				On Error Resume Next
				
				Set SignedData = CreateObject("CAPICOM.SignedData")
				Set Signer = CreateObject("CAPICOM.Signer")
				Set Att1 = CreateObject("CAPICOM.Attribute")

				Signer.Certificate = pSignerCert
				strNBD = frmArchivo.txtNumSerie.value
				'MsgBox "Serie BD " & strNBD
				strNCert = Signer.Certificate.SerialNumber
				'msgbox "Serie User " & strNCert				
'				Replace(Signer.Certificate.SerialNumber,Chr(0),"")
				If LCase(strNBD) = LCase(strNCert) Then
					SignFile = 0
				Else
					SignFile = 1
					MsgBox "El Certificado seleccionado no corresponde al registrado en el Sistema." , vbInformation," Firma Digital "
					Exit Function
				End If
				'strSrvExp = <%=Now()%>
				'strSrvExp = date()
				strSrvExp = frmArchivo.txtTimeStamp.value
				strSrvExp = mid(strSrvExp,7,4)+mid(strSrvExp,4,2)+mid(strSrvExp,1,2)
				strFecExp = mid(pstrFecExp,7,4)+mid(pstrFecExp,4,2)+mid(pstrFecExp,1,2)
				'strFecExp = "20101111"
				'msgbox "Fecha Certificado " & strFecExp
				'msgbox "Fecha Servidor " & strSrvExp
				If strFecExp < strSrvExp Then
					SignFile = 1
					MsgBox "El Certificado seleccionado se encuentra Expirado." , vbInformation ," Firma Digital "
					Exit Function
				End If

				Att1.Name = 2
				'Att1.Value = Cstr(Now()) & "|"
				Att1.Value = frmArchivo.txtTimeStamp.value & "|"
				Signer.AuthenticatedAttributes.Add Att1
				
				strContenido = frmArchivo.txtArchivo_pdf.value

				SignedData.Verify strContenido, False, CAPICOM_VERIFY_SIGNATURE_ONLY 				
				'msgbox "SignFile Error ... " & Err.Number
				If Err.Number = 0 then
					frmArchivo.txtArchivo_pdf_firmado.value = SignedData.CoSign(Signer, 0)
				Else
					'solo se firma cuando err = -2146881269, es decir cuando se quiere firmar 
					'un archivo sin firma(Etiqueta ANSI Incorrecta), 
					'los demas errores dentro de la funcion no implican firma
					If Err.Number = -2146881269 or Err.Number = -2146881532 or Err.Number = -2146881527 then
						Err.Clear
						SignedData.Content = frmArchivo.txtArchivo_pdf.value  'SignedData.Content = strContenido
						'msgbox "Inicio de Firma ..."
						frmArchivo.txtArchivo_pdf_firmado.value = SignedData.Sign(Signer, False, 0)
						'msgbox "Fin de Firma ..."
						IF Err.Number<>0 THEN
							SignFile = 1
							Exit function
						End If
					Else
						SignFile = 1
						MsgBox "Hay un error durante el proceso de firma." , vbInformation ," Firma Digital "
						Exit function
					End if
				End If
				If Len(frmArchivo.txtArchivo_pdf_firmado.value) = 0 Then
					SignFile = 1
					MsgBox "Hay un error durante el proceso de firma." , vbInformation ," Firma Digital "
					Exit function
				End If
			End Function
			
			Function Sign(pstrSN)
				Dim SignedData2
				Dim Sig
								
				On Error Resume Next

				Set SignedData2 = CreateObject("CAPICOM.SignedData")
				Set Sig = CreateObject("CAPICOM.Signer")

				strContenido = frmArchivo.txtArchivo_pdf.value
				SignedData2.Verify strContenido, False, CAPICOM_VERIFY_SIGNATURE_ONLY 
				'msgbox "Sign ... Error " & Err.Number
				If Err.Number = 0 then
					'msgbox "Serie Sign Select : " & LCase(pstrSN)
					For Each Sig In SignedData2.Signers
						strBD = Sig.Certificate.SerialNumber
						'msgbox "Serie Sign Firmas : " & strBD
						If LCase(strBD) = LCase(pstrSN) Then
						'If LCase(Replace(Sig.Cetificate.SerialNumber,Chr(0),"")) = LCase(pstrSN) Then
							Sign = 1							
							Exit Function
						End If
					Next
				End If
				Err.Clear
			End Function
			
			Sub SubmitForm(intVar)
			'---------------------------COMENTAR TODO ESTE BLOQUE SI SE QUIERE LA VERSION SIN FIRMAS-----------------------------------------------------
				On Error Resume Next
				frmArchivo.cmdFirmar.disabled = true
				Dim Certs
				Dim Store
				Dim SignerCert
				Dim strSN
				Dim strFecExp								
				Set Store = CreateObject("CAPICOM.Store")
				Set SignerCert = CreateObject("CAPICOM.Certificate")
				
				Store.Open CAPICOM_CURRENT_USER_STORE, CAPICOM_MY_STORE, CAPICOM_STORE_OPEN_READ_ONLY Or CAPICOM_STORE_OPEN_EXISTING_ONLY
				Set Certs = Store.Certificates
									
				If Certs.Count = 0 Then
				Msgbox "No se encontr� ningun certificado V�lido." , vbInformation," Firma Digital "
				frmArchivo.cmdFirmar.disabled = false
				Else
						Set SelectedCerts = Certs.Select()
					if err.number = 0 then
						Set SignerCert = SelectedCerts(1)
					else
						frmArchivo.cmdFirmar.disabled = false
						exit Sub
					end if
				
					strSN = SignerCert.SerialNumber
					strFecExp = mid(SignerCert.ValidToDate,1,10)
					If checkCertStatus(SignerCert) Then
				'		If Sign(strSN) = 0 then
							If SignFile(SignerCert,strFecExp) = 0 Then
							frmArchivo.action = "DocumentoFirmado.aspx?var=" & intVar & "&Pagina=1"
							frmArchivo.submit
							Else
							frmArchivo.cmdFirmar.disabled = false
							End If
				'		Else			
				'			Msgbox "Ud. ya ha firmado digitalmente este archivo y no puede volver a firmarlo." , vbInformation," Firma Digital "
				'			frmArchivo.cmdFirmar.disabled = false
				'		End If
					Else	
						frmArchivo.cmdFirmar.disabled = false
					End If
				End If
				Set Certs = Nothing
				Set Store = Nothing
				Set Signer = Nothing
				Set SignerCert = Nothing
				'---------------------------------------------------------------------------------
			End Sub
			Sub SubmitForm2(intVar)
				frmArchivo.action = "DocumentoFirmado.aspx?var=" & intVar & "&Pagina=1"
				frmArchivo.submit
			End Sub
		</script>
		<script language="javascript">
		   var $dialog = null;
            jQuery.showModalDialog = function(options) {

            var defaultOptns = {
                url: null,
                dialogArguments: null,
                height: 'auto',
                width: 'auto',
                position: 'top',
                resizable: false,
                scrollable: false,
                onClose: function() { },
                returnValue: null,
                doPostBackAfterCloseCallback: false,
                postBackElementId: null
            };

            var fns = {
                close: function() {
                    opts.returnValue = $dialog.returnValue;
                    $dialog = null;
                    opts.onClose();
                    if (opts.doPostBackAfterCloseCallback) {
                        postBackForm(opts.postBackElementId);
                    }
                },
                adjustWidth: function() { $frame.css("width", "100%"); }
            };

            // build main options before element iteration
            var opts = $.extend({}, defaultOptns, options);
            var $frame = $('<iframe id="iframeDialog" />');

            if (opts.scrollable)
                $frame.css('overflow', 'auto');
                
            $frame.css({
                'padding': 0,
                'margin': 0,
                'padding-bottom': 2
            });

            var $dialogWindow = $frame.dialog({
                autoOpen: true,
                modal: true,
                width: opts.width,
                height: opts.height,
                resizable: opts.resizable,
                position: opts.position,
                overlay: {
                    opacity: 0.5,
                    background: "black"
                },
                close: fns.close,
                resizeStop: fns.adjustWidth
            });

            $frame.attr('src', opts.url);
            fns.adjustWidth();

            $frame.load(function() {
                if ($dialogWindow) {
                    var maxTitleLength = 50;
                    var title = $(this).contents().find("title").html();

                    if (title.length > maxTitleLength) {
                        title = title.substring(0, maxTitleLength) + '...';
                    }
                    $dialogWindow.dialog('option', 'title', title);
                }
            });

            $dialog = new Object();
            $dialog.dialogArguments = opts.dialogArguments;
            $dialog.dialogWindow = $dialogWindow;
            $dialog.returnValue = null;
        }

        function postBackForm(targetElementId) {
            var theform;
            theform = document.forms[0];
            theform.__EVENTTARGET.value = targetElementId;
            theform.__EVENTARGUMENT.value = "";
            theform.submit();
        }
        
		function openWindow(strFirma)
		{				
			if (strFirma == 1)		 
			{
	   			SubmitForm2(1);
			}	
			else				
			{
			    var Argumentos = new Object();
				var url = 'Popup/LgnSolicitaFirma.aspx';
				$.showModalDialog({
					url: url,
					dialogArguments: null,
					heigh: 110,
					width: 360,
					scrollable: false,
					onClose: function(){ Argumentos = this.returnValue; 
					if(Argumentos == null)
					{
						window.alert('Usted a cancelado el proceso de firma!');
					}
					else
					{ //quiere decir que se ha devuelto una lista de contactos			    
						document.getElementById('txtContrasena').value = Argumentos[0];
						document.getElementById('txtNuevaContrasena').value = Argumentos[1];
						SubmitForm2(1);
					}		
					}
				});
					//var Argumentos = new Object();
					//Argumentos = window.showModalDialog("Popup/LgnSolicitaFirma.aspx", Argumentos, "dialogHeight:150px; dialogWidth:310px; edge:Raised; center:Yes; help:No; resizable:No; status:No; scroll=no"); 

					//if(Argumentos == null)
					//{
					//	window.alert('Usted a cancelado el proceso de firma!');
					//}
					//else
					//{
					//	document.getElementById('txtContrasena').value = Argumentos[0];
					//	document.getElementById('txtNuevaContrasena').value = Argumentos[1];
					//	SubmitForm2(1);
					//}
			}
		}
		</script>
		<script language="jscript">
		// CAPICOM constants
		var CAPICOM_INFO_SUBJECT_SIMPLE_NAME = 0;
		var CAPICOM_INFO_ISSUER_SIMPLE_NAME = 1;
		var CAPICOM_INFO_SUBJECT_EMAIL_NAME = 2;
		var CAPICOM_INFO_ISSUER_EMAIL_NAME  = 3;

		var CAPICOM_CHECK_NONE = 0;
		var CAPICOM_CHECK_TRUSTED_ROOT = 1;
		var CAPICOM_CHECK_TIME_VALIDITY = 2;
		var CAPICOM_CHECK_SIGNATURE_VALIDITY = 4;
		var CAPICOM_CHECK_ONLINE_REVOCATION_STATUS = 8;
		var CAPICOM_CHECK_OFFLINE_REVOCATION_STATUS = 16;
		var CAPICOM_CHECK_ONLINE_ALL = 495;

		var CAPICOM_TRUST_IS_NOT_TIME_VALID = 1;
		var CAPICOM_TRUST_IS_NOT_TIME_NESTED = 2;
		var CAPICOM_TRUST_IS_REVOKED = 4;
		var CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID = 8;
		var CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE = 16;
		var CAPICOM_TRUST_IS_UNTRUSTED_ROOT = 32;
		var CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN = 64;
		var CAPICOM_TRUST_IS_CYCLIC = 128;
		var CAPICOM_TRUST_IS_PARTIAL_CHAIN = 65536;
		var CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID = 131072;
		var CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID = 262144;
		var CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE = 524288;
		
		function checkCertStatus(pCert)
		{
			var Certificate = pCert;

			window.status="Comprobando el Estado del Certificado ...";
			// CAPICOM exposes Certificate status checking through IsValid, the CheckFlag parameter
			// allows you to specify the items you want to have checked for you.
			Certificate.IsValid().CheckFlag = (CAPICOM_CHECK_TRUSTED_ROOT | CAPICOM_CHECK_TIME_VALIDITY | CAPICOM_CHECK_SIGNATURE_VALIDITY | CAPICOM_CHECK_ONLINE_REVOCATION_STATUS);
			if (Certificate.IsValid().Result == true)
			{
				// clear the status window
				window.status="";
//				alert("El Certificado de \"" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "\" es Valido.");
				return true;
			}
			else
				{
					var Chain = new ActiveXObject("CAPICOM.Chain");
					Chain.Build(Certificate)

					// clear the status window
					window.status="";
					if (CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID & Chain.Status)
					{
						alert("Se encontr� un problema con la firma del Certificado a nombre de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "'");
						return false;
					}
					if ((CAPICOM_TRUST_IS_UNTRUSTED_ROOT & Chain.Status) || (CAPICOM_TRUST_IS_PARTIAL_CHAIN & Chain.Status))
					{
						alert("No se ha podido establecer el Certificado de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "' a una Autoridad Certificadora (CA) de confianza.");
						return false;
					}
					if (CAPICOM_TRUST_IS_REVOKED & Chain.Status)
					{
						alert("El Certificado seleccionado a nombre de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "' \n se encuentra revocado por su Autoridad Certificadora (CA).");
						return false;
					}
					else
						{
							return true;
						}
/*					if (CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN & Chain.Status)
					{
						alert("No se ha podido determinar el estado del Certificado a nombre de '" +  Certificate.GetInfo(CAPICOM_INFO_SUBJECT_SIMPLE_NAME) + "'");
						return false;
					}
*/				}
		}
		</script>
		<script language="javascript">		
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  
			function ObtenerP(nParametro) 
			{    
			var callingURL = document.location.href;    
			var cgiString = callingURL.substring(callingURL.indexOf('?')+1,callingURL.length); 
			nParametro -=1; 
			var col_array=cgiString.split("&"); 
			var part_num=0; 
				while (part_num < col_array.length) 
						{ 
								if ( part_num == nParametro ) 
								return col_array[part_num]; 
								else 
								part_num+=1; 
						} 
			return 0; 
			} 
			function Cerrar()
			{
			var NroPagina;
			var strEstado;
			document.frmArchivo.btnCerrar.disabled=true; 
			NroPagina= document.all["txtNroPagina"].value;
			strEstado= document.all["txtEstado"].value;
			if (NroPagina==2)
				{
				window.location ='AlmRetiroSolicitud_W.aspx';
				}
				else
				{
				window.location ='AlmRetiroSolicitud_W.aspx';
				}
			}
			function noboton(ev)
			{
			if (document.all) ev= event;
			if (ev.button & 2)
			alert("Bot�n inactivo.");
			}

			function bloquear()
			{
			document.onmousedown = noboton;		
			}
		</script>
	</HEAD>
	<body onload="bloquear()" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		bgColor="#f0f0f0">
		<form id="frmArchivo" name="frmArchivo" method="post">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<DIV id="cntFirmas" runat="server"></DIV>
						<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="80%">
									<asp:label id="lblFirma" runat="server" CssClass="error"></asp:label>
									<asp:literal id="LtrFirma" runat="server"></asp:literal></TD>
								<TD align="center" class="Ayuda" width="150">
									<asp:literal id="ltrExpediente" runat="server"></asp:literal>
								</TD>
								<TD align="center">
									<asp:literal id="ltrFirmar1" runat="server"></asp:literal></TD>
								<TD align="center">
									<asp:literal id="ltrFirmar2" runat="server"></asp:literal></TD>
								<TD align="center"><INPUT class="btn" id="btnCerrar" style="WIDTH: 80px" onclick="Cerrar()" type="button"
										value="Cerrar" name="btnCerrar">
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="5">
									<asp:label id="lblError" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<!--<TR>
								<TD align="center" colSpan="4">&nbsp;</TD>
							</TR>-->
						</TABLE>
						<DIV style="DISPLAY: none"><INPUT id="txtArchivo_pdf" name="txtArchivo_pdf" runat="server">
							<INPUT id="txtArchivo_pdf_firmado" name="txtArchivo_pdf_firmado" runat="server">
							<INPUT id="txtNumSerie" name="txtNumSerie" runat="server"> <INPUT id="txtNroPagina" name="txtNroPagina" runat="server">
							<INPUT id="txtEstado" name="txtEstado" runat="server"> <INPUT id="txtTimeStamp" name="txtTimeStamp" runat="server">
							<INPUT id="txtContrasena" name="txtContrasena" runat="server"> <INPUT id="txtNuevaContrasena" name="txtNuevaContrasena" runat="server"></DIV>
					</TD>
				</TR>
				<TR>
					<TD>
						<asp:literal id="ltrVisor" runat="server"></asp:literal></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
