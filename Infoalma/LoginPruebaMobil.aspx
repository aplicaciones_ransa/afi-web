<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HeaderLogin" Src="UserControls/HeaderLogin.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LoginPruebaMobil.aspx.vb" Inherits="LoginPruebaMobil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Alma Per� >> AFI >> Ingreso al Sistema</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="author" content="francisco_uni@hotmail.com">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="JavaScript">
		window.moveTo(0,0);
		if (document.all) {
		top.window.resizeTo(screen.availWidth,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
		if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
		top.window.outerHeight = screen.availHeight;
		top.window.outerWidth = screen.availWidth;
		}		
		}				
		document.oncontextmenu = function(){return false}
		function Login()
			{
			var browserName=navigator.appName; 
			//window.open('Popup/AvisoFiestas.htm','Medios','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=456px,height=375px');
			if (browserName!="Microsoft Internet Explorer")
			{ 
				if(document.layers)
				{
				document.layers["UserPassword"].visibility="hide";				
				}
				if(document.all)
				{
				//btnEntrar.style.visibility = "hidden";
				document.all["UserPassword"].style.visibility="hidden";				
				}			
				//alert("Su explorador no esta soportado por nuestro sistema!");
			}
			else
			{
				if(document.layers)
				{				
				document.layers["txtUsuario"].focus();				
				}
				if(document.all)
				{				
				document.all["txtUsuario"].focus();				
				}	
			}
			if (top.location != window.location)
				{
				 top.location = window.location;							
				}
			}
			function Abrir(url, Height, Width)
			{			
				window.open(url,"Lgn","top=250,left=350,toolbar=0,status=0,directories=0,menubar=0,scrollbars=0,resize=0,width=" + Width + ",height=" + Height);
			}
			function AbrirPoputRecordarClave() {
				window.open("Popup/poputRecordarClave.aspx","",'left=180,top=140,width=800,height=400,toolbar=0,scrollbars=0,resizable=0');
			}
		</script>
	</HEAD>
	<BODY onload="Login();" bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0"
		bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:headerlogin id="HeaderLogin1" runat="server"></uc1:headerlogin>
						<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%" background="Images/Login.jpg"
							height="400">
							<TR>
								<TD class="Titulo" height="90" vAlign="middle" align="center">Bienvenidos a nuestra 
									soluci�n en l�nea
								</TD>
							</TR>
							<TR>
								<TD align="center">&nbsp;</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center">
									<TABLE id="Table3" border="0" cellSpacing="4" cellPadding="0" width="100%">
										<TR>
											<TD align="center"><asp:label id="lblTitulo" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD class="Subtitulo1" align="center"><asp:label id="lblCuerpo" runat="server"></asp:label></TD>
										</TR>
										<TR>
											<TD align="center">
												<DIV id="UserPassword">
													<TABLE style="BORDER-BOTTOM: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-RIGHT: #cccccc 1px solid"
														id="Ingreso" border="0" cellSpacing="4" width="290" align="center">
														<TR>
															<TD class="Subtitulo2" colSpan="3" align="center">Ingreso al Sistema</TD>
														</TR>
														<TR>
															<TD height="10"></TD>
															<TD height="10" align="center"></TD>
															<TD height="10" align="right"></TD>
														</TR>
														<TR>
															<TD class="text">Usuario :
															</TD>
															<TD align="center"><asp:textbox id="txtUsuario" runat="server" CssClass="Text" Width="150px"></asp:textbox></TD>
															<TD><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtUsuario"
																	ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
														</TR>
														<TR>
															<TD width="25%"></TD>
															<TD width="50%" align="center"><asp:button id="btnEntrar" runat="server" CssClass="btn" Width="80px" Text="Aceptar"></asp:button></TD>
															<TD width="15%"></TD>
														</TR>
														</TABLE>
												</DIV>
											</TD>
										</TR>
										<TR>
											<TD align="center">
												&nbsp;</TD>
										</TR>
									</TABLE>
									<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD height="70"></TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</BODY>
</HTML>
