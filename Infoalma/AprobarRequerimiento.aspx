<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AprobarRequerimiento.aspx.vb" Inherits="AprobarRequerimiento" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Aprobar Requerimiento</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	


        $(function () {
            $("#txtFechaIngresoDesde").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaIngresoDesde").datepicker('show');
            });

            $("#txtFechaIngresoHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaIngresoHasta").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            Estado.style.display = 'none';
        }
        function chkSelect_OnMouseMove(tableRowId, checkBox) {
            if (checkBox.checked == false)
                tableRowId.style.backgroundColor = "#FFFFC0";
        }

        function chkSelect_OnMouseOut(tableRowId, checkBox, rowIndex) {
            if (checkBox.checked == false) {
                var bgColor;
                if (rowIndex % 2 == 0)
                    bgColor = "WhiteSmoke";
                else
                    bgColor = "Transparent";
                tableRowId.style.backgroundColor = bgColor;
            }
        }

        function chkSelect_OnClick(tableRowId, checkBox, rowIndex) {
            var bgColor;
            if (rowIndex % 2 == 0) {
                bgColor = 'WhiteSmoke';
            }
            else {
                bgColor = 'Transparent';
            }
            if (document.getElementById(checkBox).checked == true) {
                document.getElementById(tableRowId).style.backgroundColor = '#FFFFC0';
            }
            else {
                document.getElementById(tableRowId).style.backgroundColor = bgColor;
            }
        }
        function MsgEliminar(v) {
            if (confirm('El requerimiento temporal nro. ' + v + ' ser� eliminado. Presione OK para confimar \no Cancel para cancelar la operaci�n.') == false)
                return false;
        }

        function ValidaCheckBox() {
            var retorno = false;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        retorno = true;
                    }
                }
            }
            if (retorno == false) {
                alert("Seleccione por lo menos una solicitud para aprobar.");
            }
            return retorno;
        }
    </script>
</head>
<body bottommargin="0" bgcolor="#f0f0f0" leftmargin="0" topmargin="0" onload="Ocultar();"
    rightmargin="0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="125">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top" width="100%">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">&nbsp;APROBAR REQUERIMIENTO</td>
                            <td class="title-text" height="20"></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table6" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td class="text" style="width: 55px; height: 17px">�rea</td>
                                                    <td class="text" style="width: 6px; height: 17px" align="center">:</td>
                                                    <td style="width: 101px; height: 17px">
                                                        <asp:DropDownList ID="cboArea" runat="server" DataValueField="COD" DataTextField="DES" CssClass="Text"
                                                            Width="140px">
                                                            <asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 9px; height: 17px"></td>
                                                    <td class="text" style="width: 151px; height: 17px">Fecha Recepci�n&nbsp;&nbsp; :</td>
                                                    <td class="text" style="width: 140px; height: 17px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 55px; height: 17px">Tipo Env�o</td>
                                                    <td class="text" style="width: 6px; height: 17px" align="center">:</td>
                                                    <td style="width: 101px; height: 17px">
                                                        <asp:DropDownList ID="cboTipoEnvio" runat="server" DataValueField="COD" DataTextField="DES" CssClass="Text"
                                                            Width="140px">
                                                            <asp:ListItem Value="DOCUMENTO ORIGINAL">DOCUMENTO ORIGINAL</asp:ListItem>
                                                            <asp:ListItem Value="UNIDAD COMPLETA">UNIDAD COMPLETA</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" style="width: 9px; height: 17px"></td>
                                                    <td class="text" style="width: 151px; height: 17px">&nbsp;Desde&nbsp;
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaIngresoDesde" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaIngresoDesde" runat="server"><input class="Text" id="btnFechaInicial" type="button"
                                                                value="..." name="btnFecha"></td>
                                                    <td class="text" style="width: 160px; height: 17px">Hasta&nbsp;&nbsp;&nbsp;&nbsp;<input class="Text" onkeypress="validarcharfecha()" id="txtFechaIngresoHasta" onkeyup="this.value=formateafecha(this.value);"
                                                        maxlength="10" size="5" name="txtFechaIngresoHasta" runat="server"><input class="Text" id="btnFechaFinal" type="button"
                                                            value="..." name="btnFecha"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" style="width: 55px"></td>
                                                    <td class="text" style="width: 6px"></td>
                                                    <td class="text" style="width: 101px"></td>
                                                    <td style="width: 9px"></td>
                                                    <td style="width: 151px"></td>
                                                    <td style="width: 140px" align="right">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="td"></td>
                        </tr>
                        <tr>
                            <td class="TEXT" height="20">Resultados :
									<asp:Label ID="lblRegistros" runat="server" CssClass="text"></asp:Label></td>
                            <td class="title-text" height="20"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    PageSize="20" AllowSorting="True" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Sel">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemTemplate>
                                                <table id="Table12" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:CheckBox ID="chkSel" runat="server" CssClass="Text" Width="48px" Checked='<%# DataBinder.Eval(Container, "DataItem.FLG_SEL") %>' Height="13px"></asp:CheckBox></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="NU_REQU" HeaderText="Nro.Requ. Temp">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_TIPO_ENVI" HeaderText="Tipo Env&#237;o">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea">
                                            <HeaderStyle Width="30%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CO_USUA_CREA" HeaderText="Usuario">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_RECE" HeaderText="Fecha Reg.">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TOTA_CONS" HeaderText="Nro. Consultas">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Eliminar">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" ToolTip="Eliminar registro"
                                                    ImageUrl="Images/Eliminar.JPG" CausesValidation="False"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Ver">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemTemplate>
                                                <table id="Table15" cellspacing="0" cellpadding="0" width="30" border="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
                                                                CausesValidation="False"></asp:ImageButton></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td style="width: 512px"></td>
                                        <td style="width: 189px" align="center">
                                            <asp:Button ID="btnAprobar" runat="server" CssClass="btn" Width="150px" Text="Aprobar Requerimiento"></asp:Button></td>
                                        <td></td>
                                        <td width="500"></td>
                                    </tr>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                            <td class="td"></td>
                        </tr>
                        <tr>
                            <td class="td" id="Estado"></td>
                            <td class="td"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
