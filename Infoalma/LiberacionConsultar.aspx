<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LiberacionConsultar.aspx.vb" Inherits="LiberacionConsultar" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consultar Liberación</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript">	
			function ActivaBoton()
				{			
				frm=document.forms[0];				
				frm.btnEliminar.disabled=false;		
				}
			function AbrirVisor(sNU_TITU)
				{
				 var ventana = 'VisorDepsanet.aspx?sNU_TITU=' + sNU_TITU;
				window.open(ventana,'Retiro','left=240,top=200,width=800,height=600,toolbar=0,resizable=1');
				}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table3"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6"><uc1:menuinfo id="Menuinfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table4" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="text">Nro Warrant:</TD>
														<TD><asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
														<TD class="text">Nro Liberación:</TD>
														<TD><asp:textbox id="txtNroLiberacion" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
														<TD class="text">Estado:</TD>
														<TD><asp:dropdownlist id="cboEstado" runat="server" CssClass="Text" Width="120px"></asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
										<TR>
											<TD width="80"><asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD height="250" vAlign="top"><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										PageSize="15" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro.Warr">
												<HeaderStyle Width="40px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_TIPDOC" HeaderText="Tip.Doc">
												<HeaderStyle Width="40px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_LIBE" HeaderText="Nro Retiro">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FCH_CREA" HeaderText="Fch.Solicitud">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTIFINA" HeaderText="Endosatario">
												<HeaderStyle Width="200px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Depositante">
												<HeaderStyle Width="200px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ESTA" HeaderText="Estado">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FLG_CONT" HeaderText="Nuevo Warrant">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="IM_SALDO" HeaderText="Saldo">
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Embarque" HeaderText="Embar">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="30">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Ver.JPG" ToolTip="Ver detalles"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Selec">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id="chkSeleccionar" runat="server" Width="11px" CssClass="Text" Height="13px"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_CLIE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_ENTFINA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_UNID"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_VIRTUAL"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="TI_DOCU_RECE" HeaderText="TI_DOCU_RECE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_DOCU_RECE" HeaderText="NU_DOCU_RECE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_MODA_MOVI" HeaderText="CO_MODA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_MONE" HeaderText="CO_MONE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_ALMA" HeaderText="CO_ALMA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FE_TITU" HeaderText="FE_TITU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="TI_TITU" HeaderText="TI_TITU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="DE_MERC_GENE" HeaderText="DE_MERC_GENE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="ST_ENDO_EMBA" HeaderText="ST_ENDO_EMBA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="ST_ALMA_SUBI" HeaderText="ST_ALMA_SUBI"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="SEC_CONT"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_CIERRE" HeaderText="FLG_CIERRE"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FLG_VISTO" HeaderText="FLG_VISTO"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Visto">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:ImageButton id="ImgVisto" onclick="ImgVisto_Click" runat="server" CausesValidation="False" ImageUrl="Images/Activar2.gif"
														ToolTip="Visto bueno"></asp:ImageButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn HeaderText="Inven."></asp:BoundColumn>
										    <asp:BoundColumn DataField="COD_ALMA_DEST" Visible="False"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:button id="btnEliminar" runat="server" CssClass="btn" Width="80px" Text="Rechazar" ForeColor="White"
										BackColor="Red" Enabled="False" CausesValidation="False"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="630">
										<TR>
											<TD class="td" colSpan="8">Leyenda</TD>
										</TR>
										<TR>
											<TD bgColor="lightgoldenrodyellow" width="15%"></TD>
											<TD class="Leyenda" width="35%">Liberación de Warrant Electrónico.</TD>
											<TD width="15%"></TD>
											<TD class="Leyenda" width="35%">Liberación de Warrant Normal.</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
