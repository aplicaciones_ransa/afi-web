<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CajasExistentesxArea.aspx.vb" Inherits="CajasExistentesxArea" %>

<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Consolidado por �rea</title>
    <meta content="False" name="vs_showGrid">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFecha").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFecha").datepicker('show');
            });

        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            var TipoDocumento;
            TipoDocumento = document.all["txtTipoDocumento"].value;
            document.all["pnlAnular"].style.display = 'none';

            if (TipoDocumento == 'Warrant Electr�nico') {
                NroLib.style.display = 'none';
                txtLib.style.display = 'none';
            }
            else {
                NroLib.style.display = 'inline';
                txtLib.style.display = 'inline';
            }

            if (document.all["txtAnulacion"].value == '') {
                Anulacion.style.display = 'none';
            }
            else {
                Anulacion.style.display = 'inline';
            }
        }
        function CerrarPanel() {
            document.all["pnlAnular"].style.display = 'none';
        }
        function AbrirPanel() {
            document.all["pnlAnular"].style.display = 'inline';
        }
        function Visor() {
            window.location = 'DocumentoResumen.aspx?var=1'//,'null','Width=800px,Height=680px,top=5,left=50,scrollbars=yes');
            //window.location ='DocumentoReporte.aspx?var=1'
        }
        function Ocultar() {
            Estado.style.display = 'none';
        }
    </script>
</head>
<body bottommargin="0" bgcolor="#f0f0f0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="108">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">&nbsp;CAJAS EXISTENTES POR �REA</td>
                        </tr>
                        <tr>
                            <td class="td" style="height: 51px" valign="top" align="left">
                                <table id="Table16" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table2" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr class="Texto9pt">
                                                    <td class="text">�rea</td>
                                                    <td class="text" style="height: 18px">:</td>
                                                    <td class="text">
                                                        <asp:DropDownList ID="cboArea" runat="server" CssClass="Text" DataTextField="DES" DataValueField="COD"
                                                            Width="140px">
                                                            <asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" width="68">Fecha al</td>
                                                    <td class="text">:</td>
                                                    <td class="text" align="left">
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFecha" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFecha" runat="server"><input class="Text" id="btnFechaInicial" type="button"
                                                                value="..." name="btnFecha"></td>
                                                    <td class="text" style="height: 18px" width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" valign="top">Resultados :&nbsp;
									<asp:Label ID="lblRegistros" runat="server" CssClass="text"></asp:Label>
                                <p>
                                    <asp:DataGrid ID="dgdResultado" TabIndex="2" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                        AllowSorting="True" PageSize="20" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0"
                                        ShowFooter="True">
                                        <FooterStyle Font-Bold="True" HorizontalAlign="Right"></FooterStyle>
                                        <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                        <ItemStyle CssClass="gvRow"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea">
                                                <HeaderStyle Width="50%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="NU_CAJA_DEPS" HeaderText="Cajas Custodia en DEPSA" DataFormatString="{0:N0}">
                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="NU_CAJA_CLIE" HeaderText="Cajas Custodia en Clientes" DataFormatString="{0:N0}">
                                                <HeaderStyle Width="15%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Total">
                                                <HeaderStyle Width="13%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <%# FormatNumber(DataBinder.Eval(Container.DataItem, "NU_CAJA_CLIE") + DataBinder.Eval(Container.DataItem, "NU_CAJA_DEPS"),0) %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ver">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
                                                        CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid>
                                </p>
                                <p>&nbsp;</p>
                                <p>
                                    <asp:Label ID="Label7" runat="server" CssClass="text" ForeColor="#003366" Font-Bold="True">Total Unidades en Custodia</asp:Label></p>
                                <p>
                                    <asp:DataGrid ID="dgdResultadoTotal" runat="server" CssClass="gv" Width="240px" BorderColor="Gainsboro"
                                        PageSize="5" AutoGenerateColumns="False">
                                        <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                        <ItemStyle CssClass="gvRow"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="Depsa" HeaderText="Files">
                                                <HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Clientes" HeaderText="Clientes">
                                                <HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Total" HeaderText="Total">
                                                <ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" CssClass="gvPager"></PagerStyle>
                                    </asp:DataGrid>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table3" cellspacing="3" cellpadding="3" width="100%" border="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnVerReporte" runat="server" CssClass="btn" Width="100px" Text="Ver Reporte"></asp:Button><asp:Button ID="btnExportar" runat="server" CssClass="btn" Width="100px" Text="Exportar"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td" id="Estado"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
