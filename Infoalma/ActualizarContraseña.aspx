<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/HeaderLogin.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ActualizarContraseña.aspx.vb" Inherits="ActualizarContraseña" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Actualizar Contraseña</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script>	
		function Login()
			{										
				document.all["txtPassAnterior"].focus();
			}
		</script>
	</HEAD>
	<body onload="Login();" bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0"
		bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:Header id="Header2" runat="server"></uc1:Header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
							border="0" cellSpacing="0" cellPadding="0" width="100%" height="400">
							<TR>
								<TD class="Titulo" height="90" vAlign="bottom" align="center"><FONT size="5" face="Tahoma">
										<TABLE id="Table3" border="0" cellSpacing="5" cellPadding="0" width="80%">
											<TR>
												<TD class="subtitulo" align="center">Introduce tu contraseña actual y, a seguir, 
													elige una nueva. Haz click en&nbsp;Actualizar cuando hayas terminado, Tu 
													contraseña caduca&nbsp;dentro de 30 días a partir de la fecha de actualización.
												</TD>
											</TR>
										</TABLE>
									</FONT>
								</TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center">
									<TABLE style="BORDER-BOTTOM: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-RIGHT: #cccccc 1px solid"
										id="Table2" border="0" cellSpacing="2" width="430" align="center">
										<TR>
											<TD class="Subtitulo2" colSpan="3" align="center">Actualice su Contraseña</TD>
										</TR>
										<TR>
											<TD class="text">Usuario o Login:</TD>
											<TD class="text" height="10"><asp:label id="lblUsuario" runat="server"></asp:label></TD>
											<TD height="10"></TD>
										</TR>
										<TR>
											<TD class="text">contraseña&nbsp;Anterior:
											</TD>
											<TD align="center"><asp:textbox id="txtPassAnterior" runat="server" CssClass="Text" Width="180px" TextMode="Password"></asp:textbox></TD>
											<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtPassAnterior"
													ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="text">contraseña&nbsp;Nueva:</TD>
											<TD align="center"><asp:textbox id="txtPassNuevo" runat="server" CssClass="Text" Width="180px" TextMode="Password"></asp:textbox></TD>
											<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtPassNuevo"
													ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="text">Confirma tu contraseña:</TD>
											<TD align="center"><asp:textbox id="txtPassRepNuevo" runat="server" CssClass="Text" Width="180px" TextMode="Password"></asp:textbox></TD>
											<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtPassRepNuevo"
													ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator><asp:comparevalidator id="CompareValidator1" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtPassRepNuevo"
													ErrorMessage="CompareValidator" ForeColor=" " ControlToCompare="txtPassNuevo">No coinciden</asp:comparevalidator></TD>
										</TR>
										<TR>
											<TD class="Text" colSpan="3">
												<P align="center">Si olvidas la contraseña, te preguntaremos la respuesta secreta 
													para comprobar tu identidad.</P>
											</TD>
										</TR>
										<TR>
											<TD class="text">Pregunta de Seguridad :</TD>
											<TD align="center"><asp:dropdownlist id="cboPregSegu" runat="server" CssClass="Text" Width="220px"></asp:dropdownlist></TD>
											<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="cboPregSegu"
													ErrorMessage="RequiredFieldValidator" ForeColor=" ">Seleccione</asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="text">Respuesta Secreta :</TD>
											<TD align="center"><asp:textbox id="txtRespSecre" onkeyup="this.value = this.value.toUpperCase();" runat="server"
													CssClass="Text" Width="220px"></asp:textbox></TD>
											<TD><asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" CssClass="error" Display="Dynamic" ControlToValidate="txtRespSecre"
													ErrorMessage="RequiredFieldValidator" ForeColor=" ">Ingrese</asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD width="40%"></TD>
											<TD width="40%" align="center"><asp:button id="btnActualizar" runat="server" CssClass="btn" Text="Actualizar"></asp:button></TD>
											<TD width="20%"></TD>
										</TR>
										<TR>
											<TD colSpan="3" align="center"><asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
										</TR>
									</TABLE>
									<TABLE id="Table6" border="0" cellSpacing="5" cellPadding="0" width="100%">
										<TR>
											<TD class="Subtitulo1" align="center">Desea iniciar su sesión pulse&nbsp;
												<asp:hyperlink id="hlkSesion" runat="server" NavigateUrl="Login.aspx">Aquí</asp:hyperlink></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD height="50" vAlign="middle" align="center"></TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
