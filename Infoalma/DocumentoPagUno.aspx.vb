Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoPagUno
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
    Private objReportes As Reportes = New Reportes
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objLiberacion As Liberacion = New Liberacion
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strEndosoPath = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboEntidadFinanciera As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblAlerta As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrVisualizar As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblSICO As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents chkDobleEndoso As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents cboAsociado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents pnlAsociado As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblEndoso As System.Web.UI.WebControls.Label
    'Protected WithEvents pnlEndosatario As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblOriginal As System.Web.UI.WebControls.Label
    'Protected WithEvents pnlOriginal As System.Web.UI.WebControls.Panel
    'Protected WithEvents pnlDblEndoso As System.Web.UI.WebControls.Panel
    'Protected WithEvents txtAsociado As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents btnGrabarEndo As System.Web.UI.WebControls.Button
    'Protected WithEvents lblTipoDocumento As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaCreacion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroLiberacion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCliente As System.Web.UI.WebControls.Label
    'Protected WithEvents lblEstado As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMercaderia As System.Web.UI.WebControls.Label
    'Protected WithEvents lblSecuencia As System.Web.UI.WebControls.Label
    'Protected WithEvents txtTipoDocumento As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents btnRegistrar As RoderoLib.BotonEnviar
    'Protected WithEvents btnReporte As RoderoLib.BotonEnviar
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnRechazar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents dgWarrants As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents pnlCompromiso As System.Web.UI.WebControls.Panel
    'Protected WithEvents btnAnular As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Try
                If Page.IsPostBack = False Then
                    Me.lblOpcion.Text = "Datos del Documento "
                End If
                Me.lblError.Text = ""
                Me.lblAlerta.Text = ""
                Me.lblEndoso.Text = ""
                Me.lblMensaje.Text = ""
                Session.Remove("Mensaje")
                If Not IsPostBack Then
                    Inicializa()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim strResult As String()
        Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
        Me.btnGrabarEndo.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar, ya no podra modificar el endosatario?')== false) return false;")
        Me.btnRechazar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de rechazar el documento?')== false) return false;")
        Me.btnRegistrar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de registrar el documento?')== false) return false;")
        Me.btnReporte.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de imprimir el documento')== false) return false;")
        Me.btnAnular.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de anular el documento')== false) return false;")
        If Request.QueryString("var2") = 1 Then
            objDocumento.gUpdAccesoFirmar(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"))
        End If
        Select Case Session.Item("EstadoDoc")
            Case "01"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
            Case "02"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "8"), "-")
            Case "03"
                Me.lblOpcion.Text = "Consultar Documentos Endosados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "9"), "-")
            Case "04"
                Me.lblOpcion.Text = "Consultar Documentos Registrados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "10"), "-")
            Case "06"
                Me.lblOpcion.Text = "Consultar Documentos Rechazados"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "12"), "-")
        End Select
        Session.Item("Page") = strResult(0)
        Session.Item("Opcion") = strResult(1)
        Me.chkDobleEndoso.Enabled = False
        Me.pnlAsociado.Visible = False
        Me.cboAsociado.Enabled = False
        pnlEndosatario.Visible = True
        If Session("IdTipoDocumento") = "19" Then
            Me.pnlCompromiso.Visible = True
        Else
            Me.pnlCompromiso.Visible = False
        End If
        loadEntidad()
        loadDataDocumento()
        ValidarControles()
        BindDatagrid()
        BindDatagridWarants()
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As New DataTable
        Try
            Me.cboEntidadFinanciera.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "")
            Me.cboEntidadFinanciera.Items.Add(New ListItem("---------------------------Seleccione uno---------------------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboEntidadFinanciera.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "Error al llenar entidades: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub ValidarControles()
        Dim dtControles As New DataTable
        dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroDoc"),
                                                    Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), 0)
        Me.btnRegistrar.Visible = False
        Me.btnGrabarEndo.Visible = False
        Me.cboEntidadFinanciera.Enabled = False
        For i As Integer = 0 To dtControles.Rows.Count - 1
            Select Case dtControles.Rows(i)("COD_CTRL")
                Case "02"
                    Me.btnAnular.Visible = True
                Case "03"
                    Me.ltrVisualizar.Text = "<input id='btnVisualizar' style='WIDTH: 80px; CURSOR: hand' onclick='Visor();'	type='button' value='Visualizar' name='btnVisualizar' class='btn'>"
                Case "04"
                    Me.btnRechazar.Visible = True
                Case "08"
                    Me.btnRegistrar.Visible = True
                Case "10"
                    If Session.Item("IdSico") <> Me.txtAsociado.Value Then
                        Me.btnGrabar.Visible = True
                        Me.cboEntidadFinanciera.Enabled = True
                        Me.chkDobleEndoso.Enabled = True
                        Me.cboAsociado.Enabled = True
                    End If
                    'Case "11"
                    '    Me.btnReporte.Visible = True
                Case "12"
                    If Session.Item("IdSico") = Me.txtAsociado.Value Then
                        Me.btnGrabarEndo.Visible = True
                        Me.cboEntidadFinanciera.Enabled = True
                    End If
            End Select



        Next
        If Me.btnRechazar.Visible = True And objDocumento.gGetValidarCancelacion(Session.Item("NroDoc")) = False And Session.Item("IdTipoDocumento") = "01" Then
            Me.btnRechazar.Visible = False
        End If
        If Session.Item("IdTipoEntidad") = "03" And Me.cboEntidadFinanciera.SelectedValue = "0" And Session.Item("IdTipoEntidad") = "03" Then
            Me.lblAlerta.Text = "Seleccione"
        End If

        dtControles.Dispose()
        dtControles = Nothing

    End Sub

    Private Sub DobleEndoso()
        If chkDobleEndoso.Checked Then
            Me.pnlAsociado.Visible = True
            loadAsociados()
        Else
            Me.pnlAsociado.Visible = False
        End If
    End Sub

    Private Sub loadDataDocumento()
        Try
            Dim drUsuario As DataRow
            drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)

            If Session.Item("IdSico") = drUsuario("COD_ENTASOC") Then
                Me.pnlOriginal.Visible = True
                Me.pnlDblEndoso.Visible = False
                Me.lblOriginal.Text = CType(drUsuario("NOM_ENTI"), String).Trim
                Me.txtAsociado.Value = drUsuario("COD_ENTASOC")
            ElseIf Session.Item("IdTipoEntidad") = "02" Or Session.Item("IdTipoEntidad") = "01" Then
                Me.pnlOriginal.Visible = False
                If drUsuario("DBL_ENDO") Then
                    Me.pnlDblEndoso.Visible = True
                    Me.chkDobleEndoso.Checked = drUsuario("FLG_DBLENDO")
                Else
                    Me.pnlDblEndoso.Visible = False
                End If
            Else
                Me.pnlOriginal.Visible = False
                If drUsuario("DBL_ENDO") Then
                    Me.pnlDblEndoso.Visible = True
                    Me.chkDobleEndoso.Checked = drUsuario("FLG_DBLENDO")
                    DobleEndoso()
                    If drUsuario("COD_ENTASOC") <> "" Then
                        Me.cboAsociado.SelectedValue = drUsuario("COD_ENTASOC")
                        Me.lblEndoso.Text = ""
                    Else
                        Me.cboAsociado.SelectedIndex = 0
                        Me.lblEndoso.Text = "Seleccione"
                    End If
                Else
                    Me.pnlDblEndoso.Visible = False
                End If
            End If
            If drUsuario("FLG_CONCERT") = 0 Then
                Me.chkDobleEndoso.Enabled = False
            Else
                Me.chkDobleEndoso.Enabled = True
            End If
            Session.Add("NombrePDF", CType(drUsuario("NOM_PDF"), String).Trim)
            Me.lblSecuencia.Text = CType(drUsuario("DSC_SECU"), String).Trim
            Me.lblCliente.Text = CType(drUsuario("NOM_ENTI"), String).Trim
            Me.lblNroWarrant.Text = CType(drUsuario("NRO_DOCU"), String).Trim
            Me.lblTipoDocumento.Text = CType(drUsuario("NOM_TIPDOC"), String).Trim
            Me.txtTipoDocumento.Value = CType(drUsuario("NOM_TIPDOC"), String).Trim
            Me.lblEstado.Text = CType(drUsuario("NOM_ESTA"), String).Trim
            Me.lblFechaCreacion.Text = CType(drUsuario("FECHACREACION"), String).Trim
            Me.lblNroLiberacion.Text = CType(drUsuario("NRO_LIBE"), String).Trim
            Me.lblMercaderia.Text = CType(drUsuario("DSC_MERC"), String).Trim
            If drUsuario("Error") = "" And drUsuario("ENT_FINA") <> "" Then
                Me.cboEntidadFinanciera.SelectedValue = CType(drUsuario("ENT_FINA"), String).Trim
            Else
                Me.lblMensaje.Text = CType(drUsuario("Error"), String).Trim
            End If
            If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim)
            End If
            drUsuario = Nothing
        Catch ex As Exception
            Me.lblError.Text = "Error: " & ex.Message
        End Try
    End Sub

    Private Sub GrabaPDF(ByVal strNombArch As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Contenido, Session.Item("IdTipoDocumento"))
        fs.Close()
    End Sub

    Private Sub BindDatagrid()
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim dt As New DataTable
        Try
            dt = objDocumento.gGetLogDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
            dt.Dispose()
            objTrans.Commit()
            objTrans.Dispose()
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
            objConexion.Close()
        End Try
    End Sub

    Private Sub BindDatagridWarants()
        Dim dt As New DataTable
        Try
            dt = objDocumento.gGetWarrantEnlazados(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
            Me.dgWarrants.DataSource = dt
            Me.dgWarrants.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "GRABAR ASOCIADO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim strAsociado As String
        strAsociado = ""
        If Me.chkDobleEndoso.Checked Then
            If Me.cboAsociado.SelectedIndex <> 0 Then
                strAsociado = Me.cboAsociado.SelectedValue
            End If
        End If
        Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), strAsociado, Me.chkDobleEndoso.Checked)
        ValidarControles()
        loadDataDocumento()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim srFirmo As String
        Dim chkFirmo As New CheckBox
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            srFirmo = e.Item.DataItem("FLG_EVEN")
            chkFirmo = CType(e.Item.FindControl("chkFirmo"), CheckBox)
            If srFirmo = "F" Then
                chkFirmo.Checked = True
            Else
                chkFirmo.Checked = False
            End If
        End If
    End Sub

    Private Sub btnRegistrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "REGISTRAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim drEndoso As DataRow
        Dim drSICO As DataRow
        Dim NombreReporte As String
        Dim strIds As String
        Dim dtProrroga As DataTable
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try


            If Session.Item("IdTipoDocumento") = "05" Then
                dtProrroga = objDocumento.gGetDataProrroga(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("Cod_TipOtro"))
                If dtProrroga.Rows.Count = 0 Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("No se encontr� la Prorroga")
                    Exit Sub
                Else
                    NombreReporte = strEndosoPath & Session.Item("NombrePDF")
                    drSICO = objEntidad.gUpdProrrogaSICO(strEmpresa, dtProrroga.Rows(0)("COD_TIPDOC"), dtProrroga.Rows(0)("NRO_DOCU"),
                    dtProrroga.Rows(0)("MON_ADEC"), dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("SAL_ACTU"),
                    dtProrroga.Rows(0)("FCH_OBLIGACION"), Session.Item("UsuarioLogin"))
                    If drSICO.IsNull("UL_NUME_FOLI") Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        pr_IMPR_MENS("No se pudo actualizar en SICO")
                        Exit Sub
                    End If
                    objDocumento.gUpdFolioProrroga(drSICO("UL_NUME_FOLI"), drSICO("FE_NUME_FOLI"), drSICO("NU_SECU"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
                    objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"),
                    Session.Item("IdTipoDocumento"), "Se ha registrado la Prorroga<br><br>", "", "S", "N", objTrans)
                    objTrans.Commit()
                    objTrans.Dispose()
                End If
                '   Warrants de Custodia
            ElseIf Session.Item("IdTipoDocumento") = "20" Then
                objDocumento.gUpdRegistrar_Custodia_SICO(Session.Item("NroDoc"), Session.Item("IdTipoDocumento"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), objTrans)
                objReportes.GeneraPDFWarrantFirmas(Session.Item("Cod_Doc"), Session.Item("Cod_Doc"), strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), objTrans)

                objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"),
                Session.Item("IdTipoDocumento"), "Se ha registrado Warrants Custodia <br><br>", "", "S", "N", objTrans)

                objTrans.Commit()
                objTrans.Dispose()
                pr_IMPR_MENS("Se registr� correctamente")
            ElseIf Session.Item("IdTipoDocumento") = "03" Or Session.Item("IdTipoDocumento") = "10" Then
                objDocumento.gRegistraEndosoSICO(Session.Item("Cod_Doc"), Session.Item("IdUsuario"), objTrans)
                If objDocumento.strResultado = "0" Then
                    objDocumento.gRegistraDobleEndosoSICO(Session.Item("Cod_Doc"), objTrans)
                    If objDocumento.strResultado <> "0" Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Exit Try
                    End If
                    dtProrroga = objDocumento.GetIdsDocumentos(Session.Item("Cod_Doc"), objTrans)
                    For i As Integer = 0 To dtProrroga.Rows.Count - 1
                        If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                            objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"), objTrans)
                        Else
                            strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                        End If
                        'strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    Next
                    If strIds = "" Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        pr_IMPR_MENS("No se encontr� el warrant relacionado a este documento")
                        Exit Sub
                    End If
                    objReportes.GeneraPDFWarrantCopia(Session.Item("Cod_Doc"), strIds.Substring(0, strIds.Length - 1),
                    strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), objTrans)

                    objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
                    "Se ha registrado el Documento<br><br>", strEndosoPath, "S", "N", objTrans)

                    objTrans.Commit()
                    objTrans.Dispose()
                    '-------------------------------------------------------------------------------------
                    'If objDocumento.gGetEstadoImpresionTrans(Session.Item("Cod_Doc"), objTrans) = True Then
                    '    objTrans.Commit()
                    '    objTrans.Dispose()
                    '    Exit Try
                    'End If
                    'dtProrroga = objDocumento.GetIdsDocumentos(Session.Item("Cod_Doc"), objTrans)
                    'strIds = ""
                    'For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    '    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                    '        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, Session.Item("IdTipoDocumento"), objTrans)
                    '    Else
                    '        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    '        objReportes.GeneraPDFWarrantFirmasXDocumento(Session.Item("Cod_Doc"), dtProrroga.Rows(i)("COD_DOC"), strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), dtProrroga.Rows(i)("NOM_PDF"), objTrans)
                    '    End If
                    'Next

                    'If strIds = "" Then
                    '    objTrans.Rollback()
                    '    objTrans.Dispose()
                    '    pr_IMPR_MENS("No se encontr� el warrant relacionado a este documento")
                    '    Exit Sub
                    'End If
                    'objReportes.GeneraPDFWarrantFirmas(Session.Item("Cod_Doc"), strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), objTrans)
                    'strError = "X"
                    '-------------------------------------------------------------------
                    'Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                    'objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                    'objWSFileMaster.CopiaDocumentosInfodepsaRegistrados(Session.Item("Cod_Doc"))
                    'objTrans.Commit()
                    'objTrans.Dispose()
                    'objDocumento.gRegistraEndosoSICO(Session.Item("Cod_Doc"), Session.Item("IdUsuario"), objTrans)
                    pr_IMPR_MENS("Se registr� correctamente")
                ElseIf objDocumento.strResultado = "3" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("El documento ya fue registrado")
                    Exit Sub
                ElseIf objDocumento.strResultado = "4" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("Debe de realizar el doble endoso en el SICO antes de registrarlo")
                    Exit Sub
                Else
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("Fallo al registrar")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = ex.Message
        Finally
            ValidarControles()
            loadDataDocumento()
        End Try
    End Sub

    Private Sub chkDobleEndoso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDobleEndoso.CheckedChanged
        Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), "", Me.chkDobleEndoso.Checked)
        DobleEndoso()
    End Sub

    Private Sub loadAsociados()
        Dim dtAsociados As DataTable = New DataTable
        Try
            Me.cboAsociado.Items.Clear()
            dtAsociados = objEntidad.gGetAsociados(Session.Item("IdSico"))
            Me.cboAsociado.Items.Add(New ListItem("-------------------------Seleccione una entidad-------------------------", "0"))
            For Each dr As DataRow In dtAsociados.Rows
                Me.cboAsociado.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        Finally
            dtAsociados.Dispose()
            dtAsociados = Nothing
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
    End Sub

    Private Sub btnReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReporte.Click
        Dim dtProrroga As DataTable
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        Dim NombreReporte As String
        Dim strIds As String
        Dim drSICO As DataRow
        Dim dtDetalle As DataTable
        Dim strBody As String
        Dim strEmails As String
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            If Session.Item("IdTipoDocumento") = "02" Or Session.Item("IdTipoDocumento") = "13" Or Session.Item("IdTipoDocumento") = "14" Then
                If objDocumento.gGetEstadoImpresion(Session.Item("Cod_Doc")) = True Then
                    objTrans.Commit()
                    objTrans.Dispose()
                    Response.Redirect("DocumentoReporte.aspx", False)
                    Exit Sub
                End If
                NombreReporte = strEndosoPath & Session.Item("NombrePDF")
                dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, Session.Item("Cod_Unidad"), Session.Item("NroLib"), Session.Item("Cod_TipOtro"), Session.Item("NroDoc"), objTrans)
                objLiberacion.GetReporteLiberacionFirmas(NombreReporte, strEmpresa, Session.Item("Cod_Unidad"), "DOR", Me.lblNroLiberacion.Text, dtDetalle.Rows(0)("COD_CLIE"), dtDetalle, Session.Item("Cod_TipOtro"), dtDetalle.Rows(0)("EMB_LIBE"), dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"), Session.Item("IdTipoDocumento"), objTrans, strPathFirmas) 'Server.MapPath(strPathFirmas)
                GrabaDocumento(NombreReporte, objTrans)
                'objDocumento.gInsLogDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), "I", Session.Item("IdTipoDocumento"), objTrans)
                dtDetalle.Dispose()
                objTrans.Commit()
                objTrans.Dispose()
                Response.Redirect("DocumentoReporte.aspx", False)
            ElseIf Session.Item("IdTipoDocumento") = "05" Then
                If objDocumento.gGetEstadoImpresion(Session.Item("Cod_Doc")) = True Then
                    objTrans.Commit()
                    objTrans.Dispose()
                    Response.Redirect("DocumentoReporte.aspx", False)
                    Exit Sub
                End If
                NombreReporte = strEndosoPath & Session.Item("NombrePDF")
                objFunciones.GetReporteProrrogaFolio(NombreReporte, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strPathFirmas, Me.lblCliente.Text, Me.cboEntidadFinanciera.SelectedItem.Text, objTrans)  'Server.MapPath(strPathFirmas)
                GrabaDocumento(NombreReporte, objTrans)
                objTrans.Commit()
                objTrans.Dispose()
                Response.Redirect("DocumentoReporte.aspx", False)
            ElseIf Session.Item("IdTipoDocumento") = "03" Or Session.Item("IdTipoDocumento") = "10" Then
                dtProrroga = objDocumento.GetIdsDocumentos(Session.Item("Cod_Doc"), objTrans)
                For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"), objTrans)
                    Else
                        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    End If
                    'strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                Next
                If strIds = "" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    pr_IMPR_MENS("No se encontr� el warrant relacionado a este documento")
                    Exit Sub
                End If
                objReportes.GeneraPDFWarrantFirmas(Session.Item("Cod_Doc"), strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), objTrans)
                objTrans.Commit()
                objTrans.Dispose()
                Response.Redirect("DocumentoReporte.aspx", False)
            ElseIf Session.Item("IdTipoDocumento") = "09" Or Session.Item("IdTipoDocumento") = "17" Or Session.Item("IdTipoDocumento") = "18" Then
                objReportes.GeneraDCR(Session.Item("Cod_Doc"), strPDFPath, strPathFirmas, True, Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"), objTrans)
                objTrans.Commit()
                objTrans.Dispose()
                Response.Redirect("DocumentoReporte.aspx", False)
            End If
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Se cancel� el proceso debido al siguiente error:  " & ex.ToString
        End Try
    End Sub

    Private Sub GrabaDocumento(ByVal strNombArch As String, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strNombArch)
        FilePath = strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegEndosoFolio(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strNombre, Contenido, ObjTrans)
        fs.Close()
    End Sub

    Private Sub btnGrabarEndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarEndo.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "ASIGNAR FINANCIADOR", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        If Session.Item("IdSico") = Me.txtAsociado.Value Then
            Me.lblError.Text = objDocumento.gUpdEntidadFinanciera(Me.cboEntidadFinanciera.SelectedValue, Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), "X", Me.chkDobleEndoso.Checked)
        End If
        ValidarControles()
        loadDataDocumento()
    End Sub

    Private Overloads Sub btnEndoso_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session.Add("IdEntidadFinanciera", Me.cboEntidadFinanciera.SelectedValue)
        Response.Redirect("DocumentoPagDos.aspx?Pag=1")
    End Sub

    Private Sub btnRechazar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "RECHAZAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim strResultado As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            strResultado = objDocumento.gUpdRechazarDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            If strResultado = "X" Then
                Me.lblError.Text = "No puede rechazar el documento, est� relacionado"
                objTrans.Rollback()
                objTrans.Dispose()
                Exit Sub
            End If

            If Session.Item("IdTipoDocumento") = "24" Then
                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                System.Configuration.ConfigurationManager.AppSettings.Item("EmailInspectoria"),
                "Solicitud de Warrant fue rechazado - " & Me.lblCliente.Text,
                "Se rechaz� la solicitud de warrants Nro: <STRONG><FONT color='#330099'> " & Me.lblNroWarrant.Text & "<br> </FONT></STRONG>" &
                "Cliente: <STRONG><FONT color='#330099'> " & Me.lblCliente.Text & "<br> </FONT></STRONG>" &
                "Rechazado por: <STRONG><FONT color='#330099'> " & UCase(Session.Item("UsuarioLogin")) & "</FONT></STRONG><br>" &
                "<br><br>Atte.<br><br>S�rvanse ingresar a la web AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
            Else
                objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
                "El usuario " & Session.Item("UsuarioLogin") & " ha rechazado el documento", "", "S", "S", objTrans)
            End If
            objTrans.Commit()
            objTrans.Dispose()
            Session.Add("strMensaje", "Se rechaz� el documento correctamente")
            Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Error: " & ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        If Not (objLiberacion Is Nothing) Then
            objLiberacion = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "ANULAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            objDocumento.gUpdAnularDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
            "El usuario " & Session.Item("UsuarioLogin") & " anul� el documento", "", "S", "S", objTrans)
            objTrans.Commit()
            objTrans.Dispose()
            'Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
            Session.Add("strMensaje", "Se anul� el documento correctamente")
            Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Error: " & ex.Message
        End Try
    End Sub
End Class
