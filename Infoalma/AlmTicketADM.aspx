<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmTicketADM.aspx.vb" Inherits="AlmTicketADM" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>AlmTicketADM</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime()  - miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);
            if (fchIni == "" && fchFin == "") {
                alert("Debe Ingresar fecha Inicial y Final");
                return false;
            }

            if (fchIni > fchFin && fchIni != "" && fchFin != "") {
                alert("La fecha inicial no puede ser mayor a la fecha final");
                return false;
            }
            return true;
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table id="Table1" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        height="400" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top">
                                <table id="Table3" cellspacing="6" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="3">
                                            <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" cellspacing="0" cellpadding="0" width="630" align="center" border="0">
                                                <tr>
                                                    <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                                    <td background="Images/table_r1_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r2_c1.gif"></td>
                                                    <td>
                                                        <table id="Table6" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Text" width="20%">Fecha Ingreso/Retiro</td>
                                                                <td width="80%" colspan="4">
                                                                    <table id="Table5" cellspacing="0" cellpadding="0" width="250" border="0">
                                                                        <tr>
                                                                            <td class="Text">Del :</td>
                                                                            <td>
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaInicial" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaInicial" runat="server"><input class="text" id="btnFechaInicial" type="button"
                                                                                        value="..." name="btnFecha"></td>
                                                                            <td class="Text">Al&nbsp;:</td>
                                                                            <td class="Text">
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaFinal" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaFinal" runat="server"><input class="text" id="btnFechaFinal"  type="button"
                                                                                        value="..." name="btnFecha"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="6" background="Images/table_r2_c3.gif"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                                    <td background="Images/table_r3_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" cellspacing="4" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <div style="width: 585px; height: 322px; overflow: auto">
                                                <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                                    PageSize="20">
                                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                    <PagerStyle CssClass="gvPager"></PagerStyle>
                                                </asp:DataGrid>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
