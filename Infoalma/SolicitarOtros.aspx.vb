Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class SolicitarOtros
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objUnidad As LibCapaNegocio.clsCNUnidad
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private bolApto As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents txtFechaIngresoDesde As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaIngresoHasta As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboTipoItem As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblNota As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnIngresarDoc As System.Web.UI.WebControls.Button
    'Protected WithEvents btnVerSolicitud As System.Web.UI.WebControls.Button
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroCaja As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG7") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargarFechas()
                    CargarCombos()
                    InicializaBusqueda()
                    btnIngresarDoc.Attributes.Add("onclick", "return ValidaCheckBox();")
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Public Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        If ValidarCriteriosBusqueda() = True Then
            LlenaGrid()
            RegistrarCadenaBusqueda()
        End If
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "P", "DEPSAFIL", "SOLICITAR OTROS", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusqueda", "?ts=O&?ca=" & cboArea.SelectedIndex & "&?nc=" & txtNroCaja.Text & "&?fi=" & txtFechaIngresoDesde.Value &
            "&?ff=" & txtFechaIngresoHasta.Value & "&?ti=" & cboTipoItem.SelectedIndex & "&")
    End Sub

    Private Sub dgdResultado_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        ' Create a DataView from the DataTable.
        Dim dv As New DataView(dt)
        ' Sort property with the name of the field to sort by.
        dv.Sort = e.SortExpression
        ' by the field specified in the SortExpression property.
        Me.dgdResultado.DataSource = dv
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim itemType As ListItemType = CType(e.Item.ItemType, ListItemType)
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString
        If itemType <> ListItemType.Footer And itemType <> ListItemType.Separator Then
            If itemType = ListItemType.Header Then
                Dim chkSelAll As CheckBox = CType(e.Item.FindControl("chkSelectAll"), CheckBox)
                chkSelAll.Attributes.Add("onClick", "Javascript:chkSelectAll_OnClick(" + chkSelAll.ClientID + ")")
            Else
                Dim idFila As String = dgdResultado.ClientID & "_row" & e.Item.ItemIndex.ToString
                e.Item.Attributes.Add("id", idFila)
                Dim chkSel As CheckBox = CType(e.Item.FindControl("chkSel"), CheckBox)
                Dim idCheckBox As String = CType(chkSel, CheckBox).ClientID
                'e.Item.Attributes.Add("onMouseMove", "Javascript:chkSelect_OnMouseMove(" & idFila & "," & idCheckBox & ")")
                'e.Item.Attributes.Add("onMouseOut", "Javascript:chkSelect_OnMouseOut(" & idFila & "," & idCheckBox & "," & e.Item.ItemIndex & ")")
                chkSel.Attributes.Add("onClick", "Javascript:chkSelect_OnClick('" & idFila & "','" & idCheckBox & "'," & e.Item.ItemIndex & ")")
                If Convert.ToString(e.Item.DataItem("FLG_SEL")) = "1" Then
                    chkSel.Enabled = False
                End If
                If InStr(strSeleccionados, "SCAJ" & Convert.ToString(e.Item.DataItem("ID_UNID"))) <> 0 Or Convert.ToString(e.Item.DataItem("ST_UBIC")) = "TEM" Then
                    chkSel.Checked = True
                    chkSel.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        Me.txtFechaIngresoDesde.Value = objFuncion.gstrFechaMes(0)
        Me.txtFechaIngresoHasta.Value = objFuncion.gstrFechaMes(1)
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))
        objComun.gCargarComboTipoItem(cboTipoItem)
    End Sub

    'Private Sub LlenaGrid()
    '    Try
    '        objUnidad = New LibCapaNegocio.clsCNUnidad
    '        objFuncion = New LibCapaNegocio.clsFunciones
    '        objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtMostrarUnidades(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")), _
    '                cboArea.SelectedItem.Value, Trim(txtNroCaja.Text), _
    '                Trim(txtFechaIngresoDesde.Value), Trim(txtFechaIngresoHasta.Value), TipoBusqueda(txtNroCaja.Text), cboTipoItem.SelectedItem.Value))
    '        lblRegistros.Text = objUnidad.intFilasAfectadas.ToString & " registros"
    '        If objUnidad.intFilasAfectadas > 0 Then
    '            lblNota.Visible = True
    '        Else
    '            lblNota.Visible = False
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub

    Private Sub LlenaGrid()
        Try
            objUnidad = New LibCapaNegocio.clsCNUnidad
            objFuncion = New LibCapaNegocio.clsFunciones
            objFuncion.gCargaGrid(dgdResultado, objUnidad.gdtMostrarUnidades(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, Trim(txtNroCaja.Text),
                    Trim(txtFechaIngresoDesde.Value), Trim(txtFechaIngresoHasta.Value), TipoBusqueda(txtNroCaja.Text), cboTipoItem.SelectedItem.Value, Session.Item("UsuarioLogin"), 1, 0))
            lblRegistros.Text = objUnidad.intFilasAfectadas.ToString & " registros"
            If objUnidad.intFilasAfectadas > 0 Then
                lblNota.Visible = True
            Else
                lblNota.Visible = False
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub


    Private Sub btnIngresarDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIngresarDoc.Click
        If ValidarFormulario() = True Then
            IngresarCaja()
        End If
    End Sub

    Private Function TipoBusqueda(ByVal pstrCadena As String) As String
        If pstrCadena.IndexOf(",") <> 0 Then
            Return "S"
        Else
            Return "N"
        End If
    End Function

    Private Function QuitaBlanco(ByVal pstrCadena As String) As String
        Dim strCadena As String
        strCadena = Trim(pstrCadena)
        Return strCadena
    End Function

    Private Sub btnVerSolicitud_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerSolicitud.Click
        VerSolicitud()
    End Sub

    Private Sub VerSolicitud()
        If CStr(Session.Item("RequerimientoActivo")) = "0" Then
            lblError.Text = "No se ha ingresado documentos para ver la solicitud. Para visualizar la solicitud seleccione e ingrese los documentos."
        Else
            If Session.Item("ProcesoOtros").ToString = "PENDIENTE" Then
                Session.Add("PaginaAnterior", "SolicitarOtros.aspx")
                Response.Redirect("ConsultasGeneradasUnidades.aspx")
            End If
        End If
    End Sub

    Private Sub IngresarCaja()
        If CStr(Session.Item("ProcesoDocumento")) = "PENDIENTE" Or CStr(Session.Item("ProcesoCaja")) = "PENDIENTE" Then
            Session.Add("RequerimientoActivo", "0")
            Session.Add("ProcesoDocumento", "LISTO")
            Session.Add("ProcesoCaja", "LISTO")
        End If
        If CStr(Session.Item("RequerimientoActivo")) = "0" Then
            IngresarRequerimiento()
            Session.Add("ProcesoOtros", "PENDIENTE")
        End If
        If CStr(Session.Item("RequerimientoActivo")) <> "0" Then
            IngresarConsultas()
        End If
        If bolApto = True Then
            Session.Add("PaginaAnterior", "SolicitarOtros.aspx")
            Response.Redirect("ConsultasGeneradasUnidades.aspx")
        End If
    End Sub

    Private Sub IngresarRequerimiento()
        objRequ = New LibCapaNegocio.clsCNRequerimiento
        Session.Add("CodArea", cboArea.SelectedValue)
        If objRequ.gbolInsRequerimientoTemp(CStr(Session.Item("CodCliente")), "",
                    CStr(Session.Item("CodClieAdic")), cboTipoItem.SelectedItem.Value, CStr(Session.Item("CodPersAuto")),
                    CStr(Session.Item("DesPersAuto")), cboArea.SelectedValue, "dir envio", "N",
                   "Tipo Item: Otros", 0, CStr(Session.Item("UsuarioLogin"))) = True Then
            lblError.Text = "El registro se grab� con �xito nroRequTemp: " & objRequ.intNumRequTemp
            Session.Add("RequerimientoActivo", objRequ.intNumRequTemp)
            Session.Add("Seleccionados", "-")
        Else
            lblError.Text = objRequ.strMensajeError
        End If
    End Sub

    Private Sub IngresarConsultas()
        Dim dgItem As DataGridItem
        Dim intNumRequTemp As Integer = CInt(Session.Item("RequerimientoActivo"))
        Dim intNumConsultas As Integer = CInt(Session.Item("NroConsultas"))
        Dim strSeleccionados As String = Session.Item("Seleccionados").ToString

        Try
            For intI As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(intI)
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                    If InStr(strSeleccionados, "SCAJ" & dgItem.Cells(1).Text) = 0 And dgItem.Cells(8).Text = "0" Then
                        strSeleccionados &= "SCAJ" & dgItem.Cells(1).Text & ","
                        objConsulta = New LibCapaNegocio.clsCNConsulta
                        objConsulta.intNumRequTemp = intNumRequTemp
                        If objConsulta.gbolInsConsultaTemp(CStr(Session.Item("CodCliente")), cboTipoItem.SelectedItem.Value,
                        "Descripcion del envio", dgItem.Cells(1).Text, "loc", cboTipoItem.SelectedItem.Value, cboTipoItem.SelectedItem.Value,
                        dgItem.Cells(3).Text, dgItem.Cells(7).Text, "", dgItem.Cells(5).Text, "", 0,
                         CStr(Session.Item("UsuarioLogin")), "CJA") = True Then
                            lblError.Text = lblError.Text & " IDCons : " & objConsulta.intNumConsulta
                            intNumConsultas = intNumConsultas + 1
                        Else
                            lblError.Text = objRequ.strMensajeError
                            bolApto = False
                            Exit For
                        End If
                    End If
                End If
            Next
            Session.Add("NroConsultas", intNumConsultas)
            Session.Add("Seleccionados", strSeleccionados)
            bolApto = True
        Catch ex As Exception
            bolApto = False
        End Try
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Function ValidarFormulario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            If dgdResultado.Items.Count = 0 Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado registro(s). Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            ElseIf dgdResultado.Items.Count > 0 Then
                Dim dgItem As DataGridItem
                For Each dgItem In dgdResultado.Items
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                        bolRetorno = True
                        Exit For
                    End If
                Next
                If bolRetorno = False Then
                    lblError.Text = "No se ha seleccionado registro(s). Seleccione por lo menos un registro para continuar."
                End If
            End If
            If cboArea.SelectedValue = "" Or cboArea.SelectedValue = Nothing Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado un �rea. Seleccione un �rea para poder identificar la solicitud y poder continuar el siguiente paso."
            End If
            Return bolRetorno
        Catch ex As Exception
            lblError.Text = ex.Message
            Return False
        End Try
    End Function

    Public Function EncontrarVariable(ByVal pstrCadenaBusqueda As String, ByVal pstrVar As String) As String
        Dim intPosicion As Integer = InStr(pstrCadenaBusqueda, pstrVar)
        Dim strValor As String = ""
        If intPosicion = 0 And pstrCadenaBusqueda = "" Then
            Return ""
        Else
            intPosicion += pstrVar.Length
            strValor = pstrCadenaBusqueda.Substring(intPosicion, pstrCadenaBusqueda.IndexOf("&", intPosicion) - intPosicion)
            Return strValor
        End If
    End Function

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        If Session.Item("CadenaBusqueda") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusqueda").ToString
            If EncontrarVariable(strCadenaBusqueda, "?ts") = "O" Then
                SeleccionaItemCombo(cboArea, CInt(EncontrarVariable(strCadenaBusqueda, "?ca")))
                SeleccionaItemCombo(cboTipoItem, CInt(EncontrarVariable(strCadenaBusqueda, "?ti")))
                txtNroCaja.Text = EncontrarVariable(strCadenaBusqueda, "?nc")
                txtFechaIngresoDesde.Value = EncontrarVariable(strCadenaBusqueda, "?fi")
                txtFechaIngresoHasta.Value = EncontrarVariable(strCadenaBusqueda, "?ff")
            End If
        End If
    End Sub

    Public Sub SeleccionaItemCombo(ByVal objcombo As DropDownList, ByVal intIndex As Integer)
        Dim cboItem As ListItem
        Dim intI As Integer = 0
        For intI = 0 To objcombo.Items.Count - 1
            If (intI = intIndex) Then
                objcombo.Items(intI).Selected = True
            Else
                objcombo.Items(intI).Selected = False
            End If
        Next
    End Sub

    Private Function ValidarCriteriosBusqueda() As Boolean
        Dim blnRetorno As Boolean = False
        If cboArea.SelectedItem.Value = "-" Or cboArea.SelectedItem.Value = "" Then
            lblError.Text = "No se ha seleccionado �rea. Seleccione un �rea para continuar."
            blnRetorno = False
        Else
            blnRetorno = True
        End If
        Return blnRetorno
    End Function

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objUnidad Is Nothing) Then
            objUnidad = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
