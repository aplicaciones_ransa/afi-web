Imports System.Data

Public Class PersonasAutorizadas
    Inherits System.Web.UI.Page


    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objReporte As LibCapaNegocio.clsCNReporte

    Private dvTemporal As DataView
    Private GridExporta As New DataGrid
    '    Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnVerReporte As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    Private IntPase As New Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And ValidarPagina("PAG18") Then
            Try
                'If ValidarPagina("PAG18") = False Then Exit Sub
                Me.lblError.Text = ""
                Me.lblOpcion.Text = "Personas Autorizadas"
                If Not IsPostBack Then
                    CargarCombos()
                    LlenaGrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")
    End Sub

    Private Sub LlenaGrid()
        Try
            objReporte = New LibCapaNegocio.clsCNReporte
            objFuncion = New LibCapaNegocio.clsFunciones
            dvTemporal = New DataView(objReporte.gdrRepPersonasAutorizadas(CStr(Session.Item("CodCliente")), "", cboArea.SelectedItem.Value))
            Session.Add("dvReporte", dvTemporal)

            If IntPase = 1 Then
                objFuncion.gCargaGrid(GridExporta, dvTemporal.Table)
                IntPase = 0
            Else
                objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
            End If

            lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
    End Sub
    Public Sub Change_Page(ByVal Src As System.Object, ByVal Args As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub
    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        objFuncion = New LibCapaNegocio.clsFunciones
        Me.EnableViewState = False
        IntPase = 1
        Dim dgGrid As DataGrid = GridExporta
        LlenaGrid()
        objFuncion.ExportarAExcel("PersonasAutorizadas.xls", Response, dgGrid)
        GridExporta.Dispose()
        GridExporta = Nothing
        Me.EnableViewState = True
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            If Session.Item("PaginasPermitidas") = Nothing Then
                Return False
                Exit Function
            End If
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
        If Me.dgdResultado.Items.Count > 0 Then
            Server.Transfer("Reportes/RepPersonasAutorizadas.aspx")

        Else
            lblError.Text = "Falta informacion para imprimir"
        End If
    End Sub
    ReadOnly Property pDE_AREA() As String
        Get
            Return IIf(Me.cboArea.SelectedItem.Value = "", "TODOS", Me.cboArea.SelectedItem.Text)
        End Get
    End Property
End Class
