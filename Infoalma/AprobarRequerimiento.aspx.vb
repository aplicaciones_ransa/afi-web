Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AprobarRequerimiento
    Inherits System.Web.UI.Page
    Private lstrNumRequ As String
    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objRequ As LibCapaNegocio.clsCNRequerimiento
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private objAccesoWeb As clsCNAccesosWeb
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private bolAprobador As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object
    'Protected WithEvents txtFechaIngresoDesde As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaIngresoHasta As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoEnvio As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnAprobar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And ValidarPagina("PAG11") Then
            Try
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargarFechas()
                    CargarCombos()
                    btnAprobar.Attributes.Add("onclick", "return ValidaCheckBox();")
                    VerificaSiEsAprobador()
                    LlenaGrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")))
        objComun.gCargarComboTipoEnvio(cboTipoEnvio, "*")
    End Sub

    Private Sub VerificaSiEsAprobador()
        bolAprobador = EsAprobador()
        Session.Add("EsAprobador", bolAprobador)
        If bolAprobador = False Then
            btnAprobar.Enabled = False
            cboArea.AutoPostBack = True
        End If
    End Sub

    Public Function EsAprobador() As Boolean
        Dim strCodArea As String = ""
        Dim bolRetorno As Boolean = False
        objConsulta = New LibCapaNegocio.clsCNConsulta
        If objConsulta.gstrUsuarioAprobador(CStr(Session.Item("CodCliente")), "", cboArea.SelectedItem.Value, CStr(Session.Item("UsuarioLogin"))) = "S" Then
            bolRetorno = True
        Else
            bolRetorno = False
        End If
        Return bolRetorno
    End Function

    Private Sub LlenaGrid()
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            objFuncion = New LibCapaNegocio.clsFunciones
            bolAprobador = Session.Item("EsAprobador")
            objFuncion.gCargaGrid(dgdResultado, objRequ.gdtConRequerimientosxAprobar(CStr(Session.Item("CodCliente")),
                    cboArea.SelectedItem.Value, cboTipoEnvio.SelectedItem.Value,
                     txtFechaIngresoDesde.Value, txtFechaIngresoHasta.Value, IIf(bolAprobador = False, CStr(Session.Item("UsuarioLogin")), "")))
            lblRegistros.Text = objRequ.intFilasAfectadas.ToString & " registros"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        txtFechaIngresoDesde.Value = objFuncion.gstrFechaMes(0)
        txtFechaIngresoHasta.Value = objFuncion.gstrFechaMes(1)
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
    End Sub

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        EliminarRequerimientoTemporal(sender)
        LlenaGrid()
    End Sub

    Private Sub EliminarRequerimientoTemporal(ByVal objSender As Object)
        Try
            Dim imgEliminar As ImageButton = CType(objSender, ImageButton)
            Dim dgItem As DataGridItem
            Dim j As Integer = 0
            dgItem = CType(imgEliminar.Parent.Parent, DataGridItem)
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            If objRequ.gbolEliminaRequerimientoTemp(CStr(Session.Item("CodCliente")), CInt(dgItem.Cells(1).Text)) Then
                Me.lblError.Text = "El Requerimiento nro: " + dgItem.Cells(1).Text + " fue eliminado exitosamente!"
            Else
                Me.lblError.Text = "ERROR en el procedimiento de eliminaci�n."
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("CodRequerimiento", dgi.Cells(1).Text)
            Session.Add("PaginaAnterior", "AprobarRequerimiento.aspx")
            Response.Redirect("ConsultasxRequerimientoTemp.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim itemType As ListItemType = CType(e.Item.ItemType, ListItemType)
        If itemType <> ListItemType.Footer And itemType <> ListItemType.Separator Then
            If itemType = ListItemType.Header Then
            Else
                Dim idFila As String = dgdResultado.ClientID & "_row" & e.Item.ItemIndex.ToString
                e.Item.Attributes.Add("id", idFila)
                Dim chkSel As CheckBox = CType(e.Item.FindControl("chkSel"), CheckBox)
                Dim idCheckBox As String = CType(chkSel, CheckBox).ClientID
                'e.Item.Attributes.Add("onMouseMove", "Javascript:chkSelect_OnMouseMove(" & idFila & "," & idCheckBox & ")")
                'e.Item.Attributes.Add("onMouseOut", "Javascript:chkSelect_OnMouseOut(" & idFila & "," & idCheckBox & "," & e.Item.ItemIndex & ")")
                chkSel.Attributes.Add("onClick", "Javascript:chkSelect_OnClick('" & idFila & "','" & idCheckBox & "'," & e.Item.ItemIndex & ")")
                Dim imgEliminar As ImageButton
                imgEliminar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
                imgEliminar.Attributes.Add("onClick", "return MsgEliminar(" & e.Item.DataItem("NU_REQU") & ");")
            End If
        End If
    End Sub

    Private Sub btnAprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAprobar.Click
        If ValidarFormulario() = True Then
            AprobarRequerimientosSeleccionados()
            LlenaGrid()
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "P", "DEPSAFIL", "APROBAR REQUERIMIENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        End If
    End Sub

    Private Sub AprobarRequerimientosSeleccionados()
        Dim dgItem As DataGridItem
        Dim intMarcados As Integer = 0
        Dim bolAprobado As Boolean = False
        Try
            For Each dgItem In dgdResultado.Items
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                    intMarcados += 1
                    bolAprobado = AprobarRequerimiento(CInt(dgItem.Cells(1).Text))
                    If bolAprobado = True Then
                        lblError.Text &= " Se aprobo la solicitud nro: " & dgItem.Cells(1).Text.ToString & " generando el nro de Requeriento: " & lstrNumRequ & ", "
                    Else
                        lblError.Text &= " No se aprob� la solicitud nro: " & dgItem.Cells(1).Text.ToString
                    End If
                End If
            Next
        Catch ex As Exception
            lblError.Text &= ex.Message
        End Try
    End Sub

    'Private Function AprobarRequerimiento(ByVal pintRequerimiento As Integer) As Boolean
    '    Try
    '        objRequ = New LibCapaNegocio.clsCNRequerimiento
    '        If objRequ.gbolAprobarRequerimiento(CStr(Session.Item("CodCliente")), _
    '        pintRequerimiento, CStr(Session.Item("CodPersAuto")), _
    '            CStr(Session.Item("NomCliente")), CStr(Session.Item("UsuarioLogin"))) = True Then

    '            lstrNumRequ = objRequ.strCodRequerimiento()
    '            Return True
    '        Else
    '            lblError.Text = objRequ.strMensajeError
    '            Return False
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = "ERROR:" & ex.Message
    '        Return False
    '    End Try
    'End Function

    Private Function AprobarRequerimiento(ByVal pintRequerimiento As Integer) As Boolean
        Try
            objRequ = New LibCapaNegocio.clsCNRequerimiento
            If objRequ.gbolAprobarRequerimiento(CStr(Session.Item("CodCliente")),
            pintRequerimiento, CStr(Session.Item("CodPersAuto")),
                CStr(Session.Item("NomCliente")), CStr(Session.Item("UsuarioLogin")), "", "") = True Then

                lstrNumRequ = objRequ.strCodRequerimiento()
                Return True
            Else
                lblError.Text = objRequ.strMensajeError
                Return False
            End If
        Catch ex As Exception
            lblError.Text = "ERROR:" & ex.Message
            Return False
        End Try
    End Function

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Function ValidarFormulario() As Boolean
        Dim bolRetorno As Boolean = False
        Try
            If dgdResultado.Items.Count = 0 Then
                bolRetorno = False
                lblError.Text = "No se ha seleccionado registro(s). Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            ElseIf dgdResultado.Items.Count > 0 Then
                Dim dgItem As DataGridItem
                For Each dgItem In dgdResultado.Items
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked = True Then
                        bolRetorno = True
                        Exit For
                    End If
                Next
                If bolRetorno = False Then
                    lblError.Text = "No se ha seleccionado registro(s). Seleccione por lo menos un registro para continuar."
                End If
            End If
            Return bolRetorno
        Catch ex As Exception
            lblError.Text = ex.Message
            Return False
        End Try
    End Function

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        VerificaSiEsAprobador()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objFuncion Is Nothing) Then
            objFuncion = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objRequ Is Nothing) Then
            objRequ = Nothing
        End If
        If Not (objConsulta Is Nothing) Then
            objConsulta = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
