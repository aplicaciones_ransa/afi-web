<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AdmParametroNuevo.aspx.vb" Inherits="AdmParametroNuevo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AdmParametroNuevo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116)
			{ 
				window.event.keyCode = 505;  
				} 
				if(window.event && window.event.keyCode == 505)
				{  
				return false;     
				}  
			}  
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE id="Table3" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%">
												<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="3">
												<uc1:MenuInfo id="MenuInfo1" runat="server"></uc1:MenuInfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD align="center">
															<TABLE id="Table7" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="text" width="30%">Dominio :</TD>
																	<TD width="70%">
																		<asp:dropdownlist id="cboDominio" runat="server" CssClass="text" Width="280px"></asp:dropdownlist></TD>
																</TR>
																<TR>
																	<TD class="text">C�digo de Par�metro :</TD>
																	<TD width="70%">
																		<asp:textbox id="txtNombPar" runat="server" CssClass="text" Width="280px" MaxLength="20"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 108px">Descripci�n :</TD>
																	<TD>
																		<asp:textbox id="txtDescLarg" runat="server" CssClass="text" Width="280px" MaxLength="100"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 106px">Valor 1:</TD>
																	<TD>
																		<asp:textbox id="txtValor1" runat="server" CssClass="text" Width="280px" MaxLength="150"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 106px">Valor 2:</TD>
																	<TD>
																		<asp:textbox id="txtValor2" runat="server" CssClass="text" Width="280px" MaxLength="150"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 106px">Valor 3:</TD>
																	<TD>
																		<asp:textbox id="txtValor3" runat="server" CssClass="text" Width="280px" MaxLength="150"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 106px">Valor 4:</TD>
																	<TD>
																		<asp:textbox id="txtValor4" runat="server" CssClass="text" Width="280px" MaxLength="150"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="text" style="WIDTH: 106px">Valor 5:</TD>
																	<TD>
																		<asp:textbox id="txtValor5" runat="server" CssClass="text" Width="280px" MaxLength="150"></asp:textbox></TD>
																</TR>
															</TABLE>
															<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" cellSpacing="4" cellPadding="0" border="0">
													<TR>
														<TD width="80">
															<asp:button id="btnNuevo" runat="server" CssClass="btn" Text="Nuevo"></asp:button></TD>
														<TD width="80">
															<asp:button id="btnGrabar" runat="server" CssClass="btn" Text="Grabar"></asp:button></TD>
														<TD width="80">
															<asp:button id="btnRegresar" CssClass="btn" Text="Regresar" Runat="server" CausesValidation="False"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
