Imports Depsa.LibCapaNegocio
Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports System.Data

Public Class WarAlmacenXCliente
    Inherits System.Web.UI.Page
    Private objMercaderia As clsCNMercaderia
    Private objConexion As SqlConnection
    Private objTransaccion As SqlTransaction
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents lstAlmacen As System.Web.UI.WebControls.ListBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtAlmacen As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If Not Me.IsPostBack Then
                ListarAlmacen()
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub ListarAlmacen()
        Dim i As Integer
        objMercaderia = New clsCNMercaderia
        Dim dtAlmacenes As DataTable
        lstAlmacen.ClearSelection()
        dtAlmacenes = objMercaderia.gCNGetListarAlmacen(Session("CodCliente"))
        For i = 0 To dtAlmacenes.Rows.Count - 1
            Me.lstAlmacen.Items.Add(New ListItem(dtAlmacenes.Rows(i)("DSC_ALMA"), dtAlmacenes.Rows(i)("COD_ALMA")))
        Next
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        objMercaderia = New clsCNMercaderia
        objConexion = New SqlConnection(strCadenaConexion)
        objConexion.Open()
        objTransaccion = objConexion.BeginTransaction
        Me.lstAlmacen.Items.Add(UCase(Me.txtAlmacen.Text))
        objMercaderia.gCNInsAlmacenNuevo(UCase(Me.txtAlmacen.Text), Session("CodCliente"), Session("IdUsuario"), objTransaccion)
        Me.txtAlmacen.Text = ""
        objTransaccion.Commit()
        objTransaccion.Dispose()
    End Sub
End Class
