<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ActualizarDatos.aspx.vb" Inherits="ActualizarDatos"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ActualizarDatos</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label><FONT face="Tahoma" size="2"></FONT></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="13"><uc1:menuinfo id="MenuInfo2" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" align="center" border="0" width="670">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table7" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="Text" width="14%">Tipo Persona</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="20%"><asp:radiobutton id="rbtNatural" runat="server" CssClass="Text" GroupName="Persona" Text="Natural"></asp:radiobutton></TD>
																	<TD class="Text" width="20%"><asp:radiobutton id="rbtJuridico" runat="server" CssClass="Text" GroupName="Persona" Text="Jur�dico"></asp:radiobutton></TD>
																	<TD class="Text" width="45%"><asp:radiobutton id="rbtExtranjero" runat="server" CssClass="Text" GroupName="Persona" Text="Extranjero"></asp:radiobutton></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top" align="center">Persona Jur�dica</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table9" cellSpacing="0" cellPadding="0" align="center" border="0" width="670">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table5" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="Text" width="14%">Raz�n social</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" colSpan="4"><asp:textbox id="txtRazon" runat="server" CssClass="Text" Width="460px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="Text" width="14%">Sigla Comercial</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD width="34%"><asp:textbox id="txtSigla" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																	<TD class="Text" width="11%">C�digo SBS</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="39%"><asp:textbox id="txtSBS" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top" align="center">Persona Natural</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table10" cellSpacing="0" cellPadding="0" align="center" border="0" width="670">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table11" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="Text" width="14%">Ap. Paterno</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="34%"><asp:textbox id="txtPaterno" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																	<TD class="Text" width="11%">Ap. Materno</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="39%"><asp:textbox id="txtMaterno" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="Text" width="14%">Nombres</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" colSpan="4"><asp:textbox id="txtNombres" runat="server" CssClass="Text" Width="460px"></asp:textbox></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top" align="center">Otros Datos</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table12" cellSpacing="0" cellPadding="0" align="center" border="0" width="670">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD>
															<TABLE id="Table13" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="Text" width="14%">RUC 1</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD width="34%"><asp:textbox id="txtRuc1" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																	<TD class="Text" width="11%">RUC 2</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="39%"><asp:textbox id="txtRuc2" runat="server" CssClass="Text" Width="180px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="Text" width="14%">Web Site</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD width="34%" colSpan="4"><asp:textbox id="txtWeb" runat="server" CssClass="Text" Width="460px"></asp:textbox></TD>
																</TR>
															</TABLE>
														</TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top" align="center">Direcciones registradas</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="left"><asp:datagrid id="dgdDirecciones" runat="server" CssClass="gv" PageSize="5" AllowSorting="True"
													AutoGenerateColumns="False" BorderColor="Gainsboro">
													<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter" VerticalAlign="Top"></AlternatingItemStyle>
													<ItemStyle CssClass="gvRow" VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="NU_SECU" HeaderText="Sec"></asp:BoundColumn>
														<asp:TemplateColumn HeaderText="TipoDirec.">
															<ItemTemplate>
																<asp:DropDownList id=cboTipoDireccion runat="server" CssClass="Text" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.TI_DIRE") %>' DataValueField="TI_DIRE" DataTextField="DE_TIPO_DIRE" DataSource="<%# GetTipoDireccion() %>">
																</asp:DropDownList>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Direcci&#243;n">
															<ItemTemplate>
																<asp:TextBox id=txtDireccion runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.DE_DIRE") %>' Width="200px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Pa&#237;s">
															<ItemTemplate>
																<asp:DropDownList id=cboPais runat="server" CssClass="Text" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.CO_PAIS") %>' DataValueField="CO_PAIS" DataTextField="NO_PAIS" DataSource="<%# GetPais() %>">
																</asp:DropDownList>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Tel&#233;fono">
															<ItemTemplate>
																<asp:TextBox id=txtTelefonoDirec runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_TELE_0001") %>' Width="60px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Anexo">
															<ItemTemplate>
																<asp:TextBox id=txtAnexoDirec runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_ANEX") %>' Width="40px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Fax">
															<ItemTemplate>
																<asp:TextBox id=txtFaxDirec runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_FAXE") %>' Width="50px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Elim">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:ImageButton id="imgEliminarDirec" onclick="imgEliminarDirec_Click" runat="server" CausesValidation="False"
																	ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.JPG"></asp:ImageButton>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:button id="btnAddDirec" runat="server" CssClass="btn" Text="Agregar Filas" Width="80px"></asp:button></TD>
										</TR>
										<TR>
											<TD class="Subtitulo" vAlign="top" align="center">Contactos 
												registrados&nbsp;&nbsp;&nbsp;
												<asp:Label id="lblLeyenda" runat="server">Nota : F.E. = Recepci�n de Facrura Electronica.</asp:Label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="left"><asp:datagrid id="dgdContactos" runat="server" CssClass="gv" PageSize="5" AllowSorting="True"
													AutoGenerateColumns="False" BorderColor="Gainsboro">
													<AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter" VerticalAlign="Top"></AlternatingItemStyle>
													<ItemStyle CssClass="gvRow" VerticalAlign="Top"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="NU_SECU" HeaderText="Sec"></asp:BoundColumn>
														<asp:TemplateColumn HeaderText="TipoContac">
															<ItemTemplate>
																<asp:DropDownList id=cboTipoContacto runat="server" CssClass="Text" Width="200px" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.CO_TIPO_CONT") %>' DataValueField="CO_TIPO_CONT" DataTextField="DE_TIPO_CONT" DataSource="<%# GetTipoContacto()%>">
																</asp:DropDownList>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Nombre">
															<ItemTemplate>
																<asp:TextBox id=txtNombre runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NO_CONT") %>' Width="150px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Tel&#233;fono">
															<ItemTemplate>
																<asp:TextBox id=txtTelefono runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_TELE_0001") %>' Width="60px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Anexo">
															<ItemTemplate>
																<asp:TextBox id=txtAnexo runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_ANEX") %>' Width="40px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Fax">
															<ItemTemplate>
																<asp:TextBox id=txtFax runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_FAXE") %>' Width="50px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="E-mail">
															<ItemTemplate>
																<asp:TextBox id=txtEmail runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.DI_EMAI") %>' Width="100px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="TipoDoc">
															<ItemTemplate>
																<asp:DropDownList id=cboTipoDoc runat="server" CssClass="Text" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.TI_DOCU_IDEN") %>' DataValueField="TI_DOCU_IDEN" DataTextField="TI_DOCU_IDEN" DataSource="<%# GetTipoDoc() %>">
																</asp:DropDownList>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="NroDoc">
															<ItemTemplate>
																<asp:TextBox id=txtNroDoc runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.NU_DOCU_IDEN") %>' Width="80px">
																</asp:TextBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="F.E.">
															<ItemTemplate>
																<asp:checkbox id="checFaEl" runat="server" CssClass="Text" checked='<%# DataBinder.Eval(Container, "DataItem.ST_ENVI_FAEL") %>'>
																</asp:checkbox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Elim">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:ImageButton id="imgEliminarContac" onclick="imgEliminarContac_Click" runat="server" CausesValidation="False"
																	ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.JPG"></asp:ImageButton>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle PageButtonCount="30" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
												</asp:datagrid><asp:button id="btnAddContac" runat="server" CssClass="btn" Text="Agregar Filas"></asp:button></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" cellSpacing="4" cellPadding="0" border="0">
													<TR>
														<TD width="80"><asp:button id="btnActualizar" runat="server" CssClass="Text" Text="Solicitar Actualizaci�n"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
