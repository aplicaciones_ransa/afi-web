Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports System.Text
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class WarSolicitudWarrantDos
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    Dim strAlmacen As String
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Protected WithEvents cboTipoTitulo As System.Web.UI.WebControls.DropDownList
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents FilePDF As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacenes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnStock As System.Web.UI.WebControls.Button
    'Protected WithEvents trAlmacen As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnGrabar As RoderoLib.BotonEnviar
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHoraIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkReemplazo As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put User code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "38") Then
            Try

                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "38"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    Me.lblOpcion.Text = "Solicitud de Warrants"
                    loadFinanciador()
                    InicializaBusqueda()
                    BindDatagrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaSolicitud") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaSolicitud").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.cboEstado.SelectedValue = strFiltros(0)
            Me.cboFinanciador.SelectedValue = strFiltros(1)
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaSolicitud", Me.cboEstado.SelectedValue & "-" & Me.cboFinanciador.SelectedValue)
    End Sub
    Public Sub imgEnviar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEnviar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim dtEnviar As DataTable
            Dim j As Integer = 0
            dgi = CType(imgEnviar.Parent.Parent, DataGridItem)
            '=== validar que yenga Items ===
            dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrantDetalle(dgi.Cells(0).Text(), dgi.Cells(9).Text(), dgi.Cells(11).Text(), "0")
            If dt.Rows.Count = 0 Then
                pr_IMPR_MENS("La solicitud no tiene Items")
                Exit Sub
            End If
            '=== Enviar solicitud ===
            dtEnviar = objDocumento.SetUpdateDocumentosSolicitadorWarrant(dgi.Cells(9).Text(), Session.Item("IdUsuario"), "ENV", "")
            BindDatagrid()
            If dtEnviar.Rows.Count <> 0 Then
                pr_IMPR_MENS(dtEnviar.Rows(0).Item("MENSAJE"))
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub
    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)

            Session.Add("NroDoc", dgi.Cells(0).Text())
            Session.Add("Cod_Doc", dgi.Cells(9).Text())
            Session.Add("NroLib", dgi.Cells(10).Text())
            Session.Add("TipDoc", dgi.Cells(11).Text())
            Session.Add("CodFina", dgi.Cells(12).Text())
            Session.Add("NroSecuSol", dgi.Cells(13).Text())
            Response.Redirect("WarSolicitudWarrantRegistro.aspx", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub
    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim dtDesap As DataTable
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            dtDesap = objDocumento.SetUpdateDocumentosSolicitadorWarrant(dgi.Cells(9).Text(), Session.Item("IdUsuario"), "DES", "")
            BindDatagrid()
            If dtDesap.Rows.Count <> 0 Then
                pr_IMPR_MENS(dtDesap.Rows(0).Item("MENSAJE"))
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub loadFinanciador()
        Dim dtEntidad As New DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            'dtEntidad = objEntidad.gGetEntidadesSICO("02")
            dtEntidad = objEntidad.gGetEntidades("02", "W")
            Me.cboFinanciador.Items.Add(New ListItem("--------------- TODOS ---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
                'Me.cboFinanciador.Items.Add(New ListItem(dr("nombre").ToString(), dr("codigo")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "Error al llenar entidades: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Private Sub BindDatagrid()
        Try
            dt = Nothing
            dt = New DataTable
            dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrant("", "", "24", Me.cboEstado.SelectedValue, Session.Item("IdSico"), Me.cboFinanciador.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Protected Overrides Sub Finalize()
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Aqui van tus acciones por cada fila de datos....
            CType(e.Item.FindControl("imgEnviar"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de enviar la solicitud de warrants?')== false) return false;")
            CType(e.Item.FindControl("imgEliminar"), ImageButton).Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de eliminar la solicitud de warrants?')== false) return false;")
            If e.Item.Cells(13).Text = "2" Then
                CType(e.Item.FindControl("imgEnviar"), ImageButton).ImageUrl = "Images/Transparente.png"
                'CType(e.Item.FindControl("imgEditar"), ImageButton).ImageUrl = "Images/Transparente.png"
                CType(e.Item.FindControl("imgEliminar"), ImageButton).ImageUrl = "Images/Transparente.png"
                CType(e.Item.FindControl("imgEnviar"), ImageButton).Enabled = False
                'CType(e.Item.FindControl("imgEditar"), ImageButton).Enabled = False
                CType(e.Item.FindControl("imgEliminar"), ImageButton).Enabled = False
            End If

        End If
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        Try
            If e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemType = ListItemType.AlternatingItem Then
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                Else
                    e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFFF';")
                    e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#f0f0f0';")
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnSolicitud_Click(sender As Object, e As EventArgs) Handles btnSolicitud.Click
        Session.Add("NroDoc", "")
        Session.Add("Cod_Doc", "")
        Session.Add("TipDoc", "24")
        Session.Add("NroSecuSol", "1")
        Session.Add("CodFina", "0")
        Response.Redirect("WarSolicitudWarrantRegistro.aspx", False)
    End Sub
End Class
