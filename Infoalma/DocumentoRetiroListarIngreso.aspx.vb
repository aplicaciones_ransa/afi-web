Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Library.AccesoBL
Imports Infodepsa
Imports System.Data

Public Class DocumentoRetiroListarIngreso
    Inherits System.Web.UI.Page
    Private objRetiro As New clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroGuia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Lista de Ingresos WMS"
                If Not IsPostBack Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_INGRESO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)

                    Dim dttFecha As DateTime
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
                    Dia = "00" + CStr(dttFecha.Day)
                    Mes = "00" + CStr(dttFecha.Month)
                    Anio = "0000" + CStr(dttFecha.Year)
                    Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Bindatagrid()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dt As DataTable
            dt = objRetiro.gCNGetBuscarRetirosIngresadosWMS(Session("IdSico"), Me.txtNroGuia.Text, txtFechaInicial.Value, txtFechaFinal.Value)
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Session.Add("dtDocuListarIngreso", dt)
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaRetiro") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaRetiro").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.txtNroGuia.Text = strFiltros(0)
            txtFechaInicial.Value = strFiltros(1)
            txtFechaFinal.Value = strFiltros(2)
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.lblMensaje.Text = ""
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaRetiro", Me.txtNroGuia.Text & "-" & txtFechaInicial.Value & "-" & txtFechaFinal.Value)
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Bindatagrid()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        'Try
        '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
        '        Dim imgEditar As ImageButton
        '        imgEditar = CType(e.Item.FindControl("imgEditar"), ImageButton)
        '        imgEditar.Attributes.Add("onClick", "javascript:AbrirDetalle('" & e.Item.DataItem("ORDEN").ToString.Trim & "','" & e.Item.DataItem("DE_ALMA").ToString.Trim & "');")

        '        '    If e.Item.DataItem("FLG_ALMA").ToString.Trim = Session.Item("IdSico") And (Session.Item("IdTipoEntidad") = "01" Or Session.Item("IdTipoEntidad") = "03") Then
        '        '        e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
        '        '    ElseIf e.Item.DataItem("st_sali_alma").ToString.Trim = "S" Then
        '        '        e.Item.BackColor = System.Drawing.Color.FromArgb(173, 213, 230)
        '        '    Else
        '        '        e.Item.BackColor = System.Drawing.Color.White
        '        '    End If
        '    End If
        'Catch e1 As Exception
        '    Me.lblMensaje.Text = e1.Message
        'End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        'Dim strFileName As String = "Reporte.xls"
        ''se limpia el buffer del response
        'Response.Clear()
        'Response.Buffer = True
        ''se establece el tipo de accion a ejecutar
        'Response.ContentType = "application/download"
        'Response.Charset = ""
        'Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        ''Response.Charset = "UTF-8"
        ''Response.ContentEncoding = Encoding.Default
        ''se inhabilita el view state para no ejecutar ninguna accion
        'Me.EnableViewState = False
        ''se rederiza el control datagrid
        'Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        'Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
        'Me.dgResultado.RenderControl(htmlWriter)
        ''se escribe el contenido en el listado
        'Response.Write(sWriter.ToString())
        'Response.End()

        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Reporte.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtDocuListarIngreso"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing

    End Sub
End Class

