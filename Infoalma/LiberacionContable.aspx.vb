Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class LiberacionContable
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEndosatario As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtDescripci�n As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboTipoMercaderia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "21") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "21"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("NroRetiro") <> Nothing Then
                    pr_IMPR_MENS("Se Gener� la Solicitud de liberaci�n Contable " + Session.Item("NroSoliWarr"))
                    Session.Remove("NroRetiro")
                    Session.Remove("NroSoliWarr")
                End If
                Me.lblOpcion.Text = "Consulta para generar Liberaci�n para la emisi�n de nuevo Warrant"
                loadEndosatario()
                loadTipoMerc()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub loadEndosatario()
        Dim dtEntidad As New DataTable
        Me.cboEndosatario.Items.Clear()
        dtEntidad = objEntidad.gGetEntidades("02", "")
        Me.cboEndosatario.Items.Add(New ListItem("--------------------Todos-------------------", "0"))
        For Each dr As DataRow In dtEntidad.Rows
            Me.cboEndosatario.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), CType(dr("COD_SICO"), String).Trim))
        Next
        dtEntidad.Dispose()
        dtEntidad = Nothing
    End Sub

    Private Sub loadTipoMerc()
        Dim dtTipoMerc As New DataTable
        Me.cboTipoMercaderia.Items.Clear()
        dtTipoMerc = objEntidad.gGetTipoMerc()
        Me.cboTipoMercaderia.Items.Add(New ListItem("--------------------Todos-------------------", "0"))
        For Each dr As DataRow In dtTipoMerc.Rows
            Me.cboTipoMercaderia.Items.Add(New ListItem(dr("DE_TIPO_MERC").ToString(), CType(dr("CO_TIPO_MERC"), String).Trim))
        Next
        dtTipoMerc.Dispose()
        dtTipoMerc = Nothing
    End Sub

    Private Sub BindDatagrid()
        dt = New DataTable
        Try
            dt = objDocumento.gGetWarrantLiberaciones(Me.txtNroWarrant.Text.Replace("'", ""), Me.cboEndosatario.SelectedValue, Me.cboTipoMercaderia.SelectedValue, Me.txtDescripci�n.Text.Replace("'", ""), Session.Item("IdSico"), "N", "0")
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(0).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(1).Text())
            Session.Add("TipoComprobante", dgi.Cells(13).Text())
            Session.Add("NroComprobante", dgi.Cells(3).Text())
            Session.Add("CodUnidad", dgi.Cells(14).Text())
            Session.Add("CodEntiFina", dgi.Cells(12).Text())
            Session.Add("Virtual", dgi.Cells(15).Text())
            Session.Add("Embarque", dgi.Cells(8).Text())
            Session.Add("WarCustodia", dgi.Cells(9).Text())
            Session.Add("IdSicoDepositante", Session.Item("IdSico"))
            ''****** variables para el boton solicitar **************
            Session.Add("CodAlmaDest", dgi.Cells(17).Text())
            Session.Add("FlgContable", dgi.Cells(23).Text())

            ''Session.Add("slb_NroDoc", dgi.Cells(0).Text())
            ''Session.Add("slb_IdTipoDocumento", dgi.Cells(1).Text())
            'Session.Add("slb_FchVencBanc", dgi.Cells(22).Text())
            'Session.Add("slb_Endosatario", dgi.Cells(4).Text())
            'Session.Add("slb_DscMerc", dgi.Cells(5).Text())
            'Session.Add("slb_CoMoneda", dgi.Cells(6).Text())
            ''Session.Add("slb_StEndoEmba", dgi.Cells(9).Text())
            ''Session.Add("slb_CoEntiFina", dgi.Cells(12).Text())
            ''Session.Add("slb_TiDocuRece", dgi.Cells(13).Text())
            ''Session.Add("slb_CoUnid", dgi.Cells(14).Text())
            ''Session.Add("slb_StVirtual", dgi.Cells(15).Text())
            'Session.Add("slb_CoModaMovi", dgi.Cells(16).Text())
            ''Session.Add("slb_NuDocuRece", dgi.Cells(3).Text())
            'Session.Add("slb_CoAlma", dgi.Cells(17).Text())
            'Session.Add("slb_DeDireAlma", dgi.Cells(18).Text())
            'Session.Add("slb_TiAlma", dgi.Cells(19).Text())
            'Session.Add("slb_FlgEndo", dgi.Cells(20).Text())
            'Session.Add("slb_CoSeguEndo", dgi.Cells(21).Text())



            '****** variables para el boton solicitar **************

            'Session.Add("NroDoc", dgi.Cells(0).Text())
            'Session.Add("IdTipoDocumento", dgi.Cells(1).Text())
            'Session.Add("NroComprobante", dgi.Cells(3).Text())
            'Session.Add("slb_Endosatario", dgi.Cells(4).Text())
            'Session.Add("slb_DscMerc", dgi.Cells(5).Text())
            'Session.Add("slb_CoMoneda", dgi.Cells(6).Text())
            'Session.Add("WarCustodia", dgi.Cells(9).Text())
            'Session.Add("CodEntiFina", dgi.Cells(12).Text())
            'Session.Add("TipoComprobante", dgi.Cells(13).Text())
            'Session.Add("CodUnidad", dgi.Cells(14).Text())
            'Session.Add("Virtual", dgi.Cells(15).Text())
            'Session.Add("slb_CoModaMovi", dgi.Cells(16).Text())
            'Session.Add("slb_CoAlma", dgi.Cells(17).Text())
            'Session.Add("slb_DeDireAlma", dgi.Cells(18).Text())
            'Session.Add("slb_TiAlma", dgi.Cells(19).Text())
            'Session.Add("slb_FlgEndo", dgi.Cells(20).Text())
            'Session.Add("slb_CoSeguEndo", dgi.Cells(21).Text())
            'Session.Add("slb_FchVencBanc", dgi.Cells(22).Text())

            'Dim str_varTodo As String =
            'dgi.Cells(0).Text() & "," &
            'dgi.Cells(1).Text() & "," &
            'dgi.Cells(3).Text() & "," &
            'dgi.Cells(4).Text() & "," &
            'dgi.Cells(5).Text() & "," &
            'dgi.Cells(6).Text() & "," &
            'dgi.Cells(9).Text() & "," &
            'dgi.Cells(12).Text() & "," &
            'dgi.Cells(13).Text() & "," &
            'dgi.Cells(14).Text() & "," &
            'dgi.Cells(15).Text() & "," &
            'dgi.Cells(16).Text() & "," &
            'dgi.Cells(17).Text() & "," &
            'dgi.Cells(18).Text() & "," &
            'dgi.Cells(19).Text() & "," &
            'dgi.Cells(20).Text() & "," &
            'dgi.Cells(21).Text() & "," &
            'dgi.Cells(22).Text()


            ' usp_fn_CONS_WARR                                                                           'usp_fn_CONS_WARR
            ' LiberacionContable_old.aspx :                                                              LiberacionContable.aspx :
            '0) NU_TITU       --> Nro Warr              --> 'dgi.Cells(0).Text()  --00092298         -->  dgi.Cells(0).Text()
            '1) TI_TITU       --> Tipo Doc		        --> 'dgi.Cells(1).Text()  --WAR              -->  dgi.Cells(1).Text()
            '2) FE_VIGE_BANC  --> FchVencBanc			--> 'dgi.Cells(2).Text()  --28/12/2019       -->  dgi.Cells(22).Text()
            '4) DE_CORT_ENFI  --> Endosatario           --> 'dgi.Cells(4).Text()  --BCO.INTERAMERIC  -->  dgi.Cells(4).Text() 
            '5) DE_MERC_GENE  --> Dsc Mercader&#237;a	--> 'dgi.Cells(5).Text()  --VEHICULO         -->  dgi.Cells(5).Text()
            '6) co_mone       --> Moneda                --> 'dgi.Cells(6).Text()  --DOL              -->  dgi.Cells(6).Text()
            '8) ST_ENDO_EMBA  --> Emba                  --> 'dgi.Cells(8).Text()  --NO               -->  dgi.Cells(9).Text()
            '11 CO_ENTI_FINA  --> CO_ENTI_FINA1         --> 'dgi.Cells(11).Text() --101              -->  dgi.Cells(12).Text()
            '12 TI_DOCU_RECE  --> TI_DOCU_RECE1         --> 'dgi.Cells(12).Text() --DCR              -->  dgi.Cells(13).Text() 
            '13) CO_UNID      --> CO_UNID1              --> 'dgi.Cells(13).Text() --001              -->  dgi.Cells(14).Text()
            '14) virtual      --> virtual1              --> 'dgi.Cells(14).Text() --S                -->  dgi.Cells(15).Text()
            '15) CO_MODA_MOVI --> CO_MODA_MOVI1         --> 'dgi.Cells(15).Text() --013              -->  dgi.Cells(16).Text()
            '16) NU_DOCU_RECE --> NU_DOCU_RECE1         --> 'dgi.Cells(16).Text() --00000142501      -->  dgi.Cells(3).Text()
            '17) CO_ALMA      --> CO_ALMA               --> 'dgi.Cells(17).Text() --01304            -->  dgi.Cells(17).Text()
            '18) DE_DIRE_ALMA --> DE_DIRE_ALMA          --> 'dgi.Cells(18).Text() --PARTE DEL FUND...-->  dgi.Cells(18).Text()
            '19) TI_ALMA      --> TI_ALMA               --> 'dgi.Cells(19).Text() --002              -->  dgi.Cells(19).Text()
            '21) FLG_DENDO    --> FLG_DENDO             --> 'dgi.Cells(21).Text() --N                -->  dgi.Cells(20).Text()
            '22) CO_SEGU_ENDO --> CO_SEGU_ENDO          --> 'dgi.Cells(22).Text() --&nbsp;           -->  dgi.Cells(21).Text()




            '--dgi.Cells(0).Text() --00092298
            '--dgi.Cells(1).Text() --WAR
            '--dgi.Cells(2).Text() --28/11/2019
            '--dgi.Cells(3).Text() --00000142501
            '--dgi.Cells(4).Text() --BCO.INTERAMERIC 
            '--dgi.Cells(5).Text() --VEHICULO
            '--dgi.Cells(6).Text() --DOL
            '--dgi.Cells(7).Text() --25,000.00
            '--dgi.Cells(8).Text() --NO
            '--dgi.Cells(9).Text() --NO
            '--dgi.Cells(10).Text()--NO
            '--dgi.Cells(11).Text()--''
            '--dgi.Cells(12).Text()--101
            '--dgi.Cells(13).Text()--DCR
            '--dgi.Cells(14).Text()--001
            '--dgi.Cells(15).Text()--S

            'CO_UNID TI_DOCU_RECE	NU_DOCU_RECE	CO_CLIE_ACTU	DE_MERC_GENE	
            'FE_VIGE_BANC    virtual	CO_MODA_MOVI	DE_MODA_MOVI	FE_TITU	im_saldo
            'CO_MONE TI_TITU	NU_TITU	NO_DOCU	CO_ALMA	DE_DIRE_ALMA	CO_ENTI_FINA	DE_CORT_ENFI
            'ST_ENDO_EMBA    ST_CUST_DEPS	PARCIAL	TI_ALMA	FLG_DENDO	CO_SEGU_ENDO	FLG_CONT


            '--0) NU_TITU        --> Nro Warr
            '--1) TI_TITU        --> Tipo Doc	
            '--2) FE_TITU        --> Fecha
            '--3) NU_DOCU_RECE   --> DCR
            '--4) DE_CORT_ENFI   --> Endosatario
            '--5) DE_MERC_GENE   --> Dsc Mercader&#237;a
            '--6) co_mone        --> Moneda
            '--7) im_saldo       --> Saldo
            '--8) ST_ENDO_EMBA   --> Emba
            '--9) ST_CUST_DEPS  --> Custodia?
            '--10) PARCIAL       --> Parcial?
            '--11) "Ver">        -->
            '--12) CO_ENTI_FINA  -->
            '--13) TI_DOCU_RECE  -->
            '--14) CO_UNID	     -->
            '--15) virtual	     -->

            '--16) CO_MODA_MOVI	 -->
            '--17) CO_ALMA	     -->
            '--18) DE_DIRE_ALMA	 -->
            '--19) TI_ALMA	     -->
            '--20) FLG_DENDO	 -->
            '--21) CO_SEGU_ENDO	 -->
            '--22) FE_VIGE_BANC	 -->


            '--------------------------------------------------------------------------------------------------------------------

            ' usp_fn_CONS_WARR                                                                           'usp_fn_CONS_WARR
            ' LiberacionContable_old.aspx :                                                              LiberacionContable.aspx :
            '0) NU_TITU       --> Nro Warr              --> 'dgi.Cells(0).Text()  --00092298         -->  dgi.Cells(0).Text()
            '1) TI_TITU       --> Tipo Doc		        --> 'dgi.Cells(1).Text()  --WAR              -->  dgi.Cells(1).Text()
            '2) FE_VIGE_BANC  --> FchVencBanc			--> 'dgi.Cells(2).Text()  --28/12/2019       -->  dgi.Cells(22).Text()
            '3) NU_DOCU_RECE  --> DCR1                  --> 
            '4) DE_CORT_ENFI  --> Endosatario           --> 'dgi.Cells(4).Text()  --BCO.INTERAMERIC  -->  dgi.Cells(4).Text() 
            '5) DE_MERC_GENE  --> Dsc Mercader&#237;a	--> 'dgi.Cells(5).Text()  --VEHICULO         -->  dgi.Cells(5).Text()
            '6) co_mone       --> Moneda                --> 'dgi.Cells(6).Text()  --DOL              -->  dgi.Cells(6).Text()
            '7) im_saldo      --> Saldo	                --> 
            '8) ST_ENDO_EMBA  --> Emba                  --> 'dgi.Cells(8).Text()  --NO               -->  dgi.Cells(9).Text()
            '9) PARCIAL       --> Parcial               --> 
            '10 Nuevo Warrant --> btnContable(boton)    -->                           
            '11 CO_ENTI_FINA  --> CO_ENTI_FINA1         --> 'dgi.Cells(11).Text() --101              -->  dgi.Cells(12).Text()
            '12 TI_DOCU_RECE  --> TI_DOCU_RECE1         --> 'dgi.Cells(12).Text() --DCR              -->  dgi.Cells(13).Text() 
            '13) CO_UNID      --> CO_UNID1              --> 'dgi.Cells(13).Text() --001              -->  dgi.Cells(14).Text()
            '14) virtual      --> virtual1              --> 'dgi.Cells(14).Text() --S                -->  dgi.Cells(15).Text()


            '15) CO_MODA_MOVI --> CO_MODA_MOVI1         --> 'dgi.Cells(15).Text() --013              -->  dgi.Cells(16).Text()
            '16) NU_DOCU_RECE --> NU_DOCU_RECE1         --> 'dgi.Cells(16).Text() --00000142501      -->  dgi.Cells(3).Text()
            '17) CO_ALMA      --> CO_ALMA               --> 'dgi.Cells(17).Text() --01304            -->  dgi.Cells(17).Text()
            '18) DE_DIRE_ALMA --> DE_DIRE_ALMA          --> 'dgi.Cells(18).Text() --PARTE DEL FUND...-->  dgi.Cells(18).Text()
            '19) TI_ALMA      --> TI_ALMA               --> 'dgi.Cells(19).Text() --002              -->  dgi.Cells(19).Text()
            '20) Alm.. Dest.. --> cboListAlmacen(combo) -->  
            '21) FLG_DENDO    --> FLG_DENDO             --> 'dgi.Cells(21).Text() --N                -->  dgi.Cells(20).Text()
            '22) CO_SEGU_ENDO --> CO_SEGU_ENDO          --> 'dgi.Cells(22).Text() --&nbsp;           -->  dgi.Cells(21).Text()




            '-----------------------------------------------------------------------------------------------------------------



            'dgi.Cells(0).Text()  --00092298
            'dgi.Cells(1).Text()  --WAR
            'dgi.Cells(2).Text()  --28/12/2019
            'dgi.Cells(4).Text()  --BCO.INTERAMERIC
            'dgi.Cells(5).Text()  --VEHICULO
            'dgi.Cells(6).Text()  --DOL
            'dgi.Cells(8).Text()  --NO
            'dgi.Cells(11).Text() --101
            'dgi.Cells(12).Text() --DCR
            'dgi.Cells(13).Text() --001
            'dgi.Cells(14).Text() --S
            'dgi.Cells(15).Text() --013
            'dgi.Cells(16).Text() --00000142501
            'dgi.Cells(17).Text() --01304
            'dgi.Cells(18).Text() --PARTE DEL FUNDO MAYORAZGO LOTE 6
            'dgi.Cells(19).Text() --002
            'dgi.Cells(21).Text() --N
            'dgi.Cells(22).Text() --&nbsp;



            'Session.Item("IdTipoEntidad")
            'Session.Item("NombreEntidad")
            'Session.Item("IdSico") 
            'Session.Item("IdUsuario")



            'dgi.Cells(0).Text() 
            'dgi.Cells(1).Text()
            'dgi.Cells(2).Text() 
            'dgi.Cells(4).Text() 
            'dgi.Cells(5).Text() 
            'dgi.Cells(6).Text() 
            'dgi.Cells(8).Text() 
            'dgi.Cells(11).Text()
            'dgi.Cells(12).Text() 
            'dgi.Cells(13).Text() 
            'dgi.Cells(14).Text()
            'dgi.Cells(15).Text() 
            'dgi.Cells(16).Text()
            'dgi.Cells(17).Text() 
            'dgi.Cells(18).Text()
            'dgi.Cells(19).Text()
            'dgi.Cells(21).Text()
            'dgi.Cells(22).Text() 





            Response.Redirect("LiberacionDetalleContable.aspx?Merc=" & dgi.Cells(5).Text() & "&Banco=" & dgi.Cells(4).Text() & "&Fecha=" & dgi.Cells(2).Text() & "&Pagina=N", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Cells(7).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("im_saldo"))
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            If e.Item.DataItem("virtual") = "N" Then
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
            Else
                e.Item.BackColor = System.Drawing.Color.LightGoldenrodYellow
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='LightGoldenrodYellow'")
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub

End Class
