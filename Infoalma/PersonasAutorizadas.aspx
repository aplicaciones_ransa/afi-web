<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="PersonasAutorizadas.aspx.vb" Inherits="PersonasAutorizadas"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PersonasAutorizadas</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD style="WIDTH: 78px"></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table3" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR class="Texto9pt">
														<TD class="text" style="WIDTH: 68px">�rea</TD>
														<TD class="text" width="5%">:</TD>
														<TD class="text" width="30%"><asp:dropdownlist id="cboArea" runat="server" CssClass="Text" DataValueField="COD" DataTextField="DES"
																Width="161px">
																<asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
															</asp:dropdownlist></TD>
														<TD class="text" width="15%"><asp:button id="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:button></TD>
														<TD class="text" width="5%"></TD>
														<TD class="text" width="30%"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center">
									<P align="left">Resultados :
										<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></P>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" height="300"><asp:datagrid id="dgdResultado" tabIndex="2" runat="server" CssClass="gv" Width="100%" CellPadding="0"
										AllowSorting="True" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="DE_AREA" HeaderText="&#193;rea">
												<HeaderStyle Width="40%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CO_PERS" HeaderText="Codigo">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_NOMB_PERS" HeaderText="Persona">
												<HeaderStyle Width="40%"></HeaderStyle>
												<ItemStyle VerticalAlign="Top"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NI_AUTO" HeaderText="Nivel">
												<HeaderStyle Width="20%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
									<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD align="right"><asp:button id="btnVerReporte" runat="server" CssClass="btn" Text="Ver Reporte"></asp:button></TD>
											<TD align="left"><asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 13px" vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
