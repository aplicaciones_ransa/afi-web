<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DetalleRequ.aspx.vb" Inherits="DetalleRequ" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DetalleRequ</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="340" border="0">
				<TR>
					<TD class="Subtitulo" style="HEIGHT: 18px" align="center" width="40%" colSpan="3" height="18">
						DETALLE DEL DOCUMENTO
					</TD>
				</TR>
				<TR>
					<TD class="Text" style="HEIGHT: 7px" align="center" width="40%" colSpan="3" height="7">
						<asp:Label id="lblAviso" runat="server" ForeColor="Red" Font-Size="X-Small" Visible="False"
							Height="16px">Documento esta solicitado temporalmente</asp:Label></TD>
				</TR>
				<TR>
					<TD class="Text" width="40%">N� de Caja</TD>
					<TD class="Text" width="1%">:</TD>
					<TD width="59%"><asp:label id="lblNroCaja" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text">Tipo Documento</TD>
					<TD class="Text">:</TD>
					<TD><asp:label id="lblTipoDoc" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text">Divisi�n Desde/Hasta</TD>
					<TD class="Text">:</TD>
					<TD><asp:label id="lblDivision" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text">Solicitante</TD>
					<TD class="Text">:</TD>
					<TD><asp:label id="lblSolicitante" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text">Solicitado el</TD>
					<TD class="Text">:</TD>
					<TD><asp:label id="lblSolicitado" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text">N� Requerimiento</TD>
					<TD class="Text">:</TD>
					<TD><asp:label id="lblNroRequerimiento" runat="server" CssClass="Text"></asp:label></TD>
				</TR>
				<TR>
					<TD class="Text" colSpan="3" height="10"></TD>
				</TR>
				<TR>
					<TD class="Text" align="center" colSpan="3"><INPUT style="WIDTH: 80px" type="button" value="Cerrar" onclick="window.close()" class="Text"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
