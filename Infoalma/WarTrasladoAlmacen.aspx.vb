Imports Depsa.LibCapaNegocio
Imports Library.AccesoBL
Imports Library.AccesoDB
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data
Imports Infodepsa

Public Class WarTrasladoAlmacen
    Inherits System.Web.UI.Page
    Private objMercaderia As New clsCNMercaderia
    Private objConexion As SqlConnection
    Private objTransaccion As SqlTransaction
    Private objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objWarrant As clsCNWarrant
    Private objReporte As New Reportes
    Private objEntidad As Entidad = New Entidad

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnSolicitar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnSolicitar2 As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lnkAlmacen As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboAlmacenOrigen As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "34") Then

            Try
                Me.lblOpcion.Text = "Traslado de Mercader�a"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "34"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    loadDepositante()
                    If Session.Item("IdTipoEntidad") = "03" Then
                        Me.cboDepositante.SelectedValue = Session.Item("IdSico")
                        Me.cboDepositante.Enabled = False
                        Session.Add("CodCliente", Me.cboDepositante.SelectedValue)
                    Else
                        Me.cboDepositante.Enabled = True
                    End If
                    GetCargarTipoMercaderia()
                    GetAlmacenxCliente()
                    Me.btnSolicitar.Attributes.Add("onclick", "javascript:if(confirm('Est� seguro de solicitar el traslado?')== false) return false;")
                    Me.btnSolicitar2.Attributes.Add("onclick", "javascript:if(confirm('Est� seguro de solicitar el traslado?')== false) return false;")
                    If Session("IdTipoEntidad") = "01" Then
                        Me.lnkAlmacen.Visible = True
                        Me.lnkAlmacen.Attributes.Add("onclick", "window.open('WarAlmacenXCliente.aspx',null,'left=240,top=200,width=420,height=400,toolbar=0,resizable=0');")
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadDepositante()
        Dim dtEntidad As DataTable
        Try
            Me.cboDepositante.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("03", "")
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositante.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub GetCargarTipoMercaderia()
        Me.cboModalidad.Items.Clear()
        Try
            objWarrant = New clsCNWarrant
            Dim dt As DataTable
            With objWarrant
                dt = .gCNGetListarTipoWarrant(Session("CoEmpresa"), Me.cboDepositante.SelectedValue, "03", "S")
            End With
            For Each dr As DataRow In dt.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI").trim, dr("CO_MODA_MOVI").trim))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = CType(Session.Item("dtMovimiento"), DataView)
        Me.dgResultado.DataBind()
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dr As DataRowView
            Dim dt As DataTable
            objMercaderia = New clsCNMercaderia
            Dim CodClienteGlobalMotors As String

            dt = objMercaderia.gCNGetListarInventarioGlabal(Session("CoEmpresa"), Me.cboDepositante.SelectedValue,
                                            Session.Item("IdTipoEntidad"), Session.Item("IdSico"),
                                            Me.cboReferencia.SelectedValue, Trim(Me.txtNroReferencia.Text),
                                            Me.cboModalidad.SelectedValue, Me.cboEstado.SelectedValue, Me.cboAlmacenOrigen.SelectedValue)

            Session.Add("dtMovimiento", dt)
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Me.lblMensaje.Text = "Se encontraron " & dt.Rows.Count & " registros"
            If dt.Rows.Count > 0 Then
                If Session.Item("IdTipoEntidad") = "03" Then
                    Me.btnSolicitar.Visible = True
                    Me.btnSolicitar2.Visible = True
                End If
                If Session.Item("IdTipoEntidad") = "02" Then
                    Me.btnSolicitar.Visible = False
                    Me.btnSolicitar2.Visible = False
                End If
                If Session.Item("IdTipoEntidad") = "01" Then
                    Me.btnSolicitar.Visible = False
                    Me.btnSolicitar2.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim chkSeleccion As CheckBox
        Dim cboAlmacen As DropDownList
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            chkSeleccion = CType(e.Item.FindControl("chkSeleccion"), System.Web.UI.WebControls.CheckBox)
            cboAlmacen = CType(e.Item.FindControl("cboAlmacen"), System.Web.UI.WebControls.DropDownList)

            If e.Item.Cells(12).Text = "09" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                If Session("IdTipoEntidad") = "03" And Session("IdTipoUsuario") = "03" Then
                    chkSeleccion.Enabled = True
                    cboAlmacen.Enabled = True
                End If
            End If

            If e.Item.Cells(12).Text = "10" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")
                chkSeleccion.Enabled = False
                cboAlmacen.Enabled = False
            End If

            If e.Item.Cells(12).Text = "11" Then
                e.Item.BackColor = System.Drawing.Color.FromName("#c8dcf0")
                If Session("IdTipoEntidad") = "01" Then
                    chkSeleccion.Enabled = True
                    cboAlmacen.Enabled = False
                End If
            End If
        End If
    End Sub

    Public Function GetAlmacen() As DataTable
        Return objMercaderia.gCNGetListarAlmacen(Me.cboDepositante.SelectedValue)
    End Function

    Public Sub GetAlmacenxCliente()
        Dim dt As DataTable
        dt = objMercaderia.gCNGetListarAlmacen(Me.cboDepositante.SelectedValue)
        Me.cboAlmacenOrigen.Items.Clear()
        For Each dr As DataRow In dt.Rows
            Me.cboAlmacenOrigen.Items.Add(New ListItem(dr("DSC_ALMA").trim, dr("COD_ALMA").trim))
        Next
    End Sub

    Private Sub btnSolicitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSolicitar.Click
        Solicitar()
    End Sub

    Private Sub Solicitar()
        objMercaderia = New clsCNMercaderia
        Dim strScript As String
        Dim strBody As String
        Dim strCodFinanciador As String = ""
        For Each dgItem As DataGridItem In Me.dgResultado.Items
            Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
            Dim myCboAlmacen As DropDownList = DirectCast(dgItem.FindControl("cboAlmacen"), DropDownList)
            If myCheckbox.Checked And myCboAlmacen.SelectedIndex = 0 Then
                strScript = "<script language='javascript'> alert('Seleccione un almac�n destino valido'); </script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If
        Next
        objConexion = New SqlConnection(strCadenaConexion)
        objConexion.Open()
        objTransaccion = objConexion.BeginTransaction
        Try
            strBody = "<table border=1 style='FONT-FAMILY: Tahoma; FONT-SIZE: 10pt' width='600px'><tr><td>NRO WARRANT</td><td>NRO �TEM</td><td>DESCRIPCI�N</td><td>ALMAC�N DESTINO</td></tr>"
            For Each dgItem As DataGridItem In Me.dgResultado.Items
                Dim myCheckbox As CheckBox = DirectCast(dgItem.FindControl("chkSeleccion"), CheckBox)
                Dim myCboAlmacen As DropDownList = DirectCast(dgItem.FindControl("cboAlmacen"), DropDownList)
                If myCheckbox.Checked And myCboAlmacen.SelectedIndex > 0 Then
                    strCodFinanciador = dgItem.Cells(11).Text
                    objMercaderia.gCNCrearTranladoGlobal(Session.Item("CoEmpresa"), dgItem.Cells(13).Text,
                    dgItem.Cells(10).Text, CInt(dgItem.Cells(2).Text), dgItem.Cells(1).Text,
                    dgItem.Cells(14).Text, CType(dgItem.FindControl("cboAlmacen"), DropDownList).SelectedItem.Value,
                    "10", Session.Item("IdUsuario"), objTransaccion)
                    strBody += "<tr><td>" & dgItem.Cells(1).Text & "</td><td>" & dgItem.Cells(2).Text & "</td><td>" & dgItem.Cells(3).Text & "</td><td>" & myCboAlmacen.SelectedItem.Text & "</td></tr>"
                End If
            Next
            objTransaccion.Commit()
            objTransaccion.Dispose()
            strScript = "<script language='javascript'> alert('Se solicit� correctamente'); </script>"
            'Page.RegisterStartupScript("Open", strScript)
            ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
            strBody += "</table>"

            strBody = "<FONT face='Tahoma' size='2'>Estimados Se�ores:<br><br>" &
                    Session("NombreEntidad") & " ha solicitado el(los) siguiente(s) traslado(s) :</FONT><br><br>" & strBody &
                    "<br><FONT face='Tahoma' size='2'>Depositante: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & Session("NombreEntidad") & "</FONT></STRONG>" &
                    "<br><FONT face='Tahoma' size='2'>Solicitado por: </FONT><STRONG><FONT color='#330099' face='Tahoma' size='2'>" & UCase(Session("UsuarioLogin")) & "</FONT></STRONG>" &
                    "<br><FONT face='Tahoma' size='2'>Alma Per� supervisar� el traslado de las unidades solicitadas.</FONT><br><br><FONT face='Tahoma' size='2'>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A></FONT>"

            objfunciones.EnviaMailTraslado("00000001", Session.Item("IdSico"), strCodFinanciador, " Solicitud de traslado - " & Session("NombreEntidad"), strBody)
            Bindatagrid()
        Catch ex As Exception
            objTransaccion.Rollback()
            objTransaccion.Dispose()
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnSolicitar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSolicitar2.Click
        Solicitar()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgExport.DataSource = Session("dtMovimiento")
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Private Sub cboDepositante_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepositante.SelectedIndexChanged
        GetCargarTipoMercaderia()
        GetAlmacenxCliente()
        Session.Add("CodCliente", Me.cboDepositante.SelectedValue)
    End Sub
End Class
