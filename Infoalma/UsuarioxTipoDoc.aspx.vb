Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class UsuarioxTipoDoc
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNombres As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboCliente As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblEtiqueta As System.Web.UI.WebControls.Label
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents Cliente As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents Nombres As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents cboDepartamento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents Departamento As System.Web.UI.HtmlControls.HtmlTableCell
    'Protected WithEvents Departamento1 As System.Web.UI.HtmlControls.HtmlTableCell
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "27") Then
            Me.lblError.Text = ""
            Try
                If Not IsPostBack Then
                    Me.lblOpcion.Text = "Usuario por Tipo de Documento"
                    Me.txtNombres.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "27"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    loadTipoEntidad()
                    loadEntidad()
                    loadDepartamento()
                    Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                    If Session.Item("IdTipoEntidad") <> "01" Then
                        Me.cboTipoEntidad.SelectedValue = Session.Item("IdTipoEntidad")
                        Me.cboTipoEntidad.Enabled = False
                        loadEntidad()
                        Me.cboEntidad.SelectedValue = Session.Item("IdSico")
                        Me.cboEntidad.Enabled = False
                        Me.btnGrabar.Enabled = False
                    End If
                    BindDatagrid()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objUsuario.gGetBusquedaUsuarioxTipoDocumento(Me.cboTipoEntidad.SelectedValue, Me.cboEntidad.SelectedValue, Me.txtNombres.Text, Me.cboCliente.SelectedValue, Me.cboDepartamento.SelectedValue)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            If Me.cboTipoEntidad.SelectedValue = "01" Then
                Me.dgdResultado.Columns(1).Visible = True
                Me.dgdResultado.Columns(2).Visible = False
                Me.dgdResultado.Columns(3).Visible = False
            Else
                Me.dgdResultado.Columns(1).Visible = False
                Me.dgdResultado.Columns(2).Visible = True
                Me.dgdResultado.Columns(3).Visible = True
            End If
            Me.lblMensaje.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 0)
    End Sub

    Private Sub loadDepartamento()
        objFunciones.GetDepartamento(Me.cboDepartamento)
    End Sub

    Private Sub loadUsuarioAGD()
        objFunciones.GetUsuarioAGD(Me.cboTipoEntidad.SelectedValue, Me.cboEntidad)
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As DataTable
        Dim i As Integer
        Try
            Me.Nombres.Visible = True
            Me.cboEntidad.Items.Clear()
            If Me.cboTipoEntidad.SelectedValue = "01" Then
                Me.cboEntidad.Width = System.Web.UI.WebControls.Unit.Pixel(300)
                Me.Nombres.Visible = False
                Me.Departamento.Visible = True
                Me.Departamento1.Visible = True
                Me.lblEtiqueta.Text = "Usuario :"
                loadUsuarioAGD()
            Else
                Me.cboEntidad.Width = System.Web.UI.WebControls.Unit.Pixel(450)
                Me.Departamento.Visible = False
                Me.Departamento1.Visible = False
                Me.lblEtiqueta.Text = "Entidad :"
                If Me.cboTipoEntidad.SelectedValue = "02" Then
                    dtEntidad = objEntidad.gGetEntidades(Me.cboTipoEntidad.SelectedValue, "W")
                Else
                    dtEntidad = objEntidad.gGetEntidades(Me.cboTipoEntidad.SelectedValue, "")
                    Me.cboEntidad.Items.Add(New ListItem("---------------Todos---------------", "0"))
                End If

                For i = 0 To dtEntidad.Rows.Count - 1
                    Me.cboEntidad.Items.Add(New ListItem(dtEntidad.Rows(i)("NOM_ENTI"), dtEntidad.Rows(i)("COD_SICO")))
                Next
            End If
            If Me.cboTipoEntidad.SelectedValue = "02" Then
                Me.cboEntidad.AutoPostBack = True
                Me.Cliente.Visible = True
                Me.cboCliente.Items.Clear()

                dtEntidad = objEntidad.gGetClientesxFinanciador(Me.cboEntidad.SelectedValue)
                For i = 0 To dtEntidad.Rows.Count - 1
                    Me.cboCliente.Items.Add(New ListItem(dtEntidad.Rows(i)("NOM_ENTI"), dtEntidad.Rows(i)("COD_SICO")))
                Next
            Else
                Me.cboEntidad.AutoPostBack = False
                Me.Cliente.Visible = False
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub cboTipoEntidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoEntidad.SelectedIndexChanged
        loadEntidad()
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
        End If

        If e.Item.ItemType = ListItemType.Header Then
            Dim ck1 As CheckBox = CType(e.Item.FindControl("chkHTitulo"), CheckBox)
            If Not IsNothing(ck1) Then
                ck1.Attributes.Add("onclick", "Todos(this, 'chkTitulo');")
            End If
            Dim ck2 As CheckBox = CType(e.Item.FindControl("chkHLiberacion"), CheckBox)
            If Not IsNothing(ck2) Then
                ck2.Attributes.Add("onclick", "Todos(this, 'chkLiberacion');")
            End If
            Dim ck3 As CheckBox = CType(e.Item.FindControl("chkHSimple"), CheckBox)
            If Not IsNothing(ck3) Then
                ck3.Attributes.Add("onclick", "Todos(this, 'chkSimple');")
            End If
            Dim ck4 As CheckBox = CType(e.Item.FindControl("chkHAduana"), CheckBox)
            If Not IsNothing(ck4) Then
                ck4.Attributes.Add("onclick", "Todos(this, 'chkAduana');")
            End If
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "M", "27", "CONFIGURAR USUARIO X TIPO DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        If Me.dgdResultado.Items.Count = 0 Then
            Me.lblError.Text = "No hay registros para grabar, efectue otra busqueda"
        Else
            Me.lblError.Text = objFunciones.InsertarSecuencia(Me.dgdResultado, Me.cboEntidad.SelectedValue, Me.cboCliente.SelectedValue, Session("IdUsuario"))
            BindDatagrid()
        End If
    End Sub

    Private Sub cboEntidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEntidad.SelectedIndexChanged
        Dim dtEntidad As DataTable
        Dim i As Integer
        Me.cboCliente.Items.Clear()

        dtEntidad = objEntidad.gGetClientesxFinanciador(Me.cboEntidad.SelectedValue)
        For i = 0 To dtEntidad.Rows.Count - 1
            Me.cboCliente.Items.Add(New ListItem(dtEntidad.Rows(i)("NOM_ENTI"), dtEntidad.Rows(i)("COD_SICO")))
        Next
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
