Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class CueCancelaciones
    Inherits System.Web.UI.Page
    Dim sNO_DOCU, sFE_INIC, sFE_FINA As String
    Private objCCorrientes As clsCNCCorrientes
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtDesde As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents btnImprimir As System.Web.UI.HtmlControls.HtmlButton
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboTipoDocu As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado1 As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        Me.lblOpcion.Text = "Relaci�n de Cancelaciones"
        Session("sPA_PRIN") = ""
        Session("sNO_PAGI_ANTE") = "CueCancelaciones.ASPX"

        If Not Page.IsPostBack = True Then
            Session.Add("dtCueCanc", Nothing)
            Session.Add("dtCueCanTot", Nothing)
            Session.Add("TI_DOC", "")
            Dim strResult As String()
            Dim Dia As String
            Dim Mes As String
            Dim Anio As String
            strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "CANCELACIONES"), "-")
            Session.Item("Page") = strResult(0)
            Session.Item("Opcion") = strResult(1)
            Inicializar() 'Muestra las fechas en el formulario
            getMostrarTipoDocumento() 'llena la lista con los tipos de documentos

            pg_DATA_LIMP(Me.dgResultado)
            pg_DATA_LIMP(Me.dgResultado1)


            If Request.QueryString("sST_CONS") = "S" Then
                Me.cboTipoDocu.Items.FindByValue(Me.cboTipoDocu.SelectedItem.Value).Selected = False
                Me.cboTipoDocu.Items.FindByValue(Request.QueryString("sTI_DOCU")).Selected = True
                Me.txtFechaInicial.Value = Request.QueryString("sFE_EMIS_INIC")
                Me.txtFechaFinal.Value = Request.QueryString("sFE_EMIS_FINA")
                BuscarDocumentos()
            End If
            BuscarDocumentos()
            Dia = "00" + CStr(Now.Day)
            Mes = "00" + CStr(Now.Month)
            Anio = "0000" + CStr(Now.Year)
            Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
        End If
    End Sub

    Private Sub BindDataGrid()
        objCCorrientes = New clsCNCCorrientes
        Dim dt As DataTable
        Dim dttotal As New DataTable
        dttotal.Columns.Add(New DataColumn("TOT_SOL", GetType(Double)))
        dttotal.Columns.Add(New DataColumn("TOT_DOL", GetType(Double)))
        Dim drtotales As DataRow
        Dim inttotsol As Double = 0.0
        Dim inttotdol As Double = 0.0
        Dim i As Integer = 0
        Me.lblError.Text = ""
        Try
            With objCCorrientes
                .gCADGetCancelaciones(Session.Item("CoEmpresa"), Session.Item("IdSico"), Me.cboTipoDocu.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value)
                dt = .fn_devuelvedatatable
            End With
            Session.Item("dtCueCanc") = dt
            For Each dr As DataRow In dt.Rows
                inttotsol = inttotsol + dt.Rows(i)("SOLES")
                inttotdol = inttotdol + dt.Rows(i)("DOLARES")
                i = i + 1
            Next
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()

            If dgResultado.CurrentPageIndex = 0 Then
                drtotales = dttotal.NewRow
                drtotales("TOT_SOL") = FormatNumber(inttotsol, 2)
                drtotales("TOT_DOL") = FormatNumber(inttotdol, 2)
                dttotal.Rows.Add(drtotales)

                dgResultado1.DataSource = dttotal
                Session.Item("dtCueCanTot") = dttotal
                dgResultado1.DataBind()
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub Inicializar()
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dim dttFecha As DateTime

        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
    End Sub

    Private Sub getMostrarTipoDocumento()
        objCCorrientes = New clsCNCCorrientes
        Dim dt As DataTable
        Try
            With objCCorrientes
                .gCNGetTipoDocumentos()
                dt = .fn_devuelvedatatable
                Me.cboTipoDocu.Items.Add(New ListItem("Todos", ""))
                For Each dr As DataRow In dt.Rows
                    Me.cboTipoDocu.Items.Add(New ListItem(dr("descripcion"), dr("codigo")))
                Next
            End With
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BuscarDocumentos()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "CANCELACIONES", "CONSULTA DE CANCELACIONES", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Public Sub Change_Page(ByVal Src As System.Object, ByVal Args As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgResultado.PageIndexChanged
        dgResultado.CurrentPageIndex = Args.NewPageIndex
        BindDataGrid()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        BuscarDocumentos()
        Dim response As HttpResponse = HttpContext.Current.Response

        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Cancelaciones.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtCueCanc"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Private Sub BuscarDocumentos()
        dgResultado.CurrentPageIndex = 0

        pg_DATA_LIMP(Me.dgResultado)
        pg_DATA_LIMP(Me.dgResultado1)
        BindDataGrid()
    End Sub

    Public Sub pg_DATA_LIMP(ByVal pDG_GENE As DataGrid)
        Dim table As New DataTable
        Dim ds As New DataSet
        ds.Tables.Add(table)
        pDG_GENE.DataSource = ds
        pDG_GENE.DataBind()
    End Sub


    ReadOnly Property pNO_DOCU()
        Get
            Return cboTipoDocu.SelectedItem.Text
        End Get
    End Property

    ReadOnly Property pTI_DOCU()
        Get
            Return cboTipoDocu.SelectedItem.Value
        End Get
    End Property

    ReadOnly Property pFE_EMIS_INIC()
        Get
            Return Me.txtFechaInicial.Value
        End Get
    End Property

    ReadOnly Property pFE_EMIS_FINA()
        Get
            Return Me.txtFechaFinal.Value
        End Get
    End Property

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("TI_DOCU_COBR")) = "FAC" Or CStr(e.Item.DataItem("TI_DOCU_COBR")) = "NDE" Or CStr(e.Item.DataItem("TI_DOCU_COBR")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("CO_UNID")) & "','" & CStr(e.Item.DataItem("TI_DOCU_COBR")).Trim & "','" & CStr(e.Item.DataItem("NU_DOCU_COBR")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("TI_DOCU_COBR")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK

                    objCCorrientes = New clsCNCCorrientes
                    Dim dt As DataTable
                    dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("CO_UNID")), CStr(e.Item.DataItem("TI_DOCU_COBR")).Trim, CStr(e.Item.DataItem("NU_DOCU_COBR")))
                    If dt.Rows.Count <> 0 Then
                        CType(e.Item.FindControl("imgVer"), ImageButton).ImageUrl = "Images/Ver.jpg"
                        CType(e.Item.FindControl("imgVer"), ImageButton).ToolTip = "Ver Abono"
                        CType(e.Item.FindControl("imgVer"), ImageButton).Enabled = True
                        CType(e.Item.FindControl("imgVer"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("CO_UNID")) & "','" & CStr(e.Item.DataItem("TI_DOCU_COBR")).Trim & "','" & CStr(e.Item.DataItem("NU_DOCU_COBR")) & "')")
                    Else
                        CType(e.Item.FindControl("imgVer"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                        CType(e.Item.FindControl("imgVer"), ImageButton).ToolTip = "Sin Abono"
                        CType(e.Item.FindControl("imgVer"), ImageButton).Enabled = False
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("TI_DOCU_COBR"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub
End Class
