Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.Data
Imports Infodepsa

Public Class DocumentoFirmadoPedido
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objDocumento As Documento = New Documento
    Private objRetiro As clsCNRetiro = New clsCNRetiro
    Private objWMS As clsCNWMS
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPathDownload = ConfigurationManager.AppSettings.Item("RutaDownload")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_EMITIDO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                                    Session.Item("NombreEntidad"), "C", "1", "FIRMAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Inicializa()
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        Dim strVar As String
        Dim strPagina As String
        strVar = Request.QueryString("var")
        strPagina = Request.QueryString("Pagina")
        Dim strEntidadFinanciera As String
        Dim strArchivoFirmado As String
        Dim strContrasena As String
        Dim strContrasenaClave As String
        Dim strNuevaContrasena As String
        Dim strNuevaContrasenaClave As String
        Dim contenido As Byte()
        objWMS = New clsCNWMS
        If strVar = "3" Then
            Try
                Dim dtMail As New DataTable
                Dim strEmails As String
                Dim strTipoDocumento As String
                Dim objUsuario As Usuario = New Usuario

                Select Case Session.Item("IdTipoDocumento")
                    Case "22"
                        strTipoDocumento = "Simple"
                    Case "23"
                        strTipoDocumento = "Aduanero"
                    Case "25"
                        strTipoDocumento = "Simple"
                End Select

                objDocumento.gUpdRechazarDocumentoRetiro(Session.Item("IdUsuario"), Session.Item("NroPedido"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
                If objDocumento.strResultado = "X" Then
                    Session.Add("MensajePedido", "No se pudo rechazar el documento")
                    Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                    Exit Sub
                End If

                dtMail = objUsuario.gGetEmailAprobadores("00000001", Session.Item("CodAlmacen"), strTipoDocumento)

                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                End If

                objFunciones.SendMail("Alma Per� � Sistema AFI <" & ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                        "Solicitud de rechazo para pedido del WMS - " & Session.Item("NombreEntidad"),
                        "Estimados Se�ores:<br><br>" &
                        "El usuario " & Session.Item("UsuarioLogin") & " solicit� el rechazo del pedido N�mero: <STRONG><FONT color='#330099'> " & Session.Item("NroPedido") &
                        "</FONT></STRONG><br><br>Cliente: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                        "<br><br> Atte.<br><br>S�rvanse ingresar a la web AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")

                Session.Add("MensajePedido", "Se rechaz� correctamente")
                objAccesoWeb = New clsCNAccesosWeb
                objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                                            Session.Item("NombreEntidad"), "C", "RETIROS_LINEA", "RECHAZAR RETIRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
                Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                Exit Sub
            Catch ex As Exception
                Session.Add("MensajePedido", "ERROR " + ex.Message)
                Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                Exit Sub
            End Try
        End If
        '--------------------------------------------
        ' Dim SignedData As CAPICOM.SignedData
        '--------------------------------------------
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim strMSG As String
        Dim dtRetiro As DataTable
        Dim strNroRetiro As String
        Dim strNombreReporte As String
        Dim dttxt As New DataTable
        Dim nsecu As Integer
        If Session("IdTipoUsuario") = "03" Then 'SI ES APROBADOR
            strContrasenaClave = Request.Form("txtContrasena")
            strNuevaContrasenaClave = Request.Form("txtNuevaContrasena")
            strContrasena = Funciones.EncryptString(strContrasenaClave.Trim, "���_[]0")
            strNuevaContrasena = Funciones.EncryptString(strNuevaContrasenaClave.Trim, "���_[]0")
            strMSG = objDocumento.gGetValidaContrasena(Session.Item("IdUsuario"), strContrasena, strNuevaContrasena, "", objTrans)
            Select Case strMSG
                Case "I"
                    If strNuevaContrasena <> "" Then
                        Session.Add("strContrasenia", strNuevaContrasenaClave)
                    Else
                        Session.Add("strContrasenia", strContrasenaClave)
                    End If
                    Session.Add("strfecha", Now)
                    Exit Select
                Case "C"
                    Session.Add("MensajePedido", "La contrase�a anterior no es correcta")
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                    Exit Sub
                Case "X"
                    Session.Add("MensajePedido", "La contrase�a ingresada no es correcta")
                    Session.Remove("strcontrasenia")
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                    Exit Sub
            End Select
            'nsecu = 2
        End If

        If strVar = "2" Then
            strMSG = objFunciones.gFirmarDocumento(Session.Item("IdUsuario"), Session.Item("UsuarioLogin"), Session.Item("NroPedido"), Session.Item("NroLib"),
                                                   Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session("CertDigital"), Session("Cod_Doc"), Session("Cod_TipOtro"),
                                                   Session.Item("IdSico"), Session.Item("IdTipoEntidad"), Session.Item("Cod_Unidad"), contenido, strVar, objTrans)


            If strMSG = "X" Then
                Session.Add("MensajePedido", "Error al firmar el documento")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
            '-----------------------------------------------------
            dttxt = objWMS.gCNGENE_SALI_INFO_WMS(Session.Item("NroPedido"), objTrans)
            If dttxt Is Nothing Then
                Session.Add("MensajePedido", "No se gener� el archivo de texto")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
            If dttxt.Rows.Count > 2 Then
                If CreateTextDelimiterFile(strPathDownload & "O" & Session.Item("NroPedido") & ".txt", dttxt, "*", False, False, strNroRetiro) = False Then
                    Session.Add("MensajePedido", "No se pudo generar el pedido")
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
                    Exit Sub
                End If
            End If
            'End If
            'Session.Add("MensajePedido", "Se gener� el pedido n�mero " & strNroRetiro)
            'objTrans.Commit()
            'objTrans.Dispose()
            'Session.Remove("dtWMS")
            '-------------------------------------------------------
            Session.Add("MensajePedido", strMSG)
            objTrans.Commit()
            objTrans.Dispose()
            Response.Redirect("DocumentoAprobacionRetiro_W.aspx?var=" & strVar, False)
            Exit Sub
        End If

        Try
            'If Session("IdTipoUsuario") = "01" Then
            'nsecu = 1
            'End If
            strNroRetiro = objWMS.gCNGenNumRetiro(Session.Item("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdUsuario"), _
                            Request.Form("txtClienteDespacho"), Request.Form("txtFechaRecojo"), Request.Form("txtReferencia"), Request.Form("txtRetitradoPor"), _
                            Session.Item("Co_AlmaW"), Session("IdTipoUsuario"), objTrans)
            If strNroRetiro = "-1" Then
                Session.Add("MensajePedido", "No se pudo crear el pedido")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
            If objWMS.gCNAddItemRetiroWMS(Session.Item("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdUsuario"), _
                                            Session.Item("Co_AlmaW"), CType(Session("dtWMS"), DataTable), strNroRetiro, objTrans) = False Then
                Session.Add("MensajePedido", "No se pudo adicionar los �tems seleccionados")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
            If objWMS.gCNValidarDeuda(strNroRetiro, objTrans) <> "" Then
                Session.Add("MensajePedido", "Favor de contactarse con el �rea de cobranza")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                Exit Sub
            End If

            'If nsecu = 2 Then 'Si firma
            dttxt = objWMS.gCNGENE_SALI_INFO_WMS(strNroRetiro, objTrans)
            If dttxt Is Nothing Then
                Session.Add("MensajePedido", "No se gener� el archivo de texto")
                objTrans.Rollback()
                objTrans.Dispose()
                Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                Exit Sub
            End If
            If dttxt.Rows.Count > 2 Then
                If CreateTextDelimiterFile(strPathDownload & "O" & strNroRetiro & ".txt", dttxt, "*", False, False, strNroRetiro) = False Then
                    Session.Add("MensajePedido", "No se pudo generar el pedido")
                    objTrans.Rollback()
                    objTrans.Dispose()
                    Response.Redirect("AlmRetiroSolicitud_W.aspx?var=" & strVar, False)
                    Exit Sub
                End If
            End If
            'End If
            Session.Add("MensajePedido", "Se gener� el pedido n�mero " & strNroRetiro)
            objTrans.Commit()
            objTrans.Dispose()
            Session.Remove("dtWMS")
            Response.Redirect("AlmPedidoNuevo_W.aspx?var=" & strVar, False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Response.Write(ex.Message)
        Finally
            objConexion = Nothing
        End Try
    End Sub

    Private Function CreateTextDelimiterFile(ByVal fileName As String, _
                                         ByVal dt As DataTable, _
                                         ByVal separatorChar As Char, _
                                         ByVal hdr As Boolean, _
                                         ByVal textDelimiter As Boolean, _
                                         ByVal strNroRetiro As String) As Boolean

        If (fileName = String.Empty) OrElse _
           (dt Is Nothing) Then Throw New System.ArgumentException("Argumentos no v�lidos.")

        If (IO.File.Exists(fileName)) Then
            'If (MessageBox.Show("Ya existe un archivo de texto con el mismo nombre." & Environment.NewLine & _
            '                   "�Desea sobrescribirlo?", _
            '                   "Crear archivo de texto delimitado", _
            '                   MessageBoxButtons.YesNo, _
            '                   MessageBoxIcon.Information) = DialogResult.No) Then 
            'Return False
        End If
        Dim sw As System.IO.StreamWriter

        Try
            Dim col As Integer = 0
            Dim value As String = String.Empty
            ' Creamos el archivo de texto con la codificaci�n por defecto.
            sw = New IO.StreamWriter(fileName, False, System.Text.Encoding.Default)

            If (hdr) Then
                ' La primera l�nea del archivo de texto contiene
                ' el nombre de los campos.
                For Each dc As DataColumn In dt.Columns
                    If (textDelimiter) Then
                        ' Incluimos el nombre del campo entre el caracter
                        ' delimitador de texto especificado.
                        value &= """" & dc.ColumnName & """" & separatorChar
                    Else
                        ' No se incluye caracter delimitador de texto alguno.
                        value &= dc.ColumnName & separatorChar
                    End If
                Next
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
            End If

            ' Recorremos todas las filas del objeto DataTable
            ' incluido en el conjunto de datos.
            For Each dr As DataRow In dt.Rows
                For Each dc As DataColumn In dt.Columns
                    If ((dc.DataType Is System.Type.GetType("System.String")) And _
                       (textDelimiter = True)) Then
                        ' Incluimos el dato alfanum�rico entre el caracter
                        ' delimitador de texto especificado.
                        value &= """" & dr.Item(col).ToString & """" & separatorChar
                    Else
                        ' No se incluye caracter delimitador de texto alguno
                        value &= dr.Item(col).ToString & separatorChar
                    End If
                    ' Siguiente columna
                    col += 1
                Next
                ' Al escribir los datos en el archivo, elimino el
                ' �ltimo car�cter delimitador de la fila.
                sw.WriteLine(value.Remove(value.Length - 1, 1))
                value = String.Empty
                col = 0
            Next ' Siguiente fila
            sw.Close()
            ' Se ha creado con �xito el archivo de texto.
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        Finally
            sw = Nothing
        End Try
    End Function

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
