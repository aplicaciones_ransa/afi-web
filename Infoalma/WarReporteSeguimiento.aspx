<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarReporteSeguimiento.aspx.vb" Inherits="WarReporteSeguimiento" %>

<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Reporte de Seguimiento</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {

            if (parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value)) {
                alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
                document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
                return;
            }


            document.getElementById(Monto).value = parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="660" align="center">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                        <td>
                                            <table id="Table4" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="text">Nro.Solicitud:</td>
                                                    <td>
                                                        <asp:TextBox Style="z-index: 0" ID="txtNroWarrant" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                    <td class="text">Desde:</td>
                                                    <td class="text">
                                                        <input style="z-index: 0" id="txtFechaInicial" class="text" onkeypress="validarcharfecha()"
                                                            onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="6" name="txtFechaInicial" runat="server"><input style="z-index: 0" id="btnFechaInicial" class="Text" 
                                                                value="..." type="button" name="btnFecha"></td>
                                                    <td class="text">Hasta:</td>
                                                    <td>
                                                        <input style="z-index: 0" id="txtFechaFinal" class="text" onkeypress="validarcharfecha()"
                                                            onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="6" name="txtFechaFinal"
                                                            runat="server"><input id="btnFechaFinal" class="Text"  value="..."
                                                                type="button" name="btnFecha"></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkPendientes" runat="server" CssClass="Text" Text="Solo Pendientes"></asp:CheckBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Depositante:</td>
                                                    <td colspan="7">
                                                        <asp:DropDownList ID="cboDepositante" runat="server" CssClass="Text" Width="400px"></asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="text">Financiador:</td>
                                                    <td colspan="7">
                                                        <asp:DropDownList ID="cboFinanciador" runat="server" CssClass="Text" Width="400px"></asp:DropDownList></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td width="80">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="Text" Width="80px" Text="Buscar"></asp:Button></td>
                                        <td width="80">
                                            <asp:Button ID="btnExportar" runat="server" CssClass="Text" Width="80px" Text="Exportar"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    BorderWidth="1px" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" OnPageIndexChanged="Change_Page">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="NOM_CLIE" HeaderText="Cliente"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NUMERO_SOLICITUD" HeaderText="Nro Solicitud">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_SOLICITUD" HeaderText="Fecha Solicitud">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="RESPONSABLE_SOLICITUD" HeaderText="Responsable solicitud"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FCH_FIRM_FINAN_LIB" HeaderText="Fech. Firma Financiador Lib. Nvo Warrant"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NUMERO_TICKET" HeaderText="N&#250;mero ticket"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_TICKET" HeaderText="Fecha ticket"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RESPONSABLE_TICKET" HeaderText="Responsable Ticket"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NUMERO_INVENTARIO" HeaderText="N&#250;mero Inventario"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_APROBACION_INVENTARIO" HeaderText="Fecha aprobaci&#243;n Inventario"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RESPONSABLE_INVENTARIO" HeaderText="Responsable Inventario">
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NUMERO_DCR" HeaderText="N&#250;mero DCR">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_CREACION_DCR" HeaderText="Fecha DCR">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="RESPONSABLE_DCR" HeaderText="Responsable DCR">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="TIPO_TITULO" HeaderText="Tipo T&#237;tulo">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NUMERO_TITULO" HeaderText="Numero T&#237;tulo">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_ENVIO_INFODEPSA" HeaderText="Fecha env&#237;o al AFI">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="RESPONSABLE_ENVIO_INFODEPSA" HeaderText="Responsable env&#237;o al AFI">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_VERIFICACION_DEPSA" HeaderText="Financiador">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_VERIFICACION_CLIENTE" HeaderText="Proviene">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_VERIFICACION_FINANCIADOR" HeaderText="Fecha Aprobaci&#243;n Almacenera">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_APROBACION_CLIENTE" HeaderText="Fecha Aprobaci&#243;n Endoso">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_APROBACION_DEPSA" HeaderText="Fecha Aprobaci&#243;n Cliente"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_APROBACION_FINANCIADOR" HeaderText="Fecha Aprobaci&#243;n Financiador"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FECHA_REGISTRO_INFODEPSA" HeaderText="Fecha Titulo completado"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TIPO" HeaderText="Tipo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="PROVIENE" HeaderText="Nvo. Warrant Generado"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="TIEMPO1" HeaderText="TIEMPO1"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="TIEMPO2" HeaderText="TIEMPO2"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TCREA_DCR" HeaderText="Tiempo creaci&#243;n DCR"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TFIRM_WAR" HeaderText="Tiempo firma Alma Per�"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
