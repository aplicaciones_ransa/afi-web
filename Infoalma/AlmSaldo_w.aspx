<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmSaldo_w.aspx.vb" Inherits="AlmSaldo_w" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Inventario de Mercader�a</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button1").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button2").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });

            $("#txtFechaVenc").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVenc").click(function () {
                $("#txtFechaVenc").datepicker('show');
            });
        });

        function AbrirMovimiento(sNU_RECE, sNU_ITEM, sCO_UNID) {
            var ventana = 'Popup/AlmInventarioDetalle.aspx?sNU_RECE=' + sNU_RECE + '&sNU_ITEM=' + sNU_ITEM + '&sCO_UNID=' + sCO_UNID;
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=550,height=400");
        }

        function fn_Impresion(sFE_INIC, sFE_FINA, sFE_VENC, sCO_ALMA, sDE_ALMA, sCODIGO, sDE_MERC, sLOTE) {
            var ventana = 'Popup/AlmSaldoImpresion_w.aspx?sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sFE_VENC=' + sFE_VENC + '&sCO_ALMA=' + sCO_ALMA + '&sDE_ALMA=' + sDE_ALMA + '&sCODIGO=' + sCODIGO + '&sDE_MERC=' + sDE_MERC + '&sLOTE=' + sLOTE;
            window.open(ventana, "Impresion", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
            //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%" align="center">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="758" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td align="center">
                                                        <table style="z-index: 0" id="Table2" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text" width="18%">Almac�n</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="32%" colspan="9">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboAlmacen" runat="server" CssClass="Text" Width="400px"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="z-index: 0" class="Text" width="18%">Fec.Venc Merc &lt;=</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td style="width: 139px" width="139">
                                                                    <input style="z-index: 0" id="txtFechaVenc" class="text" onkeypress="validarcharfecha()"
                                                                        onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="10" name="txtFechaVenc" runat="server"><input style="z-index: 0" id="btnFechaVenc" class="text"
                                                                            value="..." type="button" name="btnFecha"></td>
                                                                <td class="Text" width="14%">Cod. producto
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td style="z-index: 0; width: 119px" class="Text" width="119" colspan="2">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtCodigo" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                                <td class="Text" width="1%"></td>
                                                                <td style="z-index: 0" class="Text" width="19%" align="left">Descripci�n Merc.</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td width="25%" align="left">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtDescMerc" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" width="18%">Lote</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td style="width: 139px" width="139">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtLote" runat="server" CssClass="Text" Width="140px"></asp:TextBox></td>
                                                                <td class="Text" width="14%">Fecha Del</td>
                                                                <td width="1%">:</td>
                                                                <td style="z-index: 0; width: 0" class="Text"><input style="z-index: 0" id="txtFechaInicial" class="text" onkeypress="validarcharfecha()"
                                                                    onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="10" name="txtFechaInicial" runat="server"></td>
                                                                <td><input style="z-index: 0" id="Button1" class="text" 
                                                                        value="..." type="button" name="btnFechaIni"></td>
                                                                <td class="Text" width="1%" align="right">&nbsp;</td>
                                                                <td class="Text" width="19%" align="left">Al</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td class="Text" width="25%" align="left">
                                                                    <input style="z-index: 0" id="txtFechaFinal" class="text" onkeypress="validarcharfecha()"
                                                                        onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="10" name="txtFechaFinal" runat="server"><input style="z-index: 0" id="Button2" class="text" 
                                                                            value="..." type="button" name="btnFechaFin"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                    <td width="80">
                                                        <input id="btnImprimir" class="btn" onclick="fn_Impresion('<%=txtFechaInicial.Value%>','<%=txtFechaFinal.Value%>','<%=txtFechaVenc.Value%>', '<%=cboAlmacen.SelectedItem.Value%>','<%=cboAlmacen.SelectedItem.Text%>','<%=txtCodigo.Text%>','<%=txtDescMerc.Text%>','<%=txtLote.Text%>')" value="Imprimir" type="button" name="btnImprimir"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AllowPaging="True"
                                                OnPageIndexChanged="Change_Page" BorderColor="Gainsboro" AutoGenerateColumns="False">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="PRODUCTO" HeaderText="Producto">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci&#243;n">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="UNIDAD" HeaderText="Unidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LOTE" HeaderText="Lote">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ESTADO" HeaderText="Estado"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_INGRESO" HeaderText="Fecha Ingreso">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_VENCIMIENTO" HeaderText="Fecha Vencimiento">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
