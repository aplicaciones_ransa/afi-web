<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ValidaCertificado.aspx.vb" Inherits="ValidaCertificado" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ValidaCertificado</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:Header id="Header2" runat="server"></uc1:Header></TD>
				</TR>
				<TR>
					<TD vAlign="top">
						<TABLE id="tblCertificado" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							height="100" width="100%" border="0">
							<TR>
								<TD class="td" align="center">
									<asp:label id="lblError" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:Button id="btnReintentar" runat="server" CssClass="btn" Text="Reintentar" Visible="False"
										Width="80"></asp:Button>
									<asp:Button id="btnAceptar" runat="server" CssClass="btn" Text="Ir al Menu" Width="80"></asp:Button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:Footer id="Footer2" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
