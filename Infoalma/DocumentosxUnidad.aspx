<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentosxUnidad.aspx.vb" Inherits="DocumentosxUnidad" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Documentos por Unidad</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  

			//------------------------------------------ Inicio Validar Fecha
		var primerslap=false; 
		var segundoslap=false; 
		function IsNumeric(valor) 
		{ 
			var log=valor.length; var sw="S"; 
			for (x=0; x<log; x++) 
			{ v1=valor.substr(x,1); 
			v2 = parseInt(v1); 
			//Compruebo si es un valor num�rico 
			if (isNaN(v2)) { sw= "N";} 
			} 
				if (sw=="S") {return true;} else {return false; } 
		} 	
		function formateafecha(fecha) 
		{ 
			var long = fecha.length; 
			var dia; 
			var mes; 
			var ano; 

			if ((long>=2) && (primerslap==false)){dia=fecha.substr(0,2); 
			if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
			else { fecha=""; primerslap=false;} 
			} 
			else 
			{ dia=fecha.substr(0,1); 
			if (IsNumeric(dia)==false) 
			{fecha="";} 
			if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
			} 
			if ((long>=5) && (segundoslap==false)) 
			{ mes=fecha.substr(3,2); 
			if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
			else { fecha=fecha.substr(0,3);; segundoslap=false;} 
			} 
			else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
			if (long>=7) 
			{ ano=fecha.substr(6,4); 
			if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
			else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
			} 

			if (long>=10) 
			{ 
				fecha=fecha.substr(0,10); 
				dia=fecha.substr(0,2); 
				mes=fecha.substr(3,2); 
				ano=fecha.substr(6,4); 
				// A�o no viciesto y es febrero y el dia es mayor a 28 
				if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
			} 
			return (fecha); 
		} 
		//-----------------------------------Fin formatear fecha	
		function Ocultar()
			{
			Estado.style.display='none';			
			}		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" style="WIDTH: 79px; HEIGHT: 497px"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top" style="HEIGHT: 497px">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Titulo1" height="20">DOCUMENTOS POR UNIDAD</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 60px" borderColor="#000000">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" align="left" border="0">
													<TR>
														<TD class="text" style="WIDTH: 88px; HEIGHT: 19px">Nro. de Unidad</TD>
														<TD class="TEXT" style="WIDTH: 9px; HEIGHT: 19px">:</TD>
														<TD style="WIDTH: 168px; HEIGHT: 19px"><asp:textbox id="txtNroMedio" runat="server" CssClass="Text" ReadOnly="True" Width="140px"></asp:textbox></TD>
														<TD style="HEIGHT: 19px"></TD>
													</TR>
													<TR>
														<TD class="text" style="WIDTH: 88px; HEIGHT: 17px">Tipo</TD>
														<TD class="TEXT" style="WIDTH: 9px; HEIGHT: 17px">:</TD>
														<TD style="WIDTH: 168px; HEIGHT: 17px"><asp:textbox id="txtTipo" runat="server" CssClass="Text" ReadOnly="True" Width="180px"></asp:textbox></TD>
														<TD style="HEIGHT: 17px"></TD>
													</TR>
													<TR>
														<TD class="text" style="WIDTH: 88px">�rea</TD>
														<TD class="TEXT" style="WIDTH: 9px">:</TD>
														<TD style="WIDTH: 168px"><asp:textbox id="txtArea" runat="server" CssClass="Text" ReadOnly="True" Width="180px"></asp:textbox></TD>
														<TD></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Resultado :
									<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></TD>
							</TR>
							<TR>
								<TD><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AllowSorting="True" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NU_CONT" HeaderText="Nro.">
												<HeaderStyle Width="10%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="OBSE" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="40%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_CRIT_DESD" HeaderText="Tipo Doc.">
												<HeaderStyle Width="25%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DIVI" HeaderText="Divisi&#243;n Desde/Hasta">
												<HeaderStyle Width="15%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FECHA" HeaderText="Fecha Desde/Hasta">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center"><INPUT class="btn" id="btnAtras" onclick="history.go(-1)" type="button" value="Regresar"
										name="btnAtras"></TD>
							</TR>
							<TR>
								<TD class="td"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td" id="Estado"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
