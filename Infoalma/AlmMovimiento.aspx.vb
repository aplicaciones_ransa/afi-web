Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class AlmMovimiento
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objparametro As clsCNParametro
    Private objRetiro As clsCNRetiro
    Private objKardex As clsCNKardex
    Private objMovimiento As clsCNMovimiento
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    Private objWarrant As clsCNWarrant
    Private objWMS As clsCNWMS
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    ' Protected WithEvents cboCodProducto As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboMovimiento As System.Web.UI.WebControls.DropDownList
    ' Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    'Protected WithEvents dgRetiros As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboReporte As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents dgIngresos As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        PoliticaCache()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MOVIMIENTO_MERCADERI") Then
            Me.lblOpcion.Text = "Movimiento de Mercader�a"
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MOVIMIENTO_MERCADERI"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(365))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                documento()
                GetCargarTipoMercaderia()
                LlenarAlmacenes()
                If Session("Co_AlmaW") = "" Then
                    Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                Else
                    Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                End If
                If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaW"))) > 0 Then
                    Response.Redirect("AlmMovimiento_W.aspx", False)
                    Exit Sub
                End If
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
                If Session.Item("IdTipoEntidad") = "03" And Session.Item("Vencimiento") = 0 Then
                    'Response.Write("<script language='Javascript'>window.open('Popup/AvisoPagarFacturas.aspx','Pago','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=655,height=365');</script>")
                    'Response.Write("<script language='Javascript'>window.open('Popup/Vencimientos.aspx','Vencimientos','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=800,height=600');</script>")
                    Session.Item("Vencimiento") = 1
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub cboMovimiento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMovimiento.SelectedIndexChanged
        documento()
    End Sub

    Private Sub GetCargarTipoMercaderia()
        Me.cboModalidad.Items.Clear()
        Try
            objWarrant = New clsCNWarrant
            Dim dt As DataTable
            With objWarrant
                dt = .gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "N")
            End With
            Me.cboModalidad.Items.Add(New ListItem("------ Todos -------", "0"))
            If dt Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dt.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI").trim, dr("CO_MODA_MOVI").trim))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            'Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub GetCargarReferencias(ByVal strMovimiento As String)
        Try
            objparametro = New clsCNParametro
            With objparametro
                .gCNListarReferencia(cboReferencia, strMovimiento)
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub documento()
        Me.cboEstado.Items.Clear()
        Select Case Me.cboMovimiento.SelectedValue
            Case "I"
                Me.cboEstado.Items.Add(New ListItem("DCR cerrado", "S"))
                Me.cboEstado.Items.Add(New ListItem("DCR en revisi�n", "N"))
                Me.lblFecha.Text = "Fecha Ingreso"
                GetCargarReferencias("4")
            Case "R"
                Me.cboEstado.Items.Add(New ListItem("DOR cerrado", "S"))
                Me.cboEstado.Items.Add(New ListItem("DOR pendiente de despacho", "N"))
                Me.lblFecha.Text = "Fecha Retiro"
                GetCargarReferencias("5")
        End Select
        Buscar()
    End Sub

    Private Sub BindatagridIngreso()
        Me.dgIngresos.Visible = True
        Me.dgRetiros.Visible = False

        Dim dtMovimiento As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtMovimiento = .gCNListarKardex(Session("CoEmpresa"), Session("IdSico"), Me.cboMovimiento.SelectedValue,
                                                Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboReporte.SelectedValue,
                                                Me.cboModalidad.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboEstado.SelectedValue, "S", Me.cboAlmacen.SelectedValue)

                Me.dgIngresos.DataSource = dtMovimiento
                Me.dgIngresos.DataBind()
                Session.Item("dtMovimiento") = dtMovimiento
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridIngresoDetallado()
        Me.dgIngresos.Visible = True
        Me.dgRetiros.Visible = False
        Dim dtMovimiento As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtMovimiento = .gCNListarKardex(Session("CoEmpresa"), Session("IdSico"), Me.cboMovimiento.SelectedValue,
                                                Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboReporte.SelectedValue,
                                                Me.cboModalidad.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboEstado.SelectedValue, "S", Me.cboAlmacen.SelectedValue)

                Me.dgIngresos.DataSource = dtMovimiento
                Me.dgIngresos.DataBind()
                Session.Item("dtMovimiento") = dtMovimiento
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgIngresos.CurrentPageIndex = Args.NewPageIndex
        Me.dgIngresos.DataSource = Session.Item("dtMovimiento")
        Me.dgIngresos.DataBind()
    End Sub

    Sub Change_PageRetiros(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgRetiros.CurrentPageIndex = Args.NewPageIndex
        Me.dgRetiros.DataSource = Session.Item("dtMovimiento")
        Me.dgRetiros.DataBind()
    End Sub

    Private Sub BindatagridRetiroDetallado()
        Me.dgIngresos.Visible = False
        Me.dgRetiros.Visible = True
        Dim dtMovimiento As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtMovimiento = .gCNListarKardex(Session("CoEmpresa"), Session("IdSico"), Me.cboMovimiento.SelectedValue,
                                                Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboReporte.SelectedValue,
                                                Me.cboModalidad.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboEstado.SelectedValue, "S", Me.cboAlmacen.SelectedValue)

                Me.dgRetiros.DataSource = dtMovimiento
                Me.dgRetiros.DataBind()
                Session.Item("dtMovimiento") = dtMovimiento
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridRetiro()
        Me.dgIngresos.Visible = False
        Me.dgRetiros.Visible = True
        Dim dtMovimiento As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtMovimiento = .gCNListarKardex(Session("CoEmpresa"), Session("IdSico"), Me.cboMovimiento.SelectedValue,
                                                Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.cboReporte.SelectedValue,
                                                Me.cboModalidad.SelectedValue, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboEstado.SelectedValue, "S", Me.cboAlmacen.SelectedValue)

                Me.dgRetiros.DataSource = dtMovimiento
                Me.dgRetiros.DataBind()
                Session.Item("dtMovimiento") = dtMovimiento
            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Buscar()
    End Sub

    Private Sub Buscar()
        Me.dgIngresos.CurrentPageIndex = 0
        Me.dgRetiros.CurrentPageIndex = 0
        If Me.cboMovimiento.SelectedValue = "I" And Me.cboReporte.SelectedValue = 1 Then
            BindatagridIngreso()
            Me.dgIngresos.Columns(0).Visible = True
            Me.dgIngresos.Columns(1).Visible = True
            Me.dgIngresos.Columns(2).Visible = True
            Me.dgIngresos.Columns(3).Visible = True
            Me.dgIngresos.Columns(4).Visible = False
            Me.dgIngresos.Columns(5).Visible = False

            Me.dgIngresos.Columns(7).Visible = False
            Me.dgIngresos.Columns(8).Visible = False
            Me.dgIngresos.Columns(9).Visible = False
            Me.dgIngresos.Columns(10).Visible = False

            Me.dgIngresos.Columns(13).Visible = False
            Me.dgIngresos.Columns(14).Visible = False
            Me.dgIngresos.Columns(15).Visible = True
            Me.dgIngresos.Columns(17).Visible = True
            Me.dgIngresos.Columns(19).Visible = False
        ElseIf Me.cboMovimiento.SelectedValue = "I" And Me.cboReporte.SelectedValue = 2 Then
            BindatagridIngresoDetallado()
            Me.dgIngresos.Columns(0).Visible = False
            Me.dgIngresos.Columns(1).Visible = False
            Me.dgIngresos.Columns(2).Visible = False
            Me.dgIngresos.Columns(3).Visible = False
            Me.dgIngresos.Columns(4).Visible = True
            Me.dgIngresos.Columns(5).Visible = False

            Me.dgIngresos.Columns(7).Visible = True
            Me.dgIngresos.Columns(8).Visible = True
            Me.dgIngresos.Columns(9).Visible = True
            Me.dgIngresos.Columns(10).Visible = True

            Me.dgIngresos.Columns(13).Visible = True
            Me.dgIngresos.Columns(14).Visible = True
            Me.dgIngresos.Columns(15).Visible = False
            Me.dgIngresos.Columns(17).Visible = False
            Me.dgIngresos.Columns(19).Visible = True
        ElseIf Me.cboMovimiento.SelectedValue = "R" And Me.cboReporte.SelectedValue = 1 Then
            BindatagridRetiro()
            Me.dgRetiros.Columns(0).Visible = True
            Me.dgRetiros.Columns(1).Visible = True
            Me.dgRetiros.Columns(3).Visible = False
            Me.dgRetiros.Columns(4).Visible = False

            Me.dgRetiros.Columns(5).Visible = False

            Me.dgRetiros.Columns(7).Visible = False
            Me.dgRetiros.Columns(8).Visible = False
            Me.dgRetiros.Columns(9).Visible = False
            Me.dgRetiros.Columns(10).Visible = False

            Me.dgRetiros.Columns(13).Visible = True
            Me.dgRetiros.Columns(15).Visible = True
            Me.dgRetiros.Columns(16).Visible = False
        Else
            BindatagridRetiroDetallado()
            Me.dgRetiros.Columns(0).Visible = False
            Me.dgRetiros.Columns(1).Visible = False
            Me.dgRetiros.Columns(3).Visible = True
            Me.dgRetiros.Columns(4).Visible = True

            Me.dgRetiros.Columns(5).Visible = True

            Me.dgRetiros.Columns(7).Visible = True
            Me.dgRetiros.Columns(8).Visible = True
            Me.dgRetiros.Columns(9).Visible = True
            Me.dgRetiros.Columns(10).Visible = True

            Me.dgRetiros.Columns(13).Visible = False
            Me.dgRetiros.Columns(15).Visible = False
            Me.dgRetiros.Columns(16).Visible = True
        End If
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                Session.Item("NombreEntidad"), "C", "MOVIMIENTO_MERCADERI", "CONSULTAR MOVIMIENTO MERCADERIA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgIngresos_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngresos.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(12).Text <> "&nbsp;" Then
                    e.Item.Cells(11).Font.Bold = True
                    e.Item.Cells(12).Font.Bold = True
                    e.Item.ForeColor = System.Drawing.Color.FromName("#112627")
                    e.Item.BackColor = System.Drawing.Color.FromName("#EAEAEA")
                ElseIf e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(4).Text <> "&nbsp;" Then
                    e.Item.Cells.RemoveAt(19)
                    e.Item.Cells.RemoveAt(18)
                    e.Item.Cells.RemoveAt(17)
                    e.Item.Cells.RemoveAt(16)
                    e.Item.Cells.RemoveAt(15)

                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)

                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)

                    e.Item.Cells(4).ColumnSpan = 3
                    e.Item.Cells(4).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(4).Font.Bold = True

                    e.Item.Cells(5).ColumnSpan = 7
                    e.Item.Cells(5).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(5).Font.Bold = True

                    e.Item.Cells(6).ColumnSpan = 6
                    e.Item.Cells(6).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(6).Font.Bold = True
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                Else
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    Dim sCA_CELL As String = e.Item.Cells(0).Text
                    Dim sCA_LINK As String = "<A onclick=Javascript:AbrirDCR('" & CStr(e.Item.DataItem("TI_DOCU_RECE")) & "','" & CStr(e.Item.DataItem("NU_DOCU_RECE")).Trim & "','" & CStr(e.Item.DataItem("CO_UNID")) & "','" & CStr(e.Item.DataItem("DE_MODA_MOVI")) & "'); href='#'>" & sCA_CELL & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub dgRetiros_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetiros.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(11).Text <> "&nbsp;" Then
                    'e.Item.Cells(10).Font.Bold = True
                    e.Item.Cells(11).Font.Bold = True
                    e.Item.Cells(12).Font.Bold = True

                    e.Item.ForeColor = System.Drawing.Color.FromName("#112627")
                    e.Item.BackColor = System.Drawing.Color.FromName("#EAEAEA")

                ElseIf e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(2).Text <> "&nbsp;" Then
                    e.Item.Cells.RemoveAt(18)
                    e.Item.Cells.RemoveAt(17)
                    e.Item.Cells.RemoveAt(16)
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells(2).ColumnSpan = 5
                    e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(2).Font.Bold = True

                    e.Item.Cells(3).ColumnSpan = 9
                    e.Item.Cells(3).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(3).Font.Bold = True

                    'e.Item.Cells(4).ColumnSpan = 4
                    'e.Item.Cells(4).HorizontalAlign = HorizontalAlign.Left
                    'e.Item.Cells(4).Font.Bold = True

                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        dgExport.DataSource = Session.Item("dtTemporal")
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objparametro Is Nothing) Then
            objparametro = Nothing
        End If
        If Not (objKardex Is Nothing) Then
            objKardex = Nothing
        End If
        If Not (objMovimiento Is Nothing) Then
            objMovimiento = Nothing
        End If
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) > 0 Then
            Response.Redirect("AlmMovimiento_W.aspx", False)
        End If
    End Sub
End Class
