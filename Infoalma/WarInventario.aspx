<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarInventario.aspx.vb" Inherits="WarInventario" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarInventario</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaVenc").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVenc").click(function () {
                $("#txtFechaVenc").datepicker('show');
            });

            $("#txtFechaCierre").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaCierre").click(function () {
                $("#txtFechaCierre").datepicker('show');
            });
        });

        function AbrirMovi(sNU_RECE, sNU_ITEM, sCO_UNID) {
            var ventana = 'Popup/AlmInventarioDetalle.aspx?sNU_RECE=' + sNU_RECE + '&sNU_ITEM=' + sNU_ITEM + '&sCO_UNID=' + sCO_UNID;
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=550,height=400");
        }
        function fn_Impresion(sTI_REFE, sNU_REFE, sTI_MODA, sFE_VENC, sFE_CIER, sMA_CONT) {
            var ventana = 'Reportes/RepInventario.aspx?sTI_REFE=' + sTI_REFE + '&sNU_REFE=' + sNU_REFE + '&sTI_MODA=' + sTI_MODA + '&sFE_VENC=' + sFE_VENC + '&sFE_CIER=' + sFE_CIER + '&sMA_CONT=' + sMA_CONT;
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%" align="center">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="690">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text">Almac�n</td>
                                                                <td>:</td>
                                                                <td colspan="6">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboAlmacen" runat="server" CssClass="Text" Width="500px"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">&nbsp;Referencia</td>
                                                                <td width="1%">:</td>
                                                                <td width="220">
                                                                    <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text"></asp:DropDownList>&nbsp; 
																		&nbsp;<asp:TextBox ID="txtNroReferencia" runat="server" CssClass="Text" Width="80px"></asp:TextBox>
                                                                </td>
                                                                <td class="Text">Fec.Venc Merc &lt;=</td>
                                                                <td width="95">
                                                                    <input id="txtFechaVenc" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaVenc" runat="server">
                                                                    <input id="btnFechaVenc" class="text" value="..."
                                                                        type="button" name="btnFecha">
                                                                </td>
                                                                <td style="z-index: 0" class="Text">Modalidad :</td>
                                                                <td class="text">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboModalidad" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Estado</td>
                                                                <td>:</td>
                                                                <td class="Text">
                                                                    <asp:DropDownList ID="ddlestado" runat="server" CssClass="text" Width="200px">
                                                                        <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                                                                        <asp:ListItem Value="N">Mercaderia Apta</asp:ListItem>
                                                                        <asp:ListItem Value="S">Mercaderia No Apta</asp:ListItem>
                                                                        <asp:ListItem Value="D">Diferente de Bueno</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text">Cerrado Al :</td>
                                                                <td class="Text">
                                                                    <input id="txtFechaCierre" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaCierre" runat="server"><input id="btnFechaCierre" class="text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                <td class="Text" colspan="2">
                                                                    <asp:CheckBox Style="z-index: 0" ID="chkNoCerrado" runat="server" CssClass="Text" Text="+ DCR no Cerrado"></asp:CheckBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                    <td width="80">
                                                        <input id="btnImprimir" class="btn" onclick="fn_Impresion('<%=cboReferencia.SelectedItem.Text%>','<%=txtNroReferencia.Text%>','<%=cboModalidad.SelectedItem.Text%>','<%=txtFechaVenc.Value%>','<%=txtFechaCierre.Value%>','<%=chkNoCerrado.Checked%>')" value="Imprimir" type="button" name="btnImprimir"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="1200px" AutoGenerateColumns="False"
                                                BorderColor="Gainsboro" OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="30">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_SECU" HeaderText="Item"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_REGI_INGR" HeaderText="Fec.Ingreso"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_TIPO_BULT" HeaderText="Tip.Unid"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_SALD" HeaderText="Nro.Unid" DataFormatString="{0:N2}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_SALD" HeaderText="Bulto"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Prod"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC" HeaderText="Desc.Producto"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_VCTO_MERC" HeaderText="Fec.Venc.Merc"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_UNIT" HeaderText="Prec.Unit" DataFormatString="{0:N2}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_MONE" HeaderText="Mon"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_REMO_SALD" HeaderText="Valor.Total" DataFormatString="{0:N2}"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Valor.Comprometido" DataFormatString="{0:N2}"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_ESTA_MERC" HeaderText="Estado"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="tblLeyenda" border="0" cellspacing="4" cellpadding="0" width="40%">
                                                <tr>
                                                    <td class="td" colspan="2">Leyenda</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f5dc8c" width="30%"></td>
                                                    <td class="Leyenda" width="70%" align="center">DCR no Cerrado en revisi�n</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
