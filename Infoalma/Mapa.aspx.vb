Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class Mapa
    Inherits System.Web.UI.Page
    Private strCodAlmacen As String
    Private objBD As clsCNMapa
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboAlmacenes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroChasis As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboModelo As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboColor As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dtlBodega As System.Web.UI.WebControls.DataList
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrUbicaciones As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAPA_UBICACIONES") Then
            Me.lblError.Text = ""
            If Not Page.IsPostBack Then
                Me.lblOpcion.Text = "Mapa de Ubicaciones"
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAPA_UBICACIONES"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                CargarAlmacenes()
                CargarModalidades()
                CargarModelos()
                CargarColores()
                CargarBodegas()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub CargarModalidades()
        Dim dtModalidades As DataTable
        objBD = New clsCNMapa
        strCodAlmacen = Me.cboAlmacenes.SelectedValue
        dtModalidades = objBD.GetModalidades(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen)
        Me.cboModalidad.Items.Clear()
        Me.cboModalidad.Items.Add(New ListItem("------------Todos------------", "0"))
        For i As Integer = 0 To dtModalidades.Rows.Count - 1
            Me.cboModalidad.Items.Add(New ListItem(dtModalidades.Rows(i)("de_moda_movi"), dtModalidades.Rows(i)("co_moda_movi")))
        Next
    End Sub

    Sub CargarModelos()
        Dim dtModelos As DataTable
        objBD = New clsCNMapa
        strCodAlmacen = Me.cboAlmacenes.SelectedValue
        dtModelos = objBD.GetModelos(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen)
        Me.cboModelo.Items.Clear()
        Me.cboModelo.Items.Add(New ListItem("------------Todos------------", "0"))
        For i As Integer = 0 To dtModelos.Rows.Count - 1
            Me.cboModelo.Items.Add(New ListItem(dtModelos.Rows(i)("modelo")))
        Next
    End Sub

    Sub CargarColores()
        Dim dtColores As DataTable
        objBD = New clsCNMapa
        strCodAlmacen = Me.cboAlmacenes.SelectedValue
        dtColores = objBD.GetColores(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen)
        Me.cboColor.Items.Clear()
        Me.cboColor.Items.Add(New ListItem("------------Todos------------", "0"))
        For i As Integer = 0 To dtColores.Rows.Count - 1
            Me.cboColor.Items.Add(New ListItem(dtColores.Rows(i)("color")))
        Next
    End Sub

    Sub CargarAlmacenes()
        Dim dtAlmacenes As DataTable
        objBD = New clsCNMapa
        dtAlmacenes = objBD.GetAlmacenes(Session("CoEmpresa"), "001", Session("IdSico"))
        Me.cboAlmacenes.DataSource = dtAlmacenes
        Me.cboAlmacenes.DataTextField = "de_alma"
        Me.cboAlmacenes.DataValueField = "co_alma"
        Me.cboAlmacenes.DataBind()
    End Sub

    Sub CargarBodegas()
        Dim dtBodegas As DataTable
        objBD = New clsCNMapa
        strCodAlmacen = Me.cboAlmacenes.SelectedValue
        dtBodegas = objBD.GetBodegas(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen)
        Me.dtlBodega.DataSource = dtBodegas
        Me.dtlBodega.DataBind()
    End Sub

    Private Sub cboAlmacenes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CargarBodegas()
    End Sub

    Public Sub btnBodega_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dtUbicaciones As DataTable
            Dim btnBodega As Button = CType(sender, Button)
            objBD = New clsCNMapa
            strCodAlmacen = Me.cboAlmacenes.SelectedValue
            Dim intFila As Integer = 0
            Dim strTabla As String
            Dim strCabecera As String
            Dim sModelo As String
            Dim sColor As String
            Dim sModalidad As String
            Dim sChasis As String

            sModalidad = Me.cboModalidad.SelectedValue
            sModelo = Me.cboModelo.SelectedItem.Text
            sColor = Me.cboColor.SelectedItem.Text
            sChasis = Me.txtNroChasis.Text

            Me.lblTitulo.Text = btnBodega.ToolTip
            dtUbicaciones = objBD.GetUbicaciones(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen, btnBodega.Text, Me.cboModalidad.SelectedValue, Me.cboModelo.SelectedItem.Text, Me.cboColor.SelectedItem.Text, Me.txtNroChasis.Text)

            For i As Integer = 0 To dtUbicaciones.Rows.Count - 1
                If dtUbicaciones.Rows(i)("co_ubic").ToString.Length > 1 Then
                    If intFila > 1 Then
                        Exit For
                    End If
                    strCabecera += "<td class='Cabecera'>" & dtUbicaciones.Rows(i)("co_ubic").ToString.Substring(1, 2) & "</td>"
                Else
                    intFila += 1
                End If
            Next

            strTabla = "<table id='tblUbicaciones' cellSpacing='2' cellPadding='0' border='0'><tr><td></td>" & strCabecera
            For i As Integer = 0 To dtUbicaciones.Rows.Count - 1
                If dtUbicaciones.Rows(i)("co_ubic").ToString.Length = 1 Then
                    strTabla += "</tr>"
                    strTabla += "<tr><td class='Cabecera'>" & dtUbicaciones.Rows(i)("co_ubic").ToString & "</td>"
                Else
                    If dtUbicaciones.Rows(i)("TI_SITU") = "ACT" Or dtUbicaciones.Rows(i)("TI_SITU") = "ENC" Then
                        strTabla += "<td><a href=javascript:abrir('" & strCodAlmacen & "','" & btnBodega.Text & "','" & dtUbicaciones.Rows(i)("co_ubic").ToString &
                        "','" & sModalidad & "','" & sModelo.Replace(" ", "/") & "','" & sColor.Replace(" ", "/") & "','" & sChasis &
                        "')><IMG style='CURSOR: hand' alt='" & dtUbicaciones.Rows(i)("de_ubic") & Environment.NewLine & "Ubicaci�n con vehiculos'" &
                        " src='Images/auto.jpg' style='border: none;' ></a></td>"
                    Else
                        strTabla += "<td><IMG alt='" & dtUbicaciones.Rows(i)("de_ubic") & Environment.NewLine & "Ubicaci�n Vacia' src='Images/Vacio1.jpg'></td>"
                    End If
                End If
            Next
            Me.ltrUbicaciones.Text = strTabla & "</tr></table>"
            Me.lblTotal.Text = "Total de veh�culos: " & objBD.GetTotal(Session("CoEmpresa"), "001", Session("IdSico"), strCodAlmacen, btnBodega.Text, sModalidad, sModelo, sColor, sChasis)
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "C", "MAPA_UBICACIONES", "CONSULTAR MAPA DE UBICACIONES", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objBD Is Nothing) Then
            objBD = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
