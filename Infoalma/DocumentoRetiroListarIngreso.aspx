<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoRetiroListarIngreso.aspx.vb" Inherits="DocumentoRetiroListarIngreso" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Ingresos registrados en el WMS</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function AbrirDetalle(sNU_GUIA, sDE_ALMA) {
            window.open("Popup/PoputDocRetiroListarIngreso.aspx?sNU_GUIA=" + sNU_GUIA + "&sDE_ALMA=" + sDE_ALMA, "", 'left=240,top=200,width=700,height=400,toolbar=0,scrollbars=1,resizable=1')
        }

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() - miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="6">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table style="width: 600px" id="Table6" border="0" cellspacing="4" cellpadding="0">
                                                            <tr>
                                                                <td class="Text">Nro&nbsp;Referencia :</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNroGuia" runat="server" CssClass="Text" Width="120px"></asp:TextBox></td>
                                                                <td class="Text">Del :</td>
                                                                <td>
                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="text"  value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                <td class="Text">Del :
                                                                </td>
                                                                <td>
                                                                    <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaFinal" runat="server"><input id="btnFechaFinal" class="text" value="..."
                                                                            type="button" name="btnFecha">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right">
                                            <asp:HyperLink ID="hlkNuevo" runat="server" CssClass="Text" NavigateUrl="DocumentoRetiroIngreso.aspx"
                                                Target="_parent">Nuevo Ingreso</asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button><asp:Button Style="z-index: 0" ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                    </tr>
                                    <tr>
                                        <td height="250" valign="top" align="center">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" AllowPaging="True" PageSize="20" OnPageIndexChanged="Change_Page">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Nro.Gu&#237;a">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_ALMA" HeaderText="Almac&#233;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_CANT_TOTA" HeaderText="Cant.Total">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_LLEG" HeaderText="Fecha Creaci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Ver">
                                                        <HeaderStyle Width="80px"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <a style="font-family: Verdana; font-size: 7pt; font-weight: bold" class="error" onclick='AbrirDetalle("<%# DataBinder.Eval(Container.DataItem, "ORDEN") %>","<%# DataBinder.Eval(Container.DataItem, "DE_ALMA") %>");' href="#">Detalle</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table2" border="0" cellspacing="4" cellpadding="0" width="630">
                                                <tr>
                                                    <td class="td" colspan="8">Leyenda</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f5dc8c" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Ingresos&nbsp;pendientes.</td>
                                                    <td bgcolor="lightblue" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Ingresos completados.</td>
                                                    <td bgcolor="silver" width="8%"></td>
                                                    <td class="Leyenda" width="25%">Ingresos&nbsp;anulados.</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
