<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EntidadBusqueda.aspx.vb" Inherits="EntidadBusqueda" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Busqueda Entidad</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="francisco_uni@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table3" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD class="Titulo1" width="100%">Consulta de Entidades
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="4">
									<uc1:menuinfo id="MenuInfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text">Tipo Entidad:</TD>
														<TD>
															<asp:dropdownlist id="cboTipoEntidad" runat="server" CssClass="Text" Width="100px"></asp:dropdownlist></TD>
														<TD class="Text">Nombre Entidad:</TD>
														<TD>
															<asp:textbox id="txtNombreEntidad" style="TEXT-TRANSFORM: uppercase" runat="server" CssClass="Text"
																Width="200px"></asp:textbox></TD>
														<TD>
															<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">Resultado:</TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
										OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="15" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="COD_SICO" HeaderText="C&#243;digo">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Nombre">
												<HeaderStyle Width="250px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_RUC" HeaderText="RUC">
												<HeaderStyle Width="60px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_TIPENTI" HeaderText="Tipo Entidad">
												<HeaderStyle Width="80px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" CausesValidation="False"
																	ImageUrl="Images/Ver.JPG" ToolTip="Ver detalles"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPENTI"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
