<%@ Page Language="vb" AutoEventWireup="false" CodeFile="FilValidaAcceso.aspx.vb" Inherits="FilValidaAcceso"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FilValidaAcceso</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:header id="Header2" runat="server"></uc1:header>
						<TABLE id="Table1" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="middle" align="center">
									<TABLE id="Table3" cellSpacing="4" cellPadding="0" width="100%" border="0">
										<TR>
											<TD align="center">
												<TABLE id="Table5" style="BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid"
													cellSpacing="4" width="320" align="center" border="0">
													<TR>
														<TD class="Subtitulo2" align="center">Seleccione Cliente Adicional</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" align="center" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 33px" align="center">
															<asp:dropdownlist id="cboCliente" runat="server" Width="300px" DataValueField="COD" DataTextField="DES"
																CssClass="Text" Enabled="False"></asp:dropdownlist>&nbsp;</TD>
													</TR>
													<TR>
														<TD align="center" width="50%">
															<asp:button id="btnAceptar" runat="server" Width="80px" CssClass="btn" Enabled="False" Text="Ingresar"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
									<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
