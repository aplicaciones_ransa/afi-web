Imports Library.AccesoDB
Imports System.IO
Imports System.Data

Public Class VisorRetiro
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationSettings.AppSettings.Item("RutaPDFs")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            Try
                Dim var As Integer
                Dim drPDF As DataRow
                Dim objStream As Stream
                Dim line As String
                Dim strDesencriptado As String
                Dim Contenido As Byte()
                Dim resultado As Byte()
                Dim strDescriptado As String
                Dim Parametro As String
                var = Request.QueryString("var")
                drPDF = objDocumento.gSelArchivoPDF(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), var)
                Select Case var
                    Case 1
                        If drPDF Is Nothing Then
                            Response.Write("No se encontr� el archivo PDF")
                            Exit Try
                        ElseIf drPDF("DOC_PDFFINA") Is DBNull.Value Then
                            Response.Clear()
                            Response.AddHeader("Content-Length", CType(drPDF("DOC_PDFORIG"), Byte()).Length.ToString())
                            Response.Buffer = True
                            Response.ContentType = "application/pdf"
                            Response.BinaryWrite(CType(drPDF("DOC_PDFORIG"), Byte()))
                        ElseIf drPDF("DOC_PDFORIG") Is DBNull.Value Then
                            objStream = New FileStream(strPath & Session.Item("Cod_Unidad") & Session.Item("NroLib") & ".PDF", FileMode.Open)
                            Dim FileSize As Long
                            FileSize = objStream.Length

                            Dim Buffer(CInt(FileSize)) As Byte
                            objStream.Read(Buffer, 0, CInt(FileSize))
                            objStream.Close()

                            Response.Clear()
                            Response.AddHeader("Content-Length", Buffer.Length.ToString())
                            Response.Buffer = True
                            Response.ContentType = "application/pdf"
                            Response.BinaryWrite(Buffer)
                        Else
                            Response.Clear()
                            Response.AddHeader("Content-Length", CType(drPDF("DOC_PDFFINA"), Byte()).Length.ToString())
                            Response.Buffer = True
                            Response.ContentType = "application/pdf"
                            Response.BinaryWrite(CType(drPDF("DOC_PDFFINA"), Byte()))
                        End If
                    Case 2
                        Response.Clear()
                        Response.AddHeader("Content-Length", CType(drPDF("DOC_PDFORIG"), Byte()).Length.ToString())
                        Response.Buffer = True
                        Response.ContentType = "application/pdf"
                        Response.BinaryWrite(CType(drPDF("DOC_PDFORIG"), Byte()))
                End Select
                Response.Flush()
                Response.Close()
            Catch ex As Exception
                Response.Write("No se encontr� el archivo PDF")
                Response.Write(ex.Message())
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
