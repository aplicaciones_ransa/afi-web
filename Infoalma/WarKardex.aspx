<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarKardex.aspx.vb" Inherits="WarKardex" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarKardex</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIni").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFin").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        function fn_Impresion(sNO_MODA, sEST_DCR, sFE_INIC, sFE_FINA, sREFE, sCO_PRODU) {
            var ventana = 'Popup/AlmImpresionKardex.aspx?sNO_MODA=' + sNO_MODA + '&sEST_DCR=' + sEST_DCR +
                '&sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sREFE=' + sREFE + '&sCO_PRODU=' + sCO_PRODU;
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
            //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
        }
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);

            if (fchFin == "" && fchIni == "") {
                alert("Debe ingresar por lo menos una fecha");
                return false;
            }

            if (fchFin != "" && fchIni != "") {
                if (fchIni > fchFin) {
                    alert("La fecha inicial no puede ser mayor a la fecha final");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table id="Table1" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        height="400" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top">
                                <table id="Table3" cellspacing="6" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" cellspacing="0" cellpadding="0" width="690" align="center" border="0">
                                                <tr>
                                                    <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                                    <td background="Images/table_r1_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r2_c1.gif"></td>
                                                    <td>
                                                        <table id="Table6" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Text" width="11%">Estado DCR</td>
                                                                <td width="1%">:</td>
                                                                <td width="28%">
                                                                    <asp:DropDownList ID="cboEstadoDCR" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td class="Text" width="11%">Referencia</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text" width="17%">
                                                                    <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td width="20%">
                                                                    <asp:TextBox ID="txtNroReferencia" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Modalidad</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboModalidad" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td class="Text">Fecha
                                                                </td>
                                                                <td colspan="3">
                                                                    <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td class="Text" align="right">Del :</td>
                                                                            <td style="width: 105px">
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaInicial" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaInicial" runat="server">
                                                                                <input class="text" id="btnFechaIni" type="button"
                                                                                    value="..." name="btnFechaIni"></td>
                                                                            <td class="Text" align="right">Al :</td>
                                                                            <td style="width: 105px">
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaFinal" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaFinal" runat="server">
                                                                                <input class="text" id="btnFechaFin" type="button"
                                                                                    value="..." name="btnFechaFin"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Almac�n</td>
                                                                <td>:</td>
                                                                <td colspan="4" align="left">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboAlmacen" runat="server" CssClass="Text" Width="370px"></asp:DropDownList></td>
                                                                <td align="right">
                                                                    <asp:CheckBox ID="chkDOR_PEND" runat="server" CssClass="Text" Text="+ DOR pendientes de despacho"></asp:CheckBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="6" background="Images/table_r2_c3.gif"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                                    <td background="Images/table_r3_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" cellspacing="4" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <input class="btn" id="btnImprimir" onclick="fn_Impresion('<%=cboModalidad.SelectedItem.Text%>','<%=cboEstadoDCR.SelectedItem.Text%>','<%=txtFechaInicial.Value%>','<%=txtFechaFinal.Value%>','<%=cboReferencia.SelectedItem.Text%>','<%=txtNroReferencia.Text%>')" type="button" value="Imprimir"></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="1200px" PageSize="50" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" OnPageIndexChanged="Change_Page" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="FE_MOVI" HeaderText="Fecha">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DOR">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_TIPO_BULT" HeaderText="Unidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_RECI" HeaderText="Uni.Entrada" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Uni. Salida" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_SALD" HeaderText="Uni. Saldo" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_UNME_PESO" HeaderText="Bulto">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_RECI" HeaderText="Bul.Entrada" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Bul.Salida" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_SALD" HeaderText="Bul.Saldo" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_REMO_INGR" HeaderText="Imp.Entrada" DataFormatString="{0:#,##0.00}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_REMO_RETI" HeaderText="Imp.Salida" DataFormatString="{0:#,##0.00}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_REMO_SALD" HeaderText="Imp.Saldo" DataFormatString="{0:#,##0.00}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_GUIA" HeaderText="Nro.Guia">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_OBSE" HeaderText="Observacion">
                                                        <HeaderStyle HorizontalAlign="Center" Width="300px"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="ST_SALI_ALMA" HeaderText="ESTADO"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
                                            <table id="tblLeyenda" cellspacing="4" cellpadding="0" width="40%" border="0">
                                                <tr>
                                                    <td class="td" colspan="2">Leyenda</td>
                                                </tr>
                                                <tr>
                                                    <td width="30%" bgcolor="#f5dc8c"></td>
                                                    <td class="Leyenda" align="center" width="70%">DOR pendientes de despacho</td>
                                                </tr>
                                                <tr>
                                                    <td class="Leyenda" align="center" width="70%" colspan="2"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
