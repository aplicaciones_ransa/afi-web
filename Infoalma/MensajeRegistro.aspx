<%@ Page Language="vb" AutoEventWireup="false" CodeFile="MensajeRegistro.aspx.vb" Inherits="MensajeRegistro" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Consultas Generadas</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  

			//------------------------------------------ Inicio Validar Fecha
		var primerslap=false; 
		var segundoslap=false; 
		function IsNumeric(valor) 
		{ 
			var log=valor.length; var sw="S"; 
			for (x=0; x<log; x++) 
			{ v1=valor.substr(x,1); 
			v2 = parseInt(v1); 
			//Compruebo si es un valor num�rico 
			if (isNaN(v2)) { sw= "N";} 
			} 
				if (sw=="S") {return true;} else {return false; } 
		} 	
		function formateafecha(fecha) 
		{ 
			var long = fecha.length; 
			var dia; 
			var mes; 
			var ano; 

			if ((long>=2) && (primerslap==false)){dia=fecha.substr(0,2); 
			if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
			else { fecha=""; primerslap=false;} 
			} 
			else 
			{ dia=fecha.substr(0,1); 
			if (IsNumeric(dia)==false) 
			{fecha="";} 
			if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
			} 
			if ((long>=5) && (segundoslap==false)) 
			{ mes=fecha.substr(3,2); 
			if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
			else { fecha=fecha.substr(0,3);; segundoslap=false;} 
			} 
			else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
			if (long>=7) 
			{ ano=fecha.substr(6,4); 
			if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
			else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
			} 

			if (long>=10) 
			{ 
				fecha=fecha.substr(0,10); 
				dia=fecha.substr(0,2); 
				mes=fecha.substr(3,2); 
				ano=fecha.substr(6,4); 
				// A�o no viciesto y es febrero y el dia es mayor a 28 
				if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
			} 
			return (fecha); 
		} 
		//-----------------------------------Fin formatear fecha	
		function Ocultar()
			{
			Estado.style.display='none';			
			}	
	function CerrarPanel() 
    {
    document.getElementById("pnlAnular").style.display="none";
      // document.all["pnlAnular"].style.display='none';            
    }
       function AbrirPanel()
    {
     document.getElementById("pnlAnular").style.display="inline";
     // document.all["pnlAnular"].style.display='inline';
    }	
		</script>
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="107"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Text" align="right"><asp:literal id="ltrEnviarCorreo" runat="server"></asp:literal></TD>
							</TR>
							<TR>
								<TD align="center" height="100%">
									<P>&nbsp;<asp:label id="lblNroRequerimiento" runat="server" Height="5px" Visible="False" Width="20px"></asp:label><asp:label id="lblArea" runat="server" Visible="False" Width="20px"></asp:label><asp:label id="lblTipoEnvio" runat="server" Visible="False" Width="20px"></asp:label><asp:label id="lblUsuario" runat="server" Visible="False" Width="20px"></asp:label>
										<asp:label id="lblSolicitud" runat="server" Visible="False" Width="20px"></asp:label><asp:label id="lblFecha" runat="server" Visible="False" Width="20px"></asp:label><asp:label id="lblHora" runat="server" Visible="False" Width="20px"></asp:label></P>
									<DIV id="pnlAnular" style="BORDER-RIGHT: #051e46 1px solid; BORDER-TOP: #051e46 1px solid; DISPLAY: none; BORDER-LEFT: #051e46 1px solid; WIDTH: 315px; BORDER-BOTTOM: #051e46 1px solid; HEIGHT: 120px"
										ms_positioning="FlowLayout">
										<TABLE id="Table8" style="WIDTH: 312px; HEIGHT: 119px" cellSpacing="4" cellPadding="0"
											width="312" bgColor="gainsboro" border="0">
											<TR>
												<TD class="td" colSpan="2">Enviar Correo</TD>
											</TR>
											<TR>
												<TD class="text" style="WIDTH: 66px">Destinatario :</TD>
												<TD><asp:textbox id="txtDestinatario" runat="server" Width="160px"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtDestinatario"
														ErrorMessage="RequiredFieldValidator" ForeColor=" " BackColor="Transparent" CssClass="error" BorderColor="Transparent">Ingrese</asp:requiredfieldvalidator></TD>
											</TR>
											<TR>
												<TD class="text" style="WIDTH: 66px; HEIGHT: 28px" vAlign="top">Comentario :</TD>
												<TD style="HEIGHT: 28px" vAlign="top"><asp:textbox id="txtComentario" runat="server" Width="160px" TextMode="MultiLine"></asp:textbox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 66px"></TD>
												<TD><asp:button id="btnEnviar" runat="server" CssClass="btn" Text="Enviar Correo"></asp:button><INPUT class="btn" id="btnCerrar" onclick="CerrarPanel();" type="button" value="Cerrar"></TD>
											</TR>
										</TABLE>
									</DIV>
									<P><asp:datagrid id="dgdResultado" tabIndex="2" runat="server" Height="0px" Width="100%" CssClass="gv"
											BorderColor="Gainsboro" PageSize="15" AutoGenerateColumns="False" AllowSorting="True" CellPadding="0"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
											<ItemStyle CssClass="gvRow"></ItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="gvheader" BackColor="DarkBlue"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="NU_REQU" HeaderText="Requerimiento"></asp:BoundColumn>
												<asp:BoundColumn DataField="DE_TIPO_ENVI" HeaderText="Tipo Env&#237;o"></asp:BoundColumn>
												<asp:BoundColumn DataField="DE_AREA" HeaderText="&#193;rea"></asp:BoundColumn>
												<asp:BoundColumn DataField="CO_USUA_CREA" HeaderText="Usuario"></asp:BoundColumn>
												<asp:BoundColumn DataField="SOLI" HeaderText="Solicitud"></asp:BoundColumn>
												<asp:BoundColumn DataField="FE_RECE" HeaderText="Fecha"></asp:BoundColumn>
												<asp:BoundColumn DataField="HO_RECE" HeaderText="Hora"></asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Center" CssClass="gvpager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></P>
									<P>&nbsp;</P>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 13px" align="center"><asp:hyperlink id="lnkVerEstado" runat="server" ForeColor="#B50000" CssClass="ERROR" Font-Bold="True"
										NavigateUrl="EstadoRequerimiento.aspx">VER ESTADO DEL REQUERIMIENTO >></asp:hyperlink></TD>
							</TR>
							<TR>
								<TD class="td"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
