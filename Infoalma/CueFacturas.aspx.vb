Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class CueFacturas
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    ' Protected WithEvents txtDCR As System.Web.UI.WebControls.TextBox
    Private objFactura As clsCNFactura
    Dim objCCorrientes As clsCNCCorrientes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroFactura As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.HtmlControls.HtmlInputButton
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents dgTCDOCU_CLIE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboUnidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "DETALLE_FACTURA") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "DETALLE_FACTURA"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                LlenarUnidades()
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
            End If
            Me.lblOpcion.Text = "Detalle de Factura"
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub BindDataGrid()
        objFactura = New clsCNFactura
        With objFactura
            .Lista_Facturas(Session.Item("CoEmpresa"), Session.Item("IdSico"), Me.cboUnidad.SelectedValue,
            Me.txtNroFactura.Text, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.dgTCDOCU_CLIE, Me.txtDCR.Text)
        End With
    End Sub

    Private Sub LlenarUnidades()
        Try
            objFactura = New clsCNFactura
            Dim dsTTUNID As DataSet = objFactura.fn_listarUnidades(Session("CoEmpresa"))
            Me.cboUnidad.DataSource = dsTTUNID
            Me.cboUnidad.DataBind()
            Me.cboUnidad.SelectedIndex = 0
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub btnBuscar_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.ServerClick
        Me.dgTCDOCU_CLIE.CurrentPageIndex = 0
        BindDataGrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "DETALLE_FACTURA", "DETALLE_FACTURA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgTCDOCU_CLIE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTCDOCU_CLIE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text = "&nbsp;" And e.Item.Cells(2).Text = "&nbsp;" Then
                ElseIf e.Item.Cells(0).Text <> "&nbsp;" And e.Item.Cells(2).Text = "&nbsp;" Then
                    e.Item.Cells.Item(23).Visible = False
                    e.Item.Cells.Item(22).Visible = False
                    e.Item.Cells.RemoveAt(21)
                    e.Item.Cells.RemoveAt(20)
                    e.Item.Cells.RemoveAt(19)
                    e.Item.Cells.RemoveAt(18)
                    e.Item.Cells.RemoveAt(17)
                    e.Item.Cells.RemoveAt(16)
                    e.Item.Cells.RemoveAt(15)
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells(1).ColumnSpan = 16
                    e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(1).Width = System.Web.UI.WebControls.Unit.Pixel(400)
                    e.Item.Cells(1).Font.Bold = True

                    Dim sCA_CELL As String = e.Item.Cells(0).Text
                    CType(e.Item.FindControl("LinFactutraPDF"), LinkButton).Text = "<a style='TEXT-DECORATION: underline' href='#' onclick=Javascript:AbrirFactura('" & Me.cboUnidad.SelectedValue & "','" & CStr(e.Item.DataItem("DE_DOCU")).Substring(0, 3) & "','" & CStr(e.Item.DataItem("NU_DOCU")) & "');> " & sCA_CELL & "</a>"
                    CType(e.Item.FindControl("LinFactutraPDF"), LinkButton).Visible = True


                    objCCorrientes = New clsCNCCorrientes
                    Dim dt As DataTable
                    dt = objCCorrientes.gCADGetMostrarSustento("01", Me.cboUnidad.SelectedValue, CStr(e.Item.DataItem("DE_DOCU")).Substring(0, 3), CStr(e.Item.DataItem("NU_DOCU")))
                    If dt.Rows.Count <> 0 Then
                        If dt.Rows(0)("NU_DOCU") = CStr(e.Item.DataItem("NU_DOCU")) Then
                            CType(e.Item.FindControl("lnkSustentoDCR"), LinkButton).Text = "<a style='TEXT-DECORATION: underline' href='#' onclick=Javascript:AbrirSustentoDCR('" & Me.cboUnidad.SelectedValue & "','" & CStr(e.Item.DataItem("DE_DOCU")).Substring(0, 3) & "','" & CStr(e.Item.DataItem("NU_DOCU")) & "');> " & "SUSTENTO DCR" & "</a>"
                            CType(e.Item.FindControl("lnkSustentoDCR"), LinkButton).Visible = True
                        End If
                    End If

                    If CStr(e.Item.DataItem("FLG_XML")) = "1" Then
                        CType(e.Item.FindControl("lnkExportarXML"), LinkButton).Visible = True
                    Else
                        CType(e.Item.FindControl("lnkExportarXML"), LinkButton).Visible = False
                    End If
                ElseIf e.Item.Cells(5).Text = "TOT. DOCUMENTO:" Or e.Item.Cells(5).Text = "TOT. FACTURA:" Or e.Item.Cells(5).Text = "TOT. AV. COBRANZA:" Then
                    e.Item.Cells(5).Font.Bold = True
                    e.Item.Cells(6).Font.Bold = True
                    e.Item.Cells(8).Font.Bold = True
                    e.Item.Cells(12).Font.Bold = True
                ElseIf e.Item.Cells(2).Text = "DOT" Then
                    Dim sCA_CELL1 As String = e.Item.Cells(3).Text
                    Dim sCA_LINK1 As String = "<a style='TEXT-DECORATION: underline' href='#' onclick=Javascript:AbrirDOT('" & e.Item.Cells(19).Text & "','" & e.Item.Cells(20).Text & "','" & e.Item.Cells(3).Text & "','" & e.Item.Cells(21).Text & "','" & e.Item.Cells(18).Text & "');> " & sCA_CELL1 & "</a>"
                    e.Item.Cells(3).Text = sCA_LINK1
                    'nuevo

                ElseIf e.Item.Cells(2).Text = "DCR" Then
                    Dim sCA_CELL1 As String = e.Item.Cells(3).Text
                    Dim sCA_LINK1 As String = "<a style='TEXT-DECORATION: underline' href='#' onclick=Javascript:AbrirAlmDCR('" & e.Item.Cells(2).Text & "','" & e.Item.Cells(3).Text & "','" & Me.cboUnidad.SelectedValue & "','" & e.Item.Cells(18).Text & "');> " & sCA_CELL1 & "</a>"
                    e.Item.Cells(3).Text = sCA_LINK1
                ElseIf e.Item.Cells(2).Text = "CER" Or e.Item.Cells(2).Text = "WAR" Or e.Item.Cells(2).Text = "WIP" Then
                    Dim sCA_CELL1 As String = e.Item.Cells(3).Text
                    Dim sCA_LINK1 As String = "<a style='TEXT-DECORATION: underline' href='#' onclick=Javascript:AbrirDetalleWAR('" & e.Item.Cells(19).Text & "','" & e.Item.Cells(20).Text & "','" & e.Item.Cells(3).Text & "','" & e.Item.Cells(2).Text & "');> " & sCA_CELL1 & "</a>"
                    e.Item.Cells(3).Text = sCA_LINK1

                End If
            End If
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Private Sub dgTCDOCU_CLIE_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgTCDOCU_CLIE.PageIndexChanged
        dgTCDOCU_CLIE.CurrentPageIndex = e.NewPageIndex
        Dim dvTCDOCU_CLIE As DataView = CType(Session("dvRE_GENE"), DataView)
        Me.dgTCDOCU_CLIE.DataSource = dvTCDOCU_CLIE
        Me.dgTCDOCU_CLIE.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        BindDataGrid()
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Factura.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dvRE_GENE"), DataView)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Protected Sub lnkExportarXML_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objCCorrientes As clsCNCCorrientes
            Dim lnkExportarXML As LinkButton = CType(sender, LinkButton)
            Dim dgi As DataGridItem
            dgi = CType(lnkExportarXML.Parent.Parent, DataGridItem)

            Dim dt As New DataTable
            Dim strNomXML As String
            Dim strDetaXML As String
            objCCorrientes = New clsCNCCorrientes

            If dgi.Cells(2).Text() = 1 Then
                objFactura = New clsCNFactura
                With objFactura
                    dt = objFactura.gCADGetDocumentoDetalleXML("01", Me.cboUnidad.SelectedValue, CStr(dgi.Cells(0).Text()).Substring(0, 3), dgi.Cells(3).Text(), Session.Item("IdSico"))
                End With

                If dt.Rows.Count <> 0 Then
                    strNomXML = dt.Rows(0)("NU_DOCU")
                    strDetaXML = dt.Rows(0)("DE_DOC_XML")
                Else
                    Me.lblMensaje.Text = "Documento XML no encontrado"
                    Exit Sub
                End If

                Dim response As HttpResponse = HttpContext.Current.Response
                response.Clear()
                response.Charset = ""
                response.ContentType = "text"
                response.AddHeader("Content-Disposition", "attachment;filename=" & strNomXML & ".xml")
                response.ContentEncoding = Encoding.Default
                Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
                Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

                response.Write(strDetaXML)
                response.End()
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objFactura Is Nothing) Then
            objFactura = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
