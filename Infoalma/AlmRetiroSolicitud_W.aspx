<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmRetiroSolicitud_W.aspx.vb" Inherits="AlmRetiroSolicitud_W" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Solicitud de Retiro</title>
    <meta name="vs_showGrid" content="False">
    <meta name="vs_snapToGrid" content="False">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta content="0" http-equiv="Expires">
    <meta content="no-cache" http-equiv="Pragma">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script src="JScripts/jquery.min.cache.js"></script>
    <script src="JScripts/jquery-ui.min.cache.js"></script>
    <link rel="stylesheet" type="text/css" href="Styles/jquery-ui.cache.css">

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="VBScript">
			Sub SubmitForm(intVar)
					mensaje = msgbox ( "Desea rechazar el retiro" ,vbyesno,"Confirmaci�n")
					if mensaje = vbyes then
						frmArchivo.action = "DocumentoFirmadoPedido.aspx?var=" & intVar & "&Pagina=1"
						frmArchivo.submit
						else		
						exit Sub
					end if
			End Sub
			Sub SubmitForm2(intVar)
			    frmArchivo.action = "DocumentoFirmadoPedido.aspx?var=" & intVar & "&Pagina=1"
				frmArchivo.submit
		    End Sub
    </script>
    <script type="text/javascript">

        $(function () {
            $("#txtFechaRecojo").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVenc").click(function () {
                $("#txtFechaRecojo").datepicker('show');
            });

        
        });


        if (history.forward(1)) {
            location.replace(history.forward(1));
        }
    </script>

    <script type="text/javascript" language="jscript">


        function SubmitForm(intVar) {
            if (confirm("Desea rechazar el retiro")) {
                frmArchivo.action = 'DocumentoFirmadoPedido.aspx?var=' + intVar + '&Pagina=1';
                frmArchivo.submit();
            }
        }

        function SubmitForm2(intVar) {
   
            frmArchivo.action = 'DocumentoFirmadoPedido.aspx?var=' + intVar + '&Pagina=1';
            frmArchivo.submit();
         }

        var $dialog = null;
        jQuery.showModalDialog = function (options) {

            var defaultOptns = {
                url: null,
                dialogArguments: null,
                height: 'auto',
                width: 'auto',
                position: 'top',
                resizable: false,
                scrollable: false,
                onClose: function () { },
                returnValue: null,
                doPostBackAfterCloseCallback: false,
                postBackElementId: null
            };

            var fns = {
                close: function () {
                    opts.returnValue = $dialog.returnValue;
                    $dialog = null;
                    opts.onClose();
                    if (opts.doPostBackAfterCloseCallback) {
                        postBackForm(opts.postBackElementId);
                    }
                },
                adjustWidth: function () { $frame.css("width", "100%"); }
            };

            // build main options before element iteration
            var opts = $.extend({}, defaultOptns, options);
            var $frame = $('<iframe id="iframeDialog" />');

            if (opts.scrollable)
                $frame.css('overflow', 'auto');

            $frame.css({
                'padding': 0,
                'margin': 0,
                'padding-bottom': 2
            });

            var $dialogWindow = $frame.dialog({
                autoOpen: true,
                modal: true,
                width: opts.width,
                height: opts.height,
                resizable: opts.resizable,
                position: opts.position,
                overlay: {
                    opacity: 0.5,
                    background: "black"
                },
                close: fns.close,
                resizeStop: fns.adjustWidth
            });

            $frame.attr('src', opts.url);
            fns.adjustWidth();

            $frame.load(function () {
                if ($dialogWindow) {
                    var maxTitleLength = 50;
                    var title = $(this).contents().find("title").html();

                    if (title.length > maxTitleLength) {
                        title = title.substring(0, maxTitleLength) + '...';
                    }
                    $dialogWindow.dialog('option', 'title', title);
                }
            });

            $dialog = new Object();
            $dialog.dialogArguments = opts.dialogArguments;
            $dialog.dialogWindow = $dialogWindow;
            $dialog.returnValue = null;
        }

        function postBackForm(targetElementId) {
            var theform;
            theform = document.forms[0];
            theform.__EVENTTARGET.value = targetElementId;
            theform.__EVENTARGUMENT.value = "";
            theform.submit();
        }

        function openWindow(strfirma) {
            if (confirm('Esta seguro de enviar la solicitud?') == false) return false;
            //var fecha_recojo = document.getElementById('txtFechaRecojo').value
            //if (fecha_recojo != '')
            //{
            //	window.alert('Debe registrar una fecha de recojo apr�ximado!');
            //	return;
            //}
            if (document.getElementById('txtTipoUsuario').value != '03') {
                SubmitForm2(1);
                return;
            }
            var Argumentos = new Object();
            if (strfirma == '1') {
                SubmitForm2(1);
            }
            if (strfirma == '0') {
                var Argumentos = new Object();
                var url = 'Popup/LgnSolicitaFirma.aspx';
                $.showModalDialog({
                    url: url,
                    dialogArguments: null,
                    heigh: 110,
                    width: 360,
                    scrollable: false,
                    onClose: function () {
                        Argumentos = this.returnValue;
                        if (Argumentos == null) {
                            window.alert('Usted a cancelado el proceso de firma!');
                        }
                        else { //quiere decir que se ha devuelto una lista de contactos
                            document.getElementById('txtContrasena').value = Argumentos[0];
                            document.getElementById('txtNuevaContrasena').value = Argumentos[1];
                            SubmitForm2(1);
                        }
                    }
                });
            }
        }
        function noboton(ev) {
            if (document.all) ev = event;
            if (ev.button & 2)
                alert("Bot�n inactivo.");
        }
        function bloquear() {
            document.onmousedown = noboton;
        }
    </script>
</head>
<body onload="bloquear()" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0"
    bgcolor="#f0f0f0">
    <form id="frmArchivo" method="post" name="frmArchivo" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="10">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <p>
                                                <asp:DataGrid ID="dgPedido" runat="server" CssClass="gv" AutoGenerateColumns="False" BorderColor="Gainsboro"
                                                    Width="100%">
                                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod. Producto"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DSC_PROD" HeaderText="Mercaderia"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="LOTE" HeaderText="Lote"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DSC_RETE" HeaderText="Retenci&#243;n"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="UNI_MEDI" HeaderText="Unid. Medida"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" HeaderText="Item"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="True" DataField="FCH_VENC" HeaderText="Fecha Vencimiento">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="COD_RETE"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_UNID_SALD"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Cnt a Retirar" DataFormatString="{0:N6}"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Eliminar">
                                                            <HeaderStyle Width="30px"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <table id="Table9" cellspacing="0" cellpadding="0" width="30" border="0">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" ToolTip="Eliminar Item"
                                                                                ImageUrl="Images/anulado.gif"></asp:ImageButton></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <PagerStyle CssClass="gvPager"></PagerStyle>
                                                </asp:DataGrid>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" valign="top" align="center">
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                        <td height="20" valign="top" width="100%"></td>
                                    </tr>
                                    <tr>
                                        <td class="Subtitulo" valign="top">Datos requeridos:
												<div style="display: none">
                                                    <input id="txtTipoUsuario" name="txtTipoUsuario" runat="server"><input id="txtContrasena" name="txtContrasena" runat="server"><input id="txtNuevaContrasena" name="txtNuevaContrasena" runat="server"></div>
                                        </td>
                                        <td valign="top" width="100%"></td>
                                    </tr>
                                    <tr>
                                        <td class="Text" valign="top" align="left">
                                            <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="630" align="center">
                                                <tr>
                                                    <td class="Text"></td>
                                                    <td></td>
                                                    <td class="Text" colspan="4">
                                                        <asp:TextBox ID="txtBrevete" runat="server" CssClass="Text3" Width="104px" Visible="False"></asp:TextBox><asp:Button ID="btnBuscar" runat="server" CssClass="btn" Width="60px" Visible="False" Text="Buscar"></asp:Button></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Nombre del Cliente</td>
                                                    <td class="Text">:</td>
                                                    <td class="Text">
                                                        <asp:TextBox ID="txtClienteDespacho" runat="server" CssClass="Text3" Width="250px" MaxLength="200"></asp:TextBox></td>
                                                    <td class="Text"></td>
                                                    <td class="Text"></td>
                                                    <td class="Text">
                                                        <asp:TextBox ID="txtDNI" runat="server" CssClass="Text" Width="80px" Visible="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" width="17%">Fecha de recojo</td>
                                                    <td class="Text" width="1%">:</td>
                                                    <td class="Text" width="44%">
                                                        <input id="txtFechaRecojo" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaRecojo" runat="server">
                                                        <input id="btnFechaVenc" class="text" value="..."
                                                            type="button" name="btnFecha"><asp:TextBox ID="txtPlaca" runat="server" CssClass="Text3" Width="120px" Visible="False"></asp:TextBox></td>
                                                    <td class="Text" width="10%"></td>
                                                    <td class="Text" width="1%"></td>
                                                    <td class="Text" width="27%"></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" width="17%">N�m. pedido Cliente</td>
                                                    <td class="Text" width="1%">:</td>
                                                    <td class="Text" colspan="4">
                                                        <asp:TextBox ID="txtReferencia" runat="server" CssClass="Text3" Width="248px" MaxLength="40"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" width="17%">Retirado Por</td>
                                                    <td class="Text" width="1%">:</td>
                                                    <td class="Text" colspan="4">
                                                        <asp:TextBox ID="txtRetitradoPor" runat="server" CssClass="Text3" Width="248px" MaxLength="20"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" valign="top" align="left">
                                            <asp:CheckBox ID="chkPreDespacho" runat="server" Text="Con Pre-Despacho *" Visible="False"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td class="Text" valign="top" align="left">
                                            <asp:CheckBox ID="chkImprime" runat="server" Text="Imprime totales" Visible="False" Checked="True"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td class="Text" valign="top" align="left">
                                            <asp:CheckBox ID="chkPagoElec" runat="server" Text="Con Pago Electr�nico" Visible="False"></asp:CheckBox></td>
                                    </tr>
                                    <tr>
                                        <td class="Text" valign="top" align="right">
                                            <asp:HyperLink ID="hlkRegresar" runat="server" CssClass="Text" NavigateUrl="AlmPedidoNuevo.aspx"
                                                Target="_parent">Regresar p�gina anterior</asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80"></td>
                                                </tr>
                                            </table>
                                            <asp:Literal ID="ltrEnviar" runat="server"></asp:Literal></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
