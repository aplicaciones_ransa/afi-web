<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AyudaOnLine.aspx.vb" Inherits="AyudaOnLine" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AyudaOnLine</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<!--<script language="VBScript">
		Option Explicit
		Const Title = "Descarga de CAPICOM 2"
		Const CAPICOMdnld = "http://www.microsoft.com/downloads/release.asp?ReleaseID=44155"
		Const CAPICOM_LOCAL_MACHINE_STORE	= 1
		Const CAPICOM_STORE_OPEN_READ_ONLY = 0

		Sub StartUpCheck()
		If  NOT isCapicomAvailable Then
		MsgBox "CAPICOM 2 no pudo instalarse" & vbCrLf & vbCrLf & _
		"Contáctese con su Delegación de Informática", _
		vbInformation, Title
		Exit Sub
		else
		MsgBox "CAPICOM 2 está instalada en su computadora.", _
		vbInformation, Title
		Exit Sub
		End If
		End Sub

		'******************************************************************************
		' Function  : isCapicomAvailable
		' is CAPICOM installed?
		'
		Function isCapicomAvailable()
		Dim oStore
		On Error Resume Next
		Set oStore = CreateObject("CAPICOM.Store")
		oStore.Open CAPICOM_LOCAL_MACHINE_STORE, "Root", CAPICOM_STORE_OPEN_READ_ONLY
		If Err.Number <> 0 Then
		isCapicomAvailable = False
		Exit Function
		End If
		isCapicomAvailable = True
		Set oStore = Nothing
		On Error GoTo 0
		End Function
		</script>-->
		<form id="Form1" method="post" runat="server">
			<!--<OBJECT id="oCertificate" border="0" codeBase="Manuales/capicom.cab" classid="clsid:E38FD381-6404-4041-B5E9-B2739258941F"
				width="0" align="baseline" height="0" VIEWASTEXT>
			</OBJECT>-->
			<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<TABLE id="Table1" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" background="Images/Login1.JPG"
							border="0">
							<TR>
								<TD class="Titulo" vAlign="middle" align="center" height="60"><FONT face="Tahoma" size="5"><FONT face="Tahoma" size="5"></FONT>Preguntas 
										Frecuentes</FONT></TD>
							</TR>
							<TR>
								<TD class="Titulo" vAlign="middle" align="center">
									<TABLE id="Table2" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
										<TR>
											<TD width="100"></TD>
											<TD>
												<UL>
													<LI>
														<!--<A href="Manuales/Manual de Instalacion - Usuario.pdf" target="_blank"><FONT face="Tahoma" size="2">
																<STRONG>Configuración para usuarios aprobadores</STRONG></FONT></A>
													<LI>-->
														<A href="Manuales/warrant.pdf" target="_blank"><FONT face="Tahoma" size="2"><STRONG>Manual 
																	de usuario - Warrant</STRONG></FONT></A>
													<LI>
														<A href="Manuales/almacen.pdf" target="_blank"><FONT face="Tahoma" size="2"><STRONG>Manual 
																	de Usuario - Almacén</STRONG></FONT></A>
													</LI>
												</UL>
											</TD>
											<TD width="100"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<!--<TR>
								<TD class="Titulo" vAlign="middle" align="center" height="60"><FONT face="Tahoma" size="5"><FONT face="Tahoma" size="5"></FONT>Descargas</FONT></TD>
							</TR>
							<TR>
								<TD class="Titulo" vAlign="middle" align="center">
									<TABLE id="Table4" cellSpacing="1" cellPadding="0" width="100%" align="left" border="0">
										<TR>
											<TD width="100"></TD>
											<TD>
												<UL>
													<LI>
														<A href="Manuales/capicom.dll"><FONT face="Tahoma" size="2"><STRONG>Capicom.dll</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Contrato operaciones electronicas.pdf" target="_blank"><FONT face="Tahoma" size="2">
																<STRONG>Contrato de operaciones electrónicas</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Solicitud de Certicámara.pdf" target="_blank"><FONT face="Tahoma" size="2">
																<STRONG>Solicitud de Certicámara</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Driver.zip"><FONT face="Tahoma" size="2"><STRONG>Driver 2032 para Vista </STRONG>
															</FONT></A>
													<LI>
														<A href="Manuales/PARA XP.zip"><FONT face="Tahoma" size="2"><STRONG>Driver 2032 para XP </STRONG>
															</FONT></A>
													<LI>
														<A href="Manuales/Driver iKey 1000.zip"><FONT face="Tahoma" size="2"><STRONG>Driver Ikey 
																	1000</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Driver Ikey 2000.zip"><FONT face="Tahoma" size="2"><STRONG>Driver Ikey 
																	2000</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Ikey 2032 y 4000.zip"><FONT face="Tahoma" size="2"><STRONG>Ikey 2032 y 
																	4000 (XP, Vista)</STRONG></FONT></A>
													<LI>
														<A href="Manuales/Raices.zip"><FONT face="Tahoma" size="2"><STRONG>Raices</STRONG></FONT></A>
													</LI>
												</UL>
											</TD>
											<TD width="100"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>-->
							<TR>
								<TD vAlign="middle" align="center"><INPUT style="Z-INDEX: 0" id="btnCerrar" class="btn" onclick="window.close();" value="Cerrar"
										type="button" name="btnCerrar"></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="center" height="150">
									<asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
						<uc1:Footer id="Footer2" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
