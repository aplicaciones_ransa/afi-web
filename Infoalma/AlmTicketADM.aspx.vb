Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AlmTicketADM
    Inherits System.Web.UI.Page
    Private objTicket As clsCNTicket
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "TICKETS_BALANZA_ADM") Then
            Me.lblOpcion.Text = "Tickets de ADM"
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "TICKETS_BALANZA_ADM"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Now.ToShortDateString & "')== false) return false;")
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        pr_REPO_0001()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "TICKETS_BALANZA_ADM", "CONSULTA DE TICKETS ADM", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub pr_REPO_0001()
        Try
            objTicket = New clsCNTicket
            Dim trabajador As String
            Dim entidad As String

            Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"),
                                              Me.txtFechaInicial.Value, Me.txtFechaFinal.Value)

            Me.dgResultado.DataSource = dtTCALMA_MERC
            Me.dgResultado.DataBind()

        Catch e1 As Exception
            pr_IMPR_MENS(e1.Message)
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        pr_REPO_0002()
    End Sub

    Private Sub pr_REPO_0002()
        Try
            objTicket = New clsCNTicket
            Dim dtTCALMA_MERC As DataTable = objTicket.fn_TCBALA_Sel_ReportePesadas(Session("CoEmpresa"), Session("IdSico"),
                                    Me.txtFechaInicial.Value, Me.txtFechaFinal.Value)

            Dim sb As New System.Text.StringBuilder

            For i As Integer = 0 To dtTCALMA_MERC.Rows.Count - 1
                Dim dr As DataRow = dtTCALMA_MERC.Rows(i)
                For y As Integer = 0 To dtTCALMA_MERC.Columns.Count - 1
                    sb.Append(dr.ItemArray(y).ToString)
                    If y <> dtTCALMA_MERC.Columns.Count - 1 Then
                        sb.Append("|")
                    End If
                Next
                sb.Append(ControlChars.CrLf)
            Next

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ldetpt.txt")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.Private)
            Response.ContentType = "application/vnd.text"
            Dim stringWrite As System.IO.StreamWriter
            Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
            Response.Write(sb)
            Response.End()
        Catch e1 As Exception
            Dim sCA_ERRO As String = e1.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objTicket Is Nothing) Then
            objTicket = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
