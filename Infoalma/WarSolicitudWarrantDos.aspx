<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarSolicitudWarrantDos.aspx.vb" Inherits="WarSolicitudWarrantDos" %>

<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarSolicitudWarrants</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {
            //if(parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value))
            //  {
            //   alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
            //   document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
            //   return;
            //  }				  				  
            document.getElementById(Monto).value = formatearDecimal(parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value), 3);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }

        function formatearDecimal(numero, cantDecimales) {
            if (cantDecimales > 0) {
                var i = 0;
                var aux = 1;
                for (i = 0; i < cantDecimales; i++) {
                    aux = aux * 10;
                }
                numero = numero * aux;
                numero = Math.round(numero);
                numero = numero / aux;
            }
            return numero;
        }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 23) { //para rel�gio de 12 horas altere o valor aqui
                alert("Horas inv�lidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("Minutos inv�lidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inv�lidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
    <script language="javascript">		
            function Todos(ckall, citem) {
                var actVar = ckall.checked;
                for (i = 0; i < Form1.length; i++) {
                    if (Form1.elements[i].type == "checkbox") {
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
    </script>

</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <table class="auto-style12">
                                    <tr>
                                        <td>
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                            <table id="Table1" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr >
                                                    <td class="text">
                                            <table id="Table20" cellspacing="0" cellpadding="0" width="750" align="center" border="0">
                                                <tr>
                                                    <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                                    <td background="Images/table_r1_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r2_c1.gif"></td>
                                                    <td align="center" >
                                                        <table id="Table6" cellspacing="4" cellpadding="0" border="0" class="auto-style13">
                                                            <tr>
                                                                <td class="Text">Estado</td>
                                                                <td>:</td>
                                                                <td>
                                                        <asp:DropDownList ID="cboEstado" runat="server" CssClass="Text"  Width="200px">
                                                            <asp:ListItem Value="0">--- TODOS ---</asp:ListItem>
                                                            <asp:ListItem Value="1" Selected="True">PENDIENTE DE ENVIAR</asp:ListItem>
                                                            <asp:ListItem Value="2">SOLICITUD ENVIADA</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text">Endosatario</td>
                                                                <td class="Text" align="center">:</td>
                                                                <td class="auto-style14" align="left">
                                                        <asp:DropDownList ID="cboFinanciador" runat="server" CssClass="Text"  Width="350px"></asp:DropDownList></td>
                                                            </tr>
                                                            </table>
                                                    </td>
                                                    <td width="6" background="Images/table_r2_c3.gif"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                                    <td background="Images/table_r3_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                                </tr>
                                            </table>
                                                    </td>
                                                </tr>
                                                </table>
                                        <br />
                                <br />
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td>
                                    <asp:Button class="btn" ID="btnBuscar" runat="server"  Text="Buscar" Width="170px" BorderStyle="None" BackColor="#22A458" ForeColor="White" Font-Bold="True" />
                                        </td>
                                        <td class="auto-style10">
                                    <asp:Button class="btn" ID="btnSolicitud" runat="server"  Text="Nueva Solicitud" Width="170px" BorderStyle="None" ForeColor="White" BackColor="#3968C6" Font-Bold="True" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="200" valign="top" align="center">
   
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AutoGenerateColumns="False"
                                    BorderWidth="1px" >                                  
                                 <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                             <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <Columns>
                                        <asp:BoundColumn  DataField="NRO_DOCU" HeaderText="Nro.Docu">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="NOM_TIPDOC" HeaderText="Tipo Sol.">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_ALMA" HeaderText="Almac�n"></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_FINA" HeaderText="Endosatario"></asp:BoundColumn>
                                        <%--asp:BoundColumn  DataField="DE_REGI" HeaderText="R�gimen"></asp:BoundColumn>--%>
                                        <asp:BoundColumn  DataField="DE_MONE" HeaderText="Moneda">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_ESTA" HeaderText="Estado">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="FCH_CREA" HeaderText="Fecha de registro">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>      
                                        <asp:BoundColumn DataField="VAL_ENDO" HeaderText="Valor" DataFormatString="{0:N3}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="TIPO_ENDO" HeaderText="Tipo Endoso">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                       <asp:BoundColumn  DataField="COD_DOC" HeaderText="Nro.Docu" Visible="false"></asp:BoundColumn>
                                       <asp:BoundColumn  DataField="NRO_LIBE" HeaderText="Nro.Lib" Visible="false"></asp:BoundColumn>                             
                                       <asp:BoundColumn  DataField="COD_TIPDOC" HeaderText="Tipo.Doc" Visible="false"></asp:BoundColumn>
                                       <asp:BoundColumn  DataField="ENT_FINA" HeaderText="Cod.Fina" Visible="false"></asp:BoundColumn>
                                       <asp:BoundColumn  DataField="NRO_SECU" HeaderText="Tipo.Doc" Visible="false"></asp:BoundColumn>
                                       <asp:TemplateColumn HeaderText="Envi.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEnviar" OnClick="imgEnviar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Enviar" ImageUrl="Images/Enviar.png"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                              <asp:TemplateColumn HeaderText="Edit.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Editar" ImageUrl="Images/Editar.png"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                             <asp:TemplateColumn HeaderText="Elim.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.png"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
