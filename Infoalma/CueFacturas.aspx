<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueFacturas.aspx.vb" Inherits="CueFacturas" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Facturas</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function AbrirDCR(sNU_COMP) {
            //window.showModalDialog("AlmFacturaDetalle.aspx","Retiro","dialogHeight:280px; dialogWidth:740px; edge: Raised; center: Yes; help: No; resizable=No; status: No");
            var ventana = 'Popup/AlmFacturaDetalle.aspx?sNU_COMP=' + sNU_COMP
            window.showModalDialog(ventana, window, "dialogHeight:380px; dialogWidth:740px; edge: Raised; center: Yes; help: No; resizable=No; status: No");
        }

        function AbrirAlmDCR(sTI_DOCU_RECE, sNU_DOCU_RECE, sCO_UNID) {
            var ventana = 'Popup/AlmMovimientoDetalle.aspx?sTI_DOCU_RECE=' + sTI_DOCU_RECE + '&sNU_DOCU_RECE=' + sNU_DOCU_RECE + '&sCO_UNID=' + sCO_UNID;
            window.open(ventana, 'Retiro', 'left=240,top=200,width=800,height=600,toolbar=0,resizable=1');
        }

        function AbrirFactura(sCO_UNID, sTI_DOC, sNU_FACT) {
            var ventana = 'Popup/CueImpresionDetalleDoc.aspx?sCO_UNID=' + sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOC + '&sNU_DOCU_COBR=' + sNU_FACT;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }
        function AbrirOrden(sCO_UNID, sNU_DOC) {
            var ventana = 'Popup/CueFacturasOrdenTrabajo.aspx?sCO_UNID=' + sCO_UNID + '&sNU_DOC=' + sNU_DOC
            window.showModalDialog(ventana, window, "dialogHeight:320px; dialogWidth:700px; edge: Raised; center: Yes; help: No; resizable=No; status: No");
            //window.open("CueFacturasOrdenTrabajo.aspx","Orden","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=700,height=280");
        }

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 1540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);
            if (fchIni == "" && fchFin == "") {
                alert("Debe Ingresar fecha Inicial y Final");
                return false;
            }

            if (fchIni > fchFin && fchIni != "" && fchFin != "") {
                alert("La fecha inicial no puede ser mayor a la fecha final");
                return false;
            }
            return true;
        }
        function AbrirDetalle(sCO_EMPR, sCO_UNID, sNU_DOC, sCO_CLIE) {
            var ventana = 'Popup/CueImpresionDetalleDOT.aspx?sCO_EMPR=' + sCO_EMPR + '&sCO_UNID=' + sCO_UNID + '&sNU_DOC=' + sNU_DOC + '&sCO_CLIE=' + sCO_CLIE;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }
        function AbrirDOT(sCO_EMPR, sCO_UNID, sNU_DOC, sCO_CLIE, sCO_ALMA) {
            var ventana = 'Popup/CueImpresionDetalleDOT.aspx?sCO_EMPR=' + sCO_EMPR + '&sCO_UNID=' + sCO_UNID + '&sNU_DOC=' + sNU_DOC + '&sCO_CLIE=' + sCO_CLIE + '&sCO_ALMA=' + sCO_ALMA;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }

        function AbrirDetalleWAR(sCO_EMPR, sCO_UNID, sNU_DOC, sTI_DOCU) {
            var ventana = 'Popup/CueImpresionDetalleWAR.aspx?sCO_EMPR=' + sCO_EMPR + '&sCO_UNID=' + sCO_UNID + '&sNU_DOC=' + sNU_DOC + '&sTI_DOCU=' + sTI_DOCU;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }

        function AbrirSustentoDCR(sCO_UNID, sTI_DOC, sNU_FACT) {
            var ventana = 'Popup/CueImpresionSustentoDCR.aspx?sCO_UNID=' + sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOC + '&sNU_DOCU_COBR=' + sNU_FACT;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
            //window.open("CueFacturasDetalle.aspx","Factura","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=740,height=350");
        }


    </script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table2" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table4" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="3">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table6" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table id="Table7" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text">Unidad</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text">
                                                                    <asp:DropDownList ID="cboUnidad" runat="server" CssClass="Text" DataTextField="descripcion" DataValueField="codigo"></asp:DropDownList></td>
                                                                <td class="text"></td>
                                                                <td class="text"></td>
                                                                <td class="text">Fecha Emisi�n</td>
                                                                <td class="text" width="1%">:</td>
                                                                <td class="text">
                                                                    <table id="Table5" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="Text">Del :</td>
                                                                            <td>
                                                                                <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaInicial" runat="server"><input id="btnFechaInicial" class="text" value="..."
                                                                                        type="button" name="btnFecha"></td>
                                                                            <td class="Text">Al&nbsp;:</td>
                                                                            <td class="Text">
                                                                                <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaFinal" runat="server"><input id="btnFechaFinal" class="text" value="..."
                                                                                        type="button" name="btnFecha"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">N� Factura</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text">
                                                                    <asp:TextBox ID="txtNroFactura" runat="server" CssClass="Text" Width="150px"></asp:TextBox></td>
                                                                <td class="text"></td>
                                                                <td class="text" width="1%"></td>
                                                                <td class="text">DCR</td>
                                                                <td class="text" width="1%">:</td>
                                                                <td class="text">
                                                                    <asp:TextBox ID="txtDCR" runat="server" CssClass="Text" Width="120px"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <input id="btnBuscar" class="btn" onclick="Javascript: if (ValidarFechas() == false) return false;"
                                                            value="Buscar" type="button" name="btnBuscar" runat="server"></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgTCDOCU_CLIE" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" AllowSorting="True" PageSize="100" AllowPaging="True">
                                                <AlternatingItemStyle BorderColor="White" CssClass="gvRowAlter" VerticalAlign="Top"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow" VerticalAlign="Top"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn Visible="false" DataField="DE_DOCU">
                                                        <HeaderStyle Width="5px"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn Visible="true" HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinFactutraPDF" runat="server" Visible="false"></asp:LinkButton>&nbsp;
																<asp:LinkButton ID="lnkExportarXML" runat="server" Visible="false" OnClick="lnkExportarXML_Click">DESCARGAR XML</asp:LinkButton>&nbsp;
																<asp:LinkButton ID="lnkSustentoDCR" runat="server" Visible="false">&nbsp;SUSTENTO&nbsp;DCR</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="TI_COMP" HeaderText="Tipo Documento">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_COMP" HeaderText="N&#250;mero Documento"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="NU_SECU" HeaderText="Item">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_SSER" HeaderText="Concepto"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC" HeaderText="Dec.Mercader&#237;a">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="FE_EMIS" HeaderText="Fecha emisi&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_INIC_PERI" HeaderText="Fecha Ini. Periodo">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_FINA_PERI" HeaderText="Fecha fin Periodo">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID" HeaderText="Valor/Cantidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="DE_TIPO_BULT" HeaderText="Tipo Bulto">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NO_SIMB" HeaderText="Mone.">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MO_VALO" HeaderText="Val.Mercader&#237;a" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_MONE" HeaderText="Mon.">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MO_TARI" HeaderText="Tari." DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_BRUT_AFEC" HeaderText="Afecto">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MO_TOTA" HeaderText="Afecto+IGV" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CO_ALMA" HeaderText="Almac.">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="CO_EMPR" HeaderText="CoEmpr">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="CO_UNID" HeaderText="CoUnid">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="CO_CLIE" HeaderText="CoClie">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="FLG_XML" HeaderText="flgXML"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="NU_DOCU_XML" HeaderText="CodDocuXML"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
