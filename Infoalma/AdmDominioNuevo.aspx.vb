Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AdmDominioNuevo
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objParametro As clsCNParametro
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtval1 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtval2 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtval3 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtval4 As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodDomin As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDescDomin As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnNuevo As System.Web.UI.WebControls.Button
    'Protected WithEvents hlkRegresar As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        Me.lblOpcion.Text = "Nuevo Dominio"
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_PARAMETROS") Then
            If Page.IsPostBack = False Then
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                Me.btnNuevo.Visible = False
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_PARAMETROS"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("CodDomin") <> "" Then
                    Me.lblOpcion.Text = "Modificar Dominio"
                    Me.btnGrabar.Text = "Actualizar"
                    CargarDatosDominio(Val(Session.Item("CodDomin")))
                    Me.btnNuevo.Visible = True
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        objParametro = New clsCNParametro

        If Me.txtCodDomin.Text = "" Then
            lblError.Text = "Debe ingresar un c�digo de dominio"
            Exit Sub
        Else
            If Me.btnGrabar.Text = "Grabar" Then
                Try
                    With objParametro
                        If ValidarDominio() Then
                            .gCNInsertarDominio(Me.txtCodDomin.Text, Me.txtDescDomin.Text, Me.txtval1.Text, Me.txtval2.Text,
                        Me.txtval3.Text, Me.txtval4.Text, Session.Item("IdUsuario"))
                            If .fn_devuelveResultado = "X" Then
                                Me.lblError.Text = "Se insert� correctamente el dominio"
                                Me.btnGrabar.Text = "Actualizar"
                                Me.btnNuevo.Visible = True
                                CapturarCodigo()
                            End If
                        Else
                            Me.lblError.Text = "Ya existe el c�digo,ingrese otro."
                        End If
                    End With
                Catch ex As Exception
                    Me.lblError.Text = "No se pudo insertar el dominio"
                    Me.lblError.Text = ex.Message
                End Try
            ElseIf Me.btnGrabar.Text = "Actualizar" Then
                Try
                    With objParametro
                        .gCNActualizarDominio(Val(Session.Item("CodDomin")), Me.txtCodDomin.Text, Me.txtDescDomin.Text, Me.txtval1.Text,
                                Me.txtval2.Text, Me.txtval3.Text, Me.txtval4.Text, Session.Item("IdUsuario"))
                        If .fn_devuelveResultado = "X" Then
                            Me.lblError.Text = "Se Modific� correctamente el dominio"
                        End If
                    End With
                Catch ex As Exception
                    Me.lblError.Text = "No se pudo modificar el dominio"
                    Me.lblError.Text = ex.Message
                End Try
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "M", "MAESTRO_PARAMETROS", "ACTUALIZAR DOMINIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        End If
    End Sub

    Private Sub CargarDatosDominio(ByVal intco_domin)
        objParametro = New clsCNParametro
        Dim dtTemporal As DataTable
        Dim intindice As Integer

        Try
            With objParametro
                .gCNMostrarDominios("2", intco_domin)
                dtTemporal = .fn_devuelveDataTable
            End With
            If dtTemporal.Rows.Count > 0 Then
                Me.txtCodDomin.Text = dtTemporal.Rows(0)("NO_DOMIN").trim
                Me.txtDescDomin.Text = dtTemporal.Rows(0)("DE_LARG_DOMIN").trim
                Me.txtval1.Text = dtTemporal.Rows(0)("VALOR1").trim
                Me.txtval2.Text = dtTemporal.Rows(0)("VALOR2").trim
                Me.txtval3.Text = dtTemporal.Rows(0)("VALOR3").trim
                Me.txtval4.Text = dtTemporal.Rows(0)("VALOR4").trim
            End If

        Catch ex As Exception
            Me.lblError.Text = "No se pudieron mostrar los dominios"
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub LimpiarDatos()
        Me.txtCodDomin.Text = ""
        Me.txtDescDomin.Text = ""
        Me.txtval1.Text = ""
        Me.txtval2.Text = ""
        Me.txtval3.Text = ""
        Me.txtval4.Text = ""
        Me.lblError.Text = ""
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.btnGrabar.Text = "Grabar"
        LimpiarDatos()
    End Sub

    Private Function ValidarDominio() As Boolean
        objParametro = New clsCNParametro
        Dim dt As DataTable
        With objParametro
            .gCNValidarDominio(Me.txtCodDomin.Text.Trim)
            dt = .fn_devuelveDataTable
            If dt.Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If
        End With
    End Function

    Private Sub CapturarCodigo()
        objParametro = New clsCNParametro
        Dim dt As DataTable
        With objParametro
            .gCNValidarDominio(Me.txtCodDomin.Text.Trim)
            dt = .fn_devuelveDataTable
            Session.Item("CodDomin") = dt.Rows(0)("CO_DOMIN")
        End With
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
