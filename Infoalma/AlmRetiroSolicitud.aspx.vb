Imports Depsa.LibCapaNegocio
Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Infodepsa
Imports System.Data

Public Class AlmRetiroSolicitud
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objRetiro As clsCNRetiro
    Private objConexion As SqlConnection
    Private objTransaccion As SqlTransaction
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Dim CO_MODA_MOVI As String
    Private sDE_MEN1, sDE_MEN2 As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgPedido As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCiaTransporte As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtChofer As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDNI As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtBrevete As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtPlaca As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkPreDespacho As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnEnviar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkRegresar As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents chkImprime As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtDestino As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents chkPagoElec As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        PoliticaCache()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Solicitud de Retiro"
                Me.btnEnviar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de enviar la solicitud?')== false) return false;")
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_NUEVO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Bindatagrid()
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dtRetiro As DataTable
            objRetiro = New clsCNRetiro
            dtRetiro = CType(Session("dt"), DataTable)
            If dtRetiro Is Nothing Then
            Else
                If dtRetiro.Rows.Count > 0 Then
                    If objRetiro.gCNGetPredespacho(Session.Item("CoEmpresa"), dtRetiro.Rows(0)("CO_UNID"),
                                                                  dtRetiro.Rows(0)("CO_ALMA_ACTU"), Session.Item("IdSico")) > 0 Then
                        Me.chkPreDespacho.Enabled = True
                    Else
                        Me.chkPreDespacho.Enabled = False
                    End If

                    If dtRetiro.Rows(0)("CO_MODA_MOVI") = "Simple" Then
                        Me.chkPagoElec.Visible = False
                    Else
                        Me.chkPagoElec.Visible = True
                    End If
                End If
            End If
            Me.dgPedido.DataSource = dtRetiro
            Me.dgPedido.DataBind()
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            Dim dt As DataTable
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            dt = CType(Session("dt"), DataTable)
            dt.Rows.RemoveAt(dgi.ItemIndex)
            Bindatagrid()
            Me.lblMensaje.Text = "Se encontraron " & Me.dgPedido.Items.Count & " Registros"
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub dgPedido_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPedido.ItemDataBound
        Dim btnCancelar As New ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            btnCancelar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
            btnCancelar.Attributes.Add("onclick", "javascript:if(confirm('Desea quitar el DCR.Item Nro " & e.Item.Cells(0).Text & "." & e.Item.Cells(1).Text & "?')== false) return false;")
        End If
    End Sub

    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        If Me.dgPedido.Items.Count = 0 Then
            Me.lblMensaje.Text = "Agregue por lo menos un Item a retirar"
            Exit Sub
        End If
        Dim dtRetiro As DataTable
        Dim nIM_TOTA As Decimal
        Dim nIM_UNIT As Decimal
        Dim nNU_UNID_RETI As Decimal
        Dim sCO_MONE As String
        Dim sCO_UNID As String
        Dim sTI_DOCU_RECE As String
        Dim sNU_DOCU_RECE As String
        Dim sST_VALI As String = "S"

        dtRetiro = CType(Session.Item("dt"), DataTable)
        For i As Integer = 0 To dtRetiro.Rows.Count - 1
            nNU_UNID_RETI = dtRetiro.Rows(i)("NU_UNID_RETI")
            nIM_UNIT = dtRetiro.Rows(i)("IM_UNIT")
            nIM_TOTA += CType(nNU_UNID_RETI, Decimal) * nIM_UNIT
            sCO_MONE = dtRetiro.Rows(i)("CO_MONE")
            sCO_UNID = dtRetiro.Rows(i)("CO_UNID")
            CO_MODA_MOVI = dtRetiro.Rows(i)("CO_MODA_MOVI")
            sTI_DOCU_RECE = dtRetiro.Rows(i)("TI_DOCU_RECE")
            sNU_DOCU_RECE = dtRetiro.Rows(i)("NU_DOCU_RECE")
        Next

        Dim objSICO As SICO = New SICO
        sDE_MEN1 = ""
        sDE_MEN2 = ""
        Dim sST_EJEC_RETI As String = objSICO.fn_TMCLIE_MERC_Q01(Session("CoEmpresa"), sCO_UNID, Session("IdSico"), sCO_MONE, nIM_TOTA, "", "0")

        If sST_EJEC_RETI = "N" Then
            sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), sCO_UNID, sTI_DOCU_RECE,
                            sNU_DOCU_RECE, "1", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "", "", "0", "0", "0", "0", "0")
            sST_VALI = "N"
            'pr_IMPR_MENS(sDE_MEN1)
            'Exit Sub
        End If
        If sST_EJEC_RETI = "X" Then
            sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), sCO_UNID, sTI_DOCU_RECE,
                              sNU_DOCU_RECE, "9", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "", "", "0", "0", "0", "0", "0")
            sST_VALI = "N"
            'pr_IMPR_MENS(sDE_MEN1)
            'Exit Sub
        End If
        '--validacion por antiguedad
        Dim sFE_ACTU As String = FormatDateTime(Now(), DateFormat.ShortDate)
        Dim dsTCDOCU_CLIE As DataSet = objSICO.fn_TCDOCU_CLIE_Q08(Session("CoEmpresa"), sCO_UNID, sFE_ACTU, Session("IdSico"))
        If dsTCDOCU_CLIE.Tables(0).Rows(0)("ST_ESTA") = 1 Then
            sDE_MEN2 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), sCO_UNID, sTI_DOCU_RECE,
                                    sNU_DOCU_RECE, "2", sCO_MONE, nIM_TOTA, Session("UsuarioLogin"), "N", "", "", "0", "0", "0", "0", "0")
            sST_VALI = "N"
            'pr_IMPR_MENS(sDE_MEN2)
            'Exit Sub
        End If
        IngresarRetiro(dtRetiro, sCO_UNID, sST_VALI)
    End Sub

    Private Sub IngresarRetiro(ByVal dt As DataTable, ByVal strCodUnidad As String, ByVal sST_VALI As String)
        'Dim strNombreReporte1 As String
        'objRetiro = New clsCNRetiro
        'strNombreReporte1 = strPath & strCodUnidad & "10000000" & ".PDF"
        'objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", "1000000", Session.Item("IdSico"), CType(Session.Item("dt"), DataTable), strPathFirmas, strNombreReporte1, Session.Item("UsuarioLogin"), Me.chkImprime.Checked, Session.Item("IdSico"), UCase(Me.txtDestino.Text))
        'Response.Redirect("AlmPedidoNuevo.aspx", False)
        'Return
        Dim dtMail As New DataTable
        Dim strNombreCliente As String
        Dim strEmails As String
        Dim strNroRetiro As String
        Dim strNombreReporte As String
        objRetiro = New clsCNRetiro
        objConexion = New SqlConnection(strCadenaConexion)
        objConexion.Open()
        objTransaccion = objConexion.BeginTransaction
        Try
            If objRetiro.gCNAddItemRetiro(Session.Item("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdUsuario"), dt, objTransaccion) = False Then
                Me.lblMensaje.Text = "No se pudo adicionar los items seleccionados"
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                Exit Sub
            Else
                'objTransaccion.Commit()
                'objTransaccion.Dispose()
                strNroRetiro = objRetiro.gCNCrearRetiro(Session.Item("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdUsuario"),
                                                        Me.txtBrevete.Text, Me.txtChofer.Text, UCase(Me.txtCiaTransporte.Text),
                                                        Me.txtDNI.Text, Me.txtPlaca.Text, Me.chkPreDespacho.Checked, IIf(Me.chkPagoElec.Checked, "1", "0"), Me.txtDestino.Text, objTransaccion, sST_VALI)

                If strNroRetiro = "X" Then
                    Me.lblMensaje.Text = "Se est� efectuando un retiro sobre este C / R, debe volver a Cargar Datos"
                    objTransaccion.Rollback()
                    objTransaccion.Dispose()
                    Exit Sub
                Else
                    'objTransaccion.Commit()
                    'objTransaccion.Dispose()
                    Session.Add("strNroRetiro", strNroRetiro + "\n\n" + sDE_MEN1 + " \n\n" + sDE_MEN2)

                    If CO_MODA_MOVI = "Simple" Then
                        strNombreReporte = strPath & strCodUnidad & strNroRetiro & ".PDF"
                        strNombreCliente = objRetiro.GetReporteRetiro(Session.Item("CoEmpresa"), "DOR", strNroRetiro, Session.Item("IdSico"), CType(Session.Item("dt"), DataTable),
                        strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), Me.chkImprime.Checked, Session.Item("IdSico"), UCase(Me.txtDestino.Text), sST_VALI)
                    Else
                        strNombreReporte = strPath & strCodUnidad & strNroRetiro & ".PDF"
                        strNombreCliente = objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", strNroRetiro, Session.Item("IdSico"), CType(Session.Item("dt"), DataTable),
                        strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), Me.chkImprime.Checked, Session.Item("IdSico"), UCase(Me.txtDestino.Text), sST_VALI)
                    End If

                    dtMail = objUsuario.gGetEmailAprobadores("00000001", objRetiro.strCodAlmacen, CO_MODA_MOVI)
                    If dtMail.Rows.Count > 0 Then
                        For i As Integer = 0 To dtMail.Rows.Count - 1
                            If dtMail.Rows(i)("EMAI") <> "" Then
                                strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                            End If
                        Next
                    End If

                    dtMail = objUsuario.gGetEmailAprobadores(Session.Item("IdSico"), "", CO_MODA_MOVI)
                    If dtMail.Rows.Count > 0 Then
                        For i As Integer = 0 To dtMail.Rows.Count - 1
                            If dtMail.Rows(i)("EMAI") <> "" Then
                                strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                            End If
                        Next
                    End If

                    If CO_MODA_MOVI = "Simple" Then
                        If objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & strEmails & Session.Item("dir_emai"),
                            strNombreCliente & " -  Orden de Retiro " & CO_MODA_MOVI & " Nro: " & strNroRetiro,
                            "Se ha emitido el Retiro  " & CO_MODA_MOVI & " Nro: <STRONG><FONT color='#330099'> " & strNroRetiro & "</FONT></STRONG><br>" &
                            "Generado por: <STRONG><FONT color='#330099'>" & UCase(Session.Item("UsuarioLogin")) & "</FONT></STRONG><br>" &
                            "<br><br>Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreReporte) Then
                            objTransaccion.Commit()
                            objTransaccion.Dispose()
                        Else
                            objTransaccion.Rollback()
                            objTransaccion.Dispose()
                        End If
                    Else
                        If objFunciones.SendMail("Alma Per�� Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & strEmails & Session.Item("dir_emai"),
                            strNombreCliente & " -  Orden de Retiro " & CO_MODA_MOVI & " Nro: " & strNroRetiro,
                            "Se ha emitido el Retiro  " & CO_MODA_MOVI & " Nro: <STRONG><FONT color='#330099'> " & strNroRetiro & "<br> </FONT></STRONG>" &
                            "Generado por: <STRONG><FONT color='#330099'> " & UCase(Session.Item("UsuarioLogin")) & "</FONT></STRONG><br>" &
                            "Con Pago Electr�nico: <STRONG><FONT color='#330099'> " & IIf(Me.chkPagoElec.Checked, "Si", "No") & "</FONT></STRONG><br>" &
                            "<br><br>Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreReporte) Then

                            objTransaccion.Commit()
                            objTransaccion.Dispose()
                        Else
                            objTransaccion.Rollback()
                            objTransaccion.Dispose()
                        End If
                    End If
                End If
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "C", "RETIROS_LINEA", "ENVIAR NUEVO RETIRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Session.Remove("dt")
            Response.Redirect("AlmPedidoNuevo.aspx", False)
        Catch ex As Exception
            objTransaccion.Rollback()
            objTransaccion.Dispose()
            Me.lblMensaje.Text &= " " & ex.Message
        Finally
            objConexion.Close()
            objConexion.Dispose()
            objConexion = Nothing
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BuscaTranp()
    End Sub

    Private Sub BuscaTranp()
        Try
            objRetiro = New clsCNRetiro
            Dim strScript As String
            Dim dtRetiro As DataTable
            dtRetiro = objRetiro.gCNGetBuscarTranp(txtBrevete.Text)

            If txtBrevete.Text = "" Then
                strScript = "<script language='javascript'> alert('Ingrese Nro Brevete'); </script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If
            If dtRetiro.Rows.Count >= 1 Then
                Me.txtChofer.Text = dtRetiro.Rows(0).Item("NO_CHOF").ToString
                Me.txtDNI.Text = dtRetiro.Rows(0).Item("NU_DNNI").ToString
                Me.txtPlaca.Text = dtRetiro.Rows(0).Item("NU_PLAC").ToString
            Else
                strScript = "<script language='javascript'> alert('No existe Nro Brevete'); </script>"
                'Page.RegisterStartupScript("Open", strScript)
                ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
                Exit Sub
            End If

        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

End Class
