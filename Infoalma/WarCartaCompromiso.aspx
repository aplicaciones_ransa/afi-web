<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarCartaCompromiso.aspx.vb" Inherits="WarCartaCompromiso"%>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WarCartaCompromiso</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript">		
		function Formato(Campo, teclapres){
			var tecla = teclapres.keyCode;
			var vr = new String(Campo.value);  
			vr = vr.replace(".", ""); 
			vr = vr.replace(".", ""); 
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace("/", "");
			vr = vr.replace("-", "");
			vr = vr.replace(" ", "");
			var tam = 0;
			tam = vr.length; 
			if (tam > 2 && tam < 6){
				Campo.value = vr.substr(0,tam-2) + '.' + vr.substr(tam-2,2);
			}
			if (tam >= 6 && tam < 9){
				Campo.value = vr.substr(0,tam-5) + ',' + vr.substr(tam-5,3) + '.' + vr.substr(tam-2,2);
			}
			if (tam >= 9 && tam < 12){
				Campo.value = vr.substr(0,tam-8) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3) + '.' + vr.substr(tam-2,2);
			}
			if (tam >= 12 && tam < 15){
				Campo.value = vr.substr(0,tam-11) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2); 
			}
			if (tam >= 15 && tam < 18){
				Campo.value = vr.substr(0,tam-14) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2);
			}
			if (tam >= 18 && tam < 21){
				Campo.value = vr.substr(0,tam-17) + ',' + vr.substr(tam-17,3) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3) + ',' + vr.substr(tam-5,3)+ '.' + vr.substr(tam-2,2);
			}
			if (tam >= 21){
				Campo.value = vr.substr(0,tam-20) + ',' + vr.substr(tam-20,3) + ',' + vr.substr(tam-17,3) + ',' + vr.substr(tam-14,3) + ',' + vr.substr(tam-11,3) + ',' + vr.substr(tam-8,3)+ '.' + vr.substr(tam-5,2);
			}					 
		}
		</script>
		<script language="javascript">		
			function Todos(ckall, citem) { 
				var actVar = ckall.checked ;
				for(i=0; i<Form1.length; i++) {
					if (Form1.elements[i].type == "checkbox") {
						if (Form1.elements[i].name.indexOf(citem) != -1) {
							Form1.elements[i].checked = actVar;
						}
					}
				}
			}           
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table3"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" align="center">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="660">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table1" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="text" width="16%">N�mero Carta</TD>
														<TD class="text" width="1%">:</TD>
														<TD class="text" colSpan="4"><asp:textbox id="txtNroDocumento" runat="server" CssClass="Text" EnableViewState="False" Width="200px"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" CssClass="Error" Display="Dynamic" ControlToValidate="txtNroDocumento"
																ErrorMessage="Ingrese Carta">*</asp:requiredfieldvalidator></TD>
													</TR>
													<TR>
														<TD class="text">Cliente</TD>
														<TD class="text">:</TD>
														<TD class="text" colSpan="4"><asp:dropdownlist id="cboDepositante" runat="server" CssClass="Text" Width="500px" AutoPostBack="True"></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="text">Financiador</TD>
														<TD class="text">:</TD>
														<TD class="text" colSpan="4"><asp:dropdownlist id="cboFinanciador" runat="server" CssClass="Text" Width="500px" AutoPostBack="True"></asp:dropdownlist><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" CssClass="Error" Display="Dynamic" ControlToValidate="cboFinanciador"
																ErrorMessage="Seleccione Financiador" InitialValue="0">*</asp:requiredfieldvalidator></TD>
													</TR>
													<TR>
														<TD class="text" vAlign="top">Destinatario Banco</TD>
														<TD class="text" vAlign="top">:</TD>
														<TD class="text" vAlign="top" colSpan="4"><asp:datagrid id="dgdDestinatario" runat="server" CssClass="gv" Width="400px" PageSize="5" AutoGenerateColumns="False"
																BorderWidth="1px" CellPadding="2" BorderColor="Gainsboro">
																<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
																<ItemStyle CssClass="gvRow"></ItemStyle>
																<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
																<Columns>
																	<asp:BoundColumn DataField="Nombres" HeaderText="Nombre Usuario">
																		<ItemStyle HorizontalAlign="Left"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="DIR_EMAI" HeaderText="Correo"></asp:BoundColumn>
																	<asp:TemplateColumn HeaderText="Marcar">
																		<HeaderStyle Width="30px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
																		<HeaderTemplate>
																			<asp:CheckBox id="chkAll" runat="server" CssClass="Text"></asp:CheckBox>
																		</HeaderTemplate>
																		<ItemTemplate>
																			<asp:CheckBox id="chkItem" runat="server" CssClass="Text"></asp:CheckBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD class="text">Almacen</TD>
														<TD class="text">:</TD>
														<TD class="text" colSpan="4"><asp:dropdownlist id="cboAlmacen" runat="server" CssClass="Text" Width="500px"></asp:dropdownlist><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" CssClass="Error" Display="Dynamic" ControlToValidate="cboAlmacen"
																ErrorMessage="Seleccione Almac�n" InitialValue="0">*</asp:requiredfieldvalidator></TD>
													</TR>
													<TR>
														<TD class="text">Moneda</TD>
														<TD class="text">:</TD>
														<TD class="text" width="35%"><asp:dropdownlist id="cboMoneda" runat="server" CssClass="Text"></asp:dropdownlist></TD>
														<TD class="text">Importe</TD>
														<TD class="text">:</TD>
														<TD class="text" width="55%"><INPUT id="txtValor" class="Text" onkeyup="Formato(this,event);" size="18" name="txtImporte"
																runat="server">
															<asp:requiredfieldvalidator id="RequiredFieldValidator5" runat="server" CssClass="Error" Display="Dynamic" ControlToValidate="txtValor"
																ErrorMessage="Seleccione Almac�n" InitialValue="0">*</asp:requiredfieldvalidator></TD>
													</TR>
													<TR>
														<TD class="text">Ubicar Archivo</TD>
														<TD class="text">:</TD>
														<TD class="text" colSpan="4"><INPUT style="WIDTH: 370px" id="FilePDF" class="Text" type="file" name="FilePDF" runat="server">
															<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" CssClass="Error" EnableViewState="False"
																Display="Dynamic" ControlToValidate="FilePDF" ErrorMessage="Solo archivos PDF" ForeColor=" " ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.pdf|.PDF)$">*</asp:regularexpressionvalidator><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" CssClass="Error" Display="Dynamic" ControlToValidate="FilePDF"
																ErrorMessage="Seleccione un archivo">*</asp:requiredfieldvalidator></TD>
													</TR>
												</TABLE>
												<asp:validationsummary id="ValidationSummary1" runat="server" CssClass="Error"></asp:validationsummary></TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
									<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
										<TR>
											<TD width="80"><asp:button id="btnNuevo" runat="server" CssClass="btn" Width="80px" CausesValidation="False"
													Text="Nuevo"></asp:button></TD>
											<TD width="80"><asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar"></asp:button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:literal id="ltrMensaje" runat="server"></asp:literal></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:Footer id="Footer2" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
