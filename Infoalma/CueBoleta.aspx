<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueBoleta.aspx.vb" Inherits="CueBoleta" %>

<%@ Register TagPrefix="uc1" TagName="HeaderLogin" Src="UserControls/HeaderLogin.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Facturas</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)

            if (diferencia > 1540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }

        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);
            if (fchIni == "" && fchFin == "") {
                alert("Debe Ingresar fecha Inicial y Final");
                return false;
            }

            if (fchIni > fchFin && fchIni != "" && fchFin != "") {
                alert("La fecha inicial no puede ser mayor a la fecha final");
                return false;
            }
            return true;
        }
    </script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <uc1:HeaderLogin ID="HeaderLogin1" runat="server"></uc1:HeaderLogin>
        <table id="Table2" border="0" cellspacing="0" cellpadding="0" width="100%" background="Images/Login.jpg">
            <tr>
                <td>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table4" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%" align="center">
                                            <p>
                                                <asp:Label ID="lblTitulo" runat="server" CssClass="Titulo">COMPROBANTE ELECTR�NICO<BR>SERVICIO DE PESADA</asp:Label></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="3"></td>
                                        <td style="height: 330px" valign="top" width="100%">
                                            <table style="width: 481px; height: 304px" id="Table6" border="0" cellspacing="0" cellpadding="0"
                                                width="481" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td align="center">
                                                        <table id="Table7" border="0" cellspacing="4" cellpadding="0">
                                                            <tr>
                                                                <td class="Text">Tipo Documento</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text">
                                                                    <asp:DropDownList ID="cboTipoDoc" runat="server" CssClass="Text" Width="220px" DataTextField="descripcion"
                                                                        DataValueField="codigo">
                                                                        <asp:ListItem Value="0">-- SELECCIONE --</asp:ListItem>
                                                                        <asp:ListItem Value="FAC">FACTURA ELECTRONICA</asp:ListItem>
                                                                        <asp:ListItem Value="BOL">BOLETA DE VENTA ELECTRONICA</asp:ListItem>
                                                                        <asp:ListItem Value="NCR">NOTA DE CREDITO ELECTRONICA</asp:ListItem>
                                                                        <asp:ListItem Value="NDE">NOTA DE DEBITO ELECTRONICA</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Serie</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text">
                                                                    <asp:TextBox ID="txtSerie" onkeyup="this.value = this.value.toUpperCase();" runat="server" CssClass="Text"
                                                                        Width="120px" MaxLength="4"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Correlativo</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text">
                                                                    <asp:TextBox ID="txtCorrelativo" onkeypress="javascript:return ValidNum(event);" runat="server"
                                                                        CssClass="Text" Width="120px" MaxLength="7"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Fecha Emisi�n
                                                                </td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text" width="128">
                                                                    <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="12" name="txtFechaInicial" runat="server"><input style="z-index: 0" id="btnFechaInicial" class="text" 
                                                                            value="..." type="button" name="btnFecha"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" style="height: 27px">Importe Total
                                                                </td>
                                                                <td class="Text" width="1%" style="height: 27px"></td>
                                                                <td class="Text" style="height: 26px">
                                                                    <asp:TextBox ID="txtMonto" onkeypress="javascript:return ValidNum(event);" runat="server" CssClass="Text"
                                                                        Width="120px" MaxLength="25"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">
                                                                    <table id="Table1" border="0" cellspacing="1" cellpadding="1">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Image ID="imgCaptcha" runat="server" ImageUrl="~/CreateCaptcha.aspx?New=1"></asp:Image></td>
                                                                            <td>
                                                                                <asp:ImageButton ID="imgrefresh" OnClick="imgrefresh_Click" runat="server" Width="16px"
                                                                                    ImageUrl="Images/Reload.png" ToolTip="Actualizar Imagen" CausesValidation="False" Height="16px"
                                                                                    ImageAlign="AbsBottom"></asp:ImageButton></td>
                                                                        </tr>
                                                                    </table>
                                                                    <td class="Text" width="1%"></td>
                                                                <td class="Text">Ingrese el texto que aparece en la imagen<br>
                                                                    <asp:TextBox ID="txtCaptcha" onkeyup="this.value = this.value.toUpperCase();" runat="server"
                                                                        CssClass="Text"></asp:TextBox></td>
                                                                <tr>
                                                                    <td class="Text" colspan="2"></td>
                                                                    <td align="center">
                                                                        <input style="width: 80px" id="btnVerDoc" class="btn" value="Ver PDF" type="button"
                                                                            name="btnVerDoc" runat="server"><input style="width: 80px" id="btnVerXML" class="btn" value="Ver XML"
                                                                                type="button" name="btnVerXML" runat="server"></td>
                                                                </tr>
                                                        </table>
                                                        <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
