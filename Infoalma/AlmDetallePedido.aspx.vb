Imports Depsa.LibCapaNegocio
'Imports Library.AccesoDB
Imports Library.AccesoDB
Imports System.Data.SqlClient
Imports System.Data

Public Class AlmDetallePedido
    Inherits System.Web.UI.Page
    Dim objWMS As clsCNWMS
    Private objTransaccion As SqlTransaction
    Private objConexion As SqlConnection
    Private objDocumento As Documento = New Documento
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents Table5 As System.Web.UI.WebControls.Table
    'Protected WithEvents PANEL1 As System.Web.UI.WebControls.Panel
    Private strPathDownload = ConfigurationManager.AppSettings.Item("RutaDownload")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents hiCO_UNID As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnEliminar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents lblNroPedido As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaEmision As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDestino As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRetirado As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrFirmar2 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar1 As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) Then
            Dim dtControles As New DataTable
            If Page.IsPostBack = False Then
                Me.lblOpcion.Text = "Detalle de Pedido"
                Me.lblRetirado.Text = Session("Retirado")
                Me.lblFechaEmision.Text = Session("FchEmision")
                Me.lblDestino.Text = Session("Destino")
                Me.lblNroPedido.Text = Session("NroPedido")
                Bindatagrid()
            End If
            dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroPedido"),
                                                            Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), 2)

            Dim strfirma As String
            For j As Integer = 0 To dtControles.Rows.Count - 1
                Select Case dtControles.Rows(j)("COD_CTRL")
                    Case "01"
                        If Session.Item("strcontrasenia") <> Nothing Then
                            If DateDiff(DateInterval.Minute, Session.Item("strfecha"), Now) > 5 Then
                                strfirma = "X"
                            Else
                                strfirma = "F"
                                Me.txtContrasena.Value = Session.Item("strContrasenia")
                            End If
                        Else
                            strfirma = "X"
                        End If
                        If Request.QueryString("var") = 1 Then
                            If Session.Item("CertDigital") = True Then
                                Me.lblMensaje.Text = "No tiene certificado digital"
                            Else
                                If strfirma = "X" Then
                                    Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: blue' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                Else
                                    Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: blue' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                End If
                            End If
                        End If
                    Case "04"
                        Me.ltrFirmar2.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(3)' type='button' value='Rechazar' name = 'cmdFirmar'>"
                    Case "00"
                        Me.lblMensaje.Text = "Otro usuario esta intentando firmar el documento, intente en otro momento"
                End Select
            Next
        Else
            Response.Redirect("../Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            objWMS = New clsCNWMS
            Dim dtWMS = objWMS.gCNGetListarDetallePedido(Session.Item("CoEmpresa"), Session.Item("CodCliente"), Session.Item("CodAlmacen"), Me.lblNroPedido.Text)
            Me.dgResultado.DataSource = dtWMS
            Me.dgResultado.DataBind()
            Session.Add("dtDetallePedidoWMS", dtWMS)
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'If e.Item.DataItem("DSC_RETE").ToString.Trim <> "" Then
            '    e.Item.BackColor = System.Drawing.Color.Thistle
            'End If
            Select Case e.Item.DataItem("ESTATUS")
                Case "PENDIENTE"
                    e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                Case "COMPLETADO"
                    e.Item.BackColor = System.Drawing.Color.FromArgb(173, 213, 230)
                Case "EN PROCESO"
                    e.Item.BackColor = System.Drawing.Color.FromArgb(50, 205, 50)
            End Select
        End If
    End Sub

    'Private Sub btnRegistrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    IngresarRetiroWMS_2(lblNroPedido.Text)
    'End Sub

    'Private Sub IngresarRetiroWMS_2(ByVal NroPedido As String)
    '    Dim dtMail As New DataTable
    '    Dim strNombreCliente As String
    '    Dim strEmails As String
    '    Dim strNroRetiro As String
    '    Dim strNombreReporte As String
    '    Dim strMensaje As String
    '    Dim dttxt As New DataTable

    '    objWMS = New clsCNWMS
    '    objConexion = New SqlConnection(strCadenaConexion)
    '    objConexion.Open()
    '    objTransaccion = objConexion.BeginTransaction
    '    Try
    '        Dim a As String
    '        a = Session("IdTipoUsuario")
    '        Dim nsecu As Integer = 0
    '        If Session("IdTipoUsuario") = "03" Then 'SI ES APROBADOR
    '            'Response.Redirect("DocumentoFirmaWMS.aspx?var=1", False)
    '            'SI FIRMA 
    '            nsecu = 2
    '            'SI NO FIRMA PERO DESEA GUARDAR (PENDIENTE y EMITIDOS)
    '            nsecu = 1
    '        Else 'SI NO ES APROBADOR
    '            'PREGUNTAR si desea guardar de todos modos: SI
    '            nsecu = 1
    '        End If

    '        If nsecu <> 0 Then
    '            If nsecu = 1 Then 'Si firma
    '                'Actualizar NRORETIRO con su NuSecu
    '                strMensaje = objWMS.gCNACTNumRetiro(Session.Item("IdUsuario"), nsecu, NroPedido, objTransaccion)
    '                If strMensaje = "-1" Then
    '                    Me.lblMensaje.Text = "No se pudo actualizar el Nro. Documento"
    '                    objTransaccion.Rollback()
    '                    objTransaccion.Dispose()
    '                    Exit Sub
    '                End If
    '                'strMensaje = objRetiro.gCNGENE_SALI_INFO_WMS(nsecu, strNroRetiro, objTransaccion)
    '                dttxt = objWMS.gCNGENE_SALI_INFO_WMS(strNroRetiro, objTransaccion)
    '                If dttxt Is Nothing Then
    '                    Me.lblMensaje.Text = "No se gener� el DataSet"
    '                    objTransaccion.Rollback()
    '                    objTransaccion.Dispose()
    '                    Exit Sub
    '                End If
    '                'CREAR ARCHIVO
    '                Dim bln As Boolean = CreateTextDelimiterFile(strPathDownload & "O" & strNroRetiro & ".txt", dttxt, "*", False, False, strNroRetiro)
    '                'Dim bln As Boolean = CreateTextDelimiterFile("\\161.132.96.91\soporte\SQLRocks1.txt", dttxt, "*", False, False)
    '                If bln Then
    '                    Me.lblMensaje.Text = "Se envio correctamente su solicitud"
    '                Else
    '                    Me.lblMensaje.Text = "No se pudo generar archivo TXT"
    '                    objTransaccion.Rollback()
    '                    objTransaccion.Dispose()
    '                    Exit Sub
    '                End If
    '                ''FIN
    '            End If
    '        Else
    '            'HA CANCELADO LA OPERACION 
    '            Me.lblMensaje.Text = "Ud. Cancelo la operaci�n"
    '            Exit Sub
    '        End If

    '        objTransaccion.Commit()
    '        objTransaccion.Dispose()
    '        'btnEnviar.Enabled = False
    '        'Bindatagrid()
    '        'pr_IMPR_MENS("Se envio correctamente su solicitud")
    '        Response.Redirect("AlmPedidoNuevo.aspx", False)

    '    Catch ex As Exception
    '        objTransaccion.Rollback()
    '        objTransaccion.Dispose()
    '        Me.lblMensaje.Text &= " " & ex.Message
    '    Finally
    '        objConexion.Close()
    '        objConexion.Dispose()
    '        objConexion = Nothing
    '    End Try
    'End Sub

    'Private Function CreateTextDelimiterFile(ByVal fileName As String, _
    '                                    ByVal dt As DataTable, _
    '                                    ByVal separatorChar As Char, _
    '                                    ByVal hdr As Boolean, _
    '                                    ByVal textDelimiter As Boolean, _
    '                                    ByVal strNroRetiro As String) As Boolean

    '    If (fileName = String.Empty) OrElse _
    '       (dt Is Nothing) Then Throw New System.ArgumentException("Argumentos no v�lidos.")

    '    If (IO.File.Exists(fileName)) Then
    '        'If (MessageBox.Show("Ya existe un archivo de texto con el mismo nombre." & Environment.NewLine & _
    '        '                   "�Desea sobrescribirlo?", _
    '        '                   "Crear archivo de texto delimitado", _
    '        '                   MessageBoxButtons.YesNo, _
    '        '                   MessageBoxIcon.Information) = DialogResult.No) Then 
    '        'Return False
    '    End If

    '    Dim sw As System.IO.StreamWriter

    '    Try
    '        Dim col As Integer = 0
    '        Dim value As String = String.Empty

    '        ' Creamos el archivo de texto con la codificaci�n por defecto.
    '        sw = New IO.StreamWriter(fileName, False, System.Text.Encoding.Default)

    '        If (hdr) Then
    '            ' La primera l�nea del archivo de texto contiene
    '            ' el nombre de los campos.
    '            For Each dc As DataColumn In dt.Columns

    '                If (textDelimiter) Then
    '                    ' Incluimos el nombre del campo entre el caracter
    '                    ' delimitador de texto especificado.
    '                    value &= """" & dc.ColumnName & """" & separatorChar
    '                Else
    '                    ' No se incluye caracter delimitador de texto alguno.
    '                    value &= dc.ColumnName & separatorChar
    '                End If
    '            Next
    '            sw.WriteLine(value.Remove(value.Length - 1, 1))
    '            value = String.Empty

    '        End If

    '        ' Recorremos todas las filas del objeto DataTable
    '        ' incluido en el conjunto de datos.
    '        For Each dr As DataRow In dt.Rows
    '            For Each dc As DataColumn In dt.Columns
    '                If ((dc.DataType Is System.Type.GetType("System.String")) And _
    '                   (textDelimiter = True)) Then
    '                    ' Incluimos el dato alfanum�rico entre el caracter
    '                    ' delimitador de texto especificado.
    '                    value &= """" & dr.Item(col).ToString & """" & separatorChar
    '                Else
    '                    ' No se incluye caracter delimitador de texto alguno
    '                    value &= dr.Item(col).ToString & separatorChar
    '                End If
    '                ' Siguiente columna
    '                col += 1
    '            Next

    '            ' Al escribir los datos en el archivo, elimino el
    '            ' �ltimo car�cter delimitador de la fila.
    '            sw.WriteLine(value.Remove(value.Length - 1, 1))
    '            value = String.Empty
    '            col = 0

    '        Next ' Siguiente fila

    '        sw.Close()

    '        ' Se ha creado con �xito el archivo de texto.
    '        Return True

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        Return False
    '    Finally
    '        sw = Nothing
    '    End Try
    'End Function

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim dt1 As DataTable
        dt1 = Session.Item("dtDetallePedidoWMS")
        Dim table As Table = ConvertDatatabletoTable(dt1)
        Dim sw As System.IO.StringWriter = New System.IO.StringWriter
        Dim htw As New HtmlTextWriter(sw)
        table.RenderControl(htw)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", "PEDIDO.xls"))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        HttpContext.Current.Response.Write(sw.ToString())
        HttpContext.Current.Response.End()
    End Sub

    Public Function ConvertDatatabletoTable(ByVal dtSource As DataTable) As Table
        Dim tbl As New Table
        tbl = Me.Table5
        tbl.CellPadding = 0
        tbl.CellSpacing = 0
        Dim AddedColumnName As Boolean = False

        '============== Insertando La cabecera =======================
        For Each row As TableRow In Table5.Rows
            For Each cell As TableCell In row.Cells
                If cell.ID = "R1C4" Then 'Nro pedido
                    cell.Text = Me.lblNroPedido.Text
                End If
                If cell.ID = "R2C2" Then 'Cliente
                    cell.Text = dtSource.Rows(0)("CLIENTE")
                End If
                If cell.ID = "R2C6" Then 'Enviado por
                    cell.Text = dtSource.Rows(0)("NO_USUA_ENVI")
                End If
                If cell.ID = "R3C2" Then 'destino"
                    cell.Text = Me.lblDestino.Text
                End If
                If cell.ID = "R3C6" Then 'transportista
                    cell.Text = Me.lblRetirado.Text
                End If
            Next
        Next

        '================= Insertando La Detalle =================
        For j As Integer = 0 To dtSource.Rows.Count - 1
            Dim row As New TableRow
            Dim cellb1 As New TableCell
            Dim cellb2 As New TableCell
            Dim cell2 As New TableCell
            Dim cell3 As New TableCell
            Dim cell4 As New TableCell
            Dim cell5 As New TableCell
            Dim cell6 As New TableCell
            Dim cell7 As New TableCell
            Dim cell8 As New TableCell
            Dim cell9 As New TableCell
            cellb1.Text = ""
            cell2.Text = dtSource.Rows(j)("COD_PROD")
            cell3.Text = dtSource.Rows(j)("DSC_MERC")
            cell4.Text = dtSource.Rows(j)("LOTE")
            cell5.Text = dtSource.Rows(j)("CNT_SOLI")
            cell6.Text = dtSource.Rows(j)("UNI_MEDI")
            cell7.Text = dtSource.Rows(j)("CNT_PREP")
            cell8.Text = dtSource.Rows(j)("ESTATUS")
            cell9.Text = dtSource.Rows(j)("DSC_RETE")
            cellb2.Text = ""
            cell2.HorizontalAlign = HorizontalAlign.Left
            cell3.HorizontalAlign = HorizontalAlign.Left
            cell4.HorizontalAlign = HorizontalAlign.Left
            cell5.HorizontalAlign = HorizontalAlign.Left
            cell6.HorizontalAlign = HorizontalAlign.Left
            cell7.HorizontalAlign = HorizontalAlign.Left
            cell8.HorizontalAlign = HorizontalAlign.Left
            cell9.HorizontalAlign = HorizontalAlign.Left
            row.Cells.Add(cellb1)
            row.Cells.Add(cell2)
            row.Cells.Add(cell3)
            row.Cells.Add(cell4)
            row.Cells.Add(cell5)
            row.Cells.Add(cell6)
            row.Cells.Add(cell7)
            row.Cells.Add(cell8)
            row.Cells.Add(cell9)
            row.Cells.Add(cellb2)

            tbl.Rows.Add(row)
        Next
        Return tbl
    End Function
End Class
