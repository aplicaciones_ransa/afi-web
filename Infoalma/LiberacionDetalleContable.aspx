<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LiberacionDetalleContable.aspx.vb" Inherits="LiberacionDetalleContable" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Detalle Liberación</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script>
		function Formato(Campo, teclapres){
			var tecla = teclapres.keyCode;
			var vr = new String(Campo.value);  
			vr = vr.replace(".", ""); 
			vr = vr.replace(".", ""); 
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace(",", "");
			vr = vr.replace("/", "");
			vr = vr.replace("-", "");
			vr = vr.replace(" ", "");
			var tam = 0;
			tam = vr.length; 
				if (tam > 4 && tam < 8){
					Campo.value = vr.substr(0,tam-4) + '.' + vr.substr(tam-4,4);
					}
				if (tam >= 8 && tam < 11){
					Campo.value = vr.substr(0,tam-7) + ',' + vr.substr(tam-7,3) + '.' + vr.substr(tam-4,4);
					}
				if (tam >=11 && tam < 14){
					Campo.value = vr.substr(0,tam-10) + ',' + vr.substr(tam-10,3) + ',' + vr.substr(tam-7,3) + '.' + vr.substr(tam-4,4);
					}
				if (tam >= 14 && tam < 17){
					Campo.value = vr.substr(0,tam-13) + ',' + vr.substr(tam-13,3) + ',' + vr.substr(tam-10,3) + ',' + vr.substr(tam-7,3)+ '.' + vr.substr(tam-4,4); 
					}
				if (tam >= 17 && tam < 20){
					Campo.value = vr.substr(0,tam-16) + ',' + vr.substr(tam-16,3) + ',' + vr.substr(tam-13,3) + ',' + vr.substr(tam-10,3) + ',' + vr.substr(tam-7,3)+ '.' + vr.substr(tam-4,4);
					}
				if (tam >= 20 && tam < 23){
					Campo.value = vr.substr(0,tam-19) + ',' + vr.substr(tam-19,3) + ',' + vr.substr(tam-16,3) + ',' + vr.substr(tam-13,3) + ',' + vr.substr(tam-10,3) + ',' + vr.substr(tam-7,3)+ '.' + vr.substr(tam-4,4);
					}
					if (tam >= 23){
					Campo.value = vr.substr(0,tam-22) + ',' + vr.substr(tam-22,3) + ',' + vr.substr(tam-19,3) + ',' + vr.substr(tam-16,3) + ',' + vr.substr(tam-13,3) + ',' + vr.substr(tam-10,3)+ ',' + vr.substr(tam-7,3)+ '.' + vr.substr(tam-4,4);
					}					 
			}
			function fn_CALCULA()
		   {
				document.getElementById('hiOperacion').value='Calcular';
				document.Form1.submit();
		   }
		</script>
	    <style type="text/css">
            .auto-style2 {
                height: 29px;
            }
        </style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colSpan="2"><uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table9"
							border="0" cellSpacing="6" cellPadding="0" width="100%">
							<TR>
								<TD></TD>
								<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="9"><uc1:menuinfo id="Menuinfo2" runat="server"></uc1:menuinfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="630" align="center">
										<TR>
											<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r1_c2.gif"></TD>
											<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD background="Images/table_r2_c1.gif" width="6"></TD>
											<TD>
												<TABLE id="Table4" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="Text">Modalidad:</TD>
														<TD><asp:dropdownlist id="cboModalidad" runat="server" CssClass="Text" AutoPostBack="True" Width="150px"
																DataTextField="descripcion" DataValueField="codigo"></asp:dropdownlist></TD>
														<TD class="Text">Nro. Ord. Retiro:</TD>
														<TD><asp:dropdownlist id="cboTipoRetiro" runat="server" CssClass="Text" AutoPostBack="True" Width="60px"
																DataTextField="descripcion" DataValueField="codigo"></asp:dropdownlist><asp:textbox id="txtNroRetiro" runat="server" CssClass="inactivo" Width="150px" ReadOnly="True"
																MaxLength="8"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text" width="16%">Moneda:</TD>
														<TD width="29%"><asp:dropdownlist id="cboMoneda" runat="server" CssClass="Text" Width="150px" DataTextField="descripcion"
																DataValueField="codigo"></asp:dropdownlist></TD>
														<TD class="Text" width="15%">Nro. Warrant:</TD>
														<TD width="40%"><asp:dropdownlist id="cboTipoTitulo" runat="server" CssClass="Text" Width="60px" DataTextField="descripcion"
																DataValueField="codigo" DESIGNTIMEDRAGDROP="149"></asp:dropdownlist><asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="152px" MaxLength="20"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text">Almacen:</TD>
														<TD colSpan="3"><asp:textbox id="txtAlmacen" runat="server" CssClass="Text" Width="480px" MaxLength="20"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text">Fecha Documento:</TD>
														<TD><asp:textbox id="txtFecha" runat="server" CssClass="Text" Width="150px"></asp:textbox></TD>
														<TD class="Text">Endosatario:</TD>
														<TD><asp:textbox id="txtEndosatario" runat="server" CssClass="Text" Width="200px" EnableViewState="False"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text"></TD>
														<TD><asp:textbox id="txtDeudaSoles" runat="server" CssClass="inactivo" Width="120px" ReadOnly="True"
																Visible="False"></asp:textbox></TD>
														<TD class="Text"></TD>
														<TD><asp:textbox id="txtDeudaDolares" runat="server" CssClass="inactivo" Width="118px" ReadOnly="True"
																Visible="False"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="Text"></TD>
														<TD><asp:dropdownlist id="cboUnidad" runat="server" CssClass="Text" Width="150px" DataTextField="descripcion"
																DataValueField="codigo" Visible="False"></asp:dropdownlist></TD>
														<TD class="Text"></TD>
														<TD><asp:dropdownlist id="cboTipoComprobante" runat="server" CssClass="Text" Width="60px" DataTextField="descripcion"
																DataValueField="codigo" Visible="False"></asp:dropdownlist><asp:textbox id="txtNroComprobante" runat="server" CssClass="Text" Width="150px" MaxLength="20"
																Visible="False"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD background="Images/table_r2_c3.gif" width="6"></TD>
										</TR>
										<TR>
											<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
											<TD height="6" background="Images/table_r3_c2.gif"></TD>
											<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table3" border="0" cellSpacing="4" cellPadding="0" width="630">
										<TR>
											<TD class="Text" width="16%">Observación:</TD>
											<TD width="84%"><asp:textbox id="txtObservacion" runat="server" CssClass="Text" Width="500px" EnableViewState="False"></asp:textbox></TD>
										</TR>
									</TABLE>
									<TABLE id="Table5" border="0" cellSpacing="4" cellPadding="0" width="630">
										<TR>
											<TD class="Text" width="16%">Embarque:</TD>
											<TD width="30%"><asp:checkbox id="chkEmbarque" runat="server" CssClass="Text" Enabled="False"></asp:checkbox></TD>
											<TD class="Text" width="20%">Con traslado:</TD>
											<TD width="34%" align="left"><asp:checkbox id="chkTraslado" runat="server" CssClass="Text" AutoPostBack="True" Enabled="False"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="Text" width="16%" colspan="4"><asp:panel style="Z-INDEX: 0" id="pnlDestino" runat="server" Visible="False">
													<TABLE id="Table33" border="0" cellSpacing="4" cellPadding="0" width="630">
														<TR>
															<TD style="Z-INDEX: 0" class="Text" width="16%">Almacén Destino:</TD>
															<TD colSpan="3">
																<asp:DropDownList id="cboDestino" runat="server" CssClass="Text"></asp:DropDownList></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center"><asp:label id="lbMS_REGI" runat="server" CssClass="Texto9pt" ForeColor="Red"></asp:label></TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top">Lista de Item con saldo disponible:</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:datagrid id="dgTCALMA_MERC" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										columnsFrezze="10" bodyWidth="980" bodyHeight="140" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Sel">
												<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id="chST_SELE" runat="server" CssClass="Text"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="CO_ALMA" SortExpression="CO_ALMA" HeaderText="Almacen">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NUMERO_RECEPCION" SortExpression="NUMERO_RECEPCION" HeaderText="DCR">
												<HeaderStyle Width="90px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="90px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NU_SECU" SortExpression="NU_SECU" HeaderText="Item">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="30px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_MERC" SortExpression="DE_MERC" HeaderText="Mercader&#237;a">
												<HeaderStyle Width="100px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="250px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CO_TIPO_BULT" SortExpression="CO_TIPO_BULT" HeaderText="Unidad">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CANTIDAD_UNIDAD" SortExpression="CANTIDAD_UNIDAD" HeaderText="Cant.Disponible"
												DataFormatString="{0:N6}">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UNME_PESO" SortExpression="CO_UNME_PESO" HeaderText="Bulto">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="30px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CANTIDAD_BULTO" SortExpression="CANTIDAD_BULTO" HeaderText="Cant.Bulto"
												DataFormatString="{0:N6}">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="80px"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Cant a Liberar">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="80px"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id="tbNU_UNID_RETI" runat="server" Width="75" CssClass="Text" ForeColor="blue" Font-Bold="True"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Bulto">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id=tbNU_PESO_RETI runat="server" Width="75px" CssClass="Text" ForeColor="blue" Font-Bold="True" Text='<%# String.Format("{0:N6}",DataBinder.Eval(Container, "DataItem.CANTIDAD_BULTO")) %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="CO_PROD_CLIE" SortExpression="CO_PROD_CLIE" HeaderText="Cod.Producto">
												<HeaderStyle Width="120px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="120px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="REFERENCIA" SortExpression="REFERENCIA" HeaderText="Refer/Lote">
												<HeaderStyle Width="100px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="100px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FECHA_FACTURACION" SortExpression="FECHA_FACTURACION"
												HeaderText="Fec. Fact." DataFormatString="{0:d}">
												<HeaderStyle Width="70px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="70px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FECHA_INGRESO" SortExpression="FECHA_INGRESO" HeaderText="Fec.Ingreso"
												DataFormatString="{0:d}">
												<HeaderStyle Width="70px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="70px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="FECHA_VCTO_MERCADERIA" SortExpression="FECHA_VCTO_MERCADERIA"
												HeaderText="Vcto. Producto" DataFormatString="{0:d}">
												<HeaderStyle Width="70px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="70px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_BODE_ACTU" SortExpression="CO_BODE_ACTU" HeaderText="Bodega">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="60px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UBIC_ACTU" SortExpression="CO_UBIC_ACTU" HeaderText="Ubicacion">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="60px"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_PESO_UNID"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="TI_UNID_PESO"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="IM_UNIT" HeaderText="Precio.Unit"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UNME_AREA"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_AREA_USAD"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_UNME_VOLU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_VOLU_USAD"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_TIPO_MERC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="DE_TIPO_MERC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_STIP_MERC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="DE_STIP_MERC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NU_SECU_UBIC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="CO_MONE"></asp:BoundColumn>
											<asp:BoundColumn DataField="IM_TOTA" HeaderText="Importe" DataFormatString="{0:N6}">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right" Width="60px"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Importe">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left" Width="80px"></ItemStyle>
												<ItemTemplate>
													<asp:TextBox id="txtImporte" runat="server" Width="75" CssClass="Text" ForeColor="blue" Font-Bold="True"></asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Visible="False" CssClass="gvPager"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top">
									<TABLE id="Table2" border="0" cellSpacing="4" cellPadding="0" width="630" align="center">
										<TR>
											<TD class="auto-style2"><cc1:botonenviar id="btnSolicitar" runat="server" CssClass="btn" Width="120px" ForeColor="White" TextoEnviando="Emitiendo..."
													Text="Solicitar" BackColor="Red" Font-Bold="True"></cc1:botonenviar></TD>
											<TD class="auto-style2"><asp:button id="btnRegresar" runat="server" CssClass="btn" Width="100px" Text="Regresar" CausesValidation="False"></asp:button></TD>
											<TD class="auto-style2"><!--<INPUT id="IB_Calcula2" style="WIDTH: 80px" onclick="javascript:fn_CALCULA()" type="button"
																size="20" value="Calcular" name="IB_Calcula2" runat="server">--></TD>
											<TD class="auto-style2"></TD>
											<TD width="500" class="auto-style2"></TD>
										</TR>
									</TABLE>
									<INPUT id="hiOperacion" type="hidden" name="hiOperacion" runat="server"><INPUT id="hiNU_CANT_LINE" type="hidden" name="hiNU_CANT_LINE" runat="server"><INPUT id="hiST_ALMA_SUBI" type="hidden" name="hiST_ALMA_SUBI" runat="server">
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"><asp:label id="lblMensaje" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<P><INPUT id="txtMercaderia" type="hidden" name="txtMercaderia" runat="server"><INPUT id="hiCO_ALMA" type="hidden" name="hiCO_ALMA" runat="server">
										<asp:label id="lbNU_DOCU_GRAB" runat="server" CssClass="Text" Visible="False"></asp:label></P>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
