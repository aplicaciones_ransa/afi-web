<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoReporte.aspx.vb" Inherits="DocumentoReporte" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Formato Generado</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body topmargin="0" bottomMargin="0" leftMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<TABLE id="Table3" cellSpacing="4" cellPadding="0" width="100%">
							<TR>
								<TD align="right">
									<asp:Button id="btnCerrar" runat="server" CssClass="btn" Text="Cerrar" Width="80px"></asp:Button></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:label id="lblError" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:literal id="ltrReporte" runat="server"></asp:literal></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
