<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoPagDos_old.aspx.vb" Inherits="DocumentoPagDos_old" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Endoso</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="francisco_uni@hotmail.com" name="author">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script language="javascript" src="JScripts/Funcionalidad.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">		

        $(function () {
            $("#txtVencimiento").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button1").click(function () {
                $("#txtVencimiento").datepicker('show');
            });

   
        });

        function Validar() {
            var FechVenBanco;
            var FechEmision;
            var FechVenSICO;
            FechVenBanco = document.all["txtVencimiento"].value;
            FechEmision = document.all["txtFechaEmision"].value;
            FechVenSICO = document.all["txtFechaVenSICO"].value;

            if (FechEmision.length < 10 || FechEmision.length > 10) {
                alert("La Fecha de Emisi�n no esta bien, comuniquese con el administrador");
                return false;
            }
            if (FechVenSICO.length < 10 || FechVenSICO.length > 10) {
                alert("La Fecha del 1 vencimiento no esta bien, comuniquese con el administrador");
                return false;
            }

            FechVenBanco = FechVenBanco.substr(6, 4) + FechVenBanco.substr(3, 2) + FechVenBanco.substr(0, 2);
            FechEmision = FechEmision.substr(6, 4) + FechEmision.substr(3, 2) + FechEmision.substr(0, 2);
            FechVenSICO = FechVenSICO.substr(6, 4) + FechVenSICO.substr(3, 2) + FechVenSICO.substr(0, 2);
            if (FechVenBanco < FechEmision) {
                alert("La fecha de vencimiento debe ser mayor a la fecha de Emisi�n");
                return false;
            }
            if (FechVenBanco > FechVenSICO) {
                alert("La fecha de vencimiento debe ser menor a la fecha del 1 vencimiento");
                return false;
            }

            var oficina;
            var nrocuenta;
            var tasa;
            var monto;
            var operacion;

            oficina = document.Form1["txtOficina"].value;
            nrocuenta = document.Form1["txtNroCuenta"].value;
            tasa = document.Form1["txtTasa"].value;
            monto = document.Form1["txtValor"].value;
            operacion = document.Form1["txtOperacion"].value;

            if (operacion == '') {
                if (confirm('El campo Para garantizar el cumplimiento... esta vacio desea continuar?') == false) {
                    document.all["txtOperacion"].focus();
                    return false;
                }
            }
            if (monto == '') {
                if (confirm('El campo monto esta vacio desea continuar?') == false) {
                    document.all["txtValor"].focus();
                    return false;
                }
            }
            if (tasa == '') {
                if (confirm('El campo tasa esta vacio desea continuar?') == false) {
                    document.all["txtTasa"].focus();
                    return false;
                }
            }
            if (oficina == '') {
                if (confirm('El campo Oficina esta vacio desea continuar?') == false) {
                    document.all["txtOficina"].focus();
                    return false;
                }
            }
            if (nrocuenta == '') {
                if (confirm('El campo n�mero de cuenta esta vacio desea continuar?') == false) {
                    document.all["txtNroCuenta"].focus();
                    return false;
                }
            }
        }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
    </script>
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="Table2" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        cellspacing="6" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="3">
                                <uc1:MenuInfo ID="Menuinfo2" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%">
                                <table id="Table20" cellspacing="0" cellpadding="0" width="630" align="center" border="0">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table3" cellspacing="4" cellpadding="0" width="100%" border="0" designtimedragdrop="29">
                                                <tr>
                                                    <td class="Text">Nro Warant:</td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblNroWarrant" runat="server" CssClass="Text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Nom. Depositante:</td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblCliente" runat="server" CssClass="Text"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Endosatario:</td>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblEntidadFinanciera" runat="server" CssClass="Text"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Para garantizar el cumplimiento de obligacion(es) derivada(s) de:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtOperacion" runat="server" CssClass="Text" BackColor="#E0E0E0" Width="480px"
                                                            Enabled="False" TextMode="MultiLine" Height="70px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Monto:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboMoneda" runat="server" CssClass="Text" Width="50px" Enabled="False"></asp:DropDownList>
                                                        <input class="Text" id="txtValor" onkeyup="Formato(this,event);" style="background-color: #e0e0e0"
                                                            disabled size="18" name="txtValor" runat="server">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtValor" ForeColor=" "></asp:RequiredFieldValidator></td>
                                                    <td class="Text">Warrant&nbsp;Sin Protesto:</td>
                                                    <td>
                                                        <asp:CheckBox ID="chkProtesto" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Tasa:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtTasa" runat="server" CssClass="Text" BackColor="#E0E0E0" Width="480px" Enabled="False"
                                                            TextMode="MultiLine" Height="50px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Fecha Vcto Banco:</td>
                                                    <td>
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtVencimiento" onkeyup="this.value=formateafecha(this.value);"
                                                            disabled maxlength="10" size="15" name="txtVencimiento" runat="server"><input class="Text" id="Button1" type="button" value="..."
                                                                name="btnFecha">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtVencimiento" ForeColor=" "></asp:RequiredFieldValidator></td>
                                                    <td class="Text">Endoso con Embarque:</td>
                                                    <td>
                                                        <asp:CheckBox ID="chkEmbarque" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" width="18%">Fecha Emisi�n:</td>
                                                    <td width="35%">
                                                        <input class="Text" id="txtFechaEmision" disabled name="txtFechaEmision" runat="server"></td>
                                                    <td class="Text" width="20%">Fecha 1 Vcto:</td>
                                                    <td width="27%">
                                                        <input class="Text" id="txtFechaVenSICO" disabled name="txtFechaVenSICO" runat="server"></td>
                                                </tr>
                                                <tr>
                                                    <td class="etiqueta" colspan="4" height="20">Lugar de pago de Cr�dito</td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Banco:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtBanco" runat="server" CssClass="Text" Width="480px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Oficina:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtOficina" runat="server" CssClass="Text" BackColor="#E0E0E0" Width="480px"
                                                            Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">N�mero de Cuenta:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="Text" BackColor="#E0E0E0" Width="180px"
                                                            Enabled="False"></asp:TextBox></td>
                                                    <td class="Text">DC:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDC" runat="server" CssClass="Text" Width="120px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Nro. de Garant�a:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtNroGarantia" Style="text-transform: uppercase" runat="server" CssClass="Text"
                                                            Width="180px" MaxLength="20"></asp:TextBox></td>
                                                    <td class="Text"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text">Lugar y Fecha:</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtLugar" runat="server" CssClass="Text" Width="450px" Enabled="False"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" CssClass="error" ErrorMessage="Ingrese"
                                                            Display="Dynamic" ControlToValidate="txtLugar" ForeColor=" "></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="Text" colspan="4" height="20"></td>
                                                </tr>
                                                <tr>
                                                    <td class="Etiqueta" colspan="4">
                                                        <table id="Table5" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="auto-style1">
                                                                    </td>
                                                                <td class="auto-style1">
                                                                    <asp:Button ID="btnGrabar" runat="server" CssClass="btn" BackColor="Red" Width="80px" ForeColor="White"
                                                                        Text="Grabar" Visible="False" Font-Bold="True"></asp:Button></td>
                                                                <td class="auto-style1"><asp:button id="btnRechazar" runat="server" CssClass="btn" Width="80px" Visible="False" Text="Rechazar"
																			CausesValidation="False" BackColor="Red" ForeColor="White"></asp:button></td>
                                                                <td class="auto-style1"><cc1:botonenviar id="btnRegistrar" runat="server" CssClass="btn" Width="80px" Visible="False" Text="Registrar"
																			TextoEnviando="Registrando..."></cc1:botonenviar></td>
                                                                <td class="auto-style1">
																		<asp:Button style="Z-INDEX: 0" id="btnAnular" runat="server" CssClass="Text" Width="80px" Text="Anular"
																			Visible="False"></asp:Button></td>
                                                                <td class="auto-style1"><asp:button id="btnRegresar" runat="server" CssClass="btn" Width="80px" Text="Regresar" CausesValidation="False"
																			style="Z-INDEX: 0"></asp:button></td>
                                                                <td width="500" class="auto-style1"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <input id="txtNroPagina" type="hidden" name="txtNroPagina" runat="server"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
