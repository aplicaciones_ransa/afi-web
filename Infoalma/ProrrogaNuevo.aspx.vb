Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.Data
Imports Infodepsa

Public Class ProrrogaNuevo
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    'Protected WithEvents lblEntidad As System.Web.UI.WebControls.Label
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEndosatario As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "16") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "16"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Nueva Prorroga"
                If Session.Item("IdTipoEntidad") = "03" Then
                    Me.lblEntidad.Text = "Endosatario:"
                End If
                If Session.Item("IdTipoEntidad") = "02" Then
                    Me.lblEntidad.Text = "Depositante:"
                End If
                loadEntidad()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub BindDatagrid()
        dt = New DataTable
        Try
            dt = objDocumento.gGetDocumentosProrroga(Me.txtNroWarrant.Text.Replace("'", ""), Me.cboEndosatario.SelectedValue, Session.Item("IdSico"), Session.Item("IdTipoEntidad"))
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            pr_IMPR_MENS("Error al llenar grilla: " & ex.Message)
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub btnProrroga_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "16", "SOLICITAR PRORROGA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim imgEditar As Button = CType(sender, Button)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            dtMail = objDocumento.gInsProrroga(dgi.Cells(0).Text(), dgi.Cells(1).Text(),
            dgi.Cells(10).Text(), dgi.Cells(11).Text(), dgi.Cells(5).Text(), dgi.Cells(4).Text(),
            dgi.Cells(6).Text(), dgi.Cells(2).Text(), dgi.Cells(8).Text(), dgi.Cells(12).Text(), dgi.Cells(13).Text(), dgi.Cells(7).Text(), Session.Item("IdUsuario"))

            If dtMail.Rows.Count > 0 Then
                If dtMail.Rows(0)("nro") = 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                    objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmails.Remove(strEmails.Length - 1, 1),
                    "Ingresar informaci�n de prorroga - " & dgi.Cells(14).Text(),
                    "Estimados Se�ores:<br><br>" &
                    "La entidad financiera notificada deber� ingresar la informaci�n necesaria para poder emitir la prorroga:" &
                    "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & dgi.Cells(0).Text() & "</FONT></STRONG><br>" &
                    "<br>Secuencial de prorroga N�: <STRONG><FONT color='#330099'>" & dtMail.Rows(0)("SECUENCIA") & "</FONT></STRONG><br>" &
                    "Depositante: <STRONG><FONT color='#330099'>" & dgi.Cells(14).Text() & "</FONT></STRONG>" &
                    "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
                Else
                    pr_IMPR_MENS(dtMail.Rows(0)("EMAI"))
                    Exit Sub
                End If
            End If
            pr_IMPR_MENS("Se gener� correctamente ")
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        Dim btnProrroga As New Button
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Cells(8).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("Saldo"))
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
            btnProrroga = CType(e.Item.FindControl("btnProrroga"), Button)
            btnProrroga.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de generar prorroga al warrant " & e.Item.DataItem("NUMERO_TITULO") & "?')== false) return false;")
        End If
    End Sub

    Private Sub loadEntidad()
        Try
            Dim objfunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
            If Session.Item("IdTipoEntidad") = "02" Then
                objfunciones.GetEntidades(Me.cboEndosatario, "03")
            End If
            If Session.Item("IdTipoEntidad") = "03" Then
                objfunciones.GetEntidades(Me.cboEndosatario, "02")
            End If
        Catch ex As Exception
            pr_IMPR_MENS("Error al llenar entidades: " & ex.ToString)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
