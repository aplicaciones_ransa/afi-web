Imports Library.AccesoBL
Imports System.Text
Imports Infodepsa
Imports System.Data

Public Class ReporteKardexDetalle
    Inherits System.Web.UI.Page
    Private objReporte As Reportes = New Reportes
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroOperacion As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDepositante As System.Web.UI.WebControls.Label
    'Protected WithEvents lblAlmacen As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoAlmacen As System.Web.UI.WebControls.Label
    'Protected WithEvents lblFechaIngreso As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMontoInicial As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMontoDeuda As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRegimen As System.Web.UI.WebControls.Label
    'Protected WithEvents lblVctoPrestamo As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroProrroga As System.Web.UI.WebControls.Label
    'Protected WithEvents lblVctoWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblVctoLimite As System.Web.UI.WebControls.Label
    'Protected WithEvents dgDCR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgLiberacion As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblNroGarantia As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents tblPrincipal As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Session.LCID = 10250
        Try
            If (Request.IsAuthenticated) And InStr(Session("PagId"), "28") Then
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "28"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Me.lblOpcion.Text = "Detalle del Warrant"
                    BindDatagrid()
                End If
            Else
                Response.Redirect("Salir.aspx?caduco=1", False)
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub BindDatagrid()
        Dim ds As DataSet
        Dim dr As DataRow
        ds = objReporte.gCNGetDetalleWarrant(Request.QueryString("sCO_EMPR"), Request.QueryString("sCO_UNID"),
                                            Request.QueryString("sTI_TITU"), Request.QueryString("sNU_TITU"))
        dr = ds.Tables(0).Rows(0)
        Me.lblAlmacen.Text = dr("DE_DIRE_ALMA")
        Me.lblDepositante.Text = dr("NO_CLIE_REPO")
        Me.lblFechaIngreso.Text = dr("FE_TITU")
        Me.lblMontoDeuda.Text = dr("CO_MONE") & " " & String.Format("{0:##,##0.00}", dr("IM_DEUD"))
        Me.lblMontoInicial.Text = dr("CO_MONE") & " " & String.Format("{0:##,##0.00}", dr("IM_TITU"))
        Me.lblNroGarantia.Text = dr("NRO_GARA")
        Me.lblNroOperacion.Text = dr("NU_DOCU_RECE")
        Me.lblNroProrroga.Text = dr("NU_SECU")
        Me.lblNroWarrant.Text = dr("NU_TITU")
        Me.lblRegimen.Text = dr("DSC_REGI")
        Me.lblTipoAlmacen.Text = dr("DE_TIPO_ALMA")
        Me.lblVctoLimite.Text = IIf(IsDBNull(dr("FE_VENC_LIMI")), "", dr("FE_VENC_LIMI"))
        Me.lblVctoPrestamo.Text = IIf(IsDBNull(dr("FE_VIGE_BANC")), "", dr("FE_VIGE_BANC"))
        Me.lblVctoWarrant.Text = IIf(IsDBNull(dr("FE_VENC")), "", dr("FE_VENC"))

        Me.dgDCR.DataSource = ds.Tables(1)
        Me.dgDCR.DataBind()
        Me.dgLiberacion.DataSource = ds.Tables(2)
        Me.dgLiberacion.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.doc"
        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True
        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.ContentEncoding = System.Text.Encoding.UTF7
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        'Response.Charset = "UTF-8"
        'Response.ContentEncoding = Encoding.Default
        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False
        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
        Me.tblPrincipal.RenderControl(htmlWriter)
        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objReporte Is Nothing) Then
            objReporte = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
