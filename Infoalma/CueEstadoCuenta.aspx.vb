Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class CueEstadoCuenta
    Inherits System.Web.UI.Page
    Protected dvTCDOCU_CLIE As DataView
    Private objCCorrientes As clsCNCCorrientes
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    Protected WithEvents drpMoneda As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnImprimir As System.Web.UI.WebControls.Button
    'Protected WithEvents dgYENE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgSOLE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgLIBR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgAMER As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgCANA As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgEURO As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents ddTTMONE As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lbMS_REGI As System.Web.UI.WebControls.Label
    'Protected WithEvents lbAMER As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_AMER As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTAMER As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbES_AMER As System.Web.UI.WebControls.Label
    'Protected WithEvents lbCANA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_CANA As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTCANA As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbES_CANA As System.Web.UI.WebControls.Label
    'Protected WithEvents lbEURO As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_EURO As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTEURO As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbES_EURO As System.Web.UI.WebControls.Label
    'Protected WithEvents lbLIBR As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_LIBR As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTLIBR As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbES_LIBR As System.Web.UI.WebControls.Label
    'Protected WithEvents lbSOLE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_SOLE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTSOLE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lbES_SOLE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbYENE As System.Web.UI.WebControls.Label
    'Protected WithEvents lbTO_YENE As System.Web.UI.WebControls.Label
    'Protected WithEvents dgTYENE As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "ESTADO_CUENTA") Then
            Me.lblOpcion.Text = "Estado de Cuenta"
            Session("sPA_PRIN") = ""
            Session("sNO_PAGI_ANTE") = "CueEstadoCuenta.aspx"

            If Not Page.IsPostBack Then
                Session.Add("dvRE_GENE_AMER", "")
                Session.Add("dvRE_GENE_CANA", "")
                Session.Add("dvRE_GENE_EURO", "")
                Session.Add("dvRE_GENE_LIBR", "")
                Session.Add("dvRE_GENE_SOLE", "")
                Session.Add("dvRE_GENE_YENE", "")
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "ESTADO_CUENTA"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                LlenarMonedas()
                LimpiarCampos()
                MostrarEstadoCuenta()
                objAccesoWeb = New clsCNAccesosWeb
                objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "C", "ESTADO_CUENTA", "CONSULTA ESTADO DE CUENTA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub BindDataGrid()
        objCCorrientes = New clsCNCCorrientes
        Dim dsTCDOCU_CLIE As DataSet
        Dim i As Integer
        Dim k As Integer = 0
        Try
            Session("dvRE_GENE_AMER") = Nothing
            Session("dvRE_GENE_CANA") = Nothing
            Session("dvRE_GENE_EURO") = Nothing
            Session("dvRE_GENE_LIBR") = Nothing
            Session("dvRE_GENE_SOLE") = Nothing
            Session("dvRE_GENE_YENE") = Nothing

            Dim dtTO_CUEN As New DataTable
            dtTO_CUEN.Columns.Add("DEBE", GetType(System.String))
            dtTO_CUEN.Columns.Add("HABER", GetType(System.String))
            dtTO_CUEN.Columns.Add("SALDO", GetType(System.String))
            With objCCorrientes
                .gCADGetEstadodeCuenta(Session.Item("CoEmpresa"), CStr(FormatDateTime(Now(), DateFormat.ShortDate)), Session.Item("IdSico"))
                dsTCDOCU_CLIE = .fn_devuelvedataset
            End With
            dsTCDOCU_CLIE.Tables(0).Columns.Add("Saldos", System.Type.GetType("System.Decimal"))

            Dim dvTTMONE As DataView = dsTCDOCU_CLIE.Tables(1).DefaultView
            Dim dvTDDOCU_CLIE As DataView = dsTCDOCU_CLIE.Tables(0).DefaultView
            Dim dvTDDOCU_CLIE_SESS As DataView = dsTCDOCU_CLIE.Tables(0).DefaultView

            Dim dTO_DEBE As Double
            Dim dTO_HABE As Double
            Dim drNU_REGI As DataRow
            Me.lbMS_REGI.Text = ""

            If dvTDDOCU_CLIE.Count = 0 Then
                Me.lbMS_REGI.Text = "No se encontr� Registros"
            ElseIf dvTDDOCU_CLIE.Count > 0 Then
                For i = 1 To Me.ddTTMONE.Items.Count - 1
                    dTO_DEBE = 0
                    dTO_HABE = 0
                    dtTO_CUEN = New DataTable
                    dtTO_CUEN.Columns.Add("DEBE", GetType(System.Decimal))
                    dtTO_CUEN.Columns.Add("HABER", GetType(System.Decimal))
                    dtTO_CUEN.Columns.Add("SALDO", GetType(System.Decimal))

                    dvTDDOCU_CLIE.RowFilter = "[MONEDA ORIGEN]='" + ddTTMONE.Items(i).Value + "'"
                    If dvTDDOCU_CLIE.Count > 0 Then
                        Dim dvTEMP As New DataView
                        dvTEMP.Table = dvTDDOCU_CLIE.Table.Copy
                        dvTEMP.Table.Clear()
                        Dim dvrTCDOCU_CLIE As DataRowView
                        Dim drTEMP As DataRow
                        For Each dvrTCDOCU_CLIE In dvTDDOCU_CLIE
                            dTO_DEBE += CDbl(dvrTCDOCU_CLIE.Item(11))
                            dTO_HABE += CDbl(dvrTCDOCU_CLIE.Item(12))
                            drTEMP = dvTEMP.Table.NewRow()
                            Dim j As Integer
                            For j = 0 To 15
                                drTEMP(j) = dvrTCDOCU_CLIE.Row.Item(j)
                            Next
                            dvTEMP.Table.Rows.Add(drTEMP)
                        Next
                        drNU_REGI = dtTO_CUEN.NewRow()
                        If dvTDDOCU_CLIE.Count > 0 Then
                            drNU_REGI("DEBE") = CType(dTO_DEBE, Decimal)
                            drNU_REGI("HABER") = CType(dTO_HABE, Decimal)
                            drNU_REGI("SALDO") = CType((dTO_DEBE - dTO_HABE), Decimal)
                        Else
                            drNU_REGI("DEBE") = 0
                            drNU_REGI("HABER") = 0
                            drNU_REGI("SALDO") = 0
                        End If
                        dtTO_CUEN.Rows.Add(drNU_REGI)

                        If ddTTMONE.Items(i).Value = "DOL" Then
                            Me.lbAMER.Text = "DOLARES AMERICANOS"
                            Session("dvRE_GENE_AMER") = dvTEMP
                            Me.dgAMER.DataSource = dvTEMP
                            Me.dgAMER.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgAMER.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgAMER.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_AMER.Text = "TOTAL DOLARES AMERICANOS"
                            dgTAMER.DataSource = dtTO_CUEN
                            dgTAMER.DataBind()
                            Me.lbES_AMER.Style.Item("HEIGHT") = 40
                        ElseIf ddTTMONE.Items(i).Value = "DOC" Then
                            Me.lbCANA.Text = "DOLARES CANADIENSES"
                            Session("dvRE_GENE_CANA") = dvTEMP
                            Me.dgCANA.DataSource = dvTEMP
                            Me.dgCANA.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgCANA.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgCANA.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_CANA.Text = "TOTAL DOLARES CANADIENSES"
                            Me.dgTCANA.DataSource = dtTO_CUEN
                            Me.dgTCANA.DataBind()
                            Me.lbES_CANA.Style.Item("HEIGHT") = 40
                        ElseIf ddTTMONE.Items(i).Value = "EUR" Then
                            Me.lbEURO.Text = "EUROS"
                            Session("dvRE_GENE_EURO") = dvTEMP
                            Me.dgEURO.DataSource = dvTEMP
                            Me.dgEURO.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgEURO.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgEURO.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_EURO.Text = "TOTAL EUROS"
                            Me.dgTEURO.DataSource = dtTO_CUEN
                            Me.dgTEURO.DataBind()
                            Me.lbES_EURO.Style.Item("HEIGHT") = 40
                        ElseIf ddTTMONE.Items(i).Value = "LIB" Then
                            Me.lbLIBR.Text = "LIBRAS ESTERLINAS"
                            Session("dvRE_GENE_LIBR") = dvTEMP
                            Me.dgLIBR.DataSource = dvTEMP
                            Me.dgLIBR.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgLIBR.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgLIBR.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_LIBR.Text = "TOTAL LIBRAS ESTERLINAS"
                            Me.dgTLIBR.DataSource = dtTO_CUEN
                            Me.dgTLIBR.DataBind()
                            Me.lbES_LIBR.Style.Item("HEIGHT") = 40
                        ElseIf ddTTMONE.Items(i).Value = "SOL" Then
                            Me.lbSOLE.Text = "NUEVOS SOLES"
                            Session("dvRE_GENE_SOLE") = dvTEMP
                            Me.dgSOLE.DataSource = dvTEMP
                            Me.dgSOLE.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgSOLE.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgSOLE.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_SOLE.Text = "TOTAL NUEVOS SOLES"
                            Me.dgTSOLE.DataSource = dtTO_CUEN
                            Me.dgTSOLE.DataBind()
                            Me.lbES_SOLE.Style.Item("HEIGHT") = 40
                        ElseIf ddTTMONE.Items(i).Value = "YEN" Then
                            Me.lbYENE.Text = "YENES"
                            Session("dvRE_GENE_YENE") = dvTEMP
                            Me.dgYENE.DataSource = dvTEMP
                            Me.dgYENE.DataBind()
                            Dim item As DataGridItem
                            For Each item In Me.dgYENE.Items
                                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                                'For k = 0 To 8
                                If item.Cells(7).Text = "VENCIDO" Then
                                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                    'Me.dgYENE.Columns(k).ItemStyle.BackColor = System.Drawing.Color.FromName("#ffcc66")
                                End If
                                'Next
                            Next
                            Me.lbTO_YENE.Text = "TOTAL YENES"
                            Me.dgTYENE.DataSource = dtTO_CUEN
                            Me.dgTYENE.DataBind()
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub dgCANA_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgCANA.PageIndexChanged
        Try
            dgCANA.CurrentPageIndex = e.NewPageIndex
            dvTCDOCU_CLIE = CType(Session("dvRE_GENE_CANA"), DataView)
            Me.dgCANA.DataSource = dvTCDOCU_CLIE
            Me.dgCANA.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgCANA.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgCANA)
        End Try
    End Sub

    Private Sub dgEURO_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgEURO.PageIndexChanged
        Try
            dgEURO.CurrentPageIndex = e.NewPageIndex
            dvTCDOCU_CLIE = CType(Session("dvRE_GENE_EURO"), DataView)
            Me.dgEURO.DataSource = dvTCDOCU_CLIE
            Me.dgEURO.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgEURO.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgEURO)
        End Try
    End Sub

    Private Sub dgLIBR_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgLIBR.PageIndexChanged
        Try
            dgLIBR.CurrentPageIndex = e.NewPageIndex
            dvTCDOCU_CLIE = CType(Session("dvRE_GENE_LIBR"), DataView)
            Me.dgLIBR.DataSource = dvTCDOCU_CLIE
            Me.dgLIBR.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgLIBR.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgLIBR)
        End Try
    End Sub

    Private Sub dgSOLE_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSOLE.PageIndexChanged
        Try
            dgSOLE.CurrentPageIndex = e.NewPageIndex
            dvTCDOCU_CLIE = CType(Session("dvRE_GENE_SOLE"), DataView)
            Me.dgSOLE.DataSource = dvTCDOCU_CLIE
            Me.dgSOLE.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgSOLE.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgSOLE)
        End Try
    End Sub

    Private Sub dgYENE_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgYENE.PageIndexChanged
        Try
            dgYENE.CurrentPageIndex = e.NewPageIndex
            dvTCDOCU_CLIE = CType(Session("dvRE_GENE_YENE"), DataView)
            Me.dgYENE.DataSource = dvTCDOCU_CLIE
            Me.dgYENE.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgYENE.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgYENE)
        End Try
    End Sub

    Private Sub dgAMER_PageIndexChanged(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAMER.PageIndexChanged
        Try
            dgAMER.CurrentPageIndex = e.NewPageIndex
            Dim dvTCDOCU_AMER As DataView = CType(Session("dvRE_GENE_AMER"), DataView)
            Me.dgAMER.DataSource = dvTCDOCU_AMER
            Me.dgAMER.DataBind()
            Dim item As DataGridItem
            For Each item In Me.dgAMER.Items
                item.Cells(8).Text = CStr(FormatNumber(CDbl(item.Cells(5).Text) - CDbl(item.Cells(6).Text), 2))
                If item.Cells(7).Text = "VENCIDO" Then
                    item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                End If
            Next
        Catch
            pg_DATA_LIMP(dgAMER)
        End Try
    End Sub

    Private Sub LimpiarCampos()
        Try
            Me.lbAMER.Text = ""
            Me.lbCANA.Text = ""
            Me.lbEURO.Text = ""
            Me.lbLIBR.Text = ""
            Me.lbSOLE.Text = ""
            Me.lbYENE.Text = ""
            Me.lbTO_AMER.Text = ""
            Me.lbTO_CANA.Text = ""
            Me.lbTO_EURO.Text = ""
            Me.lbTO_LIBR.Text = ""
            Me.lbTO_SOLE.Text = ""
            Me.lbTO_YENE.Text = ""
            Me.lbES_AMER.Text = ""
            Me.lbES_CANA.Text = ""
            Me.lbES_EURO.Text = ""
            Me.lbES_LIBR.Text = ""
            Me.lbES_SOLE.Text = ""
            Me.lbES_AMER.Style.Item("HEIGHT") = 0
            Me.lbES_CANA.Style.Item("HEIGHT") = 0
            Me.lbES_EURO.Style.Item("HEIGHT") = 0
            Me.lbES_LIBR.Style.Item("HEIGHT") = 0
            Me.lbES_SOLE.Style.Item("HEIGHT") = 0

            Me.dgAMER.DataSource = Nothing
            Me.dgCANA.DataSource = Nothing
            Me.dgEURO.DataSource = Nothing
            Me.dgLIBR.DataSource = Nothing
            Me.dgSOLE.DataSource = Nothing
            Me.dgYENE.DataSource = Nothing
            Me.dgAMER.DataBind()
            Me.dgCANA.DataBind()
            Me.dgEURO.DataBind()
            Me.dgLIBR.DataBind()
            Me.dgSOLE.DataBind()
            Me.dgYENE.DataBind()

            Me.dgTAMER.DataSource = Nothing
            Me.dgTCANA.DataSource = Nothing
            Me.dgTEURO.DataSource = Nothing
            Me.dgTLIBR.DataSource = Nothing
            Me.dgTSOLE.DataSource = Nothing
            Me.dgTYENE.DataSource = Nothing
            Me.dgTAMER.DataBind()
            Me.dgTCANA.DataBind()
            Me.dgTEURO.DataBind()
            Me.dgTLIBR.DataBind()
            Me.dgTSOLE.DataBind()
            Me.dgTYENE.DataBind()
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarMonedas()
        Try
            Dim dt As DataTable
            objCCorrientes = New clsCNCCorrientes
            With objCCorrientes
                .gCNGetMonedas()
                dt = .fn_devuelvedatatable
            End With
            Me.ddTTMONE.Items.Add(New ListItem("", ""))
            For Each dr As DataRow In dt.Rows
                Me.ddTTMONE.Items.Add(New ListItem(dr("DESCRIPCION_MONEDA"), dr("CODIGO_MONEDA")))
            Next
            ddTTMONE.Items(0).Selected = True
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub pg_DATA_LIMP(ByVal pDG_GENE As WebControls.DataGrid)
        Dim dtTA_NUEV As DataTable
        Dim dsCO_NUEV As DataSet

        dsCO_NUEV.Tables.Add(dtTA_NUEV)
        pDG_GENE.DataSource = dsCO_NUEV
        pDG_GENE.DataBind()
    End Sub

    Private Sub MostrarEstadoCuenta()
        LimpiarCampos()
        BindDataGrid()
    End Sub

    ReadOnly Property pDE_MONE()
        Get
            Return ddTTMONE.SelectedItem.Text
        End Get
    End Property

    ReadOnly Property pCO_MONE()
        Get
            Return ddTTMONE.SelectedItem.Value
        End Get
    End Property

    Protected Overrides Sub Finalize()
        If Not (objCCorrientes Is Nothing) Then
            objCCorrientes = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub


    Private Sub dgAMER_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAMER.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK
                    objCCorrientes = New clsCNCCorrientes
                    Dim dt As DataTable
                    dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                    If dt.Rows.Count <> 0 Then
                        CType(e.Item.FindControl("imgVer"), ImageButton).ImageUrl = "Images/Ver.jpg"
                        CType(e.Item.FindControl("imgVer"), ImageButton).ToolTip = "Ver Abono"
                        CType(e.Item.FindControl("imgVer"), ImageButton).Enabled = True
                        CType(e.Item.FindControl("imgVer"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                    Else
                        CType(e.Item.FindControl("imgVer"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                        CType(e.Item.FindControl("imgVer"), ImageButton).ToolTip = "Sin Abono"
                        CType(e.Item.FindControl("imgVer"), ImageButton).Enabled = False
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub dgCANA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCANA.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK

                    If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Then
                        objCCorrientes = New clsCNCCorrientes
                        Dim dt As DataTable
                        dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                        If dt.Rows.Count <> 0 Then
                            CType(e.Item.FindControl("imgVer1"), ImageButton).ImageUrl = "Images/Ver.jpg"
                            CType(e.Item.FindControl("imgVer1"), ImageButton).ToolTip = "Ver Abono"
                            CType(e.Item.FindControl("imgVer1"), ImageButton).Enabled = True
                            CType(e.Item.FindControl("imgVer1"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                        Else
                            CType(e.Item.FindControl("imgVer1"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                            CType(e.Item.FindControl("imgVer1"), ImageButton).ToolTip = "Sin Abono"
                            CType(e.Item.FindControl("imgVer1"), ImageButton).Enabled = False
                        End If
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub dgEURO_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEURO.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK

                    If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Then
                        objCCorrientes = New clsCNCCorrientes
                        Dim dt As DataTable
                        dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                        If dt.Rows.Count <> 0 Then
                            CType(e.Item.FindControl("imgVer2"), ImageButton).ImageUrl = "Images/Ver.jpg"
                            CType(e.Item.FindControl("imgVer2"), ImageButton).ToolTip = "Ver Abono"
                            CType(e.Item.FindControl("imgVer2"), ImageButton).Enabled = True
                            CType(e.Item.FindControl("imgVer2"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                        Else
                            CType(e.Item.FindControl("imgVer2"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                            CType(e.Item.FindControl("imgVer2"), ImageButton).ToolTip = "Sin Abono"
                            CType(e.Item.FindControl("imgVer2"), ImageButton).Enabled = False
                        End If
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub dgLIBR_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLIBR.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK

                    If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Then
                        objCCorrientes = New clsCNCCorrientes
                        Dim dt As DataTable
                        dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                        If dt.Rows.Count <> 0 Then
                            CType(e.Item.FindControl("imgVer3"), ImageButton).ImageUrl = "Images/Ver.jpg"
                            CType(e.Item.FindControl("imgVer3"), ImageButton).ToolTip = "Ver Abono"
                            CType(e.Item.FindControl("imgVer3"), ImageButton).Enabled = True
                            CType(e.Item.FindControl("imgVer3"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                        Else
                            CType(e.Item.FindControl("imgVer3"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                            CType(e.Item.FindControl("imgVer3"), ImageButton).ToolTip = "Sin Abono"
                            CType(e.Item.FindControl("imgVer3"), ImageButton).Enabled = False
                        End If
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub dgSOLE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSOLE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK


                    If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Then
                        objCCorrientes = New clsCNCCorrientes
                        Dim dt As DataTable
                        dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                        If dt.Rows.Count <> 0 Then
                            CType(e.Item.FindControl("imgVer4"), ImageButton).ImageUrl = "Images/Ver.jpg"
                            CType(e.Item.FindControl("imgVer4"), ImageButton).ToolTip = "Ver Abono"
                            CType(e.Item.FindControl("imgVer4"), ImageButton).Enabled = True
                            CType(e.Item.FindControl("imgVer4"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                        Else
                            CType(e.Item.FindControl("imgVer4"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                            CType(e.Item.FindControl("imgVer4"), ImageButton).ToolTip = "Sin Abono"
                            CType(e.Item.FindControl("imgVer4"), ImageButton).Enabled = False
                        End If
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

    Private Sub dgYENE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgYENE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NDE" Or CStr(e.Item.DataItem("DOCUMENTO")) = "NCR" Then
                    Dim sCA_LINK As String = "<A onclick=Javascript:fn_DetDoc('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "'); href='#'> " &
                                                          "" & CStr(e.Item.DataItem("DOCUMENTO")) & "</a>"
                    e.Item.Cells(0).Text = sCA_LINK

                    If CStr(e.Item.DataItem("DOCUMENTO")) = "FAC" Then
                        objCCorrientes = New clsCNCCorrientes
                        Dim dt As DataTable
                        dt = objCCorrientes.gCADGetAbono(Session.Item("CoEmpresa"), CStr(e.Item.DataItem("UNIDAD")), CStr(e.Item.DataItem("DOCUMENTO")).Trim, CStr(e.Item.DataItem("NUMERO")))
                        If dt.Rows.Count <> 0 Then
                            CType(e.Item.FindControl("imgVer5"), ImageButton).ImageUrl = "Images/Ver.jpg"
                            CType(e.Item.FindControl("imgVer5"), ImageButton).ToolTip = "Ver Abono"
                            CType(e.Item.FindControl("imgVer5"), ImageButton).Enabled = True
                            CType(e.Item.FindControl("imgVer5"), ImageButton).Attributes.Add("onclick", "Javascript:AbrirAbono('" & CStr(e.Item.DataItem("UNIDAD")) & "','" & CStr(e.Item.DataItem("DOCUMENTO")).Trim & "','" & CStr(e.Item.DataItem("NUMERO")) & "')")
                        Else
                            CType(e.Item.FindControl("imgVer5"), ImageButton).ImageUrl = "Images/Eliminar.JPG"
                            CType(e.Item.FindControl("imgVer5"), ImageButton).ToolTip = "Sin Abono"
                            CType(e.Item.FindControl("imgVer5"), ImageButton).Enabled = False
                        End If
                    End If

                Else
                    e.Item.Cells(0).Text = CStr(e.Item.DataItem("DOCUMENTO"))
                End If
            End If
        Catch e1 As Exception
            Me.lblError.Text = e1.Message
        End Try
    End Sub

End Class
