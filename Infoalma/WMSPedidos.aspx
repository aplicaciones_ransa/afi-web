<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WMSPedidos.aspx.vb" Inherits="WMSPedidos" %>

<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WMSPedidos</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <uc1:Header ID="Header2" runat="server"></uc1:Header>
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%" align="center">
                                            <table id="Table20" cellspacing="0" cellpadding="0" width="670" border="0">
                                                <tr>
                                                    <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                                    <td background="Images/table_r1_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r2_c1.gif"></td>
                                                    <td>
                                                        <table id="Table6" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Text">Estado</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td class="Text" align="left">
                                                                    <asp:DropDownList ID="cboEstado" runat="server" CssClass="Text">
                                                                        <asp:ListItem Value="01">Preparados</asp:ListItem>
                                                                        <asp:ListItem Value="02">Parcial</asp:ListItem>
                                                                        <asp:ListItem Value="03">Sin preparar</asp:ListItem>
                                                                        <asp:ListItem Selected="True" Value="00">Todos</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="Text">Ingreso</td>
                                                                <td width="5%" class="Text">Del
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td align="left">
                                                                    <input class="text" onkeypress="validarcharfecha()" id="txtFechaInicial" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaInicial" runat="server">
                                                                    <input class="text" id="btnFechaInicial" type="button"
                                                                        value="..." name="btnFecha"></td>
                                                                <td class="Text" width="5%">Al</td>
                                                                <td width="1%">:</td>
                                                                <td class="Text" align="left">
                                                                    <input class="text" onkeypress="validarcharfecha()" id="txtFechaFinal" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaFinal" runat="server">
                                                                    <input class="text" id="btnFechaFinal" type="button"
                                                                        value="..." name="btnFecha"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Pedido</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td align="left" class="Text">
                                                                    <asp:TextBox ID="txtPedido" runat="server" CssClass="Text" Width="120px"></asp:TextBox></td>
                                                                <td class="Text">Orden
                                                                </td>
                                                                <td class="Text" style="font-size: small" width="5%">:
                                                                </td>
                                                                <td width="1%"></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtNroInterno" runat="server" CssClass="Text" Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td class="Text" style="font-size: small" width="5%"></td>
                                                                <td width="1%"></td>
                                                                <td align="left" class="Text"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="6" background="Images/table_r2_c3.gif"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                                    <td background="Images/table_r3_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" AutoGenerateColumns="False" BorderColor="Gainsboro"
                                                OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="30" Width="100%">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ob_oid" HeaderText="Nro. Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="dtimecre" HeaderText="Fecha Recepci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Cliente">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="bill_addr1" HeaderText="Direcci&#243;n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="bill_addr2" HeaderText="Departamento">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Galones_Totales" HeaderText="Total Unidades" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Galones_Preparados" HeaderText="Unidades Preparados" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_RETI_DEST" HeaderText="Pedido">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="tblLeyenda" border="0" cellspacing="4" cellpadding="0" width="40%">
                                                <tr>
                                                    <td class="td" colspan="2">Leyenda</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#f5dc8c" width="30%"></td>
                                                    <td class="Leyenda" width="70%" align="center">DCR no Cerrado en revisi�n</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
