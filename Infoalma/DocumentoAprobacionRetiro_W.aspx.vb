Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Library.AccesoBL
Imports Infodepsa
Imports System.Data

Public Class DocumentoAprobacionRetiro_W
    Inherits System.Web.UI.Page
    Private objParametro As clsCNParametro
    Private objRetiro As New clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objWMS As clsCNWMS
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = ConfigurationManager.AppSettings("ConnectionStringWE")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroPedido As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents imgVer As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgEditar As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgEliminarDirec As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboEstSitu As System.Web.UI.WebControls.DropDownList
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            Try
                If Page.IsPostBack = False Then
                    ListaReferencia()
                    LlenarAlmacenes()
                    If Session("Co_AlmaW") = "" Then
                        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                    End If
                    inicializa()
                    If Session.Item("MensajePedido") <> Nothing Then
                        pr_IMPR_MENS(Session.Item("MensajePedido"))
                        Session.Remove("MensajePedido")
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ListaReferencia()
        objParametro = New clsCNParametro
        objParametro.gCNListarReferencia(Me.cboEstSitu, "9")
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub inicializa()
        If Request.Item("Estado") <> "" Then
            Session.Add("EstadoDoc", Request.Item("Estado"))
        End If
        Dim strResult As String()
        Select Case Session.Item("EstadoDoc")
            Case "02"
                Me.lblOpcion.Text = "Consultar Documentos Emitidos WMS"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_EMITIDO"), "-")
            Case "04"
                Me.lblOpcion.Text = "Consultar Documentos Registrados WMS"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_REGISTRADO"), "-")
            Case "06"
                Me.lblOpcion.Text = "Consultar Documentos Rechazados WMS"
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_RECHAZADO"), "-")
        End Select

        Session.Item("Page") = strResult(0)
        Session.Item("Opcion") = strResult(1)
        Dim dttFecha As DateTime
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        dttFecha = Date.Today.Subtract(TimeSpan.FromDays(30))
        Dia = "00" + CStr(dttFecha.Day)
        Mes = "00" + CStr(dttFecha.Month)
        Anio = "0000" + CStr(dttFecha.Year)
        If Me.txtFechaInicial.Value = "" Then
            Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        End If
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        If Me.txtFechaFinal.Value = "" Then
            Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        End If

        Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
        'If Session.Item("strNroRetiro") <> Nothing Then
        '    pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("strNroRetiro"))
        '    Session.Remove("strNroRetiro")
        'End If

        InicializaBusqueda()
        Bindatagrid()

    End Sub

    Private Sub Bindatagrid()
        Try
            objWMS = New clsCNWMS
            Dim dt As DataTable
            Dim FechaInicial As String
            Dim FechaFinal As String
            FechaInicial = txtFechaInicial.Value.Substring(6, 4) & txtFechaInicial.Value.Substring(3, 2) & txtFechaInicial.Value.Substring(0, 2)
            FechaFinal = txtFechaFinal.Value.Substring(6, 4) & txtFechaFinal.Value.Substring(3, 2) & txtFechaFinal.Value.Substring(0, 2)
            dt = objWMS.gCNGetBuscarPedidos(Session("CoEmpresa"), Session.Item("IdSico"), Session.Item("IdTipoEntidad"),
                                           Me.txtNroPedido.Text, FechaInicial, FechaFinal, Me.cboEstSitu.SelectedValue, Session.Item("EstadoDoc"), cboAlmacen.SelectedValue)
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Session.Add("dtPedido", dt)
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaPedido") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaPedido").ToString
            strFiltros = Split(strCadenaBusqueda, "-")
            Me.cboEstSitu.SelectedValue = strFiltros(0)
            Me.txtNroPedido.Text = strFiltros(1)
            txtFechaInicial.Value = strFiltros(2)
            txtFechaFinal.Value = strFiltros(3)
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaPedido", Me.cboEstSitu.SelectedValue & "-" &
          Me.txtNroPedido.Text & "-" & txtFechaInicial.Value & "-" & txtFechaFinal.Value)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.lblMensaje.Text = ""
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = CType(Session.Item("dtPedido"), DataTable)
        Me.dgResultado.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Factura.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtPedido"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Public Sub imgVer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgVer As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgVer.Parent.Parent, DataGridItem)
            Session.Add("NroPedido", dgi.Cells(0).Text())
            Session.Add("NroLib", dgi.Cells(10).Text())
            Session.Add("PeriodoAnual", dgi.Cells(11).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(12).Text())
            Session.Add("FchEmision", dgi.Cells(1).Text())
            Session.Add("Retirado", dgi.Cells(3).Text())
            Session.Add("Destino", dgi.Cells(4).Text())
            Session.Add("CodCliente", dgi.Cells(8).Text())
            Session.Add("CodAlmacen", dgi.Cells(9).Text())

            Response.Redirect("AlmDetallePedido.aspx?var=1", False)
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim btnEditar As New ImageButton
        Dim btnVer As New ImageButton
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Select Case e.Item.DataItem("ESTADO")
                    Case "PENDIENTE"
                        e.Item.BackColor = System.Drawing.Color.FromArgb(245, 220, 140)
                    Case "COMPLETADO"
                        e.Item.BackColor = System.Drawing.Color.FromArgb(173, 213, 230)
                    Case "EN PROCESO"
                        e.Item.BackColor = System.Drawing.Color.FromArgb(50, 205, 50)
                End Select
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) = 0 Then
            Session("Co_AlmaS") = Session("Co_AlmaW")
            Response.Redirect("DocumentoAprobacionRetiro.aspx", False)
        End If
    End Sub
End Class
