Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class ConfiguracionMenu
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objMenu As LibCapaNegocio.clsCNMenu
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboGrupo As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And ValidarPagina("PAG20") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                CargarCombos()
                LlenaGrid()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboGrupo(cboGrupo)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
    End Sub

    Private Sub LlenaGrid()
        Try
            objMenu = New LibCapaNegocio.clsCNMenu
            objFuncion = New LibCapaNegocio.clsFunciones
            With objMenu
                .intCodModulo = 1
                .strCodGrupo = cboGrupo.SelectedItem.Value
                objFuncion.gCargaGrid(dgdResultado, .gdtConseguirPlantillaMarcadaMenu)
                lblRegistros.Text = .intFilasAfectadas.ToString & " registros"
            End With
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        GrabarPlantillaMenu()
        LlenaGrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "P", "DEPSAFIL", "GRABAR CONFIGURACION", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub GrabarPlantillaMenu()
        Try
            objMenu = New LibCapaNegocio.clsCNMenu
            With objMenu
                .strCodGrupo = cboGrupo.SelectedItem.Value
                .intCodModulo = CInt(Session.Item("CodModulo"))
                .strCodUsuario = CStr(Session.Item("UsuarioLogin"))
                If .gbolGrabarPlantillaMenu(dgdResultado) = True Then
                    lblError.Text = "El perfil de men� fue guardado con �xito!!"
                Else
                    lblError.Text = .strMensajeError
                End If
            End With

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function
End Class
