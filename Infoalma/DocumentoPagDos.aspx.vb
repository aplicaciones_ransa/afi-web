Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class DocumentoPagDos
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    'Protected WithEvents chkProtesto As System.Web.UI.WebControls.CheckBox
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents cboMoneda As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtOperacion As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtVencimiento As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents RequiredFieldValidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtValor As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtDC As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroCuenta As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtLugar As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaVenSICO As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTasa As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtBanco As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtOficina As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtFechaEmision As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents chkEmbarque As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblEntidadFinanciera As System.Web.UI.WebControls.Label
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroGarantia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            Session.Remove("Mensaje")
            If Not IsPostBack Then
                Me.lblOpcion.Text = "Datos del Endoso"
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "7"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.txtNroPagina.Value = Request.QueryString("Pag")
                Inicializa()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Inicializa()
        If Request.QueryString("var2") = 1 Then
            objDocumento.gUpdAccesoFirmar(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), Session.Item("IdUsuario"))
        End If
        Me.txtValor.EnableViewState = True
        Me.txtVencimiento.EnableViewState = True
        Me.txtFechaEmision.EnableViewState = True
        Me.txtFechaVenSICO.EnableViewState = True
        Dim Dia As String
        Dim Mes As String
        Dim Anio As String
        Dia = "00" + CStr(Now.Day)
        Mes = "00" + CStr(Now.Month)
        Anio = "0000" + CStr(Now.Year)
        Me.txtVencimiento.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
        Me.txtBanco.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtDC.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtLugar.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtOficina.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtTasa.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtOperacion.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.txtNroCuenta.Attributes.Add("onkeyup", "javascript:this.value=this.value.toUpperCase();")
        Me.chkProtesto.Attributes.Add("onclick", "javascript:Protesto(this);")

        'Me.btnGrabar.Attributes.Add("onclick", "javascript:if(Validar(this)==false)return false;")
        Me.btnEnviar.Attributes.Add("onclick", "javascript:if(Validar(this)==false)return false;")

        loadMoneda()
        ValidarControles()
        loadDataDocumento()
    End Sub

    Private Sub loadMoneda()
        Try
            objFunciones.GetMonedas(Me.cboMoneda)
        Catch ex As Exception
            pr_IMPR_MENS("ERROR: " & ex.ToString)
        End Try
    End Sub

    Private Sub ValidarControles()
        Dim dtControles As New DataTable
        Try
            dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroDoc"),
                        Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), 0)
            For i As Integer = 0 To dtControles.Rows.Count - 1
                Select Case dtControles.Rows(i)("COD_CTRL")
                    Case "06"
                        Me.btnGrabar.Visible = True
                        Me.btnEnviar.Visible = True
                        Me.txtBanco.Enabled = True
                        Me.txtDC.Enabled = True
                        Me.txtLugar.Enabled = True
                        Me.txtNroCuenta.Enabled = True
                        Me.txtOficina.Enabled = True
                        Me.txtOperacion.Enabled = True
                        Me.txtTasa.Enabled = True
                        Me.txtValor.Disabled = False
                        Me.txtVencimiento.Disabled = False
                        Me.txtNroGarantia.Enabled = True
                        Me.cboMoneda.Enabled = True
                        Me.chkEmbarque.Enabled = True
                        Me.chkProtesto.Enabled = True
                End Select
            Next
        Catch ex As Exception
            pr_IMPR_MENS("ERROR: " & ex.ToString)
        Finally
            dtControles.Dispose()
            dtControles = Nothing
        End Try
    End Sub

    Public Function Meses(ByVal value As Double) As String
        Select Case value
            Case 1 : Return "Enero"
            Case 2 : Return "Febrero"
            Case 3 : Return "Marzo"
            Case 4 : Return "Abril"
            Case 5 : Return "Mayo"
            Case 6 : Return "Junio"
            Case 7 : Return "Julio"
            Case 8 : Return "Agosto"
            Case 9 : Return "Septiembre"
            Case 10 : Return "Octubre"
            Case 11 : Return "Noviembre"
            Case 12 : Return "Diciembre"
        End Select
    End Function

    Private Sub loadDataDocumento()
        Try
            Dim drUsuario As DataRow
            Dim dttFechaVecimiento As DateTime
            drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
            Me.lblNroWarrant.Text = CType(drUsuario("NRO_DOCU"), String).Trim
            Me.lblEntidadFinanciera.Text = CType(drUsuario("NOMBREFINANCIERA"), String).Trim
            Me.txtOperacion.Text = CType(drUsuario("OPE_ENDO"), String).Trim
            Me.cboMoneda.SelectedValue = CType(drUsuario("MON_ENDO"), String).Trim
            Me.chkEmbarque.Checked = drUsuario("FLG_EMBA")
            Me.chkProtesto.Checked = drUsuario("FLG_PROT")
            If drUsuario("VAL_ENDO") <> 0 Then
                Me.txtValor.Value = String.Format("{0:##,##0.00}", drUsuario("VAL_ENDO"))
            Else
            End If
            Me.txtVencimiento.Value = drUsuario("VEN_DOCU")
            Me.txtTasa.Text = drUsuario("TAS_DOCU")
            Me.txtBanco.Text = CType(drUsuario("BAN_ENDO"), String).Trim
            Me.txtOficina.Text = CType(drUsuario("OFI_ENDO"), String).Trim
            Me.txtNroCuenta.Text = CType(drUsuario("NRO_CTA_ENDO"), String).Trim
            Me.txtDC.Text = CType(drUsuario("DC_ENDO"), String).Trim
            If CType(drUsuario("LUG_ENDO"), String).Trim = "" Then
                Me.txtLugar.Text = "Lima, " & Now.Day & " de " & Meses(Now.Month) & " del " & Now.Year
            Else
                Me.txtLugar.Text = CType(drUsuario("LUG_ENDO"), String).Trim
            End If
            Me.txtFechaVenSICO.Value = CType(drUsuario("FCH_VCTSICO"), String).Trim
            Me.txtFechaEmision.Value = CType(drUsuario("FECHACREACION"), String).Trim
            Me.txtNroGarantia.Text = drUsuario("NRO_GARA")
            drUsuario = Nothing
        Catch ex As Exception
            pr_IMPR_MENS("ERROR: " & ex.ToString)
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        If Me.txtNroPagina.Value = "1" Then
            Response.Redirect("DocumentoPagUnoWarrant.aspx", False)
        End If
        If Me.txtNroPagina.Value = "0" Then
            'Response.Redirect("DocumentoConsultar.aspx?Estado=01", False)
            Response.Redirect("DocumentoConsultar.aspx", False)
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            Dim strCodUsuario As String
            Dim strMontoEndoso As String
            Dim strDecimal As String
            If Me.txtValor.Value = "" Then
                strDecimal = "00"
                strMontoEndoso = "0"
            ElseIf Me.txtValor.Value.Length = 1 Or Me.txtValor.Value.Length = 2 Then
                strDecimal = "00"
                strMontoEndoso = Me.txtValor.Value
            Else
                strDecimal = Me.txtValor.Value.Substring(Len(txtValor.Value) - 2, 2)
                strDecimal = strDecimal.Replace(".", "")
                strDecimal = strDecimal.Replace(",", "")
                strMontoEndoso = Me.txtValor.Value.Substring(0, Len(txtValor.Value) - 3)
                strMontoEndoso = strMontoEndoso.Replace(".", "")
                strMontoEndoso = strMontoEndoso.Replace(",", "")
            End If

            If objDocumento.gUpdEndoso(Me.txtOperacion.Text, Me.cboMoneda.SelectedValue, Me.txtVencimiento.Value,
                           strMontoEndoso & "." & strDecimal, Me.txtTasa.Text, Session.Item("IdUsuario"), Session.Item("NroDoc"),
                           Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
                           Me.txtBanco.Text, Me.txtOficina.Text, Me.txtNroCuenta.Text, Me.txtDC.Text,
                           Me.txtLugar.Text, Me.chkEmbarque.Checked, Me.txtNroGarantia.Text.ToUpper, Me.chkProtesto.Checked, "") = "X" Then
                pr_IMPR_MENS("No se puede grabar porque el documento ha pasado para su aprobaci�n")
            Else
                pr_IMPR_MENS("Se grab� correctamente")
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "7", "GRABAR ENDOSO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            pr_IMPR_MENS("Se cancel� el proceso debido al siguiente error: " & ex.Message)
        End Try
    End Sub

    Public Function GeneraPDF(strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strIdTipoDocumento As String, strEmpresa As String, strIdUsuario As String) As String
        Dim strMensaje As String
        Try

            Dim drUsuario As DataRow
            Dim strTipoDocumento As String
            Dim blnEndoso As Boolean
            drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)

            strTipoDocumento = strIdTipoDocumento
            blnEndoso = True

            ' === Volvemos a crear el Warrant con endoso ====
            If strIdTipoDocumento <> "02" And strIdTipoDocumento <> "05" And strIdTipoDocumento <> "13" And strIdTipoDocumento <> "14" And strIdTipoDocumento <> "21" Then
                objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, strIdUsuario, "0", strNroDoc)
            End If
            If objReportes.strMensaje <> "0" Then
                'Session.Add("Mensaje", objReportes.strMensaje)
                strMensaje = objReportes.strMensaje
            Else
                strMensaje = "I"
            End If

            drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)
            If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim, strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento)
                strMensaje = "I"
            End If
            drUsuario = Nothing

            Return strMensaje
        Catch ex As Exception
            strMensaje = "ERROR " + ex.Message
            Return strMensaje
        End Try
    End Function

    Private Sub GrabaPDF(ByVal strNombArch As String, strIdUsuario As String, strNroDoc As String, strNroLib As String, strPeriodoAnual As String, strIdTipoDocumento As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, Contenido, strIdTipoDocumento)
        fs.Close()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
    Protected Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Try
            Dim strCodUsuario As String
            Dim strMontoEndoso As String
            Dim strDecimal As String
            If Me.txtValor.Value = "" Then
                strDecimal = "00"
                strMontoEndoso = "0"
            ElseIf Me.txtValor.Value.Length = 1 Or Me.txtValor.Value.Length = 2 Then
                strDecimal = "00"
                strMontoEndoso = Me.txtValor.Value
            Else
                strDecimal = Me.txtValor.Value.Substring(Len(txtValor.Value) - 2, 2)
                strDecimal = strDecimal.Replace(".", "")
                strDecimal = strDecimal.Replace(",", "")
                strMontoEndoso = Me.txtValor.Value.Substring(0, Len(txtValor.Value) - 3)
                strMontoEndoso = strMontoEndoso.Replace(".", "")
                strMontoEndoso = strMontoEndoso.Replace(",", "")
            End If

            If objDocumento.gUpdEndoso(Me.txtOperacion.Text, Me.cboMoneda.SelectedValue, Me.txtVencimiento.Value,
                           strMontoEndoso & "." & strDecimal, Me.txtTasa.Text, Session.Item("IdUsuario"), Session.Item("NroDoc"),
                           Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
                           Me.txtBanco.Text, Me.txtOficina.Text, Me.txtNroCuenta.Text, Me.txtDC.Text,
                           Me.txtLugar.Text, Me.chkEmbarque.Checked, Me.txtNroGarantia.Text.ToUpper, Me.chkProtesto.Checked, "ENV") = "X" Then
                pr_IMPR_MENS("No se puede grabar porque el documento ha pasado para su aprobaci�n")
            Else
                pr_IMPR_MENS("Se envi� correctamente")
            End If
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "7", "GRABAR ENDOSO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            btnGrabar.Visible = False
            btnEnviar.Visible = False

            '=== Generar Endoso Adjunto ====
            GeneraPDF(lblNroWarrant.Text, Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa, Session.Item("IdUsuario"))
            'GenerarPDFEndoso(lblNroWarrant.Text, Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa, Session.Item("IdUsuario"))

            '=== Regresa a la lista de endoso ====
            If Me.txtNroPagina.Value = "1" Then
                Response.Redirect("DocumentoPagUnoWarrant.aspx", False)
            End If
            If Me.txtNroPagina.Value = "0" Then
                'Response.Redirect("DocumentoConsultar.aspx?Estado=01", False)
                Response.Redirect("DocumentoConsultar.aspx", False)
            End If

        Catch ex As Exception
            pr_IMPR_MENS("Se cancel� el proceso debido al siguiente error: " & ex.Message)
        End Try
    End Sub
End Class
