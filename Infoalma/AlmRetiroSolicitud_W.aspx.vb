Imports Depsa.LibCapaNegocio
Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Infodepsa
Imports System.Data

Public Class AlmRetiroSolicitud_W
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objRetiro As clsCNRetiro
    Private objWMS As clsCNWMS
    Private objConexion As SqlConnection
    Private objTransaccion As SqlTransaction
    Private objUsuario As Usuario = New Usuario
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strCConOfioper As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringOfioper")
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPathDownload = ConfigurationManager.AppSettings.Item("RutaDownload")
    Dim CO_MODA_MOVI As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents dgPedido As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents txtDNI As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtBrevete As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtPlaca As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkPreDespacho As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkRegresar As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents chkImprime As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents chkPagoElec As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtFechaRecojo As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTipoUsuario As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents ltrEnviar As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtClienteDespacho As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtRetitradoPor As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        PoliticaCache()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Solicitud de Pedido WMS"
                Me.txtTipoUsuario.Value = Session("IdTipoUsuario")
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_NUEVO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Bindatagrid()
                    Dim dttFecha As DateTime
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    dttFecha = Date.Today
                    Dia = "00" + CStr(dttFecha.Day)
                    Mes = "00" + CStr(dttFecha.Month)
                    Anio = "0000" + CStr(dttFecha.Year)
                    Me.txtFechaRecojo.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    If Session.Item("MensajePedido") <> Nothing Then
                        pr_IMPR_MENS(Session.Item("MensajePedido"))
                        Session.Remove("MensajePedido")
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dtRetiro As DataTable
            objRetiro = New clsCNRetiro
            dtRetiro = CType(Session("dtWMS"), DataTable)
            Me.dgPedido.DataSource = dtRetiro
            Me.dgPedido.DataBind()

            If Me.dgPedido.Items.Count = 0 Then
                Me.lblMensaje.Text = "Agregue por lo menos un �tem a retirar"
                Me.ltrEnviar.Text = ""
                Exit Sub
            ElseIf Session("IdTipoUsuario") = "03" Or Session("IdTipoUsuario") = "01" Then
                Me.ltrEnviar.Text = "<INPUT id='btnEnviar' class='btn' value='Enviar' type='button' name='btnEnviar' onclick='javascript:openWindow(0);'>"
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            Dim dt As DataTable
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            dt = CType(Session("dtWMS"), DataTable)
            dt.Rows.RemoveAt(dgi.ItemIndex)
            Bindatagrid()
            Me.lblMensaje.Text = "Se encontraron " & Me.dgPedido.Items.Count & " Registros"
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub dgPedido_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPedido.ItemDataBound
        Dim btnCancelar As New ImageButton
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            btnCancelar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
            btnCancelar.Attributes.Add("onclick", "javascript:if(confirm('Desea eliminar el producto Nro " & e.Item.Cells(0).Text & " - " & e.Item.Cells(1).Text & " - Lote: " & e.Item.Cells(2).Text & "?')== false) return false;")
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
