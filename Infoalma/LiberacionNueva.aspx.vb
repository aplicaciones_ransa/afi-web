Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class LiberacionNueva
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objEntidad As Entidad = New Entidad
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEndosatario As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtDescripci�n As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboTipoMercaderia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "19") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "19"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("NroRetiro") <> Nothing Then
                    pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("NroRetiro"))
                    Session.Remove("NroRetiro")
                End If
                Me.lblOpcion.Text = "Consulta para generar Nueva liberaci�n"
                loadEndosatario()
                loadTipoMerc()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        BindDatagrid()
    End Sub

    Private Sub loadEndosatario()
        Dim dtEntidad As New DataTable
        Me.cboEndosatario.Items.Clear()
        dtEntidad = objEntidad.gGetEntidades("02", "")
        Me.cboEndosatario.Items.Add(New ListItem("--------------------Todos-------------------", "0"))
        For Each dr As DataRow In dtEntidad.Rows
            Me.cboEndosatario.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), CType(dr("COD_SICO"), String).Trim))
        Next
        dtEntidad.Dispose()
        dtEntidad = Nothing
    End Sub

    Private Sub loadTipoMerc()
        Dim dtTipoMerc As New DataTable
        Me.cboTipoMercaderia.Items.Clear()
        dtTipoMerc = objEntidad.gGetTipoMerc()
        Me.cboTipoMercaderia.Items.Add(New ListItem("--------------------Todos-------------------", "0"))
        For Each dr As DataRow In dtTipoMerc.Rows
            Me.cboTipoMercaderia.Items.Add(New ListItem(dr("DE_TIPO_MERC").ToString(), CType(dr("CO_TIPO_MERC"), String).Trim))
        Next
        dtTipoMerc.Dispose()
        dtTipoMerc = Nothing
    End Sub

    Private Sub BindDatagrid()
        dt = New DataTable
        Try
            dt = objDocumento.gGetWarrantLiberaciones(Me.txtNroWarrant.Text.Replace("'", ""), Me.cboEndosatario.SelectedValue, Me.cboTipoMercaderia.SelectedValue, Me.txtDescripci�n.Text.Replace("'", ""), Session.Item("IdSico"), "N", "0")
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("NroDoc", dgi.Cells(0).Text())
            Session.Add("IdTipoDocumento", dgi.Cells(1).Text())
            Session.Add("TipoComprobante", dgi.Cells(13).Text())
            Session.Add("NroComprobante", dgi.Cells(3).Text())
            Session.Add("CodUnidad", dgi.Cells(14).Text())
            Session.Add("CodEntiFina", dgi.Cells(12).Text())
            Session.Add("Virtual", dgi.Cells(15).Text())
            Session.Add("Embarque", dgi.Cells(8).Text())
            Session.Add("WarCustodia", dgi.Cells(9).Text())
            Session.Add("IdSicoDepositante", Session.Item("IdSico"))
            Response.Redirect("LiberacionDetalle.aspx?Merc=" & dgi.Cells(5).Text() & "&Banco=" & dgi.Cells(4).Text() & "&Fecha=" & dgi.Cells(2).Text() & "&Pagina=N", False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        BindDatagrid()
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Cells(7).Text = String.Format("{0:##,##0.00}", e.Item.DataItem("im_saldo"))
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            If e.Item.DataItem("virtual") = "N" Then
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
            Else
                e.Item.BackColor = System.Drawing.Color.LightGoldenrodYellow
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='LightGoldenrodYellow'")
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
