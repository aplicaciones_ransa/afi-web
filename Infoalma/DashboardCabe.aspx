<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DashboardCabe.aspx.vb" Inherits="DashboardCabe" %>

<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarSolicitudWarrants</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {
            //if(parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value))
            //  {
            //   alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
            //   document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
            //   return;
            //  }				  				  
            document.getElementById(Monto).value = formatearDecimal(parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value), 3);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }

        function formatearDecimal(numero, cantDecimales) {
            if (cantDecimales > 0) {
                var i = 0;
                var aux = 1;
                for (i = 0; i < cantDecimales; i++) {
                    aux = aux * 10;
                }
                numero = numero * aux;
                numero = Math.round(numero);
                numero = numero / aux;
            }
            return numero;
        }

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 23) { //para relógio de 12 horas altere o valor aqui
                alert("Horas inválidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("Minutos inválidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inválidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
    <script language="javascript">		
            function Todos(ckall, citem) {
                var actVar = ckall.checked;
                for (i = 0; i < Form1.length; i++) {
                    if (Form1.elements[i].type == "checkbox") {
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
    </script>
    
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
    
 </head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label>

                                                                                </td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="6">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                            <table id="Table1" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr >
                                                    <td class="text" colspan="3">
                                                   
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td >
                                                         <asp:Panel ID="PANEL1" runat="server">
                                                           <table border="0" cellspacing="7" width="100%" style="border-style: solid solid solid solid; border-width: 1px; border-color: #c5c5c5">
                                                            <tr>
                                                                <td align="center" rowspan="2" width="10%">
                                                                    <asp:Button ID="Button1" runat="server" BackColor="#94B86E" BorderColor="#94B86E" BorderStyle="Solid" CausesValidation="False" class="btn" Font-Bold="True" Font-Size="Small" ForeColor="White" Height="40px" Text="$" Width="40px" />
                                                                </td>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="Text4" Text="SALDO SOLES"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label2" runat="server" CssClass="Text5" Text="0.00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                             <table border="0"  width="100%" style="border-style: none solid solid solid; border-width: 1px; border-color: #c5c5c5" bgcolor="White">
                                                            <tr>
                                                                <td >
                                    <asp:Button class="btn" ID="Button2" runat="server"  Text="VER DETALLE" Width="100%" BorderStyle="None" BackColor="White" Font-Bold="True" ForeColor="#666666" style="TEXT-ALIGN: left" />
                                                                </td>                                                           
                                                            </tr>
                                                        </table>
                                                             </asp:Panel>

                                                    </td>
                                                    <td >
                                                        <asp:Panel ID="PANEL2" runat="server">
                                                        <table border="0" cellspacing="7" width="100%" style="border-style: solid solid solid solid; border-width: 1px; border-color: #c5c5c5">
                                                            <tr>
                                                                <td align="center" rowspan="2" width="10%">
                                                                    <asp:Button ID="btnSol2" runat="server" BackColor="#E25856" BorderColor="#E25856" BorderStyle="Solid" CausesValidation="False" class="btn" Font-Bold="True" Font-Size="Small" ForeColor="White" Height="40px" Text="$" Width="40px" />
                                                                </td>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="lblSubTituSoles0" runat="server" CssClass="Text4" Text="SALDO DOLARES"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="lblCantSoles0" runat="server" CssClass="Text5" Text="0.00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                             <table border="0"  width="100%" style="border-style: none solid solid solid; border-width: 1px; border-color: #c5c5c5" bgcolor="White">
                                                            <tr>
                                                                <td >
                                    <asp:Button class="btn" ID="btnSaldoSoles0" runat="server"  Text="VER DETALLE" Width="100%" BorderStyle="None" BackColor="White" Font-Bold="True" ForeColor="#666666" style="TEXT-ALIGN: left" />
                                                                </td>                                                           
                                                            </tr>
                                                        </table>
                                                            </asp:Panel>
                                                        </td>
                                                    <td >
                                                             <asp:Panel ID="PANEL3" runat="server">
                                                    

                                                      <table border="0" cellspacing="7" width="100%" style="border-style: solid solid solid solid; border-width: 1px; border-color: #c5c5c5">
                                                            <tr>
                                                                <td align="center" rowspan="2" width="10%">
                                                                    <asp:Button ID="Button3" runat="server" BackColor="#FFB848" BorderColor="#FFB848" BorderStyle="Solid" CausesValidation="False" class="btn" Font-Bold="True" Font-Size="Small" ForeColor="White" Height="40px" Text="$" Width="40px" />
                                                                </td>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label3" runat="server" CssClass="Text4" Text="SALDO NO NEGOCIADOS"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label4" runat="server" CssClass="Text5" Text="0.00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                             <table border="0"  width="100%" style="border-style: none solid solid solid; border-width: 1px; border-color: #c5c5c5" bgcolor="White">
                                                            <tr>
                                                                <td >
                                    <asp:Button class="btn" ID="Button4" runat="server"  Text="VER DETALLE" Width="100%" BorderStyle="None" BackColor="White" Font-Bold="True" ForeColor="#666666" style="TEXT-ALIGN: left" />
                                                                </td>                                                           
                                                            </tr>
                                                        </table>
                                                         </asp:Panel>
                                                      </td>
                                                </tr>
                                                <tr >
                                                    <td class="text">
                                                        &nbsp;</td>
                                                    <td class="text">
                                                        &nbsp;</td>
                                                    <td class="text">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td colspan="3">
                                                        <table class="auto-style1">
                                                            <tr>
                                                                <td>
                                                                       <asp:Panel ID="PANEL4" runat="server">
                                                           <table border="0" cellspacing="7" width="100%" style="border-style: solid solid solid solid; border-width: 1px; border-color: #c5c5c5">
                                                            <tr>
                                                                <td align="center" rowspan="2" width="10%">
                                                                    <asp:Button ID="Button5" runat="server" BackColor="#E25856" BorderColor="#E25856" BorderStyle="Solid" CausesValidation="False" class="btn" Font-Bold="True" Font-Size="Small" ForeColor="White" Height="40px" Text="$" Width="40px" />
                                                                </td>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label5" runat="server" CssClass="Text4" Text="DEUDA SOLES"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label6" runat="server" CssClass="Text5" Text="0.00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                                    </asp:Panel>

                                                                </td>
                                                                <td>
                                                                      <asp:Panel ID="PANEL5" runat="server">
                                                           <table border="0" cellspacing="7" width="100%" style="border-style: solid solid solid solid; border-width: 1px; border-color: #c5c5c5">
                                                            <tr>
                                                                <td align="center" rowspan="2" width="10%">
                                                                    <asp:Button ID="Button6" runat="server" BackColor="#E25856" BorderColor="#E25856" BorderStyle="Solid" CausesValidation="False" class="btn" Font-Bold="True" Font-Size="Small" ForeColor="White" Height="40px" Text="$" Width="40px" />
                                                                </td>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label7" runat="server" CssClass="Text4" Text="DEUDA DOLARES"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" width="90%">
                                                                    <asp:Label ID="Label8" runat="server" CssClass="Text5" Text="0.00"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                                            </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left">
                                <asp:Label ID="lblSubTitulo2" runat="server" CssClass="Text5">Datos de Saldos</asp:Label>

                                                                                </td>
                        </tr>
                        <tr>
                            <td height="200" valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AutoGenerateColumns="False"
                                    BorderWidth="1px" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>    
                                        <asp:BoundColumn  DataField="RAZ_SOCIAL" HeaderText="RAZON SOCIAL">
                                             <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="NU_MON_SOL" HeaderText="SOLES" DataFormatString="{0:N2}">
                                                   <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="NU_MON_DOL" HeaderText="DOLARES" DataFormatString="{0:N2}">
                                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:BoundColumn> 
                                                             
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
