<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarConsultaWarrants.aspx.vb" Inherits="WarConsultaWarrants"%>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consulta de Warrants</title>
		<meta name="vs_snapToGrid" content="False">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript">	
			function AbrirWarr(sTI_TITU,sNU_TITU)
			{
			var ventana = 'Popup/AlmConsultaWarrantMovimiento.aspx?sTI_TITU=' + sTI_TITU + '&sNU_TITU=' + sNU_TITU
			window.showModalDialog(ventana,window,"dialogHeight:250px; dialogWidth:450px; edge: Raised; center: Yes; help: No; resizable=No; status: No");
			//window.open("CueFacturasOrdenTrabajo.aspx","Orden","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=700,height=280");
			}
				
		    function fn_Impresion(sNO_ENTI, sNU_WARR, sTI_WARR, sES_WARR)
		   {
		    var ventana = 'Reportes/RepConsultaWarrant.aspx?sNO_ENTI=' + sNO_ENTI + '&sNU_WARR=' + sNU_WARR + '&sTI_WARR=' + sTI_WARR + '&sES_WARR=' + sES_WARR;
		    window.open(ventana,"Retiro","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400")
		   }	
		</script>
	</HEAD>
	<body leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid" id="Table1"
							border="0" cellSpacing="0" cellPadding="0" width="100%" height="400">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table3" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="3"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%" align="center">
												<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="690">
													<TR>
														<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r1_c2.gif"></TD>
														<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD background="Images/table_r2_c1.gif" width="6"></TD>
														<TD><!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
															<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0" width="100%">
																<TR>
																	<TD class="Text" width="10%">Estado War</TD>
																	<TD width="1%">:</TD>
																	<TD width="19%"><asp:dropdownlist id="cboEstado" runat="server" CssClass="Text">
																			<asp:ListItem Value="1">Vigentes</asp:ListItem>
																			<asp:ListItem Value="2">Cancelados</asp:ListItem>
																		</asp:dropdownlist></TD>
																	<TD class="Text" width="9%">Modalidad</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="35%"><asp:dropdownlist id="cboTipoWar" runat="server" CssClass="Text"></asp:dropdownlist></TD>
																	<TD class="Text" width="9%">Nro War</TD>
																	<TD class="Text" width="1%">:</TD>
																	<TD class="Text" width="15%"><asp:textbox id="txtNroWar" runat="server" CssClass="Text" Width="80px"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD class="Text"><asp:label id="lblTipoEntidad" runat="server"></asp:label></TD>
																	<TD width="1%">:</TD>
																	<TD colSpan="7"><asp:dropdownlist id="cboFinanciador" runat="server" CssClass="Text"></asp:dropdownlist></TD>
																</TR>
															</TABLE> <!--</div>--></TD>
														<TD background="Images/table_r2_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r3_c2.gif"></TD>
														<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
													</TR>
												</TABLE>
												<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" border="0" cellSpacing="4" cellPadding="0">
													<TR>
														<TD width="80"><asp:button id="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:button></TD>
														<TD width="80"><asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:button></TD>
														<TD width="80"><INPUT id=btnImprimir class=btn onclick="fn_Impresion('<%=cboFinanciador.SelectedItem.Text%>','<%=txtNroWar.Text%>','<%=cboTipoWar.SelectedItem.Text%>','<%=cboEstado.SelectedItem.Text%>')"value=Imprimir type=button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD height="300" vAlign="top">
												<P><asp:datagrid id="dgVigentes" runat="server" CssClass="gv" Width="100%" OnPageIndexChanged="Change_Page"
														PageSize="30" AllowPaging="True" BorderColor="Gainsboro" AutoGenerateColumns="False">
														<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
														<ItemStyle CssClass="gvRow"></ItemStyle>
														<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="TI_TITU" HeaderText="TipoWarrant">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro.Warrant">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="FE_EMIS" HeaderText="Fec.Emis.">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="FE_VENC_VIGE" HeaderText="Fec.Vcmto">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="FE_MAXI_VENC" HeaderText="Fec.Vcmto.L&#237;mite.">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="FE_CANC" HeaderText="Fec.Cancel">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc Mercader&#237;a">
																<ItemStyle HorizontalAlign="Left"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="DE_ALMA" HeaderText="PLANTA">
																<ItemStyle HorizontalAlign="Left"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="MONEDA" HeaderText="Mon.">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="VAL_WARR" HeaderText="Valor Original Warrant" DataFormatString="{0:N2}">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="SAL_WARR" HeaderText="Saldo Valor Warrant" DataFormatString="{0:N2}">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unidad">
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="VAL_CANT" HeaderText="Cantidad Original" DataFormatString="{0:N2}">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="SAL_CANT" HeaderText="Saldo Cantidad" DataFormatString="{0:N2}">
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
															</asp:BoundColumn>
															
														</Columns>
														<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
													</asp:datagrid></P>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
