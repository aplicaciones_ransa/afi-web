<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarMovimiento.aspx.vb" Inherits="WarMovimiento" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarMovimiento</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIni").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFin").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });

            $("#txtFechaVenc").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVen").click(function () {
                $("#txtFechaVenc").datepicker('show');
            });
        });


        function AbrirWarrant(sTI_DOCU_RECE, sNU_DOCU_RECE, sCO_UNID) {
            var ventana = 'Popup/AlmMovimientoDetalle.aspx?sTI_DOCU_RECE=' + sTI_DOCU_RECE + '&sNU_DOCU_RECE=' + sNU_DOCU_RECE + '&sCO_UNID=' + sCO_UNID;
            window.open(ventana, 'Retiro', 'left=240,top=200,width=800,height=600,toolbar=0,resizable=1');
        }
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function fn_Impresion(sNO_MOVI, sTI_REFE, sNU_REFE, sTI_REPO, sTI_MODA, sFE_INIC, sFE_FINA, sNO_CERR) {
            var ventana = 'Popup/AlmMovimientoImpresion.aspx?sNO_MOVI=' + sNO_MOVI + '&sTI_REFE=' + sTI_REFE +
                '&sNU_REFE=' + sNU_REFE + '&sTI_REPO=' + sTI_REPO + '&sTI_MODA=' + sTI_MODA + '&sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sNO_CERR=' + sNO_CERR;
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
            //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table id="Table1" style="border-left: #808080 1px solid; border-right: #808080 1px solid"
                        height="400" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="top">
                                <table id="Table3" cellspacing="6" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" cellspacing="0" cellpadding="0" width="690" align="center" border="0">
                                                <tr>
                                                    <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                                    <td background="Images/table_r1_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r2_c1.gif"></td>
                                                    <td>
                                                        <table id="Table6" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Text" width="15%">Tipo Movimiento</td>
                                                                <td width="1%">:</td>
                                                                <td width="32%">
                                                                    <asp:DropDownList ID="cboMovimiento" runat="server" CssClass="Text" AutoPostBack="True">
                                                                        <asp:ListItem Value="I">Ingreso de Mercader&#237;a</asp:ListItem>
                                                                        <asp:ListItem Value="R">Retiro de Mercader&#237;a</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text" width="15%">Referencia</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td align="left" width="17%">
                                                                    <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td align="left" width="25%">
                                                                    <asp:TextBox ID="txtNroReferencia" runat="server" CssClass="Text" Width="80px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Tipo Reporte</td>
                                                                <td class="Text">:</td>
                                                                <td class="Text">
                                                                    <asp:DropDownList ID="cboReporte" runat="server" CssClass="Text">
                                                                        <asp:ListItem Value="1">Resumido</asp:ListItem>
                                                                        <asp:ListItem Value="2">Detallado</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="Text">Modalidad</td>
                                                                <td class="Text" align="left">:</td>
                                                                <td class="Text" align="left" colspan="2">
                                                                    <asp:DropDownList ID="cboModalidad" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Estado</td>
                                                                <td class="Text" align="left">:</td>
                                                                <td class="Text" align="left">
                                                                    <asp:DropDownList ID="cboEstado" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td class="Text" align="left">
                                                                    <asp:Label ID="lblFecha" runat="server" EnableViewState="False"></asp:Label></td>
                                                                <td class="Text" align="center" colspan="3">
                                                                    <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td class="Text" align="right">Del:</td>
                                                                            <td style="width: 105px">
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaInicial" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaInicial" runat="server">
                                                                                <input class="text" id="btnFechaIni" type="button"
                                                                                    value="..." name="btnFechaIni"></td>
                                                                            <td class="Text" align="right">Al:</td>
                                                                            <td style="width: 105px">
                                                                                <input class="text" onkeypress="validarcharfecha()" id="txtFechaFinal" onkeyup="this.value=formateafecha(this.value);"
                                                                                    maxlength="10" size="6" name="txtFechaFinal" runat="server">
                                                                                <input class="text" id="btnFechaFin" type="button"
                                                                                    value="..." name="btnFechaFin"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text">Almac�n</td>
                                                                <td class="Text" align="left">:</td>
                                                                <td class="Text" align="left" colspan="2">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboAlmacen" runat="server" CssClass="Text" Width="300px"></asp:DropDownList></td>
                                                                <td class="Text" align="center"></td>
                                                                <td class="Text" align="center">Vcto.Limite &lt;=</td>
                                                                <td class="Text" align="left">
                                                                    <input class="text" onkeypress="validarcharfecha()" id="txtFechaVenc" onkeyup="this.value=formateafecha(this.value);"
                                                                        maxlength="10" size="6" name="txtFechaVenc" runat="server" style="z-index: 0"><input class="text" id="btnFechaVen" type="button"
                                                                            value="..." name="btnFechaIni" style="z-index: 0"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="6" background="Images/table_r2_c3.gif"></td>
                                                </tr>
                                                <tr>
                                                    <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                                    <td background="Images/table_r3_c2.gif" height="6"></td>
                                                    <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" cellspacing="4" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                    <td width="80">
                                                        <input class="btn" id="btnImprimir" onclick="fn_Impresion('<%=cboMovimiento.SelectedItem.Text%>','<%=cboReferencia.SelectedItem.Text%>','<%=txtNroReferencia.Text%>','<%=cboReporte.SelectedItem.Text%>','<%=cboModalidad.SelectedItem.Text%>','<%=txtFechaInicial.Value%>','<%=txtFechaFinal.Value%>','<%=cboEstado.SelectedItem.Text%>')" type="button" value="Imprimir" name="btnImprimir"></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:DataGrid ID="dgIngresos" runat="server" CssClass="gv" Width="1200px" BorderColor="Gainsboro"
                                                HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True" OnPageIndexChanged="Change_Page">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_REGI_INGR" HeaderText="Fec.Ingreso" DataFormatString="{0:d}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_ACTU" HeaderText="Fec.Cierre" DataFormatString="{0:d}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_SECU" HeaderText="Sec">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC_GENE" HeaderText="Sub.TipoMercader&#237;a"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc.Mercader&#237;a"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unid">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_RECI" HeaderText="Cnt.Unidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_RECI" HeaderText="Cnt.Bulto" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_TOTA_NACI" HeaderText="Importe" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="CodProd">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_PROD_CLIE" HeaderText="Producto"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Guia.Ingr">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_TARI" HeaderText="Tarifa"></asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" DataField="CO_UNID" HeaderText="CO_UNID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_TITU" HeaderText="Nro.Warr"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid>
                                            <asp:DataGrid ID="dgRetiros" runat="server" CssClass="gv" Width="1200px" BorderColor="Gainsboro"
                                                HorizontalAlign="Center" AutoGenerateColumns="False" PageSize="50" AllowPaging="True" OnPageIndexChanged="Change_PageRetiros">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Center" CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="NU_DOCU_RETI" HeaderText="DOR">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FE_DOCU_RETI" HeaderText="Fec.Retiro" DataFormatString="{0:d}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MODA_MOVI" HeaderText="Modalidad">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="DCR">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_SECU" HeaderText="Item">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Prod">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc.Mercaderia"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unid">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_UNID_RETI" HeaderText="Cnt.Unidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_PESO_RETI" HeaderText="Cnt.Bulto" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IM_TOTA_NACI" HeaderText="Importe" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Guia.Salida">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Observaciones"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_TITU" HeaderText="Nro.Warr"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                                    Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
