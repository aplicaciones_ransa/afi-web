Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports System.Text
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class WarSolicitudWarrantRegistro
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    Dim strAlmacen As String
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Protected WithEvents cboTipoTitulo As System.Web.UI.WebControls.DropDownList
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strCoEmpresa = ConfigurationManager.AppSettings.Item("CoEmpresa")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents FilePDF As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacenes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnStock As System.Web.UI.WebControls.Button
    'Protected WithEvents trAlmacen As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnGrabar As RoderoLib.BotonEnviar
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHoraIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkReemplazo As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put User code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "38") Then
            If Not IsPostBack Then
                If Session("NroSecuSol") <> "1" Then
                    Me.btnAgregar.Visible = False
                    Me.btnAgregarNuevo.Visible = False
                    Me.dgdResultado.Columns("12").Visible = False
                Else
                    Me.btnAgregar.Visible = True
                    Me.btnAgregarNuevo.Visible = True
                    Me.dgdResultado.Columns("12").Visible = True
                End If
                LImpiarCampos()
                Me.ltrCodDoc.Text = Session.Item("Cod_Doc").trim
                Me.ltrNroDocu.Text = Session.Item("NroDoc").trim
                Me.ltrTipDoc.Text = Session.Item("TipDoc").trim
                Me.ltrNroItem.Text = Session.Item("NroItem").trim
                Me.lblOpcion.Text = "Registro de Datos generales de "
                Me.lblTipoSol.Text = "Solicitud: " & Me.ltrNroDocu.Text
                Me.lblOpcionItem.Text = "Registro de Producto "
                Me.lblTipoItem.Text = "Item: " & Me.ltrNroItem.Text
                loadProducto()
                loadUnidadMedida()
                validarCantidadInicial()
                validarAduanero()
                If Me.ltrNroItem.Text <> "0" Then
                    BindDatagrid()
                End If
                LisTarDetalle()
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ValidarBarr()

        '=== Validar Cabecera ===
        If Me.ltrCodDoc.Text <> "" Then
            'Me.ImgBarr.ImageUrl = "Images/Barr2.png"
            Me.btnSol1.BackColor = System.Drawing.Color.FromName("#94B86E")
            Me.btnSol1.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
            Me.ImgCheck1.Visible = True
            Me.lblDeSol1.CssClass = "Titulo3"
        Else
            'Me.ImgBarr.ImageUrl = "Images/Barr1.png"
            Me.btnSol1.BackColor = System.Drawing.Color.FromName("#4D7496")
            Me.btnSol1.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
            Me.ImgCheck1.Visible = False
            Me.lblDeSol1.CssClass = "Titulo1"
        End If
        '=== Validar Detalle ===
        If dgdResultado.Items.Count <> "0" Then
            Me.btnSol2.BackColor = System.Drawing.Color.FromName("#94B86E")
            Me.btnSol2.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
            Me.ImgCheck2.Visible = True
            Me.lblDeSol2.CssClass = "Titulo3"
        Else
            Me.btnSol2.BackColor = System.Drawing.Color.FromName("#4D7496")
            Me.btnSol2.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
            Me.ImgCheck2.Visible = False
            Me.lblDeSol2.CssClass = "Titulo1"
        End If
    End Sub


    Private Sub validarCantidadInicial()

        If Session.Item("TipoWarrant").trim = "019" Then
            Me.cboCantInic.Enabled = True
        Else
            Me.cboCantInic.Enabled = False
        End If

    End Sub

    Private Sub validarAduanero()
        If Session.Item("TipoWarrant").trim = "016" Then
            trAlmacen2.Visible = False
            trAlmacen3.Visible = False
            trAlmacen4.Visible = True
        Else
            trAlmacen2.Visible = True
            trAlmacen3.Visible = True
            trAlmacen4.Visible = False
        End If
    End Sub

    'Private Sub loadTipoWarrant()
    '    Dim dtEntidad As New DataTable
    '    Try
    '        Me.cboProducto.Items.Clear()
    '        dtEntidad = objDocumento.gGetBusquedaDocumentoTipoWarrant(strCoEmpresa)
    '        Me.cboProducto.Items.Add(New ListItem("--- SELECCIONE ---", 0))
    '        For Each dr As DataRow In dtEntidad.Rows
    '            Me.cboProducto.Items.Add(New ListItem(dr("DE_MODA_MOVI").ToString(), dr("CO_MODA_MOVI")))
    '        Next
    '    Catch ex As Exception
    '        Me.lblError.Text = ex.ToString
    '    Finally
    '        dtEntidad.Dispose()
    '        dtEntidad = Nothing
    '    End Try
    'End Sub

    Private Sub loadProducto()
        Dim dtProd As New DataTable
        Try
            Me.cboProducto.Items.Clear()
            dtProd = objDocumento.gGetBusquedaDocumentoProducto(strCoEmpresa)
            Me.cboProducto.Items.Add(New ListItem("--- SELECCIONE ---", 0))
            For Each dr As DataRow In dtProd.Rows
                Me.cboProducto.Items.Add(New ListItem(dr("DE_TIPO_MERC").ToString(), dr("CO_TIPO_MERC")))
            Next
        Catch ex As Exception
            Me.lblError.Text = ex.ToString
        Finally
            dtProd.Dispose()
            dtProd = Nothing
        End Try
    End Sub
    Private Sub loadUnidadMedida()
        Dim dtUnidMed As New DataTable
        Try
            Me.cboUnidadMedida.Items.Clear()
            dtUnidMed = objDocumento.gGetBusquedaDocumentoUnidadMedida(strCoEmpresa)
            Me.cboUnidadMedida.Items.Add(New ListItem("--- SELECCIONE ---", 0))
            For Each dr As DataRow In dtUnidMed.Rows
                Me.cboUnidadMedida.Items.Add(New ListItem(dr("DE_TIPO_BULT").ToString(), dr("CO_TIPO_BULT")))
            Next

        Catch ex As Exception
            Me.lblError.Text = ex.ToString
        Finally
            dtUnidMed.Dispose()
            dtUnidMed = Nothing
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try

            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim dt As DataTable
            dt = Nothing
            dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrantDetalle(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, dgi.Cells(1).Text())
            If dt.Rows.Count <> 0 Then
                cboProducto.SelectedValue = dt.Rows(0).Item("FAM_PROD").ToString
                cboUnidadMedida.SelectedValue = dt.Rows(0).Item("UNI_MEDI").ToString
                txtCantPeso.Text = dt.Rows(0).Item("CAN_PESO").ToString
                cboCantInic.SelectedValue = dt.Rows(0).Item("TIP_PROD").ToString
                txtDesc.Text = dt.Rows(0).Item("DSC_PROD").ToString
                txtValorUnit.Text = dt.Rows(0).Item("VAL_UNIT").ToString
                Me.ltrNroItem.Text = dt.Rows(0).Item("NRO_SECU").ToString
                Me.lblTipoSol.Text = "Solicitud: " & Me.ltrNroDocu.Text
                Me.lblTipoItem.Text = "Item: " & Me.ltrNroItem.Text
                Me.txtSerie.Text = dt.Rows(0).Item("NU_SERIE").ToString
                Me.txtDesc2.Text = dt.Rows(0).Item("DSC_PROD").ToString
            End If
            LisTarDetalle()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub
    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim dtDesap As DataTable
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            dtDesap = objDocumento.SetDeleteDocumentosSolicitadorWarrantDeta(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, dgi.Cells(1).Text(), Session.Item("IdUsuario"))
            If dtDesap.Rows.Count <> 0 Then
                lblError.Text = dtDesap.Rows(0).Item("MENSAJE")
            End If
            BindDatagrid()
            LisTarDetalle()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub BindDatagrid()
        Try
            Dim dt As DataTable
            dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrantDetalle(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, Me.ltrNroItem.Text)
            If dt.Rows.Count <> 0 Then

                cboProducto.SelectedValue = dt.Rows(0).Item("FAM_PROD").ToString
                cboUnidadMedida.SelectedValue = dt.Rows(0).Item("UNI_MEDI").ToString
                txtCantPeso.Text = dt.Rows(0).Item("CAN_PESO").ToString
                cboCantInic.SelectedValue = dt.Rows(0).Item("TIP_PROD").ToString
                txtDesc.Text = dt.Rows(0).Item("DSC_PROD").ToString
                txtValorUnit.Text = dt.Rows(0).Item("VAL_UNIT").ToString
                Me.lblTipoSol.Text = "Solicitud: " & Me.ltrNroDocu.Text
                Me.lblTipoItem.Text = "Item: " & dt.Rows(0).Item("NRO_SECU").ToString
                Me.txtSerie.Text = dt.Rows(0).Item("NU_SERIE").ToString
                Me.txtDesc2.Text = dt.Rows(0).Item("DSC_PROD").ToString
            End If
            ValidarBarr()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub LisTarDetalle()
        Try
            Dim dt As DataTable
            dt = objDocumento.gGetBusquedaDocumentosSolicitadorWarrantDetalle(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, "0")
            If dt.Rows.Count <> 0 Then
                Me.dgdResultado.DataSource = dt
                Me.dgdResultado.DataBind()
            Else
                Me.dgdResultado.DataSource = Nothing
                Me.dgdResultado.DataBind()
            End If
            ValidarBarr()
        Catch ex As Exception
            Me.lblError.Text = ""
        End Try
    End Sub
    Private Sub LImpiarCampos()
        Me.ltrNroItem.Text = "0"
        cboProducto.SelectedValue = "0"
        cboUnidadMedida.SelectedValue = "0"
        cboCantInic.Text = "P"
        txtDesc.Text = ""
        txtCantPeso.Text = ""
        txtValorUnit.Text = ""
    End Sub

    Protected Sub btnCancelar_Click1(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("WarSolicitudWarrantRegistro.aspx", False)
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Try
            ValidarBarr()
            Dim strDescripcion As String = ""
            If Session.Item("TipoWarrant").trim = "016" Then
                If cboCantInic.SelectedValue = "0" Then
                    lblError.Text = "Seleccione Cantidad Inicial"
                    Exit Sub
                ElseIf cboProducto.SelectedValue = "0" Then
                    lblError.Text = "Seleccione un Producto"
                    Exit Sub
                ElseIf txtDesc2.Text = "" Then
                    lblError.Text = "Ingrese Descripción"
                    Exit Sub
                ElseIf txtSerie.Text = "" Then
                    lblError.Text = "Ingrese la serie"
                    Exit Sub
                End If
                strDescripcion = txtDesc2.Text
            Else
                If cboProducto.SelectedValue = "0" Then
                    lblError.Text = "Seleccione un Producto"
                    Exit Sub
                ElseIf cboUnidadMedida.SelectedValue = "0" Then
                    lblError.Text = "Seleccione Unidad Medida"
                    Exit Sub
                ElseIf txtCantPeso.Text = "" Then
                    lblError.Text = "Ingrese Cantidad/ Peso"
                    Exit Sub
                ElseIf cboCantInic.SelectedValue = "0" Then
                    lblError.Text = "Seleccione Cantidad Inicial"
                    Exit Sub
                ElseIf txtDesc.Text = "" Then
                    lblError.Text = "Ingrese Descripción"
                    Exit Sub
                ElseIf txtValorUnit.Text = "" Then
                    lblError.Text = "Ingrese Valor Unitario"
                    Exit Sub
                End If
                strDescripcion = txtDesc.Text
            End If

            'objAccesoWeb = New clsCNAccesosWeb
            'objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
            '                            Session.Item("NombreEntidad"), "M", "38", "GRABA SOLICITUD WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim strResultado As String
            Dim dtGrabar As DataTable

            dtGrabar = objDocumento.SetGrabarSolicitudWarrantDetalle(Me.ltrNroDocu.Text, Me.ltrCodDoc.Text, Me.ltrTipDoc.Text, Me.ltrNroItem.Text,
                                                                      cboProducto.SelectedValue, cboUnidadMedida.SelectedValue, IIf(txtCantPeso.Text = "", 0, txtCantPeso.Text),
                                                                      cboCantInic.Text, strDescripcion, IIf(txtValorUnit.Text = "", 0, txtValorUnit.Text), Session.Item("IdSico"),
                                                                      Session.Item("IdUsuario"), IIf(txtSerie.Text = "", 0, txtSerie.Text))

            If dtGrabar.Rows.Count <> 0 Then
                If dtGrabar.Rows(0).Item("RESULTADO").ToString = "I" Then
                ElseIf dtGrabar.Rows(0).Item("RESULTADO").ToString = "X" Then
                End If
                Me.lblError.Text = dtGrabar.Rows(0).Item("MENSAJE").ToString
                Me.lblTipoSol.Text = "Solicitud: " & Me.ltrNroDocu.Text
                Me.lblTipoItem.Text = "Item: 0"
            End If
            LImpiarCampos()
            Session.Add("NroItem", "0")
            LisTarDetalle()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSol1_Click(sender As Object, e As EventArgs) Handles btnSol1.Click
        Response.Redirect("WarSolicitudWarrantRegistro.aspx", False)
    End Sub
    Protected Sub btnSol2_Click(sender As Object, e As EventArgs) Handles btnSol2.Click
        LisTarDetalle()
    End Sub
    Protected Sub btnAgregarNuevo_Click(sender As Object, e As EventArgs) Handles btnAgregarNuevo.Click
        LImpiarCampos()
        Session.Add("NroItem", "0")
        Me.lblTipoItem.Text = "Item: 0"
        LisTarDetalle()
    End Sub
End Class
