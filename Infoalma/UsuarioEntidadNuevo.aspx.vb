Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Infodepsa
Imports System.Data

Public Class UsuarioEntidadNuevo
    Inherits System.Web.UI.Page
    Private objUsuario As Usuario = New Usuario
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objAccesoWeb As clsCNAccesosWeb
    'Protected WithEvents chkSucursales As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkVistoBueno As System.Web.UI.WebControls.CheckBox
    Private strPath = ConfigurationManager.AppSettings.Item("RutaFirmas")
    'Private strPathFirmas = ConfigurationManager.AppSettings.Item("Firmas")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents chkLiberador As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents imgFirma As System.Web.UI.WebControls.Image
    'Protected WithEvents lblNombreImg As System.Web.UI.WebControls.Label
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents cboEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents chkNotificar As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents cboTipoEntidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents chkUsrActivo As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents txtTelefono As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFax As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkComun As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents chkSupervisor As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents chkAprobador As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents chkAdministrador As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents FileFirma As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents txtCodUsuario As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtCodEntidad As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkAlerta As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents chkAdjunto As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents cboGrupo As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_USUARIO") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Try
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_USUARIO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Me.txtTelefono.EnableViewState = True
                    Me.txtFax.EnableViewState = True
                    Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")

                    loadTipoEntidad()
                    loadEntidad()
                    ValidarControles()
                    llenarGrupos()
                    If Request.QueryString("Nombre") <> "" Then
                        Me.lblTitulo.Text = "Datos del usuario " & Request.QueryString("Nombre")
                    End If

                    If Request.QueryString("Cod") <> "" Then
                        Me.txtCodUsuario.Value = Request.QueryString("Cod")
                    End If

                    If Request.QueryString("Ent") = "X" Then
                        Me.btnGrabar.Text = "Grabar"
                    Else
                        Me.txtCodEntidad.Value = Session.Item("CodEntidadSico")
                        loadDataUsuario()
                        Me.btnGrabar.Text = "Actualizar"
                    End If

                    If Me.cboTipoEntidad.SelectedValue = "01" Then
                        Me.chkAdministrador.Disabled = False
                    Else
                        Me.chkAdministrador.Checked = False
                        Me.chkAdministrador.Disabled = True
                    End If

                    If Me.cboTipoEntidad.SelectedValue = "02" And Session.Item("IdTipoUsuario") = "04" Then
                        Me.chkSucursales.Enabled = True
                    Else
                        Me.chkSucursales.Enabled = False
                    End If
                    If Me.cboTipoEntidad.SelectedValue = "03" Then
                        Me.chkLiberador.Enabled = True
                    Else
                        Me.chkLiberador.Enabled = False
                    End If
                    If Me.cboTipoEntidad.SelectedValue = "01" Then
                        Me.chkVistoBueno.Enabled = True
                    Else
                        Me.chkVistoBueno.Enabled = False
                    End If
                    'If Me.cboTipoEntidad.SelectedValue = "04" Then
                    '    Me.chkLiberador.Enabled = False
                    '    Me.chkVistoBueno.Enabled = False
                    'Else
                    '    Me.chkLiberador.Enabled = True
                    '    Me.chkVistoBueno.Enabled = True
                    'End If
                Catch ex As Exception
                    Me.lblError.Text = ex.Message
                End Try
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ValidarControles()
        If Session.Item("IdTipoUsuario") = "04" Then
            Me.btnGrabar.Enabled = True
            Me.cboTipoEntidad.Enabled = True
            Me.cboEntidad.Enabled = True
            Me.txtTelefono.Disabled = False
            Me.txtFax.Disabled = False
            Me.chkNotificar.Enabled = True
            'Me.chkLiberador.Enabled = True
            'Me.chkVistoBueno.Enabled = True
            Me.chkUsrActivo.Enabled = True
            Me.chkComun.Disabled = False
            Me.chkSupervisor.Disabled = False
            Me.chkAprobador.Disabled = False
            Me.chkAdministrador.Disabled = False
            Me.FileFirma.Disabled = False
            Me.chkAdjunto.Enabled = True
            Me.chkAlerta.Enabled = True
        Else
            Me.btnGrabar.Enabled = False
        End If
    End Sub

    Private Sub loadDataUsuario()
        Dim dtUsuario As DataTable = New DataTable
        Try
            dtUsuario = objUsuario.gGetDataUsuarioxEntidad(Me.txtCodUsuario.Value, Me.txtCodEntidad.Value)
            If dtUsuario.Rows.Count = 0 Then
                Me.lblError.Text = "Error al retornar los datos del usuario seleccionado"
            Else
                If Not IsDBNull(dtUsuario.Rows(0)("TEL_EMPR")) Then
                    Me.txtTelefono.Value = CType(dtUsuario.Rows(0)("TEL_EMPR"), String).Trim
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FAX_EMPR")) Then
                    Me.txtFax.Value = CType(dtUsuario.Rows(0)("FAX_EMPR"), String).Trim
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("COD_TIPENTI")) Then
                    Me.cboTipoEntidad.SelectedValue = CType(dtUsuario.Rows(0)("COD_TIPENTI"), String).Trim
                End If
                llenarGrupos()
                Me.chkUsrActivo.Checked = dtUsuario.Rows(0)("EST_USER")
                loadEntidad()
                If Not IsDBNull(dtUsuario.Rows(0)("COD_SICO")) Then
                    Me.cboEntidad.SelectedValue = CType(dtUsuario.Rows(0)("COD_SICO"), String).Trim
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("COD_GRUP")) Then
                    Me.cboGrupo.SelectedValue = CType(dtUsuario.Rows(0)("COD_GRUP"), String).Trim
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("USR_LIBE")) Then
                    Me.chkLiberador.Checked = CType(dtUsuario.Rows(0)("USR_LIBE"), Boolean)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("USR_VISTO")) Then
                    Me.chkVistoBueno.Checked = CType(dtUsuario.Rows(0)("USR_VISTO"), Boolean)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FIR_NOMB")) Then
                    Me.lblNombreImg.Text = CType(dtUsuario.Rows(0)("FIR_NOMB"), String)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FLG_NOTI")) Then
                    Me.chkNotificar.Checked = CType(dtUsuario.Rows(0)("FLG_NOTI"), Boolean)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FLG_ALER")) Then
                    Me.chkAlerta.Checked = CType(dtUsuario.Rows(0)("FLG_ALER"), Boolean)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FLG_ADJU")) Then
                    Me.chkAdjunto.Checked = CType(dtUsuario.Rows(0)("FLG_ADJU"), Boolean)
                End If
                If Not IsDBNull(dtUsuario.Rows(0)("FLG_SUCU")) Then
                    Me.chkSucursales.Checked = CType(dtUsuario.Rows(0)("FLG_SUCU"), Boolean)
                End If
                If dtUsuario.Rows(0)("FIR_NOMB") <> "" Then
                    'imgFirma.ImageUrl = strPath & dtUsuario.Rows(0)("FIR_NOMB")
                    imgFirma.ImageUrl = "../firmas/" & dtUsuario.Rows(0)("FIR_NOMB")
                Else
                    'imgFirma.ImageUrl = strPath & "aspl.JPG"
                    imgFirma.ImageUrl = "../firmas/" & "aspl.JPG"
                End If
                Select Case dtUsuario.Rows(0)("COD_TIPUSR")
                    Case "01"
                        Me.chkComun.Checked = True
                    Case "02"
                        Me.chkSupervisor.Checked = True
                    Case "03"
                        Me.chkAprobador.Checked = True
                    Case "04"
                        Me.chkAdministrador.Checked = True
                End Select
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        Finally
            dtUsuario.Dispose()
            dtUsuario = Nothing
        End Try
    End Sub

    Private Sub loadTipoEntidad()
        objFunciones.GetTipoEntidad(Me.cboTipoEntidad, 0)
    End Sub

    Private Sub loadEntidad()
        Dim dtEntidad As DataTable = New DataTable
        Try
            Me.cboEntidad.Items.Clear()
            dtEntidad = objEntidad.gGetEntidadesSICO(Me.cboTipoEntidad.SelectedValue)
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboEntidad.Items.Add(New ListItem(dr("Nombre").ToString(), CType(dr("Codigo"), String).Trim))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            Dim strResult As String
            Dim strNombre As String
            Dim strTipo As String
            Dim blnLiberacion As Boolean
            Dim blnVisto As Boolean
            Dim strTipoUsuario As String
            If Me.chkComun.Checked = True Then
                strTipoUsuario = "01"
            ElseIf Me.chkSupervisor.Checked = True Then
                strTipoUsuario = "02"
            ElseIf Me.chkAprobador.Checked = True Then
                strTipoUsuario = "03"
            ElseIf Me.chkAdministrador.Checked = True Then
                strTipoUsuario = "04"
            End If

            If Me.chkAprobador.Checked Then
                blnLiberacion = Me.chkLiberador.Checked
            Else
                blnLiberacion = False
            End If
            blnVisto = Me.chkVistoBueno.Checked

            Dim FlujoBinario As System.IO.Stream
            Dim Contenido As Byte()
            strNombre = System.IO.Path.GetFileName(Me.FileFirma.PostedFile.FileName)
            strTipo = System.IO.Path.GetExtension(Me.FileFirma.PostedFile.FileName)
            If strNombre <> "" Then
                FileFirma.PostedFile.SaveAs(strPath & strNombre)
            End If
            FlujoBinario = Request.Files(0).InputStream
            ReDim Contenido(FlujoBinario.Length)
            FlujoBinario.Read(Contenido, 0, Convert.ToInt32(FlujoBinario.Length))
            Dim dr As DataRow
            Dim strRUC As String
            Dim strDireccion As String
            Dim strNomLargo As String

            dr = objEntidad.GetInformacionFinanciera(Me.cboEntidad.SelectedValue, Me.cboTipoEntidad.SelectedValue)
            If Not IsDBNull(dr("RUC")) Then
                strRUC = dr("RUC")
            Else
                strRUC = ""
            End If
            If Not IsDBNull(dr("DIRECCION")) Then
                strDireccion = dr("DIRECCION")
            Else
                strDireccion = ""
            End If
            If Not IsDBNull(dr("NombreLargo")) Then
                strNomLargo = dr("NombreLargo")
            Else
                strNomLargo = ""
            End If
            If Me.btnGrabar.Text = "Grabar" Then
                If objUsuario.gInsUsuarioEntidad(Me.cboEntidad.SelectedValue,
                                            Me.txtFax.Value, Me.txtTelefono.Value, Contenido, strNombre,
                                            Me.cboEntidad.SelectedItem.Text, Me.cboTipoEntidad.SelectedValue,
                                            Session.Item("IdUsuario"), blnLiberacion, Me.chkNotificar.Checked,
                                            strRUC, strDireccion, strNomLargo, Me.txtCodUsuario.Value, strTipoUsuario,
                                            Me.chkUsrActivo.Checked, Me.chkAlerta.Checked, Me.chkAdjunto.Checked,
                                            Me.cboGrupo.SelectedValue, Session.Item("cod_sist"), Me.chkSucursales.Checked, blnVisto) = "X" Then
                    Me.lblError.Text = "El usuario ya esta registrado para la entidad seleccionada, cambielo!"
                    Exit Sub
                Else
                    Me.txtCodEntidad.Value = Me.cboEntidad.SelectedValue
                    strResult = "Se grab� correctamente"
                End If
            Else
                strResult = objUsuario.gUpdUsuarioEntidad(Me.cboEntidad.SelectedValue, Me.txtFax.Value, Me.txtTelefono.Value,
                                           Contenido, strNombre, Me.cboEntidad.SelectedItem.Text, Me.cboTipoEntidad.SelectedValue,
                                           Me.txtCodUsuario.Value, Session.Item("IdUsuario"), blnLiberacion,
                                           Me.chkNotificar.Checked, strRUC, strDireccion, strNomLargo, Me.txtCodEntidad.Value,
                                           strTipoUsuario, Me.chkUsrActivo.Checked, Me.chkAlerta.Checked, Me.chkAdjunto.Checked,
                                           Me.cboGrupo.SelectedValue, Session.Item("cod_sist"), Me.chkSucursales.Checked, blnVisto)
                Me.txtCodEntidad.Value = Me.cboEntidad.SelectedValue
            End If
            loadDataUsuario()
            Me.lblError.Text = strResult
            Me.btnGrabar.Text = "Actualizar"
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_USUARIO", "GRABAR DETALLE DE ENTIDAD", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            Me.lblError.Text = "Se produjo el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub cboTipoEntidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoEntidad.SelectedIndexChanged
        loadEntidad()
        llenarGrupos()

        If Me.cboTipoEntidad.SelectedValue = "01" Then
            Me.chkAdministrador.Disabled = False
        Else
            Me.chkAdministrador.Checked = False
            Me.chkAdministrador.Disabled = True
        End If
        If Me.cboTipoEntidad.SelectedValue = "02" And Session.Item("IdTipoUsuario") = "04" Then
            Me.chkSucursales.Enabled = True
        Else
            Me.chkSucursales.Enabled = False
        End If
        If Me.cboTipoEntidad.SelectedValue = "03" Then
            Me.chkLiberador.Enabled = True
        Else
            Me.chkLiberador.Enabled = False
        End If
        If Me.cboTipoEntidad.SelectedValue = "01" Then
            Me.chkVistoBueno.Enabled = True
        Else
            Me.chkVistoBueno.Enabled = False
        End If
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("UsuarioNuevo.aspx?Cod=" & Me.txtCodUsuario.Value)
    End Sub

    Private Sub llenarGrupos()
        Dim dtGrupos As DataTable
        dtGrupos = objEntidad.gMostrarGrupos(Session.Item("CoSist"))
        Me.cboGrupo.Items.Clear()
        Me.cboGrupo.Items.Add(New ListItem("--------Seleccione--------", "0"))
        For Each dr As DataRow In dtGrupos.Rows
            If Me.cboTipoEntidad.SelectedValue <> "01" And dr("CO_GRUP") = "ADMIN" Then
                'No pintar
            Else
                Me.cboGrupo.Items.Add(New ListItem(dr("NO_GRUP"), dr("CO_GRUP")))
            End If
        Next
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
