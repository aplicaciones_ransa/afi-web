<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarTrasladoAlmacen.aspx.vb" Inherits="WarTrasladoAlmacen" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Retiro en L�nea</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="Styles/Styles.css">
		<script language="javascript" src="JScripts/calendar.js"></script>
		<script language="javascript" src="JScripts/Validaciones.js"></script>
		<script language="javascript">	
		function ValidaFecha(sFecha)
				{
				var fecha1 = sFecha
				var fecha2 = document.getElementById('txtFechaInicial').value
				var miFecha1 = new Date(fecha1.substr(6,4), fecha1.substr(3,2), fecha1.substr(0,2))
				var miFecha2 = new Date(fecha2.substr(6,4), fecha2.substr(3,2), fecha2.substr(0,2)) 
				var diferencia = (miFecha1.getTime()�- miFecha2.getTime())/(1000*60*60*24)
				//alert (diferencia);

					if (diferencia > 540){
					window.open("Popup/SolicitarInformacion.aspx","Vencimientos","top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
					return false;
					}
					else
					{
					return true;
					}
				}		
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:header id="Header1" runat="server"></uc1:header>
						<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table3" border="0" cellSpacing="6" cellPadding="0" width="100%">
										<TR>
											<TD></TD>
											<TD width="100%"><asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="6"><uc1:menuinfo id="MenuInfo1" runat="server"></uc1:menuinfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table20" border="0" cellSpacing="0" cellPadding="0" width="670" align="center">
													<TR>
														<TD height="6" background="Images/table_r1_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r1_c2.gif"></TD>
														<TD height="6" background="Images/table_r1_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD background="Images/table_r2_c1.gif" width="6"></TD>
														<TD align="center">
															<TABLE id="Table6" border="0" cellSpacing="4" cellPadding="0" width="100%">
																<TR>
																	<TD class="Text">Depositante:</TD>
																	<TD colSpan="4"><asp:dropdownlist id="cboDepositante" runat="server" CssClass="text" AutoPostBack="True" Width="550px"></asp:dropdownlist></TD>
																	<TD class="Text"></TD>
																</TR>
																<TR>
																	<TD style="Z-INDEX: 0" class="Text">Referencia:</TD>
																	<TD><asp:dropdownlist id="cboReferencia" runat="server" CssClass="text" Width="120px">
																			<asp:ListItem Value="1">Nro Warrants</asp:ListItem>
																			<asp:ListItem Value="2">Dsc. Mercader&#237;a </asp:ListItem>
																		</asp:dropdownlist></TD>
																	<TD class="Text"><asp:textbox id="txtNroReferencia" runat="server" CssClass="text" Width="120px"></asp:textbox></TD>
																	<TD style="Z-INDEX: 0" class="Text">Modalidad:</TD>
																	<TD class="Text"><asp:dropdownlist id="cboModalidad" runat="server" CssClass="Text" Width="150px"></asp:dropdownlist></TD>
																	<TD class="Text"></TD>
																</TR>
																<TR>
																	<TD class="Text">Almac�n Origen:</TD>
																	<TD colSpan="2"><asp:dropdownlist id=cboAlmacenOrigen runat="server" CssClass="Text" Width="300px" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.DE_ALMA_DEST") %>' DataValueField="DSC_ALMA" DataTextField="DSC_ALMA" DataSource="<%# GetAlmacen() %>">
																		</asp:dropdownlist></TD>
																	<TD style="Z-INDEX: 0" class="Text">Estado:</TD>
																	<TD class="Text"><asp:dropdownlist id="cboEstado" runat="server" CssClass="text" Width="120px">
																			<asp:ListItem Value="0">Todos</asp:ListItem>
																			<asp:ListItem Value="09">Ubicado</asp:ListItem>
																			<asp:ListItem Value="10">Pendiente aprobaci&#243;n</asp:ListItem>
																			<asp:ListItem Value="11">Pendiente traslado</asp:ListItem>
																		</asp:dropdownlist></TD>
																	<TD class="Text"></TD>
																</TR>
															</TABLE>
														</TD>
														<TD background="Images/table_r2_c3.gif" width="6"></TD>
													</TR>
													<TR>
														<TD height="6" background="Images/table_r3_c1.gif" width="6"></TD>
														<TD height="6" background="Images/table_r3_c2.gif"></TD>
														<TD height="6" background="Images/table_r3_c3.gif" width="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0" width="100%">
													<TR>
														<TD width="25%"></TD>
														<TD width="50%" align="center"><asp:button id="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:button><asp:button style="Z-INDEX: 0" id="btnSolicitar2" runat="server" CssClass="btn" Width="80px"
																Text="Solicitar" Visible="False"></asp:button><asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:button></TD>
														<TD width="25%" align="right"><asp:hyperlink id="lnkAlmacen" runat="server" CssClass="otros-links" Visible="False" NavigateUrl="#">Agregar almacenes</asp:hyperlink></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"><asp:datagrid id="dgResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
													BorderColor="Gainsboro" OnPageIndexChanged="Change_Page" AllowPaging="True" PageSize="30">
													<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
													<ItemStyle CssClass="gvRow"></ItemStyle>
													<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn HeaderText="Sel">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:CheckBox id="chkSeleccion" runat="server" CssClass="Text" Enabled="False"></asp:CheckBox>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="NU_TITU" HeaderText="Nro Warrant">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="NU_SECU" HeaderText="&#205;tem">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc. Mercaderia">
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DE_TIPO_BULT" HeaderText="Unidad">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="NO_ENTI_FINA" HeaderText="Financiador"></asp:BoundColumn>
														<asp:BoundColumn DataField="DE_ALMA_ORIG" HeaderText="Almac&#233;n origen">
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="Almac&#233;n destino">
															<ItemTemplate>
																<asp:DropDownList id=cboAlmacen runat="server" CssClass="Text" Width="350px" Enabled="False" DataSource="<%# GetAlmacen() %>" DataTextField="DSC_ALMA" DataValueField="COD_ALMA" SelectedValue='<%#  DataBinder.Eval(Container, "DataItem.CO_ALMA_DEST") %>'>
																</asp:DropDownList>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn Visible="False" HeaderText="Nro solicitud">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn DataField="DE_ESTA_TRAS" HeaderText="Estado">
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="NU_DOCU_RECE" HeaderText="Nro Comprobante"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="CO_ENTI_FINA"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="ST_TRAS" HeaderText="COD_ESTADO"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="CO_UNID"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="CO_ALMA_ORIG"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="CO_ALMA_DEST"></asp:BoundColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="tblLeyenda" border="0" cellSpacing="4" cellPadding="0" width="100%">
													<TR>
														<TD class="td" colSpan="6"><asp:button id="btnSolicitar" runat="server" CssClass="btn" Width="80px" Text="Solicitar" Visible="False"></asp:button></TD>
													</TR>
													<TR>
														<TD bgColor="#ffffff" width="15%"></TD>
														<TD class="Leyenda" align="center">Ubicado</TD>
														<TD bgColor="#f5dc8c" width="15%"></TD>
														<TD class="Leyenda" align="center">Pendiente aprobaci�n</TD>
														<TD bgColor="#c8dcf0" width="15%"></TD>
														<TD class="Leyenda" align="center">Pendiente traslado</TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
