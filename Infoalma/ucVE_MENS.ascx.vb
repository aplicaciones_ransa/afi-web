Public MustInherit Class ucVE_MENS
    Inherits System.Web.UI.UserControl
    Private sCA_MENS, sPA_RETO As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Public Property pCA_MENS() As String
        Get
            Return sCA_MENS
        End Get
        Set(ByVal Value As String)
            sCA_MENS = Value
        End Set
    End Property

    Public Property pPA_RETO() As String
        Get
            Return sPA_RETO
        End Get
        Set(ByVal Value As String)
            sPA_RETO = Value
        End Set
    End Property

    Protected Overrides Sub Render(ByVal Output As HtmlTextWriter)
        Output.Write("<script language=Javascript>alert('" & pCA_MENS & "');")
        If pPA_RETO <> "" Then
            Output.Write(" window.location.href='" & pPA_RETO & "'; ")
        End If
        Output.Write("</script>")
    End Sub
End Class
