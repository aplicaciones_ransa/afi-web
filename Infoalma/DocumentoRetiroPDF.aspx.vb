Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports Infodepsa

Public Class DocumentoRetiroPDF
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objRetiro As New clsCNRetiro
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objReportes As Reportes = New Reportes
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private oHashedData As New CAPICOM.HashedData

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblFirma As System.Web.UI.WebControls.Label
    'Protected WithEvents LtrFirma As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar1 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar2 As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrVisor As System.Web.UI.WebControls.Literal
    'Protected WithEvents cntFirmas As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents txtArchivo_pdf As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtArchivo_pdf_firmado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNumSerie As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtEstado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTimeStamp As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_EMITIDO") Then
            Dim dtControles As New DataTable
            Dim dtRetiro As New DataTable
            Try
                Dim var As Integer
                Dim strNombreReporte As String
                Dim strNombreCliente As String

                var = Request.QueryString("var")
                Me.txtNroPagina.Value = var
                Me.txtEstado.Value = Session.Item("EstadoDoc")
                loadDataDocumento()
                txtNumSerie.Value = Session.Item("NroCertificado")
                Me.lblFirma.Text = ""
                Me.txtTimeStamp.Value = Now.ToString("dd/MM/yyyy hh:mm:ss tt")
                If var = 1 Or var = 3 Then
                    Dim listaAux As String
                    Dim drWarrant As DataRow
                    Dim strHTMLTable As String
                    Dim arrLista()
                    Dim arrDatosFila()
                    Dim i As Integer
                    '----------------------------
                    oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
                    '------------------------------
                    Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visorRetiro.aspx?var=1' runat='server'></iframe>"
                    drWarrant = objDocumento.gSelArchivoPDF(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), var)

                    If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                        txtArchivo_pdf.Value = Convert.ToBase64String(drWarrant("DOC_PDFFIRM"))
                    Else
                        oHashedData.Hash(Convert.ToBase64String(drWarrant("DOC_PDFORIG")))
                        Me.txtArchivo_pdf.Value = oHashedData.Value
                    End If

                    strHTMLTable = "<TABLE WIDTH='100%' BORDER='0' CELLSPACING='4' CELLPADDING='0' bgColor='#f0f0f0'>" _
                    & "<THEAD><TR><TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>FIRMADO DIGITALMENTE POR:</TD>" _
                    & "<TD style='font-Family:Verdana; font-size:11px; font-weight:bold; COLOR: #003399'>TIMESTAMP LOCAL</TD></TR></THEAD><TBODY>"
                    Dim dtFirmantes As DataTable
                    dtFirmantes = objDocumento.BLGetDatosFirmantes(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"))
                    For Each dr As DataRow In dtFirmantes.Rows
                        strHTMLTable = strHTMLTable & "<TR><TD width='60%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("NOMBRES") & "</TD>" _
                                            & "<TD width='40%' style='font-Family:Verdana; font-size:9px; font-weight:bold'>" & dr("FECHA") & "</TD></TR>"
                    Next
                    strHTMLTable = strHTMLTable & "</TBODY></TABLE>"
                    LtrFirma.Text = strHTMLTable
                End If

                'If Session.Item("IdTipoDocumento") = "23" Then
                '    Dim objConexion As SqlConnection
                '    Dim objTrans As SqlTransaction
                '    objConexion = New SqlConnection(strConn)
                '    objConexion.Open()
                '    objTrans = objConexion.BeginTransaction()
                '    dtRetiro = objRetiro.gCNCrearRetiroAprobado(Session.Item("CoEmpresa"), Session.Item("Cod_Unidad"), "DOR", Session.Item("NroLib"))
                '    strNombreReporte = strPDFPath & Session.Item("Cod_Unidad") & Session.Item("NroLib") & ".PDF"
                '    strNombreCliente = objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", Session.Item("NroLib"), _
                '                       Session.Item("IdSico"), CType(dtRetiro, DataTable), strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), _
                '                       True, Session.Item("IdSico"), "") ', Session.Item("Cod_Doc"), objTrans)
                'If Session.Item("IdSico") = System.Configuration.ConfigurationManager.AppSettings("CodClieDEPSA") Then
                '    If Session.Item("Aprobado") <> "S" And Session.Item("EstadoDoc") = "02" Then
                '        lblError.Text = "Debe registrar la informacion del Retiro Aduanero en el sico"
                '        'objTrans.Rollback()
                '        'objTrans.Dispose()
                '        Exit Sub
                '    End If
                'End If
                '    objTrans.Commit()
                '    objTrans.Dispose()
                'End If

                If Session.Item("IdTipoDocumento") = "23" And Session.Item("IdSico") = System.Configuration.ConfigurationManager.AppSettings("CodClieDEPSA") And
                    Session.Item("EstadoDoc") = "02" And Session.Item("FlgDepsa") = "N" Then
                    lblError.Text = "Debe registrar la información del Retiro Aduanero en el sico"
                    Exit Sub
                End If

                dtControles = objDocumento.gGetAccesoControles(Session.Item("IdTipoEntidad"), Session.Item("IdTipoUsuario"), Session.Item("NroDoc"),
                                                            Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdUsuario"), Session.Item("IdTipoDocumento"), Session.Item("IdSico"), var)

                Dim strfirma As String
                For j As Integer = 0 To dtControles.Rows.Count - 1
                    Select Case dtControles.Rows(j)("COD_CTRL")
                        Case "01"
                            If Session.Item("strcontrasenia") <> Nothing Then
                                If DateDiff(DateInterval.Minute, Session.Item("strfecha"), Now) > 5 Then
                                    strfirma = "X"
                                Else
                                    strfirma = "F"
                                    Me.txtContrasena.Value = Session.Item("strContrasenia")
                                End If
                            Else
                                strfirma = "X"
                            End If
                            If Request.QueryString("var") = 1 Then
                                If Session.Item("CertDigital") = True Then
                                    Me.lblError.Text = "No tiene certificado digital"
                                    'Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(1)' type='button' value='Firmar' name = 'cmdFirmar'>"
                                    'Me.ltrFirmar2.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(3)' type='button' value='Rechazar' name = 'cmdFirmar'>"
                                Else
                                    If strfirma = "X" Then
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: blue' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                        'Me.ltrFirmar2.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(3)' type='button' value='Rechazar' name = 'cmdFirmar'>"
                                    Else
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: blue' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                        'Me.ltrFirmar2.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(3)' type='button' value='Rechazar' name = 'cmdFirmar'>"
                                    End If
                                End If
                            End If
                            'Validar()
                            Dim objSICO As SICO = New SICO
                            Dim sST_VAL_APRO As String = objSICO.fn_TCRETI_MERC_SEL_APR(Session("CoEmpresa"), Session("Cod_Unidad"), "DOR", Session("NroLib"))
                            If sST_VAL_APRO = "N" Then
                                Validar()
                            End If
                        Case "04"
                            Me.ltrFirmar2.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 80px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(3)' type='button' value='Rechazar' name = 'cmdFirmar'>"
                        Case "00"
                            Me.lblFirma.Text = "Otro usuario esta intentando firmar el documento, intente en otro momento"
                    End Select
                Next

                Me.lblError.Text = Session.Item("Mensaje")
                Session.Add("Mensaje", "")
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            Finally
                dtRetiro.Dispose()
                dtRetiro = Nothing
                dtControles.Dispose()
                dtControles = Nothing
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Validar()
        'Dim objSICO As SICO = New SICO
        'Dim sDE_MEN1, sDE_MEN2 As String
        'Dim sST_EJEC_RETI As String = objSICO.fn_TMCLIE_MERC_Q01(Session("CoEmpresa"), Session("Cod_Unidad"), Session("IdSico"), Session("Moneda"), Session("Importe"))

        'If sST_EJEC_RETI = "N" Then

        '    sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Session("Cod_Unidad"), "DOR",
        '                    Session("NroLib"), "1", Session("Moneda"), Session("Importe"), Session("UsuarioLogin"), "N", "")
        '    pr_IMPR_MENS(sDE_MEN1)
        '    Me.ltrFirmar1.Text = ""
        '    Exit Sub
        'End If
        'If sST_EJEC_RETI = "X" Then
        '    sDE_MEN1 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Session("Cod_Unidad"), "DOR",
        '                      Session("NroLib"), "9", Session("Moneda"), Session("Importe"), Session("UsuarioLogin"), "N", "")
        '    pr_IMPR_MENS(sDE_MEN1)
        '    Me.ltrFirmar1.Text = ""
        '    Exit Sub
        'End If
        ''--validacion por antiguedad
        'Dim sFE_ACTU As String = FormatDateTime(Now(), DateFormat.ShortDate)
        'Dim dsTCDOCU_CLIE As DataSet = objSICO.fn_TCDOCU_CLIE_Q08(Session("CoEmpresa"), Session("Cod_Unidad"), sFE_ACTU, Session("IdSico"))
        'If dsTCDOCU_CLIE.Tables(0).Rows(0)("ST_ESTA") = 1 Then
        '    sDE_MEN2 = objSICO.fn_THESTA_CLIE_I01(Session("CoEmpresa"), Session("Cod_Unidad"), "DOR",
        '                            Session("NroLib"), "2", Session("Moneda"), Session("Importe"), Session("UsuarioLogin"), "N", "")
        '    pr_IMPR_MENS(sDE_MEN2)
        '    Me.ltrFirmar1.Text = ""
        '    Exit Sub
        'End If
    End Sub

    Function listaFirmas(ByVal prmArchivo As Byte(), ByVal prmArchivoOriginal As Byte(), ByVal strFechaModi As DateTime) As String
        Try
            Dim SignedData As CAPICOM.SignedData
            Dim Signer As CAPICOM.Signer
            Dim strLista As String
            Dim strFila As String
            Dim Atrib As CAPICOM.Attribute
            Dim strNombreArchivo As String
            Dim strTimeStamp As String
            Dim Contenido As String
            Dim mstream As System.IO.MemoryStream
            '------------------------------
            oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
            '-----------------------------
            SignedData = New CAPICOM.SignedData
            Contenido = Convert.ToBase64String(prmArchivo)
            SignedData.Verify(Contenido, False, CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_ONLY)
            '-------------------------------
            Dim fechaAux As Date
            fechaAux = Format("MM/dd/yyyy", ConfigurationManager.AppSettings.Item("FechaCambioValidador"))
            If DateDiff(("d"), fechaAux, Date.Now) > 0 Then
                oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
                If oHashedData.Value <> SignedData.Content Then
                    listaFirmas = "X"
                    Exit Function
                End If
            End If
            'If strFechaModi > CType(ConfigurationManager.AppSettings.Item("FechaCambioValidador"), DateTime) Then
            '    oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
            '    If oHashedData.Value <> SignedData.Content Then
            '        listaFirmas = "X"
            '        Exit Function
            '    End If
            'End If
            '--------------------------------
            listaFirmas = ""
            strLista = ""
            Signer = New CAPICOM.Signer
            For Each Signer In SignedData.Signers
                strFila = Signer.Certificate.GetInfo(0) & "*"
                For Each Atrib In Signer.AuthenticatedAttributes
                    If Atrib.Name = 2 Then
                        strTimeStamp = Atrib.Value
                        strFila = strFila & strTimeStamp
                    End If
                Next
                strLista = strLista & strFila & ";"
            Next
            listaFirmas = Mid(UCase(strLista), 1, Len(UCase(strLista)) - 1)
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Function

    Private Sub loadDataDocumento()
        Try
            Dim drUsuario As DataRow
            If Me.txtNroPagina.Value = "1" Then
                drUsuario = objDocumento.gGetDataDocumento(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), strEmpresa)
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim)
                End If
            End If
            drUsuario = Nothing
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub GrabaPDF(ByVal strNombArch As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim Parametro As String
        Dim strEncriptado As String
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Contenido, Session.Item("IdTipoDocumento"))
        fs.Close()
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
