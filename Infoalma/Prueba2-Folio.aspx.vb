﻿Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports DEPSA.LibCapaNegocio
'Imports InfodepsaWS.Infodepsa
Imports System.IO
'Imports Infodepsa
'Imports WsFilemaster.Infodepsa
Imports System.Data
Partial Class Prueba2_Folio
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb = New clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    Private objDocumento As Documento = New Documento
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    'Private objAccesoWeb As Library.AccesoBL.clsCNAccesosWeb = New Library.AccesoBL.clsCNAccesosWeb
    'Private oHashedData As New CAPICOM.HashedData
    Private strEndosoPath As String = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strEmpresa As String = System.Configuration.ConfigurationManager.AppSettings("CoEmpresa")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strSessMensaje As String
    Private strSessResultado As String
    Protected Sub btnProbar_Click(sender As Object, e As EventArgs) Handles btnProbar.Click
        Try

            'lblError.Text = ""
            'Dim strNroWarrant As String, strNroLiberacion As String, strPeriodoAnual As String,
            'strIdTipoDocumento As String, strIdSico As String, strContrasenaClave As String, strNuevaContrasenaClave As String,
            'strIdUsuario As String, strCodEmpr As String, strUsuarioLogin As String, strIdTipoEntidad As String, strTipoAccion As String
            'strNroWarrant = "00092972" 'numero de warrant
            'strNroLiberacion = "00000000000" ' numero de libberacion
            'strPeriodoAnual = "2018" 'periodo anual
            'strIdTipoDocumento = "01" ' Tipo de Documento
            'strIdSico = "5000"  '--.Id del combo ,empresas que fue seleccionado - campo(COD_SICO) - proc(Usp_WFUSRXENTDAD_Mobil_Sel_ValidaEntidadUsuario)
            'strContrasenaClave = "0B4C78AB762E569A" ' --- password de firma campo(USR_PASFIRM) - proc(Usp_WFUSUARIO_Mobil_Sel_ValidaUsuario)
            'strNuevaContrasenaClave = "0B4C78AB762E569A" '---nueva contraseña
            'strIdUsuario = "2181" '--- Id del usuario - campo(COD_USER) - proc(Usp_WFUSUARIO_Mobil_Sel_ValidaUsuario)
            'strCodEmpr = "01" ' Codigo de empresa del web config
            'strUsuarioLogin = "AMARTINEZ" '-- usuario que utilizo para el logueo - campo(USR_ACCE) - proc(Usp_WFUSUARIO_Mobil_Sel_ValidaUsuario)
            'strIdTipoEntidad = "02"  '--.Código del tipo de entidad  ,empresas que fue seleccionado - campo(COD_TIPENTI) - proc(Usp_WFUSRXENTDAD_Mobil_Sel_ValidaEntidadUsuario)
            'strTipoAccion = "APR" '----.”APR” Aprobar – “DES” = Rechazar
            '' strTipoAccion = "DES" '----.”APR” Aprobar – “DES” = Rechazar

            'Dim dtResultado As DataTable
            'Dim strMensaje As String = ""
            'Dim strResultado As String = ""

            'Dim objDocumentoWS As DocumentoWS
            'objDocumentoWS = New DocumentoWS

            'strMensaje = objDocumentoWS.InicializarObtenerDatos(strNroWarrant, strNroLiberacion, strPeriodoAnual, strIdTipoDocumento, strIdSico, strContrasenaClave,
            ' strNuevaContrasenaClave, strIdUsuario, strCodEmpr, strUsuarioLogin, strIdTipoEntidad, strTipoAccion)


            '=== Retornando el resultado ===
            'strSessResultado = objDocumentoWS.FnSessResultado
            'strSessMensaje = objDocumentoWS.FnSessMensaje
            'dtResultado = crearTabla()
            'Dim dr As DataRow
            'dr = dtResultado.NewRow
            'dr(0) = strSessResultado
            'dr(1) = strSessMensaje
            'dtResultado.Rows.Add(dr)
            'Return dtResultado

            'lblError.Text = strMensaje

        Catch ex As Exception
            strSessResultado = "X" : strSessMensaje = ex.Message.ToString

        End Try
    End Sub

    'Function crearTabla() As DataTable
    '    Dim dtResultado As New DataTable
    '    'dtIngreso.Rows.Clear()
    '    Dim Column0 As New DataColumn("RESULTADO")
    '    Column0.DataType = GetType(String)
    '    Dim Column1 As New DataColumn("MENSAJE")
    '    Column1.DataType = GetType(String)
    '    dtResultado.Columns.Add(Column0)
    '    dtResultado.Columns.Add(Column1)
    '    Session.Add("dtResultado", dtResultado)
    '    Return dtResultado
    'End Function

    'Protected Sub btnEnviarFirma_Click(sender As Object, e As EventArgs) Handles btnEnviarFirma.Click
    '    Try
    '        Dim strMensaje As String = ""
    '        Dim strResultado As String = ""


    '        'Dim strEncIdUsuario As String
    '        'Dim strEncUsuarioLogin As String
    '        'Dim strEncEmail As String
    '        'strEncIdUsuario = Funciones.EncryptString("1929", "©¹ã_[]0")
    '        'strEncUsuarioLogin = Funciones.EncryptString("FMALAVER", "©¹ã_[]0")
    '        'strEncEmail = Funciones.EncryptString("fmalavers@depsa.com.pe", "©¹ã_[]0")

    '        Dim strIdUsuario As String
    '        Dim strUsuarioLogin As String
    '        Dim strEmail As String

    '        strIdUsuario = "5E2A6359C7A17FC1" '-- encriptado (1929)
    '        strUsuarioLogin = "0F71387077A5A6D6697B229614C84EC2" '-- encriptado (FMALAVER)
    '        strEmail = "B21F8D3BC5307B70D0F557ED5094DD08AF4B74A43F17AA73" '-- encriptado (fmalavers@depsa.com.pe)

    '        Dim objDocumentoWS As DocumentoWS
    '        objDocumentoWS = New DocumentoWS
    '        'strMensaje = objValorCampoVerificacionBC.Fn_ResetearClaveFirma(strEncIdUsuario, strEncUsuarioLogin, strEncEmail)
    '        strMensaje = objDocumentoWS.Fn_ResetearClaveFirma(strIdUsuario, strUsuarioLogin, strEmail)
    '        lblError.Text = strMensaje

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    Protected Sub btnEnviarFirma_Click(sender As Object, e As EventArgs) Handles btnEnviarFirma.Click
        'Dim dtAcceso As DataTable
        'Dim strIdTipoUsuario As String = ""
        'dtAcceso = objAccesoWeb.gCNMostrarDatosUsuario("0499", "00000001")
        'For Each dr0 As DataRow In dtAcceso.Rows
        '    strIdTipoUsuario = dtAcceso.Rows(0).Item("COD_TIPUSR")
        'Next
    End Sub
    Protected Sub btnregistrar_Click(sender As Object, e As EventArgs) Handles btnregistrar.Click

        Dim dt As DataTable
        Dim strIdTipoUsuario As String, strIdUsuario As String, strIdTipoEntidad As String,
            strIdSico As String, strNombreEntidad As String, strNombrePDF As String, strUsuarioLogin As String,
            strIdTipoDocumento As String, strNroDoc As String, strNroLib As String, strCod_TipOtro As String,
            strPeriodoAnual As String, strCod_Doc As String, strFechApro As DateTime, strNroFolio As String

        dt = objDocumento.gGetDocumento_PruRegu("")

        For Each dr1 As DataRow In dt.Rows

            strIdTipoUsuario = dt.Rows(0).Item("COD_TIPUSR")
            strIdTipoEntidad = dt.Rows(0).Item("ID_TIPO_ENTI")
            strIdSico = dt.Rows(0).Item("ID_SICO")
            strNombreEntidad = dt.Rows(0).Item("NOM_ENTI")
            strNombrePDF = dt.Rows(0).Item("NOM_PDF")
            strUsuarioLogin = dt.Rows(0).Item("USR_ACCE")
            strNroDoc = dt.Rows(0).Item("NRO_DOCU")
            strNroLib = dt.Rows(0).Item("NRO_LIBE")
            strCod_TipOtro = dt.Rows(0).Item("COD_TIPOTRO")
            strPeriodoAnual = dt.Rows(0).Item("PRD_DOCU")
            strFechApro = dt.Rows(0).Item("FCH_APRO_FINA")
            strNroFolio = dt.Rows(0).Item("NRO_FOLIO")

            strIdTipoDocumento = dt.Rows(0).Item("COD_TIPDOC")
            strCod_Doc = dt.Rows(0).Item("COD_DOC")
            strIdUsuario = dt.Rows(0).Item("COD_USER")

            'RegistrarDocumento3(strIdTipoDocumento, strCod_Doc, strIdUsuario, strNroDoc)

            RegistrarDocumento2(strIdTipoUsuario, strIdUsuario, strIdTipoEntidad, strIdSico,
                                   strNombreEntidad, strNombrePDF, strUsuarioLogin, strIdTipoDocumento,
                                   strNroDoc, strNroLib, strCod_TipOtro, strPeriodoAnual, strCod_Doc,
                                   strFechApro, strNroFolio)



        Next


    End Sub



    '========= prueba para regularizar los warrant que en la firma final no registro el folio 
    Private Sub RegistrarDocumento2(strIdTipoUsuario As String, strIdUsuario As String, strIdTipoEntidad As String, strIdSico As String,
                                   strNombreEntidad As String, strNombrePDF As String, strUsuarioLogin As String, strIdTipoDocumento As String,
                                   strNroDoc As String, strNroLib As String, strCod_TipOtro As String, strPeriodoAnual As String, strCod_Doc As String,
                                   strFechApro As DateTime, strNroFolio As String)



        'Private Sub RegistrarDocumento2(strIdTipoDocumento As String, strCod_Doc As String, strIdUsuario As String, strNroDoc As String)


        '=== VALIDAMOS CONFIGURACION PARA REGISTRAR === str
        'Dim strValidaRegistrar As Boolean = False
        'Dim dtControles As New DataTable
        'dtControles = objDocumento.gGetAccesoControles(strIdTipoEntidad, strIdTipoUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdUsuario, strIdTipoDocumento, strIdSico, 0)
        'For i As Integer = 0 To dtControles.Rows.Count - 1
        '    Select Case dtControles.Rows(i)("COD_CTRL")
        '        '==== REGISTRAR ===
        '        Case "08"
        '            strValidaRegistrar = True
        '    End Select
        'Next

        ' If strValidaRegistrar = True Then

        'objAccesoWeb = New clsCNAccesosWeb
        'objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, CStr(strIdTipoEntidad).Substring(1), strIdSico, strNombreEntidad, "C", "1", "REGISTRAR DOCUMENTO - MOVIL", "", "")

        Dim drEndoso As DataRow
        Dim drSICO As DataRow
        Dim NombreReporte As String
        Dim strIds As String
        Dim dtProrroga As DataTable
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try

            'If strIdTipoDocumento = "05" Then
            '    dtProrroga = objDocumento.gGetDataProrroga(strNroDoc, strNroLib, strCod_TipOtro) 'ok(sin sessiones internas)
            '    If dtProrroga.Rows.Count = 0 Then
            '        objTrans.Rollback()
            '        objTrans.Dispose()
            '        strSessResultado = "X" : strSessMensaje = "No se encontró la Prorroga"
            '        Exit Sub
            '    Else
            '        NombreReporte = strEndosoPath & strNombrePDF
            '        drSICO = objEntidad.gUpdProrrogaSICO(strEmpresa, dtProrroga.Rows(0)("COD_TIPDOC"), dtProrroga.Rows(0)("NRO_DOCU"),
            '        dtProrroga.Rows(0)("MON_ADEC"), dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("SAL_ACTU"),
            '        dtProrroga.Rows(0)("FCH_OBLIGACION"), strUsuarioLogin) 'ok(sin sessiones internas)
            '        If drSICO.IsNull("UL_NUME_FOLI") Then
            '            objTrans.Rollback()
            '            objTrans.Dispose()
            '            strSessResultado = "X" : strSessMensaje = "No se pudo actualizar en SICO"
            '            Exit Sub
            '        End If

            '        objDocumento.gUpdFolioProrroga(drSICO("UL_NUME_FOLI"), drSICO("FE_NUME_FOLI"), drSICO("NU_SECU"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)
            '        objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado la Prorroga<br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)
            '        objTrans.Commit()
            '        objTrans.Dispose()
            '    End If
            '    '   Warrants de Custodia
            'ElseIf strIdTipoDocumento = "20" Then
            '    objDocumento.gUpdRegistrar_Custodia_SICO(strNroDoc, strIdTipoDocumento, strNroLib, strPeriodoAnual, objTrans) 'ok(sin sessiones internas)
            '    objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)

            '    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado Warrants Custodia <br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)

            '    objTrans.Commit()
            '    objTrans.Dispose()
            '    strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
            'Else


            If strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "10" Then

                objDocumento.gRegistraEndosoSICO_PruRegu(strCod_Doc, strIdUsuario, strFechApro, strNroFolio, objTrans) 'ok(sin sessiones internas)[OK]

                If objDocumento.strResultado = "0" Then
                    objDocumento.gRegistraDobleEndosoSICO_PruRegu(strCod_Doc, objTrans) 'ok(sin sessiones internas)
                    If objDocumento.strResultado <> "0" Then
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Exit Try
                    End If
                    'dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans) 'ok(sin sessiones internas)
                    'For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    '    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                    '        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans) 'ok(sin sessiones internas)

                    '    Else
                    '        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    '    End If
                    '    'strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                    'Next



                    'If strIds = "" Then
                    '    objTrans.Rollback()
                    '    objTrans.Dispose()
                    '    strSessResultado = "X" : strSessMensaje = "No se encontró el warrant relacionado a este documento"
                    '    Exit Sub
                    'End If
                    objReportes.GeneraPDFWarrantCopia(strCod_Doc, "", strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)
                    'objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado el Documento<br><br>", strEndosoPath, "S", "N", objTrans) 'ok(sin sessiones internas)

                    objTrans.Commit()
                    objTrans.Dispose()
                    strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
                ElseIf objDocumento.strResultado = "3" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "El documento ya fue registrado"
                    Exit Sub
                ElseIf objDocumento.strResultado = "4" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "Debe de realizar el doble endoso en el SICO antes de registrarlo"
                    Exit Sub
                Else
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "X" : strSessMensaje = "Fallo al registrar"
                    Exit Sub
                End If
            End If


            Me.lblError.Text = Me.lblError.Text + strSessResultado + " " + strSessMensaje + " " + strNroDoc

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            strSessResultado = "X" : strSessMensaje = ex.Message.ToString
            Me.lblError.Text = "X" : strSessMensaje = ex.Message.ToString
        Finally
        End Try
        ' End If
    End Sub


    Private Sub RegistrarDocumento3(strIdTipoDocumento As String, strCod_Doc As String, strIdUsuario As String, strNroDoc As String)


        '=== VALIDAMOS CONFIGURACION PARA REGISTRAR === str
        'Dim strValidaRegistrar As Boolean = False
        'Dim dtControles As New DataTable
        'dtControles = objDocumento.gGetAccesoControles(strIdTipoEntidad, strIdTipoUsuario, strNroDoc, strNroLib, strPeriodoAnual, strIdUsuario, strIdTipoDocumento, strIdSico, 0)
        'For i As Integer = 0 To dtControles.Rows.Count - 1
        '    Select Case dtControles.Rows(i)("COD_CTRL")
        '        '==== REGISTRAR ===
        '        Case "08"
        '            strValidaRegistrar = True
        '    End Select
        'Next

        ' If strValidaRegistrar = True Then

        'objAccesoWeb = New clsCNAccesosWeb
        'objAccesoWeb.gCNInsAccesosWeb(strIdUsuario, CStr(strIdTipoEntidad).Substring(1), strIdSico, strNombreEntidad, "C", "1", "REGISTRAR DOCUMENTO - MOVIL", "", "")

        Dim drEndoso As DataRow
        Dim drSICO As DataRow
        Dim NombreReporte As String
        Dim strIds As String
        Dim dtProrroga As DataTable
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try

            'If strIdTipoDocumento = "05" Then
            '    dtProrroga = objDocumento.gGetDataProrroga(strNroDoc, strNroLib, strCod_TipOtro) 'ok(sin sessiones internas)
            '    If dtProrroga.Rows.Count = 0 Then
            '        objTrans.Rollback()
            '        objTrans.Dispose()
            '        strSessResultado = "X" : strSessMensaje = "No se encontró la Prorroga"
            '        Exit Sub
            '    Else
            '        NombreReporte = strEndosoPath & strNombrePDF
            '        drSICO = objEntidad.gUpdProrrogaSICO(strEmpresa, dtProrroga.Rows(0)("COD_TIPDOC"), dtProrroga.Rows(0)("NRO_DOCU"),
            '        dtProrroga.Rows(0)("MON_ADEC"), dtProrroga.Rows(0)("SAL_ADEC"), dtProrroga.Rows(0)("SAL_ACTU"),
            '        dtProrroga.Rows(0)("FCH_OBLIGACION"), strUsuarioLogin) 'ok(sin sessiones internas)
            '        If drSICO.IsNull("UL_NUME_FOLI") Then
            '            objTrans.Rollback()
            '            objTrans.Dispose()
            '            strSessResultado = "X" : strSessMensaje = "No se pudo actualizar en SICO"
            '            Exit Sub
            '        End If

            '        objDocumento.gUpdFolioProrroga(drSICO("UL_NUME_FOLI"), drSICO("FE_NUME_FOLI"), drSICO("NU_SECU"), strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)
            '        objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado la Prorroga<br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)
            '        objTrans.Commit()
            '        objTrans.Dispose()
            '    End If
            '    '   Warrants de Custodia
            'ElseIf strIdTipoDocumento = "20" Then
            'objDocumento.gUpdRegistrar_Custodia_SICO(strNroDoc, strIdTipoDocumento, strNroLib, strPeriodoAnual, objTrans) 'ok(sin sessiones internas)
            '    objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)

            '    objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado Warrants Custodia <br><br>", "", "S", "N", objTrans) 'ok(sin sessiones internas)

            '    objTrans.Commit()
            '    objTrans.Dispose()
            '    strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
            'Else


            'If strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "10" Then

            ' objDocumento.gRegistraEndosoSICO_PruRegu(strCod_Doc, strIdUsuario, strFechApro, strNroFolio, objTrans) 'ok(sin sessiones internas)[OK]

            ' If objDocumento.strResultado = "0" Then
            'objDocumento.gRegistraDobleEndosoSICO_PruRegu(strCod_Doc, objTrans) 'ok(sin sessiones internas)
            'If objDocumento.strResultado <> "0" Then
            '    objTrans.Rollback()
            '    objTrans.Dispose()
            '    Exit Try
            'End If
            'dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans) 'ok(sin sessiones internas)
            'For i As Integer = 0 To dtProrroga.Rows.Count - 1
            '    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
            ' objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans) 'ok(sin sessiones internas)

            'Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
            'objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
            'objWSFileMaster.CopiaDocumentosInfodepsaRegistrados(strCod_Doc)





            If strIdTipoDocumento = "03" Or strIdTipoDocumento = "10" Or strIdTipoDocumento = "12" Or strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Then
                If objDocumento.gGetEstadoImpresion(strCod_Doc) = True Then
                    objTrans.Commit()
                    objTrans.Dispose()
                    Exit Try
                End If
                dtProrroga = objDocumento.GetIdsDocumentos(strCod_Doc, objTrans)
                For i As Integer = 0 To dtProrroga.Rows.Count - 1
                    If dtProrroga.Rows(i)("COD_TIPDOC") = "09" Or dtProrroga.Rows(i)("COD_TIPDOC") = "17" Or dtProrroga.Rows(i)("COD_TIPDOC") = "18" Then
                        objReportes.GeneraDCR(dtProrroga.Rows(i)("COD_DOC"), strPDFPath, strPathFirmas, True, strIdTipoDocumento, strIdUsuario, objTrans)
                    Else
                        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                        objReportes.GeneraPDFWarrantFirmasXDocumento(strCod_Doc, dtProrroga.Rows(i)("COD_DOC"), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, dtProrroga.Rows(i)("NOM_PDF"), objTrans)
                    End If
                Next

                If strIds = "" Then
                    objTrans.Rollback()
                    objTrans.Dispose()
                    strSessResultado = "No se encontró el warrant relacionado a este documento"
                    Exit Sub
                End If
                If strIdTipoDocumento <> "12" Then
                    objReportes.GeneraPDFWarrantFirmas(strCod_Doc, strCod_Doc, strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans)
                End If
                objTrans.Commit()
                objTrans.Dispose()
                strSessResultado = "X"
                '------------------------------------------------------------------- cf4
                Dim objWSFileMaster As DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster = New DEPSA.FileMaster.BL.SI.Infodepsa
                objWSFileMaster.CopiaDocumentosInfodepsaRegistrados(strCod_Doc)
                'End If

                'Else
                '        strIds += dtProrroga.Rows(i)("COD_DOC") & "-"
                'End If
                'Next



                'If strIds = "" Then
                '    objTrans.Rollback()
                '    objTrans.Dispose()
                '    strSessResultado = "X" : strSessMensaje = "No se encontró el warrant relacionado a este documento"
                '    Exit Sub
                'End If
                'objReportes.GeneraPDFWarrantCopia(strCod_Doc, strIds.Substring(0, strIds.Length - 1), strEndosoPath, strPathFirmas, True, True, strIdUsuario, strIdTipoDocumento, objTrans) 'ok(sin sessiones internas)
                ' objFunciones.gEnvioMailConAdjunto(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, "Se ha registrado el Documento<br><br>", strEndosoPath, "S", "N", objTrans) 'ok(sin sessiones internas)
                objTrans.Commit()
                objTrans.Dispose()
                strSessResultado = "I" : strSessMensaje = "Se registró correctamente"
                'ElseIf objDocumento.strResultado = "3" Then
                '    objTrans.Rollback()
                '    objTrans.Dispose()
                '    strSessResultado = "X" : strSessMensaje = "El documento ya fue registrado"
                '    Exit Sub
                'ElseIf objDocumento.strResultado = "4" Then
                '    objTrans.Rollback()
                '    objTrans.Dispose()
                '    strSessResultado = "X" : strSessMensaje = "Debe de realizar el doble endoso en el SICO antes de registrarlo"
                '    Exit Sub
                'Else
                '    objTrans.Rollback()
                '    objTrans.Dispose()
                '    strSessResultado = "X" : strSessMensaje = "Fallo al registrar"
                '    Exit Sub
                'End If
            End If


            Me.lblError.Text = Me.lblError.Text + strSessResultado + " " + strSessMensaje + " " + strNroDoc

        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            strSessResultado = "X" : strSessMensaje = ex.Message.ToString
            Me.lblError.Text = "X" : strSessMensaje = ex.Message.ToString
        Finally
        End Try
        ' End If
    End Sub


    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '----genera solicitud warrant prueba






    End Sub
End Class
