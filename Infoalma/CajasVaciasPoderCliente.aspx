<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CajasVaciasPoderCliente.aspx.vb" Inherits="CajasVaciasPoderCliente" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DEPSA Files - Unidades Vac�as en Poder del Cliente</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="nanglesc@hotmail.com" name="author">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	
        
        $(function () {
            $("#txtFechaHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#Button2").click(function () {
                $("#txtFechaHasta").datepicker('show');
            });

        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        //-----------------------------------Fin formatear fecha	
        function Ocultar() {
            Estado.style.display = 'none';
        }
    </script>
</head>
<body bottommargin="0" bgcolor="#f0f0f0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f0f0f0" border="0">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td valign="top" width="108">
                    <uc1:Menu ID="Menu1" runat="server"></uc1:Menu>
                </td>
                <td valign="top">
                    <table id="Table1" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Titulo1" height="20">CAJAS&nbsp;VACIAS EN PODER DEL CLIENTE</td>
                        </tr>
                        <tr>
                            <td class="td" valign="top" align="left">
                                <table id="Table16" cellspacing="0" cellpadding="0" border="0" align="center" width="630">
                                    <tr>
                                        <td width="6" background="Images/table_r1_c1.gif" height="6"></td>
                                        <td background="Images/table_r1_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r1_c3.gif" height="6"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r2_c1.gif"></td>
                                        <td>
                                            <table id="Table2" cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr class="Texto9pt">
                                                    <td class="text">�rea</td>
                                                    <td class="text" style="height: 12px" width="2">:</td>
                                                    <td class="text">
                                                        <asp:DropDownList ID="cboArea" runat="server" CssClass="Text" DataTextField="DES" DataValueField="COD"
                                                            Width="140px">
                                                            <asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="text" width="68">Fecha&nbsp;Despacho al</td>
                                                    <td class="text">:</td>
                                                    <td class="text" align="left">&nbsp;
                                                        <input class="Text" onkeypress="validarcharfecha()" id="txtFechaHasta" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaHasta" runat="server"><input class="Text" id="Button2"  type="button" value="..."
                                                                name="btnFecha"></td>
                                                    <td class="text" width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="6" background="Images/table_r2_c3.gif"></td>
                                    </tr>
                                    <tr>
                                        <td width="6" background="Images/table_r3_c1.gif" height="6"></td>
                                        <td background="Images/table_r3_c2.gif" height="6"></td>
                                        <td width="6" background="Images/table_r3_c3.gif" height="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <p class="text">
                                    Resultados :
										<asp:Label ID="lblRegistros" runat="server" CssClass="text"></asp:Label><asp:DataGrid ID="dgdResultado" TabIndex="2" runat="server" CssClass="gv" Width="100%" AllowSorting="True"
                                            PageSize="20" AllowPaging="True" AutoGenerateColumns="False" CellPadding="0" BorderColor="Gainsboro">
                                            <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                            <ItemStyle CssClass="gvRow"></ItemStyle>
                                            <HeaderStyle CssClass="gvHeader"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="DE_PERS_SOLI" HeaderText="Solicitante">
                                                    <HeaderStyle Width="40%"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="NU_CAJA" HeaderText="Cant. Cajas">
                                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="FE_ENVI_UNVA" HeaderText="Fecha Despacho" DataFormatString="{0:d}">
                                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="FE_CIER_ENTR" HeaderText="Fecha Devoluci&#243;n">
                                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn Visible="False" DataField="CO_AREA"></asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle NextPageText="&amp;gt;s" HorizontalAlign="Center" PageButtonCount="20" CssClass="gvPager"
                                                Mode="NumericPages"></PagerStyle>
                                        </asp:DataGrid>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 25px" valign="top">
                                <p>
                                    <table class="text" id="Table3" cellspacing="2" cellpadding="0" width="100%" border="0">
                                        <tr class="Texto9pt">
                                            <td style="height: 20px" width="40%">
                                                <asp:Label ID="Label3" runat="server" CssClass="text" Text="Total de Cajas en poder del Cliente"
                                                    Font-Bold="true">Total de Cajas en poder del Cliente</asp:Label></td>
                                            <td style="height: 20px" width="60%">
                                                <asp:Label ID="lblTotalCajas" runat="server" Font-Bold="true">0</asp:Label></td>
                                        </tr>
                                    </table>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <table id="Table4" cellspacing="3" cellpadding="3" width="100%" border="0">
                                    <tr>
                                        <td align="right" width="50%">
                                            <asp:Button ID="btnVerReporte" runat="server" CssClass="btn" Width="100px" Text="Ver Reporte"></asp:Button></td>
                                        <td align="left">
                                            <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="td" id="Estado"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
