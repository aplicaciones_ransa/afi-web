<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ReporteKardexDetalle.aspx.vb" Inherits="ReporteKardexDetalle" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ReporteKardexDetalle</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="7">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" align="center" width="100%">
									<TABLE id="tblPrincipal" cellSpacing="0" cellPadding="0" width="690" border="0" runat="server">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="100%" bgColor="whitesmoke" border="0">
													<TR>
														<TD class="Text" width="17%">N�mero Warrant</TD>
														<TD class="Text" width="1%">:</TD>
														<TD class="Text" width="32%">
															<asp:label id="lblNroWarrant" runat="server"></asp:label></TD>
														<TD class="Text" width="15%">Fecha Ingreso</TD>
														<TD class="Text" width="1%">:</TD>
														<TD class="Text" width="34%">
															<asp:label id="lblFechaIngreso" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">N�mero DCR</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblNroOperacion" runat="server"></asp:label></TD>
														<TD class="Text">Monto Inicial</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblMontoInicial" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Depositante</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblDepositante" runat="server"></asp:label></TD>
														<TD class="Text">Monto de la Deuda</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblMontoDeuda" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Almac�n</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblAlmacen" runat="server"></asp:label></TD>
														<TD class="Text">N� Garant�a</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblNroGarantia" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Tipo de Almac�n</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblTipoAlmacen" runat="server"></asp:label></TD>
														<TD class="Text">R�gimen</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblRegimen" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="td" colSpan="3" height="30">Vencimientos Almacenera</TD>
														<TD class="td" colSpan="3" height="30">Vencimientos Banco</TD>
													</TR>
													<TR>
														<TD class="Text">Fecha Vcto. Warrant</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblVctoWarrant" runat="server"></asp:label></TD>
														<TD class="Text">1� Vcto. Pr�stamo</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblVctoPrestamo" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text">Vcto. L�mite Warrant</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblVctoLimite" runat="server"></asp:label></TD>
														<TD class="Text">N� Prorroga</TD>
														<TD class="Text">:</TD>
														<TD class="Text">
															<asp:label id="lblNroProrroga" runat="server"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text"></TD>
														<TD class="Text"></TD>
														<TD class="Text"></TD>
														<TD class="Text"></TD>
														<TD class="Text"></TD>
														<TD class="Text"></TD>
													</TR>
													<TR>
														<TD class="Text" align="center" colSpan="6">
															<asp:button id="btnExportar" runat="server" CssClass="btn" Text="Exportar" Width="80px"></asp:button><INPUT class="btn" id="btnRegresar" style="WIDTH: 80px" onclick="javascript:history.go(-1)"
																type="button" value="Regresar"></TD>
													</TR>
													<TR>
														<TD class="td" colSpan="6">Detalle del DCR</TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="6">
															<asp:datagrid id="dgDCR" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro" AutoGenerateColumns="False"
																PageSize="30" BorderWidth="1px">
																<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
																<ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
																<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
																<Columns>
																	<asp:BoundColumn DataField="NU_SECU" HeaderText="Item">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc. Mercader&#237;a">
																		<ItemStyle HorizontalAlign="Left"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unidades">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="NU_UNID_RECI" HeaderText="Cantidad" DataFormatString="{0:N2}">
																		<ItemStyle HorizontalAlign="Right"></ItemStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD class="td" colSpan="6">Liberaciones Asociadas</TD>
													</TR>
													<TR>
														<TD class="Text" colSpan="6">
															<asp:datagrid id="dgLiberacion" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
																AutoGenerateColumns="False" PageSize="30" BorderWidth="1px">
																<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
																<ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
																<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Nro. Liberaci&#243;n">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:HyperLink Runat="server" Target="_blank" NavigateUrl='<%# "KardexPDF.aspx?sNU_TITU=" & Container.DataItem("NU_TITU") & "&sNU_DOCU_RETI=" & Container.DataItem("NU_DOCU_RETI") & "&sCOD_TIPDOC=" & Container.DataItem("COD_TIPDOC") & "&sPRD_DOCU=" & Container.DataItem("PRD_DOCU") %>' ID="hlkPDF" ToolTip="Ver Liberaci�n">
																				<%# DataBinder.Eval(Container, "DataItem.NU_DOCU_RETI") %>
																			</asp:HyperLink>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn DataField="FE_SALI_ALMA" HeaderText="Fecha Atenci&#243;n" DataFormatString="{0:N2}">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="IM_RETI" HeaderText="Valor" DataFormatString="{0:N2}">
																		<ItemStyle HorizontalAlign="Right"></ItemStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="td" vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
