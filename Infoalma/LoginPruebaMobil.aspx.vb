Imports System.Data
Imports System.Web.Security
Imports Infodepsa

Public Class LoginPruebaMobil
    Inherits System.Web.UI.Page
    Private objAcceso As Library.AccesoDB.Acceso
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objUsuario As Library.AccesoDB.Usuario = New Library.AccesoDB.Usuario
#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnEntrar As System.Web.UI.WebControls.Button
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtContrase�a As System.Web.UI.WebControls.TextBox
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtUsuario As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblCuerpo As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    'Protected WithEvents cboOpcion As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblIP As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Introducir aqu� el c�digo de usuario para inicializar la p�gina
        Response.Expires = -1000
        Me.lblMensaje.Text = ""
        Me.lblCuerpo.Text = ""
        Me.lblTitulo.Text = ""
        Dim intCaduco As Integer
        intCaduco = Request.QueryString("Caduco")
        If Session.Item("strMensaje") <> Nothing Then
            pr_IMPR_MENS(Session.Item("strMensaje"))
            Session.Remove("strMensaje")
        End If
        If intCaduco = 1 Then
            Me.lblTitulo.Text = "Tu sesi�n ha caducado"
            Me.lblCuerpo.Text = ""
        ElseIf intCaduco = 2 Then
            Me.lblTitulo.Text = "Tu sesi�n ha finalizado correctamente"
            Me.lblCuerpo.Text = "Gracias por realizar tus operaciones por este medio"
        End If

    End Sub


    Private Overloads Sub btnEntrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEntrar.Click
        Try
            objAcceso = New Library.AccesoDB.Acceso
            Dim strContrase�a As String
            'strContrase�a = Library.AccesoBL.Funciones.EncryptString(Me.txtContrase�a.Text.Trim, "���_[]0")
            With objAcceso
                If .gInicioSesionMobil(Me.txtUsuario.Text.Trim, "863532032240960") Then
                    'Session.Add("IdUsuario", .strIdUsuario)
                    'Session.Add("IdTipoUsuarioFiles", .strTipoUsuario)
                    'Session.Add("UsuarioLogin", Me.txtUsuario.Text.Trim)
                    'Session.Add("PeriodoAnual", Date.Now.Year)
                    'Session.Add("nom_user", .strNombre)
                    'Session.Add("dir_emai", .strDirEmai)
                    'Session.Add("cod_sist", "SMIF0002")
                    'Session.Add("NroCertificado", .strNroCertificado)
                    'FormsAuthentication.Initialize()
                    'FormsAuthentication.RedirectFromLoginPage(Session.Item("IdUsuario"), False)
                    'PoliticaCache()
                    'Response.Redirect("Cliente.aspx?sPagina=", False)
                    ''Response.Redirect("Cliente.aspx?sPagina=" & Me.cboOpcion.SelectedValue, False)
                    'Session.Add("cboOpcionCerrarEncuesta", "")
                    Me.lblMensaje.Text = "Para que actualice su contrase�a, se envi� un enlace a su email registrado en el sistema, pero aseg�rese de revisar sus filtros de SPAM si no llegan."

                    Exit Sub
                Else
                    If .strResult = "N" Then
                        lblMensaje.Text = "El usuario " & Me.txtUsuario.Text & " que ingresastes no esta registrado en el sistema. Por favor, vuelve a intentarlo"
                        Me.txtUsuario.Text = ""
                        Exit Sub
                    End If

                    'If .strResult = "X" Then
                    '    lblMensaje.Text = "La contrase�a ingresada no es correcta, por favor, vuelve a intentarlo"
                    '    Dim dtIntentos As DataTable
                    '    Dim dr As DataRow
                    '    dtIntentos = Session.Item("dtIntentos")
                    '    dr = dtIntentos.NewRow
                    '    dr(0) = Me.txtUsuario.Text
                    '    dtIntentos.Rows.Add(dr)
                    '    Dim drTotal As DataRow()
                    '    Dim count As Integer
                    '    count = 0
                    '    drTotal = dtIntentos.Select("IdUsuario='" & Me.txtUsuario.Text & "'")
                    '    For Each df As DataRow In drTotal
                    '        count += 1
                    '    Next
                    '    If count >= 3 Then
                    '        objUsuario.gCambiarEstadoUsuario(.strIdUsuario, "0", "X")
                    '        Me.lblMensaje.Text = "Te has equivocado 3 veces al ingresar tu clave. Tu usuario ha sido desactivado, comunicate con el administrador"
                    '    End If
                    '    Session.Add("dtIntentos", dtIntentos)
                    '    dtIntentos.Dispose()
                    '    dtIntentos = Nothing
                    '    Exit Sub
                    'End If
                End If
            End With
        Catch ex As Exception
            lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAcceso Is Nothing) Then
            objAcceso = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
