Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports System.Text
Imports System.Data.SqlClient
Imports Infodepsa
Imports System.Data

Public Class AlmRetiro
    Inherits System.Web.UI.Page
    Private objRetiro As New clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboEstadoPedido As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroPedido As System.Web.UI.WebControls.TextBox
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkNuevo As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents cboTipRetiro As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_LINEA") Then
            Try
                Me.lblOpcion.Text = "Retiros en L�nea"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_LINEA"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Dim dttFecha As DateTime
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    dttFecha = Date.Today.Subtract(TimeSpan.FromDays(2))
                    Dia = "00" + CStr(dttFecha.Day)
                    Mes = "00" + CStr(dttFecha.Month)
                    Anio = "0000" + CStr(dttFecha.Year)
                    Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Bindatagrid()
                    Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
                    If Session.Item("strNroRetiro") <> Nothing Then
                        pr_IMPR_MENS("Se Gener� la Orden de Retiro con el N�mero " + Session.Item("strNroRetiro"))
                        Session.Remove("strNroRetiro")
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dt As DataTable
            'dt = objRetiro.gCNGetBuscarRetiros(Session("CoEmpresa"), Session.Item("IdSico"), Me.cboEstadoPedido.SelectedValue, _
            '                               Me.txtNroPedido.Text, Me.txtFechaInicial.Value, Me.txtFechaFinal.Value, Me.cboTipRetiro.SelectedValue)
            Me.dgResultado.DataSource = dt
            Me.dgResultado.DataBind()
            Session.Add("dtMovimiento", dt)
            If Session("IdTipoEntidad") = "01" Then
                Me.hlkNuevo.Text = ""
                dgResultado.Columns(10).Visible = False
            Else
                dgResultado.Columns(11).Visible = False
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Public Sub btnRechazar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dtMail As New DataTable
            Dim strEmails As String
            Dim imgEditar As Button = CType(sender, Button)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Dim objUsuario As Usuario = New Usuario

            dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(13).Text(), dgi.Cells(8).Text())
            If dtMail.Rows.Count > 0 Then
                For i As Integer = 0 To dtMail.Rows.Count - 1
                    If dtMail.Rows(i)("EMAI") <> "" Then
                        strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                    End If
                Next
            End If

            objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                    "Solicitud de rechazo para Retiro " & dgi.Cells(8).Text() & " - " & Session.Item("NombreEntidad"),
                    "Estimados Se�ores:<br><br>" &
                    "El usuario " & Session.Item("UsuarioLogin") & " solicit� el rechazo del Retiro " & dgi.Cells(8).Text() & "  N�mero: <STRONG><FONT color='#330099'> " & dgi.Cells(12).Text() &
                    "</FONT></STRONG><br><br>Fecha Emisi�n Retiro: <STRONG><FONT color='#330099'> " & dgi.Cells(4).Text() & "</FONT></STRONG><br>" &
                    "Cliente: <STRONG><FONT color='#330099'>" & Session.Item("NombreEntidad") & "</FONT></STRONG>" &
                    "<br><br> Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")

            pr_IMPR_MENS("Se solicit� correctamente ")
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                                        Session.Item("NombreEntidad"), "C", "RETIROS_LINEA", "RECHAZAR RETIRO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
        Me.dgResultado.DataBind()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Factura.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = CType(Session.Item("dtMovimiento"), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(7).Text = "&nbsp;" Then
                    CType(e.Item.FindControl("btnRechazar"), Button).Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de solicitar el rechazo del retiro n�mero " & e.Item.DataItem("DOR") & "?')== false) return false;")
                    e.Item.BackColor = System.Drawing.Color.FromName("#f5dc8c")
                Else
                    CType(e.Item.FindControl("btnRechazar"), Button).Visible = False
                End If

                If e.Item.Cells(8).Text = "Simple" Then
                    CType(e.Item.FindControl("btnAutorizarRetiro"), Button).Visible = False
                End If

                If e.Item.Cells(14).Text = "S" Then
                    CType(e.Item.FindControl("btnAutorizarRetiro"), Button).Enabled = False
                Else
                    If e.Item.Cells(8).Text = "ADUANERO" And e.Item.Cells(17).Text = Session.Item("IdUsuario") Then
                        CType(e.Item.FindControl("btnAutorizarRetiro"), Button).Enabled = True
                    Else
                        CType(e.Item.FindControl("btnAutorizarRetiro"), Button).Enabled = False
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Public Sub btnAutorizarRetiro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Try
            Dim dtMail As New DataTable
            Dim dtRetiro As DataTable
            Dim dtRetiroUsu As DataTable
            Dim strEmails As String
            Dim imgEditar As Button = CType(sender, Button)
            Dim dgi As DataGridItem
            Dim strScript As String
            Dim UsuaModifcado As String
            Dim strCodUnidad As String
            Dim strNroRetiro As String
            Dim strNombreCliente As String
            Dim strNombreReporte As String
            Dim nIM_TOTA As Decimal
            Dim nIM_UNIT As Decimal
            Dim nNU_UNID_RETI As Decimal
            Dim sCO_MONE As String
            Dim sCO_UNID As String
            Dim CO_MODA_MOVI As String
            Dim CO_USUA_MODI As String
            Dim objUsuario As Usuario = New Usuario

            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            strCodUnidad = dgi.Cells(15).Text()
            strNroRetiro = dgi.Cells(12).Text()

            dtRetiro = objRetiro.gCNCrearRetiroAprobado(Session.Item("CoEmpresa"), strCodUnidad, "DOR", strNroRetiro)
            For i As Integer = 0 To dtRetiro.Rows.Count - 1
                CO_USUA_MODI = dtRetiro.Rows(i)("CO_USUA_MODI")
            Next
            Dim strCadenaConexion As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
            Dim objConexion As SqlConnection
            Dim objTransaccion As SqlTransaction
            objConexion = New SqlConnection(strCadenaConexion)
            objConexion.Open()
            objTransaccion = objConexion.BeginTransaction

            If objRetiro.gCNUpdRetiro(Session.Item("CoEmpresa"), strCodUnidad, Left(Session.Item("UsuarioLogin"), 8), "DOR", dgi.Cells(12).Text(), objTransaccion).ToString = "UsuarioAutorizo" Then
                pr_IMPR_MENS("Ya ha sido aprobado por el usuario " & CO_USUA_MODI)
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                objConexion.Close()
                Bindatagrid()
                Exit Sub
            End If

            If objRetiro.gCNUpdRetiro(Session.Item("CoEmpresa"), strCodUnidad, Left(Session.Item("UsuarioLogin"), 8), "DOR", dgi.Cells(12).Text(), objTransaccion).ToString = "DatosIncompletos" Then
                pr_IMPR_MENS("Faltan Datos para la Aprobaci�n ")
                objTransaccion.Rollback()
                objTransaccion.Dispose()
                objConexion.Close()
                Bindatagrid()
                Exit Sub
            Else
                objTransaccion.Commit()
                objTransaccion.Dispose()
                objConexion.Close()
                '**************** Reporte de Aprobacion***************************
                strNombreReporte = strPath & strCodUnidad & strNroRetiro & ".PDF"
                strNombreCliente = objRetiro.GetReporteRetiroAduanero(Session.Item("CoEmpresa"), "DOR", strNroRetiro, Session.Item("IdSico"), CType(dtRetiro, DataTable), strPathFirmas, strNombreReporte, Session.Item("UsuarioLogin"), True, Session.Item("IdSico"), "", "S")

                '**************** Email  ******************************
                dtMail = objUsuario.gGetEmailAprobadores("00000001", dgi.Cells(13).Text(), dgi.Cells(8).Text())
                If dtMail.Rows.Count > 0 Then
                    For i As Integer = 0 To dtMail.Rows.Count - 1
                        If dtMail.Rows(i)("EMAI") <> "" Then
                            strEmails = strEmails & dtMail.Rows(i)("EMAI") & ","
                        End If
                    Next
                End If
                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", System.Configuration.ConfigurationManager.AppSettings.Item("EmailDocumentaria") & "; " & Session.Item("dir_emai") & "; " & strEmails,
                                  "Se Autoriz� el Retiro " & dgi.Cells(8).Text() & " Nro " & dgi.Cells(12).Text(),
                                  "Estimados Se�ores:<br><br>" &
                                  "El usuario " & Session.Item("UsuarioLogin") & "  autoriz� el Retiro " & dgi.Cells(8).Text() & " N�mero: <STRONG><FONT color='#330099'>" & dgi.Cells(12).Text() &
                                  "</FONT></STRONG><br><br>Fecha Emisi�n Retiro: <STRONG><FONT color='#330099'>" & dgi.Cells(4).Text() & "</FONT></STRONG><br>" &
                                  "Cliente:<STRONG><FONT color='#330099'> " & dgi.Cells(1).Text() & "</FONT></STRONG>" &
                                  "<br>Atte.<br><br>S�rvanse ingresar al AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreReporte)

                pr_IMPR_MENS("Se aprob� correctamente")
                Bindatagrid()
            End If
        Catch ex As Exception
            pr_IMPR_MENS("ERROR " + ex.Message)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
