<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DocumentoRetiroPDF.aspx.vb" Inherits="DocumentoRetiroPDF" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>DocumentoPDF</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles/Styles.css" type="text/css" rel="stylesheet">
   <script src="JScripts/jquery.min.cache.js"></script>
    <script src="JScripts/jquery-ui.min.cache.js"></script>
        <link rel="stylesheet" type="text/css" href="Styles/jquery-ui.cache.css">
  <%--   <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
     <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />--%>



    <!--<script language="VBScript">
			Sub SubmitForm(intVar)
					mensaje = msgbox ( "Desea rechazar el retiro" ,vbyesno,"Confirmación")
					if mensaje = vbyes then
						frmArchivo.action = "DocumentoFirmadoRetiro.aspx?var=" & intVar & "&Pagina=1"
						frmArchivo.submit
						else		
						exit Sub
					end if
			End Sub
			Sub SubmitForm2(intVar)
			    frmArchivo.action = "DocumentoFirmadoRetiro.aspx?var=" & intVar & "&Pagina=1"
				frmArchivo.submit
		    End Sub
		</script>-->
    <script language="javascript">
        var $dialog = null;
        jQuery.showModalDialog = function (options) {

            var defaultOptns = {
                url: null,
                dialogArguments: null,
                height: 'auto',
                width: 'auto',
                position: 'top',
                resizable: false,
                scrollable: false,
                onClose: function () { },
                returnValue: null,
                doPostBackAfterCloseCallback: false,
                postBackElementId: null
            };

            var fns = {
                close: function () {
                    opts.returnValue = $dialog.returnValue;
                    $dialog = null;
                    opts.onClose();
                    if (opts.doPostBackAfterCloseCallback) {
                        postBackForm(opts.postBackElementId);
                    }
                },
                adjustWidth: function () { $frame.css("width", "100%"); }
            };

            // build main options before element iteration
            var opts = $.extend({}, defaultOptns, options);
            var $frame = $('<iframe id="iframeDialog" />');

            if (opts.scrollable)
                $frame.css('overflow', 'auto');

            $frame.css({
                'padding': 0,
                'margin': 0,
                'padding-bottom': 2
            });

            var $dialogWindow = $frame.dialog({
                autoOpen: true,
                modal: true,
                width: opts.width,
                height: opts.height,
                resizable: opts.resizable,
                position: opts.position,
                overlay: {
                    opacity: 0.5,
                    background: "black"
                },
                close: fns.close,
                resizeStop: fns.adjustWidth
            });

            $frame.attr('src', opts.url);
            fns.adjustWidth();

            $frame.load(function () {
                if ($dialogWindow) {
                    var maxTitleLength = 50;
                    var title = $(this).contents().find("title").html();

                    if (title.length > maxTitleLength) {
                        title = title.substring(0, maxTitleLength) + '...';
                    }
                    $dialogWindow.dialog('option', 'title', title);
                }
            });

            $dialog = new Object();
            $dialog.dialogArguments = opts.dialogArguments;
            $dialog.dialogWindow = $dialogWindow;
            $dialog.returnValue = null;
        }

        function postBackForm(targetElementId) {
            var theform;
            theform = document.forms[0];
            theform.__EVENTTARGET.value = targetElementId;
            theform.__EVENTARGUMENT.value = "";
            theform.submit();
        }

        function openWindow(strfirma) {
            var Argumentos = new Object();
            if (strfirma == '1') {
                SubmitForm2(1);
            }
            if (strfirma == '0') {
                var Argumentos = new Object();
                var url = 'Popup/LgnSolicitaFirma.aspx';
                $.showModalDialog({
                    url: url,
                    dialogArguments: null,
                    heigh: 110,
                    width: 360,
                    scrollable: false,
                    onClose: function () {
                        Argumentos = this.returnValue;
                        if (Argumentos == null) {
                            window.alert('Usted a cancelado el proceso de firma!');
                        }
                        else { //quiere decir que se ha devuelto una lista de contactos			    
                            document.getElementById('txtContrasena').value = Argumentos[0];
                            document.getElementById('txtNuevaContrasena').value = Argumentos[1];
                            SubmitForm2(1);
                        }
                    }
                });

                //prueba de dialogo
                //$(document).ready(function () {
                //    $('#cmdFirmar').live('click', function (e) {
                //        e.preventDefault();
                //        var page = 'Popup/LgnSolicitaFirma.aspx'
                //        var pagetitle = 'Clave'
                //        var $dialog = $('<div></div>')
                //            .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                //            .dialog({
                //                autoOpen: false,
                //                modal: true,
                //                height: 625,
                //                width: 500,
                //                title: pagetitle
                //            });
                //        $dialog.dialog('open');
                //    });
                //});
                //fin prueba
                //Argumentos = window.showModalDialog("Popup/LgnSolicitaFirma.aspx", Argumentos, "dialogHeight:40px; dialogWidth:310px; edge:Raised; center:Yes; help:No; resizable:No; status:No"); 
                //if(Argumentos == null)
                //{
                //	window.alert('Usted a cancelado el proceso de firma!');
                //}
                //else
                //{
                //	document.getElementById('txtContrasena').value = Argumentos[0];
                //	document.getElementById('txtNuevaContrasena').value = Argumentos[1];
                //	SubmitForm2(1);
                //}
            }
        }
    </script>
    <script language="javascript">		
        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }
        function ObtenerP(nParametro) {
            var callingURL = document.location.href;
            var cgiString = callingURL.substring(callingURL.indexOf('?') + 1, callingURL.length);
            nParametro -= 1;
            var col_array = cgiString.split("&");
            var part_num = 0;
            while (part_num < col_array.length) {
                if (part_num == nParametro)
                    return col_array[part_num];
                else
                    part_num += 1;
            }
            return 0;
        }
        function Cerrar() {
            var NroPagina;
            var strEstado;
            document.frmArchivo.btnCerrar.disabled = true;
            NroPagina = document.all["txtNroPagina"].value;
            strEstado = document.all["txtEstado"].value;
            window.location = 'DocumentoAprobacionRetiro.aspx?Estado=' + strEstado + '&var2=1';
        }
        function noboton(ev) {
            if (document.all) ev = event;
            if (ev.button & 2)
                alert("Botón inactivo.");
        }
        function bloquear() {
            document.onmousedown = noboton;
        }

        function SubmitForm(intVar) {
            if (confirm("Desea rechazar el retiro")) {
                frmArchivo.action = 'DocumentoFirmadoRetiro.aspx?var=' + intVar + '&Pagina=1';
                frmArchivo.submit();
            }
        }

        function SubmitForm2(intVar) {
            frmArchivo.action = 'DocumentoFirmadoRetiro.aspx?var=' + intVar + '&Pagina=1';
            frmArchivo.submit();
        }
    </script>
</head>
<body onload="bloquear()" bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0"
    bgcolor="#f0f0f0">
    <form id="frmArchivo" name="frmArchivo" method="post">
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <div id="cntFirmas" runat="server"></div>
                    <table id="Table2" cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="90%">
                                <asp:Label ID="lblFirma" runat="server" CssClass="error"></asp:Label>
                                <asp:Literal ID="LtrFirma" runat="server"></asp:Literal></td>
                            <td align="center">
                                <asp:Literal ID="ltrFirmar1" runat="server"></asp:Literal></td>
                            <td align="center">
                                <asp:Literal ID="ltrFirmar2" runat="server"></asp:Literal></td>
                            <td align="center">
                                <input class="btn" id="btnCerrar" style="width: 80px" onclick="Cerrar()" type="button"
                                    value="Cerrar" name="btnCerrar">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4"></td>
                        </tr>
                    </table>
                    <div style="display: none">
                        <input id="txtArchivo_pdf" name="txtArchivo_pdf" runat="server">
                        <input id="txtArchivo_pdf_firmado" name="txtArchivo_pdf_firmado" runat="server">
                        <input id="txtNumSerie" name="txtNumSerie" runat="server">
                        <input id="txtNroPagina" name="txtNroPagina" runat="server"><input id="txtEstado" name="txtEstado" runat="server"><input id="txtTimeStamp" name="txtTimeStamp" runat="server"><input id="txtContrasena" name="txtContrasena" runat="server"><input id="txtNuevaContrasena" name="txtNuevaContrasena" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="ltrVisor" runat="server"></asp:Literal></td>
            </tr>
        </table>
    </form>
</body>
</html>
