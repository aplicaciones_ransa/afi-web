Imports System.Data.SqlClient
Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports System.IO
Imports System.Text
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa

Public Class WarSolicitudWarrant
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objAccesoWeb As clsCNAccesosWeb
    Private objReportes As Reportes = New Reportes
    Dim strAlmacen As String
    Private strPDFPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
    'Protected WithEvents cboTipoTitulo As System.Web.UI.WebControls.DropDownList
    Private strPathFirmas = ConfigurationManager.AppSettings.Item("RutaFirmas")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents FilePDF As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacenes As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnStock As System.Web.UI.WebControls.Button
    'Protected WithEvents trAlmacen As System.Web.UI.HtmlControls.HtmlTableRow
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents btnGrabar As RoderoLib.BotonEnviar
    'Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    'Protected WithEvents txtHoraIngreso As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents chkReemplazo As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function

    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "38") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "38"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Ingreso de Solicitud de Warrants"
                loadFinanciador()
                loadAlmacenes()
                If Me.trAlmacen.Visible = False Then
                    Me.cboAlmacen.Enabled = True
                End If
                'Me.tblAdjuntar.Visible = False
                loadAlmacen()
                BindDatagrid()
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                Dim dttFecha As DateTime
                dttFecha = Date.Today
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de registrar la solicitud de warrants?')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadAlmacenes()
        Dim dtEntidad As DataTable
        Try
            Me.cboAlmacenes.Items.Clear()
            dtEntidad = objEntidad.gGetNombreAlmacen(Session.Item("IdSico"))
            If dtEntidad.Rows.Count = 1 Then
                Me.trAlmacen.Visible = False
            Else
                Me.trAlmacen.Visible = True
            End If
            Me.cboAlmacenes.Items.Add(New ListItem("-------------------------------------------------Seleccione----------------------------------------------------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboAlmacenes.Items.Add(New ListItem(dr("Nombre").ToString(), dr("Codigo").ToString()))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadFinanciador()
        Dim dtEntidad As New DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            'dtEntidad = objEntidad.gGetEntidadesSICO("02")
            dtEntidad = objEntidad.gGetEntidades("02", "W")
            Me.cboFinanciador.Items.Add(New ListItem("-------------------------------------------------Seleccione----------------------------------------------------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
                'Me.cboFinanciador.Items.Add(New ListItem(dr("nombre").ToString(), dr("codigo")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "Error al llenar entidades: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadAlmacen()
        Dim dtEntidad As DataTable
        Try
            Me.cboAlmacen.Items.Clear()
            dtEntidad = objEntidad.gGetAlmacen(Session.Item("IdSico"))
            Me.cboAlmacen.Items.Add(New ListItem("-------------------------------------------------Seleccione----------------------------------------------------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboAlmacen.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            Dim j As Integer = 0
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            CargarTabla()
            dt.Rows.RemoveAt(dgi.ItemIndex)
            For Each dr1 As DataRow In dt.Rows
                dt.Rows(j).AcceptChanges()
                dt.Rows(j).BeginEdit()
                'dt.Rows(j).Item(0) = j + 1
                dt.Rows(j).EndEdit()
                j += 1
            Next
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        BindDatagrid()
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim dr As DataRow
        CargarTabla()
        dr = dt.NewRow
        dr(0) = ""
        dr(1) = ""
        dr(2) = "0"
        dr(3) = "0"
        dr(4) = "0"
        dr(5) = "S"
        dr(6) = "0"
        dt.Rows.Add(dr)
        Me.dgdResultado.DataSource = dt
        Me.dgdResultado.DataBind()
    End Sub

    Private Sub CargarTabla()
        Dim dr As DataRow
        Dim dgItem As DataGridItem
        dt.Rows.Clear()
        For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
            dgItem = Me.dgdResultado.Items(i)
            dr = dt.NewRow
            dr(0) = CType(dgItem.FindControl("txtDescripcion"), TextBox).Text
            dr(1) = CType(dgItem.FindControl("txtUnidad"), TextBox).Text
            dr(2) = CType(dgItem.FindControl("txtstock"), TextBox).Text
            dr(3) = CType(dgItem.FindControl("txtCantidad"), TextBox).Text
            dr(4) = CType(dgItem.FindControl("txtprecio"), TextBox).Text
            dr(5) = CType(dgItem.FindControl("cboMoneda"), DropDownList).SelectedValue
            dr(6) = CType(dgItem.FindControl("txtImporte"), TextBox).Text
            dt.Rows.Add(dr)
        Next i
    End Sub

    Private Property dt() As DataTable
        Get
            Return (ViewState("dt"))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dt") = Value
        End Set
    End Property

    Protected Overrides Sub Finalize()
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "38", "GRABA SOLICITUD WARRANT", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Dim strNombre As String
            Dim strResultado As String
            Dim strResultado1 As String
            Dim strResultado2 As String
            Dim strMoneda As String
            Dim strPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
            Dim NombreReporte As String
            Dim FlujoBinario As System.IO.Stream
            Dim Contenido As Byte()
            Dim intContador As Integer
            Dim strTipoTitulo As String

            If Me.cboAlmacenes.SelectedItem.Value = "OTROS" Or Me.trAlmacen.Visible = False Then
                strAlmacen = Me.cboAlmacen.SelectedItem.Value
            Else
                strAlmacen = Me.cboAlmacenes.SelectedItem.Value
            End If

            If Me.txtFechaInicial.Value = "" Then pr_IMPR_MENS("Debe ingresar la fecha de solicitud") : Exit Sub
            If Me.txtHoraIngreso.Value = "" Then pr_IMPR_MENS("Debe ingresar la Hora de Solicitud") : Exit Sub

            If Me.cboTipoTitulo.SelectedValue = "0" Then
                pr_IMPR_MENS("Debe seleccionar un tipo de t�tulo")
                Return
            Else
                strTipoTitulo = Me.cboTipoTitulo.SelectedValue
            End If
            Dim strReemplazo As String
            If Me.chkReemplazo.Checked Then
                strReemplazo = "S"
            Else
                strReemplazo = ""
            End If

            Dim dgItem As DataGridItem
            For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
                dgItem = Me.dgdResultado.Items(i)
                If CType(dgItem.FindControl("chkSel"), CheckBox).Checked Then
                    If CType(dgItem.FindControl("txtDescripcion"), TextBox).Text = "" Then
                        pr_IMPR_MENS("Debe ingresar un descripci�n de �tem") : Exit Sub
                    End If
                    If CType(dgItem.FindControl("txtCantidad"), TextBox).Text = "0" Or CType(dgItem.FindControl("txtCantidad"), TextBox).Text = "" Then
                        pr_IMPR_MENS("Debe ingresar la cantidad solicitada") : Exit Sub
                    End If
                    strMoneda = CType(dgItem.FindControl("cboMoneda"), DropDownList).SelectedValue
                    intContador += 1
                End If
            Next

            strNombre = System.IO.Path.GetFileName(Me.FilePDF.PostedFile.FileName)
            NombreReporte = strPath & strNombre

            If intContador = 0 Then
                pr_IMPR_MENS("Debe seleccionar por lo menos un �tem") : Exit Sub
            End If

            If strNombre <> "" Then
                FilePDF.PostedFile.SaveAs(NombreReporte)
                FlujoBinario = Request.Files(0).InputStream
                ReDim Contenido(FlujoBinario.Length)
                FlujoBinario.Read(Contenido, 0, Convert.ToInt32(FlujoBinario.Length))
            Else
                Contenido = New Byte() {&H12, &HFF}
                NombreReporte = ""
            End If

            objDocumento.InsSolicitudWarrants(Session.Item("IdSico"), strAlmacen, Me.cboFinanciador.SelectedValue,
                                            Session.Item("IdUsuario"), Contenido, 0, strMoneda, Me.txtFechaInicial.Value, Me.txtHoraIngreso.Value, "", "", strTipoTitulo)
            strResultado = objDocumento.strResultado
            If strResultado = "X" Then pr_IMPR_MENS("No se pudo insertar los datos del cliente") : Exit Sub

            If intContador > 0 Then
                Dim dtSolDet As New DataTable
                For i As Integer = 0 To Me.dgdResultado.Items.Count - 1
                    dgItem = Me.dgdResultado.Items(i)
                    If CType(dgItem.FindControl("chkSel"), CheckBox).Checked Then
                        dtSolDet = objDocumento.InsSolicitudWarrantsDetalle(strResultado,
                                    CType(dgItem.FindControl("txtstock"), TextBox).Text,
                                    UCase(CType(dgItem.FindControl("txtDescripcion"), TextBox).Text),
                                    CType(dgItem.FindControl("txtCantidad"), TextBox).Text,
                                    CType(dgItem.FindControl("cboMoneda"), DropDownList).SelectedValue,
                                    CType(dgItem.FindControl("txtprecio"), TextBox).Text,
                                    CType(dgItem.FindControl("txtImporte"), TextBox).Text,
                                    CType(dgItem.FindControl("txtUnidad"), TextBox).Text)
                    End If
                Next
                objReportes.LstrNombreExternoPDF = NombreReporte

                objReportes.GeneraPDFWarrant(dtSolDet.Rows(0)(0), strPDFPath, strPathFirmas, False, False,
                                            dtSolDet.Rows(0)(1), Session.Item("IdUsuario"), "0", strReemplazo)
                If objReportes.LstrMensaje <> "0" Then
                    Me.lblError.Text = objReportes.LstrMensaje
                    Exit Sub
                End If
                NombreReporte = strPDFPath & dtSolDet.Rows(0)(2)
            End If
            Dim dtCorreos As DataTable = objDocumento.EnviarFueraLimite(Session.Item("IdSico"), strAlmacen, strResultado)
            'Dim strEmailsConAdjunto As String = ""
            'If dtCorreos.Rows.Count > 0 Then
            '    For i As Integer = 0 To dtCorreos.Rows.Count - 1
            '        strEmailsConAdjunto = strEmailsConAdjunto & dtCorreos.Rows(i)("DIR_EMAI") & ","
            '    Next
            'End If
            objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                                 System.Configuration.ConfigurationManager.AppSettings.Item("EmailInspectoria"),
                                 "Solicitud de Warrant - " & Session.Item("NombreEntidad"),
                                 "Se ha registrado la solicitud de warrants Nro: <STRONG><FONT color='#330099'> " & strResultado & "<br> </FONT></STRONG>" &
                                 "Generado por: <STRONG><FONT color='#330099'> " & UCase(Session.Item("UsuarioLogin")) & "</FONT></STRONG><br>" &
                                 "<br><br>Atte.<br><br>S�rvanse ingresar a la web AFI haciendo click sobre el siguiente vinculo: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", NombreReporte)
            pr_IMPR_MENS("Se gener� la solicitud de Warrant n�mero " & strResultado & ", est� en Registrados")
            dt.Clear()
            BindDatagrid()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub cboAlmacenes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacenes.SelectedIndexChanged
        If cboAlmacenes.SelectedItem.Value = "OTROS" Then
            Me.cboAlmacen.Enabled = True
        Else
            Me.cboAlmacen.Enabled = False
            BindDatagrid()
        End If
    End Sub

    Private Sub dgdResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdResultado.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Aqui van tus acciones por cada fila de datos....
            Dim txtCant As New TextBox
            Dim txtPre As New TextBox
            Dim txtTot As New TextBox
            Dim txtStock As New TextBox
            Dim cboModena As New DropDownList
            txtCant = CType(e.Item.FindControl("txtCantidad"), TextBox)
            txtPre = CType(e.Item.FindControl("txtprecio"), TextBox)
            txtTot = CType(e.Item.FindControl("txtImporte"), TextBox)
            txtStock = CType(e.Item.FindControl("txtstock"), TextBox)
            cboModena = CType(e.Item.FindControl("cboMoneda"), DropDownList)

            If Session.Item("CodMoneda") <> Nothing Then
                cboModena.SelectedValue = Session.Item("CodMoneda")
            End If
            If txtStock.Text = 0 Then
                txtCant.Text = 0
                txtPre.Text = 0
                txtTot.Text = 0
            End If
            txtCant.Attributes.Add("onkeypress", "Javascript:validardecimal();")
            txtPre.Attributes.Add("onkeypress", "Javascript:validardecimal();")
            txtStock.Attributes.Add("onkeypress", "Javascript:validardecimal();")
            txtTot.Attributes.Add("onkeypress", "Javascript:validardecimal();")
            txtCant.Attributes.Add("onkeyup", "CalcAmount('" + txtCant.ClientID + "','" + txtPre.ClientID + "','" + txtTot.ClientID + "','" + txtStock.ClientID + "')")
            txtPre.Attributes.Add("onkeyup", "CalcAmount('" + txtCant.ClientID + "','" + txtPre.ClientID + "','" + txtTot.ClientID + "','" + txtStock.ClientID + "')")
        End If
        If e.Item.ItemType = ListItemType.Header Then
            Dim ck1 As CheckBox = CType(e.Item.FindControl("chkTodos"), CheckBox)
            If Not IsNothing(ck1) Then
                ck1.Attributes.Add("onclick", "Todos(this, 'chkSel');")
            End If
        End If
    End Sub

    Public Sub cboMoneda_select(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim imgEditar As DropDownList = CType(sender, DropDownList)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("CodMoneda", CType(dgi.FindControl("cboMoneda"), DropDownList).SelectedValue)
            For Each dgi In dgdResultado.Items
                CType(dgi.FindControl("cboMoneda"), DropDownList).SelectedValue = Session.Item("CodMoneda")
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    'Private Sub txtImporteTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If txtImporteTotal.Value = "" Then
    '        Me.btnAgregar.Enabled = False
    '        Me.btnBuscar.Enabled = False
    '    Else
    '        Me.btnAgregar.Enabled = True
    '        Me.btnBuscar.Enabled = True
    '    End If
    'End Sub

    'Private Sub rbAdjuntar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If rbAdjuntar.Checked Then
    '        Me.tblAdjuntar.Visible = True
    '        Me.FilePDF.Disabled = False
    '        Me.cboMonedaPrincipal.Enabled = True
    '        Me.txtImporteTotal.Disabled = False
    '        Me.btnAgregar.Visible = False
    '        Me.btnBuscar.Visible = False
    '        Me.dgdResultado.Visible = False
    '    End If
    'End Sub

    'Private Sub rbGrilla_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If rbGrilla.Checked Then
    '        Me.tblAdjuntar.Visible = False
    '        Me.FilePDF.Disabled = True
    '        Me.cboMonedaPrincipal.Enabled = False
    '        Me.txtImporteTotal.Disabled = True
    '        Me.btnAgregar.Visible = True
    '        Me.btnBuscar.Visible = True
    '        Me.dgdResultado.Visible = True
    '        BindDatagrid()
    '    End If
    'End Sub

    Private Sub btnStock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStock.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""
        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Stock.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        dg.DataSource = objDocumento.gGetBusquedaDocumentosSolicitador(Session("IdSico"), "")
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Private Sub BindDatagrid()
        Try
            dt = Nothing
            dt = New DataTable
            If Me.cboAlmacenes.SelectedItem.Value = "OTROS" Then
                strAlmacen = Me.cboAlmacen.SelectedItem.Value
            Else
                strAlmacen = Me.cboAlmacenes.SelectedItem.Value
            End If
            dt = objDocumento.gGetBusquedaDocumentosSolicitador(Session("IdSico"), strAlmacen)
            Me.dgdResultado.DataSource = dt
            Me.dgdResultado.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub
End Class
