Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Public Class Prueba
    Inherits System.Web.UI.Page
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objEntidad As Entidad = New Entidad
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objLiberacion As Liberacion = New Liberacion
    Private objReportes As Reportes = New Reportes
    Private strConn As String = ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    Private strAGD As String = System.Configuration.ConfigurationManager.AppSettings("CodTipoEntidadAGD")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPDFPath2 As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strEndosoPath As String = ConfigurationManager.AppSettings.Item("RutaEndoso")
    Private strSessMensaje As String
    Private strSessResultado As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents Button1 As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        EnvioMail()
    End Sub

    Private Sub EnvioMail()
        'objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "francisco.uni@gmail.com", "Contrase�a de Acceso", "Datos de acceso al Sistema AFI " &
        '                 ":<br><br>Usuario: JFRANCISCO<br>Contrase�a: 1234567<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Sistema AFI</A>" &
        '                 "<br>Cualquier consulta sobre la aplicaci�n escribir a soporte@almaperu.com.pe", "E:\SOPORTE\NCRF51-0000153.PDF")
        '\\10.66.3.16\Documentos\00001783.PDF
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim strPath As String
        Dim strNombrePDF As String
        Dim dtDetalle As DataTable
        Dim strNroDoc As String = txtwarr.Text ' "00089408" 'numero de warrant
        Dim strNroLib As String = txtlib.Text ' "00000354312" 'numero de liberacion
        Dim strCodUnidad As String = "001" '
        Dim strCod_TipOtro As String = txttipowarr.Text ' "01" 'warrant, wip
        Dim strTipoDoc As String = txttipolib.Text ' "02"  'tipo liberacion simple 02, contable 13
        Dim strPeriodoAnual As String = txtanio.Text ' "2019"
        Try
            'If objDocumento.gGetEstadoImpresion(6405, objTrans) = False Then 'ok(sin sessiones internas)
            strPath = strEndosoPath
                strNombrePDF = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
                dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCodUnidad, strNroLib,
                                                                        strCod_TipOtro, strNroDoc, objTrans) 'ok(sin sessiones internas)
                '=== Generar Liberaci�n PDF
                objLiberacion.GetReporteLiberacionFirmas(strNombrePDF, strEmpresa, strCodUnidad, "DOR",
                                                    dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle,
                                                    strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"),
                                                    dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"),
                                                    strTipoDoc, objTrans, strPathFirmas) 'ok(sin sessiones internas)
            'GrabaDocumento(strNombrePDF, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, objTrans) 'ok(sin sessiones internas)
            dtDetalle.Dispose()
                strTipoDoc = "02"
            'End If
            objTrans.Commit()
            objTrans.Dispose()
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Session.Add("Mensaje", ex.Message)
            Response.Write(ex.Message)
            'Return ex.Message
        Finally
            objConexion = Nothing
        End Try
    End Sub

    Private Sub Grabapdf()
        'objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", "francisco.uni@gmail.com", "Contrase�a de Acceso", "Datos de acceso al Sistema AFI " &
        '                 ":<br><br>Usuario: JFRANCISCO<br>Contrase�a: 1234567<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Sistema AFI</A>" &
        '                 "<br>Cualquier consulta sobre la aplicaci�n escribir a soporte@almaperu.com.pe", "E:\SOPORTE\NCRF51-0000153.PDF")
        '\\10.66.3.16\Documentos\00001783.PDF
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Dim strPath As String
        Dim strNombrePDF As String
        Dim dtDetalle As DataTable
        Dim strNroDoc As String = txtwarr.Text ' "00089408" 'numero de warrant
        Dim strNroLib As String = txtlib.Text ' "00000354312" 'numero de liberacion
        Dim strCodUnidad As String = "001" '
        Dim strCod_TipOtro As String = txttipowarr.Text ' "01" 'warrant, wip
        Dim strTipoDoc As String = txttipolib.Text ' "02"  'tipo liberacion simple 02, contable 13
        Dim strPeriodoAnual As String = txtanio.Text ' "2019"
        Try
            'If objDocumento.gGetEstadoImpresion(6405, objTrans) = False Then 'ok(sin sessiones internas)
            strPath = strEndosoPath
            strNombrePDF = strEndosoPath & "02" & strNroDoc & strNroLib & ".pdf"
            dtDetalle = objDocumento.gGetDetalleLiberacionRegistra(strEmpresa, strCodUnidad, strNroLib,
                                                                        strCod_TipOtro, strNroDoc, objTrans) 'ok(sin sessiones internas)
            '=== Generar Liberaci�n PDF
            objLiberacion.GetReporteLiberacionFirmas(strNombrePDF, strEmpresa, strCodUnidad, "DOR",
                                                    dtDetalle.Rows(0)("NRO_LIBE"), dtDetalle.Rows(0)("COD_CLIE"), dtDetalle,
                                                    strCod_TipOtro, dtDetalle.Rows(0)("EMB_LIBE"),
                                                    dtDetalle.Rows(0)("FLG_CONT"), dtDetalle.Rows(0)("FLG_TRAS"),
                                                    strTipoDoc, objTrans, strPathFirmas) 'ok(sin sessiones internas)
            GrabaDocumento(strNombrePDF, strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, objTrans) 'ok(sin sessiones internas)
            dtDetalle.Dispose()
            strTipoDoc = "02"
            'End If
            objTrans.Commit()
            objTrans.Dispose()
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Session.Add("Mensaje", ex.Message)
            Response.Write(ex.Message)
            'Return ex.Message
        Finally
            objConexion = Nothing
        End Try
    End Sub
    Private Sub GrabaDocumento(ByVal strNombArch As String, ByVal strNroDoc As String, ByVal strNroLib As String, ByVal strPeriodoAnual As String, ByVal strTipoDoc As String, ByVal ObjTrans As SqlTransaction)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        Dim strNombre As String
        strNombre = System.IO.Path.GetFileName(strNombArch)
        FilePath = strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegEndosoFolio(strNroDoc, strNroLib, strPeriodoAnual, strTipoDoc, strNombre, Contenido, ObjTrans) 'ok(sin sessiones internas)
        fs.Close()
    End Sub
    Protected Sub btngraba_Click(sender As Object, e As EventArgs) Handles btngraba.Click
        Grabapdf()
    End Sub
    Protected Sub btndeuda_Click(sender As Object, e As EventArgs) Handles btndeuda.Click
        ConsDeuda()
    End Sub

    Private Sub ConsDeuda()
        Dim objWSSAP As wsSAP.Service
        objWSSAP = New wsSAP.Service
        Dim objDeuda As wsSAP.CREDIT_ACCOUNT()

        Dim nPorcentajeDeuda As String
        Dim sDeudaTotal As Decimal = 0
        Dim sMoneda As String = "DOL"
        Dim strCodSAP As String = Session("CodSAP")
        If strCodSAP = "" Then
            strCodSAP = "0"
        End If
        objDeuda = objWSSAP.ZBAPI_VECTOR_CRED_AS400("1095927")
        nPorcentajeDeuda = objDeuda(0).AGOTAMIENTO
        sDeudaTotal = objDeuda(0).RCVBL_VALS
        sMoneda = objDeuda(0).CURRENCY

        If Trim(sMoneda) = "USD" Or sMoneda Is Nothing Then
            sMoneda = "DOL"
        End If
        If Trim(sMoneda) = "PEN" Then
            sMoneda = "SOL"
        End If

        Dim dt As New DataTable
        Dim OBJ = New WZAPI.ServiceSoapClient()

        Dim co_clie As String
        Dim dia, mes, anio As String
        Dim vfecha As String
        Dim v30 As Decimal = "0"
        Dim v60 As Decimal = "0"
        Dim v90 As Decimal = "0"
        Dim vdvencida As Decimal = "0"
        Dim vpvencer As Decimal = "0"
        anio = DateAndTime.Year(Today)
        mes = DateAndTime.Month(Today)
        dia = DateAndTime.Day(Today)
        vfecha = anio + mes.PadLeft(2, "0"c) + dia.PadLeft(2, "0"c)

        co_clie = txtdclie.Text

        'Limpiamos Lista
        Dim Str As List(Of WZAPI.LINEITEMS) '= Nothing
        'Traer data del WS
        Str = OBJ.ZBAPI_AR_ACC_GETOPENITEMS(co_clie, vfecha)

        For Each sItem In Str
            If sItem.BSCHL = "01" Then


                If Convert.ToInt32(sItem.DSCRDT) < "0" Then
                    vpvencer = vpvencer + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)

                ElseIf Convert.ToInt32(sItem.DSCRDT) > "0" And Convert.ToInt32(sItem.DSCRDT) <= "30" Then
                    v30 = v30 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)

                ElseIf Convert.ToInt32(sItem.DSCRDT) > "30" And Convert.ToInt32(sItem.DSCRDT) <= "60" Then
                    v60 = v60 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)

                ElseIf Convert.ToInt32(sItem.DSCRDT) > "60" Then
                    v90 = v90 + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)

                End If
                If Convert.ToInt32(sItem.DSCRDT) > "0" Then
                    vdvencida = vdvencida + Convert.ToDecimal(sItem.ZZSUMSAL_USD.Trim)
                End If


            End If
        Next
        txtvencer.Text = vpvencer
        txt30.Text = v30
        txt60.Text = v60
        txt90.Text = v90
        txtdvencida.Text = vdvencida

    End Sub
End Class
