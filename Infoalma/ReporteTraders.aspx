<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ReporteTraders.aspx.vb" Inherits="ReporteTraders" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ReporteTraders</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript">

        $(function () {
            $("#txtFechaDesde").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaDesde").datepicker('show');
            });

            $("#txtFechaHasta").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFinal").click(function () {
                $("#txtFechaHasta").datepicker('show');
            });
        });

    </script>

</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table9"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="6">
                                <uc1:MenuInfo ID="MenuInfo2" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" width="100%" align="center">
                                <table id="Table20" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                        <td>
                                            <table id="Table41" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td class="text">
                                                        <asp:Label ID="lblTipEntidad" runat="server"></asp:Label></td>
                                                    <td class="text" width="1%">:</td>
                                                    <td colspan="15">
                                                        <asp:DropDownList ID="cboCliente" runat="server" CssClass="Text" AutoPostBack="True" Width="100%"></asp:DropDownList>
                                                        </td>                                                      
                                                </tr>
                                                <tr>
                                                    <td class="text">Desde</td>
                                                    <td class="text" width="1%">:</td>
                                                    <td>
                                                        <input id="txtFechaDesde" class="Text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaDesde" runat="server"></td>
                                                    <td>
                                                        <input id="btnFechaInicial" class="Text" value="..."
                                                                type="button" name="btnFecha1"></td>
                                                    <td class="text">Hasta</td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input id="txtFechaHasta" class="Text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                            maxlength="10" size="6" name="txtFechaHasta" runat="server"></td>
                                                    <td>
                                                        <input style="z-index: 0" id="btnFechaFinal" class="Text" value="..." type="button" name="btnFecha"></td>
                                                    <td class="text">Estado Traders</td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <asp:DropDownList Style="z-index: 0" ID="cboEstado" runat="server" CssClass="Text" Width="100px">
                                                            <asp:ListItem Value="T">Todos</asp:ListItem>
                                                            <asp:ListItem Value="0">Abiertos</asp:ListItem>
                                                            <asp:ListItem Value="1">Cerrados</asp:ListItem>
                                                            <asp:ListItem Value="x">Anulados</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="Text">Vapor</td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtVapor" runat="server" CssClass="text" Width="80px">
                                                        </asp:TextBox>
                                                      
                                                    </td>
                                                    <td class="Text"> Estado Warrant</td>
                                                       <td class="Text"> :</td>
                                                     <td class="Text">
                                                        <asp:DropDownList Style="z-index: 0" ID="cboEstadoWarrant" runat="server" CssClass="Text" Width="100px">
                                                            <asp:ListItem Value="0">Todos</asp:ListItem>
                                                            <asp:ListItem Value="TSW">Sin Warrant</asp:ListItem>
                                                            <asp:ListItem Value="WCS">Activo</asp:ListItem>
                                                            <asp:ListItem Value="WSS">Cancelado</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                    </tr>
                                    <tr>
                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td" valign="top" align="center">
                                <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:Button><asp:Button ID="btnExportar" runat="server" CssClass="btn" Width="80px" Text="Exportar"></asp:Button></td>
                        </tr>
                        <tr>
                            <td class="text" valign="top" align="left">Resultado:</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                    AutoGenerateColumns="False" PageSize="30" BorderWidth="1px">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle HorizontalAlign="Right" CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="NU_DOCU" HeaderText="Nro. Doc.">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Nombre" HeaderText="Cliente">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundColumn>
                                         <asp:BoundColumn DataField="DE_COND_LOCA" HeaderText="Conductor">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_DIRE_ALMA" HeaderText="Direcci&#243;n">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_VAPO" HeaderText="Vapor">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NU_TITU" HeaderText="Warrant"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="FE_INI_RECE" HeaderText="Fecha Recepci&#243;n">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DE_ESTA" HeaderText="Estado Traders">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Ver">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgVer" OnClick="imgVer_Click" runat="server" ImageUrl="Images/Ver.JPG" ToolTip="Ver registro"
                                                    CausesValidation="False"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <table style="width: 376px; height: 37px" id="Table4" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td style="height: 16px" class="td" colspan="8">Leyenda</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" width="30%"></td>
                                        <td class="Leyenda" width="20%">Ingreso Abierto.</td>
                                        <td bgcolor="#c8dcf0" width="30%"></td>
                                        <td class="Leyenda" width="30%">Ingreso Cerrado.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
