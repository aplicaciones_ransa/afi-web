<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="WarSolicitudWarrantRegistroDeta.aspx.vb" Inherits="WarSolicitudWarrantRegistro" %>

<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarSolicitudWarrants</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {
            //if(parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value))
            //  {
            //   alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
            //   document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
            //   return;
            //  }				  				  
            document.getElementById(Monto).value = formatearDecimal(parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value), 3);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }

        function formatearDecimal(numero, cantDecimales) {
            if (cantDecimales > 0) {
                var i = 0;
                var aux = 1;
                for (i = 0; i < cantDecimales; i++) {
                    aux = aux * 10;
                }
                numero = numero * aux;
                numero = Math.round(numero);
                numero = numero / aux;
            }
            return numero;
        }

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 23) { //para rel�gio de 12 horas altere o valor aqui
                alert("Horas inv�lidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("Minutos inv�lidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inv�lidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
    <script language="javascript">		
            function Todos(ckall, citem) {
                var actVar = ckall.checked;
                for (i = 0; i < Form1.length; i++) {
                    if (Form1.elements[i].type == "checkbox") {
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
    </script>
  

  
 </head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3" runat="server"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        <td align="right" width="50%">
                                <asp:Label ID="lblTipoSol" runat="server" CssClass="Titulo2"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                            <table id="Table1" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                <tr >
                                                    <td class="text">
                                                       </td>
                                                    <td class="text" colspan="3">
                                                        <table >
                                                            <tr>
                                                                <td width="20%"></td>
                                                                <td width="2%" >
                                    <asp:Button class="btn" ID="btnSol1" runat="server"  Text="1" Width="40px" BorderStyle="Solid" Height="40px" BorderColor="Silver" Font-Bold="True" Font-Size="Small" />
                                                                </td>
                                                                <td class="Titulo3" width="20%">
                                                                    <asp:Image ID="ImgCheck1" runat="server" ImageUrl="~/Images/CheckText.png" />
                                                                    <asp:Label ID="lblDeSol1" runat="server" Text="Solicitud Cabecera" CssClass="Titulo1"></asp:Label>
                                                                </td>
                                                                <td width="2%" >
                                    <asp:Button class="btn" ID="btnSol2" runat="server"  Text="2" Width="40px" BorderStyle="Solid" Height="40px" ForeColor="Black" BackColor="#ECECEC" BorderColor="Silver" Font-Bold="True" Font-Size="Small" post/>
                                                                </td>
                                                                <td class="Titulo1" width="20%">
                                                                    <asp:Image ID="ImgCheck2" runat="server" ImageUrl="~/Images/CheckText.png" />
                                                                    <asp:Label ID="lblDeSol2" runat="server" Text="Solicitud Detalle" CssClass="Titulo1"></asp:Label>
                                                                </td>
                                                                <td width="20%" ></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td class="Text" colspan="4">
                                                        <asp:Image ID="ImgBarr" runat="server" Height="20px" Width="100%" ImageUrl="~/Images/Barr2.png" />
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td class="Text" colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td width="50%">
                                <asp:Label ID="lblOpcionItem" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        <td align="right" width="50%">
                                <asp:Label ID="lblTipoItem" runat="server" CssClass="Titulo2"></asp:Label></td>
                                    </tr>
                                </table>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td class="Text">
                                                        Producto</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboProducto" runat="server" CssClass="Text" Width="200px"></asp:DropDownList></td>
                                                    <td class="Text">
                                                        INS-PROD (WIP)</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboCantInic" runat="server" CssClass="Text" Width="200px" Enabled="False">
                                                            <asp:ListItem Value="P">PRODUCTO</asp:ListItem>
                                                            <asp:ListItem Value="I">INSUMO</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr id="trAlmacen2" runat="server">
                                                    <td class="Text">
                                                        Unidad Medida</td>
                                                    <td>
                                                        <asp:DropDownList ID="cboUnidadMedida" runat="server" CssClass="Text" Width="200px">
                                                            <asp:ListItem Value="0">SIMPLE</asp:ListItem>
                                                            <asp:ListItem Value="1">DOBLE</asp:ListItem>
                                                        </asp:DropDownList></td>
                                                    <td class="Text">
                                                        Descripci�n</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc" runat="server" Width="200px" CssClass="Text" MaxLength="300"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trAlmacen3" runat="server">
                                                    <td class="Text">
                                                        Cantidad/Peso</td>
                                                    <td colspan="1">
                                                        <asp:TextBox ID="txtCantPeso" runat="server" Width="200px" CssClass="Text" onkeypress="javascript:return ValidNum(event);" ></asp:TextBox>
                                                    </td>
                                                    <td class="Text">
														Valor Unitario</td>
                                                    <td>
                                                        <asp:TextBox ID="txtValorUnit" runat="server" Width="200px" CssClass="Text" onkeypress="javascript:return ValidNum(event);" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trAlmacen4" runat="server">
                                                    <td class="Text">
                                                        Serie</td>
                                                    <td colspan="1">
                                                        <asp:TextBox ID="txtSerie" runat="server" Width="200px" CssClass="Text" onkeypress="javascript:return ValidNum(event);" ></asp:TextBox>
                                                    </td>
                                                    <td class="Text">
                                                        Descripci�n</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDesc2" runat="server" Width="200px" CssClass="Text" MaxLength="300"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                </table>
                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                    <tr>
                                        <td>
                                    <asp:Button class="btn" ID="btnCancelar" runat="server"  Text="Regresar" Width="180px" BorderStyle="None" BackColor="#B92303" Font-Bold="True" ForeColor="White" />
                                        </td>
                                        <td>
                                    <asp:Button class="btn" ID="btnAgregarNuevo" runat="server"  Text="Nuevo Item" Width="180px" BorderStyle="None" BackColor="White" Font-Bold="True" ForeColor="#3968C6" />
                                        </td>
                                        <td>
                                    <asp:Button class="btn" ID="btnAgregar" runat="server"  Text="Grabar" Width="180px" BorderStyle="None" BackColor="#3968C6" Font-Bold="True" ForeColor="White" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AutoGenerateColumns="False"
                                    BorderWidth="1px" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>    
                                       <asp:BoundColumn  DataField="NRO_DOCU" HeaderText="Nro.Docu" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="NRO_SECU" HeaderText="Item">
                                          <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_TIPO_MERC" HeaderText="Producto"></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DSC_PROD" HeaderText="Descripci�n"></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_TIPO_BULT" HeaderText="Unidad Medida">
                                               <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>   
                                        <asp:BoundColumn DataField="CAN_PESO" HeaderText="Cantidad/Peso" DataFormatString="{0:N3}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:BoundColumn>
                                        <asp:BoundColumn  DataField="DE_PROD" HeaderText="Cantidad Inicial">
                                               <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="VAL_UNIT" HeaderText="Valor Unit" DataFormatString="{0:N3}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:BoundColumn>   
                                        <asp:BoundColumn DataField="VAL_TOTA" HeaderText="Total" DataFormatString="{0:N3}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle></asp:BoundColumn>       
                                       <asp:BoundColumn  DataField="COD_DOC" HeaderText="Nro.Docu" Visible="false"></asp:BoundColumn>                           
                                      <asp:BoundColumn  DataField="COD_TIPDOC" HeaderText="Tipo.Doc" Visible="false"></asp:BoundColumn>  
                                              <asp:TemplateColumn HeaderText="Edit.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditar" OnClick="imgEditar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Editar" ImageUrl="Images/Editar.png"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                             <asp:TemplateColumn HeaderText="Elim.">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEliminar" OnClick="imgEliminar_Click" runat="server" CausesValidation="False"
                                                    ToolTip="Eliminar registro" ImageUrl="Images/Eliminar.png"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                                                               
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
        <asp:Literal ID="ltrNroDocu" runat="server" Visible="False"></asp:Literal>
        <asp:Literal ID="ltrCodDoc" runat="server" Visible="False"></asp:Literal>
        <asp:Literal ID="ltrTipDoc" runat="server" Visible="False"></asp:Literal>
        <asp:Literal ID="ltrNroItem" runat="server" Visible="False"></asp:Literal>
    </form>
</body>
</html>
