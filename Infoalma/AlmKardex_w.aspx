<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmKardex_w.aspx.vb" Inherits="AlmKardex_w" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Kardex Detallado</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">	

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIni").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFin").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() - miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }

        function fn_Impresion(sFE_INIC, sFE_FINA, sCO_ALMA, sDE_ALMA, sCODIGO, sDE_MERC, sLOTE, sCO_PROD) {
            var ventana = 'Popup/AlmImpresionKardex_w.aspx?sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sCO_ALMA=' + sCO_ALMA + '&sDE_ALMA=' + sDE_ALMA + '&sCODIGO=' + sCODIGO + '&sDE_MERC=' + sDE_MERC + '&sLOTE=' + sLOTE + '&sCO_PROD=' + sCO_PROD;
            window.open(ventana, "Impresion", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400");
            //window.showModalDialog(ventana,window,"dialogWidth:900px;dialogHeight:900px");  
        }

        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);

            if (fchFin == "" && fchIni == "") {
                alert("Debe ingresar por lo menos una fecha");
                return false;
            }

            if (fchFin != "" && fchIni != "") {
                if (fchIni > fchFin) {
                    alert("La fecha inicial no puede ser mayor a la fecha final");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" EnableViewState="False" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="4">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table style="z-index: 0" id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text" width="18%">Almac�n</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td width="32%" colspan="9">
                                                                    <asp:DropDownList Style="z-index: 0" ID="cboAlmacen" runat="server" CssClass="Text" AutoPostBack="True"
                                                                        Width="98%">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="z-index: 0" class="Text" width="18%">Nro. Ingreso</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td style="width: 139px" width="139">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtCodigo" runat="server" CssClass="Text" Width="140px"></asp:TextBox></td>
                                                                <td class="Text" width="14%">C�d. producto
                                                                </td>
                                                                <td width="1%">:</td>
                                                                <td style="z-index: 0; width: 119px" class="Text" width="119" colspan="2">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtCodprod" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                                <td class="Text" width="1%"></td>
                                                                <td style="z-index: 0" class="Text" width="19%" align="left">Descripci�n Merc.</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td width="25%" align="left">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtDescMerc" runat="server" CssClass="Text" Width="100px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Text" width="18%">Lote</td>
                                                                <td class="Text" width="1%">:</td>
                                                                <td style="width: 139px" width="139">
                                                                    <asp:TextBox Style="z-index: 0" ID="txtLote" runat="server" CssClass="Text" Width="140px"></asp:TextBox></td>
                                                                <td class="Text" width="14%">Fecha Del</td>
                                                                <td width="1%">:</td>
                                                                <td style="z-index: 0; width: 0" class="Text"><input style="z-index: 0" id="txtFechaInicial" class="text" onkeypress="validarcharfecha()"
                                                                    onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="10" name="txtFechaInicial" runat="server"> </td>
                                                                <td style="z-index: 0; width: 59px" class="Text" width="119">
                                                                    <input style="z-index: 0" id="btnFechaIni" class="text" 
                                                                        value="..." type="button" name="btnFechaIni"></td>
                                                                <td class="Text" width="1%" align="right"></td>
                                                                <td class="Text" width="19%" align="left">Al</td>
                                                                <td width="1%" align="left">:</td>
                                                                <td class="Text" width="25%" align="left">
                                                                    <input  id="txtFechaFinal" class="text" onkeypress="validarcharfecha()"
                                                                        onkeyup="this.value=formateafecha(this.value);" maxlength="10" size="10" name="txtFechaFinal" runat="server">
                                                                    <input style="z-index: 0" id="btnFechaFin" class="text" 
                                                                            value="..." type="button" name="btnFechaFin"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <input id="btnImprimir" class="btn" onclick="fn_Impresion('<%=txtFechaInicial.Value%>','<%=txtFechaFinal.Value%>','<%=cboAlmacen.SelectedItem.Value%>','<%=cboAlmacen.SelectedItem.Text%>','<%=txtCodigo.Text%>','<%=txtDescMerc.Text%>','<%=txtLote.Text%>','<%=txtCodprod.Text%>')" value="Imprimir" type="button" name="btnImprimir"></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" AllowPaging="True" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" PageSize="50">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="ORDEN" HeaderText="Orden">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="COD_PRODUCTO" HeaderText="Cod.Producto">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DESCRIPCION" HeaderText="Descripci�n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LOTE" HeaderText="Lote">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CANTIDAD" HeaderText="Cantidad" DataFormatString="{0:N2}">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MEDIDA" HeaderText="Medida">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_VENCIMIENTO" HeaderText="Fech.Vencimiento">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CODIGO_RETENCION" HeaderText="Cod.Retenci�n">
                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FECHA_CONFIRMACION" HeaderText="Fech.Confirmaci�n">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ORDENAMIENTO" HeaderText="Ordenamiento" Visible="False">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MOVIMIENTO" HeaderText="Movimiento">
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
