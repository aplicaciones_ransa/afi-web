Imports Depsa.LibCapaNegocio
Imports System.Text
Imports Library.AccesoDB
Imports Infodepsa
Imports System.Data
''Imports System.Data.SqlClient
'Imports Library.AccesoBL

Public Class AlmMovimiento_W
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objparametro As clsCNParametro
    Private objRetiro As New clsCNRetiro
    Private objKardex As clsCNKardex
    Private objMovimiento As clsCNMovimiento
    Private objWMS As clsCNWMS
    'Protected WithEvents dgIngresos As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgIngRes As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgIngDet As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgRetRes As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents dgRetDet As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboMovimiento As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboReporte As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtCodigo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodprod As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDescMerc As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtLote As System.Web.UI.WebControls.TextBox
    Private objWarrant As clsCNWarrant
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboCodProducto As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblNroWarrant As System.Web.UI.WebControls.Label
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        PoliticaCache()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MOVIMIENTO_MERCADERI") Then
            Me.lblOpcion.Text = "Movimiento de Mercaderķa WMS"
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MOVIMIENTO_MERCADERI"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Dim dttFecha As DateTime
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                dttFecha = Date.Today.Subtract(TimeSpan.FromDays(365))
                Dia = "00" + CStr(dttFecha.Day)
                Mes = "00" + CStr(dttFecha.Month)
                Anio = "0000" + CStr(dttFecha.Year)
                Me.txtFechaInicial.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                Dia = "00" + CStr(Now.Day)
                Mes = "00" + CStr(Now.Month)
                Anio = "0000" + CStr(Now.Year)
                Me.txtFechaFinal.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                ' documento()
                'GetCargarTipoMercaderia()
                LlenarAlmacenes()
                If Session("Co_AlmaW") = "" Then
                    Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                Else
                    Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                End If
                If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaW"))) = 0 Then
                    Response.Redirect("AlmMovimiento.aspx", False)
                    Exit Sub
                End If
                Me.btnBuscar.Attributes.Add("onclick", "Javascript:if(ValidaFecha('" & Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4) & "')== false) return false;")
                If Session.Item("IdTipoEntidad") = "03" And Session.Item("Vencimiento") = 0 Then
                    'Response.Write("<script language='Javascript'>window.open('Popup/AvisoPagarFacturas.aspx','Pago','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=655,height=365');</script>")
                    'Response.Write("<script language='Javascript'>window.open('Popup/Vencimientos.aspx','Vencimientos','top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=800,height=600');</script>")
                    Session.Item("Vencimiento") = 1
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            ' Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub BindatagridIngreso()

        Me.dgIngRes.Visible = True
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = False

        Dim dtIngRes As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtIngRes = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, Me.cboReporte.SelectedValue, _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))

                If dtIngRes.Rows.Count Then
                    Me.dgIngRes.Visible = True
                    Me.dgIngRes.DataSource = dtIngRes
                    Me.dgIngRes.DataBind()
                    Session.Item("dtMovimientoWMS") = dtIngRes
                End If

            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub
    Private Sub BindatagridIngresoDetallado()

        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = True
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = False

        Dim dtIngDet As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtIngDet = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, Me.cboReporte.SelectedValue, _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))
                If dtIngDet.Rows.Count Then
                    Me.dgIngDet.DataSource = dtIngDet
                    Me.dgIngDet.DataBind()
                    Session.Item("dtMovimientoWMS") = dtIngDet
                End If

            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridRetiro()
        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = True
        Me.dgRetDet.Visible = False

        Dim dtRetRes As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtRetRes = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, Me.cboReporte.SelectedValue, _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))
                If dtRetRes.Rows.Count Then

                    Me.dgRetRes.Visible = True
                    Me.dgRetRes.DataSource = dtRetRes
                    Me.dgRetRes.DataBind()
                    Session.Item("dtMovimientoWMS") = dtRetRes
                End If

            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub BindatagridRetiroDetallado()

        Me.dgIngRes.Visible = False
        Me.dgIngDet.Visible = False
        Me.dgRetRes.Visible = False
        Me.dgRetDet.Visible = True

        Dim dtRetDet As DataTable
        Try
            objMovimiento = New clsCNMovimiento
            With objMovimiento

                dtRetDet = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, Me.cboReporte.SelectedValue, _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))
                If dtRetDet.Rows.Count Then
                    Me.dgRetDet.Visible = True
                    Me.dgRetDet.DataSource = dtRetDet
                    Me.dgRetDet.DataBind()
                    Session.Item("dtMovimientoWMS") = dtRetDet
                End If

            End With
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Sub Change_dgIngDet(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgIngDet.CurrentPageIndex = Args.NewPageIndex
        Me.dgIngDet.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgIngDet.DataBind()
    End Sub

    Sub Change_dgIngRes(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgIngRes.CurrentPageIndex = Args.NewPageIndex
        Me.dgIngRes.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgIngRes.DataBind()
    End Sub

    Sub Change_dgRetRes(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgRetRes.CurrentPageIndex = Args.NewPageIndex
        Me.dgRetRes.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgRetRes.DataBind()
    End Sub

    Sub Change_dgRetDet(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgRetDet.CurrentPageIndex = Args.NewPageIndex
        Me.dgRetDet.DataSource = Session.Item("dtMovimientoWMS")
        Me.dgRetDet.DataBind()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Me.dgIngRes.CurrentPageIndex = 0
        Me.dgIngDet.CurrentPageIndex = 0
        Me.dgIngRes.CurrentPageIndex = 0
        Me.dgIngDet.CurrentPageIndex = 0
        Me.lblMensaje.Text = ""

        If Me.cboMovimiento.SelectedValue = "1" And Me.cboReporte.SelectedValue = 2 Then
            BindatagridIngreso()

        ElseIf Me.cboMovimiento.SelectedValue = "1" And Me.cboReporte.SelectedValue = 1 Then
            BindatagridIngresoDetallado()

        ElseIf Me.cboMovimiento.SelectedValue = "2" And Me.cboReporte.SelectedValue = 2 Then
            BindatagridRetiro()

        ElseIf Me.cboMovimiento.SelectedValue = "2" And Me.cboReporte.SelectedValue = 1 Then
            BindatagridRetiroDetallado()

        End If
        objAccesoWeb = New clsCNAccesosWeb

        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
        Session.Item("NombreEntidad"), "C", "MOVIMIENTO_MERCADERI", "CONSULTAR MOVIMIENTO MERCADERIA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgIngRes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngRes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                Dim sCA_CELL As String = e.Item.Cells(0).Text
                Dim sCA_LINK As String = "<A onclick=Javascript:AbrirDetaMoviWMS('" & Me.cboMovimiento.SelectedValue & "','" & e.Item.Cells(0).Text & "','" & Me.cboAlmacen.SelectedValue & "'); href='#'>" & sCA_CELL & "</a>"
                e.Item.Cells(0).Text = sCA_LINK
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub dgIngDet_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIngDet.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                If e.Item.Cells(10).Text = 1 Then
                    e.Item.Cells(0).Text = e.Item.Cells(0).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(8).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(11).Text
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                ElseIf e.Item.Cells(10).Text = 2 Then
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    'e.Item.Cells.RemoveAt(10)
                    'e.Item.Cells.RemoveAt(9)
                    'e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    'e.Item.Cells.RemoveAt(6)
                    'e.Item.Cells.RemoveAt(5)
                    'e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    'e.Item.Cells.RemoveAt(2)
                    'e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).Text = ""
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(10).Text = 3 Then
                    e.Item.Cells(0).Text = "TOTAL :&nbsp;&nbsp;&nbsp;" & e.Item.Cells(12).Text
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(10).Text = 4 Then
                    e.Item.Cells(0).Text = "&nbsp;"
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 10
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                End If

            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub dgRetRes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetRes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                Dim sCA_CELL As String = e.Item.Cells(0).Text
                Dim sCA_LINK As String = "<A onclick=Javascript:AbrirDetaMoviWMS('" & Me.cboMovimiento.SelectedValue & "','" & e.Item.Cells(0).Text & "','" & Me.cboAlmacen.SelectedValue & "'); href='#'>" & sCA_CELL & "</a>"
                e.Item.Cells(0).Text = sCA_LINK
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub


    Private Sub dgRetDet_ItemDataBound1(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRetDet.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                If e.Item.Cells(11).Text = 1 Then
                    e.Item.Cells(0).Text = e.Item.Cells(0).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(10).Text & "&nbsp;&nbsp;&nbsp;" & e.Item.Cells(12).Text
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.ForeColor = System.Drawing.Color.FromName("#FFFFFF")
                    e.Item.BackColor = System.Drawing.Color.FromName("#8096A1")
                ElseIf e.Item.Cells(11).Text = 2 Then
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    'e.Item.Cells.RemoveAt(10)
                    'e.Item.Cells.RemoveAt(9)
                    'e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    'e.Item.Cells.RemoveAt(6)
                    'e.Item.Cells.RemoveAt(5)
                    'e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    'e.Item.Cells.RemoveAt(2)
                    'e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).Text = ""
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(11).Text = 3 Then
                    e.Item.Cells(0).Text = "TOTAL CANTIDAD SOLICITADA :&nbsp;&nbsp;&nbsp;" & (e.Item.Cells(13).Text).ToString & "&nbsp;&nbsp;&nbsp;" _
                                         & "TOTAL CANTIDAD PREPARADA :&nbsp;&nbsp;&nbsp;" & (e.Item.Cells(14).Text).ToString

                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                ElseIf e.Item.Cells(11).Text = 4 Then
                    e.Item.Cells(0).Text = "&nbsp;"
                    e.Item.Cells.RemoveAt(14)
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    'e.Item.Cells.RemoveAt(0)
                    e.Item.Cells(0).ColumnSpan = 11
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Center
                    e.Item.BackColor = System.Drawing.Color.FromName("#ffffff")
                End If

            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub


    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim strFileName As String = "Reporte.xls"
        Dim dgExport As DataGrid = New DataGrid

        'se limpia el buffer del response
        Response.Clear()
        Response.Buffer = True

        'se establece el tipo de accion a ejecutar
        Response.ContentType = "application/download"
        Response.Charset = ""
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName)
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default

        'se inhabilita el view state para no ejecutar ninguna accion
        Me.EnableViewState = False

        'se rederiza el control datagrid
        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
        Dim dtExport As DataTable

        If Me.cboMovimiento.SelectedValue = "1" And Me.cboReporte.SelectedValue = 2 Then
            dtExport = Session.Item("dtMovimientoWMS")
        ElseIf Me.cboMovimiento.SelectedValue = "1" And Me.cboReporte.SelectedValue = 1 Then
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtExport = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, "3", _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))
            End With
        ElseIf Me.cboMovimiento.SelectedValue = "2" And Me.cboReporte.SelectedValue = 2 Then
            dtExport = Session.Item("dtMovimientoWMS")

        ElseIf Me.cboMovimiento.SelectedValue = "2" And Me.cboReporte.SelectedValue = 1 Then
            objMovimiento = New clsCNMovimiento
            With objMovimiento
                dtExport = .gCNListarMoviMercWMS(Me.cboMovimiento.SelectedValue, "3", _
                                Me.txtCodigo.Text, Me.txtDescMerc.Text, Me.txtLote.Text, Me.txtFechaInicial.Value, _
                                Me.txtFechaFinal.Value, Me.cboAlmacen.SelectedValue, Me.txtCodprod.Text, Session.Item("IdSico"))
            End With
        End If

        dgExport.DataSource = dtExport
        dgExport.DataBind()
        dgExport.RenderControl(htmlWriter)

        'se escribe el contenido en el listado
        Response.Write(sWriter.ToString())
        Response.End()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objparametro Is Nothing) Then
            objparametro = Nothing
        End If
        If Not (objKardex Is Nothing) Then
            objKardex = Nothing
        End If
        If Not (objMovimiento Is Nothing) Then
            objMovimiento = Nothing
        End If
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) = 0 Then
            Response.Redirect("AlmMovimiento.aspx", False)
        End If
    End Sub
End Class
