Imports System.Web.Mail
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Security.Cryptography
Imports System
Imports System.Globalization


Public Class Utils

    Public Shared Function EncryptString(ByVal InputString As String, ByVal SecretKey As String, _
                                        Optional ByVal CyphMode As CipherMode = CipherMode.ECB) As String
        Dim Des As New TripleDESCryptoServiceProvider
        'Put the string into a byte array
        Dim InputbyteArray() As Byte = Encoding.UTF8.GetBytes(InputString)
        'Create the crypto objects, with the key, as passed in
        Dim hashMD5 As New MD5CryptoServiceProvider
        Des.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(SecretKey))
        Des.Mode = CyphMode
        Dim ms As MemoryStream = New MemoryStream
        Dim cs As CryptoStream = New CryptoStream(ms, Des.CreateEncryptor(), _
        CryptoStreamMode.Write)
        'Write the byte array into the crypto stream
        '(It will end up in the memory stream)
        cs.Write(InputbyteArray, 0, InputbyteArray.Length)
        cs.FlushFinalBlock()
        'Get the data back from the memory stream, and into a string
        Dim ret As StringBuilder = New StringBuilder
        Dim b() As Byte = ms.ToArray
        ms.Close()
        Dim I As Integer
        For I = 0 To UBound(b)
            'Format as hex
            ret.AppendFormat("{0:X2}", b(I))
        Next
        Return ret.ToString()
    End Function

    Public Shared Function DecryptString(ByVal InputString As String, ByVal SecretKey As String, _
                                            Optional ByVal CyphMode As CipherMode = CipherMode.ECB) As String
        If InputString = String.Empty Then
            Return ""
        Else
            Dim Des As New TripleDESCryptoServiceProvider
            'Put the string into a byte array
            Dim InputbyteArray(CType(InputString.Length / 2 - 1, Integer)) As Byte '= Encoding.UTF8.GetBytes(InputString)
            'Create the crypto objects, with the key, as passed in
            Dim hashMD5 As New MD5CryptoServiceProvider
            Des.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(SecretKey))
            Des.Mode = CyphMode
            'Put the input string into the byte array
            Dim X As Integer
            For X = 0 To InputbyteArray.Length - 1
                Dim IJ As Int32 = (Convert.ToInt32(InputString.Substring(X * 2, 2), 16))
                InputbyteArray(X) = New Byte
                InputbyteArray(X) = CType(CType(IJ, Byte), Byte)
            Next
            Dim ms As MemoryStream = New MemoryStream
            Dim cs As CryptoStream = New CryptoStream(ms, Des.CreateDecryptor(), _
            CryptoStreamMode.Write)
            'Flush the data through the crypto stream into the memory stream
            cs.Write(InputbyteArray, 0, InputbyteArray.Length)
            cs.FlushFinalBlock()
            '//Get the decrypted data back from the memory stream
            Dim ret As StringBuilder = New StringBuilder
            Dim B() As Byte = ms.ToArray
            ms.Close()
            Dim I As Integer
            For I = 0 To UBound(B)
                ret.Append(Chr(B(I)))
            Next
            Return ret.ToString()
        End If
    End Function
    'Public Shared Function SendMail(ByVal strCorreoDe As String, ByVal strCorreoPara As String, _
    '                            ByVal strAsunto As String, ByVal strMensaje As String, ByVal strFile As String) As Boolean
    '    Try
    '        Dim mmCorreo As New MailMessage
    '        With mmCorreo
    '            Const ConfigNamespace As String = "http://schemas.microsoft.com/cdo/configuration/"
    '            Dim Flds As System.Collections.IDictionary
    '            Flds = mmCorreo.Fields
    '            With Flds
    '                .Add(ConfigNamespace & "smtpserver", System.Configuration.ConfigurationSettings.AppSettings("smtpserver"))
    '                .Add(ConfigNamespace & "smtpserverport", System.Configuration.ConfigurationSettings.AppSettings("smtpserverport"))
    '                .Add(ConfigNamespace & "sendusing", System.Configuration.ConfigurationSettings.AppSettings("sendusing"))
    '                .Add(ConfigNamespace & "sendusername", System.Configuration.ConfigurationSettings.AppSettings("sendusername"))
    '                '    .Add(ConfigNamespace & "sendpassword", System.Configuration.ConfigurationSettings.AppSettings("sendpassword"))
    '                '    .Add(ConfigNamespace & "smtpauthenticate", System.Configuration.ConfigurationSettings.AppSettings("smtpauthenticate"))
    '            End With
    '            'Se Indica la Dirección de correo que envia
    '            .From = strCorreoDe
    '            'Se Indica la Dirección de correo que recibira
    '            .To = strCorreoPara
    '            'Se Indica el Asunto del correo a enviar
    '            .Subject = strAsunto
    '            'El Mensaje del Correo
    '            .Body = strMensaje
    '            'establece el tipo de contenido del texto del mensaje de correo electrónico.
    '            'MailFormat.Html o MailFormat.Text
    '            .BodyFormat = MailFormat.Html
    '            'Establece la prioridad del mensaje de correo electrónico
    '            'MailPriority.High, MailPriority.Normal o MailPriority.Low
    '            .Priority = MailPriority.High
    '            'Establece el nombre del servidor de transmisión de correo SMTP 
    '            'que se va a utilizar para enviar los mensajes de correo electrónico.mail.servidor.com 
    '            'SmtpMail.SmtpServer = "161.132.96.92" '"System.Configuration.ConfigurationSettings.AppSettings("SMTPServerName")
    '            'Envía un mensaje de correo electrónico utilizando argumentos 
    '            'suministrados en las propiedades de la clase MailMessage.
    '            If strFile <> "" Then
    '                .Attachments.Add(New MailAttachment(strFile))
    '            End If
    '            SmtpMail.Send(mmCorreo)
    '        End With
    '        Return True
    '    Catch ex As Exception
    '        Dim errorMessage As String = ""
    '        While Not IsDBNull(ex)
    '            errorMessage += ex.Message + "\n"
    '            ex = ex.InnerException
    '        End While
    '        Return False
    '    End Try
    'End Function
    Public Sub gCargaCombo(ByRef objCombo As System.Web.UI.WebControls.DropDownList, ByVal pdtData As DataTable)
        objCombo.DataSource = pdtData
        objCombo.DataBind()
        pdtData.Dispose()
        pdtData = Nothing
    End Sub

    Public Sub gCargaGrid(ByRef objGrid As System.Web.UI.WebControls.DataGrid, ByVal pdtData As DataTable)
        objGrid.DataSource = pdtData
        objGrid.DataBind()
        pdtData.Dispose()
        pdtData = Nothing
    End Sub
    Public Shared Function fn_encr_clav(ByVal sDE_CLAV) As String
        Dim nCO_CADE As String = 1
        Dim nCA_ASCI As String = 0
        Dim sCA_PARC As String = ""
        Dim sCA_RESU As String = ""

        Do While (nCO_CADE <= Len(sDE_CLAV))
            sCA_PARC = Mid(sDE_CLAV, nCO_CADE, 1)
            nCA_ASCI = Asc(sCA_PARC) + 10 + (nCO_CADE - 1)
            sCA_PARC = Chr(nCA_ASCI)
            sCA_RESU = sCA_RESU & sCA_PARC
            nCO_CADE = nCO_CADE + 1
        Loop

        Return sCA_RESU
    End Function
    Public Shared Function gGetPosicionMenu(ByVal strCoGrup As String, ByVal strCoSist As String, _
                                            ByVal strCoMenu As String) As String
        Dim dtMenu As DataTable
        Dim strCodModulo As String
        Dim i As Integer = 0
        Dim intModulo As Integer = -1
        Dim intMenu As Integer = 0
        Dim objAcceso As Library.AccesoDB.Acceso
        objAcceso = New Library.AccesoDB.Acceso
        dtMenu = objAcceso.gGetMenuPrincipal(strCoGrup, strCoSist)
        strCodModulo = ""
        While i < dtMenu.Rows.Count
            If strCodModulo <> dtMenu.Rows(i)("CO_MODU") Then
                intModulo = intModulo + 1
                strCodModulo = dtMenu.Rows(i)("CO_MODU")
                intMenu = 0
            Else
                intMenu = intMenu + 1
            End If
            If dtMenu.Rows(i)("CO_MENU") = strCoMenu Then
                Exit While
            End If
            i += 1
        End While
        Return CStr(intModulo) & "-" & CStr(intMenu)
    End Function
End Class
Module Funciones
    Public Sub pg_DATA_LIMP(ByVal pDG_GENE As WebControls.DataGrid)
        Dim dtTA_NUEV As New DataTable
        Dim dsCO_NUEV As New DataSet

        dsCO_NUEV.Tables.Add(dtTA_NUEV)
        pDG_GENE.DataSource = dsCO_NUEV
        pDG_GENE.DataBind()
    End Sub
End Module
