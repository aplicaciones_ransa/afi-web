<%@ Page Language="vb" AutoEventWireup="false" CodeFile="LiberacionPendiente.aspx.vb" Inherits="LiberacionPendiente" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>LiberacionPendiente</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
			function ActivaBoton()
				{			
				frm=document.forms[0];				
				frm.btnEliminar.disabled=false;		
				}	
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0" align="center">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text">Nro Warrant:</TD>
														<TD>
															<asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
														<TD class="text">Nro Liberación:</TD>
														<TD>
															<asp:textbox id="txtNroLiberacion" runat="server" CssClass="Text" Width="100px"></asp:textbox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center">
									<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="250">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										PageSize="15" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="COD_EMPR"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_UNID"></asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro.Warr">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_TIPDOC" HeaderText="Tip.Doc">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NRO_LIBE" HeaderText="Nro Retiro">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FCH_CREA" HeaderText="Fch.Creaci&#243;n">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Descripci&#243;n Mercader&#237;a">
												<ItemTemplate>
													<asp:HyperLink id=hplDescipcion runat="server" CssClass="Text" Target="_blank" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.NOM_PDF") %>' ToolTip="Click para abrir PDF">
														<%# DataBinder.Eval(Container, "DataItem.DSC_MERC") %>
													</asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="NOM_ENTIFINA" HeaderText="Endosatario">
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NOM_ESTA" HeaderText="Estado">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
                                            <asp:BoundColumn DataField="IM_RETI" HeaderText="Importe">
                                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                            </asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Aprobar">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgAprobar" onclick="imgAprobar_Click" runat="server" ToolTip="Aprobar Liberación Electrónica"
																	ImageUrl="Images/Activar.gif"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Anular">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="30">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgCancelar" onclick="imgCancelar_Click" runat="server" ImageUrl="Images/anulado.gif"
																	ToolTip="Anular Liberación Electrónica"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_ENTFINA" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="co_alma"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE id="Table2" cellSpacing="4" cellPadding="0" width="630" border="0">
										<TR>
											<TD class="td" colSpan="8">Leyenda</TD>
										</TR>
										<TR>
											<TD width="15%" bgColor="lightgoldenrodyellow"></TD>
											<TD class="Leyenda" width="35%">Liberación de Warrant Electrónico.</TD>
											<TD width="15%"></TD>
											<TD class="Leyenda" width="35%">Liberación de Warrant Normal.</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
