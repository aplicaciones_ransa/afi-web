Imports Depsa.LibCapaNegocio
Imports System.Text
Imports System
Imports System.IO
Imports System.Collections
Imports Infodepsa
Imports System.Data

Public Class AlmSaldo
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objParametro As clsCNParametro
    Private objWarrant As clsCNWarrant
    Private objMercaderia As clsCNMercaderia
    Private dvInventario As DataView
    Private objRetiro As clsCNRetiro
    'Protected WithEvents hlkReporte As System.Web.UI.WebControls.LinkButton
    Private objWMS As clsCNWMS
    Private strPathRepoBulxPal = ConfigurationManager.AppSettings.Item("PathRepoBulxPal")
    ' Protected WithEvents ddlestado As System.Web.UI.WebControls.DropDownList
    Private strAccesoRepoBulxPal = ConfigurationManager.AppSettings.Item("AccesoRepoBulxPal")

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboModalidad As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboCodProducto As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents chkNoCerrado As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents txtFechaVenc As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaCierre As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        objWMS = New clsCNWMS
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "INVENTARIO_MERCADERI") Then
            Try
                Me.lblOpcion.Text = "Inventario Mercaderķa"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "INVENTARIO_MERCADERI"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaCierre.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    ListaReferencia()
                    LlenarModalidad()
                    LlenarAlmacenes()

                    If InStr(strAccesoRepoBulxPal, Session.Item("IdSico")) Then
                        hlkReporte.Visible = True
                    Else
                        hlkReporte.Visible = False
                    End If

                    If Session("Co_AlmaW") = "" Then
                        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session("Co_AlmaW")
                    End If
                    If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session.Item("Co_AlmaW"))) > 0 Then
                        Response.Redirect("AlmSaldo_W.aspx", False)
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ListaReferencia()
        objParametro = New clsCNParametro
        objParametro.gCNListarReferencia(Me.cboReferencia, "4")
    End Sub

    Private Sub LlenarModalidad()
        Try
            objWarrant = New clsCNWarrant
            Dim dtTipoWar As DataTable
            Me.cboModalidad.Items.Clear()
            dtTipoWar = objWarrant.gCNGetListarTipoWarrant(Session("CoEmpresa"), Session("IdSico"), Session("IdTipoEntidad"), "N")
            Me.cboModalidad.Items.Add(New ListItem("-----Todos-----", 0))
            If dtTipoWar Is Nothing Then
                Exit Sub
            End If
            For Each dr As DataRow In dtTipoWar.Rows
                Me.cboModalidad.Items.Add(New ListItem(dr("DE_MODA_MOVI"), dr("CO_MODA_MOVI")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            'Me.cboAlmacen.Items.Add(New ListItem("------ Todos -------", "0"))
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Me.dgResultado.DataSource = fn_Quiebre(CType(Session.Item("dtMovimiento"), DataView))
        Me.dgResultado.DataBind()
    End Sub

    Private Function fn_Quiebre(ByVal dvInventario As DataView) As DataTable
        'Try
        Dim dr As DataRowView
        Dim drNU_REGI As DataRow
        Dim strNroDCR As String = "&nbsp;"
        Dim dtTCDOCU_CLIE As New DataTable
        dtTCDOCU_CLIE.Columns.Add("NU_ORDE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("CO_UNID", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_DOCU_RECE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MODA_MOVI", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_SECU", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("FE_REGI_INGR", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_TIPO_BULT", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("NU_UNID_SALD", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("NU_PESO_SALD", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_PROD_CLIE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("FE_VCTO_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("IM_UNIT", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("CO_MONE", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("IM_REMO_SALD", GetType(System.Decimal))
        dtTCDOCU_CLIE.Columns.Add("DE_ESTA_MERC", GetType(System.String))
        dtTCDOCU_CLIE.Columns.Add("DE_REFE_0002", GetType(System.String))

        For Each dr In dvInventario
            If strNroDCR <> dr("NU_DOCU_RECE") Then
                drNU_REGI = dtTCDOCU_CLIE.NewRow()
                drNU_REGI("DE_MODA_MOVI") = "DCR : " & dr("NU_DOCU_RECE")
                drNU_REGI("DE_TIPO_BULT") = "GUIA INGRESO : " & dr("DE_REFE_0002")
                drNU_REGI("DE_MERC") = "ALMACEN : " & dr("DE_ALMA")
                dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
                strNroDCR = dr("NU_DOCU_RECE")
            End If

            drNU_REGI = dtTCDOCU_CLIE.NewRow()
            drNU_REGI("NU_ORDE") = dr("NU_ORDE")
            drNU_REGI("CO_UNID") = dr("CO_UNID")
            drNU_REGI("NU_DOCU_RECE") = dr("NU_DOCU_RECE")
            drNU_REGI("DE_MODA_MOVI") = dr("DE_MODA_MOVI")
            drNU_REGI("NU_SECU") = dr("NU_SECU")
            drNU_REGI("FE_REGI_INGR") = dr("FE_REGI_INGR")
            drNU_REGI("DE_TIPO_BULT") = dr("DE_TIPO_BULT")
            drNU_REGI("NU_UNID_SALD") = CType(dr("NU_UNID_SALD"), Decimal)
            drNU_REGI("NU_PESO_SALD") = CType(dr("NU_PESO_SALD"), Decimal)
            drNU_REGI("CO_PROD_CLIE") = dr("CO_PROD_CLIE")
            drNU_REGI("DE_MERC") = dr("DE_MERC")
            drNU_REGI("FE_VCTO_MERC") = dr("FE_VCTO_MERC")
            drNU_REGI("IM_UNIT") = CType(dr("IM_UNIT"), Decimal)
            drNU_REGI("CO_MONE") = dr("CO_MONE")
            drNU_REGI("IM_REMO_SALD") = CType(dr("IM_REMO_SALD"), Decimal)
            drNU_REGI("DE_ESTA_MERC") = dr("DE_ESTA_MERC")
            drNU_REGI("DE_REFE_0002") = dr("DE_REFE_0002")
            dtTCDOCU_CLIE.Rows.Add(drNU_REGI)
        Next
        Return dtTCDOCU_CLIE
        'Catch e1 As Exception
        '    Dim sCA_ERRO As String = e1.Message
        'End Try
    End Function

    Private Sub Bindatagrid()
        Try
            objMercaderia = New clsCNMercaderia
            dvInventario = objMercaderia.gCNGetListarInventario(Session("CoEmpresa"), Session.Item("IdSico"), Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, _
                                             Me.cboModalidad.SelectedValue, Me.txtFechaVenc.Value, Me.txtFechaCierre.Value, Me.chkNoCerrado.Checked, "S", Me.cboAlmacen.SelectedValue, Me.ddlestado.SelectedValue).DefaultView
            Session.Add("dtMovimiento", dvInventario)
            Me.dgResultado.DataSource = fn_Quiebre(dvInventario)
            Me.dgResultado.DataBind()
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"), _
                                Session.Item("NombreEntidad"), "C", "INVENTARIO_MERCADERI", "CONSULTAR INVENTARIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(0).Text <> "&nbsp;" And e.Item.Cells(1).Text = "&nbsp;" Then
                    e.Item.Cells.RemoveAt(13)
                    e.Item.Cells.RemoveAt(12)
                    e.Item.Cells.RemoveAt(11)
                    e.Item.Cells.RemoveAt(10)
                    e.Item.Cells.RemoveAt(9)
                    e.Item.Cells.RemoveAt(8)
                    'e.Item.Cells.RemoveAt(7)
                    e.Item.Cells.RemoveAt(6)
                    e.Item.Cells.RemoveAt(5)
                    e.Item.Cells.RemoveAt(4)
                    'e.Item.Cells.RemoveAt(3)
                    e.Item.Cells.RemoveAt(2)
                    e.Item.Cells.RemoveAt(1)
                    e.Item.Cells(0).ColumnSpan = 3
                    e.Item.Cells(0).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(0).Font.Bold = True
                    e.Item.Cells(1).ColumnSpan = 4
                    e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(1).Font.Bold = True
                    e.Item.Cells(2).ColumnSpan = 7
                    e.Item.Cells(2).HorizontalAlign = HorizontalAlign.Left
                    e.Item.Cells(2).Font.Bold = True
                    e.Item.BackColor = System.Drawing.Color.Lavender
                Else
                    Dim sCA_CELL As String = e.Item.Cells(1).Text
                    Dim sCA_LINK As String = "<A onclick=Javascript:AbrirMovimiento('" & e.Item.DataItem("NU_DOCU_RECE").ToString.Trim & "','" & e.Item.DataItem("NU_SECU") & "','" & e.Item.DataItem("CO_UNID") & "'); href='#'> " & _
                                             "" & sCA_CELL & "</a>"
                    e.Item.Cells(1).Text = sCA_LINK
                    If e.Item.DataItem("NU_ORDE") = "1" Then
                        e.Item.BackColor = System.Drawing.Color.FromName("#ffcc66")
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.Charset = ""

        response.ContentType = "application/vnd.ms-excel"
        response.AddHeader("Content-Disposition", "attachment;filename=" & "Warrants.xls")
        response.Charset = "UTF-8"
        response.ContentEncoding = Encoding.Default

        Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)

        Dim dg As DataGrid = New DataGrid
        objMercaderia = New clsCNMercaderia
        dg.DataSource = objMercaderia.gCNGetListarInventario(Session("CoEmpresa"), Session.Item("IdSico"), Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, _
                                             Me.cboModalidad.SelectedValue, Me.txtFechaVenc.Value, Me.txtFechaCierre.Value, Me.chkNoCerrado.Checked, "S", Me.cboAlmacen.SelectedValue, Me.ddlestado.SelectedValue)
        dg.DataBind()
        dg.RenderControl(htmlWriter)
        response.Write(sWriter.ToString())
        response.End()
        dg = Nothing
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objWarrant Is Nothing) Then
            objWarrant = Nothing
        End If
        If Not (objMercaderia Is Nothing) Then
            objMercaderia = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaW") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaW"))) > 0 Then
            Response.Redirect("AlmSaldo_w.aspx", False)
        End If
    End Sub

    Protected Sub hlkReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim objCCorrientes As clsCNCCorrientes
            Dim shlkRepxPaleta As LinkButton = CType(sender, LinkButton)
            Dim strDetaRep As String

            objMercaderia = New clsCNMercaderia
            Dim dt As DataTable
            dt = objMercaderia.gCNGetGeneraRepoBulxPal("01", "001", DateTime.Now, Me.cboAlmacen.SelectedValue, Session.Item("IdSico"))

            If dt.Rows.Count <> 0 Then

                Dim sLine As String = ""
                Dim strCabecera As String = ""
                Dim imax As Integer = (dt.Columns.Count - 1)
                Dim i As Integer = 0

                For Each column As DataColumn In dt.Columns
                    strCabecera = strCabecera & column.ColumnName
                    If i = imax Then
                        Exit For
                    Else
                        strCabecera = strCabecera & ","
                    End If
                    i += 1
                Next

                strDetaRep = strDetaRep & Replace(Replace(strCabecera, Chr(13), ""), Chr(10), "") & Chr(13) & Chr(10)

                ' ========================== Insertando Detalle =============================
                For j As Integer = 0 To dt.Rows.Count - 1
                    sLine = ""
                    For jj As Integer = 0 To i
                        If jj = i Then
                            sLine = sLine & (Replace(IIf((dt.Rows(j)(jj)) Is DBNull.Value, "", (dt.Rows(j)(jj))), ",", " "))
                        Else
                            sLine = sLine & (Replace(IIf((dt.Rows(j)(jj)) Is DBNull.Value, "", (dt.Rows(j)(jj))), ",", " ") & ",")
                        End If
                    Next
                    'eliminando salto de linea
                    strDetaRep = strDetaRep & Replace(Replace(sLine, Chr(13), ""), Chr(10), "") & Chr(13) & Chr(10)
                Next

                Dim response As HttpResponse = HttpContext.Current.Response
                response.Clear()
                response.Charset = ""
                response.ContentType = "text"
                response.AddHeader("Content-Disposition", "attachment;filename=" & "RepoPal" & ".csv")
                response.ContentEncoding = Encoding.Default
                Dim sWriter As System.IO.StringWriter = New System.IO.StringWriter
                Dim htmlWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sWriter)
                response.Write(strDetaRep)
                response.End()
            Else
                Me.lblMensaje.Text = "Documento no existe"
            End If
            'End If
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR " + ex.Message
        End Try
    End Sub
End Class
