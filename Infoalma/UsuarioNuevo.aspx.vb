Imports Depsa.LibCapaNegocio
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Infodepsa
Imports System.Data

Partial  Class UsuarioNuevo
    Inherits System.Web.UI.Page

    Private objUsuario As Usuario = New Usuario
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    'Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents btnResetear As System.Web.UI.WebControls.Button
    'Protected WithEvents Requiredfieldvalidator6 As System.Web.UI.WebControls.RequiredFieldValidator
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtNombre As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDNI As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtCodUsuario As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaCadu As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtLogin As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblTitulo As System.Web.UI.WebControls.Label
    Protected WithEvents TipoBusqueda As System.Web.UI.HtmlControls.HtmlSelect
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnAddEntidad As System.Web.UI.WebControls.Button
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdEntidad As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnEnviarContraseña As System.Web.UI.WebControls.Button
    'Protected WithEvents txtCorreo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents rdoSI As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents rdoNO As System.Web.UI.HtmlControls.HtmlInputRadioButton
    'Protected WithEvents chkActivo As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtApellido As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_USUARIO") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_USUARIO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Dim Dia As String
                Dim Mes As String
                Dim Anio As String
                Me.btnAddEntidad.Attributes.Add("onclick", "javascript:if(confirm('Se grabará los cambios realizados al usuario \n antes de agregar una nueva entidad?')== false) return false;")
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                Me.txtDNI.EnableViewState = True
                Me.txtCodUsuario.EnableViewState = True
                ValidarControles()
                If Request.Item("Cod") <> "" And Request.Item("Cod") <> "X" Then
                    txtCodUsuario.Value = Request.Item("Cod")
                    Me.lblTitulo.Text = "Editar Usuario"
                    Me.btnGrabar.Text = "Actualizar"
                    loadDataUsuario()
                Else
                    Me.lblTitulo.Text = "Nuevo Usuario"
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaCadu.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                    Me.btnEnviarContraseña.Visible = False
                    Me.btnResetear.Visible = False
                End If
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub ValidarControles()
        If Session.Item("IdTipoUsuario") = "04" Then
            Me.btnGrabar.Enabled = True
            Me.btnEnviarContraseña.Visible = True
            Me.btnAddEntidad.Visible = True
            Me.txtNombre.Enabled = True
            Me.txtApellido.Enabled = True
            Me.txtLogin.Enabled = True
            Me.txtDNI.Disabled = False
            Me.txtCorreo.Enabled = True
            Me.txtFechaCadu.Disabled = False
            Me.chkActivo.Enabled = True
            Me.btnResetear.Visible = True
        Else
            Me.btnGrabar.Enabled = False
            Me.btnEnviarContraseña.Visible = False
            Me.btnAddEntidad.Visible = False
            Me.btnResetear.Visible = False
        End If
    End Sub

    Private Sub loadDataUsuario()
        Dim dtUsuario As DataTable = New DataTable
        Try
            dtUsuario = objUsuario.gGetUsuario(txtCodUsuario.Value)
            If dtUsuario.Rows.Count = 0 Then
                Me.lblError.Text = "Error al retornar los datos del usuario seleccionado"
            Else
                Me.txtNombre.Text = CType(dtUsuario.Rows(0)("NOM_USER"), String).Trim
                Me.txtApellido.Text = CType(dtUsuario.Rows(0)("APE_USER"), String).Trim
                Me.txtDNI.Value = CType(dtUsuario.Rows(0)("NRO_DOCIDEN"), String).Trim
                Me.txtCorreo.Text = CType(dtUsuario.Rows(0)("DIR_EMAI"), String).Trim
                Me.txtLogin.Text = CType(dtUsuario.Rows(0)("USR_ACCE"), String).Trim
                Me.txtFechaCadu.Value = CType(dtUsuario.Rows(0)("FCH_CADFIR"), String).Trim
                If dtUsuario.Rows(0)("CER_NUMSERI") = "" Then
                    Me.rdoNO.Checked = True
                Else
                    Me.rdoSI.Checked = True
                End If
                Me.chkActivo.Checked = CType(dtUsuario.Rows(0)("FLG_CERT"), String).Trim
                BindDatagrid()
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        Finally
            dtUsuario.Dispose()
            dtUsuario = Nothing
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            Me.lblError.Text = GrabarDataUsuario()
            loadDataUsuario()
            Me.btnGrabar.Text = "Actualizar"
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                        Session.Item("NombreEntidad"), "M", "MAESTRO_USUARIO", "ACTUALIZA USUARIO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        Catch ex As Exception
            Me.lblError.Text = "Se produjo el siguiente error: " & ex.Message
        End Try
    End Sub

    Function GrabarDataUsuario() As String

        Dim strResult As String
        Dim strNombre As String
        Dim strTipo As String
        Dim dr As DataRow
        If Me.btnGrabar.Text = "Grabar" Then
            Dim strContraseña As String
            Dim strEncriptado As String
            strContraseña = Funciones.GetSimpleUserPassword(6)
            strEncriptado = Funciones.EncryptString(strContraseña, "©¹ã_[]0")
            Me.txtCodUsuario.Value = objUsuario.gInsUsuario(Me.txtDNI.Value, Me.txtNombre.Text.ToUpper,
                                        Me.txtApellido.Text.ToUpper, Me.txtFechaCadu.Value,
                                        Me.txtLogin.Text, strEncriptado, Session.Item("IdUsuario"), Me.txtCorreo.Text.ToUpper, Me.chkActivo.Checked)
            If Me.txtCodUsuario.Value = "X" Then
                strResult = "Ya existe el login de usuario ingresado, cambielo!"
                Exit Function
            Else
                strResult = "Se grabó correctamente"
            End If
            EnvioMail(Me.txtLogin.Text, strContraseña)
        Else
            strResult = objUsuario.gUpdUsuario(Me.txtDNI.Value, Me.txtNombre.Text.ToUpper, Me.txtApellido.Text.ToUpper,
                                                 Me.txtFechaCadu.Value, Me.txtLogin.Text,
                                                Me.txtCodUsuario.Value, Session.Item("IdUsuario"), Me.txtCorreo.Text.ToUpper, Me.chkActivo.Checked)
        End If
        Return strResult
    End Function

    Sub ResetearClaveFirma()
        Dim strResult As String
        Dim strNombre As String
        Dim strTipo As String
        Dim dr As DataRow
        Dim strContraseña As String
        Dim strEncriptado As String
        'strContraseña = "456321"       

        Dim Generator As System.Random = New System.Random()
        strContraseña = (Generator.Next(100000, 999999)).ToString()

        strEncriptado = Funciones.EncryptString(strContraseña, "©¹ã_[]0")
        strResult = objUsuario.gUpdClaveFirma(strEncriptado, Me.txtCodUsuario.Value, Session.Item("IdUsuario"))
        lblError.Text = strResult
        EnvioMailReseteo(Me.txtLogin.Text, strContraseña)
    End Sub

    Private Sub EnvioMail(ByVal strLogin As String, ByVal strContraseña As String)
        objFunciones.SendMail("Alma Perú – Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", Me.txtCorreo.Text, "Contraseña de Acceso", "Datos de acceso al Sistema AFI " &
                         ":<br><br>Usuario: " & strLogin & "<br>Contraseña: " & strContraseña & "<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Alma Perú - Sistema AFI</A>" &
                         "<br>Cualquier consulta sobre la aplicación escribir a soporte@almaperu.com.pe", "")
    End Sub

    Private Sub EnvioMailReseteo(ByVal strLogin As String, ByVal strContraseña As String)
        objFunciones.SendMail("Alma Perú – Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", Me.txtCorreo.Text, "Clave para firmar", "Estimado(a) sr(a).  " &
                           strLogin.ToLower & "  su clave para firmar los documentos es:  " & strContraseña & "<br><br>El Link de acceso al sistema es: <A href='https://afi.almaperu.com.pe'>Alma Perú - Sistema AFI</A>" &
                         "<br>Cualquier consulta sobre la aplicación escribir a soporte@almaperu.com.pe", "")
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        'Session.Add("ubPag", "1")
        Response.Redirect("UsuarioBusqueda.aspx")
    End Sub

    Private Sub BindDatagrid()
        Dim dt As DataTable
        Try
            dt = objEntidad.gGetEntidadesUsuario(Me.txtCodUsuario.Value)
            Me.dgdEntidad.DataSource = dt
            Me.dgdEntidad.DataBind()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Public Sub imgEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEditar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEditar.Parent.Parent, DataGridItem)
            Session.Add("CodEntidadSico", dgi.Cells(0).Text())
            Response.Redirect("UsuarioEntidadNuevo.aspx?Nombre=" & Me.txtApellido.Text.ToUpper & " " & Me.txtNombre.Text.ToUpper & "&Cod=" & Me.txtCodUsuario.Value, False)
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Public Sub imgEliminar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgEliminar As ImageButton = CType(sender, ImageButton)
            Dim dgi As DataGridItem
            dgi = CType(imgEliminar.Parent.Parent, DataGridItem)
            If objUsuario.gDelEntidadDeUsuario(Me.txtCodUsuario.Value, dgi.Cells(0).Text()) = "X" Then
                Me.lblError.Text = "No se pudo quitar la entidad"
            Else
                BindDatagrid()
                Me.lblError.Text = "Se eliminó correctamente"
            End If
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub btnAddEntidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEntidad.Click
        Try
            GrabarDataUsuario()
            Response.Redirect("UsuarioEntidadNuevo.aspx?Nombre=" & Me.txtApellido.Text.ToUpper & " " & Me.txtNombre.Text.ToUpper & "&Cod=" & Me.txtCodUsuario.Value & "&Ent=X", False)
        Catch ex As Exception
            Me.lblError.Text = "Se produjo el siguiente error: " & ex.Message
        End Try
    End Sub

    Private Sub btnEnviarContraseña_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarContraseña.Click
        Dim dt As DataTable = New DataTable
        Dim strContraseña As String
        Dim strUsuario As String
        Try
            dt = objUsuario.gGetUsuario(Me.txtCodUsuario.Value)
            strContraseña = dt.Rows(0)("USR_PASW")
            strUsuario = dt.Rows(0)("USR_ACCE")
            strContraseña = Funciones.DecryptString(strContraseña, "©¹ã_[]0")
            EnvioMail(strUsuario, strContraseña)
            Me.lblError.Text = "Se notificó satisfactoriamente"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt.Dispose()
            dt = Nothing
        End Try
    End Sub

    Private Sub dgdEntidad_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdEntidad.ItemDataBound
        Dim btnEliminar As ImageButton
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            btnEliminar = CType(e.Item.FindControl("imgEliminar"), ImageButton)
            If Session.Item("IdTipoUsuario") = "04" Then 'Solo el Administrador de DEPSA podra anular usuarios
                btnEliminar.Enabled = True
            Else
                btnEliminar.Enabled = False
            End If
        End If
    End Sub

    Private Sub dgdEntidad_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdEntidad.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#D7D8B8'")
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFFFF'")
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objEntidad Is Nothing) Then
            objEntidad = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub btnResetear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetear.Click
        ResetearClaveFirma()
    End Sub
End Class
