Imports System.Data
Imports Infodepsa
Imports Library.AccesoDB
Public Class ActualizarContrase�a
    Inherits System.Web.UI.Page
    Private objAcceso As Acceso = New Acceso
    'Protected WithEvents cboPregSegu As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtRespSecre As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Private objFunciones As Library.AccesoBL.Funciones
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnActualizar As System.Web.UI.WebControls.Button
    'Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassRepNuevo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassNuevo As System.Web.UI.WebControls.TextBox
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtPassAnterior As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblUsuario As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkSesion As System.Web.UI.WebControls.HyperLink

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            pr_VALI_SESI()
            Me.lblUsuario.Text = Session.Item("UsuarioLogin")
            ListarPreguntaSeguridad()
            Bindatagrid()
        End If
    End Sub

    Private Sub ListarPreguntaSeguridad()
        Try
            Dim dt As New DataTable
            Me.cboPregSegu.Items.Clear()
            dt = objAcceso.gCADListarPreguntaSeguridad()
            Me.cboPregSegu.Items.Add(New ListItem(" ", "0"))
            For Each dr As DataRow In dt.Rows
                Me.cboPregSegu.Items.Add(New ListItem(dr("DSC_PGTSECR").ToUpper(), dr("COD_PGTSECR")))
            Next
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dt2 As New DataTable
            dt2 = objAcceso.gCADListarConfiguracionSeguridad(Me.lblUsuario.Text)

            If dt2.Rows.Count > 0 Then
                Dim strRespSecre As String
                strRespSecre = Library.AccesoBL.Funciones.DecryptString(CType(dt2.Rows(0)("RPT_SECR"), String).Trim, "���_[]0")
                Me.cboPregSegu.SelectedValue = CType(dt2.Rows(0)("COD_PGTSECR"), String).Trim
                Me.txtRespSecre.Text = strRespSecre
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.Message
        End Try
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            objFunciones = New Library.AccesoBL.Funciones
            Session.Add("Intentos", Session.Item("Intentos") + 1)
            If Me.txtPassNuevo.Text.Trim.Length >= 6 And Me.txtPassNuevo.Text.Trim.Length <= 12 Then
                If Session.Item("Intentos") < 4 Then
                    If Me.cboPregSegu.SelectedValue = 0 Then
                        pr_IMPR_MENS("Seleccione una pregunta de seguridad")
                        Return
                    End If
                    If Me.txtRespSecre.Text = "" Then
                        pr_IMPR_MENS("Debe ingresar la respuesta a la pregunta secreta")
                        Return
                    End If
                    Dim strContrase�aNueva As String
                    Dim strContrase�aAnterior As String
                    Dim strRespSecre As String
                    strContrase�aNueva = Library.AccesoBL.Funciones.EncryptString(Me.txtPassNuevo.Text.Trim, "���_[]0")
                    strContrase�aAnterior = Library.AccesoBL.Funciones.EncryptString(Me.txtPassAnterior.Text.Trim, "���_[]0")
                    strRespSecre = Library.AccesoBL.Funciones.EncryptString(Me.txtRespSecre.Text.Trim, "���_[]0")
                    If objAcceso.gCambiarContrase�a(Session.Item("IdUsuario"), strContrase�aAnterior,
                        strContrase�aNueva, System.Configuration.ConfigurationManager.AppSettings("NroDiasCaducaPass"),
                        Me.cboPregSegu.SelectedValue, strRespSecre) Then
                        Session.Remove("Intentos")
                        Response.Redirect("Login.aspx", False)
                        Exit Sub
                    Else
                        pr_IMPR_MENS("El password anterior no coincide")
                    End If
                Else
                    pr_IMPR_MENS("A excedido el n�mero de intentos, intente en otra ocasi�n")
                End If
            Else
                pr_IMPR_MENS("La Contrase�a nueva debe de tener como m�nimo 6 digitos y como m�ximo 12 digitos")
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = "ERROR: " & ex.ToString
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAcceso Is Nothing) Then
            objAcceso = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
