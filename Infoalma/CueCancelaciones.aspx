<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="CueCancelaciones.aspx.vb" Inherits="CueCancelaciones" %>

<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Relacion de Cancelaciones</title>
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaIni").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

            $("#txtFechaFinal").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaFin").click(function () {
                $("#txtFechaFinal").datepicker('show');
            });
        });

        function fn_DetDoc(sCO_UNID, sTI_DOCU_COBR, sNU_DOCU_COBR) {
            var ventana = 'Popup/CueImpresionDetalleDoc.aspx?sCO_UNID=' + sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOCU_COBR + '&sNU_DOCU_COBR=' + sNU_DOCU_COBR;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=800,height=600,toolbar=0,resizable=1")
        }
        function fn_Impresion(sNO_DOCU, sFE_INIC, sFE_FINA, sTI_DOCU) {
            var ventana = 'Popup/CueImpresionCancela.aspx?sNO_DOCU=' + sNO_DOCU + '&sFE_INIC=' + sFE_INIC + '&sFE_FINA=' + sFE_FINA + '&sTI_DOCU=' + sTI_DOCU;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");  
            window.open(ventana, "Retiro", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=500,height=400")
        }

        function AbrirAbono(sCO_UNID, sTI_DOCU_COBR, sNU_DOCU_COBR) {
            var ventana = 'Popup/PoputAbonos.aspx?sCO_UNID=' + sCO_UNID + '&sTI_DOCU_COBR=' + sTI_DOCU_COBR + '&sNU_DOCU_COBR=' + sNU_DOCU_COBR;
            //window.showModalDialog(ventana,window,"dialogWidth:100%;dialogHeight:100%");
            window.open(ventana, "Retiro", "left=240,top=200,width=600,height=300,toolbar=0,resizable=1")
        }

        document.onkeydown = function () {
            if (window.event && window.event.keyCode == 116) {
                window.event.keyCode = 505;
            }
            if (window.event && window.event.keyCode == 505) {
                return false;
            }
        }

        function ValidarFechas() {
            var fchIni;
            var fchFin;

            fchIni = document.getElementById("txtFechaInicial").value;
            fchFin = document.getElementById("txtFechaFinal").value;

            fchIni = fchIni.substr(6, 4) + fchIni.substr(3, 2) + fchIni.substr(0, 2);
            fchFin = fchFin.substr(6, 4) + fchFin.substr(3, 2) + fchFin.substr(0, 2);

            if (fchFin == "" && fchIni == "") {
                alert("Debe ingresar por lo menos una fecha");
                return false;
            }

            if (fchFin != "" && fchIni != "") {
                if (fchIni > fchFin) {
                    alert("La fecha final no puede ser mayor a la fecha inicial");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        var primerslap = false;
        var segundoslap = false;
        function IsNumeric(valor) {
            var log = valor.length; var sw = "S";
            for (x = 0; x < log; x++) {
                v1 = valor.substr(x, 1);
                v2 = parseInt(v1);
                //Compruebo si es un valor num�rico 
                if (isNaN(v2)) { sw = "N"; }
            }
            if (sw == "S") { return true; } else { return false; }
        }
        function ValidaFecha(sFecha) {
            var fecha1 = sFecha
            var fecha2 = document.getElementById('txtFechaInicial').value
            var miFecha1 = new Date(fecha1.substr(6, 4), fecha1.substr(3, 2), fecha1.substr(0, 2))
            var miFecha2 = new Date(fecha2.substr(6, 4), fecha2.substr(3, 2), fecha2.substr(0, 2))
            var diferencia = (miFecha1.getTime() �- miFecha2.getTime()) / (1000 * 60 * 60 * 24)
            //alert (diferencia);

            if (diferencia > 540) {
                window.open("Popup/SolicitarInformacion.aspx", "Vencimientos", "top=250,left=300,toolbar=0,status=0,directories=0,menubar=0,scrollbars=1,resize=0,width=440,height=300");
                return false;
            }
            else {
                return true;
            }
        }
        function formateafecha(fecha) {
            var long = fecha.length;
            var dia;
            var mes;
            var ano;

            if ((long >= 2) && (primerslap == false)) {
                dia = fecha.substr(0, 2);
                if ((IsNumeric(dia) == true) && (dia <= 31) && (dia != "00")) { fecha = fecha.substr(0, 2) + "/" + fecha.substr(3, 7); primerslap = true; }
                else { fecha = ""; primerslap = false; }
            }
            else {
                dia = fecha.substr(0, 1);
                if (IsNumeric(dia) == false)
                { fecha = ""; }
                if ((long <= 2) && (primerslap = true)) { fecha = fecha.substr(0, 1); primerslap = false; }
            }
            if ((long >= 5) && (segundoslap == false)) {
                mes = fecha.substr(3, 2);
                if ((IsNumeric(mes) == true) && (mes <= 12) && (mes != "00")) { fecha = fecha.substr(0, 5) + "/" + fecha.substr(6, 4); segundoslap = true; }
                else { fecha = fecha.substr(0, 3);; segundoslap = false; }
            }
            else { if ((long <= 5) && (segundoslap = true)) { fecha = fecha.substr(0, 4); segundoslap = false; } }
            if (long >= 7) {
                ano = fecha.substr(6, 4);
                if (IsNumeric(ano) == false) { fecha = fecha.substr(0, 6); }
                else { if (long == 10) { if ((ano == 0) || (ano < 1900) || (ano > 2100)) { fecha = fecha.substr(0, 6); } } }
            }

            if (long >= 10) {
                fecha = fecha.substr(0, 10);
                dia = fecha.substr(0, 2);
                mes = fecha.substr(3, 2);
                ano = fecha.substr(6, 4);
                // A�o no viciesto y es febrero y el dia es mayor a 28 
                if ((ano % 4 != 0) && (mes == 02) && (dia > 28)) { fecha = fecha.substr(0, 2) + "/"; }
            }
            return (fecha);
        }
		//-----------------------------------Fin formatear fecha	
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0"
    text="#e0e0c0">
    <form id="Form1" method="post" runat="server">
        <table id="Table2" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <uc1:Header ID="Header1" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="0" cellpadding="0" width="100%" height="400">
                        <tr>
                            <td valign="top">
                                <table id="Table4" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tr>
                                        <td></td>
                                        <td width="100%">
                                            <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" rowspan="3">
                                            <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                        </td>
                                        <td valign="top" width="100%">
                                            <table id="Table6" border="0" cellspacing="0" cellpadding="0" width="670" align="center">
                                                <tr>
                                                    <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r1_c2.gif"></td>
                                                    <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td background="Images/table_r2_c1.gif" width="6"></td>
                                                    <td>
                                                        <table id="Table7" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td class="Text" width="15%">Tipo Documento :</td>
                                                                <td class="Text" width="29%">
                                                                    <asp:DropDownList ID="cboTipoDocu" runat="server" CssClass="Text"></asp:DropDownList></td>
                                                                <td class="Text" width="18%">Fecha Cancelaci�n</td>
                                                                <td class="text" width="38%">
                                                                    <table id="Table5" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td class="Text" width="10%">Del:</td>
                                                                            <td>
                                                                                <input id="txtFechaInicial" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                maxlength="10" size="6" name="txtFechaInicial" runat="server"></td>
                                                                            <td>
                                                                                <input id="btnFechaIni" class="text" value="..."
                                                                                    type="button" name="btnFechaIni"></td>
                                                                            <td class="Text" width="10%">Al:</td>
                                                                            <td class="Text">
                                                                                <input id="txtFechaFinal" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                                maxlength="10" size="6" name="txtFechaFinal" runat="server"></td>
                                                                            <td class="Text">
                                                                                <input id="btnFechaFin" class="text" value="..."
                                                                                    type="button" name="btnFechaFin"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td background="Images/table_r2_c3.gif" width="6"></td>
                                                </tr>
                                                <tr>
                                                    <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                    <td height="6" background="Images/table_r3_c2.gif"></td>
                                                    <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                <tr>
                                                    <td width="80">
                                                        <asp:Button ID="btnBuscar" runat="server" CssClass="btn" Text="Buscar"></asp:Button></td>
                                                    <td width="80">
                                                        <input id="btnImprimir" class="btn" onclick='fn_Impresion("<%=cboTipoDocu.SelectedItem.Text%>","<%=txtFechaInicial.Value%>","<%=txtFechaFinal.Value%>","<%=cboTipoDocu.SelectedItem.Value%>")' value="Imprimir" type="button"></td>
                                                    <td width="80">
                                                        <asp:Button ID="btnExportar" runat="server" CssClass="btn" Text="Exportar"></asp:Button></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False" AllowPaging="True">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Left" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="Tipo Doc.">
                                                        <HeaderStyle HorizontalAlign="Center" Width="50px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="NU_DOCU_COBR" HeaderText="Nro.Doc.">
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FEC. DOCUMENTO" HeaderText="Fec.Doc." DataFormatString="{0:d}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="FEC. CANCELACION" HeaderText="Fec.Cancelac." DataFormatString="{0:d}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SOLES" HeaderText="Importe S/." DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DOLARES" HeaderText="Importe $." DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="False" HeaderImageUrl="CO_UNID" HeaderText="CO_UNID"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Abono">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgVer" runat="server" ImageUrl="Images/Ver.jpg" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                            </asp:DataGrid><br>
                                            <asp:DataGrid ID="dgResultado1" runat="server" CssClass="gv" Width="50%" BorderColor="Gainsboro"
                                                AutoGenerateColumns="False">
                                                <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                <ItemStyle CssClass="gvRow"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Left" CssClass="gvHeader"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="TOT_SOL" HeaderText="Total Soles" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TOT_DOL" HeaderText="Total D&#243;lares" DataFormatString="{0:#,##0.00}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle"></HeaderStyle>
                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Bottom"></ItemStyle>
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle CssClass="gvPager"></PagerStyle>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer1" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
