<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConfiguracionMenu.aspx.vb" Inherits="ConfiguracionMenu" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ConfiguracionMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		}  
		
		function Ocultar()
			{
			Estado.style.display='none';			
			}
	/*function ValidaPagInicio(vid)
	{
		var retorno = false;
		
		for(i=0; i < document.forms[0].elements.length; i++)
		{
			elm = document.forms[0].elements[i];
			if(elm.type == 'checkbox' && elm.id.search('chkPagi')==true)
			{
				if(elm.id != vid)
				{
					if document.getElementById(vid).checked == true
					{
						elm.disabled = 'disabled';
					}
					else
					{
						elm.disabled = 'enabled';
					}
				}
			}
		}
		if(retorno == false)
		{
			alert("Seleccione por lo menos un registro para continuar.");
		}
		return retorno;
	}*/			
		</script>
	</HEAD>
	<body leftMargin="0" topMargin="0" bottomMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="113">
						<uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Titulo1" height="20">CONFIGURACI�N DEL MEN�</TD>
							</TR>
							<TR>
								<TD class="td">
									<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="630" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text" width="7%">Grupo</TD>
														<TD class="text" align="center" width="1%">:</TD>
														<TD class="text" width="60%" colSpan="3">&nbsp;
															<asp:dropdownlist id="cboGrupo" runat="server" CssClass="Text" DataTextField="DES" DataValueField="COD"
																Width="350px">
																<asp:ListItem Value="Presidencia del Directorio">Presidencia del Directorio</asp:ListItem>
																<asp:ListItem Value="Contabilidad">Contabilidad</asp:ListItem>
																<asp:ListItem Value="Tesoreria">Tesoreria</asp:ListItem>
															</asp:dropdownlist></TD>
														<TD class="text" style="WIDTH: 84px">
															<asp:button id="btnBuscar" runat="server" CssClass="btn" Text="Mostrar"></asp:button></TD>
														<TD class="text"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Resultado :
									<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										AutoGenerateColumns="False" AllowPaging="True" PageSize="30">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NO_MENU" HeaderText="Men&#250;"></asp:BoundColumn>
											<asp:BoundColumn DataField="NO_MENU2" HeaderText="Sub Men&#250;"></asp:BoundColumn>
											<asp:BoundColumn DataField="DE_MENU" HeaderText="Descripci&#243;n">
												<HeaderStyle Width="200px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Pag. Inic.">
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox id=chkPagi runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.FL_PAGI") %>'>
													</asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Sel">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="60" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkSel runat="server" CssClass="Text" Checked='<%# DataBinder.Eval(Container, "DataItem.FL_SELE") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="CO_MENU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="NI_MENU"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="PA_MENU"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="center">
									<asp:button id="btnGrabar" runat="server" CssClass="btn" Text="Grabar"></asp:button></TD>
							</TR>
							<TR>
								<TD class="td">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD class="td" id="Estado"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
