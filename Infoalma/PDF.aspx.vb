Imports System.IO
Public Class PDF
    Inherits System.Web.UI.Page
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            Dim NombrePdf As String = Request.QueryString("Cod")
            Dim objStream As Stream
            objStream = New FileStream(strPath & NombrePdf & ".pdf", FileMode.Open)
            Dim FileSize As Long
            FileSize = objStream.Length

            Dim Buffer(CInt(FileSize)) As Byte
            objStream.Read(Buffer, 0, CInt(FileSize))
            objStream.Close()

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/pdf"
            Response.BinaryWrite(Buffer)
            Response.Flush()
            Response.Close()
        Catch ex As Exception
            Response.Write("Esta Orden de retiro no fue hecha por la Web")
        End Try
    End Sub
End Class
