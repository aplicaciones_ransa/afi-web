Imports Library.AccesoDB
Imports System.IO

Public Class VisorDepsanet
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "20") Then
            Try
                Dim drPDF As Data.DataRow
                drPDF = objDocumento.gCADGelArchivoPDF(Request.QueryString("sNU_TITU")).Rows(0)
                Response.Clear()
                Response.AddHeader("Content-Length", CType(drPDF("DOC_PDF"), Byte()).Length.ToString())
                Response.Buffer = True
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(CType(drPDF("DOC_PDF"), Byte()))
            Catch ex As Exception
                Response.Write(ex.Message)
            Finally
                Response.Flush()
                Response.Close()
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
