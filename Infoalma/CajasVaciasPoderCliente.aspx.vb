Imports System.Data

Public Class CajasVaciasPoderCliente
    Inherits System.Web.UI.Page

    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnExportar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnVerReporte As System.Web.UI.WebControls.Button
    'Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    'Protected WithEvents dgdResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblRegistros As System.Web.UI.WebControls.Label
    'Protected WithEvents cboArea As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblTotalCajas As System.Web.UI.WebControls.Label

    Private objComun As LibCapaNegocio.clsCNComun
    Private objFuncion As LibCapaNegocio.clsFunciones
    Private objReporte As LibCapaNegocio.clsCNReporte
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    Private dvTemporal As DataView
    Private GridExporta As New DataGrid
    Private IntPase As New Integer


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    '  Protected WithEvents txtFechaHasta As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Request.IsAuthenticated) And ValidarPagina("PAG19") Then
            Try
                'If ValidarPagina("PAG19") = False Then Exit Sub
                Me.lblError.Text = ""
                If Not IsPostBack Then
                    CargarFechas()
                    CargarCombos()
                    LlenaGrid()
                    NroTotalCajas()
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    Private Sub CargarCombos()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboArea(cboArea, CStr(Session.Item("CodCliente")), CStr(Session.Item("AreasPermitidas")), CStr(Session.Item("UsuarioLogin")), "*")
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgdResultado.PageIndexChanged
        dgdResultado.CurrentPageIndex = Args.NewPageIndex
        LlenaGrid()
        NroTotalCajas()
    End Sub

    Private Sub CargarFechas()
        objFuncion = New LibCapaNegocio.clsFunciones
        txtFechaHasta.Value = objFuncion.gstrFecha()
    End Sub
    Private Sub LlenaGrid()
        Try
            objReporte = New LibCapaNegocio.clsCNReporte
            objFuncion = New LibCapaNegocio.clsFunciones
            dvTemporal = New DataView(objReporte.gdrRepUnidadesVaciasPoderCliente(CStr(Session.Item("CodCliente")), CStr(Session.Item("CodClieAdic")),
                    cboArea.SelectedItem.Value, "", txtFechaHasta.Value, Session.Item("UsuarioLogin")))
            Session.Add("dvReporte", dvTemporal)
            If IntPase = 1 Then
                objFuncion.gCargaGrid(GridExporta, dvTemporal.Table)
                IntPase = 0
            Else
                objFuncion.gCargaGrid(dgdResultado, dvTemporal.Table)
            End If


            lblRegistros.Text = objReporte.intFilasAfectadas.ToString & " registros"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgdResultado.CurrentPageIndex = 0
        LlenaGrid()
        NroTotalCajas()
    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        objFuncion = New LibCapaNegocio.clsFunciones
        Me.EnableViewState = False
        IntPase = 1
        Dim dgGrid As DataGrid = GridExporta
        LlenaGrid()

        'objFuncion.ExportarAExcel("CajasVaciasPoderCliente.xls", Response, dgdResultado)
        objFuncion.ExportarAExcel("CajasVaciasPoderCliente.xls", Response, dgGrid)
        GridExporta.Dispose()
        GridExporta = Nothing
        Me.EnableViewState = True
    End Sub

    Private Sub NroTotalCajas()
        Dim diItem As DataGridItem
        Dim intTotalCajas As Integer = 0

        For Each diItem In dgdResultado.Items
            intTotalCajas += CInt(diItem.Cells(1).Text)
        Next
        lblTotalCajas.Text = intTotalCajas
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        Dim objSegu As New LibCapaNegocio.clsSeguridad
        With objSegu
            If Session.Item("PaginasPermitidas") = Nothing Then
                Return False
                Exit Function
            End If
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
        If Me.dgdResultado.Items.Count > 0 Then
            Server.Transfer("Reportes/RepCajasVaciasPoderCliente.aspx")
        Else
            lblError.Text = "Falta informacion para imprimir"
        End If
    End Sub

    ReadOnly Property pFE_EXIS() As String
        Get
            Return Me.txtFechaHasta.Value
        End Get
    End Property
    ReadOnly Property pDE_AREA() As String
        Get
            Return IIf(Me.cboArea.SelectedItem.Value = "", "TODOS", Me.cboArea.SelectedItem.Text)
        End Get
    End Property

    ReadOnly Property pTO_CAJA() As String
        Get
            Return lblTotalCajas.Text
        End Get
    End Property
End Class

