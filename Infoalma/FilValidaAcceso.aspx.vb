Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class FilValidaAcceso
    Inherits System.Web.UI.Page
    Private objComun As LibCapaNegocio.clsCNComun
    Private objCliente As LibCapaNegocio.clsCNCliente
    Private objUsuario As LibCapaNegocio.clsCNUsuario
    Private objAccesoWeb As clsCNAccesosWeb
    Private objmenu As LibCapaNegocio.clsCNMenu
    Private objSegu As LibCapaNegocio.clsSeguridad
    Private objConsulta As LibCapaNegocio.clsCNConsulta
    Private intCodModulo As Integer = 1
    'Private objUsuario As LibCapaNegocio.clsCNUsuario
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents btnAceptar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboCliente As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        pr_VALI_SESI()
        Try
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "DEPSAFIL"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                If Session.Item("IdTipoUsuarioFiles") = "EXTERNO" Then
                    objUsuario = New LibCapaNegocio.clsCNUsuario
                    With objUsuario
                        If .gConsigueTipoCliente(Session.Item("UsuarioLogin")) = True Then
                            Session.Add("CodCliente", .strCodCliente)
                            Session.Add("NomCliente", .strNomCliente)
                            Session.Add("CodEmpresa", .strCodEmpresa)
                            Session.Add("blnDepsaFile", .blnDepsaFile)

                            Session.Add("CodUnidad", "001")
                            Session.Add("CodPersAuto", "004")
                            Session.Add("DesPersAuto", "")
                            Session.Add("RequerimientoActivo", "0")
                            Session.Add("NroConsultas", "0")
                            Session.Add("CodModulo", intCodModulo)
                            Session.Add("Seleccionados", "-")
                        Else
                            Me.lblMensaje.Text = .strMensajeError
                            Exit Sub
                        End If
                    End With
                Else
                    lblMensaje.Text = "Acceso denegado! Usuario interno no tiene acceso"
                    Exit Sub
                End If
                RegistrarPaginasPermitidas()
                If ValidarPaginasPermitidas() = True Then
                    'PoliticaCache()
                    RegistrarPaginaInicio()
                    If (Request.IsAuthenticated) And ValidarPagina("PAG") Then
                        If Not IsPostBack Then
                            ConseguirAreasPermitidas()
                            CargarCombo()
                            VerificarClienteAdicional()
                            Me.btnAceptar.Enabled = True
                            Me.cboCliente.Enabled = True
                        End If
                    Else
                        Response.Redirect("Salir.aspx?caduco=1", False)
                    End If
                End If
            End If
        Catch ex As Exception
            lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(0, 1), Session.Item("IdSico"),
                            Session.Item("NombreEntidad"), "C", "DEPSAFIL", "VALIDA ACCESO DEPSAFILE", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
        RegistrarClienteAdicional()
        '        EliminarTemporal(sender)
        RedireccionarPaginaInicio()

        Exit Sub
    End Sub

    Private Sub CargarCombo()
        objComun = New LibCapaNegocio.clsCNComun
        objComun.gCargarComboClienteAdicional(cboCliente, CStr(Session.Item("CodCliente")))
    End Sub

    Private Sub VerificarClienteAdicional()
        If objComun.intFilasAfectadas = 1 Then
            RegistrarClienteAdicional()
            RedireccionarPaginaInicio()
            Exit Sub
        End If
    End Sub

    Private Sub RegistrarClienteAdicional()
        Session.Add("CodClieAdic", cboCliente.SelectedItem.Value)
        Session.Add("NomClieAdic", cboCliente.SelectedItem.Text)
    End Sub

    Private Sub RedireccionarPaginaInicio()
        EliminarTemporal()
        Response.Redirect(Session.Item("PaginaInicio").ToString)
    End Sub

    Private Sub ConseguirAreasPermitidas()
        Try
            objCliente = New LibCapaNegocio.clsCNCliente
            With objCliente
                .strCodCliente = Session.Item("CodCliente").ToString
                .strCodUsuario = Session.Item("UsuarioLogin").ToString
                Session.Add("AreasPermitidas", .gstrConseguirCadenaAreas())
            End With
        Catch ex As Exception
            lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Function ValidarPagina(ByVal strIDPagina As String) As Boolean
        objSegu = New LibCapaNegocio.clsSeguridad
        With objSegu
            .strIDPaginasPermitidas = Session.Item("PaginasPermitidas").ToString
            .strPaginaID = strIDPagina
            .bolAutenticado = Request.IsAuthenticated
            If Not (.gbolValidaPagina And .gbolValidaSesion) Then
                Response.Redirect(.strPagina, False)
                Return False
                Exit Function
            End If
        End With
        Return True
    End Function

    Private Sub RegistrarPaginasPermitidas()
        objmenu = New LibCapaNegocio.clsCNMenu
        With objmenu
            .strCodGrupo = Session.Item("CoGrup")
            .intCodModulo = intCodModulo
            Session.Add("PaginasPermitidas", .gstrCadenaMenuAcceso)
        End With
    End Sub
    Private Function ValidarPaginasPermitidas() As Boolean
        If Session.Item("PaginasPermitidas") = "" Or Session.Item("PaginasPermitidas") = Nothing Then
            lblMensaje.Text = "No se ha aperturado su perfil para el uso del sistema. Consulte con su oficina de Informática."
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub RegistrarPaginaInicio()
        objmenu = New LibCapaNegocio.clsCNMenu
        With objmenu
            .strCodGrupo = Session.Item("CoGrup")
            .intCodModulo = intCodModulo
            Session.Add("PaginaInicio", .gstrConseguirPaginaInicio())
        End With
    End Sub
    'Private Sub EliminarTemporal(ByVal objSender As Object)
    Private Sub EliminarTemporal()
        Try
            Dim j As Integer = 0
            Dim intNroConsultas As Integer = CInt(Session.Item("NroConsultas"))
            objConsulta = New LibCapaNegocio.clsCNConsulta
            'If objConsulta.gbolEliminaConsultaTemporal(CStr(Session.Item("Cliente")), Session.Item("UsuarioLogin")) Then
            If objConsulta.gbolEliminaConsultaTemporal(CStr(Session.Item("codcli")), Session.Item("UsuarioLogin")) Then
            Else
                'Me.lblError.Text = "ERROR en el procedimiento de eliminación."
            End If
        Catch ex As Exception
            'Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objComun Is Nothing) Then
            objComun = Nothing
        End If
        If Not (objCliente Is Nothing) Then
            objCliente = Nothing
        End If
        If Not (objUsuario Is Nothing) Then
            objUsuario = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objmenu Is Nothing) Then
            objmenu = Nothing
        End If
        If Not (objSegu Is Nothing) Then
            objSegu = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
