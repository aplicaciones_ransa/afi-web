<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ProrrogaConsultar.aspx.vb" Inherits="ProrrogaConsultar" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ProrrogaConsultar</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="5">
									<uc1:MenuInfo id="MenuInfo2" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="630" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="text">Nro Warrant:</TD>
														<TD>
															<asp:textbox id="txtNroWarrant" runat="server" CssClass="Text" Width="150px"></asp:textbox></TD>
														<TD class="text">Estado:</TD>
														<TD>
															<asp:dropdownlist id="cboEstado" runat="server" CssClass="Text" Width="150px"></asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="Text" vAlign="top" align="center">
									<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="300">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										PageSize="15" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NRO_DOCU" HeaderText="Nro Warrant">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_TIPDOC" HeaderText="Tipo Docum">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SEC_PRORR" HeaderText="Secuencia"></asp:BoundColumn>
											<asp:BoundColumn DataField="FCH_1VEN" HeaderText="Fecha">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Depositante">
												<HeaderStyle Width="150px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DSC_MERC" HeaderText="Dsc Mercader&#237;a">
												<HeaderStyle Width="150px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ESTA" HeaderText="Estado">
												<HeaderStyle Width="60px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Ver">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEditar" onclick="imgEditar_Click" runat="server" ToolTip="Ver detalles" ImageUrl="Images/Ver.JPG"
																	CausesValidation="False"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPDOC"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="SEC_PRORR"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Eliminar">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" ToolTip="Eliminar Prorroga"
																	ImageUrl="Images/Eliminar.JPG" CausesValidation="False"></asp:ImageButton></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_ESTA"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center"></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer1" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
