Imports System.Data.SqlClient
Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class ProrrogaDetalle
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objDocumento As Documento = New Documento
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private strPuntoDecimal As String = System.Configuration.ConfigurationManager.AppSettings("PuntoDecimal")
    Private strPath = ConfigurationManager.AppSettings.Item("RutaPDFs")
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents txtMercaderia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboEstado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtFechaObligacion As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaRegistro As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtSecuencia As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents btnRegresar As System.Web.UI.WebControls.Button
    'Protected WithEvents btnGenerarPDF As System.Web.UI.WebControls.Button
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtFina As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodFina As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCliente As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodCliente As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtNroWarrant As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTipoDocumento As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtFechaAnterior As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaLimite As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents cboMonedaAdecuado As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboMonedaActual As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtSaldoSICO As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSaldoAdecuado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtSaldoActual As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtConCertificado As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents txtModalidad As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "17") Then
            Try
                Me.lblError.Text = ""
                Session.Remove("Mensaje")
                Me.btnGenerarPDF.Attributes.Add("onclick", "javascript:if(Validar()==false){return false;}else{if(confirm('Esta seguro de generar PDF?')== false) return false;}")
                If Not IsPostBack Then
                    Dim strResult As String()
                    Me.lblOpcion.Text = "Datos del Documento"
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "17"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    Me.txtCliente.Enabled = False
                    Me.txtCodCliente.Enabled = False
                    Me.txtFina.Enabled = False
                    Me.txtCodFina.Enabled = False
                    Me.txtNroWarrant.Enabled = False
                    Me.txtTipoDocumento.Enabled = False
                    Me.txtMercaderia.Enabled = False
                    Me.txtFechaLimite.Disabled = True
                    Me.txtFechaAnterior.Disabled = True
                    Me.txtSecuencia.Enabled = False
                    Me.txtSaldoSICO.Enabled = False
                    Me.cboEstado.Enabled = False
                    loadMonedaActual()
                    loadMonedaAdecuado()
                    LlenaEstado()
                    Me.txtSecuencia.Text = Session.Item("NroSecu")
                    loadDataDocumento()
                    Dim Dia As String
                    Dim Mes As String
                    Dim Anio As String
                    Dia = "00" + CStr(Now.Day)
                    Mes = "00" + CStr(Now.Month)
                    Anio = "0000" + CStr(Now.Year)
                    Me.txtFechaRegistro.Value = Dia.Substring(Dia.Length - 2) & "/" & Mes.Substring(Mes.Length - 2) & "/" & Anio.Substring(Anio.Length - 4)
                End If
            Catch ex As Exception
                Me.lblError.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub LlenaEstado()
        objFunciones.GetLlenaEstado(Me.cboEstado, "G")
    End Sub

    Private Sub loadDataDocumento()
        Dim dtUsuario As DataTable
        dtUsuario = objDocumento.gGetDataProrroga(Session.Item("NroWarr"), Session.Item("NroSecu"), Session.Item("TipDoc"))
        If dtUsuario.Rows.Count = 1 Then
            If Not IsDBNull(dtUsuario.Rows(0)("EMI_CERT")) Then
                Me.txtConCertificado.Value = CType(dtUsuario.Rows(0)("EMI_CERT"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("MOD_PRORR")) Then
                Me.txtModalidad.Value = CType(dtUsuario.Rows(0)("MOD_PRORR"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("NOM_TIPDOC")) Then
                Me.txtTipoDocumento.Text = CType(dtUsuario.Rows(0)("NOM_TIPDOC"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("NRO_DOCU")) Then
                Me.txtNroWarrant.Text = CType(dtUsuario.Rows(0)("NRO_DOCU"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("COD_CLIE")) Then
                Me.txtCodCliente.Text = CType(dtUsuario.Rows(0)("COD_CLIE"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("NOM_CLIE")) Then
                Me.txtCliente.Text = CType(dtUsuario.Rows(0)("NOM_CLIE"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("COD_ENTFINA")) Then
                Me.txtCodFina.Text = CType(dtUsuario.Rows(0)("COD_ENTFINA"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("NOM_FINA")) Then
                Me.txtFina.Text = CType(dtUsuario.Rows(0)("NOM_FINA"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("EST_PRORR")) Then
                Me.cboEstado.SelectedValue = CType(dtUsuario.Rows(0)("EST_PRORR"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("DSC_MERC")) Then
                Me.txtMercaderia.Text = CType(dtUsuario.Rows(0)("DSC_MERC"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("FCH_LIM")) Then
                Me.txtFechaLimite.Value = CType(dtUsuario.Rows(0)("FCH_LIM"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("SAL_DOCU")) Then
                Me.txtSaldoSICO.Text = String.Format("{0:##,##0.00}", dtUsuario.Rows(0)("SAL_DOCU"))
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("FCH_1VEN")) Then
                Me.txtFechaAnterior.Value = CType(dtUsuario.Rows(0)("FCH_1VEN"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("SEC_SICO")) Then
                Me.txtSecuencia.Text = dtUsuario.Rows(0)("SEC_SICO")
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("SAL_ADEC")) Then
                Me.txtSaldoAdecuado.Value = String.Format("{0:##,##0.00}", dtUsuario.Rows(0)("SAL_ADEC"))
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("SAL_ACTU")) Then
                Me.txtSaldoActual.Value = String.Format("{0:##,##0.00}", dtUsuario.Rows(0)("SAL_ACTU"))
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("MON_ADEC")) Then
                Me.cboMonedaAdecuado.SelectedValue = CType(dtUsuario.Rows(0)("MON_ADEC"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("MON_ACTU")) Then
                Me.cboMonedaActual.SelectedValue = CType(dtUsuario.Rows(0)("MON_ACTU"), String).Trim
            End If
            If Not IsDBNull(dtUsuario.Rows(0)("FCH_OBLIGACION")) Then
                Me.txtFechaObligacion.Value = dtUsuario.Rows(0)("FCH_OBLIGACION")
            End If
            If dtUsuario.Rows(0)("GENERA") = 0 And Session.Item("IdTipoEntidad") = "02" And (Session.Item("IdTipoUsuario") = "01" Or Session.Item("IdTipoUsuario") = "03") Then
                Me.btnGenerarPDF.Enabled = True
            Else
                Me.txtSaldoActual.Disabled = True
                Me.txtSaldoAdecuado.Disabled = True
                Me.txtFechaObligacion.Disabled = True
                Me.btnGenerarPDF.Enabled = False
                Me.cboMonedaAdecuado.Enabled = False
                Me.cboMonedaActual.Enabled = False
            End If
        Else
            Me.lblMensaje.Text = "No se encontr� informaci�n"
        End If
        dtUsuario.Dispose()
        dtUsuario = Nothing
    End Sub

    Private Sub loadMonedaActual()
        Try
            objFunciones.GetMonedas(Me.cboMonedaActual)
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        End Try
    End Sub

    Private Sub loadMonedaAdecuado()
        Try
            objFunciones.GetMonedas(Me.cboMonedaAdecuado)
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.ToString
        End Try
    End Sub

    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("ProrrogaConsultar.aspx")
    End Sub

    Private Sub btnGenerarPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarPDF.Click
        If Me.cboMonedaActual.SelectedValue <> 0 And Me.cboMonedaAdecuado.SelectedValue <> 0 Then
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "C", "17", "EMITIR PRORROGA", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

            Dim objConexion As SqlConnection
            Dim objTrans As SqlTransaction
            objConexion = New SqlConnection(strConn)
            objConexion.Open()
            objTrans = objConexion.BeginTransaction()
            Dim dtMail As New DataTable
            Try
                Dim strEmails As String
                Dim strFchProrroga As String
                Dim strFchRegistro As String
                Dim strNombreArchivoPDF As String
                Dim strSaldoAdecuado As String
                Dim strDecimal As String
                Dim strSaldoActual As String
                Dim strDecimalSA As String
                Dim strEmailsConAdjunto As String = ""
                Dim strEmailsSinAdjunto As String = ""
                strDecimal = Me.txtSaldoAdecuado.Value.Substring(Len(txtSaldoAdecuado.Value) - 2, 2)
                strSaldoAdecuado = Me.txtSaldoAdecuado.Value.Substring(0, Len(txtSaldoAdecuado.Value) - 3)
                strSaldoAdecuado = strSaldoAdecuado.Replace(".", "")
                strSaldoAdecuado = strSaldoAdecuado.Replace(",", "")
                strDecimalSA = Me.txtSaldoActual.Value.Substring(Len(txtSaldoActual.Value) - 2, 2)
                strSaldoActual = Me.txtSaldoActual.Value.Substring(0, Len(txtSaldoActual.Value) - 3)
                strSaldoActual = strSaldoActual.Replace(".", "")
                strSaldoActual = strSaldoActual.Replace(",", "")

                strFchRegistro = Me.txtFechaRegistro.Value.Substring(6, 4) & Me.txtFechaRegistro.Value.Substring(3, 2) & Me.txtFechaRegistro.Value.Substring(0, 2)
                strFchProrroga = Me.txtFechaObligacion.Value.Substring(6, 4) & Me.txtFechaObligacion.Value.Substring(3, 2) & Me.txtFechaObligacion.Value.Substring(0, 2)
                dtMail = objDocumento.gUpdProrroga(Me.txtNroWarrant.Text, Session.Item("NroSecu"), Session.Item("TipDoc"),
                                                     strFchRegistro, strFchProrroga, strSaldoAdecuado & "." & strDecimal, strSaldoActual & "." & strDecimalSA, Me.cboMonedaAdecuado.SelectedValue, Me.cboMonedaActual.SelectedValue, Session.Item("IdUsuario"), objTrans)
                strNombreArchivoPDF = strPath & "05" & Session.Item("TipDoc") & Me.txtNroWarrant.Text & Me.txtSecuencia.Text & ".pdf"

                objFunciones.GetReporteProrroga(strNombreArchivoPDF, Me.txtNroWarrant.Text, Me.txtFechaRegistro.Value, Me.txtFechaObligacion.Value,
                strSaldoAdecuado & strPuntoDecimal & strDecimal, strSaldoActual & strPuntoDecimal & strDecimalSA, Me.cboMonedaAdecuado.SelectedItem.Text, Me.cboMonedaActual.SelectedItem.Text,
                Me.txtFechaLimite.Value, Me.txtFechaAnterior.Value, Me.txtConCertificado.Value, Me.txtModalidad.Value, Me.txtCliente.Text, Me.txtFina.Text)

                If dtMail.Rows.Count > 0 Then
                    If dtMail.Rows(0)("nro") = 0 Then
                        If dtMail.Rows.Count > 0 Then
                            For i As Integer = 0 To dtMail.Rows.Count - 1
                                If dtMail.Rows(i)("FLG_ADJU") = True Then
                                    strEmailsConAdjunto = strEmailsConAdjunto & dtMail.Rows(i)("DIR_EMAI") & ","
                                Else
                                    strEmailsSinAdjunto = strEmailsSinAdjunto & dtMail.Rows(i)("DIR_EMAI") & ","
                                End If
                            Next
                            If strEmailsConAdjunto <> "" Then
                                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsConAdjunto.Remove(strEmailsConAdjunto.Length - 1, 1),
                                "Aprobaci�n de Prorroga " & Me.txtCliente.Text,
                                 "Estimados Se�ores:<br><br>" &
                                "Deberan ingresar los usuarios aprobadores del cliente " & Me.txtCliente.Text & " para poder aprobar la siguiente prorroga " &
                                "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & Me.txtNroWarrant.Text & "</FONT></STRONG><br>" &
                                "<br>Secuencial de prorroga N�: <STRONG><FONT color='#330099'>" & Me.txtSecuencia.Text & "</FONT></STRONG><br>" &
                                "Depositante: <STRONG><FONT color='#330099'>" & Me.txtCliente.Text & "</FONT></STRONG>" &
                                "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", strNombreArchivoPDF)
                            End If
                            If strEmailsSinAdjunto <> "" Then
                                objFunciones.SendMail("Alma Per� � Sistema AFI <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">", strEmailsSinAdjunto.Remove(strEmailsSinAdjunto.Length - 1, 1),
                                "Aprobaci�n de Prorroga " & Me.txtCliente.Text,
                                 "Estimados Se�ores:<br><br>" &
                                "Deberan ingresar los usuarios aprobadores del cliente " & Me.txtCliente.Text & " para poder aprobar la siguiente prorroga " &
                                "<br><br>Warrant(s) N�: <STRONG><FONT color='#330099'>" & Me.txtNroWarrant.Text & "</FONT></STRONG><br>" &
                                "<br>Secuencial de prorroga N�: <STRONG><FONT color='#330099'>" & Me.txtSecuencia.Text & "</FONT></STRONG><br>" &
                                "Depositante: <STRONG><FONT color='#330099'>" & Me.txtCliente.Text & "</FONT></STRONG>" &
                                "<br><br>Saludos.<br><br>S�rvanse ingresar a la web AFI para aprobaciones: <A href='https://afi.almaperu.com.pe'>Alma Per� - Sistema AFI</A>", "")
                            End If
                        End If
                    Else
                        Me.lblError.Text = "No se puedo grabar"
                        objTrans.Rollback()
                        objTrans.Dispose()
                        Exit Sub
                    End If
                Else
                    Me.lblError.Text = "No se env�o ningun correo"
                End If
                objTrans.Commit()
                objTrans.Dispose()
                Me.lblError.Text = "Se realiz� con exito la operaci�n"
            Catch ex As Exception
                objTrans.Rollback()
                objTrans.Dispose()
                Me.lblError.Text = "Se cancel� el proceso debido al siguiente error: " & ex.Message
            Finally
                dtMail.Dispose()
                dtMail = Nothing
            End Try
        Else
            Me.lblError.Text = "Seleccione tipo de moneda para los saldos ingresados"
        End If
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objFunciones Is Nothing) Then
            objFunciones = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
