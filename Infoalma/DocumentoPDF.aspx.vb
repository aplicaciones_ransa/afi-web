Imports System.IO
Imports Library.AccesoDB
Imports Library.AccesoBL
Imports System.Data
Imports Infodepsa
Imports System.Data.SqlClient
Imports DEPSA.LibCapaNegocio

Public Class DocumentoPDF
    Inherits System.Web.UI.Page
    Private objDocumento As Documento = New Documento
    Private objReportes As Reportes = New Reportes
    Private strPDFPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
    Private strPathFirmas As String = ConfigurationManager.AppSettings.Item("RutaFirmas")
    Private strEmpresa As String = ConfigurationManager.AppSettings("CoEmpresa")
    'Private oHashedData As New CAPICOM.HashedData
    Private strConn As String = System.Configuration.ConfigurationManager.AppSettings("ConnectionStringWE")
    Private objAccesoWeb As clsCNAccesosWeb
    'Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones

    Dim var As Integer
    Dim strIdUsuario As String
    Dim strNroDoc As String
    Dim strNomEnti As String
    Dim strNroLib As String
    Dim strPeriodoAnual As String
    Dim strIdTipoDocumento As String
    Dim strIdTipoEntidad As String
    Dim strIdTipoUsuario As String
    Dim strIdSico As String
    Dim strcontrasenia As String
    'Dim strstrfecha As String
    Dim strCertDigital As String
    Dim strCod_TipOtro As String
    Dim strCod_Doc As String
    Dim strDscMerc As String
    Dim strCod_Unidad As String
    Dim strflgVisFirm As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents ltrVisor As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents ltrFirmar2 As System.Web.UI.WebControls.Literal
    'Protected WithEvents ltrFirmar1 As System.Web.UI.WebControls.Literal
    'Protected WithEvents lblFirma As System.Web.UI.WebControls.Label
    'Protected WithEvents cntFirmas As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents txtArchivo_pdf As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtArchivo_pdf_firmado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNumSerie As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNroPagina As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtEstado As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtTimeStamp As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtNuevaContrasena As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents LtrFirma As System.Web.UI.WebControls.Literal
    'Protected WithEvents hlkFilemaster As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents ltrExpediente As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtDeItem As System.Web.UI.HtmlControls.HtmlInputText

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "1") Then
            If Not IsPostBack Then
                HiNavSel.Value = 0
                loadCabeDocumento()
                Me.btnRechazar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de rechazar el documento?')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function

    Private Sub loadCabeDocumento()
        Dim dtControles As New DataTable
        Dim dtIngreso001 As DataTable = New DataTable
        Try
            '<INPUT id = "txtArchivo_pdf" name="txtArchivo_pdf" runat="server">
            '<INPUT id = "txtArchivo_pdf_firmado" name="txtArchivo_pdf_firmado" runat="server">
            '<INPUT id = "txtNumSerie" name="txtNumSerie" runat="server">
            ' <INPUT id = "txtNroPagina" name="txtNroPagina" runat="server">
            '<INPUT id = "txtEstado" name="txtEstado" runat="server"> 
            ' <INPUT id = "txtTimeStamp" name="txtTimeStamp" runat="server">
            '<INPUT id = "txtContrasena" name="txtContrasena" runat="server"> 
            '<INPUT id = "txtNuevaContrasena" name="txtNuevaContrasena" runat="server">
            '===== Recorrer Datatable =====
            If Session.Item("IdTipoEntidad") = "01" Then
                Me.lblck.Enabled = True
                Me.lblck.Visible = True
                Me.ckimpr.Enabled = True
                Me.ckimpr.Visible = True
                If Session.Item("CKIMPRE") = True Then
                    Me.ckimpr.Checked = True
                Else
                    Me.ckimpr.Checked = False
                End If
            Else

                Me.lblck.Enabled = False
                Me.lblck.Visible = False
                Me.ckimpr.Enabled = False
                Me.ckimpr.Visible = False
            End If




            If Not dtIngreso001.Rows.Count = 0 Then
                dtIngreso001.Rows.Clear()
            Else
                dtIngreso001 = Session.Item("dtIngreso001")
            End If

            '=== Navegador de Documentos ====
            HiNavMax.Value = dtIngreso001.Rows.Count
            If HiNavMax.Value >= 1 Then
                HiNavMin.Value = 1
                'primera selecci�n
                If HiNavSel.Value = 0 Then
                    HiNavSel.Value = 1
                End If
            Else
                HiNavMin.Value = 0
            End If
            '=== Deshabilitando Botones de Navegacion
            OcultarControlesdeNavegacion()
            txtDeItem.Text = (HiNavSel.Value).ToString & " de " & (HiNavMax.Value).ToString
            ' =========== Asignando Variables ===========
            var = 0
            strNroDoc = "" : strNomEnti = "" : strNroLib = "" : strIdTipoDocumento = "" : strPeriodoAnual = "" : strCod_TipOtro = "" : strCod_Doc = ""
            strDscMerc = "" : strCod_Unidad = ""
            strIdUsuario = "" : strIdTipoEntidad = "" : strIdTipoUsuario = "" : strIdSico = "" : strcontrasenia = "" : strCertDigital = "" : strflgVisFirm = ""


            Dim hns As Integer = (CInt(HiNavSel.Value) - 1)
            'Valores de ssession enviados po la pagina DocumentoAprobacion :
            strNroDoc = dtIngreso001.Rows(hns).Item(1)
            strNomEnti = dtIngreso001.Rows(hns).Item(2) ' se envia pero no se usa
            strNroLib = dtIngreso001.Rows(hns).Item(3)
            strIdTipoDocumento = dtIngreso001.Rows(hns).Item(4)
            strPeriodoAnual = dtIngreso001.Rows(hns).Item(5)
            strCod_TipOtro = dtIngreso001.Rows(hns).Item(6)
            strCod_Doc = dtIngreso001.Rows(hns).Item(7) ' se envia pero no se usa
            strDscMerc = dtIngreso001.Rows(hns).Item(8)
            strCod_Unidad = dtIngreso001.Rows(hns).Item(9) ' se envia pero no se usa
            strflgVisFirm = dtIngreso001.Rows(hns).Item(10) ' visualiza el boton firma

            'Otros valores enviados por sesion: 
            var = Request.QueryString("var")
            Me.txtNroPagina.Value = var
            Me.txtEstado.Value = Session.Item("EstadoDoc")
            txtNumSerie.Value = Session.Item("NroCertificado")
            strIdUsuario = Session.Item("IdUsuario")
            strIdTipoEntidad = Session.Item("IdTipoEntidad")
            strIdTipoUsuario = Session.Item("IdTipoUsuario")
            strIdSico = Session.Item("IdSico")
            strcontrasenia = Session.Item("strcontrasenia")
            strCertDigital = Session.Item("CertDigital")

            'Enviando datos de session al visor :
            Session.Add("NroDoc", strNroDoc)
            Session.Add("NroLib", strNroLib)
            Session.Add("PeriodoAnual", strPeriodoAnual)
            Session.Add("IdTipoDocumento", strIdTipoDocumento)
            'limpiando Link al Filemaster:
            Me.ltrExpediente.Text = ""
            loadDataDocumento()


            Me.lblFirma.Text = ""
            Me.txtTimeStamp.Value = Now.ToString("dd/MM/yyyy hh:mm:ss tt")
            If var = 1 Then
                'Dim listaAux As String
                'Dim drWarrant As DataRow
                Dim strHTMLTable As String
                'Dim arrLista()
                'Dim arrDatosFila()
                'Dim i As Integer
                '----------------------------
                'oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
                '------------------------------
                Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visor.aspx?var=1' runat='server'></iframe>"
                'drWarrant = objDocumento.gSelArchivoPDF(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, var)

                'If Not IsDBNull(drWarrant("DOC_PDFFIRM")) Then
                '    txtArchivo_pdf.Value = Convert.ToBase64String(drWarrant("DOC_PDFFIRM"))
                'Else
                '    oHashedData.Hash(Convert.ToBase64String(drWarrant("DOC_PDFORIG")))
                '    Me.txtArchivo_pdf.Value = oHashedData.Value
                'End If

                strHTMLTable = "<TABLE WIDTH='100%' BORDER='0' CELLSPACING='4' CELLPADDING='0' bgColor='#f0f0f0' class='Text'>" _
                & "<THEAD><TR>" _
                & "<TD >#</TD><TD>Entidad</TD><TD>Nombres</TD><TD>Fecha Firma</TD><TD>Hora</TD>" _
                & "</TR></THEAD><TBODY>"
                Dim dtFirmantes As DataTable
                dtFirmantes = objDocumento.BLGetDatosFirmantes(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento)
                For Each dr As DataRow In dtFirmantes.Rows

                    strHTMLTable = strHTMLTable & "<TR><TD style=' color: #ABA5A4' >" & dr("ITEM") & "</TD>" & "<TD style=' color: #ABA5A4'>" & dr("NOM_TIPENTI") & "</TD>"
                    strHTMLTable = strHTMLTable & "<TD style=' color: #ABA5A4'>" & dr("NOMBRES") & "</TD style=' color: #ABA5A4'>" & "<TD style=' color: #ABA5A4'>" & dr("FECHA") & "</TD>"
                    strHTMLTable = strHTMLTable & "<TD style=' color: #ABA5A4'>" & dr("HORA") & "</TD></TR>"
                Next
                strHTMLTable = strHTMLTable & "</TBODY></TABLE>"
                LtrFirma.Text = strHTMLTable

            End If

            If var = 2 Then
                'Dim listaAux As String
                'Dim drWarrant As DataRow
                'Dim strHTMLTable As String
                'Dim arrLista()
                'Dim arrDatosFila()
                'Dim i As Integer
                Me.ltrVisor.Text = "<iframe id='fraArchivo' style='WIDTH: 100%; HEIGHT: 900px' name='fraArchivo' src='visor.aspx?var=1' runat='server'></iframe>"
            End If

            dtControles = objDocumento.gGetAccesoControles(strIdTipoEntidad, strIdTipoUsuario, strNroDoc,
                                                        strNroLib, strPeriodoAnual, strIdUsuario, strIdTipoDocumento, strIdSico, var)
            Dim strfirma As String

            For j As Integer = 0 To dtControles.Rows.Count - 1
                Select Case dtControles.Rows(j)("COD_CTRL")
                    Case "01"
                        If strcontrasenia <> Nothing Then
                            If DateDiff(DateInterval.Minute, Convert.ToDateTime(Session.Item("strfecha")), Now) > 5 Then
                                strfirma = "X"
                            Else
                                strfirma = "F"
                                Me.txtContrasena.Value = strcontrasenia
                            End If
                        Else
                            strfirma = "X"
                        End If

                        If var = 1 Then

                            If strflgVisFirm = "S" Then
                                If strCertDigital = True Then
                                    Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(1)' type='button' value='Firmar' name = 'cmdFirmar'>"
                                Else
                                    If strfirma = "X" Then
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                    Else
                                        Me.ltrFirmar1.Text = "<input id='cmdFirmar' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar'>"
                                    End If
                                End If
                            End If

                        End If
                    Case "07"
                        If var = 2 Then
                            If strcontrasenia <> Nothing Then
                                If DateDiff(DateInterval.Minute, Convert.ToDateTime(Session.Item("strfecha")), Now) > 5 Then
                                    strfirma = "X"
                                Else
                                    strfirma = "F"
                                    Me.txtContrasena.Value = strcontrasenia
                                End If
                            Else
                                strfirma = "X"
                            End If
                            If strCertDigital = True Then
                                Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='SubmitForm(2)' type='button'	value='Firmar' name='cmdFirmar2'>"
                            Else
                                If strfirma = "X" Then
                                    Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(0);' type='button' value='Firmar' name = 'cmdFirmar2'>"
                                Else
                                    Me.ltrFirmar2.Text = "<input id='cmdFirmar2' style='FONT-WEIGHT: bold; FONT-SIZE: 9pt; WIDTH: 100px; height:25px; COLOR: white; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: red' onclick='javascript:openWindow(1);' type='button' value='Firmar' name = 'cmdFirmar2'>"
                                End If
                            End If
                        End If
                    Case "04"
                        Me.btnRechazar.Visible = True
                    Case "00"
                        Me.lblFirma.Text = "Otro usuario esta intentando firmar el documento, intente en otro momento"
                End Select
            Next

            Dim drUsuario As DataRow
            drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)
            If (IsDBNull(drUsuario("ENT_FINA")) Or drUsuario("ENT_FINA") = "" Or drUsuario("ENT_FINA") = "0") And strIdTipoEntidad = "03" And Me.txtEstado.Value = "01" Then
                Session.Add("Mensaje", "Grabe un endosatario para el warrant antes de verificar")
                Me.ltrFirmar1.Text = ""
                Me.btnRechazar.Visible = False
            End If
            If (drUsuario("COD_ENTASOC") = "" And strIdTipoEntidad = "03" And drUsuario("FLG_DBLENDO") = True) Then
                Session.Add("Mensaje", "Seleccione una entidad asociada antes de verificar")
                Me.ltrFirmar1.Text = ""
                Me.btnRechazar.Visible = False
            End If
            Me.lblError.Text = Session.Item("Mensaje")
            Session.Add("Mensaje", "")
            If strIdTipoEntidad = "01" And (strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "03" Or strIdTipoDocumento = "10" Or strIdTipoDocumento = "12") Then
                Dim strWarrant As String()
                Dim strLink As String
                If (strIdTipoDocumento = "01" Or strIdTipoDocumento = "04" Or strIdTipoDocumento = "12" Or strIdTipoDocumento = "10") Then
                    strWarrant = Split(CStr(strNroDoc), "-")
                End If
                If (strIdTipoDocumento = "03") Then
                    strWarrant = Split(strDscMerc, "-")
                End If
                For Each strWar As String In strWarrant
                    strLink = strLink & "<A href='http://10.72.3.138/filemasterweb/warrant.aspx?IDServicioCliente=4&WAR=" & strWar & "&Grupo=FILEMAST'  Class='Ayuda' Target='_search'>VerExpediente:" & strWar & "</A><BR>"
                    'strLink = strLink & "<A href='http://181.65.210.150/filemasterweb/warrant.aspx?IDServicioCliente=4&WAR=" & strWar & "&Grupo=FILEMAST'  Class='Ayuda' Target='_search'>VerExpediente:" & strWar & "</A><BR>"
                Next
                Me.ltrExpediente.Text = strLink
            End If
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dtControles.Dispose()
            dtControles = Nothing
        End Try
    End Sub

    'Function listaFirmas(ByVal prmArchivo As Byte(), ByVal prmArchivoOriginal As Byte(), ByVal strFechaModi As DateTime) As String
    '    Try
    'Dim SignedData As CAPICOM.SignedData
    'Dim Signer As CAPICOM.Signer
    'Dim strLista As String
    'Dim strFila As String
    'Dim Atrib As CAPICOM.Attribute
    'Dim strNombreArchivo As String
    'Dim strTimeStamp As String
    'Dim Contenido As String
    'Dim mstream As System.IO.MemoryStream
    '------------------------------
    'oHashedData.Algorithm = CAPICOM.CAPICOM_HASH_ALGORITHM.CAPICOM_HASH_ALGORITHM_SHA1
    '-----------------------------
    'SignedData = New CAPICOM.SignedData
    'Contenido = Convert.ToBase64String(prmArchivo)
    'SignedData.Verify(Contenido, False, CAPICOM.CAPICOM_SIGNED_DATA_VERIFY_FLAG.CAPICOM_VERIFY_SIGNATURE_ONLY)
    '-------------------------------
    'Dim fechaAux As Date
    'fechaAux = Format("MM/dd/yyyy", ConfigurationManager.AppSettings.Item("FechaCambioValidador"))
    'If DateDiff(("d"), fechaAux, Date.Now) > 0 Then
    '    oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
    '    If oHashedData.Value <> SignedData.Content Then
    '        listaFirmas = "X"
    '        Exit Function
    '    End If
    'End If
    'If strFechaModi > CType(ConfigurationManager.AppSettings.Item("FechaCambioValidador"), DateTime) Then
    '    oHashedData.Hash(Convert.ToBase64String(prmArchivoOriginal))
    '    If oHashedData.Value <> SignedData.Content Then
    '        listaFirmas = "X"
    '        Exit Function
    '    End If
    'End If
    '--------------------------------
    'listaFirmas = ""
    'strLista = ""
    'Signer = New CAPICOM.Signer
    'For Each Signer In SignedData.Signers
    '    strFila = Signer.Certificate.GetInfo(0) & "*"
    '    For Each Atrib In Signer.AuthenticatedAttributes
    '        If Atrib.Name = 2 Then
    '            strTimeStamp = Atrib.Value
    '            strFila = strFila & strTimeStamp
    '        End If
    '    Next
    '    strLista = strLista & strFila & ";"
    'Next
    'listaFirmas = Mid(UCase(strLista), 1, Len(UCase(strLista)) - 1)
    'Catch ex As Exception
    '        Me.lblError.Text = ex.Message
    '    End Try
    'End Function

    Private Sub loadDataDocumento()
        Try
            Dim drUsuario As DataRow
            Dim strTipoDocumento As String
            Dim blnEndoso As Boolean
            drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)

            'LimpiarCampos()
            'txtCliente.Text = drUsuario("NOM_ENTI").ToString
            'txtFinanciador.Text = drUsuario("NOMBREFINANCIERA").ToString
            'txtAlmacen.Text = drUsuario("DE_DIRE_ALMA").ToString
            'txtNroOper.Text = drUsuario("NU_DOCU_OPER").ToString
            'txtNroDocu.Text = strNroDoc
            'txtFechEmis.Text = drUsuario("FECHACREACION").ToString
            'txtValorTotal.Text = drUsuario("CO_MONE") & " " & String.Format("{0:##,##0.00}", drUsuario("IMP_TOTA"))

            If strIdTipoDocumento = "20" Then
                strTipoDocumento = strCod_TipOtro
                blnEndoso = True
            Else
                strTipoDocumento = strIdTipoDocumento
                blnEndoso = False
            End If
            ' If Me.txtNroPagina.Value = "2" Then
            If Me.txtNroPagina.Value = "1" Then
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    'Nueva validacion- no tiene q pasar la prorroga porque sale error
                    If strIdTipoDocumento <> "02" And strIdTipoDocumento <> "05" And strIdTipoDocumento <> "13" And strIdTipoDocumento <> "14" And strIdTipoDocumento <> "21" And strIdTipoDocumento <> "11" And strIdTipoDocumento <> "09" Then
                        objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, strIdUsuario, "0", "")
                    End If

                    If objReportes.strMensaje <> "0" Then
                        Session.Add("Mensaje", objReportes.strMensaje)
                    End If
                End If
            End If

            If Me.txtNroPagina.Value = "1" Then
                If strIdTipoDocumento = "20" Or strIdTipoDocumento = "12" Then
                    If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                        objReportes.GeneraPDFWarrant(CStr(drUsuario("COD_DOC")), strPDFPath, strPathFirmas, blnEndoso, False, strTipoDocumento, strIdUsuario, "0", "")
                        If objReportes.strMensaje <> "0" Then
                            Session.Add("Mensaje", objReportes.strMensaje)
                        End If
                    End If
                    Exit Sub
                End If
                drUsuario = objDocumento.gGetDataDocumento(strNroDoc, strNroLib, strPeriodoAnual, strIdTipoDocumento, strEmpresa)
                If IsDBNull(drUsuario("DOC_PDFORIG")) Then
                    GrabaPDF(CType(drUsuario("NOM_PDF"), String).Trim)
                End If
            End If
            drUsuario = Nothing
        Catch ex As Exception
            Me.lblError.Text = "ERROR " + ex.Message
        End Try
    End Sub

    Private Sub GrabaPDF(ByVal strNombArch As String)
        Dim FilePath As String
        Dim fs As System.IO.FileStream
        Dim Contenido As Byte()
        FilePath = strPDFPath & strNombArch
        fs = New FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
        ReDim Contenido(fs.Length)
        fs.Read(Contenido, 0, Convert.ToInt32(fs.Length))
        objDocumento.gRegistraPDF(strIdUsuario, strNroDoc, strNroLib, strPeriodoAnual, Contenido, strIdTipoDocumento)
        fs.Close()
    End Sub

    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objDocumento Is Nothing) Then
            objDocumento = Nothing
        End If
        If Not (objReportes Is Nothing) Then
            objReportes = Nothing
        End If
        MyBase.Finalize()
    End Sub

    'Private Sub LimpiarCampos()
    '    txtCliente.Text = ""
    '    txtFinanciador.Text = ""
    '    txtAlmacen.Text = ""
    '    txtNroOper.Text = ""
    '    txtNroDocu.Text = ""
    '    txtFechEmis.Text = ""
    '    txtValorTotal.Text = ""
    'End Sub

    Protected Sub btnSiguiente_Click(sender As Object, e As EventArgs) Handles btnSiguiente.Click
        If Me.HiNavSel.Value < HiNavMax.Value Then
            Me.HiNavSel.Value = (CInt(HiNavSel.Value) + 1)
        End If
        OcultarControlesdeNavegacion()
        loadCabeDocumento()
    End Sub

    Protected Sub btnAnterior_Click(sender As Object, e As EventArgs) Handles btnAnterior.Click
        If Me.HiNavSel.Value > HiNavMin.Value Then
            Me.HiNavSel.Value = (CInt(HiNavSel.Value) - 1)
        End If
        OcultarControlesdeNavegacion()
        loadCabeDocumento()
    End Sub

    Private Sub OcultarControlesdeNavegacion()
        If HiNavSel.Value = HiNavMin.Value Then
            btnAnterior.Enabled = False
        Else
            btnAnterior.Enabled = True
        End If
        If HiNavSel.Value = HiNavMax.Value Then
            btnSiguiente.Enabled = False
        Else
            btnSiguiente.Enabled = True
        End If
    End Sub

    Protected Sub btnRechazar_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                Session.Item("NombreEntidad"), "C", "1", "RECHAZAR DOCUMENTO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim strResultado As String
        Dim objConexion As SqlConnection
        Dim objTrans As SqlTransaction
        objConexion = New SqlConnection(strConn)
        objConexion.Open()
        objTrans = objConexion.BeginTransaction()
        Try
            strResultado = objDocumento.gUpdRechazarDocumento(Session.Item("IdUsuario"), Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"), objTrans)
            If strResultado = "X" Then
                Me.lblError.Text = "No puede rechazar el documento, est� relacionado"
                objTrans.Rollback()
                objTrans.Dispose()
                Exit Sub
            End If

            If Session.Item("IdTipoDocumento") = "24" Then
                'objFunciones.SendMail("Depsa � Sistema Infodepsa <" & System.Configuration.ConfigurationManager.AppSettings.Item("EmailDepsa") & ">",
                'System.Configuration.ConfigurationManager.AppSettings.Item("EmailInspectoria"),
                '"Solicitud de Warrant fue rechazado - " & Me.txtCliente.Text,
                '"Se rechaz� la solicitud de warrants Nro: <STRONG><FONT color='#330099'> " & Session.Item("NroDoc") & "<br> </FONT></STRONG>" &
                '"Cliente: <STRONG><FONT color='#330099'> " & Me.txtCliente.Text & "<br> </FONT></STRONG>" &
                '"Rechazado por: <STRONG><FONT color='#330099'> " & UCase(Session.Item("UsuarioLogin")) & "</FONT></STRONG><br>" &
                '"<br><br>Atte.<br><br>S�rvanse ingresar al Infodepsa haciendo click sobre el siguiente vinculo: <A href='https://www.depsa.com.pe/Infodepsa/Login.aspx'>Depsa - Sistema Infodepsa</A>", "")
            Else
                objFunciones.gEnvioMailConAdjunto(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("PeriodoAnual"), Session.Item("IdTipoDocumento"),
                "El usuario " & Session.Item("UsuarioLogin") & " ha rechazado el documento", "", "S", "S", objTrans)
            End If
            objTrans.Commit()
            objTrans.Dispose()
            Session.Add("strMensaje", "Se rechaz� el documento correctamente")
            Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc"), False)
        Catch ex As Exception
            objTrans.Rollback()
            objTrans.Dispose()
            Me.lblError.Text = "Error: " & ex.Message
        End Try
    End Sub

    Protected Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Response.Redirect("DocumentoAprobacion.aspx?Estado=" & Session.Item("EstadoDoc") & "&var2=1", False)
    End Sub
    Protected Sub ckimpr_CheckedChanged(sender As Object, e As EventArgs) Handles ckimpr.CheckedChanged
        'Dim objConexion As SqlConnection
        'Dim objTrans As SqlTransaction
        'objConexion = New SqlConnection(strConn)
        ''objConexion.Open()
        'objTrans = objConexion.BeginTransaction()
        'Try
        'objDocumento.gUpdDocumento_IMPR(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("IdTipoDocumento"), ckimpr.Checked, objTrans)
        objDocumento.gUpdDocumento_IMPR(Session.Item("NroDoc"), Session.Item("NroLib"), Session.Item("IdTipoDocumento"), ckimpr.Checked)
        'Catch ex As Exception
        'objTrans.Rollback()
        'objTrans.Dispose()
        'Me.lblError.Text = "Error: " & ex.Message
        'End Try
    End Sub
End Class
