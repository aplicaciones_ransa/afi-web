<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConsultasxRequerimientoTemp.aspx.vb" Inherits="Infodepsa.ConsultasxRequerimientoTemp" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DEPSA Files - Consultas Generadas</title>
		<meta content="True" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="nanglesc@hotmail.com" name="author">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116){ 
			window.event.keyCode = 505;  
			} 
			if(window.event && window.event.keyCode == 505){  
			return false;     
			}  
		} 
		
		function Ocultar()
			{
			Estado.style.display='none';			
			}
		function MsgEliminar(v)
		{
			if (confirm('La consulta nro. '+v+' ser� eliminada. Presione OK para confimar \no Cancel para cancelar la operaci�n.')==false) 
				return false;
		}					
		</script>
	</HEAD>
	<body bottomMargin="0" bgColor="#f0f0f0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" bgColor="#f0f0f0" border="0">
				<TR>
					<TD colSpan="2"><uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="top" width="125"><uc1:menu id="Menu1" runat="server"></uc1:menu></TD>
					<TD vAlign="top">
						<TABLE id="Table1" cellSpacing="4" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="Titulo1" height="20">REQUERIMIENTO TEMPORAL</TD>
							</TR>
							<TR>
								<TD class="td" style="HEIGHT: 39px">
									<TABLE id="Table16" cellSpacing="0" cellPadding="0" border="0" align="center" width="630">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" style="WIDTH: 68px; HEIGHT: 20px">N�mero</TD>
														<TD class="Text" style="WIDTH: 19px; HEIGHT: 20px" align="center">:</TD>
														<TD class="text" style="WIDTH: 225px; HEIGHT: 20px">&nbsp;
															<asp:textbox id="txtNroRequerimiento" runat="server" ReadOnly="True" CssClass="Text"></asp:textbox></TD>
														<TD class="Text" style="WIDTH: 116px; HEIGHT: 20px" align="right"></TD>
														<TD class="Text" style="WIDTH: 47px; HEIGHT: 20px" align="center"></TD>
														<TD class="Text" style="HEIGHT: 20px"></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD class="text" height="20">Consultas :
									<asp:label id="lblRegistros" runat="server" CssClass="text"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top"><asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" AllowSorting="True"
										PageSize="20" AllowPaging="True" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False" BorderColor="Gainsboro">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="NU_CONS" SortExpression="NU_CONS" HeaderText="Nro.">
												<HeaderStyle Width="5%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ID_UNID" SortExpression="ID_UNID" HeaderText="Nro. Caja">
												<HeaderStyle Width="7%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_OBSE_0001" HeaderText="Detalle">
												<HeaderStyle Width="30%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="DE_TIPO_ENVI" HeaderText="Tipo Env&#237;o"></asp:BoundColumn>
											<asp:BoundColumn DataField="CO_USUA_CREA" HeaderText="Usuario"></asp:BoundColumn>
											<asp:BoundColumn DataField="FE_USUA_CREA" HeaderText="Fecha"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Eliminar">
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<ItemTemplate>
													<asp:ImageButton id="imgEliminar" onclick="imgEliminar_Click" runat="server" CausesValidation="False"
														ImageUrl="Images/Eliminar.JPG" ToolTip="Eliminar registro"></asp:ImageButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD class="td"><asp:button id="btnRegresar" runat="server" CssClass="btn" Text="Regresar"></asp:button></TD>
							</TR>
							<TR>
								<TD class="td"><asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2"><uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
