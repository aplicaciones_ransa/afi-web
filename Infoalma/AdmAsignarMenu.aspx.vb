Imports System.Data
Imports System.Web.Security
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AdmAsignarMenu
    Inherits System.Web.UI.Page
    Private objAccesoWeb As clsCNAccesosWeb
    Private objMenu As clsCNMenu
    Private objPerfil As clsCNPerfil
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents cboSistema As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboGrupo As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pr_VALI_SESI()
        Me.lblOpcion.Text = "Asignar men� a Grupo de Usuario"
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "MAESTRO_ACCESO") Then
            If Page.IsPostBack = False Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "MAESTRO_ACCESO"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de grabar?')== false) return false;")
                Try
                    llenarSistemas()
                    Me.cboSistema.SelectedIndex = 0
                    llenarGrupos(Me.cboSistema.SelectedValue.Trim)
                    BindDataGrid()
                Catch ex As Exception
                    Me.lblError.Text = ex.Message
                End Try
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub llenarGrupos(ByVal strco_sist As String)
        Me.cboGrupo.Items.Clear()
        Try
            objPerfil = New clsCNPerfil
            Dim dt As DataTable
            With objPerfil
                .gCNLlenarComboGrupos(strco_sist)
                dt = .fn_DevolverDataTable
            End With
            For Each dr As DataRow In dt.Rows
                Me.cboGrupo.Items.Add(New ListItem(dr("NO_GRUP").trim, dr("CO_GRUP").trim))
            Next
            Me.cboGrupo.SelectedIndex = 0
            BindDataGrid()
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub llenarSistemas()
        Me.cboSistema.Items.Clear()
        Try
            objPerfil = New clsCNPerfil
            Dim dt As DataTable
            With objPerfil
                .gCNLlenarSistemas()
                dt = .fn_DevolverDataTable
            End With
            For Each dr As DataRow In dt.Rows
                Me.cboSistema.Items.Add(New ListItem(dr("DE_LARG_PAR"), dr("NO_PAR")))
            Next
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub BindDataGrid()
        Try
            objMenu = New clsCNMenu
            With objMenu
                If Me.cboSistema.SelectedIndex = 0 Then
                    .gCNTraerMenusxSistema(Me.cboSistema.SelectedValue.Trim, Me.cboGrupo.SelectedValue.Trim, "1")
                ElseIf Me.cboSistema.SelectedIndex = 1 Then
                    .gCNTraerMenusxSistema(Me.cboSistema.SelectedValue.Trim, Me.cboGrupo.SelectedValue.Trim, "2")
                End If
                Me.dgResultado.DataSource = .fn_DevolverDataTable
                Me.dgResultado.DataBind()
                Me.lblError.Text = ""
            End With
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub cboSistema_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSistema.SelectedIndexChanged
        llenarGrupos(Me.cboSistema.SelectedValue.Trim)
    End Sub

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim btnestado As CheckBox
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            btnestado = CType(e.Item.FindControl("chkActivo"), CheckBox)
            If e.Item.DataItem("FLG_ACTI") = 0 Then
                btnestado.Checked = False
            Else
                btnestado.Checked = True
            End If
        End If
    End Sub

    Public Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs) Handles dgResultado.PageIndexChanged
        dgResultado.CurrentPageIndex = Args.NewPageIndex
        BindDataGrid()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Try
            objMenu = New clsCNMenu
            With objMenu
                If Me.cboSistema.SelectedIndex = 0 Then
                    .gCNGrabarPlantillaMenu(Me.dgResultado, Me.cboSistema.SelectedValue.Trim,
                    Me.cboGrupo.SelectedValue.Trim, Session.Item("IdUsuario"), "1")
                ElseIf Me.cboSistema.SelectedIndex = 1 Then
                    .gCNGrabarPlantillaMenu(Me.dgResultado, Me.cboSistema.SelectedValue.Trim,
                   Me.cboGrupo.SelectedValue.Trim, Session.Item("IdUsuario"), "2")
                End If
            End With
            BindDataGrid()
            objAccesoWeb = New clsCNAccesosWeb
            objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                            Session.Item("NombreEntidad"), "M", "MAESTRO_ACCESO", "ACTUALIZAR ACCESO POR GRUPO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)
            Me.lblError.Text = "Se grab� correctamente"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Private Sub cboGrupo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrupo.SelectedIndexChanged
        dgResultado.CurrentPageIndex = 0
        BindDataGrid()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        If Not (objMenu Is Nothing) Then
            objMenu = Nothing
        End If
        If Not (objPerfil Is Nothing) Then
            objPerfil = Nothing
        End If
        MyBase.Finalize()
    End Sub
End Class
