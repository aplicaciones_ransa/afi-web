Public Class DetalleRequ
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblNroCaja As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTipoDoc As System.Web.UI.WebControls.Label
    'Protected WithEvents lblDivision As System.Web.UI.WebControls.Label
    'Protected WithEvents lblSolicitante As System.Web.UI.WebControls.Label
    'Protected WithEvents lblSolicitado As System.Web.UI.WebControls.Label
    'Protected WithEvents lblNroRequerimiento As System.Web.UI.WebControls.Label
    'Protected WithEvents lblAviso As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private objRequerimiento As New LibCapaNegocio.clsCNRequerimiento
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Page.IsPostBack = False Then
            Me.lblDivision.Text = Request.QueryString("param3")
            Me.lblNroCaja.Text = Request.QueryString("param1")
            Me.lblNroRequerimiento.Text = Request.QueryString("param6")
            If Me.lblNroRequerimiento.Text = "" Then
                Me.lblAviso.Visible = True
            End If
            If objRequerimiento.gbolDetalle_Doc(Session.Item("CodCliente"), Request.QueryString("param6"), "") = True Then
                Me.lblSolicitante.Text = objRequerimiento.strNomUsuario
            End If
            Me.lblSolicitado.Text = Request.QueryString("param5")
            'Me.lblSolicitante.Text = Request.QueryString("param4")
            Me.lblTipoDoc.Text = Request.QueryString("param2")
        End If
    End Sub

End Class
