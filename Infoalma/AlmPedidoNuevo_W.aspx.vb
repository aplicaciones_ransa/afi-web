Imports System.Data
Imports Depsa.LibCapaNegocio
Imports Infodepsa

Public Class AlmPedidoNuevo_W
    Inherits System.Web.UI.Page
    Private objParametro As clsCNParametro
    Private objRetiro As clsCNRetiro
    Private objAccesoWeb As clsCNAccesosWeb
    Private objWarrant As clsCNWarrant
    Private objWMS As clsCNWMS
    Private bolApto As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents btnBuscar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents dgResultado As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents txtFechaInicial As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaFinal As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents txtFechaVenc As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents lblMensaje As System.Web.UI.WebControls.Label
    'Protected WithEvents hlkSolicitud As System.Web.UI.WebControls.HyperLink
    'Protected WithEvents btnAgregar As System.Web.UI.WebControls.Button
    'Protected WithEvents txtLote As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtCodProducto As System.Web.UI.WebControls.TextBox
    'Protected WithEvents cboReferencia As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents txtNroReferencia As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region
    Public Sub PoliticaCache()
        Dim PageCache As HttpCachePolicy = Page.Response.Cache
        PageCache.SetExpires(Now())
        PageCache.SetCacheability(HttpCacheability.NoCache)
        PageCache.SetNoStore()
        PageCache.SetRevalidation(HttpCacheRevalidation.AllCaches)
        PageCache.SetNoServerCaching()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strAgent As String = Request.UserAgent
        Dim myBrowserCapabilities As HttpBrowserCapabilities = Request.Browser
        Dim majorVersion As Int32 = myBrowserCapabilities.MajorVersion
        If (strAgent.Contains("like Gecko") And strAgent.Contains("Trident") And majorVersion = 0) Then
            Page.ClientTarget = "uplevel"
        End If

        'Put user code to initialize the page here
        pr_VALI_SESI()
        PoliticaCache()
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "RETIROS_NUEVO") Then
            Try
                Me.lblOpcion.Text = "Nuevo Pedido WMS"
                If Page.IsPostBack = False Then
                    Dim strResult As String()
                    strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "RETIROS_NUEVO"), "-")
                    Session.Item("Page") = strResult(0)
                    Session.Item("Opcion") = strResult(1)
                    LlenarAlmacenes()
                    If Session("Co_AlmaS") = "" Then
                        Session("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
                    Else
                        Me.cboAlmacen.SelectedValue = Session("Co_AlmaS")
                    End If
                    ListaReferencia()
                    InicializaBusqueda()
                    Bindatagrid()
                    If Session.Item("MensajePedido") <> Nothing Then
                        pr_IMPR_MENS(Session.Item("MensajePedido"))
                        Session.Remove("MensajePedido")
                    End If
                Else
                    If ValidarItemsSeleccionados2() = False Then
                        Bindatagrid()
                    End If

                End If
            Catch ex As Exception
                Me.lblMensaje.Text = ex.Message
            End Try
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub RegistrarCadenaBusqueda()
        Session.Add("CadenaBusquedaPedidoNuevo", Me.cboAlmacen.SelectedValue & "||" & Me.txtCodProducto.Text.Replace("'", "") &
                    "||" & Me.txtLote.Text.Replace("'", "") & "||" & Me.cboReferencia.SelectedValue &
                    "||" & Me.txtNroReferencia.Text.Replace("'", "") & "||" & txtFechaVenc.Value)
    End Sub

    Public Sub InicializaBusqueda()
        Dim strCadenaBusqueda As String = ""
        Dim strFiltros As String()
        If Session.Item("CadenaBusquedaPedidoNuevo") <> Nothing Then
            strCadenaBusqueda = Session.Item("CadenaBusquedaPedidoNuevo").ToString
            strFiltros = Split(strCadenaBusqueda, "||")
            Me.cboAlmacen.SelectedValue = strFiltros(0)
            Me.txtCodProducto.Text = strFiltros(1)
            Me.txtLote.Text = strFiltros(2)
            Me.cboReferencia.SelectedValue = strFiltros(3)
            Me.txtNroReferencia.Text = strFiltros(4)
            Me.txtFechaVenc.Value = strFiltros(5)
        End If
    End Sub

    Private Sub ListaReferencia()
        objParametro = New clsCNParametro
        objParametro.gCNListarReferencia(Me.cboReferencia, "8")
    End Sub

    Private Sub Bindatagrid()
        Try
            Dim dtRetiro As DataTable
            objWMS = New clsCNWMS
            dtRetiro = objWMS.gCNGetBuscarMercaderia_WMS(Session("CoEmpresa"), Session.Item("IdSico"), Me.cboAlmacen.SelectedValue, Me.txtCodProducto.Text, Me.txtLote.Text,
                                                        Me.cboReferencia.SelectedValue, Me.txtNroReferencia.Text, Me.txtFechaVenc.Value, Session.Item("IdUsuario"))
            Me.dgResultado.DataSource = dtRetiro
            Me.dgResultado.DataBind()
            Me.lblMensaje.Text = "Se encontraron " & dtRetiro.Rows.Count & " Registros"
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub LlenarAlmacenes()
        Try
            objRetiro = New clsCNRetiro
            Dim dtAlmacenes As DataTable
            cboAlmacen.Items.Clear()
            dtAlmacenes = objRetiro.gCNGetListarAlmacenes(Session("CoEmpresa"), Session("IdSico"), "S")
            For Each dr As DataRow In dtAlmacenes.Rows
                cboAlmacen.Items.Add(New ListItem(dr("Almacen"), dr("CodAlmacen")))
            Next
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Try
            Dim dgItem As DataGridItem
            Dim bST_VALI As Boolean = False

            For Each dgItem In dgResultado.Items
                If CType(dgItem.FindControl("chkItem"), CheckBox).Checked = True And CType(dgItem.FindControl("chkItem"), CheckBox).Enabled = True Then
                    bST_VALI = True
                    Exit For
                End If
            Next

            If bST_VALI = True Then
                If ValidarItemsSeleccionados() = True Then
                    Response.Redirect("AlmRetiroSolicitud_W.aspx", False)
                End If
            Else
                Me.lblMensaje.Text = "Realice una b�squeda de la cual se pueda seleccionar uno o m�s registros para continuar."
            End If
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
        End Try
    End Sub

    Private Function ValidarItemsSeleccionados() As Boolean
        Dim bolRetorno As Boolean = True
        Dim bolcantidad As Boolean = True
        'Dim bST_SEL0 As Boolean = False
        'Dim bST_SELE As Boolean = False
        'Dim bST_SEL1 As Boolean = False
        Dim bST_SEL2 As Boolean = False
        'Dim bST_SEL3 As Boolean = False
        'Dim bST_SEL4 As Boolean = False
        'Dim bST_SEL5 As Boolean = False

        'Dim sCO_ALMA_EVAL As String
        'Dim sNU_DOCU_REC1 As String = ""
        'Dim sNU_SECU As Integer = 0
        'Dim sNU_TITU As String
        'Dim sTI_DOCU_REC1 As String
        'Dim nNU_FILA As Integer
        'Dim sNU_MANI_EVAL As String
        'Dim sVA_EVAL_FLAG As String
        'Dim sCO_MONE As String
        Dim dt As DataTable
        Dim dr As DataRow
        Try
            dt = AgregarRegistros()
            For Each dr In dt.Rows
                bST_SEL2 = True

                If dr("NU_UNID_RETI") <= 0 Then
                    bolRetorno = False
                End If

                If dr("NU_UNID_RETI") > dr("NU_UNID_SALD") Then
                    bolcantidad = False
                End If
            Next
            If bST_SEL2 = False Then
                Me.lblMensaje.Text = "Debe seleccionar por lo menos un �tem"
                Return False
            End If
            If bolRetorno = False Then
                Me.lblMensaje.Text = "No ha ingresado la cantidad a retirar"
                Return False
            End If
            If bolcantidad = False Then
                Me.lblMensaje.Text = "Esta tratando de retirar mas de lo que tiene disponible"
                Return False
            End If

            'bST_SELE = False
            'bST_SEL1 = False
            'bST_SEL2 = False

            'Dim filterRows As DataRow()
            'Dim a As String

            'For Each dr In dt.Rows
            '    a = dr("CO_PROD_CLIE")
            '    a = dr("LOTE")
            '    filterRows = dt.Select("CO_PROD_CLIE = '" & dr("CO_PROD_CLIE") & "' and LOTE = '" & dr("LOTE") & "' and LOTE = '" & dr("LOTE") & "'", "")
            '    If filterRows.Length > 1 Then
            '        bST_SEL0 = True
            '        Exit For
            '    End If
            'Next
            'If bST_SEL0 = True Then
            '    Bindatagrid()
            '    Me.lblMensaje.Text = "No puede agregar varias veces el mismo �tem, verifique!!"
            '    Return False
            'End If
            Session.Add("dtWMS", dt)
            Return True
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
            Return False
        End Try
    End Function
    Private Function ValidarItemsSeleccionados2() As Boolean
        Dim bolRetorno As Boolean = True
        Dim bolcantidad As Boolean = True
        Dim bST_SEL2 As Boolean = False

        Dim dt As DataTable
        Dim dr As DataRow
        Try
            dt = AgregarRegistros()
            For Each dr In dt.Rows
                bST_SEL2 = True

                If dr("NU_UNID_RETI") <= 0 Then
                    bolRetorno = False
                End If

                If dr("NU_UNID_RETI") > dr("NU_UNID_SALD") Then
                    bolcantidad = False
                End If
            Next
            If bST_SEL2 = False Then
                Me.lblMensaje.Text = "Debe seleccionar por lo menos un �tem"
                Return False
            End If
            If bolRetorno = False Then
                Me.lblMensaje.Text = "No ha ingresado la cantidad a retirar"
                Return False
            End If
            If bolcantidad = False Then
                Me.lblMensaje.Text = "Esta tratando de retirar mas de lo que tiene disponible"
                Return False
            End If

            'Session.Add("dtWMS", dt)
            Return True
        Catch ex As Exception
            Me.lblMensaje.Text = ex.Message
            Return False
        End Try
    End Function
    Function AgregarRegistros() As DataTable
        Dim dt1 As New DataTable
        Dim dgItem As DataGridItem
        Dim dr As DataRow

        If Session.Item("dtWMS") Is Nothing Then
            'CAVA
            dt1.Columns.Add(New DataColumn("CO_PROD_CLIE", GetType(String)))
            dt1.Columns.Add(New DataColumn("DSC_PROD", GetType(String)))
            dt1.Columns.Add(New DataColumn("LOTE", GetType(String)))
            dt1.Columns.Add(New DataColumn("FCH_VENC", GetType(String)))
            dt1.Columns.Add(New DataColumn("COD_RETE", GetType(String)))
            dt1.Columns.Add(New DataColumn("DSC_RETE", GetType(String)))
            dt1.Columns.Add(New DataColumn("UNI_MEDI", GetType(String)))
            dt1.Columns.Add(New DataColumn("NU_UNID_SALD", GetType(Decimal)))
            dt1.Columns.Add(New DataColumn("NU_UNID_RETI", GetType(Decimal)))
        Else
            dt1 = CType(Session.Item("dtWMS"), DataTable).Copy()
        End If

        Dim nNU_UNID_RETI, nNU_UNID_SALD, nNU_PESO_UNID, nNU_PESO_RETI, nNU_UNID_RECI, nNU_PESO_RECI As Decimal
        Dim sTI_PESO_UNID As String

        For Each dgItem In dgResultado.Items
            If CType(dgItem.FindControl("chkItem"), CheckBox).Checked = True And CType(dgItem.FindControl("chkItem"), CheckBox).Enabled = True Then
                nNU_UNID_RETI = IIf(CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text.Trim = "", 0, CType(dgItem.FindControl("txtUnidRetirar"), TextBox).Text)
                nNU_UNID_RETI = FormatNumber(nNU_UNID_RETI, 3)
                dr = dt1.NewRow
                'CAVA
                dr("CO_PROD_CLIE") = dgItem.Cells(1).Text
                dr("DSC_PROD") = dgItem.Cells(2).Text
                dr("LOTE") = dgItem.Cells(3).Text
                dr("FCH_VENC") = dgItem.Cells(4).Text
                dr("COD_RETE") = dgItem.Cells(5).Text
                dr("DSC_RETE") = dgItem.Cells(6).Text
                dr("UNI_MEDI") = dgItem.Cells(7).Text
                dr("NU_UNID_SALD") = IIf(dgItem.Cells(8).Text.Trim = "", 0, dgItem.Cells(8).Text)
                dr("NU_UNID_RETI") = nNU_UNID_RETI
                dt1.Rows.Add(dr)
            End If
        Next
        Return dt1
    End Function

    Private Sub dgResultado_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResultado.ItemDataBound
        Dim dt As DataTable
        Dim dr As DataRow()
        Dim txtPeso As New TextBox
        Dim txtUnidad As New TextBox
        dt = CType(Session("dtWMS"), DataTable)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                txtUnidad = CType(e.Item.FindControl("txtUnidRetirar"), TextBox)
                txtUnidad.Attributes.Add("onkeypress", "Javascript:validardecimal();")
                If e.Item.DataItem("DSC_RETE").ToString.Trim <> "" Then
                    e.Item.BackColor = System.Drawing.Color.Thistle
                End If
                If Session.Item("dtWMS") Is Nothing Then
                Else
                    If dt.Rows.Count <> 0 Then
                        dr = dt.Select("CO_PROD_CLIE='" & e.Item.DataItem("COD_PROD") & "' and LOTE ='" & e.Item.DataItem("LOTE") & "'")
                        If dr.Length > 0 Then
                            CType(e.Item.FindControl("chkItem"), CheckBox).Checked = True
                            CType(e.Item.FindControl("chkItem"), CheckBox).Enabled = False
                            CType(e.Item.FindControl("txtUnidRetirar"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtUnidRetirar"), TextBox).BackColor = System.Drawing.Color.Gray
                            e.Item.BackColor = System.Drawing.Color.Gray
                        End If
                    End If
                End If
            End If
        Catch e1 As Exception
            Me.lblMensaje.Text = e1.Message
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Me.dgResultado.CurrentPageIndex = 0
        Bindatagrid()
        RegistrarCadenaBusqueda()
    End Sub

    Sub Change_Page(ByVal Src As Object, ByVal Args As DataGridPageChangedEventArgs)
        Me.dgResultado.CurrentPageIndex = Args.NewPageIndex
        Bindatagrid()
    End Sub

    Protected Overrides Sub Finalize()
        If Not (objRetiro Is Nothing) Then
            objRetiro = Nothing
        End If
        If Not (objParametro Is Nothing) Then
            objParametro = Nothing
        End If
        If Not (objAccesoWeb Is Nothing) Then
            objAccesoWeb = Nothing
        End If
        MyBase.Finalize()
    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        objWMS = New clsCNWMS
        Session("Co_AlmaS") = CStr(Me.cboAlmacen.SelectedValue)
        Session("Co_Almaw") = CStr(Me.cboAlmacen.SelectedValue)
        If CInt(objWMS.gGetExisteWMS(Session.Item("IdSico"), Session("Co_AlmaS"))) = 0 Then
            Response.Redirect("AlmPedidoNuevo.aspx")
        End If
    End Sub

End Class
