<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="DashboardDeta.aspx.vb" Inherits="DashboardDeta" %>

<%@ Register TagPrefix="cc1" Namespace="RoderoLib" Assembly="RoderoLib" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WarSolicitudWarrants</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>

    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        $(function () {
            $("#txtFechaInicial").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaInicial").click(function () {
                $("#txtFechaInicial").datepicker('show');
            });

        });

        function CalcAmount(Cantidad, Precio, Monto, Stock) {
            //if(parseFloat(document.getElementById(Cantidad).value) > parseFloat(document.getElementById(Stock).value))
            //  {
            //   alert("La cantidad a solicitar debe ser menor o igual a : " + parseFloat(document.getElementById(Stock).value));
            //   document.getElementById(Cantidad).value = parseFloat(document.getElementById(Stock).value);
            //   return;
            //  }				  				  
            document.getElementById(Monto).value = formatearDecimal(parseFloat(document.getElementById(Cantidad).value) * parseFloat(document.getElementById(Precio).value), 3);

            if (document.getElementById(Monto).value == "NaN") {
                document.getElementById(Monto).value = "";
            }
        }

        function formatearDecimal(numero, cantDecimales) {
            if (cantDecimales > 0) {
                var i = 0;
                var aux = 1;
                for (i = 0; i < cantDecimales; i++) {
                    aux = aux * 10;
                }
                numero = numero * aux;
                numero = Math.round(numero);
                numero = numero / aux;
            }
            return numero;
        }

        function ValidNum(e) { var tecla = document.all ? tecla = e.keyCode : tecla = e.which; return ((tecla > 47 && tecla < 58) || tecla == 46); }

        function Formato(Campo, teclapres) {
            var tecla = teclapres.keyCode;
            var vr = new String(Campo.value);
            vr = vr.replace(".", "");
            vr = vr.replace(".", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace(",", "");
            vr = vr.replace("/", "");
            vr = vr.replace("-", "");
            vr = vr.replace(" ", "");
            var tam = 0;
            tam = vr.length;
            if (tam > 2 && tam < 6) {
                Campo.value = vr.substr(0, tam - 2) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 6 && tam < 9) {
                Campo.value = vr.substr(0, tam - 5) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 9 && tam < 12) {
                Campo.value = vr.substr(0, tam - 8) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 12 && tam < 15) {
                Campo.value = vr.substr(0, tam - 11) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 15 && tam < 18) {
                Campo.value = vr.substr(0, tam - 14) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 18 && tam < 21) {
                Campo.value = vr.substr(0, tam - 17) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + ',' + vr.substr(tam - 5, 3) + '.' + vr.substr(tam - 2, 2);
            }
            if (tam >= 21) {
                Campo.value = vr.substr(0, tam - 20) + ',' + vr.substr(tam - 20, 3) + ',' + vr.substr(tam - 17, 3) + ',' + vr.substr(tam - 14, 3) + ',' + vr.substr(tam - 11, 3) + ',' + vr.substr(tam - 8, 3) + '.' + vr.substr(tam - 5, 2);
            }
        }
        function dois_pontos(tempo) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.returnValue = false;
            }
            if (tempo.value.length == 2) {
                tempo.value += ":";
            }
        }

        function valida_horas(tempo) {
            horario = tempo.value.split(":");
            var horas = horario[0];
            var minutos = horario[1];
            var segundos = horario[2];
            if (horas > 23) { //para rel�gio de 12 horas altere o valor aqui
                alert("Horas inv�lidas"); event.returnValue = false; tempo.focus()
            }
            if (minutos > 59) {
                alert("Minutos inv�lidos"); event.returnValue = false; tempo.focus()
            }
            if (segundos > 59) {
                alert("Segundos inv�lidos"); event.returnValue = false; tempo.focus()
            }
        }
    </script>
    <script language="javascript">		
            function Todos(ckall, citem) {
                var actVar = ckall.checked;
                for (i = 0; i < Form1.length; i++) {
                    if (Form1.elements[i].type == "checkbox") {
                        if (Form1.elements[i].name.indexOf(citem) != -1) {
                            Form1.elements[i].checked = actVar;
                        }
                    }
                }
            }
    </script>
    
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
    
 </head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table7" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table3"
                        border="0" cellspacing="6" cellpadding="0" width="100%">
                        <tr>
                            <td></td>
                            <td width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        <td align="right" class="auto-style2">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                            </td>
                            <td valign="top" align="center">
                                <table class="auto-style1">
                                    <tr>
                                        <td width="50%">
                                <asp:Label ID="lblSubTitulo2" runat="server" CssClass="Text5">Ver Detalle</asp:Label>

                                                                                </td>
                                        <td width="50%">
                                            <asp:ImageButton ID="ImageButton1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="200" valign="top" align="center">
                                <asp:DataGrid ID="dgdResultado" runat="server" CssClass="gv" Width="100%" PageSize="30" AutoGenerateColumns="False"
                                    BorderWidth="1px" BorderColor="Gainsboro">
                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                    <Columns>                                                                                                    
                                         <asp:BoundColumn  DataField="NRO_OPER" HeaderText="Nro Operaci�n" >
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="NRO_WARR" HeaderText="Nro Warrant" >
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="NU_VALO_INI" HeaderText="Valor Inicial"  DataFormatString="{0:N2}">
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="NU_VALO_ACT" HeaderText="Valor Actual"  DataFormatString="{0:N2}">
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="CO_FINA" HeaderText="Banco" >
                                             <ItemStyle HorizontalAlign="Left"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="CO_CLIE" HeaderText="Cliente" >
                                             <ItemStyle HorizontalAlign="Left"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="FE_EMIS" HeaderText="Fecha de emision" >
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="FE_VEN_MAX" HeaderText="Fecha de vencimiento m�ximo" >
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                         <asp:BoundColumn  DataField="FLG_WARR_XVEN" HeaderText="Warrant por vencer" >
                                             <ItemStyle HorizontalAlign="center"></ItemStyle></asp:BoundColumn>
                                                                               
                                                                               
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
                                        Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
