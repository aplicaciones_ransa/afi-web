<%@ Page Language="vb" AutoEventWireup="false" CodeFile ="AdmDominioNuevo.aspx.vb" Inherits="AdmDominioNuevo" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AdmDominioNuevo</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">	
		 document.onkeydown = function(){  
			if(window.event && window.event.keyCode == 116)
			{ 
				window.event.keyCode = 505;  
				} 
				if(window.event && window.event.keyCode == 505)
				{  
				return false;     
				}  
			}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:Header id="Header2" runat="server"></uc1:Header>
						<TABLE id="Table3" style="BORDER-RIGHT: #808080 1px solid; BORDER-LEFT: #808080 1px solid"
							height="400" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table4" cellSpacing="6" cellPadding="0" width="100%" border="0">
										<TR>
											<TD></TD>
											<TD width="100%">
												<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
										</TR>
										<TR>
											<TD vAlign="top" rowSpan="4">
												<uc1:MenuInfo id="MenuInfo1" runat="server"></uc1:MenuInfo></TD>
											<TD vAlign="top" width="100%">
												<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="630" border="0" align="center">
													<TR>
														<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
														<TD background="Images/table_r1_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r2_c1.gif"></TD>
														<TD align="center">
															<TABLE id="Table7" cellSpacing="4" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="text" width="20%">C�digo de Dominio :</TD>
																	<TD width="80%">
																		<asp:TextBox id="txtCodDomin" runat="server" CssClass="text" MaxLength="20" Width="280px"></asp:TextBox></TD>
																</TR>
																<TR>
																	<TD class="text">Descripci�n :</TD>
																	<TD>
																		<asp:TextBox id="txtDescDomin" runat="server" CssClass="text" MaxLength="100" Width="280px"></asp:TextBox></TD>
																</TR>
																<TR>
																	<TD class="text">Valor 1:</TD>
																	<TD>
																		<asp:TextBox id="txtval1" runat="server" CssClass="text" MaxLength="150" Width="280px"></asp:TextBox></TD>
																</TR>
																<TR>
																	<TD class="text">Valor 2:</TD>
																	<TD>
																		<asp:TextBox id="txtval2" runat="server" CssClass="text" MaxLength="150" Width="280px"></asp:TextBox></TD>
																</TR>
																<TR>
																	<TD class="text">Valor 3:</TD>
																	<TD>
																		<asp:TextBox id="txtval3" runat="server" CssClass="text" MaxLength="150" Width="280px"></asp:TextBox></TD>
																</TR>
																<TR>
																	<TD class="text">Valor 4:</TD>
																	<TD>
																		<asp:TextBox id="txtval4" runat="server" CssClass="text" MaxLength="150" Width="280px"></asp:TextBox></TD>
																</TR>
															</TABLE>
															<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
														<TD width="6" background="Images/table_r2_c3.gif"></TD>
													</TR>
													<TR>
														<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
														<TD background="Images/table_r3_c2.gif" height="6"></TD>
														<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top" align="right">
												<asp:HyperLink id="hlkRegresar" runat="server" CssClass="Text" NavigateUrl="AdmParametro.aspx"
													Target="_parent">Regresar p�gina anterior</asp:HyperLink></TD>
										</TR>
										<TR>
											<TD vAlign="top" align="center">
												<TABLE id="Table8" cellSpacing="4" cellPadding="0" border="0">
													<TR>
														<TD>
															<asp:Button id="btnNuevo" CssClass="btn" Runat="server" Text="Nuevo"></asp:Button></TD>
														<TD width="80">
															<asp:button id="btnGrabar" runat="server" CssClass="btn" Text="Grabar"></asp:button></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<uc1:Footer id="Footer2" runat="server"></uc1:Footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
