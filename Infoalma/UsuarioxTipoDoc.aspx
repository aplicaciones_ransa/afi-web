<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UsuarioxTipoDoc.aspx.vb" Inherits="UsuarioxTipoDoc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>UsuarioxTipoDoc</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">		
			function Todos(ckall, citem) { 
				var actVar = ckall.checked ;
				for(i=0; i<Form1.length; i++) {
					if (Form1.elements[i].type == "checkbox") {
						if (Form1.elements[i].name.indexOf(citem) != -1) {
							Form1.elements[i].checked = actVar;
						}
					}
				}
			}           
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgColor="#f0f0f0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2">
						<uc1:header id="Header2" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<TABLE id="Table9" style="BORDER-LEFT: #808080 1px solid; BORDER-RIGHT: #808080 1px solid"
							cellSpacing="6" cellPadding="0" width="100%" border="0">
							<TR>
								<TD></TD>
								<TD width="100%">
									<asp:label id="lblOpcion" runat="server" CssClass="Titulo1"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" rowSpan="6">
									<uc1:MenuInfo id="MenuInfo1" runat="server"></uc1:MenuInfo></TD>
								<TD vAlign="top" width="100%">
									<TABLE id="Table20" cellSpacing="0" cellPadding="0" width="690" align="center" border="0">
										<TR>
											<TD width="6" background="Images/table_r1_c1.gif" height="6"></TD>
											<TD background="Images/table_r1_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r1_c3.gif" height="6"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r2_c1.gif"></TD>
											<TD>
												<TABLE id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">
													<TR>
														<TD class="Text" width="9%">TipoEnt:</TD>
														<TD width="15%">
															<asp:dropdownlist id="cboTipoEntidad" runat="server" CssClass="Text" AutoPostBack="True" Width="90px"></asp:dropdownlist></TD>
														<TD class="Text" width="8%">
															<asp:label id="lblEtiqueta" runat="server"></asp:label></TD>
														<TD width="49%">
															<asp:dropdownlist id="cboEntidad" runat="server" CssClass="Text" Width="300px"></asp:dropdownlist></TD>
														<TD class="Text" id="Departamento" width="6%" runat="server">Depart:</TD>
														<TD id="Departamento1" width="13%" runat="server">
															<asp:dropdownlist id="cboDepartamento" runat="server" CssClass="Text" Width="90px"></asp:dropdownlist></TD>
													</TR>
													<TR id="Nombres" runat="server">
														<TD class="Text">Nombres:</TD>
														<TD colSpan="5">
															<asp:textbox id="txtNombres" runat="server" CssClass="Text" Width="450px"></asp:textbox></TD>
													</TR>
													<TR id="Cliente" runat="server">
														<TD class="Text">Cliente:</TD>
														<TD colSpan="5">
															<asp:dropdownlist id="cboCliente" runat="server" CssClass="Text"></asp:dropdownlist></TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="6" background="Images/table_r2_c3.gif"></TD>
										</TR>
										<TR>
											<TD width="6" background="Images/table_r3_c1.gif" height="6"></TD>
											<TD background="Images/table_r3_c2.gif" height="6"></TD>
											<TD width="6" background="Images/table_r3_c3.gif" height="6"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:button id="btnBuscar" runat="server" CssClass="btn" Width="80px" Text="Buscar"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="250">
									<asp:datagrid id="dgdResultado" runat="server" CssClass="gv" Width="100%" BorderColor="Gainsboro"
										PageSize="25" OnPageIndexChanged="Change_Page" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
										<ItemStyle CssClass="gvRow"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="COD_USER" HeaderText="Codigo"></asp:BoundColumn>
											<asp:BoundColumn DataField="DE_ALMA" HeaderText="Almacen"></asp:BoundColumn>
											<asp:BoundColumn DataField="Nombres" HeaderText="Apellidos y Nombres">
												<HeaderStyle Width="250px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="NOM_ENTI" HeaderText="Entidad">
												<HeaderStyle Width="300px"></HeaderStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<HeaderTemplate>
													Títulos
													<asp:CheckBox id="chkHTitulo" runat="server" CssClass="Text"></asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<TABLE id="Table15" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkTitulo runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_TITU") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<HeaderTemplate>
													Liberación
													<asp:CheckBox id=chkHLiberacion runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_LIBE") %>'>
													</asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkLiberacion runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_LIBE") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<HeaderTemplate>
													Simple
													<asp:CheckBox id=chkHSimple runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_SIMP") %>'>
													</asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<TABLE id="Table17" border="0" cellSpacing="0" cellPadding="0" width="30">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkSimple runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_SIMP") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<HeaderTemplate>
													Aduana
													<asp:CheckBox id=chkHAduana runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_ADUA") %>'>
													</asp:CheckBox>
												</HeaderTemplate>
												<ItemTemplate>
													<TABLE id="Table18" cellSpacing="0" cellPadding="0" width="30" border="0">
														<TR>
															<TD align="center">
																<asp:CheckBox id=chkAduana runat="server" CssClass="Text" Checked='<%#DataBinder.Eval(Container, "DataItem.DOC_ADUA") %>'>
																</asp:CheckBox></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="COD_SICO"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_TIPENTI" HeaderText="COD_TIPENTI"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="COD_CORR" HeaderText="Correlativo"></asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Center" Position="TopAndBottom" PageButtonCount="30" CssClass="gvPager"
											Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblMensaje" runat="server" CssClass="error"></asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:button id="btnGrabar" runat="server" CssClass="btn" Width="80px" Text="Grabar"></asp:button></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<asp:label id="lblError" runat="server" CssClass="Error"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<uc1:footer id="Footer2" runat="server"></uc1:footer></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
