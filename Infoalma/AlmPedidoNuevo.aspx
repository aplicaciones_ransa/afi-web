<%@ Register TagPrefix="uc1" TagName="Footer" Src="UserControls/Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Menu" Src="UserControls/Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="UserControls/Header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuInfo" Src="UserControls/MenuInfo.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeFile="AlmPedidoNuevo.aspx.vb" Inherits="AlmPedidoNuevo" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Retiro Nuevo</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="Styles/Styles.css">
    <script language="javascript" src="JScripts/calendar.js"></script>
    <script language="javascript" src="JScripts/Validaciones.js"></script>
    
    <script type="text/javascript" src="Scripts/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.12.1.js"></script>
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">


        $(function () {
            $("#txtFechaVenc").datepicker({
                dateFormat: "dd/mm/yy",
                dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                firstDay: 1,
                gotoCurrent: true,
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"]
            });

            $("#btnFechaVenc").click(function () {
                $("#txtFechaVenc").datepicker('show');
            });

        });

        function CalcularPeso(sCntRetirar, sBultoRetirar, sCntDisponible, sBultoDisponible, sTextboxOriginal) {
            if (parseFloat(sCntRetirar) > parseFloat(sCntDisponible)) {
                alert("La cantidad a retirar debe ser menor o igual a: " + sCntDisponible);
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
                return
            }
            if (parseFloat(sCntRetirar) < 0) {
                alert("La cantidad a retirar debe ser mayor a cero ");
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
                return
            }
            //if (sCntRetirar.length == 0)
            //{
            //alert("La cantidad a retirar debe ser mayor a cero ");
            //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible);
            //Form1.all(sTextboxOriginal).value = parseFloat(sCntDisponible);
            //return
            //}
            //document.getElementById(sBultoRetirar).disabled = false;
            //Form1.all(sBultoRetirar).value = parseFloat(sBultoDisponible)*parseFloat(sCntRetirar)/parseFloat(sCntDisponible);
        }
        function CalcularCantidad(sCntRetirar, sBultoRetirar, sCntDisponible, sBultoDisponible, sTextboxOriginal) {
            if (parseFloat(sCntRetirar) > parseFloat(sBultoDisponible)) {
                alert("El bulto a retirar debe ser menor o igual a: " + sBultoDisponible);
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = sCntDisponible;
                //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
                return
            }
            if (parseFloat(sCntRetirar) < 0) {
                alert("El bulto a retirar debe ser mayor a cero ");
                document.getElementById(sBultoRetirar).value = "";
                document.getElementById(sTextboxOriginal).value = "";
                //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible);
                //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
                return
            }
            //if (sCntRetirar.length == 0)
            //{
            //alert("El bulto a retirar debe ser mayor a cero ");
            //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible);
            //Form1.all(sTextboxOriginal).value = parseFloat(sBultoDisponible);
            //return
            //}
            //document.getElementById(sBultoRetirar).disabled = false;
            //Form1.all(sBultoRetirar).value = parseFloat(sCntDisponible)*parseFloat(sCntRetirar)/parseFloat(sBultoDisponible);
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" bgcolor="#f0f0f0">
    <form id="Form1" method="post" runat="server">
        <table id="Table4" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <!-- Text='<%# String.Format("{0:N3}",DataBinder.Eval(Container, "DataItem.NU_UNID_SALD")) %>'-->
                    <!--Text='<%# String.Format("{0:N3}",DataBinder.Eval(Container, "DataItem.NU_UNID_PESO")) %>'-->
                    <uc1:Header ID="Header2" runat="server"></uc1:Header>
                    <table style="border-left: #808080 1px solid; border-right: #808080 1px solid" id="Table1"
                        border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table id="Table3" border="0" cellspacing="6" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td width="100%">
                                                <asp:Label ID="lblOpcion" runat="server" CssClass="Titulo1"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" rowspan="6">
                                                <uc1:MenuInfo ID="MenuInfo1" runat="server"></uc1:MenuInfo>
                                            </td>
                                            <td valign="top" width="100%" align="center">
                                                <table id="Table20" border="0" cellspacing="0" cellpadding="0" width="670">
                                                    <tr>
                                                        <td height="6" background="Images/table_r1_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r1_c2.gif"></td>
                                                        <td height="6" background="Images/table_r1_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td background="Images/table_r2_c1.gif" width="6"></td>
                                                        <td>
                                                            <table id="Table6" border="0" cellspacing="4" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="Text" width="10%">Almacen :</td>
                                                                    <td width="62%" colspan="4">
                                                                        <asp:DropDownList ID="cboAlmacen" runat="server" CssClass="Text" AutoPostBack="True" Width="451px"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text" width="10%">Referencia :</td>
                                                                    <td width="15%">
                                                                        <asp:DropDownList ID="cboReferencia" runat="server" CssClass="Text" Width="110px"></asp:DropDownList>&nbsp;
																			<asp:TextBox ID="txtNroReferencia" runat="server" CssClass="Text" Width="80px"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Text" width="7%">Modalidad&nbsp;:</td>
                                                                    <td width="12%">
                                                                        <asp:DropDownList ID="cboModalidad" runat="server" CssClass="text" Width="135px"></asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Text">Estado:</td>
                                                                    <td class="Text">
                                                                        <asp:DropDownList ID="ddlestado" runat="server" CssClass="text" Width="200px">
                                                                            <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                                                                            <asp:ListItem Value="N">Mercaderia Apta</asp:ListItem>
                                                                            <asp:ListItem Value="S">Mercaderia No Apta</asp:ListItem>
                                                                            <asp:ListItem Value="D">Diferente de Bueno</asp:ListItem>
                                                                        </asp:DropDownList></td>
                                                                    <td class="Text">Fech Venc :</td>
                                                                    <td class="Text">
                                                                        <input id="txtFechaVenc" class="text" onkeypress="validarcharfecha()" onkeyup="this.value=formateafecha(this.value);"
                                                                            maxlength="10" size="6" name="txtFechaVenc" runat="server" width="90">
                                                                        <input id="btnFechaVenc" class="text" value="..."
                                                                            type="button" name="btnFecha"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td background="Images/table_r2_c3.gif" width="6"></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="6" background="Images/table_r3_c1.gif" width="6"></td>
                                                        <td height="6" background="Images/table_r3_c2.gif"></td>
                                                        <td height="6" background="Images/table_r3_c3.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="right">
                                                <asp:HyperLink ID="hlkSolicitud" runat="server" CssClass="Text" Target="_parent" NavigateUrl="AlmRetiroSolicitud.aspx">Ver solicitud antes de enviar</asp:HyperLink></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="center">
                                                <table id="Table8" border="0" cellspacing="4" cellpadding="0">
                                                    <tr>
                                                        <td width="80">
                                                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn" CausesValidation="False" Text="Buscar"></asp:Button></td>
                                                        <td width="80">
                                                            <asp:Button ID="btnAgregar" runat="server" CssClass="Text" Text="Agregar Items"></asp:Button></td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="lblMensaje" runat="server" CssClass="error"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="Text" valign="top" align="left">Observaci�n: Debe ingresar la cantidad a 
													retirar en una de las cajas de texto, si ingresa en las dos cajas (Unidad y 
													Bulto), se tomar� como unidad ingresada la primera.</td>
                                        </tr>
                                        <tr>
                                            <td height="200" valign="top">
                                                <!--<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 322px">-->
                                                <asp:DataGrid ID="dgResultado" runat="server" CssClass="gv" Width="100%" AutoGenerateColumns="False"
                                                    BorderColor="Gainsboro" AllowPaging="True" PageSize="30" OnPageIndexChanged="Change_Page">
                                                    <AlternatingItemStyle CssClass="gvRowAlter"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="gvRow"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Center" CssClass="gvHeader"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Sel">
                                                            <HeaderStyle Width="30px"></HeaderStyle>
                                                            <ItemTemplate>
                                                                <table id="Table5" cellspacing="0" cellpadding="0" width="30" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkItem" runat="server" CssClass="Text"></asp:CheckBox></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="NU_DOCU_RECE" HeaderText="Nro.DCR">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NU_SECU" HeaderText="Item">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DE_MERC" HeaderText="Dsc.Merc."></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CO_PROD_CLIE" HeaderText="Cod.Prod.">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="LOTE" HeaderText="Referencia">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="FE_VCTO_MERC" HeaderText="FechaVenc">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CO_BODE_ACTU" HeaderText="Bodega">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CO_TIPO_BULT" HeaderText="Unid">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NU_UNID_SALD" HeaderText="Saldo.Unid" DataFormatString="{0:N3}">
                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Unid. a Retirar">
                                                            <HeaderStyle Width="60px"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtUnidRetirar" runat="server" CssClass="Text" Width="60" ForeColor="blue" Font-Bold="True"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn Visible="False" DataField="ADUANERO" HeaderText="Aduanero"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_ALMA_ACTU" HeaderText="CodAlma"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_UNID" HeaderText="Unidad">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="FLG_SEL" HeaderText="flg_sel"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_UBIC_ACTU" HeaderText="Ubicaci&#243;n"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="DE_TIPO_MERC" HeaderText="Tipo Mercader&#237;a"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="DE_STIP_MERC" HeaderText="SubTipo Mercader&#237;a"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="TI_DOCU_RECE"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="FE_REGI_INGR"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_SECU_UBIC"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_TIPO_MERC"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_STIP_MERC"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="DE_TIPO_BULT"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CO_UNME_PESO" HeaderText="Bulto">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="DE_UNME"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NU_UNID_PESO" HeaderText="Saldo.Bulto" DataFormatString="{0:N6}">
                                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Bulto a Retirar">
                                                            <HeaderStyle Width="60px"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtBultoRetirar" runat="server" CssClass="Text" Width="60" ForeColor="blue"
                                                                    Font-Bold="True"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="CO_MONE" HeaderText="Moneda">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="TI_TITU"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_TITU"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_UNME_AREA"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_AREA_USAD"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_UNME_VOLU"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_VOLU_USAD"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="ST_VALI_RETI"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_UNID_RECI"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_PESO_RECI"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NU_PESO_UNID"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="TI_UNID_PESO"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="NU_MANI"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="ST_TEAL"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="IM_UNIT" HeaderText="Valor.Unit" DataFormatString="{0:N6}"></asp:BoundColumn>
                                                        <asp:BoundColumn Visible="False" DataField="CO_MODA_MOVI" HeaderText="Tipo Retiro"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ESTADO" HeaderText="Estado"></asp:BoundColumn>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Center" Position="TopAndBottom" CssClass="gvPager" Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid><!--</div>--></td>
                                        </tr>
                                        <tr>
                                            <td class="Text" valign="top" align="center">
                                                <table id="tblLeyenda" border="0" cellspacing="4" cellpadding="0" width="50%">
                                                    <tr>
                                                        <td class="td" colspan="4">Leyenda</td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="thistle" width="15%"></td>
                                                        <td class="Leyenda" width="20%" align="center">�tem observado
                                                        </td>
                                                        <td bgcolor="#f5dc8c" width="15%"></td>
                                                        <td class="Leyenda" width="20%" align="center">DCR Aduaneros
                                                        </td>
                                                        <td style="background-color: darkseagreen" width="30%" align="center"></td>
                                                        <td class="Leyenda" width="15%" align="center">DCR Prendados</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <uc1:Footer ID="Footer2" runat="server"></uc1:Footer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
