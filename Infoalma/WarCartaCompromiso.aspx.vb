Imports Library.AccesoDB
Imports Depsa.LibCapaNegocio
Imports Infodepsa
Imports System.Data

Public Class WarCartaCompromiso
    Inherits System.Web.UI.Page
    Private objEntidad As Entidad = New Entidad
    Private objFunciones As Library.AccesoBL.Funciones = New Library.AccesoBL.Funciones
    Private objDocumento As Documento = New Documento
    Private objUsuario As Usuario = New Usuario
    Private objAccesoWeb As clsCNAccesosWeb
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents lblOpcion As System.Web.UI.WebControls.Label
    'Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    'Protected WithEvents txtNroDocumento As System.Web.UI.WebControls.TextBox
    'Protected WithEvents FilePDF As System.Web.UI.HtmlControls.HtmlInputFile
    'Protected WithEvents btnNuevo As System.Web.UI.WebControls.Button
    'Protected WithEvents cboDepositante As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents cboAlmacen As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents btnGrabar As System.Web.UI.WebControls.Button
    'Protected WithEvents cboFinanciador As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents RequiredFieldValidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents ValidationSummary1 As System.Web.UI.WebControls.ValidationSummary
    'Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents RequiredFieldValidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents cboMoneda As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents RequiredFieldValidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents txtValor As System.Web.UI.HtmlControls.HtmlInputText
    'Protected WithEvents ltrMensaje As System.Web.UI.WebControls.Literal
    'Protected WithEvents dgdDestinatario As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Funciones Comunes"
    Private Function pr_OBTI_IP_PETI() As String
        Return Request.ServerVariables("REMOTE_ADDR")
    End Function
    Private Function pr_OBTI_IP_REAL() As String
        Dim ClientIP, Forwaded, RealIP
        RealIP = ""
        ClientIP = Request.ServerVariables("HTTP_CLIENT-IP")
        If ClientIP <> "" Then
            RealIP = ClientIP
        Else
            Forwaded = Request.ServerVariables("HTTP_X-Forwarded-For")
            If Forwaded <> "" Then
                RealIP = Forwaded
            End If
        End If
        Return RealIP
    End Function
    '--------------------------------------------------------'
    '--Funcion           : pr_INFO_CABE                    --'
    '--Descripcion       : Muestra informacion de cabecera --'
    '--                    de paginas                      --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 21/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_VALI_SESI()
        '--Validacion de conexion al sistema
        If Session("IdUsuario") = "" Then
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub
    '--------------------------------------------------------'
    '--Funcion           : pr_IMPR_MENS                    --'
    '--Descripcion       : Muestra mensaje de advertencia  --'
    '--Desarrollado por  : Diana Mena Cordova              --' 
    '--Fecha Creacion    : 13/11/2002                      --' 
    '--------------------------------------------------------'
    Private Sub pr_IMPR_MENS(ByVal sIN_MENS As String, Optional ByVal sPA_RETO As String = "")
        Dim strScript = "<script language='javascript'> alert('" & sIN_MENS & "'); </script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "Open", strScript)
        Exit Sub
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        If (Request.IsAuthenticated) And InStr(Session("PagId"), "33") Then
            Me.lblError.Text = ""
            If Not IsPostBack Then
                Dim strResult As String()
                strResult = Split(Utils.gGetPosicionMenu(Session.Item("CoGrup"), Session.Item("CoSist"), "33"), "-")
                Session.Item("Page") = strResult(0)
                Session.Item("Opcion") = strResult(1)
                Me.lblOpcion.Text = "Ingreso de cartas de compromiso"
                loadFinanciador()
                loadDepositante()
                loadAlmacen()
                loadMoneda()
                Me.btnGrabar.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de registrar la Carta de Compromiso?')== false) return false;")
                Me.btnNuevo.Attributes.Add("onclick", "javascript:if(confirm('Esta seguro de ingresar una nueva Carta de Compromiso?')== false) return false;")
            End If
        Else
            Response.Redirect("Salir.aspx?caduco=1")
        End If
    End Sub

    Private Sub loadDepositante()
        Dim dtEntidad As DataTable
        Try
            Me.cboDepositante.Items.Clear()
            dtEntidad = objEntidad.gGetEntidadesSICO("03")
            Me.cboDepositante.Items.Add(New ListItem("---------------Seleccione---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboDepositante.Items.Add(New ListItem(dr("Nombre").ToString(), dr("Codigo")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadDestinatario()
        Dim dt As DataTable
        Try
            dt = objUsuario.gGetBusquedaUsuario("02", Me.cboFinanciador.SelectedValue, "")
            Me.dgdDestinatario.DataSource = dt
            Me.dgdDestinatario.DataBind()
            Me.lblError.Text = "Se encontraron " & dt.Rows.Count.ToString & " registros"
        Catch ex As Exception
            Me.lblError.Text = ex.Message
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub loadFinanciador()
        'Dim dtEntidad As DataTable
        'Try
        '    Me.cboFinanciador.Items.Clear()
        '    dtEntidad = objEntidad.gGetEntidadesSICO("02")
        '    Me.cboFinanciador.Items.Add(New ListItem("---------------Seleccione---------------", 0))
        '    For Each dr As DataRow In dtEntidad.Rows
        '        Me.cboFinanciador.Items.Add(New ListItem(dr("Nombre").ToString(), dr("Codigo")))
        '    Next
        'Catch ex As Exception
        '    Me.lblError.Text = "ERROR: " & ex.Message
        'Finally
        '    dtEntidad.Dispose()
        '    dtEntidad = Nothing
        'End Try

        Dim dtEntidad As New DataTable
        Try
            Me.cboFinanciador.Items.Clear()
            dtEntidad = objEntidad.gGetEntidades("02", "W")
            Me.cboFinanciador.Items.Add(New ListItem("---------------Seleccione---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboFinanciador.Items.Add(New ListItem(dr("NOM_ENTI").ToString(), dr("COD_SICO")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "Error al llenar entidades: " & ex.ToString
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub loadAlmacen()
        Dim dtEntidad As DataTable
        Try
            Me.cboAlmacen.Items.Clear()
            dtEntidad = objEntidad.gGetAlmacen(Me.cboDepositante.SelectedValue)
            Me.cboAlmacen.Items.Add(New ListItem("---------------Seleccione---------------", 0))
            For Each dr As DataRow In dtEntidad.Rows
                Me.cboAlmacen.Items.Add(New ListItem(dr("DE_ALMA").ToString(), dr("CO_ALMA")))
            Next
        Catch ex As Exception
            Me.lblError.Text = "ERROR: " & ex.Message
        Finally
            dtEntidad.Dispose()
            dtEntidad = Nothing
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.cboAlmacen.SelectedValue = "0"
        Me.cboDepositante.SelectedValue = "0"
        Me.cboFinanciador.SelectedValue = "0"
    End Sub

    Private Sub loadMoneda()
        Try
            objFunciones.GetMonedas(Me.cboMoneda)
        Catch ex As Exception
            pr_IMPR_MENS("ERROR: " & ex.ToString)
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        objAccesoWeb = New clsCNAccesosWeb
        objAccesoWeb.gCNInsAccesosWeb(Session.Item("IdUsuario"), CStr(Session.Item("IdTipoEntidad")).Substring(1), Session.Item("IdSico"),
                                    Session.Item("NombreEntidad"), "M", "33", "GRABA CARTA DE COMPROMISO", pr_OBTI_IP_PETI, pr_OBTI_IP_REAL)

        Dim strNombre As String
        Dim strDestinatario As String = ""
        Dim strResultado As String
        Dim strPath As String = ConfigurationManager.AppSettings.Item("RutaPDFs")
        Dim FlujoBinario As System.IO.Stream
        Dim Contenido As Byte()
        Dim dgItem As DataGridItem

        strNombre = System.IO.Path.GetFileName(Me.FilePDF.PostedFile.FileName)
        If strNombre <> "" Then
            FilePDF.PostedFile.SaveAs(strPath & strNombre)
        End If
        FlujoBinario = Request.Files(0).InputStream
        ReDim Contenido(FlujoBinario.Length)
        FlujoBinario.Read(Contenido, 0, Convert.ToInt32(FlujoBinario.Length))

        For i As Integer = 0 To Me.dgdDestinatario.Items.Count - 1
            dgItem = Me.dgdDestinatario.Items(i)
            If CType(dgItem.FindControl("chkItem"), CheckBox).Checked = True Then
                strDestinatario += dgItem.Cells(1).Text.Trim & ","
            End If
        Next

        strResultado = objDocumento.InsCartaCompromiso(Me.txtNroDocumento.Text.Trim.ToUpper, Me.cboDepositante.SelectedValue,
                                           Me.cboFinanciador.SelectedValue, Me.cboAlmacen.SelectedValue, Contenido,
                                           Session.Item("IdUsuario"), Me.cboMoneda.SelectedValue, Me.txtValor.Value, strDestinatario)
        Select Case strResultado
            Case "0"
                pr_IMPR_MENS("Se grab� correctamente, pas� a emitidos")
            Case "1"
                pr_IMPR_MENS("El documento ya est� cargado en el sistema!")
            Case "2"
                pr_IMPR_MENS("No se pudo insertar los datos del cliente")
            Case "3"
                pr_IMPR_MENS("No se pudo insertar los datos del financiador")
            Case "4"
                pr_IMPR_MENS("No se logr� cargar el documento")
            Case Else
                Me.ltrMensaje.Text = strResultado
        End Select
    End Sub

    Private Sub cboDepositante_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepositante.SelectedIndexChanged
        loadAlmacen()
    End Sub

    Private Sub cboFinanciador_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFinanciador.SelectedIndexChanged
        loadDestinatario()
    End Sub

    Private Sub dgdDestinatario_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdDestinatario.ItemCreated
        If e.Item.ItemType = ListItemType.Header Then
            Dim ck1 As CheckBox = CType(e.Item.FindControl("chkAll"), CheckBox)
            If Not IsNothing(ck1) Then
                ck1.Attributes.Add("onclick", "Todos(this, 'chkItem');")
            End If
        End If
    End Sub
End Class
